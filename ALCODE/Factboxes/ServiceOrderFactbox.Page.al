page 56323 "ICI Service Order Factbox"
{
    Caption = 'Ticket Factbox', Comment = 'de-DE=Ticket Factbox';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service header";

    layout
    {
        area(content)
        {
            field("ICI Order Tickets - Open"; Rec."ICI Order Tickets - Open")
            {
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;
            }
            field("ICI Order Tickets - Processing"; Rec."ICI Order Tickets - Processing")
            {
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
            }
            field("ICI Order Tickets - Waiting"; Rec."ICI Order Tickets - Waiting")
            {
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ApplicationArea = All;
            }
            field("ICI Order Tickets - Closed"; Rec."ICI Order Tickets - Closed")
            {
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;
            }
        }
    }

    actions
    {
    }
}
