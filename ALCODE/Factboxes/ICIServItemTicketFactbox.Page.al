page 56336 "ICI Serv. Item Ticket Factbox"
{
    Caption = 'Serv. Item Ticketinfo Factbox', Comment = 'de-DE=Serviceartikel - Tickets';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service Item";

    layout
    {
        area(content)
        {
            field("ICI Tickets - Preparation"; Rec."ICI Tickets - Preparation")
            {
                ToolTip = 'No. of Tickets - Preparation', Comment = 'de-DE=Tickets - Vorbereitung';
                ApplicationArea = All;
                Visible = false;
            }
            field("ICI Tickets - Open"; Rec."ICI Tickets - Open")
            {
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;
            }
            field("ICI Tickets - Processing"; Rec."ICI Tickets - Processing")
            {
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
            }
            field("ICI Tickets - Waiting"; Rec."ICI Tickets - Waiting")
            {
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ApplicationArea = All;
                Visible = false;
            }
            field("ICI Tickets - Closed"; Rec."ICI Tickets - Closed")
            {
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;
            }
            field("ICI Tickets - Total"; Rec."ICI Tickets - Total")
            {
                ToolTip = 'No. of Tickets - Total', Comment = 'de-DE=Tickets - Gesamt';
                ApplicationArea = All;
                Visible = false;
            }
        }
    }
}
