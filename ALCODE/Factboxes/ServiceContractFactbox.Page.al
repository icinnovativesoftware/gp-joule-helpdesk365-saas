page 56327 "ICI Service Contract Factbox"
{
    Caption = 'Ticket Factbox', Comment = 'de-DE=Ticket Factbox';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service Contract Header";

    layout
    {
        area(content)
        {
            field("ICI Con. Tickets - Open"; Rec."ICI Con. Tickets - Open")
            {
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;
            }
            field("ICI Con. Tickets - Processing"; Rec."ICI Con. Tickets - Processing")
            {
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
            }
            field("ICI Con. Tickets - Waiting"; Rec."ICI Con. Tickets - Waiting")
            {
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ApplicationArea = All;
            }
            field("ICI Con. Tickets - Closed"; Rec."ICI Con. Tickets - Closed")
            {
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;
            }
        }
    }

    actions
    {
    }
}
