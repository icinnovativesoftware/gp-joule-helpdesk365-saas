page 56328 "ICI Service Con. Quote Factbox"
{
    Caption = 'Ticket Factbox', Comment = 'de-DE=Ticket Factbox';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service Contract Header";

    layout
    {
        area(content)
        {
            field("ICI Quote Tickets - Open"; Rec."ICI Quote Tickets - Open")
            {
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;
            }
            field("ICI Quote Tickets - Processing"; Rec."ICI Quote Tickets - Processing")
            {
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
            }
            field("ICI Quote Tickets - Waiting"; Rec."ICI Quote Tickets - Waiting")
            {
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ApplicationArea = All;
            }
            field("ICI Quote Tickets - Closed"; Rec."ICI Quote Tickets - Closed")
            {
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;
            }
        }
    }

    actions
    {
    }
}
