page 56334 "ICI Ticket Doc. Factbox"
{

    Caption = 'Ticket Doc. Factbox', Comment = 'de-DE=Erzeugte Belege';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "ICI Support Ticket";

    layout
    {
        area(content)
        {
            group("Sales Docs")
            {
                Caption = 'Sales Docs', Comment = 'de-DE=Verkauf';
                field(NoOfSalesQuote; NoOfSalesQuote)
                {
                    Caption = 'No. of Sales Quote', Comment = 'de-DE=Angebote';
                    ToolTip = 'No. of Sales Quote', Comment = 'de-DE=Angebote';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesDocs("Sales Document Type"::Quote);
                    end;
                }
                field(NoOfSalesOrder; NoOfSalesOrder)
                {
                    Caption = 'No. of Sales Order', Comment = 'de-DE=Aufträge';
                    ToolTip = 'No. of Sales Order', Comment = 'de-DE=Aufträge';

                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesDocs("Sales Document Type"::Order);
                    end;
                }
                field(NoOfSalesInvoice; NoOfSalesInvoice)
                {
                    Caption = 'No. of Sales Invoice', Comment = 'de-DE=Rechnungen';
                    ToolTip = 'No. of Sales Invoice', Comment = 'de-DE=Rechnungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesDocs("Sales Document Type"::Invoice);
                    end;
                }
                field(NoOfSalesReturnOrder; NoOfSalesReturnOrder)
                {
                    Caption = 'No. of Sales Return Order', Comment = 'de-DE=Reklamationen';
                    ToolTip = 'No. of Sales Return Order', Comment = 'de-DE=Reklamationen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesDocs("Sales Document Type"::"Return Order");
                    end;
                }
                field(NoOfSalesCrMemo; NoOfSalesCrMemo)
                {
                    Caption = 'No. of Sales Cr. Memo', Comment = 'de-DE=Gutschriften';
                    ToolTip = 'No. of Sales Cr. Memo', Comment = 'de-DE=Gutschriften';
                    // Visible = false;
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesDocs("Sales Document Type"::"Credit Memo");
                    end;
                }
                field(NoOfPstdSalesShip; NoOfPstdSalesShip)
                {
                    Caption = 'No. of Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferungen';
                    ToolTip = 'No. of Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferungen';
                    // Visible = false;
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesShipments();
                    end;
                }
                field(NoOfPstdSalesInvoice; NoOfPstdSalesInvoice)
                {
                    Caption = 'No. of Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ToolTip = 'No. of Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesInvoices();
                    end;
                }
                field(NoOfPstdSalesCrMemo; NoOfPstdSalesCrMemo)
                {
                    Caption = 'No. of Posted Sales Credit Memo', Comment = 'de-DE=Geb. Gutschriften';
                    ToolTip = 'No. of Posted Sales Credit Memo', Comment = 'de-DE=Geb. Gutschriften';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenSalesCrMemos();
                    end;
                }
                field(NoofOpportunitys; NoofOpportunitys)
                {
                    Caption = 'No. of Opportunities', Comment = 'de-DE=Chancen';
                    ToolTip = 'No. of Opportunities', Comment = 'de-DE=Verkaufschancen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenOpportunities();
                    end;
                }
            }
            group("Purchase Docs")
            {
                Caption = 'Purchase Docs', Comment = 'de-DE=Einkauf';
                field(NoOfPurchOrder; NoOfPurchOrder)
                {
                    Caption = 'No. of Purch Order', Comment = 'de-DE=Bestellungen';
                    ToolTip = 'No. of Purch Order', Comment = 'de-DE=Bestellungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenPurchaseDocs(GlobPurchaseHeader."Document Type"::Order);
                    end;
                }
                field(NoOfPurchReturnOrder; NoOfPurchReturnOrder)
                {
                    Caption = 'No. of Purch Return Order', Comment = 'de-DE=Reklamationen';
                    ToolTip = 'No. of Purch Return Order', Comment = 'de-DE=Reklamationen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenPurchaseDocs(GlobPurchaseHeader."Document Type"::"Return Order");
                    end;
                }
                field(NoOfPstdPurchInvoice; NoOfPstdPurchInvoice)
                {
                    Caption = 'No. of Pstd. Purch. Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ToolTip = 'No. of Pstd. Purch. Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenPurchInvoices();
                    end;
                }
                field(NoOfPstdPurchCrMemo; NoOfPstdPurchCrMemo)
                {
                    Caption = 'No. of Pstd. Purch. Cr. Memo', Comment = 'de-DE=Geb. Gutschriften';
                    ToolTip = 'No. of Pstd. Purch. Cr. Memo', Comment = 'de-DE=Geb. Gutschriften';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenPurchCrMemos();
                    end;
                }
            }
            group("Service Docs")
            {
                Caption = 'Service Docs', Comment = 'de-DE=Service';
                field(NoOfServiceQuote; NoOfServiceQuote)
                {
                    Caption = 'No. of Service Quote', Comment = 'de-DE=Angebote';
                    ToolTip = 'No. of Service Quote', Comment = 'de-DE=Angebote';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServiceDocs("Service Document Type"::Quote);
                    end;
                }
                field(NoOfServiceOrder; NoOfServiceOrder)
                {
                    Caption = 'No. of Service Order', Comment = 'de-DE=Aufträge';
                    ToolTip = 'No. of Service Order', Comment = 'de-DE=Aufträge';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServiceDocs("Service Document Type"::Order);
                    end;
                }
                field(NoOfServiceInvoice; NoOfServiceInvoice)
                {
                    Caption = 'No. of Service Invoice', Comment = 'de-DE=Rechnungen';
                    ToolTip = 'No. of Service Invoice', Comment = 'de-DE=Rechnungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServiceDocs("Service Document Type"::Invoice);
                    end;
                }
                field(NoOfPstdServiceShip; NoOfPstdServiceShip)
                {
                    Caption = 'No. of Pst. Service Shipments', Comment = 'de-DE=Geb. Lieferungen';
                    ToolTip = 'No. of Pst. Service Shipments', Comment = 'de-DE=Geb. Lieferungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServShipments();
                    end;
                }
                field(NoOfPstdServiceInvoice; NoOfPstdServiceInvoice)
                {
                    Caption = 'No. of Pst. Service Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ToolTip = 'No. of Pst. Service Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServInvoices();
                    end;
                }
                field(NoOfPstdServiceCrMemo; NoOfPstdServiceCrMemo)
                {
                    Caption = 'No. of Pst. Service Cr.Memos', Comment = 'de-DE=Geb. Gutschriften';
                    ToolTip = 'No. of Pst. Service Cr.Memos', Comment = 'de-DE=Geb. Gutschriften';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenServCrMemos();
                    end;
                }

            }
            group("Other")
            {
                Caption = 'Other', Comment = 'de-DE=Sonstiges|en-US=Other';
                field(NoOfItemLedgerEntries; NoOfItemLedgerEntries)
                {
                    Caption = 'No. of Item Ledger Entries', Comment = 'de-DE=Artikelposten|en-US=No. of Item Ledger Entries';
                    ToolTip = 'No. of Item Ledger Entries', Comment = 'de-DE=Artikelposten|en-US=No. of Item Ledger Entries';
                    ApplicationArea = All;

                    trigger OnDrillDown()
                    begin
                        OpenItemLedgerEntries();
                    end;
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        UpdatePage();
    end;

    var
        GlobSalesHeader: Record "Sales Header";
        GlobPurchaseHeader: Record "Purchase Header";
        GlobServiceHeader: Record "Service Header";
        NoOfSalesQuote: Integer;
        NoOfSalesOrder: Integer;
        NoOfSalesInvoice: Integer;
        NoOfSalesReturnOrder: Integer;
        NoOfSalesCrMemo: Integer;
        NoOfPstdSalesInvoice: Integer;
        NoOfPstdSalesCrMemo: Integer;
        NoOfPurchOrder: Integer;
        NoOfPurchReturnOrder: Integer;
        NoOfPstdPurchInvoice: Integer;
        NoOfPstdPurchCrMemo: Integer;
        NoOfServiceQuote: Integer;
        NoOfServiceOrder: Integer;
        NoOfServiceInvoice: Integer;
        NoOfPstdServiceInvoice: Integer;
        NoOfPstdSalesShip: Integer;
        NoOfPstdServiceShip: Integer;
        NoOfPstdServiceCrMemo: Integer;
        NoofOpportunitys: Integer;
        NoOfItemLedgerEntries: Integer;

    local procedure UpdatePage()
    begin
        NoOfSalesQuote := CountSalesDoc("Sales Document Type"::Quote);
        NoOfSalesOrder := CountSalesDoc("Sales Document Type"::Order);
        NoOfSalesInvoice := CountSalesDoc("Sales Document Type"::Invoice);
        NoOfSalesReturnOrder := CountSalesDoc("Sales Document Type"::"Return Order");
        NoOfSalesCrMemo := CountSalesDoc("Sales Document Type"::"Credit Memo");
        NoOfPstdSalesInvoice := CountPstdSalesInvoiceDoc();
        NoOfPstdSalesCrMemo := CountPstdSalesCrMemoDoc();
        NoOfPstdSalesShip := CountPstdSalesShipmentDoc();

        NoOfPurchOrder := CountPurchDoc("Purchase Document Type"::Order);
        NoOfPurchReturnOrder := CountPurchDoc("Purchase Document Type"::"Return Order");
        NoOfPstdPurchInvoice := CountPstdPurchInvoiceDoc();
        NoOfPstdPurchCrMemo := CountPstdPurchCrMemoDoc();

        NoOfServiceQuote := CountServiceDoc(GlobServiceHeader."Document Type"::Quote);
        NoOfServiceOrder := CountServiceDoc(GlobServiceHeader."Document Type"::Order);
        NoOfServiceInvoice := CountServiceDoc(GlobServiceHeader."Document Type"::Invoice);
        NoOfPstdServiceInvoice := CountPstdServiceInvoiceDoc();
        NoOfPstdServiceShip := CountPstdServiceShipmentDoc();
        NoOfPstdServiceCrMemo := CountPstdServiceCrMemoDoc();

        NoofOpportunitys := CountOpportunitys();

        NoOfItemLedgerEntries := CountItemLedgerEntries();
    end;

    local procedure CountSalesDoc(DocType: Enum "Sales Document Type"): Integer
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.SetRange("Document Type", DocType);
        SalesHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(SalesHeader.Count());
    end;

    local procedure OpenSalesDocs(DocType: Enum "Sales Document Type")
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.SetRange("Document Type", DocType);
        SalesHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesHeader.SetRange("ICI Support Ticket No.", Rec."No.");

        CASE DocType OF
            "Sales Document Type"::Quote:
                PAGE.RUN(PAGE::"Sales Quotes", SalesHeader);
            "Sales Document Type"::Order:
                PAGE.RUN(PAGE::"Sales Order List", SalesHeader);
            "Sales Document Type"::"Return Order":
                PAGE.RUN(PAGE::"Sales Return Order List", SalesHeader);
            "Sales Document Type"::"Credit Memo":
                PAGE.RUN(PAGE::"Sales Credit Memos", SalesHeader);
            "Sales Document Type"::Invoice:
                PAGE.RUN(PAGE::"Sales Invoice List", SalesHeader);
        END;
    end;

    local procedure CountPstdSalesInvoiceDoc(): Integer
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        SalesInvoiceHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesInvoiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(SalesInvoiceHeader.Count());
    end;

    local procedure OpenSalesInvoices()
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        SalesInvoiceHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesInvoiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Sales Invoices", SalesInvoiceHeader);
    end;

    local procedure CountPstdSalesShipmentDoc(): Integer
    var
        SalesShipmentHeader: Record "Sales Shipment Header";
    begin
        SalesShipmentHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesShipmentHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(SalesShipmentHeader.Count());
    end;

    local procedure CountOpportunitys(): Integer
    var
        Opportunity: Record Opportunity;
    begin
        Opportunity.SetCurrentKey("ICI Support Ticket No.");
        Opportunity.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(Opportunity.Count());
    end;

    local procedure OpenSalesShipments()
    var
        SalesShipmentHeader: Record "Sales Shipment Header";
    begin
        SalesShipmentHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesShipmentHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Sales Shipments", SalesShipmentHeader);
    end;

    local procedure CountPstdSalesCrMemoDoc(): Integer
    var
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        SalesCrMemoHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesCrMemoHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(SalesCrMemoHeader.Count());
    end;

    local procedure OpenSalesCrMemos()
    var
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        SalesCrMemoHeader.SetCurrentKey("ICI Support Ticket No.");
        SalesCrMemoHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Sales Credit Memos", SalesCrMemoHeader);
    end;

    local procedure OpenOpportunities()
    var
        Opportunity: Record Opportunity;
    begin
        Opportunity.SetCurrentKey("ICI Support Ticket No.");
        Opportunity.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Opportunity List", Opportunity);
    end;

    local procedure CountPurchDoc(DocType: Enum "Purchase Document Type"): Integer
    var
        PurchaseHeader: Record "Purchase Header";
    begin
        PurchaseHeader.SetRange("Document Type", DocType);
        PurchaseHeader.SetCurrentKey("ICI Support Ticket No.");
        PurchaseHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(PurchaseHeader.Count());
    end;

    local procedure OpenPurchaseDocs(DocType: Enum "Purchase Document Type")
    var
        PurchaseHeader: Record "Purchase Header";
    begin
        PurchaseHeader.SetRange("Document Type", DocType);
        PurchaseHeader.SetCurrentKey("ICI Support Ticket No.");
        PurchaseHeader.SetRange("ICI Support Ticket No.", Rec."No.");

        CASE DocType OF
            GlobSalesHeader."Document Type"::Quote:
                PAGE.RUN(PAGE::"Purchase Quotes", PurchaseHeader);
            GlobSalesHeader."Document Type"::Order:
                PAGE.RUN(PAGE::"Purchase Order List", PurchaseHeader);
            GlobSalesHeader."Document Type"::"Return Order":
                PAGE.RUN(PAGE::"Purchase Return Order List", PurchaseHeader);
            GlobSalesHeader."Document Type"::"Credit Memo":
                PAGE.RUN(PAGE::"Purchase Credit Memos", PurchaseHeader);
            GlobSalesHeader."Document Type"::Invoice:
                PAGE.RUN(PAGE::"Purchase Invoices", PurchaseHeader);
        END;
    end;

    local procedure CountPstdPurchInvoiceDoc(): Integer
    var
        PurchInvHeader: Record "Purch. Inv. Header";
    begin
        PurchInvHeader.SetCurrentKey("ICI Support Ticket No.");
        PurchInvHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(PurchInvHeader.Count());
    end;

    local procedure OpenPurchInvoices()
    var
        PurchInvHeader: Record "Purch. Inv. Header";
    begin
        PurchInvHeader.SetCurrentKey("ICI Support Ticket No.");
        PurchInvHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Purchase Invoices", PurchInvHeader);
    end;

    local procedure CountPstdPurchCrMemoDoc(): Integer
    var
        PurchCrMemoHdr: Record "Purch. Cr. Memo Hdr.";
    begin
        PurchCrMemoHdr.SetCurrentKey("ICI Support Ticket No.");
        PurchCrMemoHdr.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(PurchCrMemoHdr.Count());
    end;

    local procedure OpenPurchCrMemos()
    var
        PurchCrMemoHdr: Record "Purch. Cr. Memo Hdr.";
    begin
        PurchCrMemoHdr.SetCurrentKey("ICI Support Ticket No.");
        PurchCrMemoHdr.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Purchase Credit Memos", PurchCrMemoHdr);
    end;


    local procedure CountServiceDoc(DocType: Enum "Service Document Type"): Integer
    var
        ServiceHeader: Record "Service Header";
    begin
        ServiceHeader.SetRange("Document Type", DocType);
        ServiceHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(ServiceHeader.Count());
    end;

    local procedure OpenServiceDocs(DocType: Enum "Service Document Type")
    var
        ServiceHeader: Record "Service Header";
    begin
        ServiceHeader.SetRange("Document Type", DocType);
        ServiceHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");

        CASE DocType OF
            GlobSalesHeader."Document Type"::Quote:
                PAGE.RUN(PAGE::"Service Quotes", ServiceHeader);
            GlobSalesHeader."Document Type"::Order:
                PAGE.RUN(PAGE::"Service Orders", ServiceHeader);
            GlobSalesHeader."Document Type"::"Credit Memo":
                PAGE.RUN(PAGE::"Service Credit Memos", ServiceHeader);
            GlobSalesHeader."Document Type"::Invoice:
                PAGE.RUN(PAGE::"Service Invoices", ServiceHeader);
        END;
    end;


    local procedure CountPstdServiceInvoiceDoc(): Integer
    var
        ServiceInvoiceHeader: Record "Service Invoice Header";
    begin
        ServiceInvoiceHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceInvoiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(ServiceInvoiceHeader.Count());
    end;

    local procedure OpenServInvoices()
    var
        ServiceInvoiceHeader: Record "Service Invoice Header";
    begin
        ServiceInvoiceHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceInvoiceHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Service Invoices", ServiceInvoiceHeader);
    end;

    local procedure CountPstdServiceShipmentDoc(): Integer
    var
        ServiceShipmentHeader: Record "Service Shipment Header";
    begin
        ServiceShipmentHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceShipmentHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(ServiceShipmentHeader.Count());
    end;

    local procedure OpenServShipments()
    var
        ServiceShipmentHeader: Record "Service Shipment Header";
    begin
        ServiceShipmentHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceShipmentHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Service Shipments", ServiceShipmentHeader);
    end;

    local procedure CountPstdServiceCrMemoDoc(): Integer
    var
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
    begin
        ServiceCrMemoHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceCrMemoHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        EXIT(ServiceCrMemoHeader.Count());
    end;

    local procedure OpenServCrMemos()
    var
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
    begin
        ServiceCrMemoHeader.SetCurrentKey("ICI Support Ticket No.");
        ServiceCrMemoHeader.SetRange("ICI Support Ticket No.", Rec."No.");
        PAGE.RUN(PAGE::"Posted Service Credit Memos", ServiceCrMemoHeader);
    end;

    local procedure CountItemLedgerEntries(): Integer
    var
        ItemLedgerEntry: Record "Item Ledger Entry";
    begin
        ItemLedgerEntry.SetCurrentKey("Document No.");
        ItemLedgerEntry.SetRange("Document No.", Rec."No.");
        Exit(ItemLedgerEntry.Count());
    end;

    local procedure OpenItemLedgerEntries()
    var
        ItemLedgerEntry: Record "Item Ledger Entry";
    begin
        ItemLedgerEntry.SetCurrentKey("Document No.");
        ItemLedgerEntry.SetRange("Document No.", Rec."No.");
        PAGE.RUN(PAGE::"Item Ledger Entries", ItemLedgerEntry);
    end;
}
