page 56337 "ICI Item Ticket Factbox"
{

    Caption = 'ICI Item Ticket Factbox', Comment = 'de-DE=Artikel - Tickets';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Item";

    layout
    {
        area(content)
        {
            field("Item No."; Rec."No.")
            {
                Caption = 'No.', Comment = 'de-DE=Nr.';
                ToolTip = 'No.', Comment = 'de-DE=Nr.';
                Lookup = false;
                ApplicationArea = All;

                trigger OnDrillDown()
                var
                    Item: Record Item;
                begin
                    Item.GET(Rec."No.");
                    PAGE.RUN(PAGE::"Item Card", Item)
                end;
            }
            field("Item Description"; Rec."Description")
            {
                Caption = 'Description', Comment = 'de-DE=Beschreibung';
                ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                AssistEdit = false;
                DrillDown = false;
                Lookup = false;
                ApplicationArea = All;
            }
            field(ItemTicketsOpen; ItemTicketsOpen)
            {
                Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;

                trigger OnDrillDown() // Causes Blue Underline + Clickable in Factboxes
                begin
                    LookupTickets("ICI Ticket State"::Open);
                end;
            }
            field(ItemTicketsProc; ItemTicketsProc)
            {

                Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
                trigger OnDrillDown() // Causes Blue Underline + Clickable in Factboxes
                begin
                    LookupTickets("ICI Ticket State"::Processing);
                end;
            }
            field(ItemTicketsWfa; ItemTicketsWfa)
            {
                Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                Visible = false;
                ApplicationArea = All;

                trigger OnDrillDown() // Causes Blue Underline + Clickable in Factboxes
                begin
                    LookupTickets("ICI Ticket State"::Waiting);
                end;

            }
            field(ItemTicketsClose; ItemTicketsClose)
            {
                Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;

                trigger OnDrillDown() // Causes Blue Underline + Clickable in Factboxes
                begin
                    LookupTickets("ICI Ticket State"::Closed);
                end;
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        CLEAR(ItemTicketsPrep);
        CLEAR(ItemTicketsOpen);
        CLEAR(ItemTicketsProc);
        CLEAR(ItemTicketsWfa);
        CLEAR(ItemTicketsClose);

        ICITicketsPerItem.SETRANGE("No", Rec."No.");
        ICITicketsPerItem.OPEN();

        WHILE ICITicketsPerItem.READ() DO
            CASE ICITicketsPerItem.State OF
                ICITicketsPerItem.State::Preparation:
                    ItemTicketsPrep := ICITicketsPerItem.Count_;
                ICITicketsPerItem.State::Open:
                    ItemTicketsOpen := ICITicketsPerItem.Count_;
                ICITicketsPerItem.State::Processing:
                    ItemTicketsProc := ICITicketsPerItem.Count_;
                ICITicketsPerItem.State::Waiting:
                    ItemTicketsWfa := ICITicketsPerItem.Count_;
                ICITicketsPerItem.State::Closed:
                    ItemTicketsClose := ICITicketsPerItem.Count_;
            END;
    end;

    var
        ICITicketsPerItem: Query "ICI Tickets Per Item";
        ItemTicketsPrep: Integer;
        ItemTicketsOpen: Integer;
        ItemTicketsProc: Integer;
        ItemTicketsWfa: Integer;
        ItemTicketsClose: Integer;

    local procedure LookupTickets(State: Enum "ICI Ticket State")
    var
        TempICISupportTicket: Record "ICI Support Ticket" temporary;
        ICISupportTicket: Record "ICI Support Ticket";
        ServiceItem: Record "Service Item";
    begin
        CLEAR(TempICISupportTicket);

        ServiceItem.SETCURRENTKEY("Item No.");
        ServiceItem.SETRANGE("Item No.", Rec."No.");
        IF ServiceItem.FindSet(false) THEN
            REPEAT
                ICISupportTicket.SETCURRENTKEY("Service Item No.");
                ICISupportTicket.SETRANGE("Service Item No.", ServiceItem."No.");
                ICISupportTicket.SETRANGE("Ticket State", State);
                IF ICISupportTicket.FindSet(false) THEN
                    REPEAT
                        TempICISupportTicket := ICISupportTicket;
                        TempICISupportTicket.INSERT();

                    UNTIL ICISupportTicket.NEXT() = 0;
            UNTIL ServiceItem.NEXT() = 0;

        PAGE.RUN(PAGE::"ICI Support Ticket List", TempICISupportTicket);
    end;
}
