page 56338 "ICI Support Ticket Factbox"
{
    Caption = 'ICI Support Ticket Factbox', Comment = 'de-DE=Ticket';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "ICI Support Ticket";

    layout
    {
        area(content)
        {
            field("Last Activity Date"; Rec."Last Activity Date")
            {
                ApplicationArea = All;
                ToolTip = 'Last Activity Date', Comment = 'de-DE=Letzte Aktivität am';
            }
            field("Last Activity By Code"; Rec."Last Activity By Code")
            {
                ApplicationArea = All;
                ToolTip = 'Last Activity By Code', Comment = 'de-DE=Letzte Aktivität von';
            }

            field("Created On"; Rec."Created On")
            {
                ApplicationArea = All;
                ToolTip = 'Created On', Comment = 'de-DE=Erstellt am';
            }
            field("First Opened On"; Rec."First Opened On")
            {
                ApplicationArea = All;
                ToolTip = 'First Opened On', Comment = 'de-DE=Eröffnet am';
            }
            field("First Closed On"; Rec."First Closed On")
            {
                ApplicationArea = All;
                ToolTip = 'First Closed On', Comment = 'de-DE=Geschlossen am';
            }
            field(TicketProcessingTime; Rec.GetTicketProcessingTime())
            {
                ApplicationArea = All;
                ToolTip = 'Processing Time', Comment = 'de-DE=Bearbeitungsdauer';
                Caption = 'Processing Time', Comment = 'de-DE=Bearbeitungsdauer';
            }
        }
    }

}
