page 56333 "ICI Service Item Factbox"
{

    Caption = 'Service Item', Comment = 'de-DE=Serviceartikel';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service Item";

    layout
    {
        area(content)
        {
            field("No."; Rec."No.")
            {
                ApplicationArea = All;
                ToolTip = 'No.', Comment = 'de-DE=Nr.';
                trigger OnDrillDown()
                var
                    ServiceItem: Record "Service Item";
                begin
                    if Rec."No." = '' THEN
                        exit;
                    if not ServiceItem.get(Rec."No.") then
                        exit;
                    PAGE.RUN(Page::"Service Item Card", ServiceItem);
                end;
            }
            field(Description; Rec.Description)
            {
                ApplicationArea = All;
                ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
            }
            field("Warranty Ending Date (Labor)"; Rec."Warranty Ending Date (Labor)")
            {
                ApplicationArea = All;
                ToolTip = 'Warranty Ending Date (Labor)', Comment = 'de-DE=Garantieende (Arbeit)';
            }
            field("Warranty Ending Date (Parts)"; Rec."Warranty Ending Date (Parts)")
            {
                ApplicationArea = All;
                ToolTip = 'Warranty Ending Date (Parts)', Comment = 'de-DE=Garantieende (Teile)';
            }
            field("Warranty % (Labor)"; Rec."Warranty % (Labor)")
            {
                ApplicationArea = All;
                ToolTip = 'Warranty % (Labor)', Comment = 'de-DE=Garantie % (Arbeit)';
            }
            field("Warranty % (Parts)"; Rec."Warranty % (Parts)")
            {
                ApplicationArea = All;
                ToolTip = 'Warranty % (Parts)', Comment = 'de-DE=Garantie % (Teile)';
            }
            field(Comment; Rec.Comment)
            {
                ApplicationArea = All;
                ToolTip = 'Comment', Comment = 'de-DE=Kommentare';
            }
            field("No. of Active Contracts"; Rec."No. of Active Contracts")
            {
                ApplicationArea = All;
                ToolTip = 'No. of Active Contracts', Comment = 'de-DE=Anzahl aktiver Verträge';
            }
        }
    }
    actions
    {
    }

}
