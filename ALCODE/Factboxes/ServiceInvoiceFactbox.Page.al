page 56326 "ICI Service Invoice Factbox"
{

    Caption = 'Ticket Factbox', Comment = 'de-DE=Ticket Factbox';
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = CardPart;
    SourceTable = "Service Invoice Header";

    layout
    {
        area(content)
        {
            field("ICI Tickets - Open"; Rec."ICI Tickets - Open")
            {
                ToolTip = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
                ApplicationArea = All;
            }
            field("ICI Tickets - Processing"; Rec."ICI Tickets - Processing")
            {
                ToolTip = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
                ApplicationArea = All;
            }
            field("ICI Tickets - Waiting"; Rec."ICI Tickets - Waiting")
            {
                ToolTip = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
                ApplicationArea = All;
            }
            field("ICI Tickets - Closed"; Rec."ICI Tickets - Closed")
            {
                ToolTip = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
                ApplicationArea = All;
            }
        }
    }

    actions
    {
    }

}
