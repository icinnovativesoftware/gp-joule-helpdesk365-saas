codeunit 56296 "ICI Service Integration Mgt."
{
    procedure CreateServiceItemForTicket(SerialNo: Code[50]; TicketNo: Code[20]): Code[20]
    var
        ServiceItem: Record "Service Item";
        ICISupportTicket: Record "ICI Support Ticket";
        AutoCreateLbl: Label 'Automatically Created Service Item %1 from Ticket %2', Comment = '%1=SerialNo; %2=TicketNo|de-DE=Auto. erz. Serviceartikel %1 aus Ticket %2';
    begin
        ServiceItem.INIT();
        ServiceItem.Validate("No.", '');
        ServiceItem.INSERT(true);

        ServiceItem.Validate(Description, StrSubstNo(AutoCreateLbl, SerialNo, TicketNo));
        ServiceItem.Validate("Serial No.", SerialNo);
        ICISupportTicket.GET(TicketNo);
        IF ICISupportTicket."Customer No." <> '' THEN
            ServiceItem.Validate("Customer No.", ICISupportTicket."Customer No.");

        IF ICISupportTicket."Item No." <> '' then
            ServiceItem.Validate("Item No.", ICISupportTicket."Item No.");

        ServiceItem.Modify(true);
        EXIT(ServiceItem."No.");
    end;

    procedure UpdateServiceItem(ServiceItemNo: Code[20]; CustomerNo: code[20])
    var
        Customer: Record Customer;
        ServiceItem: Record "Service Item";
        UpdateCustomerLbl: Label 'Do you want to change the Customer No.in the Service Item as well?', Comment = 'de-DE=Wollen Sie auch die Debitorennr. auch im Serviceartikel ändern?';
    begin
        IF NOT ServiceItem.GET(ServiceItemNo) THEN
            exit;
        IF ServiceItem."Customer No." = CustomerNo then
            exit;
        IF not Customer.GET(CustomerNo) THEN
            exit;


        IF GuiAllowed then
            IF NOT Confirm(UpdateCustomerLbl) THEN
                exit;
        ServiceItem.Validate("Customer No.", Customer."No.");
        ServiceItem.Modify(true);
    end;

    PROCEDURE FindShipToAddress(CustomerNo: Code[20]; ShipToName: Text[100]; ShipToName2: Text[50]; pShipToAddress: Text[100]; pShipToAddress2: Text[50]; ShipToPostCode: Code[20];
                            ShipToCity: Text[30]; ShipToCounty: Text[30]; ShipToCountryRegionCode: Code[10]): Code[10]
    VAR
        ShiptoAddress: Record "Ship-to Address";
    BEGIN
        IF CustomerNo = '' THEN
            EXIT;

        ShiptoAddress.SETRANGE("Customer No.", CustomerNo);
        ShiptoAddress.SETFILTER(Name, '%1', ShipToName);
        ShiptoAddress.SETFILTER("Name 2", '%1', ShipToName2);
        ShiptoAddress.SETFILTER(Address, '%1', pShipToAddress);
        ShiptoAddress.SETFILTER("Address 2", '%1', pShipToAddress2);
        ShiptoAddress.SETFILTER("Post Code", '%1', ShipToPostCode);
        ShiptoAddress.SETFILTER(City, '%1', ShipToCity);
        ShiptoAddress.SETFILTER("Country/Region Code", '%1', ShipToCountryRegionCode);
        ShiptoAddress.SETFILTER(County, '%1', ShipToCounty);
        IF ShiptoAddress.FINDLAST() THEN
            EXIT(ShiptoAddress.Code)
    END;

}
