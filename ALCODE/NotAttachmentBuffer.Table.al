table 56314 "ICI Not. Attachment Buffer"
{
    Caption = 'Not. Attachment Buffer', Comment = 'de-DE=Benachrichtigungsanhang';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(10; Name; Text[250])
        {
            Caption = 'Name', Comment = 'de-DE=Name';
            DataClassification = CustomerContent;
        }
        field(11; "Content Type"; Text[250])
        {
            Caption = 'Content Type', Comment = 'de-DE=MIME/Type';
            DataClassification = CustomerContent;
        }
        field(12; Base64; Blob)
        {
            Caption = 'Base64', Comment = 'de-DE=Daten';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

}
