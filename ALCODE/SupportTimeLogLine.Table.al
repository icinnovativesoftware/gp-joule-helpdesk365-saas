table 56299 "ICI Support Time Log Line"
{
    Caption = 'ICI Support Time Log Line', Comment = 'de-DE=Ticket Zeiterfassungszeile';
    DrillDownPageID = "ICI Support Time Log Subpage";
    LookupPageID = "ICI Support Time Log Subpage";

    fields
    {
        field(1; "Support Ticket No."; Code[20])
        {
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket"."No.";
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                ICISupportTicket: Record "ICI Support Ticket";
            begin
                IF ICISupportTicket.GET("Support Ticket No.") THEN
                    VALIDATE("Customer No.", ICISupportTicket."Customer No.");
            end;
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No', Comment = 'de-DE=Zeilennr.';
            Editable = true;
            DataClassification = CustomerContent;
        }

        field(10; "Support User"; Code[50])
        {
            Caption = 'Support User', Comment = 'de-DE=Benutzer';
            Editable = true;
            NotBlank = true;
            TableRelation = "ICI Support User";
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                ICISupportUser: Record "ICI Support User";
            begin
                ICISupportUser.GetCurrUser(ICISupportUser);
                IF ICISupportUser."Employee No." <> '' then
                    Validate("Employee No.", ICISupportUser."Employee No.");
            end;
        }
        field(11; Date; Date)
        {
            Caption = 'Date', Comment = 'de-DE=Datum';
            Editable = true;
            DataClassification = CustomerContent;
        }

        field(12; "Customer No."; Code[20])
        {

            Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
            TableRelation = Customer."No.";
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                Customer: Record "Customer";
            begin
                CALCFIELDS("Customer Name");
                IF Customer.GET("Customer No.") THEN
                    IF Customer."Bill-to Customer No." <> '' THEN
                        "Bill-to Customer No." := Customer."Bill-to Customer No."
                    ELSE
                        "Bill-to Customer No." := Customer."No.";

            end;
        }
        field(13; "Customer Name"; Text[100])
        {
            CalcFormula = Lookup(Customer.Name WHERE("No." = FIELD("Customer No.")));

            Caption = 'Customer Name', Comment = 'de-DE=Debitorenname';
            Editable = false;
            FieldClass = FlowField;
        }

        field(15; "Employee No."; Code[20])
        {
            Caption = 'Employee No.', Comment = 'de-DE=Mitarbeiternr.';
            Editable = true;
            NotBlank = true;
            TableRelation = Employee."No.";
            DataClassification = CustomerContent;
        }

        field(16; Closed; Boolean)
        {
            Caption = 'Closed', Comment = 'de-DE=Geschlossen';
            DataClassification = CustomerContent;
        }

        field(18; "Sales Doc No."; Code[20])
        {
            Caption = 'Sales Doc No.', Comment = 'de-DE=Abgerechnet in VK-Beleg';
            Editable = false;
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                Cleared := ("Sales Doc No." <> '') OR ("Service Doc No." <> '');
            end;
        }
        // field(19; "Current Salesperson"; Code[50])
        // {
        //     CalcFormula = Lookup("ICI Support Ticket"."Support User ID" WHERE("No." = FIELD("Support Ticket No.")));
        //     Caption = 'Current Salesperson', Comment = 'de-DE=Zuständiger Verkäufer';
        //     Description = 'x';
        //     Editable = false;
        //     FieldClass = FlowField;
        // }
        field(23; Cleared; Boolean)
        {
            Caption = 'Cleared', Comment = 'de-DE=In Beleg';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                IF NOT Cleared THEN BEGIN
                    "Sales Doc No." := '';
                    "Service Doc No." := '';
                END;
            end;
        }
        field(5010; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }


        field(35; "Service Doc No."; Code[20])
        {
            Caption = 'Service Doc No.', Comment = 'de-DE=Abgerechnet in Servicebeleg';
            Editable = false;
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                Cleared := ("Sales Doc No." <> '') OR ("Service Doc No." <> '');
            end;
        }
        field(36; "Sales Payoff Type"; Enum "Sales Line Type")
        {
            Editable = false;
            Caption = 'Payoff Type', Comment = 'de-DE=Abrechnungsart';
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support User"."Sales Payoff Type" where("User ID" = field("Support User")));
        }
        field(37; "Sales Payoff No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Payoff No.', Comment = 'de-DE=Abrechnungsnr.';
            Editable = false;
            TableRelation = IF ("Sales Payoff Type" = CONST(" ")) "Standard Text"
            ELSE
            IF ("Sales Payoff Type" = CONST("G/L Account")) "G/L Account"
            ELSE
            IF ("Sales Payoff Type" = CONST("Resource")) Resource
            ELSE
            IF ("Sales Payoff Type" = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF ("Sales Payoff Type" = CONST("Charge (Item)")) "Item Charge"
            ELSE
            IF ("Sales Payoff Type" = CONST(Item)) Item;
            ValidateTableRelation = false;
        }

        field(1002; "Bill-to Customer No."; Code[20])
        {
            Caption = 'Bill-to Customer No.', Comment = 'de-DE=Rechnung an Deb. Nr.';
            TableRelation = Customer."No." WHERE("Bill-to Customer No." = FIELD("Customer No."));
            DataClassification = CustomerContent;
        }
        // field(5000; Accommodation; Boolean)
        // {
        //     Caption = 'Accommodation';
        //     DataClassification = CustomerContent;
        // }

        // field(5002; "Company Contact No."; Text[50])
        // {
        //     CalcFormula = Lookup("ICI Support Ticket"."Company Contact No." WHERE("No." = FIELD("Support Ticket No.")));
        //     Caption = 'Company Contact No.';
        //     Editable = false;
        //     FieldClass = FlowField;
        // }
        field(5003; "Support Ticket Status"; Enum "ICI Ticket State")
        {
            CalcFormula = Lookup("ICI Support Ticket"."Ticket State" WHERE("No." = FIELD("Support Ticket No.")));
            Caption = 'Support Ticket Status', Comment = 'de-DE=Ticketstatus';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5004; "Ticket Description"; Text[250])
        {
            CalcFormula = Lookup("ICI Support Ticket".Description WHERE("No." = FIELD("Support Ticket No.")));
            Caption = 'Ticket Description', Comment = 'de-DE=Ticket Beschreibung';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5005; "Time from"; Time)
        {
            Caption = 'Time from', Comment = 'de-DE=Zeit von';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                IF ("Time from" <> 0T) AND ("Time to" <> 0T) THEN
                    VALIDATE("Total Time", ConvIntToTime(CalcTimeDifference("Time from", "Time to")));
            end;
        }
        field(5006; "Time to"; Time)
        {
            Caption = 'Time to', Comment = 'de-DE=Zeit bis';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                IF ("Time from" <> 0T) AND ("Time to" <> 0T) THEN
                    VALIDATE("Total Time", ConvIntToTime(CalcTimeDifference("Time from", "Time to")));
            end;
        }
        field(5007; "Time Decimal"; Decimal)
        {
            Caption = 'Time Decimal', Comment = 'de-DE=Zeit Decimal';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                IF ("Time from" <> 0T) AND ("Time to" <> 0T) THEN
                    IF (CalcTimeDifference("Time from", "Time to") / 3600000) <> "Time Decimal" THEN
                        "Time to" := "Time from" + ROUND("Time Decimal" * 3600000);

                IF ("Time from" <> 0T) AND ("Time to" = 0T) THEN
                    "Time to" := "Time from" + ROUND("Time Decimal" * 3600000);

                IF ("Time from" = 0T) AND ("Time to" <> 0T) THEN
                    "Time from" := "Time to" - ROUND("Time Decimal" * 3600000);

                CheckTime();
            end;
        }
        field(5008; "Time Int"; Decimal)
        {
            Caption = 'Time Int', Comment = 'de-DE=Zeit Int';
            DecimalPlaces = 0 : 4;
            DataClassification = CustomerContent;
        }
        field(5009; "Total Time"; Time)
        {
            Caption = 'Total Time', Comment = 'de-DE=Gesamtzeit';
            Editable = false;
            DataClassification = CustomerContent;

            trigger OnValidate()

            begin
                "Time Int" := ConvTimeToInt("Total Time");
                "Time Decimal" := "Time Int" / 3600000;

                CheckTime();
            end;
        }
        field(5012; "Accounting Type"; Code[20])
        {
            Caption = 'Accounting', Comment = 'de-DE=Abrechnung';
            TableRelation = "ICI Support Time Acc. Type";
            DataClassification = CustomerContent;
        }
        field(5013; "No. of Comment Lines"; Integer)
        {
            CalcFormula = Count("ICI Support Time Log Comment" WHERE("Support Ticket No." = field("Support Ticket No."),
                                                                    "Time Log Line No." = FIELD("Line No.")
                                                                  ));
            Caption = 'No. of Comment Lines', Comment = 'de-DE=Anzahl Kommentarzeilen';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5014; "Discount %"; Decimal)
        {
            CalcFormula = lookup("ICI Support Time Acc. Type"."Discount %" where(Code = Field("Accounting Type")));
            Caption = 'Discount %', Comment = 'de-DE=Rabatt %';
            Editable = false;
            FieldClass = FlowField;
        }
        field(5015; "Work Type Code"; Code[20])
        {
            Caption = 'Work type', Comment = 'de-DE=Arbeitstyp';
            TableRelation = "Work Type";
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                TestField("Sales Payoff Type", "Sales Payoff Type"::Resource);
                TestField("Sales Payoff No.");
            end;
        }

    }

    keys
    {
        key(PQ; "Support Ticket No.", "Line No.")
        {
            Clustered = true;
            SumIndexFields = "Time Decimal";
        }
        key(Key1; "Employee No.", Date, "Line No.", "Time from")
        {
            SumIndexFields = "Time Decimal";
        }
        key(Key2; "Customer No.")
        {
        }
        key(Key3; "Customer No.", "Support Ticket No.")
        {
            SumIndexFields = "Time Decimal";
        }
        key(Key4; "Accounting Type", Date, "Customer No.")
        {
            SumIndexFields = "Time Decimal";
        }
        key(Key5; "Accounting Type", "Support Ticket No.")
        {
            SumIndexFields = "Time Decimal";
        }
        key(Key6; "Bill-to Customer No.", Date, "Support Ticket No.", "Accounting Type")
        {
            SumIndexFields = "Time Decimal";
        }
    }

    fieldgroups
    {
    }

    trigger OnInsert()
    begin
        VALIDATE("Support User", UserID());
    end;

    trigger OnDelete()
    begin
        TESTFIELD(Closed, FALSE);
    end;

    trigger OnModify()
    begin
        TESTFIELD(Closed, FALSE);
    end;

    trigger OnRename()
    begin
        TESTFIELD(Closed, FALSE);
    end;

    var
        TimeExceeds24HErr: Label 'Times cannot excceed this day, enter a value from 0:00 - 23:59 \ The Starttime cannot be less than the Endtime!';

    local procedure CheckTime()
    var
        SupportTimeLogLine: Record "ICI Support Time Log Line";
    begin
        IF ("Time from" > "Time to") AND ("Time from" <> 0T) THEN
            ERROR(TimeExceeds24HErr);

        SupportTimeLogLine.SETRANGE("Support User", "Support User");
        SupportTimeLogLine.SETRANGE(Date, Date);
        SupportTimeLogLine.SETFILTER("Line No.", '<>%1', "Line No.");
        SupportTimeLogLine.CALCSUMS("Time Decimal");
        IF SupportTimeLogLine."Time Decimal" + "Time Decimal" > 23.9997222222222222 THEN
            ERROR(TimeExceeds24HErr);
    end;

    procedure ConvTimeToInt(TimeVar: Time) IntVar: Integer
    begin
        IF TimeVar = 0T THEN
            IntVar := 0
        ELSE
            IntVar := TimeVar - 000000T;
    end;

    procedure ConvIntToTime(IntVar: Integer) TimeVar: Time
    begin
        TimeVar := 000000T + IntVar;
    end;

    procedure CalcTimeDifference(T1: Time; T2: Time) IntVar: Integer
    begin
        IntVar := T2 - T1;
    end;

    procedure GetPayableAccountingTypeFilter() AccountingFilter: Text;
    var
        ICISupportTimeAccType: Record "ICI Support Time Acc. Type";
    begin
        ICISupportTimeAccType.SetRange(Accounting, true);
        IF ICISupportTimeAccType.FINDSET() THEN
            REPEAT
                IF AccountingFilter <> '' then
                    AccountingFilter += '|';
                AccountingFilter += ICISupportTimeAccType.Code;
            UNTIL ICISupportTimeAccType.Next() = 0;
    end;

    procedure GetAccTypeFilterNotFreeLines() AccountingFilter: Text;
    var
        ICISupportTimeAccType: Record "ICI Support Time Acc. Type";
    begin
        ICISupportTimeAccType.SetRange(Accounting, true);
        ICISupportTimeAccType.SetFilter("Discount %", '<>%1', 100);
        IF ICISupportTimeAccType.FINDSET() THEN
            REPEAT
                IF AccountingFilter <> '' then
                    AccountingFilter += '|';
                AccountingFilter += ICISupportTimeAccType.Code;
            UNTIL ICISupportTimeAccType.Next() = 0;
    end;

    procedure GetAccTypeFilterFreeLines() AccountingFilter: Text;
    var
        ICISupportTimeAccType: Record "ICI Support Time Acc. Type";
    begin
        ICISupportTimeAccType.SetRange(Accounting, true);
        ICISupportTimeAccType.SetFilter("Discount %", '<>%1', 100);
        IF ICISupportTimeAccType.FINDSET() THEN
            REPEAT
                IF AccountingFilter <> '' then
                    AccountingFilter += '|';
                AccountingFilter += ICISupportTimeAccType.Code;
            UNTIL ICISupportTimeAccType.Next() = 0;
    end;
}
