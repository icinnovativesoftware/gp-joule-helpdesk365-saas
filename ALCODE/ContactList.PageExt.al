pageextension 56276 "ICI Contact List" extends "Contact List"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Contact Ticket Factbox"; "ICI Contact Ticket Factbox")
            {
                ApplicationArea = All;
                Visible = ShowContactFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdesk)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                action(CreateTicket)
                {
                    Caption = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ToolTip = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ApplicationArea = All;
                    Image = New;
                    trigger OnAction()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                    begin
                        ICISupportTicket.Init();
                        ICISupportTicket.Insert(true);
                        IF Rec.Type = Rec.Type::Company THEN
                            ICISupportTicket.Validate("Company Contact No.", Rec."Company No.")
                        ELSE
                            ICISupportTicket.Validate("Current Contact No.", Rec."No.");
                        ICISupportTicket.FillMissingContactOrCustomerInformation();
                        ICISupportTicket.Modify(true);

                        PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket);
                    end;
                }

                action(ICISendLogin)
                {
                    Caption = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                    ToolTip = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                    ApplicationArea = All;
                    Image = SendMail;
                    Visible = PortalIntegration;

                    trigger OnAction();
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                        LoginSentTxt: Label 'Logindata has been sent', Comment = 'de-DE=Zugangsdaten erfolgreich versendet.';
                        LoginNotSentTxt: Label 'Logindata has not been sent', Comment = 'de-DE=Zugangsdaten konnten nicht versendet werden.';
                    begin
                        CurrPage.SaveRecord();
                        IF ICISupportMailMgt.SendContactLogin(Rec."No.", Rec.ICIGeneratePassword()) THEN
                            MESSAGE(LoginSentTxt)
                        ELSE
                            MESSAGE(LoginNotSentTxt);
                        CurrPage.Update(false);
                    end;
                }
                action(ICIOpenPortal)
                {
                    Caption = 'Open Portal', Comment = 'de-DE=Zugang öffnen';
                    ToolTip = 'Open Portal', Comment = 'de-DE=Zugang öffnen';
                    ApplicationArea = All;
                    Image = LaunchWeb;
                    Visible = PortalIntegration;

                    trigger OnAction();
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        Hyperlink(ICISupportMailMgt.GenerateContactLink(Rec."No."));
                    end;
                }
                action(ICISentMails)
                {
                    Caption = 'Sent Mails', Comment = 'de-DE=Gesendete E-Mails';
                    ToolTip = 'Sent Mails', Comment = 'de-DE=Öffnet die gesendeten E-Mails zu diesem Kontakt';
                    ApplicationArea = All;
                    Image = Log;
                    RunObject = Page "Sent Emails";
                    trigger OnAction()
                    var
                        EMail: Codeunit Email;
                    begin
                        EMail.OpenSentEmails(Database::"Contact", Rec.SystemId);
                    end;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() THEN begin
                PortalIntegration := ICISupportSetup."Portal Integration";
                ShowContactFactbox := true;
            end;
    end;

    var
        PortalIntegration: Boolean;
        ShowContactFactbox: Boolean;
}
