page 56300 "ICI Support Process Stage List"
{

    Caption = 'ICI Support Process Stage List', Comment = 'de-DE=Prozessstufen';
    PageType = List;
    SourceTable = "ICI Support Process Stage";
    SourceTableView = sorting("Process Code", Stage);
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Process Code"; Rec."Process Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Process Code', Comment = 'de-DE=Prozesscode';
                }
                field(Stage; Rec.Stage)
                {
                    ApplicationArea = All;
                    ToolTip = 'Stage', Comment = 'de-DE=Stufe';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Reset processing time"; Rec."Reset processing time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Reset processing time', Comment = 'de-DE=Gibt an, ob die Bearbeitungszeit zurückgesetzt werden soll';
                }
            }
        }
    }
    trigger OnNewRecord(BelowxRec: Boolean)
    var
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        IF Rec.GetFilter("Process Code") <> '' then
            Rec."Process Code" := COPYSTR(Rec.GetFilter("Process Code"), 1, 20);

        IF Rec.GetFilter("Stage") <> '0' then
            IF Evaluate(Rec."Stage", Rec.GetFilter("Stage")) THEN;

        IF ICISupportProcessStage.GET(Rec."Process Code", Rec.Stage) then
            Rec."Ticket State" := ICISupportProcessStage."Ticket State";

    end;

}
