page 56380 "ICI Support User Search Option"
{

    Caption = 'ICI Support User Card', Comment = 'de-DE=Benutzer Suchoptionen';
    PageType = Card;
    SourceTable = "ICI Support User";
    UsageCategory = None;

    layout
    {
        area(content)
        {


            group("Options-Sales")
            {
                Caption = 'Sales', Comment = 'de-DE=Belegsuche - Verkauf';
                group(SalesQuote)
                {
                    Caption = 'Salesquotes', Comment = 'de-DE=Angebote';
                    field("Search S.Q. by Ext. Doc. No."; Rec."Search S.Q. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote by External Document No.', Comment = 'de-DE=Verkaufsangebote nach externer Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Quote by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.Q. by No."; Rec."Search S.Q. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote by No.', Comment = 'de-DE=Verkaufsangebote nach Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Quote by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                }
                group(SalesOrder)
                {
                    Caption = 'Salesorders', Comment = 'de-DE=Aufträge';
                    field("Search S.O. by Ext. Doc. No."; Rec."Search S.O. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by External Document No.', Comment = 'de-DE=Verkaufsaufträge nach externer Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Order by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.O. by No."; Rec."Search S.O. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by No.', Comment = 'de-DE=Verkaufsaufträge nach Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Order by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.O. by Quote No."; Rec."Search S.O. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by Quote No.', Comment = 'de-DE=Verkaufsaufträge nach Angebotsnr.';
                        Caption = 'Specifies the value of the Search Sales Order by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;
                    }
                }
                group(SalesQuoteArchive)
                {
                    Caption = 'Sales Quote Archive', Comment = 'de-DE=VK-Angebotsarchiv';
                    field("Search S.Q.A. by Ext. Doc. No."; Rec."Search S.Q.A. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote Archive by External Document No.', Comment = 'de-DE=Verkaufsangebote nach externer Belegnr.';
                        Caption = 'External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.Q.A. by No."; Rec."Search S.Q.A. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote Archove by No.', Comment = 'de-DE=Verkaufsangebote nach Belegnr.';
                        Caption = 'No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                }

                group(SalesOrderArchive)
                {
                    Caption = 'Sales Order Archive', Comment = 'de-DE=VK-Auftragsarchiv';
                    field("Search S.O.A. by Ext. Doc. No."; Rec."Search S.O.A. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by External Document No.', Comment = 'de-DE=Verkaufsaufträge nach externer Belegnr.';
                        Caption = 'External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.O.A. by No."; Rec."Search S.O.A. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by No.', Comment = 'de-DE=Verkaufsaufträge nach Belegnr.';
                        Caption = 'No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.O.An by Quote No."; Rec."Search S.O.A. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by Quote No.', Comment = 'de-DE=Verkaufsaufträge nach Angebotsnr.';
                        Caption = 'Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;
                    }
                }
                group(PstSalesInv)
                {
                    Caption = 'Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    field("Search S.I. by Ext. Doc. No."; Rec."Search S.I. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by External Document No.', Comment = 'de-DE=Geb. Rechnungen nach externer Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.I. by No."; Rec."Search S.I. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by No.', Comment = 'de-DE=Geb. Rechnungen nach Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.I. by Order No."; Rec."Search S.I. by Order No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by Order No.', Comment = 'de-DE=Geb. Rechnungen nach Auftragsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by Order No.', Comment = 'de-DE=Auftragsnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.I. by Quote No."; Rec."Search S.I. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by Quote No.', Comment = 'de-DE=Geb. Rechnungen nach Angebotsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;
                    }

                }
                group(PstSalesShip)
                {
                    Caption = 'Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferungen';
                    field("Search S.S. by Ext. Doc. No."; Rec."Search S.S. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by External Document No.', Comment = 'de-DE=Geb. Lieferungen nach externer Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.S. by No."; Rec."Search S.S. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by No.', Comment = 'de-DE=Geb. Lieferungen nach Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.S. by Order No."; Rec."Search S.S. by Order No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by Order No.', Comment = 'de-DE=Geb. Lieferungen nach Auftragsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by Order No.', Comment = 'de-DE=Auftragsnr.';
                        ApplicationArea = All;
                    }
                    field("Search S.S. by Quote No."; Rec."Search S.S. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by Quote No.', Comment = 'de-DE=Geb. Lieferungen nach Angebotsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;
                    }
                }
            }
            group("Options-Service")
            {
                Caption = 'Service', Comment = 'de-DE=Belegsuche - Service';
                group("Service-Quote")
                {
                    Caption = 'Servicequotes', Comment = 'de-DE=Angebote';
                    field("Search Serv. Q. by No."; Rec."Search Serv. Q. by No.")
                    {
                        ToolTip = 'Search Service Quote by No.', Comment = 'de-DE=Serviceangebote nach Belegnr.';
                        Caption = 'Search Service Quote by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                }
                group("Service-Order")
                {
                    Caption = 'Serviceorders', Comment = 'de-DE=Aufträge';
                    field("Search Serv. O. by No."; Rec."Search Serv. O. by No.")
                    {
                        ToolTip = 'Search Service Order by No.', Comment = 'de-DE=Serviceaufträge nach Belegnr.';
                        Caption = 'Search Service Order by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                    field("Search Serv. O. by Quote No."; Rec."Search Serv. O. by Quote No.")
                    {
                        ToolTip = 'Search Service Order by Quote No.', Comment = 'de-DE=Serviceaufträge nach Angebotsnr.';
                        Caption = 'Search Service Order by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;
                    }
                }
                group("PstServiceInv")
                {
                    Caption = 'Posted Service Invoices', Comment = 'de-DE=Geb. Rechnungen';
                    field("Search Serv. I. by No."; Rec."Search Serv. I. by No.")
                    {
                        ToolTip = 'Search Service Invoice by No.', Comment = 'de-DE=Geb. Servicerechnung nach Belegnr.';
                        Caption = 'Search Service Invoice by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                }
                group("PstServiceShipments")
                {
                    Caption = 'Posted Service Shipments', Comment = 'de-DE=Geb. Lieferungen';
                    field("Search Serv. S. by No."; Rec."Search Serv. S. by No.")
                    {
                        ToolTip = 'Search Service Shipment by No.', Comment = 'de-DE=Geb. Servicelieferung nach Belegnr.';
                        Caption = 'Search Service Shipment by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;
                    }
                }
                group("Servicecontractquotes")
                {
                    Caption = 'Servicecontractquotes', Comment = 'de-DE=Vertragsangebote';
                    field("Search Serv. C. Q. by No."; Rec."Search Serv. C. Q. by No.")
                    {
                        ToolTip = 'Search Service Contract Quote by No.', Comment = 'de-DE=Servicevertragsangebot nach Vertragsnr.';
                        Caption = 'Search Service Contract Quote by No.', Comment = 'de-DE=Servicevertragsangebot nach Vertragsnr.';
                        ApplicationArea = All;
                    }
                }
                group("Servicecontracts")
                {
                    Caption = 'Servicecontracts', Comment = 'de-DE=Verträge';
                    field("Search Serv. C. by No."; Rec."Search Serv. C. by No.")
                    {
                        ToolTip = 'Search Service Contract by No.', Comment = 'de-DE=Servicevertrag nach Vertragsnr.';
                        Caption = 'Search Service Contract by No.', Comment = 'de-DE=Vertragsnr.';
                        ApplicationArea = All;
                    }
                }
            }
            group(Service)
            {
                Caption = 'Service Item', Comment = 'de-DE=Seriennummernsuche';

                field("Search Se. It. by No."; Rec."Search Se. It. by No.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by No.', Comment = 'de-DE=Serviceartikel nach Nummer';
                    Caption = 'Specifies the value of the Search Service Items by No.', Comment = 'de-DE=Serviceartikelnr.';
                    ApplicationArea = All;
                }
                field("Search Se. It. by Serial No."; Rec."Search Se. It. by Serial No.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Serviceartikel nach Seriennr.';
                    Caption = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Seriennummer';
                    ApplicationArea = All;
                }
                field("Search Se. It. by Item No."; Rec."Search Se. It. by Item No.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Item No.', Comment = 'de-DE=Serviceartikel nach Artikelnr.';
                    Caption = 'Specifies the value of the Search Service Items by Item No.', Comment = 'de-DE=Artkelnr.';
                    ApplicationArea = All;
                }
                field("Search Se. It. by Search Desc."; Rec."Search Se. It. by Search Desc.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Search Desc.', Comment = 'de-DE=Serviceartikel nach Suchbegriff';
                    Caption = 'Specifies the value of the Search Service Items by Search Desc.', Comment = 'de-DE=Suchbegriff';
                    ApplicationArea = All;
                }
                field("Search Se. It. by No. 2"; Rec."Search Se. It. by No. 2")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Serviceartikel nach Nr. 2';
                    Caption = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Nummer 2';
                    ApplicationArea = All;
                }
            }

            group("Address Search")
            {
                Caption = 'Address Search', Comment = 'de-DE=Adressensuche';

                field("Search Cust. by Addr."; Rec."Search Cust. by Addr.")
                {
                    ToolTip = 'Specifies the value of the Search Customer by Address', Comment = 'de-DE=Debitor nach Adresse';
                    Caption = 'Specifies the value of the Search Customer by Address', Comment = 'de-DE=Debitor';
                    ApplicationArea = All;
                }
                field("Search Contacts by Addr."; Rec."Search Contacts by Addr.")
                {
                    ToolTip = 'Specifies the value of the Search Contacts by Address', Comment = 'de-DE=Kontakte nach Adresse';
                    Caption = 'Specifies the value of the Search Contacts by Address', Comment = 'de-DE=Kontakte';
                    ApplicationArea = All;
                }
                field("Search ServiceItems by Addr."; Rec."Search ServiceItems by Addr.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikel nach Adresse';
                    Caption = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikel nach Addr.';
                    ApplicationArea = All;
                }
                field("Search SI by Ship-To Addr."; Rec."Search SI by Ship-To Addr.")
                {
                    ToolTip = 'Specifies the value of the Search Service Items by Ship To Address', Comment = 'de-DE=Serviceartikel nach Lieferadresse';
                    Caption = 'Specifies the value of the Search Service Items by Ship To Address', Comment = 'de-DE=Serviceartikel nach Lief.-Addr.';
                    ApplicationArea = All;
                }
            }
        }
    }
}

