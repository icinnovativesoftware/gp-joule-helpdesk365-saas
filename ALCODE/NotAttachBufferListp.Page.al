page 56368 "ICI Not. Attach. Buffer Listp"
{

    Caption = 'Files', Comment = 'de-DE=Anhänge';
    PageType = List;
    SourceTable = "ICI Not. Attachment Buffer";
    SourceTableTemporary = true;
    ModifyAllowed = false;
    InsertAllowed = false;
    DeleteAllowed = true;
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Name; Rec.Name)
                {
                    ToolTip = 'Specifies the value of the Name field.', Comment = 'de-DE=Gibt den Dateinamen des Anhangs an';
                    ApplicationArea = All;
                    trigger OnAssistEdit()
                    var
                        Base64Convert: Codeunit "Base64 Convert";
                        TempBLOB: Codeunit "Temp Blob";
                        AttachmentContentB64: Text;
                        lInstream: InStream;
                        lInstream2: InStream;
                        AttachmentContent: Text;
                        lOutStream: OutStream;
                    begin
                        // Download File
                        Rec.CalcFields(Base64);
                        Rec.Base64.CreateInStream(lInstream);

                        lInStream.Read(AttachmentContentB64);
                        AttachmentContent := Base64Convert.FromBase64(AttachmentContentB64, TextEncoding::Windows);

                        TempBLOB.CreateOutStream(lOutStream, TextEncoding::Windows);
                        lOutStream.Write(AttachmentContent);
                        TempBLOB.CreateInStream(lInstream2);

                        //CopyStream(lOutStream, lInstream2);

                        DownloadFromStream(
                            lInStream2,  // InStream to save
                            '',   // Not used in cloud
                            '',   // Not used in cloud
                            '',   // Not used in cloud
                            Rec.Name); // Filename is browser download folder
                    end;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(AddAttachmentToList)
            {
                Image = AddContacts;
                ApplicationArea = All;
                Caption = 'Adds an Attachment', Comment = 'de-DE=Anhang hinzufügen';
                ToolTip = 'Adds an Attachment', Comment = 'de-DE=Anhang hinzufügen';
                Promoted = true;

                trigger OnAction()
                var
                    Base64Convert: Codeunit "Base64 Convert";
                    ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
                    TempBlob: Codeunit "Temp Blob";
                    AttachmentName: Text;
                    AttachmentB64Text: Text;
                    lInstream: InStream;
                    lInStream2: InStream;
                    lOutStream: OutStream;
                    lOutStream2: OutStream;
                    DialogTitleLbl: Label 'Upload File', Comment = 'de-DE=Datei hochladen';
                begin
                    UploadIntoStream(DialogTitleLbl, '', 'All Files (*.*)|*.*', AttachmentName, lInstream);

                    TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
                    Copystream(lOutStream, lInstream);
                    TempBlob.CreateInStream(lInStream2);

                    if AttachmentName = '' THEN
                        Error('');

                    Rec.Init();
                    Rec.Validate("Entry No.", Rec.COUNT() + 1);
                    Rec.Name := AttachmentName;
                    AttachmentB64Text := Base64Convert.ToBase64(lInstream2);
                    Rec."Content Type" := ICISupDragboxMgt.GetFileContentType(ICISupDragboxMgt.GetFileExtension(AttachmentName));

                    Rec.Base64.CreateOutStream(lOutStream2, TextEncoding::Windows);
                    lOutStream2.Write(AttachmentB64Text);

                    Rec.Insert(true);

                    ICINotificationDialogMgt.AddAttachment(Rec.Name, '', AttachmentB64Text);

                    CurrPage.Update(false);
                end;
            }
        }
    }
    trigger OnDeleteRecord(): Boolean
    begin
        ICINotificationDialogMgt.DeleteAttachment(Rec);
        EXIT(true);
    end;

    procedure InitAttachments(Attachments: JsonArray)
    var
        NoOfAttachments: Integer;
        Counter: Integer;
        JTok: JsonToken;
    begin
        NoOfAttachments := Attachments.Count();
        For Counter := 0 to (NoOfAttachments - 1) do // JSON Arrays start at 0
            IF Attachments.GET(Counter, JTok) THEN
                AddAttachment(JTok.AsObject());

        CurrPage.Update(false);
    end;

    procedure AddAttachment(Attachment: JsonObject)
    var
        jTok: JsonToken;
        lOutStream: OutStream;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", Rec.COUNT() + 1);

        IF Attachment.Get('AttachmentName', jTok) then
            Rec.Name := jTok.AsValue().AsText();
        IF Attachment.Get('ContentType', jTok) then
            Rec."Content Type" := jTok.AsValue().AsText();
        IF Attachment.Get('AttachmentBase64', jTok) then begin
            Rec.Base64.CreateOutStream(lOutStream, TextEncoding::Windows);
            lOutStream.Write(jTok.AsValue().AsText());
        end;

        Rec.Insert(true);
    end;

    procedure SetNotificationDialog(var pICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    var
        Values: JsonObject;
        jTok: JsonToken;
    begin
        ICINotificationDialogMgt := pICINotificationDialogMgt;
        Values := ICINotificationDialogMgt.CreateNotificationValues();

        IF Values.GET('Attachments', JTok) then
            InitAttachments(JTok.AsArray());
    end;

    var
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
}
