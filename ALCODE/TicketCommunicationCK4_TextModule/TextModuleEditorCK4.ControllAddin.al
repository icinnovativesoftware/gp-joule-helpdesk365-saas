controladdin "ICI Text Module Editor CK4"
{
    Scripts = 'ALCODE/TicketCommunicationCK4_TextModule/Scripts/MainScript.js', 'https://licmgt.ic-innovative.de/Helpdesk365/CKEditor_TextModule/ckeditor.js', 'https://code.jquery.com/jquery-2.1.0.min.js';
    //Scripts = 'ALCODE\TicketCommunicationCK4\Scripts\MainScript.js', 'https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js', 'https://code.jquery.com/jquery-2.1.0.min.js';
    //Scripts = 'ALCODE\TicketCommunication\Scripts\MainScript.js', 'ALCODE\TicketCommunication\Scripts\ckeditor.js.map', 'ALCODE\TicketCommunication\Scripts\ckeditor.js', 'https://code.jquery.com/jquery-2.1.0.min.js';
    StartupScript = 'ALCODE/TicketCommunicationCK4/Scripts/startupScript.js';
    StyleSheets = 'https://licmgt.ic-innovative.de/Helpdesk365/MW.css', 'ALCODE/TicketCommunicationCK4/Style/Chat.css';
    VerticalStretch = true;
    HorizontalStretch = true;
    MinimumHeight = 400;
    RequestedHeight = 400;
    // JS -> AL
    event ControlReady();
    event SaveRequested(Data: JsonObject);
    event ContentChanged();
    event OnAfterInit();
    event DownloadFile(Data: JsonObject);
    // AL -> JS
    procedure Init(Data: JsonObject);
    procedure LoadChat(data: JsonArray);
    procedure LoadTicketTextModule(data: JsonArray);
    procedure Load(data: Text);
    procedure RequestSave();
    procedure SetReadOnly(readonly: boolean);
    procedure SendFileToClient(Data: JsonObject);
}