var InputArea
var Editor
var isFullScreen = false;
function Init(data){
    
    var div = document.getElementById("controlAddIn");
    if(data.FullScreen){
        isFullScreen = true;
        div.classList = "FullScreen";
        // code is from: https://github.com/ajkauffmann/ControlAddInSizeDemo/blob/master/ControlAddIn/Startup.js
        // ParentElement.Parentelement is now necessary since another wrapper was added
        var iframe = window.frameElement;
        iframe.parentElement.parentElement.style.display = 'flex';
        iframe.parentElement.parentElement.style.flexDirection = 'column';
        iframe.parentElement.parentElement.style.flexGrow = '1';
        // $(iframe.parentElement).closest('.ms-nav-layout-main .ms-nav-scrollable').css("padding-right","0px");
        $(iframe.parentElement).closest('.ms-nav-layout-main').css('padding-right','0px')
        iframe.style.removeProperty('height');
        iframe.style.removeProperty('min-height');
        iframe.style.removeProperty('max-height');
        iframe.style.flexGrow = '1';
        iframe.style.flexShrink = '1';
        iframe.style.flexBasis = 'auto';
        iframe.style.paddingBottom = '42px';
    }
    // // Chat Input Editor
    InputArea = document.createElement("textarea");
    InputArea.id = "Comment";
    InputArea.name = "Comment";
    div.appendChild(InputArea);    
    CKEDITOR.replace( 'Comment', {
        /*customConfig: '',
        customConfig: false, //no config.js
        stylesSet: false, //no styles.js
        defaultLanguage: 'de', //default language
        language: 'de', //ui language*/
    });
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("OnAfterInit",[]);
}
// Chat Editor
function Load(data) {
    CKEDITOR.instances.Comment.setData(data);
}
function RequestSave() {
    // debugger;
    var editorData = CKEDITOR.instances.Comment.getData();
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("SaveRequested",[{Text: editorData}]);
}