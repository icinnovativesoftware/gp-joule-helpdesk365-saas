pageextension 56294 "ICI Sales Order" extends "Sales Order"
{
    layout
    {
        addlast(content)
        {
            group(Helpdesk365)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                }

                field("ICI Hidden from Webarchive"; Rec."ICI Hidden from Webarchive")
                {
                    Visible = ShowWebarchiv;
                    ApplicationArea = All;
                    ToolTip = 'Activate this Option, if you dont want this Document to be shown in the Webarchive', Comment = 'de-DE=Aktivieren Sie diese Option, um diesen Beleg im Belegarchiv auszublenden';
                }
                field("ICI Ticket Process"; Rec."ICI Ticket Process")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process', Comment = 'de-DE=Ticketprozess';
                }
                field("ICI Ticket Process Stage"; Rec."ICI Ticket Process Stage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process Stage', Comment = 'de-DE=Ticketprozessstufe';
                }
                // field("ICI Ticket Process Stage Desc."; "ICI Ticket Process Stage Desc.")
                // {
                //     ApplicationArea = All;
                //     ToolTip = 'Ticket Processstage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
                // }
            }
        }
        addfirst(factboxes)
        {
            part("ICI Sales Order Factbox"; "ICI Sales Order Factbox")
            {
                ApplicationArea = All;
                Visible = ShowSalesFactbox;
                SubPageLink = "No." = field("No."), "Document Type" = Field("Document Type");
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';

                action(AddPDFToTicket)
                {
                    Caption = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ToolTip = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ApplicationArea = All;
                    Image = SendEmailPDF;
                    Visible = Rec."ICI Support Ticket No." <> '';
                    trigger OnAction()
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        Rec.TestField("ICI Support Ticket No.");
                        Rec.SetRecFilter();
                        ICISupportMailMgt.AttachSalesHeaderToTicket(Rec);
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() then begin
                ShowSalesFactbox := ICISupportSetup."Document Integration";
                IF ICIWebarchiveSetup.ReadPermission THEN
                    IF ICIWebarchiveSetup.GET() then
                        ShowWebarchiv := ICISupportSetup."Portal Integration" AND ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Order";
            end;
    end;

    var
        ShowSalesFactbox: Boolean;
        ShowWebarchiv: Boolean;
}
