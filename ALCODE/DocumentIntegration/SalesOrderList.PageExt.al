pageextension 56295 "ICI Sales Order List" extends "Sales Order List"
{

    layout
    {
        addlast(Control1)
        {
            field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
            {
                ApplicationArea = All;
                ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';

                trigger OnDrillDown()
                var
                    SupportTicket: Record "ICI Support Ticket";
                begin
                    IF SupportTicket.GET(Rec."ICI Support Ticket No.") then
                        PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
                end;
            }
        }
        addfirst(factboxes)
        {
            part("ICI Sales Order Factbox"; "ICI Sales Order Factbox")
            {
                ApplicationArea = All;
                Visible = ShowSalesFactbox;
                SubPageLink = "No." = field("No."), "Document Type" = Field("Document Type");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() then
                ShowSalesFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowSalesFactbox: Boolean;
}
