pageextension 56289 "ICI Service Contract Quotes" extends "Service Contract Quotes"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Service Con. Quote Factbox"; "ICI Service Con. Quote Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "Contract No." = field("Contract No."), "Contract Type" = field("Contract Type");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                ShowServiceFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowServiceFactbox: Boolean;
}
