pageextension 56287 "ICI Service Contracts" extends "Service Contracts"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Service Contract Factbox"; "ICI Service Contract Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "Contract No." = field("Contract No."), "Contract Type" = field("Contract Type");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                ShowServiceFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowServiceFactbox: Boolean;
}
