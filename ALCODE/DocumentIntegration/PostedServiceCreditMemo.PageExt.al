pageextension 56306 "ICI Posted Service Credit Memo" extends "Posted Service Credit Memo"
{
    layout
    {
        addlast(content)
        {
            group(ICIHelpdesk365)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                }
                field("ICI Hidden from Webarchive"; Rec."ICI Hidden from Webarchive")
                {
                    Visible = ShowWebarchiv;
                    ApplicationArea = All;
                    ToolTip = 'Activate this Option, if you dont want this Document to be shown in the Webarchive', Comment = 'de-DE=Aktivieren Sie diese Option, um diesen Beleg im Belegarchiv auszublenden';
                }
                field("ICI Ticket Process"; Rec."ICI Ticket Process")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process', Comment = 'de-DE=Ticketprozess';
                }
                field("ICI Ticket Process Stage"; Rec."ICI Ticket Process Stage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process Stage', Comment = 'de-DE=Ticketprozessstufe';
                }
                // field("ICI Ticket Process Stage Desc."; "ICI Ticket Process Stage Desc.")
                // {
                //     ApplicationArea = All;
                //     ToolTip = 'Ticket Processstage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
                // }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                action(AddPDFToTicket)
                {
                    Caption = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ToolTip = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ApplicationArea = All;
                    Image = SendEmailPDF;
                    Visible = Rec."ICI Support Ticket No." <> '';
                    trigger OnAction()
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        Rec.TestField("ICI Support Ticket No.");
                        Rec.SetRecFilter();
                        ICISupportMailMgt.AttachServiceCrMemoHeaderToTicket(Rec);
                    end;
                }

                action(ShowInWebarchive)
                {
                    Caption = 'Show In Webarchive', Comment = 'de-DE=Im Belegarchiv anzeigen';
                    ToolTip = 'Show In Webarchive', Comment = 'de-DE=Im Belegarchiv anzeigen';
                    ApplicationArea = All;
                    Image = CreateDocument;
                    Visible = ShowWebarchiv AND Rec."ICI Hidden from Webarchive";

                    trigger OnAction();
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        ICISupportDocumentMgt.SetHiddenFromWebarchiveServiceCrMemo(Rec, False);
                        CurrPage.Update(false);
                    end;
                }
                action(HideInWebarchive)
                {
                    Caption = 'Hide In Webarchive', Comment = 'de-DE=Im Belegarchiv ausblenden';
                    ToolTip = 'Hide In Webarchive', Comment = 'de-DE=Im Belegarchiv ausblenden';
                    ApplicationArea = All;
                    Image = CloseDocument;
                    Visible = ShowWebarchiv AND NOT Rec."ICI Hidden from Webarchive";

                    trigger OnAction();
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        ICISupportDocumentMgt.SetHiddenFromWebarchiveServiceCrMemo(Rec, True);
                        CurrPage.Update(false);
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                IF ICIWebarchiveSetup.ReadPermission THEN
                    IF ICIWebarchiveSetup.GET() then
                        ShowWebarchiv := ICISupportSetup."Portal Integration" AND ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Posted Service Cr.Memo";
    end;

    var

        ShowWebarchiv: Boolean;
}
