codeunit 56279 "ICI Support Document Mgt."
{
    Permissions = tabledata "Sales Shipment Header" = M, tabledata "Sales Invoice Header" = M, tabledata "Sales Cr.Memo Header" = M, tabledata "Service Shipment Header" = M, tabledata "Service Invoice Header" = M, tabledata "Service Cr.Memo Header" = M;
    // SALES ----------------
    EventSubscriberInstance = StaticAutomatic;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"Sales-Post", 'OnAfterSalesCrMemoHeaderInsert', '', true, true)]
    local procedure OnAfterSalesCrMemoHeaderInsert(CommitIsSuppressed: Boolean; SalesHeader: Record "Sales Header"; var SalesCrMemoHeader: Record "Sales Cr.Memo Header");
    begin
        SalesCrMemoHeader.Validate("ICI Support Ticket No.", SalesHeader."ICI Support Ticket No.");
    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"Sales-Post", 'OnAfterSalesInvHeaderInsert', '', true, true)]
    local procedure OnAfterSalesInvHeaderInsert(CommitIsSuppressed: Boolean; SalesHeader: Record "Sales Header"; var SalesInvHeader: Record "Sales Invoice Header");
    begin
        SalesInvHeader.Validate("ICI Support Ticket No.", SalesHeader."ICI Support Ticket No.");
    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"Sales-Post", 'OnAfterSalesShptHeaderInsert', '', true, true)]
    local procedure OnAfterSalesShptHeaderInsert(SalesHeader: Record "Sales Header"; SuppressCommit: Boolean; var SalesShipmentHeader: Record "Sales Shipment Header");
    begin
        SalesShipmentHeader.Validate("ICI Support Ticket No.", SalesHeader."ICI Support Ticket No.");
    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"Sales-Quote to Order", 'OnAfterOnRun', '', true, true)]
    local procedure SalesQuoteToOrderOnAfterOnRun(var SalesHeader: Record "Sales Header"; var SalesOrderHeader: Record "Sales Header");
    begin
        SalesOrderHeader.Validate("ICI Support Ticket No.", SalesHeader."ICI Support Ticket No.");
    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"Sales-Quote to Invoice", 'OnAfterOnRun', '', true, true)]
    local procedure SalesQuoteToInvoiceOnAfterOnRun(var SalesHeader: Record "Sales Header"; var SalesInvoiceHeader: Record "Sales Header");
    begin
        SalesInvoiceHeader.Validate("ICI Support Ticket No.", SalesHeader."ICI Support Ticket No.");
    end;

    procedure CreateSalesDocument(ICISupportTicket: Record "ICI Support Ticket"; DocumentType: Enum "Sales Document Type"; SalesHeaderDate: Date);
    VAR
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        ICISupportTicketItem: Record "ICI Support Ticket Item";
        IsHandled: Boolean;
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
        TicketCreatedTxt: Label 'Ticket Created by %1', Comment = 'de-DE=Ticket erstellt von %1';
        TicketProccedTxt: Label 'Ticket processed by %1', Comment = 'de-DE=Ticket bearbeitet von %1';
        TicketShippedTxt: Label 'Delevered on %1 per Remote', Comment = 'de-DE=Geliefert am %1 per Fernwartung';
        OpDocTxt: Label 'Open new Order %1?', Comment = 'de-DE=Möchten Sie %1 %2 öffnen?';
    BEGIN
        // VK-Beleg aus Ticket erstellen
        ICISupportMailSetup.GET();
        ICISupportTicket.CALCFIELDS("Support User Name");

        InsertSalesHeader(ICISupportTicket, SalesHeader, DocumentType, SalesHeaderDate);

        // HeaderLines
        OnBeforeSalesLineHeader(IsHandled, ICISupportTicket, SalesHeader, SalesLine);
        if not IsHandled then begin
            InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(TicketRefTxt, ICISupportTicket."No."));
            InsertTextSalesLine(SalesHeader, SalesLine, ICISupportTicket.Description);
            InsertTextSalesLine(SalesHeader, SalesLine, '');
        end;
        OnAfterSalesLineHeader(ICISupportTicket, SalesHeader, SalesLine);

        //InsertLines
        CASE DocumentType OF
            DocumentType::Quote:
                ;
            DocumentType::Order:
                InsertSalesPayoffLines(ICISupportTicket, SalesHeader);
            DocumentType::Invoice:
                InsertSalesPayoffLines(ICISupportTicket, SalesHeader);
            DocumentType::"Return Order":
                InsertSalesReturnLines(ICISupportTicket, SalesHeader);
        END;

        //Insert TicketItems
        ICISupportTicketItem.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketItem.SETRANGE("Use in Sales Doc.", TRUE);
        IF ICISupportTicketItem.FINDSET() THEN
            REPEAT
                CASE ICISupportTicketItem.Type OF
                    ICISupportTicketItem.Type::Item:
                        BEGIN
                            InsertSalesLine(
                              SalesHeader, SalesLine
                              , "Sales Line Type"::Item, ICISupportTicketItem."No.", ICISupportTicketItem."Item Variant Code", ICISupportTicketItem.Quantity, 0, ICISupportTicketItem.Description, 0, '', 0, '');
                            IF SalesLine."Unit of Measure Code" <> ICISupportTicketItem."Unit of Measure Code" THEN BEGIN
                                SalesLine.VALIDATE("Unit of Measure Code", ICISupportTicketItem."Unit of Measure Code");
                                SalesLine.MODIFY();
                            END;
                            OnAfterInsertSalesLineWithItem(SalesHeader, SalesLine, ICISupportTicketItem);
                        END;
                    ICISupportTicketItem.Type::Resource:
                        InsertSalesLine(
                          SalesHeader, SalesLine
                          , "Sales Line Type"::Resource, ICISupportTicketItem."No.", '', ICISupportTicketItem.Quantity, 0, ICISupportTicketItem.Description, 0, '', 0, ICISupportTicketItem."Work Type Code");
                    ICISupportTicketItem.Type::ServiceItem:
                        //Not Allowed?
                        // XXX TODO
                        Error(NotInLicenseErr);

                END;
            UNTIL ICISupportTicketItem.NEXT() = 0;


        //FooterLines
        IsHandled := false;
        OnBeforeSalesLineFooter(IsHandled, ICISupportTicket, SalesHeader, SalesLine);
        if not IsHandled then
            IF DocumentType IN [DocumentType::Order, DocumentType::Invoice] THEN BEGIN
                InsertTextSalesLine(SalesHeader, SalesLine, '');
                InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(TicketCreatedTxt, ICISupportTicket."Curr. Contact Name"));
                InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(TicketProccedTxt, ICISupportTicket."Support User Name"));
                InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(TicketShippedTxt, WORKDATE()));
            END;
        OnAfterSalesLineFooter(ICISupportTicket, SalesHeader, SalesLine);

        //Open Document
        IF GUIALLOWED THEN
            IF CONFIRM(OpDocTxt, TRUE, SalesHeader."Document Type", SalesHeader."No.") THEN BEGIN
                SalesHeader.SETRECFILTER();
                CASE DocumentType OF
                    DocumentType::Quote:
                        PAGE.RUN(PAGE::"Sales Quote", SalesHeader);
                    DocumentType::Order:
                        PAGE.RUN(PAGE::"Sales Order", SalesHeader);
                    DocumentType::Invoice:
                        PAGE.RUN(PAGE::"Sales Invoice", SalesHeader);
                    DocumentType::"Return Order":
                        PAGE.RUN(PAGE::"Sales Return Order", SalesHeader);
                    DocumentType::"Credit Memo":
                        PAGE.RUN(PAGE::"Sales Credit Memo", SalesHeader);
                END;
            END;
    END;

    LOCAL procedure InsertSalesHeader(VAR ICISupportTicket: Record "ICI Support Ticket"; VAR SalesHeader: Record "Sales Header"; DocumentType: Enum "Sales Document Type"; SalesHeaderDate: Date);
    VAR
        Customer: Record "Customer";
        Contact: Record "Contact";
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportUser: Record "ICI Support User";
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
    BEGIN

        ICISupportSetup.GET();
        Contact.GET(ICISupportTicket."Company Contact No.");

        IF DocumentType <> DocumentType::Quote THEN
            IF ICISupportTicket."Customer No." = '' THEN
                FindOrAddCustomer(ICISupportTicket);


        IF ICISupportTicket."Customer No." <> '' THEN
            Customer.GET(ICISupportTicket."Customer No.")
        ELSE
            CLEAR(ICISupportTicket."Customer No.");

        SalesHeader.INIT();
        SalesHeader.SetHideValidationDialog(TRUE);
        SalesHeader."No." := '';

        SalesHeader.Validate("Document Type", DocumentType);

        // CASE DocumentType OF
        //     DocumentType::Quote:
        //         SalesHeader."Document Type" := SalesHeader."Document Type"::Quote;
        //     DocumentType::Order:
        //         SalesHeader."Document Type" := SalesHeader."Document Type"::Order;
        //     DocumentType::Invoice:
        //         SalesHeader."Document Type" := SalesHeader."Document Type"::Invoice;
        //     DocumentType::"Return Order":
        //         SalesHeader."Document Type" := SalesHeader."Document Type"::"Return Order";
        // END;

        SalesHeader.VALIDATE("Posting Date", SalesHeaderDate);
        SalesHeader.VALIDATE("Document Date", SalesHeaderDate);
        SalesHeader.INSERT(TRUE);

        SalesHeader.VALIDATE("Your Reference", STRSUBSTNO(TicketRefTxt, ICISupportTicket."No."));
        IF (DocumentType = DocumentType::Quote) AND (ICISupportTicket."Customer No." = '') THEN
            SalesHeader.VALIDATE("Sell-to Customer Templ. Code", ICISupportSetup."Customer Template Code")
        ELSE
            SalesHeader.VALIDATE("Sell-to Customer No.", Customer."No.");

        IF ICISupportTicket."Current Contact No." = '' THEN
            SalesHeader.VALIDATE("Sell-to Contact No.", ICISupportTicket."Company Contact No.")
        ELSE
            SalesHeader.VALIDATE("Sell-to Contact No.", ICISupportTicket."Current Contact No.");

        SalesHeader.VALIDATE("Order Date", SalesHeaderDate);
        SalesHeader.VALIDATE("Shipment Date", SalesHeaderDate);

        ICISupportUser.GET(ICISupportTicket."Support User ID");
        SalesHeader.VALIDATE("Salesperson Code", ICISupportUser."Salesperson Code");
        SalesHeader.VALIDATE("ICI Support Ticket No.", ICISupportTicket."No.");

        IF ICISupportTicket."Ship-to Code" <> '' THEN
            SalesHeader.VALIDATE("Ship-to Code", ICISupportTicket."Ship-to Code");

        IF ((ICISupportTicket."Ship-to Name" <> '') OR (ICISupportTicket."Ship-to Address" <> '') OR (ICISupportTicket."Ship-to City" <> '') OR (ICISupportTicket."Ship-to Post Code" <> '')) THEN BEGIN
            SalesHeader.VALIDATE("Ship-to Name", ICISupportTicket."Ship-to Name");
            SalesHeader.VALIDATE("Ship-to Name 2", ICISupportTicket."Ship-to Name 2");
            SalesHeader.VALIDATE("Ship-to Address", ICISupportTicket."Ship-to Address");
            SalesHeader.VALIDATE("Ship-to Address 2", ICISupportTicket."Ship-to Address 2");
            SalesHeader.VALIDATE("Ship-to City", ICISupportTicket."Ship-to City");
            SalesHeader.VALIDATE("Ship-to Post Code", ICISupportTicket."Ship-to Post Code");
            SalesHeader.VALIDATE("Ship-to Contact", ICISupportTicket."Ship-to Contact");
            SalesHeader.VALIDATE("Ship-to County", ICISupportTicket."Ship-to County");
            SalesHeader.VALIDATE("Ship-to Country/Region Code", ICISupportTicket."Ship-to Country/Region Code");
        END;

        IF ICISupportTicket."Shipment Method Code" <> '' THEN
            SalesHeader.VALIDATE("Shipment Method Code", ICISupportTicket."Shipment Method Code");
        IF ICISupportTicket."Shipping Agent Code" <> '' THEN BEGIN
            SalesHeader.VALIDATE("Shipping Agent Code", ICISupportTicket."Shipping Agent Code");
            SalesHeader.VALIDATE("Shipping Agent Service Code", ICISupportTicket."Shipping Agent Service Code");
        END;

        IF ICISupportTicket."Location Code" <> '' THEN
            SalesHeader.VALIDATE("Location Code", ICISupportTicket."Location Code");

        SalesHeader.MODIFY(TRUE);


    END;

    PROCEDURE InsertTextSalesLine(VAR SalesHeader: Record "Sales Header"; VAR SalesLine: Record "Sales Line"; Description: Text);
    BEGIN
        InsertSalesLine(SalesHeader, SalesLine, "Sales Line Type"::" ", '', '', 0, 0, Description, 1, '', 0, '');
    END;

    PROCEDURE InsertSalesLine(VAR SalesHeader: Record "Sales Header"; VAR SalesLine: Record "Sales Line"; pSalesLineType: Enum "Sales Line Type"; No: Code[20]; VariantCode: Code[10]; Quantity: Decimal; UnitPrice: Decimal; Description: Text; PrintMarks: Option " ",Fettdruck,Zwischensumme,Seitenwechsel,Unsichtbar; Position: Code[10]; LineDiscount: Decimal; WorkTypeCode: Code[20]);
    VAR
        LastSalesLine: Record "Sales Line";
        NewLineNo: Integer;
    BEGIN
        // Einfügen einer Verkaufszeile

        Description := COPYSTR(Description, 1, MAXSTRLEN(SalesLine.Description));

        LastSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
        LastSalesLine.SETRANGE("Document No.", SalesHeader."No.");
        IF LastSalesLine.FINDLAST() THEN
            NewLineNo := LastSalesLine."Line No." + 10000
        ELSE
            NewLineNo := 10000;

        SalesLine.INIT();
        SalesLine.VALIDATE("Document Type", SalesHeader."Document Type");
        SalesLine.VALIDATE("Document No.", SalesHeader."No.");
        SalesLine.VALIDATE("Line No.", NewLineNo);
        SalesLine.INSERT(TRUE);

        CASE pSalesLineType OF
            pSalesLineType::" ":
                BEGIN
                    SalesLine.VALIDATE(Type, "Sales Line Type"::" ");
                    SalesLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        SalesLine.VALIDATE(Description, Description);
                END;
            pSalesLineType::Item:
                BEGIN
                    SalesLine.VALIDATE(Type, "Sales Line Type"::Item);
                    SalesLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        SalesLine.VALIDATE(Description, Description);
                    IF VariantCode <> '' THEN
                        SalesLine.VALIDATE("Variant Code", VariantCode);
                    SalesLine.VALIDATE(Quantity, Quantity);
                    IF UnitPrice <> 0 THEN
                        SalesLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                    IF LineDiscount <> 0 THEN
                        SalesLine.VALIDATE("Line Discount %", LineDiscount);
                END;

            pSalesLineType::"G/L Account":
                BEGIN
                    SalesLine.VALIDATE(Type, "Sales Line Type"::"G/L Account");
                    SalesLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        SalesLine.VALIDATE(Description, Description);
                    SalesLine.VALIDATE(Quantity, Quantity);
                    IF UnitPrice <> 0 THEN
                        SalesLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                END;

            pSalesLineType::Resource:
                BEGIN
                    SalesLine.VALIDATE(Type, "Sales Line Type"::Resource);
                    SalesLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        SalesLine.VALIDATE(Description, Description);
                    SalesLine.VALIDATE(Quantity, Quantity);
                    IF WorkTypeCode <> '' then
                        SalesLine.Validate("Work Type Code", WorkTypeCode);
                    IF UnitPrice <> 0 THEN
                        SalesLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                END;
        END;
        OnBeforeSalesLineModify(SalesHeader, SalesLine);
        SalesLine.MODIFY(TRUE);
    END;


    local procedure InsertSalesPayoffLines(VAR ICISupportTicket: Record "ICI Support Ticket"; var SalesHeader: Record "Sales Header")
    var
        SupportSetup: Record "ICI Support Setup";
        SalesLine: Record "Sales Line";
        ICISupportTimeLogLine: Record "ICI Support Time Log Line";
        ICISupportTimeLogComment: Record "ICI Support Time Log Comment";
        SupportPayoffMgt: Codeunit "ICI Support Payoff Mgt.";
        ServiceEntryNo: Integer;
        LineDiscount: Integer;
        NoOfTextLines: Integer;
        FreeLbl: Label '(Free Service without Charge)', Comment = 'de-DE=(Kulanz-Leistung ohne Berechnung)';
    begin
        // Sales Lines und erbrachte Leistungen anlegen
        SupportSetup.GET();
        // IF SupportSetup."Sales Payoff Type" = SupportSetup."Sales Payoff Type"::" " THEN
        //     EXIT;

        ServiceEntryNo := 1;

        //Leistungszeilen einfügen
        ICISupportTimeLogLine.SetAutoCalcFields("Sales Payoff Type");
        ICISupportTimeLogLine.SetFilter("Sales Payoff Type", '<>%1', ICISupportTimeLogLine."Sales Payoff Type"::" ");

        ICISupportTimeLogLine.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTimeLogLine.SETFILTER("Accounting Type", ICISupportTimeLogLine.GetPayableAccountingTypeFilter());
        ICISupportTimeLogLine.SETRANGE(Cleared, FALSE);
        IF ICISupportTimeLogLine.FINDSET() THEN
            REPEAT
                NoOfTextLines := 0;

                ICISupportTimeLogLine.CalcFields("Discount %");
                LineDiscount := ICISupportTimeLogLine."Discount %";

                //ICISupportTimeLogLine.CalcFields("Sales Payoff Type"); // Autocalcfields
                // Insert Time Log Line
                InsertSalesLine(
                     SalesHeader
                     , SalesLine
                     , ICISupportTimeLogLine."Sales Payoff Type", ICISupportTimeLogLine."Sales Payoff No.", '',
                     ICISupportTimeLogLine."Time Decimal", 0, ICISupportTimeLogLine.Description,
                     0, FORMAT(ServiceEntryNo), LineDiscount,
                     ICISupportTimeLogLine."Work Type Code");

                SalesLine."ICI Support Ticket No." := ICISupportTimeLogLine."Support Ticket No.";
                SalesLine."ICI Time Log Line No." := ICISupportTimeLogLine."Line No.";
                SalesLine.MODIFY();

                // Insert Time Log Line Comments
                ICISupportTimeLogComment.SetRange("Support Ticket No.", ICISupportTimeLogLine."Support Ticket No.");
                ICISupportTimeLogComment.SetRange("Time Log Line No.", ICISupportTimeLogLine."Line No.");
                IF ICISupportTimeLogComment.FINDSET() THEN
                    REPEAT
                        InsertTextSalesLine(SalesHeader, SalesLine, ICISupportTimeLogComment.Description);
                    UNTIL ICISupportTimeLogComment.NEXT() = 0;



                //Kulanzzeile
                IF LineDiscount = 100 THEN BEGIN
                    InsertTextSalesLine(SalesHeader, SalesLine, FreeLbl);
                    NoOfTextLines += 2;
                END;

                IF NoOfTextLines > 1 THEN
                    InsertTextSalesLine(SalesHeader, SalesLine, '');

                ServiceEntryNo += 1;
                SupportPayoffMgt.SetSettlement(ICISupportTimeLogLine, SalesHeader."No.", '');

            UNTIL ICISupportTimeLogLine.NEXT() = 0;

        //Update Ticket
        // XXX - TODO 
        // IF SalesHeader."Document Type" = SalesHeader."Document Type"::Order THEN
        //     SupportPayoffMgt.CloseTicket(Rec, SalesHeader."No.");
    end;

    local procedure InsertSalesReturnLines(VAR ICISupportTicket: Record "ICI Support Ticket"; var SalesHeader: Record "Sales Header")
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesLine: Record "Sales Line";
        ServiceItem: Record "Service Item";
        Item: Record "Item";
        ItemTrackingCode: Record "Item Tracking Code";
        YourReturnTxt: Label 'Your Return Shipment:', Comment = 'de-DE=Ihre Rücksendung:';
        SNTxt: Label 'Serial No.: %1', Comment = '%1=Serial No;de-DE=Seriennr.: %1';
    begin
        IF NOT ICISupportSetup."Service Integration" THEN BEGIN

            IF NOT ServiceItem.GET(ICISupportTicket."Service Item No.") THEN
                EXIT;

            IF ICISupportSetup."Return Service Item" then begin
                InsertTextSalesLine(SalesHeader, SalesLine, YourReturnTxt);

                IF Item.GET(ServiceItem."Item No.") THEN begin
                    IF NOT ItemTrackingCode.GET(Item."Item Tracking Code") THEN
                        CLEAR(ItemTrackingCode);
                    InsertSalesLine(
                  SalesHeader
                  , SalesLine
                  , "Sales Line Type"::Item, Item."No.", ServiceItem."Variant Code", 1, 0, '',
                  0, '', 0, ''
                  );
                end;

                SalesLine."ICI Support Ticket No." := ICISupportTicket."No.";
                SalesLine.MODIFY();

            end;

            IF (ServiceItem."Serial No." <> '') THEN BEGIN

                IF (ItemTrackingCode."SN Sales Inbound Tracking" OR ItemTrackingCode."SN Specific Tracking") THEN
                    InsertTracking(SalesLine, '', ServiceItem."Serial No.");

                InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(SNTxt, ServiceItem."Serial No."));
            END;
            InsertTextSalesLine(SalesHeader, SalesLine, '');

        END ELSE BEGIN

            InsertTextSalesLine(SalesHeader, SalesLine, YourReturnTxt);
            CopyFromAttachedSalesDoc(ICISupportTicket, SalesHeader);

        END;
    end;

    local procedure InsertTracking(var SalesLine: Record "Sales Line"; LotNo: Text[50]; SerialNo: Text[50])
    var
        TempTrackingSpecification: Record "Tracking Specification" temporary;
        CreateReservEntry: Codeunit "Create Reserv. Entry";
        EntryNo: Integer;
    begin
        IF TempTrackingSpecification.FINDLAST() THEN
            EntryNo := TempTrackingSpecification."Entry No.";

        CLEAR(TempTrackingSpecification);

        EntryNo := EntryNo + 1;
        TempTrackingSpecification.VALIDATE("Entry No.", EntryNo);
        TempTrackingSpecification.VALIDATE("Source Type", DATABASE::"Sales Line");
        TempTrackingSpecification.VALIDATE("Source Subtype", SalesLine."Document Type"::"Return Order");
        TempTrackingSpecification.VALIDATE("Source ID", SalesLine."Document No.");
        TempTrackingSpecification.VALIDATE("Source Ref. No.", SalesLine."Line No.");
        TempTrackingSpecification.VALIDATE("Item No.", SalesLine."No.");
        TempTrackingSpecification.VALIDATE("Variant Code", SalesLine."Variant Code");
        TempTrackingSpecification.VALIDATE("Location Code", SalesLine."Location Code");
        IF SalesLine."Bin Code" <> '' THEN
            TempTrackingSpecification.VALIDATE("Bin Code", SalesLine."Bin Code");
        TempTrackingSpecification.VALIDATE(Positive, TRUE);

        IF SerialNo <> '' THEN BEGIN
            TempTrackingSpecification.VALIDATE("Serial No.", SerialNo);
            TempTrackingSpecification.VALIDATE("New Serial No.", SerialNo);
        END;
        IF LotNo <> '' THEN BEGIN
            TempTrackingSpecification.VALIDATE("Lot No.", LotNo);
            TempTrackingSpecification.VALIDATE("New Lot No.", LotNo);
        END;
        TempTrackingSpecification.VALIDATE("Creation Date", SalesLine."Posting Date");
        TempTrackingSpecification.VALIDATE("Quantity (Base)", SalesLine.Quantity);
        TempTrackingSpecification.INSERT(TRUE);

        // CreateReservEntry.CreateReservEntryFor(
        //   "Source Type",
        //   "Source Subtype",
        //   "Source ID",
        //   "Source Batch Name",
        //   "Source Prod. Order Line",
        //   "Source Ref. No.",
        //   "Qty. per Unit of Measure",
        //   1,
        //   "Quantity (Base)",
        //   "Serial No.",
        //   "Lot No.");
        // CreateReservEntry.SetNewSerialLotNo(SerialNo, LotNo);

        CreateReservEntry.CreateReservEntryFrom(TempTrackingSpecification);

        CreateReservEntry.CreateEntry(
          TempTrackingSpecification."Item No.",
          TempTrackingSpecification."Variant Code",
          TempTrackingSpecification."Location Code",
          TempTrackingSpecification.Description,
          0D, 0D, 0, "Reservation Status"::Tracking);

    end;

    local procedure CopyFromAttachedSalesDoc(VAR ICISupportTicket: Record "ICI Support Ticket"; SalesHeader: Record "Sales Header")
    var
        SalesShipmentLine: Record "Sales Shipment Line";
        SalesInvoiceLine: Record "Sales Invoice Line";
        SalesLine: Record "Sales Line";
        CopyDocumentMgt: Codeunit "Copy Document Mgt.";
        MissingExCostRevLink: Boolean;
        LinesNotCopied: Integer;
        CopyFromDocQst: Label 'Copy Positions from Document %1?', Comment = '%1= Source Document No|de-DE=Positionen aus Beleg %1 übernehmen?';
        NotCopiedLbl: Label 'The Positions containing a G/L Account that does not allow direct posting were not copied', Comment = 'de-DE=Die Belegzeile(n) mit einem Sachkonto, das kein direktes Buchen zulässt, wurden nicht in den neuen Beleg kopiert.';
    begin
        IF ICISupportTicket."Pst. Sales Shipment No." <> '' THEN BEGIN

            IF CONFIRM(CopyFromDocQst, FALSE, ICISupportTicket."Pst. Sales Shipment No.") THEN BEGIN
                SalesShipmentLine.SETRANGE("Document No.", ICISupportTicket."Pst. Sales Shipment No.");
                IF SalesShipmentLine.FINDLAST() THEN BEGIN

                    LinesNotCopied := 0;
                    CLEAR(CopyDocumentMgt);
                    CopyDocumentMgt.SetProperties(FALSE, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE);
                    CopyDocumentMgt.CopySalesShptLinesToDoc(SalesHeader, SalesShipmentLine, LinesNotCopied, MissingExCostRevLink);
                    IF LinesNotCopied <> 0 THEN
                        MESSAGE(NotCopiedLbl);

                    SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                    SalesLine.SETRANGE("Document No.", SalesHeader."No.");
                    SalesLine.SETFILTER(Type, '<>%1', "Sales Line Type"::" ");
                    IF SalesLine.FINDSET() THEN
                        REPEAT
                            SalesLine.VALIDATE("ICI Support Ticket No.", ICISupportTicket."No.");
                        UNTIL SalesLine.NEXT() = 0;

                    InsertTextSalesLine(SalesHeader, SalesLine, '');

                END;
            END;
        END ELSE
            IF ICISupportTicket."Pst. Sales Invoice No." <> '' THEN
                IF CONFIRM(CopyFromDocQst, FALSE, ICISupportTicket."Pst. Sales Invoice No.") THEN BEGIN
                    SalesInvoiceLine.SETRANGE("Document No.", ICISupportTicket."Pst. Sales Invoice No.");
                    IF SalesInvoiceLine.FINDLAST() THEN BEGIN

                        LinesNotCopied := 0;
                        CLEAR(CopyDocumentMgt);
                        CopyDocumentMgt.SetProperties(FALSE, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE);
                        CopyDocumentMgt.CopySalesInvLinesToDoc(SalesHeader, SalesInvoiceLine, LinesNotCopied, MissingExCostRevLink);
                        IF LinesNotCopied <> 0 THEN
                            MESSAGE(NotCopiedLbl);

                        SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                        SalesLine.SETRANGE("Document No.", SalesHeader."No.");
                        SalesLine.SETFILTER(Type, '<>%1', SalesLine.Type::" ");
                        IF SalesLine.FINDSET() THEN
                            REPEAT
                                SalesLine.VALIDATE("ICI Support Ticket No.", ICISupportTicket."Pst. Sales Invoice No.");
                            UNTIL SalesLine.NEXT() = 0;
                        InsertTextSalesLine(SalesHeader, SalesLine, '');
                    END;
                END;
    end;

    LOCAL PROCEDURE FindOrAddCustomer(var ICISupportTicket: Record "ICI Support Ticket");
    VAR

        Contact: Record "Contact";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Confirmed: Boolean;
        NoCustErr: Label 'Cannot show the Document, because there is no Customer for Contact %1.', Comment = 'de-DE=Der Beleg kann nicht erzeugt werden, da es zum Kontakt %1 keinen Debitoren gibt.';
        CreateCustTxt: Label 'Do you want to create a Customer for Contact %2, using Customer Template %1?', Comment = 'de-DE=Wollen Sie mit der Debitorenvorlage %1 einen Debitoren für Kontakt %2 erzeugen?';
    BEGIN

        //Update Customer By Company Contact
        UpdateCustByCont(ICISupportTicket, ICISupportTicket."Company Contact No.");
        IF ICISupportTicket."Customer No." <> '' THEN BEGIN
            ICISupportTicket.MODIFY();
            EXIT;
        END;

        //Debior erzeugen
        ICISupportMailSetup.GET();
        ICISupportSetup.GET();
        ICISupportSetup.TESTFIELD("Customer Template Code");

        IF NOT GUIALLOWED THEN
            Confirmed := FALSE
        ELSE
            Confirmed := CONFIRM(CreateCustTxt, FALSE, ICISupportSetup."Customer Template Code", ICISupportTicket."Company Contact No.");

        IF Confirmed THEN BEGIN
            Contact.SetHideValidationDialog(TRUE);
            Contact.GET(ICISupportTicket."Company Contact No.");
            Contact.CreateCustomerFromTemplate(ICISupportSetup."Customer Template Code"); // >V18 
            UpdateCustByCont(ICISupportTicket, ICISupportTicket."Company Contact No.");
            Contact.SetHideValidationDialog(FALSE);
            ICISupportTicket.MODIFY();
        END ELSE
            ERROR(NoCustErr, ICISupportTicket."Company Contact No.");

    END;

    LOCAL PROCEDURE UpdateCustByCont(ICISupportTicket: Record "ICI Support Ticket"; CompanyNo: Code[20]);
    VAR
        ContactBusinessRelation: Record "Contact Business Relation";
    BEGIN
        ContactBusinessRelation.SETCURRENTKEY("Link to Table", "Contact No.");
        ContactBusinessRelation.SETRANGE("Contact No.", CompanyNo);
        ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);

        IF ContactBusinessRelation.FINDFIRST() THEN
            ICISupportTicket.VALIDATE("Customer No.", ContactBusinessRelation."No.");

    END;


    PROCEDURE CreatePurchaseDocument(var ICISupportTicket: Record "ICI Support Ticket"; DocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order"; PurchaseHeaderDate: Date);
    VAR
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        PurchaseHeader: Record "Purchase Header";
        Vendor: Record "Vendor";
        PurchaseLine: Record "Purchase Line";
        ServiceItem: Record "Service Item";
        ICISupportTicketItem: Record "ICI Support Ticket Item";
        IsHandled: Boolean;
        DocTypeNotSupportedTxt: Label 'The Document Type %1 is not supported', Comment = 'de-DE=Die Belegart %1 wird nicht unterstützt.';
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
        OpenPurchDocTxt: Label 'Do you want to Edit the %1 %2?', Comment = 'de-DE=Möchten sie den %1 %2 bearbeiten?';
    BEGIN
        IF NOT ((DocumentType = DocumentType::Order) OR (DocumentType = DocumentType::"Return Order")) THEN
            ERROR(DocTypeNotSupportedTxt, FORMAT(DocumentType));

        ICISupportMailSetup.GET();

        GetVendorForDocType(ICISupportTicket, Vendor, DocumentType, ICISupportTicket."Service Item No.");

        InsertPurchaseHeader(ICISupportTicket, PurchaseHeader, DocumentType, PurchaseHeaderDate, Vendor);

        OnBeforePurchaseLineHeader(IsHandled, ICISupportTicket, PurchaseHeader, PurchaseLine);
        if not IsHandled then begin
            InsertTextPurchLine(PurchaseHeader, PurchaseLine, STRSUBSTNO(TicketRefTxt, ICISupportTicket."No."));
            InsertTextPurchLine(PurchaseHeader, PurchaseLine, ICISupportTicket.Description);
            InsertTextPurchLine(PurchaseHeader, PurchaseLine, '');
        end;
        OnAfterPurchaseLineHeader(ICISupportTicket, PurchaseHeader, PurchaseLine);

        IF ServiceItem.GET(ICISupportTicket."Service Item No.") THEN
            IF ServiceItem."Item No." <> '' THEN
                InsertPurchaseItemLine(PurchaseHeader, PurchaseLine, ServiceItem."Item No.", '', 1);

        //Insert TicketItems
        ICISupportTicketItem.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketItem.SETRANGE("Use in Purch. Doc.", TRUE);
        IF ICISupportTicketItem.FINDSET() THEN
            REPEAT
                CASE ICISupportTicketItem.Type OF
                    ICISupportTicketItem.Type::Item:
                        BEGIN
                            InsertPurchaseItemLine(PurchaseHeader, PurchaseLine, ICISupportTicketItem."No.", ICISupportTicketItem."Item Variant Code", ICISupportTicketItem.Quantity);

                            IF PurchaseLine."Unit of Measure Code" <> ICISupportTicketItem."Unit of Measure Code" THEN BEGIN
                                PurchaseLine.VALIDATE("Unit of Measure Code", ICISupportTicketItem."Unit of Measure Code");
                                PurchaseLine.MODIFY();
                            END;
                        END;
                    ELSE
                        Error(NotInLicenseErr);
                END;
            UNTIL ICISupportTicketItem.NEXT() = 0;

        IsHandled := false;
        OnBeforePurchaseLineFooter(IsHandled, ICISupportTicket, PurchaseHeader, PurchaseLine);
        // this would be the place for purchase doc footer lines
        OnAfterPurchaseLineFooter(ICISupportTicket, PurchaseHeader, PurchaseLine);

        //Open Doc.
        IF GUIALLOWED THEN
            IF CONFIRM(OpenPurchDocTxt, TRUE, PurchaseHeader."Document Type", PurchaseHeader."No.") THEN
                CASE DocumentType OF
                    DocumentType::Order:
                        PAGE.RUN(PAGE::"Purchase Order", PurchaseHeader);
                    DocumentType::"Return Order":
                        PAGE.RUN(PAGE::"Purchase Return Order", PurchaseHeader);
                END;

    END;

    LOCAL PROCEDURE GetVendorForDocType(VAR ICISupportTicket: Record "ICI Support Ticket"; VAR Vendor: Record "Vendor"; DocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order"; ServiceItemNo: Code[20]);
    VAR
        ServiceItem: Record "Service Item";
        NoVendorFoundTxt: Label 'Could not find Vendor for Service Item %1 - "%2', Comment = 'de-DE=Kein Kreditor zu Serviceartikel %1 - "%2';
    BEGIN
        IF ICISupportTicket."Service Item No." = '' THEN EXIT;

        CASE DocumentType OF

            DocumentType::Order:
                IF NOT GetVendorFromSerialNo(Vendor, ServiceItemNo) THEN
                    IF NOT GetVendorFromItemNo(Vendor, ServiceItemNo) THEN
                        ERROR(NoVendorFoundTxt, ServiceItem."No.", ServiceItem.Name);


            DocumentType::"Return Order":
                IF NOT GetVendorFromILE(Vendor, ServiceItemNo) THEN
                    IF NOT GetVendorFromSerialNo(Vendor, ServiceItemNo) THEN
                        IF NOT GetVendorFromItemNo(Vendor, ServiceItemNo) THEN
                            ERROR(NoVendorFoundTxt, ServiceItem."No.", ServiceItem.Name);

        END;
    END;

    LOCAL PROCEDURE GetVendorFromSerialNo(VAR Vendor: Record "Vendor"; ServiceItemNo: Code[20]) OK: Boolean;
    VAR
        ServiceItem: Record "Service Item";
    BEGIN
        ServiceItem.GET(ServiceItemNo);
        IF ServiceItem."Vendor No." <> '' THEN
            OK := Vendor.GET(ServiceItem."Vendor No.");

    END;

    LOCAL PROCEDURE GetVendorFromItemNo(VAR Vendor: Record "Vendor"; ServiceItemNo: Code[20]) OK: Boolean;
    VAR
        Item: Record "Item";
        ServiceItem: Record "Service Item";
    BEGIN
        ServiceItem.GET(ServiceItemNo);
        if Item.GET(ServiceItem."Item No.") then begin

            IF Item."Vendor No." <> '' THEN
                OK := Vendor.GET(Item."Vendor No.");
        end else
            OK := false;
    END;

    LOCAL PROCEDURE GetVendorFromILE(VAR Vendor: Record "Vendor"; ServiceItemNo: Code[20]) OK: Boolean;
    VAR
        ServiceItem: Record "Service Item";
        Item: Record "Item";
        ItemLedgerEntry: Record "Item Ledger Entry";
    BEGIN
        ServiceItem.GET(ServiceItemNo);
        if Item.GET(ServiceItem."Item No.") then begin

            ItemLedgerEntry.SETRANGE("Item No.", Item."No.");
            ItemLedgerEntry.SETFILTER(Quantity, '>0');
            ItemLedgerEntry.SETRANGE("Source Type", ItemLedgerEntry."Source Type"::Vendor);

            IF ItemLedgerEntry.FINDLAST() THEN
                OK := Vendor.GET(ItemLedgerEntry."Source No.");
        end else
            OK := false;
    END;

    LOCAL PROCEDURE InsertPurchaseHeader(VAR ICISupportTicket: Record "ICI Support Ticket"; VAR PurchaseHeader: Record "Purchase Header"; DocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order"; SalesHeaderDate: Date; VAR Vendor: Record "Vendor");
    VAR
        //Customer: Record "Customer";
        Contact: Record "Contact";
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
    BEGIN
        Contact.GET(ICISupportTicket."Company Contact No.");
        //IF "Customer No." <> '' THEN
        //  Customer.GET("Customer No.")
        //ELSE
        //  CLEAR("Customer No.");

        // Purchase Header anlegen
        PurchaseHeader.INIT();
        PurchaseHeader.SetHideValidationDialog(TRUE);
        PurchaseHeader."No." := '';
        PurchaseHeader.VALIDATE("Document Type", DocumentType);
        PurchaseHeader.VALIDATE("Posting Date", SalesHeaderDate);
        PurchaseHeader.VALIDATE("Document Date", SalesHeaderDate);
        PurchaseHeader.INSERT(TRUE);

        PurchaseHeader.VALIDATE("Your Reference", STRSUBSTNO(TicketRefTxt, ICISupportTIcket."No."));
        IF Vendor."No." <> '' THEN
            PurchaseHeader.VALIDATE("Buy-from Vendor No.", Vendor."No.");
        PurchaseHeader.VALIDATE("Order Date", SalesHeaderDate);
        PurchaseHeader.VALIDATE("ICI Support Ticket No.", ICISupportTIcket."No.");
        PurchaseHeader.MODIFY(TRUE);


    END;

    LOCAL PROCEDURE InsertPurchaseItemLine(VAR PurchaseHeader: Record "Purchase Header"; VAR PurchaseLine: Record "Purchase Line"; ItemNo: Code[20]; VariantCode: Code[20]; Qty: Decimal);
    var
        LastPurchaseLine: Record "Purchase Line";
        NewLineNo: Integer;

    BEGIN

        LastPurchaseLine.SETRANGE("Document Type", PurchaseHeader."Document Type");
        LastPurchaseLine.SETRANGE("Document No.", PurchaseHeader."No.");
        IF LastPurchaseLine.FINDLAST() THEN
            NewLineNo := LastPurchaseLine."Line No." + 10000
        ELSE
            NewLineNo := 10000;

        PurchaseLine.INIT();
        PurchaseLine.VALIDATE("Document Type", PurchaseHeader."Document Type");
        PurchaseLine.VALIDATE("Document No.", PurchaseHeader."No.");
        PurchaseLine.VALIDATE("Line No.", NewLineNo);
        PurchaseLine.INSERT(TRUE);

        PurchaseLine.VALIDATE(Type, PurchaseLine.Type::Item);
        PurchaseLine.VALIDATE("No.", ItemNo);
        IF VariantCode <> '' THEN
            PurchaseLine.VALIDATE("Variant Code", VariantCode);
        PurchaseLine.VALIDATE(Quantity, Qty);

        PurchaseLine.MODIFY(TRUE);

    END;

    PROCEDURE InsertTextPurchLine(VAR PurchaseHeader: Record "Purchase Header"; VAR PurchaseLine: Record "Purchase Line"; Description: Text);
    BEGIN
        InsertPurchLine(
          PurchaseHeader
          , PurchaseLine, 0, '', '', 0, 0
          , Description, 1, '', 0);
    END;

    PROCEDURE InsertPurchLine(VAR PurchaseHeader: Record "Purchase Header"; VAR PurchaseLine: Record "Purchase Line"; Type: Option "Text","G/L Account",Item,Resource; No: Code[20]; VariantCode: Code[10]; Quantity: Decimal; UnitPrice: Decimal; Description: Text; PrintMarks: Option " ",Fettdruck,Zwischensumme,Seitenwechsel,Unsichtbar; Position: Code[10]; LineDiscount: Decimal);
    VAR
        LastPurchaseLine: Record "Purchase Line";
        NewLineNo: Integer;
    BEGIN
        // Einf?gen einer Verkaufszeile

        Description := COPYSTR(Description, 1, MAXSTRLEN(PurchaseLine.Description));

        LastPurchaseLine.SETRANGE("Document Type", PurchaseHeader."Document Type");
        LastPurchaseLine.SETRANGE("Document No.", PurchaseHeader."No.");
        IF LastPurchaseLine.FINDLAST() THEN
            NewLineNo := LastPurchaseLine."Line No." + 10000
        ELSE
            NewLineNo := 10000;

        PurchaseLine.INIT();
        PurchaseLine.VALIDATE("Document Type", PurchaseHeader."Document Type");
        PurchaseLine.VALIDATE("Document No.", PurchaseHeader."No.");
        PurchaseLine.VALIDATE("Line No.", NewLineNo);
        PurchaseLine.INSERT(TRUE);

        CASE Type OF
            Type::Text:
                BEGIN
                    PurchaseLine.VALIDATE(Type, PurchaseLine.Type::" ");
                    PurchaseLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        PurchaseLine.VALIDATE(Description, Description);
                END;
            Type::Item:
                BEGIN
                    PurchaseLine.VALIDATE(Type, PurchaseLine.Type::Item);
                    PurchaseLine.VALIDATE("No.", No);
                    IF Description <> '' THEN
                        PurchaseLine.VALIDATE(Description, Description);
                    IF VariantCode <> '' THEN
                        PurchaseLine.VALIDATE("Variant Code", VariantCode);
                    PurchaseLine.VALIDATE(Quantity, Quantity);
                    IF UnitPrice <> 0 THEN
                        PurchaseLine.VALIDATE("Unit Price (LCY)", ROUND(UnitPrice));
                    IF LineDiscount <> 0 THEN
                        PurchaseLine.VALIDATE("Line Discount %", LineDiscount);
                END;

        END;

        PurchaseLine.MODIFY(TRUE);
    END;

    PROCEDURE CreateServiceDocument(VAR ICISupportTicket: Record "ICI Support Ticket"; DocumentType: Option Quote,"Order",Invoice,"Credit Memo"; SalesHeaderDate: Date);
    VAR
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        //Customer: Record Customer;
        //Contact: Record Contact;
        //Salesperson: Record "Salesperson/Purchaser";
        ServiceItemLine: Record "Service Item Line";
        ServiceHeader: Record "Service Header";
        ServiceLine: Record "Service Line";
        SupportTicketItem: Record "ICI Support Ticket Item";
        ICISupportSetup: Record "ICI Support Setup";
        ICIServiceIntegrationMgt: Codeunit "ICI Service Integration Mgt.";
        NextServiceItem: Integer;
        IsHandled: Boolean;
        ServiceItemNo: Code[20];
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
        OpenServDocTxt: Label 'Do you want to Edit the %1 - %2?', Comment = 'de-DE=Möchten sie den %1 %2 bearbeiten?';
        TicketCreatedTxt: Label 'Ticket Created by %1', Comment = 'de-DE=Ticket erstellt von %1';
        TicketProccedTxt: Label 'Ticket processed by %1', Comment = 'de-DE=Ticket bearbeitet von %1';
        TicketShippedTxt: Label 'Delivered on %1 per Remote', Comment = 'de-DE=Geliefert am %1 per Fernwartung';
        CreateServiceItemTxt: Label 'Do yout want to create an empty Service Item?', Comment = 'de-DE=Wollen Sie einen leeren Serivceartikel anlegen?';
    BEGIN
        ICISupportMailSetup.GET();
        ICISupportSetup.GET();

        IF NOT ICISupportSetup."Service Doc. w.o. Serviceitem" THEN
            ICISupportTicket.TESTFIELD("Service Item No.");

        IF (ICISupportTicket."Service Item No." = '') AND (ICISupportTicket."Serial No." = '') THEN
            IF GuiAllowed then
                IF Confirm(CreateServiceItemTxt, true, ICISupportTicket."Serial No.") THEN begin
                    ServiceItemNo := ICIServiceIntegrationMgt.CreateServiceItemForTicket(ICISupportTicket."Serial No.", ICISupportTicket."No.");
                    ICISupportTicket.Validate("Service Item No.", ServiceItemNo);
                    ICISupportTicket.Modify();
                end;

        ICISupportTicket.TESTFIELD("Customer No.");

        ICISupportTicket.CALCFIELDS("Support User Name");

        InsertServiceHeader(ICISupportTicket, ServiceHeader, DocumentType, SalesHeaderDate);
        InsertServiceItemLine(ServiceHeader, ServiceItemLine, ICISupportTicket."Service Item No.", 10000);

        // HeaderLines
        OnBeforeServiceLineHeader(IsHandled, ICISupportTicket, ServiceHeader, ServiceLine, ServiceItemLine);
        IF NOT IsHandled then begin
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, STRSUBSTNO(TicketRefTxt, ICISupportTicket."No."));
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, ICISupportTicket.Description);
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, '');
        end;
        OnAfterServiceLineHeader(ICISupportTicket, ServiceHeader, ServiceLine, ServiceItemLine);


        //InsertLines
        CASE DocumentType OF
            DocumentType::Quote:
                ;
            DocumentType::Order:
                InsertServicePayoffLines(ICISupportTicket, ServiceHeader, ServiceItemLine, ServiceLine);
            DocumentType::Invoice:
                InsertServicePayoffLines(ICISupportTicket, ServiceHeader, ServiceItemLine, ServiceLine);
            DocumentType::"Credit Memo":
                ;
        END;


        //Insert TicketItems
        NextServiceItem := 20000;
        SupportTicketItem.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
        SupportTicketItem.SETRANGE("Use in Service Doc.", TRUE);
        IF SupportTicketItem.FINDSET() THEN
            REPEAT
                CASE SupportTicketItem.Type OF
                    SupportTicketItem.Type::Item:
                        BEGIN
                            InsertServLine(
                                ServiceHeader, ServiceLine, ServiceItemLine
                                , ServiceLine.Type::Item, SupportTicketItem."No.", SupportTicketItem."Item Variant Code", SupportTicketItem.Quantity, 0, SupportTicketItem.Description, 0);

                            IF ServiceLine."Unit of Measure Code" <> SupportTicketItem."Unit of Measure Code" THEN BEGIN
                                ServiceLine.VALIDATE("Unit of Measure Code", SupportTicketItem."Unit of Measure Code");
                                ServiceLine.MODIFY();
                            END;
                        END;

                    SupportTicketItem.Type::Resource:
                        InsertServLine(
                            ServiceHeader, ServiceLine, ServiceItemLine
                            , ServiceLine.Type::Resource, SupportTicketItem."No.", SupportTicketItem."Item Variant Code", SupportTicketItem.Quantity, 0, SupportTicketItem.Description, 0);

                    SupportTicketItem.Type::ServiceItem:
                        BEGIN
                            InsertServiceItemLine(ServiceHeader, ServiceItemLine, SupportTicketItem."No.", NextServiceItem);
                            NextServiceItem += 10000;
                        END;
                    SupportTicketItem.Type::ServiceCost:
                        InsertServLine(ServiceHeader, ServiceLine, ServiceItemLine,
                        ServiceLine.Type::Cost, SupportTicketItem."No.", SupportTicketItem."Item Variant Code", SupportTicketItem.Quantity, 0, SupportTicketItem.Description, 0);
                END;
            UNTIL SupportTicketItem.NEXT() = 0;


        //FooterLines
        IsHandled := false;
        OnBeforeServiceLineFooter(IsHandled, ICISupportTicket, ServiceHeader, ServiceLine, ServiceItemLine);
        if not IsHandled then begin
            IF DocumentType IN [DocumentType::Order, DocumentType::Invoice] THEN
                InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, '');
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, STRSUBSTNO(TicketCreatedTxt, ICISupportTicket."Curr. Contact Name"));
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, STRSUBSTNO(TicketProccedTxt, ICISupportTicket."Support User Name"));
            InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, STRSUBSTNO(TicketShippedTxt, WORKDATE()));
        end;
        OnAfterServiceLineFooter(ICISupportTicket, ServiceHeader, ServiceLine, ServiceItemLine);


        //Open Doc.
        IF GUIALLOWED THEN
            IF CONFIRM(OpenServDocTxt, TRUE, ServiceHeader."Document Type", ServiceHeader."No.") THEN
                CASE DocumentType OF
                    DocumentType::Quote:
                        PAGE.RUN(PAGE::"Service Quote", ServiceHeader);
                    DocumentType::Order:
                        PAGE.RUN(PAGE::"Service Order", ServiceHeader);
                    DocumentType::Invoice:
                        PAGE.RUN(PAGE::"Service Invoice", ServiceHeader);
                    DocumentType::"Credit Memo":
                        PAGE.RUN(PAGE::"Service Credit Memo", ServiceHeader);
                END;
    END;

    LOCAL PROCEDURE InsertServiceHeader(VAR ICISupportTicket: Record "ICI Support Ticket"; VAR ServiceHeader: Record "Service Header"; DocumentType: Option Quote,"Order",Invoice,ReturnOrder; SalesHeaderDate: Date);
    VAR
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = 'de-DE=Ticket-Referenz: %1';
    BEGIN

        Contact.GET(ICISupportTicket."Company Contact No.");

        // Service Header anlegen
        ServiceHeader.INIT();
        ServiceHeader.SetHideValidationDialog(TRUE);
        ServiceHeader."No." := '';
        ServiceHeader.VALIDATE("Document Type", DocumentType);
        ServiceHeader.VALIDATE("Posting Date", SalesHeaderDate);
        ServiceHeader.VALIDATE("Document Date", SalesHeaderDate);
        ServiceHeader.INSERT(TRUE);

        ServiceHeader.VALIDATE("Your Reference", STRSUBSTNO(TicketRefTxt, ICISupportTicket."No."));
        ServiceHeader.VALIDATE(Description, COPYSTR(ICISupportTicket.Description, 1, MAXSTRLEN(ServiceHeader.Description)));
        ServiceHeader.VALIDATE("Customer No.", ICISupportTicket."Customer No.");

        IF ICISupportTicket."Current Contact No." <> '' THEN
            ServiceHeader.VALIDATE("Contact No.", ICISupportTicket."Current Contact No.");

        //IF "Service Contract No." <> '' THEN
        //    VALIDATE("Contract No.", "Service Contract No.");

        ServiceHeader.VALIDATE("Order Date", SalesHeaderDate);

        ICISupportUser.GET(ICISupportTicket."Support User ID");
        ServiceHeader.VALIDATE("Salesperson Code", ICISupportUser."Salesperson Code");
        ServiceHeader.VALIDATE("ICI Support Ticket No.", ICISupportTicket."No.");

        IF ICISupportTicket."Ship-to Code" <> '' THEN
            ServiceHeader.VALIDATE("Ship-to Code", ICISupportTicket."Ship-to Code");

        IF ((ICISupportTicket."Ship-to Name" <> '') OR (ICISupportTicket."Ship-to Address" <> '') OR (ICISupportTicket."Ship-to City" <> '') OR (ICISupportTicket."Ship-to Post Code" <> '')) THEN BEGIN
            ServiceHeader.VALIDATE("Ship-to Name", ICISupportTicket."Ship-to Name");
            ServiceHeader.VALIDATE("Ship-to Name 2", ICISupportTicket."Ship-to Name 2");
            ServiceHeader.VALIDATE("Ship-to Address", ICISupportTicket."Ship-to Address");
            ServiceHeader.VALIDATE("Ship-to Address 2", ICISupportTicket."Ship-to Address 2");
            ServiceHeader.VALIDATE("Ship-to City", ICISupportTicket."Ship-to City");
            ServiceHeader.VALIDATE("Ship-to Post Code", ICISupportTicket."Ship-to Post Code");
            ServiceHeader.VALIDATE("Ship-to Contact", ICISupportTicket."Ship-to Contact");
            ServiceHeader.VALIDATE("Ship-to County", ICISupportTicket."Ship-to County");
            ServiceHeader.VALIDATE("Ship-to Country/Region Code", ICISupportTicket."Ship-to Country/Region Code");
        END;

        IF ICISupportTicket."Shipment Method Code" <> '' THEN
            ServiceHeader.VALIDATE("Shipment Method Code", ICISupportTicket."Shipment Method Code");
        IF ICISupportTicket."Shipping Agent Code" <> '' THEN BEGIN
            ServiceHeader.VALIDATE("Shipping Agent Code", ICISupportTicket."Shipping Agent Code");
            ServiceHeader.VALIDATE("Shipping Agent Service Code", ICISupportTicket."Shipping Agent Service Code");
        END;

        IF ICISupportTicket."Location Code" <> '' THEN
            ServiceHeader.VALIDATE("Location Code", ICISupportTicket."Location Code");

        ServiceHeader.MODIFY(TRUE);

    END;

    LOCAL PROCEDURE InsertServiceItemLine(VAR ServiceHeader: Record "Service Header"; VAR ServiceItemLine: Record "Service Item Line"; ServiceItemNo: Code[20]; LineNo: Integer);
    BEGIN
        ServiceItemLine.INIT();
        ServiceItemLine.VALIDATE("Document Type", ServiceHeader."Document Type");
        ServiceItemLine.VALIDATE("Document No.", ServiceHeader."No.");
        ServiceItemLine.VALIDATE("Line No.", LineNo);
        ServiceItemLine.INSERT(TRUE);

        if ServiceItemNo <> '' THEN
            ServiceItemLine.VALIDATE("Service Item No.", ServiceItemNo);
        ServiceItemLine.MODIFY(TRUE);

    END;

    PROCEDURE InsertServLine(VAR ServiceHeader: Record "Service Header"; VAR ServiceLine: Record "Service Line"; VAR ServiceItemLine: Record "Service Item Line"; LineType: enum "Service Line Type"; LineNo: Code[20];
                                                                                                                                                                                VariantCode: Code[10];
                                                                                                                                                                                LineQuantity: Decimal;
                                                                                                                                                                                UnitPrice: Decimal;
                                                                                                                                                                                LineDescription: Text;
                                                                                                                                                                                LineDiscount: Decimal);
    VAR
        LastServiceLine: Record "Service Line";
        NewLineNo: Integer;
    BEGIN
        // Einfügen einer Verkaufszeile

        LineDescription := COPYSTR(LineDescription, 1, MAXSTRLEN(ServiceLine.Description));

        LastServiceLine.SETRANGE("Document Type", ServiceHeader."Document Type");
        LastServiceLine.SETRANGE("Document No.", ServiceHeader."No.");
        IF LastServiceLine.FINDLAST() THEN
            NewLineNo := LastServiceLine."Line No." + 10000
        ELSE
            NewLineNo := 10000;

        //WITH ServiceLine DO BEGIN
        ServiceLine.INIT();
        ServiceLine.SetHideReplacementDialog(TRUE);
        ServiceLine.VALIDATE("Document Type", ServiceHeader."Document Type");
        ServiceLine.VALIDATE("Document No.", ServiceHeader."No.");
        ServiceLine.VALIDATE("Line No.", NewLineNo);
        ServiceLine.VALIDATE("Service Item No.", ServiceItemLine."Service Item No.");
        ServiceLine.INSERT(TRUE);

        CASE LineType OF
            LineType::" ":
                BEGIN
                    ServiceLine.VALIDATE(Type, "Service Line Type"::" ");
                    ServiceLine.VALIDATE("No.", LineNo);
                    IF LineDescription <> '' THEN
                        ServiceLine.VALIDATE(Description, LineDescription);
                END;

            LineType::Item:
                BEGIN
                    ServiceLine.VALIDATE(Type, "Service Line Type"::Item);
                    ServiceLine.VALIDATE("No.", LineNo);
                    IF LineDescription <> '' THEN
                        ServiceLine.VALIDATE(Description, LineDescription);
                    IF VariantCode <> '' THEN
                        ServiceLine.VALIDATE("Variant Code", VariantCode);
                    ServiceLine.VALIDATE(Quantity, LineQuantity);
                    IF UnitPrice <> 0 THEN
                        ServiceLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                    IF LineDiscount <> 0 THEN
                        ServiceLine.VALIDATE("Line Discount %", LineDiscount);
                END;

            LineType::"G/L Account":
                BEGIN
                    ServiceLine.VALIDATE(Type, "Service Line Type"::"G/L Account");
                    ServiceLine.VALIDATE("No.", LineNo);
                    IF LineDescription <> '' THEN
                        ServiceLine.VALIDATE(Description, LineDescription);
                    ServiceLine.VALIDATE(Quantity, LineQuantity);
                    IF UnitPrice <> 0 THEN
                        ServiceLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                END;

            LineType::Resource:
                BEGIN
                    ServiceLine.VALIDATE(Type, "Service Line Type"::Resource);
                    ServiceLine.VALIDATE("No.", LineNo);
                    IF LineDescription <> '' THEN
                        ServiceLine.VALIDATE(Description, LineDescription);
                    ServiceLine.VALIDATE(Quantity, LineQuantity);
                    IF UnitPrice <> 0 THEN
                        ServiceLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                END;

            LineType::Cost:
                BEGIN
                    ServiceLine.VALIDATE(Type, "Service Line Type"::Cost);
                    ServiceLine.VALIDATE("No.", LineNo);
                    IF LineDescription <> '' THEN
                        ServiceLine.VALIDATE(Description, LineDescription);
                    ServiceLine.VALIDATE(Quantity, LineQuantity);
                    IF UnitPrice <> 0 THEN
                        ServiceLine.VALIDATE("Unit Price", ROUND(UnitPrice));
                END;
        END;

        ServiceLine.MODIFY(TRUE);
    END;

    PROCEDURE InsertTextServLine(VAR ServiceHeader: Record "Service Header"; VAR ServiceLine: Record "Service Line"; VAR ServiceItemLine: Record "Service Item Line"; Description: Text);
    BEGIN
        InsertServLine(
          ServiceHeader, ServiceLine, ServiceItemLine
          , "Service Line Type"::" ", '', '', 0, 0
          , Description, 0);
    END;

    local procedure InsertServicePayoffLines(VAR ICISupportTicket: Record "ICI Support Ticket"; var ServiceHeader: Record "Service Header"; var ServiceItemLine: Record "Service Item Line"; var ServiceLine: Record "Service Line")
    var
        ICISupportTimeLogLine: Record "ICI Support Time Log Line";
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTimeLogComment: Record "ICI Support Time Log Comment";
        SupportPayoffMgt: Codeunit "ICI Support Payoff Mgt.";
        ServiceLineType: Enum "Service Line Type";
        LineDiscount: Decimal;
        NoOfTextLines: Integer;
        FreeLbl: Label '(Free Service without Charge)', Comment = 'de-DE=(Kulanz-Leistung ohne Berechnung)';
        ServiceAccountingNotSupportedErr: Label 'Service Accounting %1 is not supported', Comment = '%1=Accounting Type|de-DE=Serviceabrechung %1 ist nicht unterstützt.';
    begin
        // Sales Lines und erbrachte Leistungen anlegen

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Service Integration" THEN
            EXIT;

        // IF ICISupportSetup."Sales Payoff Type" = ICISupportSetup."Sales Payoff Type"::" " THEN
        //     EXIT;

        //Leistungszeilen einfügen
        ICISupportTimeLogLine.SetAutoCalcFields("Sales Payoff Type");
        ICISupportTimeLogLine.SetFilter("Sales Payoff Type", '<>%1', ICISupportTimeLogLine."Sales Payoff Type"::" ");

        ICISupportTimeLogLine.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTimeLogLine.SETFILTER("Accounting Type", ICISupportTimeLogLine.GetPayableAccountingTypeFilter());
        ICISupportTimeLogLine.SETRANGE(Cleared, FALSE);
        IF ICISupportTimeLogLine.FINDSET() THEN
            REPEAT

                NoOfTextLines := 0;

                ICISupportTimeLogLine.CalcFields("Discount %");
                LineDiscount := ICISupportTimeLogLine."Discount %";

                // Insert Time Log Line
                // TODO - XXX- maybe introduce Service Payoff Type -  Workaround is not so great
                // ICISupportTimeLogLine.CalcFields("Sales Payoff Type"); - Autocalcfields
                Case ICISupportTimeLogLine."Sales Payoff Type"
                OF
                    ICISupportTimeLogLine."Sales Payoff Type"::Item:
                        ServiceLineType := "Service Line Type"::Item;
                    ICISupportTimeLogLine."Sales Payoff Type"::"G/L Account":
                        ServiceLineType := "Service Line Type"::"G/L Account";
                    ICISupportTimeLogLine."Sales Payoff Type"::Resource:
                        ServiceLineType := "Service Line Type"::Resource;
                    else
                        ERROR(ServiceAccountingNotSupportedErr, ICISupportTimeLogLine."Accounting Type");
                End;

                InsertServLine(ServiceHeader, ServiceLine, ServiceItemLine, ServiceLineType, ICISupportTimeLogLine."Sales Payoff No.", '', ICISupportTimeLogLine."Time Decimal", 0, ICISupportTimeLogLine.Description, LineDiscount);

                ServiceLine."ICI Support Ticket No." := ICISupportTimeLogLine."Support Ticket No.";
                ServiceLine."ICI Time Log Line No." := ICISupportTimeLogLine."Line No.";
                ServiceLine.MODIFY();

                // Insert Time Log Line Comments
                ICISupportTimeLogComment.SetRange("Support Ticket No.", ICISupportTimeLogLine."Support Ticket No.");
                ICISupportTimeLogComment.SetRange("Time Log Line No.", ICISupportTimeLogLine."Line No.");
                IF ICISupportTimeLogComment.FINDSET() THEN
                    REPEAT
                        InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, ICISupportTimeLogComment.Description);
                    UNTIL ICISupportTimeLogComment.NEXT() = 0;


                //Kulanzzeile
                IF LineDiscount = 100 THEN BEGIN
                    InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, FreeLbl);
                    NoOfTextLines += 2;
                END;

                IF NoOfTextLines > 1 THEN
                    InsertTextServLine(ServiceHeader, ServiceLine, ServiceItemLine, '');

                SupportPayoffMgt.SetSettlement(ICISupportTimeLogLine, '', ServiceHeader."No.");

            UNTIL ICISupportTimeLogLine.NEXT() = 0;
    end;


    procedure ProcessReferenceNoBuffer(var ICIReferenceNoBuffer: Record "ICI Reference No. Buffer"; var ICISupportTicket: Record "ICI Support Ticket"; ReferenceNo: Code[50]): Boolean
    var
        Contact: Record Contact;
        ShipToAddress: Record "Ship-to Address";
        ReferenceNotFoundConfirmLbl: Label 'Reference No. %1 not found. Do you want to create the Ticket anyways?', Comment = '%1=Reference No.|de-DE=Referenznr. %1 nicht gefunden. Wollen Sie dennoch ein Ticket erstellen?';
        ReferenceNotFoundLbl: Label 'Reference No. %1 not found', Comment = '%1=Reference No.|de-DE=Referenznr. %1 nicht gefunden';
        ReferenceNoEmptyErr: Label 'Reference No must not be Empty', Comment = 'de-DE=Referenznr. darf nicht leer sein.';
    begin
        IF ICIReferenceNoBuffer."Reference No." <> '' then
            ReferenceNo := ICIReferenceNoBuffer."Reference No."; // Input Reference No

        // Error, if empty
        IF ReferenceNo = '' then
            ERROR(ReferenceNoEmptyErr);

        // Input Reference was not found
        IF ICIReferenceNoBuffer.Code = '' then begin
            IF ICISupportTicket."No." = '' then
                if NOT CONFIRM(StrSubstNo(ReferenceNotFoundConfirmLbl, ReferenceNo)) THEN
                    EXIT(False)
                else
                    CreateTicket(ICISupportTicket, StrSubstNo(ReferenceNotFoundLbl, ReferenceNo), '');
            ICISupportTicket.GET(ICISupportTicket."No.");
            ICISupportTicket."Reference No." := ReferenceNo; // Input Reference No - is not empty
            ICISupportTicket."Reference No. Style Expr" := 'Unfavorable';
            ICISupportTicket.Modify();
            EXIT(true);
        end;

        // Create Ticket if not exists
        IF ICISupportTicket."No." = '' then
            CreateTicket(ICISupportTicket, FORMAT(ICIReferenceNoBuffer."Document Search Type") + ' ' + ICIReferenceNoBuffer."Code", ICIReferenceNoBuffer."Customer No.");

        //ICISupportTicket.GET(ICISupportTicket."No.");
        Case ICIReferenceNoBuffer."Document Search Type" of
            ICIReferenceNoBuffer."Document Search Type"::Order:
                ICISupportTicket.Validate("Sales Order No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::Quote:
                ICISupportTicket.Validate("Sales Quote No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Pst.Invoice":
                ICISupportTicket.Validate("Pst. Sales Invoice No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Pst.Shipment":
                ICISupportTicket.Validate("Pst. Sales Shipment No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Service Item":
                ICISupportTicket.Validate("Service Item No.", ICIReferenceNoBuffer.Code); // Validates SerialNo and ItemNo 
            ICIReferenceNoBuffer."Document Search Type"::"Service Order":
                ICISupportTicket.Validate("Service Order No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Service Quote":
                ICISupportTicket.Validate("Service Quote No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Pst. Service Invoice":
                ICISupportTicket.Validate("Pst. Service Invoice No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Pst. Service Shipment":
                ICISupportTicket.Validate("Pst. Service Shipment No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Service Contract":
                ICISupportTicket.Validate("Service Contract No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Service Contract Quote":
                ICISupportTicket.Validate("Service Contract Quote No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Order Archive":
                ICISupportTicket.Validate("Sales Order Archive No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Quote Archive":
                ICISupportTicket.Validate("Sales Quote Archive No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Contact Address":
                begin
                    Contact.GET(ICIReferenceNoBuffer.Code);
                    IF Contact.Type = Contact.Type::Company then
                        ICISupportTicket.Validate("Company Contact No.", ICIReferenceNoBuffer.Code)
                    Else
                        ICISupportTicket.Validate("Current Contact No.", ICIReferenceNoBuffer.Code);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Customer Address":
                ICISupportTicket.Validate("Customer No.", ICIReferenceNoBuffer.Code);
            ICIReferenceNoBuffer."Document Search Type"::"Ship-To Address":
                begin
                    ShipToAddress.GET(ICIReferenceNoBuffer."Customer No.", ICIReferenceNoBuffer.Code);

                    ICISupportTicket.Validate("Customer No.", ShipToAddress."Customer No.");
                    IF ShipToAddress."E-Mail" <> '' then
                        ICISupportTicket.Validate("Contact E-Mail", ShipToAddress."E-Mail");
                end;

        end;

        ICISupportTicket."Reference No." := ICIReferenceNoBuffer."Reference No."; //Original ReferenceNo.
        ICISupportTicket."Reference No. Style Expr" := 'Favorable';
        ICISupportTicket.Modify(true);
        EXIT(true);
    end;

    procedure CreateTicket(var ICISupportTicket: Record "ICI Support Ticket"; pDescription: Text; CustomerNo: Code[20])
    begin
        ICISupportTicket.INIT();
        ICISupportTicket.Validate("No.", '');
        ICISupportTicket.INSERT(true);
        ICISupportTicket.Validate(Description, CopySTR(pDescription, 1, 250));
        ICISupportTicket.Validate("Customer No.", CustomerNo);
        ICISupportTicket.FillMissingContactOrCustomerInformation();
        ICISupportTicket.Modify(True);
    end;

    procedure CreateOpportunity(ICISupportTicket: Record "ICI Support Ticket")
    var
        Opportunity: Record Opportunity;
        ICISupportUser: Record "ICI Support User";
        OpenOpportunityLbl: Label 'Open new Opportunity %1?', Comment = 'de-DE=Möchten Sie die Verkaufschance %1 öffnen?';
    // OpportunityCard:Page 
    begin
        Opportunity.Init();
        Opportunity.Validate("No.", '');
        Opportunity.Insert(true);
        Opportunity.Validate(Description, ICISupportTicket.Description);
        Opportunity.Validate("ICI Support Ticket No.", ICISupportTicket."No.");
        Opportunity.Validate("Contact Company No.", ICISupportTicket."Company Contact No.");
        Opportunity.Validate("Contact No.", ICISupportTicket."Current Contact No.");

        ICISupportUser.GET(ICISupportTicket."Support User ID");
        IF ICISupportUser."Salesperson Code" <> '' then
            Opportunity.Validate("Salesperson Code", ICISupportUser."Salesperson Code");
        Opportunity.Modify(true);

        IF GuiAllowed then
            IF Confirm(OpenOpportunityLbl, true, Opportunity."No.") THEN
                PAGE.RUN(PAGE::"Opportunity Card", Opportunity);
    end;


    procedure SetHiddenFromWebarchiveShipment(var SalesShipmentHeader: Record "Sales Shipment Header"; SetToValue: Boolean)
    begin
        SalesShipmentHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        SalesShipmentHeader.Modify(false);
    end;

    procedure SetHiddenFromWebarchiveInvoice(var SalesInvoiceHeader: Record "Sales Invoice Header"; SetToValue: Boolean)
    begin
        SalesInvoiceHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        SalesInvoiceHeader.Modify(false);
    end;

    procedure SetHiddenFromWebarchiveCrMemo(var SalesCrMemoHeader: Record "Sales Cr.Memo Header"; SetToValue: Boolean)
    begin
        SalesCrMemoHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        SalesCrMemoHeader.Modify(false);
    end;

    procedure SetHiddenFromWebarchiveServiceShipment(var ServiceShipmentHeader: Record "Service Shipment Header"; SetToValue: Boolean)
    begin
        ServiceShipmentHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        ServiceShipmentHeader.Modify(false);
    end;

    procedure SetHiddenFromWebarchiveServiceInvoice(var ServiceInvoiceHeader: Record "Service Invoice Header"; SetToValue: Boolean)
    begin
        ServiceInvoiceHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        ServiceInvoiceHeader.Modify(false);
    end;

    procedure SetHiddenFromWebarchiveServiceCrMemo(var ServiceCrMemoHeader: Record "Service Cr.Memo Header"; SetToValue: Boolean)
    begin
        ServiceCrMemoHeader.Validate("ICI Hidden from Webarchive", SetToValue);
        ServiceCrMemoHeader.Modify(false);
    end;

    procedure GetFileNameForDocumentPDF(pRecordID: RecordID): Text[250]
    var
        SalesHeader: Record "Sales Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        ServiceHeader: Record "Service Header";
        ServiceShipmentHeader: Record "Service Shipment Header";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
        ServiceContractHeader: Record "Service Contract Header";
        lRecordRef: RecordRef;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        IF NOT lRecordRef.GET(pRecordID) THEN
            EXIT('');
        case pRecordID.TableNo of
            DATABASE::"Sales Header":
                begin
                    lRecordRef.SetTable(SalesHeader);
                    Exit(StrSubstNo(PDFNameLbl, SalesHeader."Document Type", SalesHeader."No."));
                end;
            DATABASE::"Sales Shipment Header":
                begin
                    lRecordRef.SetTable(SalesShipmentHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Lieferschein', SalesShipmentHeader."No."));
                end;
            DATABASE::"Sales Invoice Header":
                begin
                    lRecordRef.SetTable(SalesInvoiceHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Geb. Rechnung', SalesInvoiceHeader."No."));
                end;
            DATABASE::"Sales Cr.Memo Header":
                begin
                    lRecordRef.SetTable(SalesCrMemoHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Gutschrift', SalesCrMemoHeader."No."));
                end;
            DATABASE::"Service Header":
                begin
                    lRecordRef.SetTable(ServiceHeader);
                    EXIT(StrSubstNo(PDFNameLbl, ServiceHeader."Document Type", ServiceHeader."No."));
                end;
            DATABASE::"Service Shipment Header":
                begin
                    lRecordRef.SetTable(ServiceShipmentHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Servicelieferschein', ServiceShipmentHeader."No."));
                end;
            DATABASE::"Service Invoice Header":
                begin
                    lRecordRef.SetTable(ServiceInvoiceHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Geb. Servicerechnung', ServiceInvoiceHeader."No."));
                end;
            DATABASE::"Service Cr.Memo Header":
                begin
                    lRecordRef.SetTable(ServiceCrMemoHeader);
                    EXIT(StrSubstNo(PDFNameLbl, 'Geb. Servicegutschrift', ServiceCrMemoHeader."No."));
                end;
            DATABASE::"Service Contract Header":
                begin
                    lRecordRef.SetTable(ServiceContractHeader);
                    EXIT(StrSubstNo(PDFNameLbl, ServiceContractHeader."Contract Type", ServiceContractHeader."Contract No."));
                end;
        end;
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeServiceLineHeader(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; ServiceHeader: Record "Service Header"; var ServiceLine: Record "Service Line"; ServiceItemLine: Record "Service Item Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterServiceLineHeader(ICISupportTicket: Record "ICI Support Ticket"; ServiceHeader: Record "Service Header"; var ServiceLine: Record "Service Line"; ServiceItemLine: Record "Service Item Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeServiceLineFooter(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; ServiceHeader: Record "Service Header"; var ServiceLine: Record "Service Line"; ServiceItemLine: Record "Service Item Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterServiceLineFooter(ICISupportTicket: Record "ICI Support Ticket"; ServiceHeader: Record "Service Header"; var ServiceLine: Record "Service Line"; ServiceItemLine: Record "Service Item Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeSalesLineHeader(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterSalesLineHeader(ICISupportTicket: Record "ICI Support Ticket"; SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeSalesLineFooter(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterSalesLineFooter(ICISupportTicket: Record "ICI Support Ticket"; SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforePurchaseLineHeader(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterPurchaseLineHeader(ICISupportTicket: Record "ICI Support Ticket"; PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforePurchaseLineFooter(var IsHandled: Boolean; ICISupportTicket: Record "ICI Support Ticket"; PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterPurchaseLineFooter(ICISupportTicket: Record "ICI Support Ticket"; PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeSalesLineModify(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterInsertSalesLineWithItem(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; ICISupportTicketItem: Record "ICI Support Ticket Item");
    begin
    end;

    var
        NotInLicenseErr: Label 'Not in Licence', Comment = 'Nicht Lizenziert';
}
