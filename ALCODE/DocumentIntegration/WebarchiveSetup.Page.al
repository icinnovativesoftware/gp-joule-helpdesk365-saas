page 56324 "ICI Webarchive Setup"
{

    Caption = 'Support Webarchive Setup', Comment = 'de-DE=Support Belegarchiv Einrichtung';
    UsageCategory = Administration;
    ApplicationArea = All;
    PageType = Card;
    SourceTable = "ICI Webarchive Setup";

    layout
    {
        area(content)
        {
            group(SalesDocuments)
            {
                Caption = 'Sales', Comment = 'de-DE=Verkauf';
                Visible = DocumentIntegration;
                field("Webarchiv Sales Quote"; Rec."Webarchiv Sales Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Sales Order"; Rec."Webarchiv Sales Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Sales Invoice"; Rec."Webarchiv Sales Invoice")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Sales Credit Memo"; Rec."Webarchiv Sales Credit Memo")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Sales Blanket Order"; Rec."Webarchiv Sales Blanket Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Sales Return Order"; Rec."Webarchiv Sales Return Order")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webar. Posted Sales Shipment"; Rec."Webar. Posted Sales Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Posted Sales Invoice"; Rec."Webarchiv Posted Sales Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Posted Sales Cr.Memo"; Rec."Webarchiv Posted Sales Cr.Memo")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
            }
            group(ServiceDocuments)
            {
                Caption = 'Service', Comment = 'de-DE=Service';
                Visible = ServiceIntegration;
                field("Webarchiv Service Quote"; Rec."Webarchiv Service Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Service Order"; Rec."Webarchiv Service Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Service Invoice"; Rec."Webarchiv Service Invoice")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Service Credit Memo"; Rec."Webarchiv Service Credit Memo")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webar. Posted Service Shipment"; Rec."Webar. Posted Service Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webar. Posted Service Invoice"; Rec."Webar. Posted Service Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webar. Posted Service Cr.Memo"; Rec."Webar. Posted Service Cr.Memo")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webarchiv Service Contract"; Rec."Webarchiv Service Contract")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
                field("Webar. Service Contract Quote"; Rec."Webar. Service Contract Quote")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob die gewählte Belegart Kundenportal angezeigt wird.';
                }
            }
        }
    }
    var
        DocumentIntegration: Boolean;
        ServiceIntegration: Boolean;

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        Rec.Reset();
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert();
        end;
        IF ICISupportSetup.GET() THEN begin
            DocumentIntegration := ICISupportSetup."Document Integration";
            ServiceIntegration := ICISupportSetup."Service Integration";
        end;
    end;

}
