tableextension 56301 "ICI Opportunity" extends Opportunity
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'ICI Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                SupportTicket: Record "ICI Support Ticket";
            begin
                IF SupportTicket.GET("ICI Support Ticket No.") then
                    PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
            end;
        }
        field(56277; "ICI Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Opportunity No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56278; "ICI Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Opportunity No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56279; "ICI Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Opportunity No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56280; "ICI Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Opportunity No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
    }
    keys
    {
        key(Support; "ICI Support Ticket No.") { }
    }
}
