tableextension 56276 "ICI Sales Header" extends "Sales Header"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'ICI Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                SupportTicket: Record "ICI Support Ticket";
            begin
                IF SupportTicket.GET("ICI Support Ticket No.") then
                    PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
            end;
        }
        field(56277; "ICI Order Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Order No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56278; "ICI Order Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Order No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56279; "ICI Order Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Order No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56280; "ICI Order Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Order No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }

        field(56281; "ICI Quote Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Quote No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56282; "ICI Quote Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Quote No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56283; "ICI Quote Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Quote No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56284; "ICI Quote Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Sales Quote No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
        field(56285; "ICI Hidden from Webarchive"; Boolean)
        {
            Caption = 'ICI Hidden from Webarchive', Comment = 'de-DE=Im Belegarchiv ausblenden';
            DataClassification = CustomerContent;
        }
        field(56286; "ICI Ticket Process"; Code[20])
        {
            Caption = 'ICI Support Process', Comment = 'de-DE=Ticketprozess';
            TableRelation = "ICI Support Process";
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support Ticket"."Process Code" where("No." = field("ICI Support Ticket No.")));
            Editable = false;
        }
        field(56287; "ICI Ticket Process Stage"; Integer)
        {
            Caption = 'ICI Support Process Stage', Comment = 'de-DE=Ticketprozessstufe';
            TableRelation = "ICI Support Process";
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support Ticket"."Process Stage" where("No." = field("ICI Support Ticket No.")));
            Editable = false;
        }
        // field(56288; "ICI Ticket Process Stage Desc."; Text[100])
        // {
        //     Caption = 'ICI Ticket Process Stage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
        //     TableRelation = "ICI Support Process";
        //     FieldClass = FlowField;
        //     CalcFormula = lookup("ICI Support Ticket"."Process Stage Description" where("No." = field("ICI Support Ticket No.")));
        //     Editable = false;
        // }
    }
    keys
    {
        key(Support; "ICI Support Ticket No.") { }
    }
}
