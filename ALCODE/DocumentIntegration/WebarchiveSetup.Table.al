table 56310 "ICI Webarchive Setup"
{
    Caption = 'Support Webarchive Setup', Comment = 'de-DE=Support Belegarchiv Einrichtung';
    DataClassification = OrganizationIdentifiableInformation;


    fields
    {
        field(1; Code; Code[10])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = SystemMetadata;
        }
        field(95; "Webarchiv Sales Quote"; Boolean)
        {
            Caption = 'Sales Quote', Comment = 'de-DE=Angebot';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Quote" = false then
                    Contact.ModifyAll("ICI Sales Quote", False);
            end;
        }
        field(96; "Webarchiv Sales Order"; Boolean)
        {
            Caption = 'Sales Order', Comment = 'de-DE=Auftrag';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Order" = false then
                    Contact.ModifyAll("ICI Sales Order", False);
            end;
        }
        field(97; "Webarchiv Sales Invoice"; Boolean)
        {
            Caption = 'Sales Invoice', Comment = 'de-DE=Rechnung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Invoice" = false then
                    Contact.ModifyAll("ICI Sales Invoice", False);
            end;
        }
        field(98; "Webarchiv Sales Credit Memo"; Boolean)
        {
            Caption = 'Sales Credit Memo', Comment = 'de-DE=Gutschrift';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Credit Memo" = false then
                    Contact.ModifyAll("ICI Sales Credit Memo", False);
            end;
        }
        field(99; "Webarchiv Sales Blanket Order"; Boolean)
        {
            Caption = 'Sales Blanket Order', Comment = 'de-DE=Rahmenauftrag';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Blanket Order" = false then
                    Contact.ModifyAll("ICI Sales Blanket Order", False);
            end;
        }
        field(100; "Webarchiv Sales Return Order"; Boolean)
        {
            Caption = 'Sales Return Order', Comment = 'de-DE=Reklamation';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Sales Return Order" = false then
                    Contact.ModifyAll("ICI Sales Return Order", False);
            end;
        }
        field(101; "Webar. Posted Sales Shipment"; Boolean)
        {
            Caption = 'Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webar. Posted Sales Shipment" = false then
                    Contact.ModifyAll("ICI Posted Sales Shipment", False);
            end;
        }
        field(102; "Webarchiv Posted Sales Invoice"; Boolean)
        {
            Caption = 'Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Posted Sales Invoice" = false then
                    Contact.ModifyAll("ICI Posted Sales Invoice", False);
            end;
        }
        field(103; "Webarchiv Posted Sales Cr.Memo"; Boolean)
        {
            Caption = 'Posted Sales Credit Memo', Comment = 'de-DE=Geb. Gutschrift';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Posted Sales Cr.Memo" = false then
                    Contact.ModifyAll("ICI Posted Sales Cr.Memo", False);
            end;
        }
        field(104; "Webarchiv Service Quote"; Boolean)
        {
            Caption = 'Service Quote', Comment = 'de-DE=Angebot';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Service Quote" = false then
                    Contact.ModifyAll("ICI Service Quote", False);
            end;
        }
        field(105; "Webarchiv Service Order"; Boolean)
        {
            Caption = 'Service Order', Comment = 'de-DE=Auftrag';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Service Order" = false then
                    Contact.ModifyAll("ICI Service Order", False);
            end;
        }
        field(106; "Webarchiv Service Invoice"; Boolean)
        {
            Caption = 'Service Invoice', Comment = 'de-DE=Rechnung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Service Invoice" = false then
                    Contact.ModifyAll("ICI Service Invoice", False);
            end;
        }
        field(107; "Webarchiv Service Credit Memo"; Boolean)
        {
            Caption = 'Service Credit Memo', Comment = 'de-DE=Gutschrift';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Service Credit Memo" = false then
                    Contact.ModifyAll("ICI Service Credit Memo", False);
            end;
        }
        field(108; "Webar. Posted Service Shipment"; Boolean)
        {
            Caption = 'Posted Service Shipment', Comment = 'de-DE=Geb. Lieferung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webar. Posted Service Shipment" = false then
                    Contact.ModifyAll("ICI Posted Service Shipment", False);
            end;
        }
        field(109; "Webar. Posted Service Invoice"; Boolean)
        {
            Caption = 'Pstd. Serv. Invoice', Comment = 'de-DE=Geb. Rechnung';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webar. Posted Service Invoice" = false then
                    Contact.ModifyAll("ICI Posted Service Invoice", False);
            end;
        }
        field(110; "Webar. Posted Service Cr.Memo"; Boolean)
        {
            Caption = 'Posted Service Credit Memo', Comment = 'de-DE=Geb. Gutschrift';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webar. Posted Service Cr.Memo" = false then
                    Contact.ModifyAll("ICI Posted Service Cr.Memo", False);
            end;
        }
        field(111; "Webarchiv Service Contract"; Boolean)
        {
            Caption = 'Service Contract', Comment = 'de-DE=Vertrag';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webarchiv Service Contract" = false then
                    Contact.ModifyAll("ICI Service Contract", False);
            end;
        }
        field(112; "Webar. Service Contract Quote"; Boolean)
        {
            Caption = 'Service Contract Quote', Comment = 'de-DE=Vertragsangebot';
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                IF "Webar. Service Contract Quote" = false then
                    Contact.ModifyAll("ICI Service Contract Quote", False);
            end;
        }

    }
    keys
    {
        key(PK; "Code")
        {
            Clustered = true;
        }
    }

}
