tableextension 56281 "ICI Sales Shipment Line" extends "Sales Shipment Line"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'ICI Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                SupportTicket: Record "ICI Support Ticket";
            begin
                IF SupportTicket.GET("ICI Support Ticket No.") then
                    PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
            end;
        }
    }
    keys
    {
        key(Support; "ICI Support Ticket No.") { }
    }
}
