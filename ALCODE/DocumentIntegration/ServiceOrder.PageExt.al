pageextension 56279 "ICI Service Order" extends "Service Order"
{
    layout
    {
        addlast(content)
        {
            group(ICIHelpdesk365)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                }
                field("ICI Hidden from Webarchive"; Rec."ICI Hidden from Webarchive")
                {
                    Visible = ShowWebarchiv;
                    ApplicationArea = All;
                    ToolTip = 'Activate this Option, if you dont want this Document to be shown in the Webarchive', Comment = 'de-DE=Aktivieren Sie diese Option, um diesen Beleg im Belegarchiv auszublenden';
                }
                field("ICI Ticket Process"; Rec."ICI Ticket Process")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process', Comment = 'de-DE=Ticketprozess';
                }
                field("ICI Ticket Process Stage"; Rec."ICI Ticket Process Stage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Process Stage', Comment = 'de-DE=Ticketprozessstufe';
                }
                // field("ICI Ticket Process Stage Desc."; "ICI Ticket Process Stage Desc.")
                // {
                //     ApplicationArea = All;
                //     ToolTip = 'Ticket Processstage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
                // }
            }
        }
        addfirst(factboxes)
        {
            part("ICI Service Order Factbox"; "ICI Service Order Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "No." = field("No."), "Document Type" = field("Document Type");
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';

                action(AddPDFToTicket)
                {
                    Caption = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ToolTip = 'Add PDF to Ticket', Comment = 'de-DE=PDF an Ticket anhängen';
                    ApplicationArea = All;
                    Image = SendEmailPDF;
                    Visible = Rec."ICI Support Ticket No." <> '';
                    trigger OnAction()
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        Rec.TestField("ICI Support Ticket No.");
                        Rec.SetRecFilter();
                        ICISupportMailMgt.AttachServiceHeaderToTicket(Rec);
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                IF ICIWebarchiveSetup.ReadPermission THEN
                    IF ICIWebarchiveSetup.GET() then
                        ShowWebarchiv := ICISupportSetup."Portal Integration" AND ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Order";
    end;

    var
        ShowServiceFactbox: Boolean;
        ShowWebarchiv: Boolean;
}

