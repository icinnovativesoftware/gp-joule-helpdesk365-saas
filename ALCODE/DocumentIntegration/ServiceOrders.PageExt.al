pageextension 56280 "ICI Service Orders" extends "Service Orders"
{
    layout
    {
        addlast(Control1)
        {
            field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
            {
                ApplicationArea = All;
                ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';

                trigger OnDrillDown()
                var
                    SupportTicket: Record "ICI Support Ticket";
                begin
                    IF SupportTicket.GET(Rec."ICI Support Ticket No.") then
                        PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
                end;
            }
        }
        addfirst(factboxes)
        {
            part("ICI Service Order Factbox"; "ICI Service Order Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "No." = field("No."), "Document Type" = field("Document Type");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                ShowServiceFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowServiceFactbox: Boolean;
}
