pageextension 56315 "ICI Service Invoices" extends "Service Invoices"
{
    layout
    {
        addlast(Control1)
        {
            field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
            {
                ApplicationArea = All;
                ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';

                trigger OnDrillDown()
                var
                    SupportTicket: Record "ICI Support Ticket";
                begin
                    IF SupportTicket.GET(Rec."ICI Support Ticket No.") then
                        PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
                end;
            }
        }
    }
}
