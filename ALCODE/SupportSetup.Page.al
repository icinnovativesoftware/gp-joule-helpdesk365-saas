page 56276 "ICI Support Setup"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Support Setup', Comment = 'de-DE=HelpDesk Einrichtung';
    PageType = Card;
    SourceTable = "ICI Support Setup";

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("Support Ticket Nos."; Rec."Support Ticket Nos.")
                {
                    ApplicationArea = All;
                    Tooltip = 'Support Ticket Nos.', Comment = 'de-DE=Ticketnummernserie';
                    ShowMandatory = true;
                }
                field("Ticket Category Connection"; Rec."Ticket Category Connection")
                {
                    ApplicationArea = All;
                    Tooltip = 'Ticket Category Connection', Comment = 'de-DE=Gibt an, wie Sich die Kategorie 1 und 2 Felder in Support Tickets verhalten.';
                }
                field("First Supportuser Allocation"; Rec."First Supportuser Allocation")
                {
                    ApplicationArea = All;
                    Tooltip = 'First Supportuser Allocation', Comment = 'de-DE=Gibt an, welcher Supportbenutzer als Vorbeleung für neue Tickets herangezogen wird, nachdem die Suche in der Erstzuweisung erfolglos war';
                    Importance = Promoted;
                    ShowMandatory = true;
                }
                group(Mandatory)
                {
                    Caption = 'Mandatory', Comment = 'de-DE=Pflichtfelder';
                    field("Person Mandatory"; Rec."Person Mandatory")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Person Mandatory', Comment = 'de-DE=Gibt an, ob bei Tickets ein Personenkontakt erfasst werden muss';
                    }
                    field("Ticket Contact Creation"; rec."Ticket Contact Creation")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Allow to Insert Contact Data direkt in a Ticket without Creat a Person Contact', Comment = 'de-DE=Ermöglicht es Personenkontaktdaten direkt im Ticket zu erfassen, ohne einen Personenkontakt zu erzeugen.';

                    }
                    field("Message Mandatory"; Rec."Initial Message Mandatory")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Mandatory Message on Open', Comment = 'de-DE=Gibt an, ob beim eröffnen von Tickets ein Nachricht erfasst werden muss';
                    }
                }
                field("RMA-Report ID"; Rec."RMA-Report ID")
                {
                    ApplicationArea = All;
                    Tooltip = 'RMA-Report ID', Comment = 'de-DE=Gibt an, welcher Bericht für den RMA-Schein verwendet wird';
                }
                group(Defaults)
                {
                    field("Default Ticket Process"; Rec."Default Ticket Process")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Default Ticket Process', Comment = 'de-DE=Gibt an, welcher Ticketprozess als Vorbeleung für neue Tickets herangezogen wird';
                    }
                    field("Default Department Code"; Rec."Default Department Code")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Default Department Code', Comment = 'de-DE=Gibt an, welche Abteilung als Vorbeleung für neue Tickets herangezogen wird';
                    }

                }

                group(Integration)
                {
                    field("Document Integration"; Rec."Document Integration")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Document Integration', Comment = 'de-DE=Belegintegration';
                        Importance = Promoted;
                    }
                    field("Service Integration"; Rec."Service Integration")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Service Integration', Comment = 'de-DE=Serviceintegration';
                        Importance = Promoted;
                    }
                    field("Time Registration"; Rec."Time Registration")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Time Registration', Comment = 'de-DE=Zeiterfassung';
                        Importance = Promoted;
                    }
                    field("Task Integration"; Rec."Task Integration")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Task Integration', Comment = 'de-DE=Aufgabenintegration';
                        Importance = Promoted;
                    }
                }

                field("Send new File as Attachment"; Rec."Send new File as Attachment")
                {
                    ApplicationArea = All;
                    Tooltip = 'Send new File as Attachment', Comment = 'de-DE=Gibt an, ob Ticket-Dateien als Anhang versendet werden';
                }
                field("Daylight Savings Time"; Rec."Daylight Savings Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Daylight Savings Time', Comment = 'de-DE=Zeitumstellung berücksichtigen. Hinweis: Bei OnPrem Installationen muss dieser Wert nicht gesetzt werden, da die Zeitzone am Business Central Server gesetzt ist.';
                    Importance = Additional;
                }
                field("Communication - Default Intern"; Rec."Communication - Default Intern")
                {
                    ApplicationArea = All;
                    ToolTip = 'Communication default internally', Comment = 'de-DE=Gibt an, ob die Ticketkommunikation mit vorbelegung Interne Nachrichten absenden soll';
                    Importance = Promoted;
                }
                field("Show Filetypes as Image"; Rec."Show Filetypes as Image")
                {
                    ApplicationArea = All;
                    ToolTip = 'Show Filetypes as Image. Default Value: apng.avif.gif.jpg.jpeg.jfif.pjpeg.pjp.png.svg.webp.bmp.ico.cur.tif.tiff', Comment = 'de-DE=Liste der unterstützten Bildtypen (inklusive .). Diese werden im Portal in der Ticketkommunikation als Bild angezeigt. Vorbelegung: apng.avif.gif.jpg.jpeg.jfif.pjpeg.pjp.png.svg.webp.bmp.ico.cur.tif.tiff';
                    Importance = additional;
                    ShowMandatory = true;
                }
            }
            group(SalesService)
            {
                Caption = 'Sales & Service', Comment = 'de-DE=Verkaufs- und Serviceintegration';
                group(ServiceGroup)
                {
                    Caption = 'Service', Comment = 'de-DE=Serviceintegration';

                    field("Return Service Item"; Rec."Return Service Item")
                    {
                        ApplicationArea = All;
                        Tooltip = 'check this field, if a Return Order should include the Service Item', Comment = 'de-DE=Gibt an, ob ein Serviceartikel in eine VK-Reklamation übernommen wird';
                    }
                    field("Service Doc. w.o. Serviceitem"; Rec."Service Doc. w.o. Serviceitem")
                    {
                        ApplicationArea = All;
                        ToolTip = 'allows the creation of Service Documents without a Service Item', Comment = 'de-DE=erlaubt das Erzeugen von Servicedokumenten ohne Serviceartikel';
                    }
                }
                group(SalesGroup)
                {
                    Caption = 'Sales', Comment = 'de-DE=Verkaufsintegration';
                    field("Customer Template Code"; Rec."Customer Template Code")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Customer Template Code', Comment = 'de-DE=Debitorenvorlage';
                    }
                    field("Use Shipment in Ticket"; Rec."Use Shipment in Ticket")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Use Shipment in Ticket', Comment = 'de-DE=Gibt an, ob bei Ticket Lieferadressen erfasst werden können';
                    }
                    field("Ship-To Address modify"; Rec."Ship-To Address modify")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-To Address modify', Comment = 'de-DE=Gibt an, wie die neue Lieferadresse in den Serviceartikel eingetragen wird';
                    }
                }
            }
            group(TimeRegistration)
            {
                Caption = 'Time Registration', Comment = 'de-DE=Zeiterfassung und Abrechnung';
                Visible = Rec."Time Registration";
                group(payoff)
                {
                    Caption = 'Payoff', Comment = 'de-DE=Abrechnung';
                    field("Payoff Type"; Rec."Sales Payoff Type")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Payoff Type', Comment = 'de-DE=Abrechnungsart';
                        Importance = Promoted;
                        trigger OnValidate()
                        begin
                            CurrPage.Update()
                        end;
                    }
                    field("Payoff No."; Rec."Sales Payoff No.")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Payoff No.', Comment = 'de-DE=Abrechnungsnr.';
                        Importance = Promoted;
                        trigger OnValidate()
                        begin
                            CurrPage.Update()
                        end;
                    }
                }
            }
            group(Cues)
            {
                Caption = 'Cues', Comment = 'de-DE=Stapelindikatoren';

                group(CategoryTile1)
                {
                    Caption = 'CategoryTile 1', Comment = 'de-DE=Kategoriekachel 1';
                    field("Cue 1 - Category 1 Filter"; Rec."Cue 1 - Category 1 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 1 - Category 1 Code', Comment = 'de-DE=Kategoriefilter der Kachel';
                        Importance = Promoted;

                    }
                    field("Cue 1 - Category 2 Filter"; Rec."Cue 1 - Category 2 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 1 - Category 2 Code', Comment = 'de-DE=Unterkategoriefilter der Kachel';
                        Importance = Promoted;
                    }
                }
                group(CategoryTile2)
                {
                    Caption = 'CategoryTile 2', Comment = 'de-DE=Kategoriekachel 2';
                    field("Cue 2 - Category 1 Filter"; Rec."Cue 2 - Category 1 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 2 - Category 1 Code', Comment = 'de-DE=Kategoriefilter der Kachel';
                        Importance = Promoted;

                    }
                    field("Cue 2 - Category 2 Filter"; Rec."Cue 2 - Category 2 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 2 - Category 2 Code', Comment = 'de-DE=Unterkategoriefilter der Kachel';
                        Importance = Promoted;
                    }
                }
                group(CategoryTile3)
                {
                    Caption = 'CategoryTile 3', Comment = 'de-DE=Kategoriekachel 3';
                    field("Cue 3 - Category 1 Filter"; Rec."Cue 3 - Category 1 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 3 - Category 1 Code', Comment = 'de-DE=Kategoriefilter der Kachel';
                        Importance = Additional;

                    }
                    field("Cue 3 - Category 2 Filter"; Rec."Cue 3 - Category 2 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 3 - Category 2 Code', Comment = 'de-DE=Unterkategoriefilter der Kachel';
                        Importance = Additional;
                    }
                }
                group(CategoryTile4)
                {
                    Caption = 'CategoryTile 4', Comment = 'de-DE=Kategoriekachel 4';
                    field("Cue 4 - Category 1 Filter"; Rec."Cue 4 - Category 1 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 4 - Category 1 Code', Comment = 'de-DE=Kategoriefilter der Kachel';
                        Importance = Additional;

                    }
                    field("Cue 4 - Category 2 Filter"; Rec."Cue 4 - Category 2 Filter")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 4 - Category 2 Code', Comment = 'de-DE=Unterkategoriefilter der Kachel';
                        Importance = Additional;
                    }
                }
                group(ProcessTile1)
                {
                    Caption = 'ProcessTile 1', Comment = 'de-DE=Prozesskachel 1';
                    field("Cue 1 - Process Code"; Rec."Cue 1 - Process Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 1 - Process Code', Comment = 'de-DE=Prozessfilter der Kachel';
                        Importance = Promoted;
                    }
                    field("Cue 1 - Process Stage"; Rec."Cue 1 - Process Stage")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 1 - Process Stage', Comment = 'de-DE=Prozessstufenfilter der Kachel';
                        Importance = Promoted;
                    }
                }
                group(ProcessTile2)
                {
                    Caption = 'ProcessTile 2', Comment = 'de-DE=Prozesskachel 2';
                    field("Cue 2 - Process Code"; Rec."Cue 2 - Process Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 2 - Process Code', Comment = 'de-DE=Prozessfilter der Kachel';
                        Importance = Promoted;
                    }
                    field("Cue 2 - Process Stage"; Rec."Cue 2 - Process Stage")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 2 - Process Stage', Comment = 'de-DE=Prozessstufenfilter der Kachel';
                        Importance = Promoted;
                    }
                }
                group(ProcessTile3)
                {
                    Caption = 'ProcessTile 3', Comment = 'de-DE=Prozesskachel 3';
                    field("Cue 3 - Process Code"; Rec."Cue 3 - Process Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 3 - Process Code', Comment = 'de-DE=Prozessfilter der Kachel';
                        Importance = Additional;
                    }
                    field("Cue 3 - Process Stage"; Rec."Cue 3 - Process Stage")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 3 - Process Stage', Comment = 'de-DE=Prozessstufenfilter der Kachel';
                        Importance = Additional;
                    }
                }
                group(ProcessTile4)
                {
                    Caption = 'ProcessTile 4', Comment = 'de-DE=Prozesskachel 4';
                    field("Cue 4 - Process Code"; Rec."Cue 4 - Process Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 4 - Process Code', Comment = 'de-DE=Prozessfilter der Kachel';
                        Importance = Additional;
                    }
                    field("Cue 4 - Process Stage"; Rec."Cue 4 - Process Stage")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Cue 4 - Process Stage', Comment = 'de-DE=Prozessstufenfilter der Kachel';
                        Importance = Additional;
                    }
                }
                field("Cue Indicator Low"; Rec."Cue Indicator Low")
                {
                    ApplicationArea = All;
                    ToolTip = 'Cue Indicator Low', Comment = 'de-DE=Werte unterhalb dieser Grenze werden als Grün markiert. Werte zwischen unterer und oberer Grenze werden gelb markiert';
                    Importance = Additional;
                }
                field("Cue Indicator High"; Rec."Cue Indicator High")
                {
                    ApplicationArea = All;
                    ToolTip = 'Cue Indicator High', Comment = 'de-DE=Werte oberhalb dieser Grenze werden als rot markiert. Werte zwischen unterer und oberer Grenze werden gelb markiert';
                    Importance = Additional;
                }
            }

            group(Crm)
            {
                Visible = Rec."Task Integration";
                Caption = 'CRM', Comment = 'de-DE=CRM & Aufgabenverwaltung';
                group(activities)
                {
                    Caption = 'Activitiy Templates', Comment = 'de-DE=Aktivitätenvorlagen';
                    field("Closing Interaction Template"; Rec."Closing Interaction Template")
                    {
                        ApplicationArea = All;
                        Caption = 'Close Ticket', Comment = 'de-DE=Ticket schließen';
                        ToolTip = 'Ticket Closing Interaction Template', Comment = 'de-DE=Ticket Aktivitätenvorlage';
                    }
                    field("Letter Interaction Template"; Rec."Letter Interaction Template")
                    {
                        ApplicationArea = All;
                        Caption = 'Letter Template', Comment = 'de-DE=Briefvorlage';
                        ToolTip = 'Letter Interaction Template', Comment = 'de-DE=Brief Aktivitätenvorlage';
                    }
                    field("Meeting Interaction Template"; Rec."Meeting Interaction Template")
                    {
                        ApplicationArea = All;
                        Caption = 'Meeting Template', Comment = 'de-DE=Terminvorlage';
                        ToolTip = 'Meeting Interaction Template', Comment = 'de-DE=Termin Aktivitätenvorlage';
                    }
                    field("Phone Interaction Template"; Rec."Phone Interaction Template")
                    {
                        ApplicationArea = All;
                        Caption = 'Phone Templates', Comment = 'de-DE=Telefonatvorlage';
                        ToolTip = 'Phone Interaction Template', Comment = 'de-DE=Telefonat Aktivitätenvorlage';
                    }
                }
            }
            group(Layouts)
            {
                Caption = 'Layout', Comment = 'de-DE=Farbkonfiguration';
                group(Chat)
                {

                    Caption = 'Chat', Comment = 'de-DE=Ticketkommunikation';
                    field("Color - Contact Message"; Rec."Color - Contact Message")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Color - Contact Message', Comment = 'de-DE=Farbe der Kontaktnachrichten in der Ticketkommunikation';
                    }

                    field("Color - User Message"; Rec."Color - User Message")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Color - User Message', Comment = 'de-DE=Farbe der Benutzernachrichten in der Ticketkommunikation';
                    }
                    field("Color - Internal Message"; Rec."Color - Internal Message")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Color - Internal Message', Comment = 'de-DE=Farbe der internen Nachrichten in der Ticketkommunikation';
                    }
                }
                group(Ticketboard)
                {
                    Visible = Rec."Ticketboard active";
                    Caption = 'Ticketboard', Comment = 'de-DE=Ticketboard';
                    field("Ticketboard - Base Color"; Rec."Ticketboard - Base Color")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ticketboard - Base Color', Comment = 'de-DE=CSS Farbcode für Ticketboard';
                    }
                }
            }

        }
    }

    actions
    {
        area(Processing)
        {

            group(setup)
            {
                ToolTip = 'Base Setup', Comment = 'de-DE=Grundeinrichtung';
                Caption = 'Base Setup', Comment = 'de-DE=Grundeinrichtung';
                Image = Setup;

                action(Init)
                {
                    ApplicationArea = All;
                    Image = QuestionaireSetup;
                    ToolTip = 'Initializes Default Values', Comment = 'de-DE=Einrichtung erzeugen';
                    Caption = 'Initializes Default Values', Comment = 'de-DE=Einrichtung erzeugen';
                    trigger OnAction()
                    var
                        InstallMgt: Codeunit "ICI Install Mgt.";
                    begin
                        InstallMgt.InitSupportModuleDefaults();
                        CurrPage.Update(FALSE);
                    end;
                }
                action(InitService)
                {
                    ApplicationArea = Advanced;
                    Image = ServiceSetup;
                    ToolTip = 'Initializes Default Values for Service Mgt.', Comment = 'de-DE=Std. Serviceeinrichtung erzeugen';
                    Caption = 'Initializes Default Valuesfor Service Mgt.', Comment = 'de-DE=Std. Serviceeinrichtung erzeugen';
                    Visible = Rec."Service Integration";
                    trigger OnAction()
                    var
                        InstallMgt: Codeunit "ICI Install Mgt.";
                    begin
                        InstallMgt.InitService();
                        CurrPage.Update(FALSE);
                    end;
                }
                action(CTIInit)
                {
                    ApplicationArea = All;
                    Image = Calls;
                    ToolTip = 'Initializes CTI Phone Numbers', Comment = 'de-DE=CTI Einrichtung erzeugen';
                    Caption = 'Initializes CTI Phone Numbers', Comment = 'de-DE=CTI Einrichtung erzeugen';
                    trigger OnAction()
                    var
                        CTIMgt: Codeunit "ICI CTI Management";
                        DoneLbl: Label 'Done', Comment = 'de-DE=Fertig';
                    begin
                        CTIMgt.InitContact(true);
                        Message(DoneLbl);
                    end;
                }
                action(CTIUpdate)
                {
                    ApplicationArea = All;
                    Image = Calls;
                    ToolTip = 'Updates CTI Phone Numbers', Comment = 'de-DE=CTI Einrichtung aktualisieren';
                    Caption = 'Updates CTI Phone Numbers', Comment = 'de-DE=CTI Einrichtung aktualisieren';
                    trigger OnAction()
                    var
                        CTIMgt: Codeunit "ICI CTI Management";
                        DoneLbl: Label 'Done', Comment = 'de-DE=Fertig';
                    begin
                        CTIMgt.InitContact(false);
                        Message(DoneLbl);
                    end;
                }
            }

            group(Functions)
            {
                action(TicketPayoff)
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Payoff', Comment = 'de-DE=Tickets abrechnen';
                    Caption = 'Ticket Payoff', Comment = 'de-DE=Tickets abrechnen';
                    Image = CreateDocuments;
                    RunObject = report "ICI Support Ticket Payoff";
                    Visible = Rec."Time Registration";
                }
                action(Escalation)
                {
                    ApplicationArea = All;
                    ToolTip = 'Escalation', Comment = 'de-DE=Eskalation ausführen';
                    Caption = 'Escalation', Comment = 'de-DE=Eskalation ausführen';
                    Image = Timesheet;
                    trigger OnAction()
                    var
                        ICIEscalationMgt: Codeunit "ICI Escalation Mgt.";
                    begin
                        ICIEscalationMgt.EscalationTimer();
                    end;
                }

            }

        }
        area(Navigation)
        {
            action(PortalSetup)
            {
                Caption = 'Portal Setup', Comment = 'de-DE=Portaleinrichtung';
                ToolTip = 'RMA-Layouts per Language', Comment = 'de-DE=Konfigurieren Sie Ihr NAVonline HelpDesk Kundenportal';
                ApplicationArea = All;
                Image = Setup;
                RunObject = page "ICI support portal setup";
            }
            action(License)
            {
                Caption = 'HelpDesk License', Comment = 'de-DE=Lizenz';
                ApplicationArea = All;
                Image = EncryptionKeys;
                RunObject = page "ICI support license";
            }
            action(RMALayouts)
            {
                Caption = 'RMA-Layouts per Language', Comment = 'de-DE=RMA-Berichtslayout nach Sprache';
                ToolTip = 'RMA-Layouts per Language', Comment = 'de-DE=RMA-Berichtslayout nach Sprache';
                ApplicationArea = All;
                Image = Language;
                Visible = Rec."RMA-Report ID" <> 0;
                RunObject = page "ICI Languages";
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        if not ICISupportSetup.GET() THEN
            ICISupportSetup.INSERT(true);
    end;

}
