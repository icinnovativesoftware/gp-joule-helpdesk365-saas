report 56277 "ICI RMA Bill"
{
    ApplicationArea = All;
    Caption = 'RMA Bill', Comment = 'de-DE=RMA-Formular';
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = Word;
    WordLayout = 'ALCode/Reporting/RMA-Bill.docx';
    dataset
    {
        dataitem(ICISupportTicket; "ICI Support Ticket")
        {
            column(Category1Code; "Category 1 Code") { }
            column(Category1Description; "Category 1 Description") { }
            column(Category2Code; "Category 2 Code") { }
            column(Category2Description; "Category 2 Description") { }
            column(Description; Description) { }
            column(CustomerName; "Cust. Name") { }
            column(CustomerNo; "Customer No.") { }
            column(ItemDescription; "Item Description") { }
            column(ItemNo; "Item No.") { }
            column(No; "No.") { }
            column(NoBarcode128; NoBarcode128) { }
            column(NoBarcode39; NoBarcode39) { }
            column(NoBarcode93; NoBarcode93) { }
            column(PstSalesInvoiceNo; "Pst. Sales Invoice No.") { }
            column(PstSalesShipmentNo; "Pst. Sales Shipment No.") { }
            column(PstServiceInvoiceNo; "Pst. Service Invoice No.") { }
            column(PstServiceShipmentNo; "Pst. Service Shipment No.") { }
            column(SerialNo; "Serial No.") { }
            column(ServiceItemDescription; "Service Item Description") { }
            column(ServiceItemNo; "Service Item No.") { }
            column(SupportUserName; "Support User Name") { }
            column(CompanyContactName; "Comp. Contact Name") { }
            column(CompanyContactNo; "Company Contact No.") { }
            column(ContactMobilePhoneNo; "Contact Mobile Phone No.") { }
            column(ContactPhoneNo; "Contact Phone No.") { }
            column(CurrentContactName; "Curr. Contact Name") { }
            column(CurrentContactNo; "Current Contact No.") { }
            column(FirstCustomerMessage; FirstCustomerMessage) { }
            dataitem(CompanyInformation; "Company Information")
            {
                column(CompanyInformationAddress; CompanyInformation.Address) { }
                column(CompanyInformationPicture; CompanyInformation.Picture) { }
                column(CompanyInformationName; CompanyInformation.Name) { }
                column(CompanyInformationPostCode; CompanyInformation."Post Code") { }
                column(CompanyInformationCity; CompanyInformation."City") { }
                column(CompanyInformationCounty; CompanyInformation."County") { }

                column(CompanyInformationCountryRegionCode; CompanyInformation."Country/Region Code") { }
                dataitem(CountryRegion; "Country/Region")
                {
                    DataItemLink = Code = field("Country/Region Code");
                    column(CompanyInformationCountryRegionName; CountryRegion.Name) { }
                }
                trigger OnAfterGetRecord()
                begin
                    Calcfields(Picture);
                end;
            }
            dataitem(Contact; Contact)
            {
                DataItemLink = "No." = field("Current Contact No.");
                column(ContactEMail; "E-Mail") { }
            }
            dataitem(ServiceItem; "Service Item")
            {
                DataItemLink = "No." = field("Service Item No.");
                column(LocationofServiceItem; "Location of Service Item") { }
                column(ServiceItemGroupCode; "Service Item Group Code") { }
                column(ServiceItemShipToName; ServiceItemShipToName) { }
                column(ServiceItemShipToAddress; ServiceItemShipToAddress) { }

                column(ServiceItemShipToAddress2; ServiceItemShipToAddress2) { }
                column(ServiceItemShipToPostCode; ServiceItemShipToPostCode) { }
                column(ServiceItemShipToCity; ServiceItemShipToCity) { }
                column(ServiceItemShipToCounty; ServiceItemShipToCounty) { }
                column(ServiceItemShipToPhoneNo; ServiceItemShipToPhoneNo) { }
                column(ServiceItemShipToContact; ServiceItemShipToContact) { }


                trigger OnAfterGetRecord() // Service Item
                begin
                    // Prio 1: Lieferadresse aus Ticket
                    // Falls Leer ->
                    // Prio 2: Lieferadresse aus Serviceartikel
                    IF ((ICISupportTicket."Ship-to Name" <> '') OR (ICISupportTicket."Ship-to Address" <> '') OR (ICISupportTicket."Ship-to City" <> '') OR (ICISupportTicket."Ship-to Post Code" <> '')) THEN BEGIN
                        ServiceItemShipToName := ICISupportTicket."Ship-to Name";
                        ServiceItemShipToAddress := ICISupportTicket."Ship-to Address";
                        ServiceItemShipToAddress := ICISupportTicket."Ship-to Address 2";
                        ServiceItemShipToPostCode := ICISupportTicket."Ship-to Post Code";
                        ServiceItemShipToCity := ICISupportTicket."Ship-to City";
                        ServiceItemShipToCounty := ICISupportTicket."Ship-to County";
                        ServiceItemShipToPhoneNo := ICISupportTicket."Ship-to Phone No.";
                        ServiceItemShipToContact := ICISupportTicket."Ship-to Contact";
                    END ELSE
                        IF "Ship-to Code" = '' THEN BEGIN
                            CALCFIELDS(
                              Name, "Name 2", Address, "Address 2", "Post Code", City,
                              County, "Country/Region Code", Contact, "Phone No.");

                            ServiceItemShipToName := Name;
                            ServiceItemShipToAddress := Address;
                            ServiceItemShipToAddress2 := "Address 2";
                            ServiceItemShipToPostCode := "Post Code";
                            ServiceItemShipToCity := City;
                            ServiceItemShipToCounty := County;
                            ServiceItemShipToPhoneNo := "Phone No.";
                            ServiceItemShipToContact := Contact;
                        END ELSE begin
                            CALCFIELDS(
                                "Ship-to Name", "Ship-to Name 2", "Ship-to Address", "Ship-to Address 2", "Ship-to Post Code",
                                "Ship-to City", "Ship-to County", "Ship-to Country/Region Code", "Ship-to Contact", "Ship-to Phone No.");
                            ServiceItemShipToName := "Ship-to Name";
                            ServiceItemShipToAddress := "Ship-to Address";
                            ServiceItemShipToAddress := "Ship-to Address 2";
                            ServiceItemShipToPostCode := "Ship-to Post Code";
                            ServiceItemShipToCity := "Ship-to City";
                            ServiceItemShipToCounty := "Ship-to County";
                            ServiceItemShipToPhoneNo := "Ship-to Phone No.";
                            ServiceItemShipToContact := "Ship-to Contact";
                        end;


                end;
            }
            // This should work: https://docs.microsoft.com/en-us/dynamics365/business-central/dev-itpro/developer/devenv-report-add-barcodes
            trigger OnAfterGetRecord() // Ticket Table
            var
                ICISupportTicketLog: Record "ICI Support Ticket Log";
                lInStream: InStream;
                Code39Lbl: Label '*%1*', Comment = '%1 = Code';
            // IBarcodeFontProvider: Interface "Barcode Font Provider";
            begin
                // IBarcodeFontProvider := Enum::"Barcode Font Provider"::IDAutomation1D;

                // NoBarcode128 := IBarcodeFontProvider.EncodeFont("No.", Enum::"Barcode Symbology"::Code128);
                // NoBarcode39 := IBarcodeFontProvider.EncodeFont("No.", Enum::"Barcode Symbology"::Code39);
                // NoBarcode93 := IBarcodeFontProvider.EncodeFont("No.", Enum::"Barcode Symbology"::Code93);

                NoBarcode39 := StrSubstNo(Code39Lbl, "No.");

                // Erste Kundennachricht
                ICISupportTicketLog.SetRange("Support Ticket No.", ICISupportTicket."No.");
                ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External Message");
                IF ICISupportTicketLog.FindFirst() THEN begin
                    ICISupportTicketLog.CalcFields(Data);
                    ICISupportTicketLog.Data.CreateInStream(lInStream);
                    lInStream.Read(FirstCustomerMessage); // Readtext ließt nur eine Zeile
                end;
            end;
        }
    }
    var
        NoBarcode128: Text;
        NoBarcode39: Text;
        NoBarcode93: Text;

        ServiceItemShipToAddress: Text[100];
        ServiceItemShipToAddress2: Text[50];
        ServiceItemShipToName: Text[100];
        ServiceItemShipToPostCode: Code[20];
        ServiceItemShipToCity: Text[30];
        ServiceItemShipToContact: Text[100];
        ServiceItemShipToCounty: Text[30];
        ServiceItemShipToPhoneNo: Text[30];
        FirstCustomerMessage: Text;
}
