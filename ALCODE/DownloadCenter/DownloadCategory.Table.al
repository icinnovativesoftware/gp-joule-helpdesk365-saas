table 56312 "ICI Download Category"
{
    Caption = 'Download Category', Comment = 'de-DE=Downloadkategorie';
    DataClassification = CustomerContent;
    DrillDownPageId = "ICI Download Categories";
    LookupPageId = "ICI Download Categories";

    fields
    {
        field(1; "Code"; Code[30])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; Layer; Integer)
        {
            Caption = 'Layer', Comment = 'de-DE=Ebene';
            MinValue = 0;
            MaxValue = 2;
            DataClassification = CustomerContent;
        }
        field(12; "Parent Category"; Code[30])
        {
            Caption = 'Parent Category', Comment = 'de-DE=Gehört zu Downloadkategorie';
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                ICIDownloadCategory: Record "ICI Download Category";
            begin
                ICIDownloadCategory.GET("Parent Category");
                Layer := ICIDownloadCategory.Layer + 1;
            end;
        }
        field(13; Inactive; Boolean)
        {
            Caption = 'Inactive', Comment = 'de-DE=Inaktiv';
            DataClassification = CustomerContent;
        }
        field(14; "No. of Files"; Integer)
        {
            Caption = 'No. of Files', Comment = 'de-DE=Anzahl Dateien';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Count("ICI Sup. Dragbox File" where("ICI Download Category Code" = field(Code)));
        }
        field(15; Online; Boolean)
        {
            Caption = 'Online', Comment = 'de-DE=Online';
            DataClassification = CustomerContent;
            InitValue = true;
        }
    }
    keys
    {
        key(PK; "Code")
        {
            Clustered = true;
        }
        key(sorting; Layer, "Parent Category", Code) { }
    }

    procedure GetNoOfChildrenFiles(): Integer
    var
        ICIDownloadCategory: Record "ICI Download Category";
        ICIDownloadCategory2: Record "ICI Download Category";
        NoOfFilesAcc: Integer;
    begin
        IF Layer = 2 THEN begin
            CalcFields("No. of Files");
            EXIT("No. of Files");
        end;
        IF Layer = 1 THEN begin
            ICIDownloadCategory.SetRange("Parent Category", Code);
            ICIDownloadCategory.SetAutoCalcFields("No. of Files");
            IF ICIDownloadCategory.FindSet() THEN
                repeat
                    NoOfFilesAcc += ICIDownloadCategory."No. of Files";
                until ICIDownloadCategory.Next() = 0;
            EXIT(NoOfFilesAcc);
        end;
        // Layer = 0
        ICIDownloadCategory.SetRange("Parent Category", Code);
        ICIDownloadCategory.SetAutoCalcFields("No. of Files");
        IF ICIDownloadCategory.FindSet() THEN
            repeat
                ICIDownloadCategory2.SetRange("Parent Category", ICIDownloadCategory.Code);
                ICIDownloadCategory2.SetAutoCalcFields("No. of Files");
                IF ICIDownloadCategory2.FindSet() THEN
                    repeat
                        NoOfFilesAcc += ICIDownloadCategory2."No. of Files";
                    until ICIDownloadCategory2.Next() = 0;
                NoOfFilesAcc += ICIDownloadCategory."No. of Files";
            until ICIDownloadCategory.Next() = 0;
        EXIT(NoOfFilesAcc);
    end;

    procedure GetLayerCodes(var CategoryLayer0: Text; var CategoryLayer1: Text; var CategoryLayer2: Text)
    var
        lICIDownloadCategory: Record "ICI Download Category";
    begin
        Case Rec.Layer OF
            0:
                begin
                    CategoryLayer0 := Rec.Code;
                    CategoryLayer1 := '';
                    CategoryLayer2 := '';
                end;
            1:
                begin
                    CategoryLayer0 := Rec."Parent Category";
                    CategoryLayer1 := Rec.Code;
                    CategoryLayer2 := '';
                end;
            2:
                begin
                    lICIDownloadCategory.Get(Rec."Parent Category");
                    CategoryLayer0 := lICIDownloadCategory."Parent Category"; // Parent Cagetory der Parent Category
                    CategoryLayer1 := Rec."Parent Category";
                    CategoryLayer2 := Rec.Code;
                end;
        END;
    end;

    procedure GetChildrenFilter() CategoryFilter: Text;
    var
        ICIDownloadCategory: Record "ICI Download Category";
        ICIDownloadCategory2: Record "ICI Download Category";
    begin
        CategoryFilter := Rec.Code;
        IF Layer = 2 THEN
            EXIT;

        IF Layer = 1 THEN begin
            ICIDownloadCategory.SetRange("Parent Category", Code);
            IF ICIDownloadCategory.FindSet() then
                repeat
                    CategoryFilter += '|' + ICIDownloadCategory.Code;
                until ICIDownloadCategory.Next() = 0;
            EXIT;
        end;
        // Layer = 0
        ICIDownloadCategory.SetRange("Parent Category", Code);
        IF ICIDownloadCategory.FindSet() THEN
            repeat
                ICIDownloadCategory2.SetRange("Parent Category", ICIDownloadCategory.Code);
                IF ICIDownloadCategory2.FindSet() THEN
                    repeat
                        CategoryFilter += '|' + ICIDownloadCategory2.Code;
                    until ICIDownloadCategory2.Next() = 0;
                CategoryFilter += '|' + ICIDownloadCategory.Code;
            until ICIDownloadCategory.Next() = 0;
    end;
}
