page 56364 "ICI Download Categories"
{

    ApplicationArea = All;
    Caption = 'Download Categories', Comment = 'de-DE=Downloadcenter';
    PageType = List;
    SourceTable = "ICI Download Category";
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                IndentationColumn = Indent;
                IndentationControls = Code;
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Parent Category"; Rec."Parent Category")
                {
                    Visible = ShowMoreColumns;
                    ApplicationArea = All;
                    ToolTip = 'Parent Category', Comment = 'de-DE=Gehört zu Kategorie';
                }
                field(Layer; Rec.Layer)
                {
                    Visible = ShowMoreColumns;
                    ApplicationArea = All;
                    ToolTip = 'Layer', Comment = 'de-DE=Ebene';
                }
                field(Inactive; Rec.Inactive)
                {
                    Visible = ShowMoreColumns;
                    ApplicationArea = All;
                    ToolTip = 'Inactive', Comment = 'de-DE=Inaktiv';
                }
                field("No. of Files"; Rec."No. of Files")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of Files', Comment = 'de-DE=Anzahl Dateien';
                    BlankZero = true;
                }
                field(Online; Rec.Online)
                {
                    ApplicationArea = All;
                    ToolTip = 'Online', Comment = 'de-DE=Online';
                }
            }
            part("ICI Sup. Dragbox Files Subpage"; "ICI Sup. Dragbox Files Subpage")
            {
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
        area(FactBoxes)
        {
            part("ICI Download Category Drabgox"; "ICI Download Category Drabgox")
            {
                SubPageLink = "Code" = field("Code");
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
    }
    actions
    {
        area(Creation)
        {
            action(ShowMore)
            {
                Visible = Not ShowMoreColumns;
                Caption = 'Show More', Comment = 'de-DE=Mehr Spalten Anzeigen';
                ToolTip = 'Show More', Comment = 'de-DE=Mehr Spalten Anzeigen';
                Image = SetupColumns;
                Promoted = true;
                PromotedIsBig = true;
                PromotedOnly = true;

                ApplicationArea = All;

                trigger OnAction()
                begin
                    ShowMoreColumns := true;
                    CurrPage.Update();
                end;
            }
            action(ShowLess)
            {
                Visible = ShowMoreColumns;
                Caption = 'Show Less', Comment = 'de-DE=Weniger Spalten Anzeigen';
                ToolTip = 'Show Less', Comment = 'de-DE=Weniger Spalten Anzeigen';
                Image = SetupList;
                Promoted = true;
                PromotedIsBig = true;
                PromotedOnly = true;

                ApplicationArea = All;
                trigger OnAction()
                begin
                    ShowMoreColumns := false;
                    CurrPage.Update();

                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        Indent := Rec.Layer;
    end;

    trigger OnAfterGetCurrRecord()
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
    begin
        ICISupDragboxFile.SetRange("Record ID", Rec.RecordId());
        CurrPage."ICI Sup. Dragbox Files Subpage".Page.SetTableView(ICISupDragboxFile);
        CurrPage."ICI Sup. Dragbox Files Subpage".Page.Editable(true);
        CurrPage."ICI Sup. Dragbox Files Subpage".Page.Update();
    end;


    var
        Indent: Integer;
        ShowMoreColumns: Boolean;

}
