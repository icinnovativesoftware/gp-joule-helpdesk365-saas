page 56283 "ICI Support Ticket Log ListP"
{

    Caption = 'ICI Ticket Log List', Comment = 'de-DE=Ticketprotokoll';
    PageType = List;
    SourceTable = "ICI Support Ticket Log";
    UsageCategory = None;
    Editable = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field(LogDescription; LogDescription)
                {
                    ApplicationArea = All;
                    ToolTip = 'LogDescription';
                    Caption = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Created By Type"; Rec."Created By Type")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Created By Type', Comment = 'de-DE=Erstellt von Art';
                }
                field("Created By"; Rec."Created By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Created By', Comment = 'de-DE=Erstellt von';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Date', Comment = 'de-DE=Erstellt am';
                }
                field("Creation Time"; Rec."Creation Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Time', Comment = 'de-DE=Erstellt um';
                }
                field("Entry No."; Rec."Entry No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
                }
                field(DataText; DataText)
                {
                    ApplicationArea = All;
                    ToolTip = 'DataText', Comment = 'de-DE=Daten';
                    Caption = 'DataText', Comment = 'de-DE=Daten';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            // action(XMLDebug)
            // {
            //     ToolTip = 'XMLDebug';
            //     Image = Debug;
            //     ApplicationArea = All;

            //     trigger OnAction()
            //     var
            //         ICISupportTicketLog: Record "ICI Support Ticket Log";
            //         ICISupportFilePort: XmlPort "ICI Support File Port";
            //     begin
            //         ICISupportTicketLog.SetRange("Entry No.", Rec."Entry No.");
            //         ICISupportFilePort.SetTableView(ICISupportTicketLog);
            //         ICISupportFilePort.Export();
            //     end;
            // }

        }
    }

    trigger OnAfterGetCurrRecord()
    begin
        LogDescription := ICISupportTicketLogMgt.GetTicketLogDescription(Rec."Entry No.");
    end;

    trigger OnAfterGetRecord()
    var
        lInStream: InStream;
    begin
        IF Rec.Data.HasValue() THEN begin
            Rec.CalcFields(Data);
            Rec.Data.CreateInStream(lInStream);
            lInStream.Read(DataText);
            DataText := CopyStr(DataText, 1, 250);
        end;

        LogDescription := ICISupportTicketLogMgt.GetTicketLogDescription(Rec."Entry No.");
    end;

    procedure SetTicketRecordID(newTicketRecID: RecordID)
    begin
        TicketRecID := newTicketRecID;
        Rec.SetRange("Record ID", TicketRecID);
    end;

    var
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        TicketRecID: RecordId;
        LogDescription: Text;
        DataText: Text;
}
