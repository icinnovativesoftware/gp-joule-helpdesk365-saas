table 56281 "ICI Support Ticket Category"
{
    Caption = 'ICI Support Ticket Category', Comment = 'de-DE=Support Ticket Kategorien';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Support T. Cat. Lookup";
    DrillDownPageId = "ICI Support T. Cat. Lookup";

    fields
    {
        field(1; Code; Code[50])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "Parent Category"; Code[50])
        {
            Caption = 'Parent Category', Comment = 'de-DE=Gehört zu Kategorie';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket Category";
        }
        field(12; Online; Boolean)
        {
            Caption = 'Online', Comment = 'de-DE=Online';
            DataClassification = CustomerContent;
            InitValue = true;
        }
        field(13; Inactive; Boolean)
        {
            Caption = 'Inactive', Comment = 'de-DE=Inaktiv';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                IF Inactive then
                    Validate(Online, false);
            end;
        }

        field(14; "Activity Code"; Code[20])
        {
            Caption = 'Activity Code', Comment = 'de-DE=Aktivitätencode';
            DataClassification = CustomerContent;
            TableRelation = Activity;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Process Stage';
        }
        field(15; "Interaction Template Code"; Code[10])
        {
            Caption = 'Interaction Template', Comment = 'de-DE=Aktivitätenvorlagencode';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Process Stage';
        }
        field(16; "Ticketboard - Color"; Text[20])
        {
            Caption = 'Ticketboard - Base Color', Comment = 'de-DE=Ticketboard Farbe';
            DataClassification = CustomerContent;
        }
        field(17; "Category Filter"; Code[50])
        {
            Caption = 'Parent Category', Comment = 'de-DE=Gehört zu Kategorie';
            FieldClass = FlowFilter;
        }
        field(18; Layer; Integer)
        {
            InitValue = 0;
            MinValue = 0;
            MaxValue = 1;
            Caption = 'Layer', Comment = 'de-DE=Ebene';
            DataClassification = CustomerContent;
        }
        field(19; "Default Online"; Boolean)
        {
            Caption = 'Default Online', Comment = 'de-DE=Vorbel. Online';
            DataClassification = CustomerContent;
            InitValue = true;
        }
        field(20; "Default Escalation Code"; Code[20])
        {
            Caption = 'Default Escalation Code', Comment = 'de-DE=Vorbel. Eskalationscode';
            TableRelation = "ICI Escalation";
            DataClassification = CustomerContent;
        }
        field(21; "Default Accounting Type"; Code[20])
        {
            Caption = 'Accounting', Comment = 'de-DE=Vorbel. Abrechnung';
            TableRelation = "ICI Support Time Acc. Type";
            DataClassification = CustomerContent;
        }
        field(22; "Default Process Code"; Code[20])
        {
            Caption = 'Process Code', Comment = 'de-DE=Vorbel. Prozess Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Process";
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
        key(Lookup; "Parent Category", Layer, Code) { }
    }

    procedure GetDescription(): Text[100]
    var
        ICITickCategoryTranslation: Record "ICI Tick. Category Translation";
        Language: Codeunit Language;
        LanguageCode: Code[10];
    begin
        LanguageCode := Language.GetLanguageCode(GlobalLanguage);

        IF ICITickCategoryTranslation.GET(Code, LanguageCode) then
            EXIT(ICITickCategoryTranslation.Description);

        EXIT(Description);
    end;

    procedure ApplyCategoryDefaults(var ICISupportTicket: Record "ICI Support Ticket")
    var
    begin
        IF GuiAllowed THEN // Nicht bei Ticketerstellung aus portal - sonst ist es direkt verschwunden
            ICISupportTicket.Validate(Online, Rec."Default Online");

        IF "Default Process Code" <> '' then
            ICISupportTicket.Validate("Process Code", Rec."Default Process Code");
        IF "Default Accounting Type" <> '' THEN
            ICISupportTicket.Validate("Accounting Type", Rec."Default Accounting Type");
        IF "Default Escalation Code" <> '' then
            ICISupportTicket.Validate("Escalation Code", Rec."Default Escalation Code");

    end;
}
