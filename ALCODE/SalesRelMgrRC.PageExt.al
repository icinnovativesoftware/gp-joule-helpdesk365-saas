pageextension 56303 "ICI Sales & Rel. Mgr. RC" extends "Sales & Relationship Mgr. RC"
{
    layout
    {
        addafter(Control1)
        {
            part(ICISupportActivities; "ICI Support Activities")
            {
                ApplicationArea = All;
            }
            part(ICITicketsByUserChart; "ICI Tickets by User Chart")
            {
                ApplicationArea = All;
            }
            part(ICITicketsByCompanyChart; "ICI Tickets by Company Chart")
            {
                ApplicationArea = All;
            }
            part("ICI Escalation Log"; "ICI Escalation Log")
            {
                ApplicationArea = All;
            }
            part(Mailbox; "ICI Mailrobot Mailbox Listpart")
            {
                ApplicationArea = All;
            }

        }
    }

    actions
    {
        addlast(sections)
        {
            group(ICIHelpdesk365)
            {
                ToolTip = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';

                action(ICITickets)
                {
                    ApplicationArea = All;
                    ToolTip = 'Opens Ticket List', Comment = 'de-DE=Öffnet die Ticket Liste';
                    Caption = 'Tickets', Comment = 'de-DE=Tickets';
                    RunObject = Page "ICI Support Ticket List";
                }
                action("ICI Ticket Board")
                {
                    ApplicationArea = All;
                    Caption = 'Ticket Board', Comment = 'de-DE=Ticketboard';
                    ToolTip = 'Ticket Board', Comment = 'de-DE=Ticketboard';
                    RunObject = Page "ICI Ticket Board";
                }
                action(ICIUsers)
                {
                    ApplicationArea = All;
                    ToolTip = 'Opens User List', Comment = 'de-DE=Öffnet die Benutzer Liste';
                    Caption = 'User', Comment = 'de-DE=Supportbenutzer';
                    RunObject = Page "ICI Support User List";
                }
                action("ICI Mailrobot Blacklist")
                {
                    ApplicationArea = All;
                    Caption = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mailrobot Blacklist';
                    ToolTip = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mailrobot Blacklist';
                    RunObject = Page "ICI Mailrobot Blacklist";
                }
                group(ICIAdministration)
                {
                    ToolTip = 'Administration', Comment = 'de-DE=Administration';
                    Caption = 'Administration', Comment = 'de-DE=Administration';

                    action("ICI Support Setup")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Opens Support Setup', Comment = 'de-DE=Öffnet die Support Einrichtung';
                        Caption = 'Support Setup', Comment = 'de-DE=HelpDesk Einrichtung';
                        RunObject = Page "ICI Support Setup";
                    }
                    action("ICI Support User")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Opens Support User List', Comment = 'de-DE=Öffnet die Supportbenutzer';
                        Caption = 'Support Users', Comment = 'de-DE=Supportbenutzer';
                        RunObject = Page "ICI Support User List";
                    }
                    action("ICI Support Mail Setup")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Opens Support Mail Setup', Comment = 'de-DE=Öffnet die Support E-Mail Einrichtung';
                        Caption = 'Support Mail Setup', Comment = 'de-DE=E-Mail Einrichtung';
                        RunObject = Page "ICI Support Mail Setup";
                    }
                    action("ICI Mailrobot Setup")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Opens Mailrobot Setup', Comment = 'de-DE=Öffnet die Mailrobot Einrichtung';
                        Caption = 'Mailrobot Setup', Comment = 'de-DE=Mailrobot Einrichtung';
                        RunObject = Page "ICI Mailrobot Setup";
                    }
                    action("ICI Support Dagbox Setup")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Opens Dragbox Setup', Comment = 'de-DE=Öffnet die Dragbox Einrichtung';
                        Caption = 'Dragbox Setup', Comment = 'de-DE=Dragbox Einrichtung';
                        RunObject = Page "ICI Sup. Dragbox Setup";
                    }
                    action("ICI Support E-Mail Text Module")
                    {
                        ApplicationArea = All;
                        Caption = 'ICI Support E-Mail Text Module', Comment = 'de-DE=E-Mailvorlagen';
                        ToolTip = 'ICI Support E-Mail Text Module', Comment = 'de-DE=E-Mailvorlagen';
                        RunObject = Page "ICI Support E-Mail Text Module";
                    }
                    action("ICI Support Ticket Text Module")
                    {
                        ApplicationArea = All;
                        Caption = 'ICI Support Ticket Text Module', Comment = 'de-DE=Textvorlagen';
                        ToolTip = 'ICI Support Ticket Text Module', Comment = 'de-DE=Textvorlagen';
                        RunObject = Page "ICI Support Ticket Text Module";
                    }
                    action("ICI Support Process List")
                    {
                        ApplicationArea = All;
                        Caption = 'ICI Support Process List', Comment = 'de-DE=Prozesse';
                        ToolTip = 'ICI Support Process List', Comment = 'de-DE=Prozess Liste';
                        RunObject = Page "ICI Support Process List";
                    }
                    action("ICI Support Ticket Cat. List")
                    {
                        ApplicationArea = All;
                        Caption = 'ICI Support Ticket Cat. List', Comment = 'de-DE=Ticketkategorien';
                        ToolTip = 'ICI Support Ticket Cat. List', Comment = 'de-DE=Ticketkategorien';
                        RunObject = Page "ICI Support Ticket Cat. List";
                    }
                    action("ICI Portal Cue Sets")
                    {
                        ApplicationArea = All;
                        Caption = 'Portal Cue Sets', Comment = 'de-DE=Kundenportal Kacheln';
                        ToolTip = 'Portal Cue Sets', Comment = 'de-DE=Kundenportal Kacheln';
                        RunObject = Page "ICI Portal Cue Sets";
                    }

                    action("ICI Escalations")
                    {
                        ApplicationArea = All;
                        Caption = 'Escalations', Comment = 'de-DE=Eskalationen';
                        ToolTip = 'Escalations', Comment = 'de-DE=Eskalationen';
                        RunObject = Page "ICI Escalations";
                    }
                    action("ICI First Allocation Matrix")
                    {
                        ApplicationArea = All;
                        Caption = 'First Allocation Matrix', Comment = 'de-DE=Ticketzuweisung';
                        ToolTip = 'First Allocation Matrix', Comment = 'de-DE=Ticketzuweisung';
                        RunObject = Page "ICI First Allocation Matrix";
                    }
                    action("ICI Mailrobot Inboxes")
                    {
                        ApplicationArea = All;
                        Caption = 'Mailrobot Inboxes', Comment = 'de-DE=Mailrobot Postfächer';
                        ToolTip = 'Mailrobot Inboxes', Comment = 'de-DE=Mailrobot Postfächer';
                        RunObject = Page "ICI Mailrobot Mailboxes";
                    }
                }
                group(ICIHistory)
                {
                    ToolTip = 'History', Comment = 'de-DE=Archiv';
                    Caption = 'History', Comment = 'de-DE=Archiv';
                    action("ICI Mailrobot Inbox List")
                    {
                        ApplicationArea = All;
                        Caption = 'Support E-Mail Robot Log', Comment = 'de-DE=Mailimportprotokoll';
                        ToolTip = 'Support E-Mail Robot Log', Comment = 'de-DE=Mailimportprotokoll';
                        RunObject = Page "ICI Mailrobot Inbox List";
                    }
                    action("ICI Support Log List")
                    {
                        ApplicationArea = All;
                        Caption = 'ICI Support Log List', Comment = 'de-DE=Versandprotokoll';
                        ToolTip = 'ICI Support Log List', Comment = 'de-DE=Supportprotokoll';
                        RunObject = Page "ICI Support Log List";
                    }
                }

                group(ICITasks)
                {
                    ToolTip = 'Tasks', Comment = 'de-DE=Aufgaben';
                    Caption = 'Tasks', Comment = 'de-DE=Aufgaben';
                    action("ICI Ticket Payoff")
                    {
                        ApplicationArea = All;
                        Caption = 'Ticket Payoff', Comment = 'de-DE=Ticketabrechnung';
                        ToolTip = 'Ticket Payoff', Comment = 'de-DE=Ticketabrechnung';
                        RunObject = Report "ICI Support Ticket Payoff";
                    }
                }
            }
        }
        // addfirst(embedding) // zweite zeile im rollencenter. Sieht man auf den ersten Blick
        // {
        //     action(ICITickets)
        //     {
        //         ApplicationArea = All;
        //         ToolTip = 'Opens Ticket List', Comment = 'de-DE=Öffnet die Ticket Liste';
        //         Caption = 'Tickets', Comment = 'de-DE=Ticket Liste';
        //         RunObject = Page "ICI Support Ticket List";
        //     }
        // }
    }
}
