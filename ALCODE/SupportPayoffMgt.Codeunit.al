codeunit 56290 "ICI Support Payoff Mgt."
{
    trigger OnRun()
    begin
    end;

    var
        SupportSetup: Record "ICI Support Setup";
        ICISupportTimeLogLine: Record "ICI Support Time Log Line";
        Customer: Record "Customer";
        SupportTicket: Record "ICI Support Ticket";
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        SupportInvoiceLbl: Label 'Support Invoice until %1', Comment = '%1 = Date|de-DE=Supportabrechnung zum %1';
        // Text001: Label '%1: %2';
        FreeLbl: Label '(Free Service without Charge)', Comment = 'de-DE=(Kulanz-Leistung ohne Berechnung)';
        ShippedOnLbl: Label 'Shipped on %1', Comment = '%1 = Date|de-DE=Geliefert am %1';
        NoOfDocuments: Integer;
        //PositionCounter: Integer;
        Enddate: Date;
        //PostingDate: Date;
        DeliveryText: Text[250];
        ReportedByLbl: Label 'Reported by: %1', Comment = '%1=Contact Name|de-DE=Gemeldet durch: %1';
        ProcessedByLbl: Label 'Processed By: %1', Comment = '%1=User Name|de-DE=Bearbeitet durch: %1';
        SupportUserFilter: Text[250];
        //ConfirmRunWithoutFilterLbl: Label 'Do you really want to run the Ticket Invoicing without Filter?', Comment = 'de-DE=Wollen Sie die Ticketabrechnung wirklich ohne Mitarbeiterfilter - für alle Teams - durchführen?';
        YourReference: Text[250];
        SalesPersonCode: Code[20];
    // NoPayoffSetupErr: Label 'Support Setup for Ticket Invoicing is not configured.', Comment = 'de-DE=Die Einrichtung für die Ticketabrechnung wurde nicht vorgenommen.';

    procedure Inizialize(var ParCustomer: Record "Customer"; ParEnddate: Date; ParPostingDate: Date; ParSalesPerson: Code[20])
    begin
        ICISupportTimeLogLine.SETCURRENTKEY("Bill-to Customer No.", Date, "Support Ticket No.", "Accounting Type");

        Enddate := ParEnddate;
        Customer.COPY(ParCustomer);
        //PostingDate := ParPostingDate;
        SalesPersonCode := ParSalesPerson;

        SupportSetup.GET();
        // IF SupportSetup."Sales Payoff Type" = SupportSetup."Sales Payoff Type"::" " THEN
        //     ERROR(NoPayoffSetupErr);

        NoOfDocuments := 0;
    end;

    procedure SetSupportUserFilter(LocSupportUserFilter: Text[250])
    begin
        SupportUserFilter := LocSupportUserFilter;
    end;

    procedure SetReferenceText(LocYourReference: Text[250])
    begin
        YourReference := LocYourReference;
    end;

    procedure Payoff(): Integer
    var
        //ClearSalesLine: Record "Sales Line";
        Contact: Record "Contact";
        //TransferExtendedText: Codeunit "Transfer Extended Text";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        LastTicketNo: Text[20];
        //ServiceQuotaExists: Boolean;
        PayableTTLExists: Boolean;
        //LineCounter: Integer;
        //LineCounter2: Integer;
        TicketRefTxt: Label 'Ticket-Reference: %1', Comment = '%1 = Ticket Reference|de-DE=Ticket-Referenz: %1';
    begin
        IF Customer.FINDSET() THEN
            REPEAT

                PayableTTLExists := FindPayableTTL(Customer, SupportUserFilter);
                IF PayableTTLExists THEN BEGIN

                    ICISupportTimeLogLine.SETCURRENTKEY("Bill-to Customer No.", Cleared, "Support Ticket No.", Date, "Accounting Type");
                    ICISupportTimeLogLine.SETRANGE("Bill-to Customer No.", Customer."No.");
                    ICISupportTimeLogLine.SETRANGE(Cleared, FALSE);
                    ICISupportTimeLogLine.SETFILTER(Date, '..%1', Enddate); //Festes Startdatum für 1.-Abrechnung

                    ICISupportTimeLogLine.SetFilter("Accounting Type", ICISupportTimeLogLine.GetPayableAccountingTypeFilter());
                    ICISupportTimeLogLine.SETRANGE("Support Ticket Status", ICISupportTimeLogLine."Support Ticket Status"::Closed);

                    IF SupportUserFilter <> '' THEN
                        ICISupportTimeLogLine.SETFILTER("Support User", SupportUserFilter);

                    ICISupportTimeLogLine.SETAUTOCALCFIELDS("Ticket Description");
                    IF ICISupportTimeLogLine.FINDSET() THEN BEGIN

                        Contact.GET(GetContByCust(Customer."No."));

                        LastTicketNo := '';
                        //PositionCounter := 1;
                        NoOfDocuments += 1;
                        //LineCounter := 0;

                        InsertSalesHeader(Enddate);

                        REPEAT

                            IF ICISupportTimeLogLine."Support Ticket No." <> SupportTicket."No." THEN
                                SupportTicket.GET(ICISupportTimeLogLine."Support Ticket No.");

                            IF (LastTicketNo <> ICISupportTimeLogLine."Support Ticket No.") THEN BEGIN

                                InsertLastSalesLine(LastTicketNo);

                                // HeaderLines
                                ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(TicketRefTxt, ICISupportTimeLogLine."Support Ticket No."));
                                ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, ICISupportTimeLogLine."Ticket Description");
                                ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, '');
                            END;

                            //Body-Line
                            InsertCurrSalesLine();
                            LastTicketNo := ICISupportTimeLogLine."Support Ticket No.";

                            SetSettlement(ICISupportTimeLogLine, SalesHeader."No.", '');

                        // TODO - XXX
                        // IF SupportTicket."Linked Order No." <> SalesHeader."No." THEN
                        //     CloseTicket(SupportTicket, SalesLine."Document No.");

                        UNTIL ICISupportTimeLogLine.NEXT() = 0;

                        InsertLastSalesLine(LastTicketNo);

                    END;
                END;

            UNTIL Customer.NEXT() = 0;

        EXIT(NoOfDocuments);
    end;

    local procedure FindPayableTTL(Customer: Record "Customer"; SupportUserFilter: Text[250]): Boolean
    var
        lICISupportTimeLogLine: Record "ICI Support Time Log Line";
    begin
        lICISupportTimeLogLine.SETCURRENTKEY("Bill-to Customer No.", Date, Cleared, "Support Ticket No.", "Accounting Type");
        lICISupportTimeLogLine.SETRANGE("Bill-to Customer No.", Customer."No.");
        lICISupportTimeLogLine.SETRANGE(Cleared, FALSE);
        lICISupportTimeLogLine.SETFILTER(Date, '..%1', Enddate);

        lICISupportTimeLogLine.SETFILTER("Accounting Type", lICISupportTimeLogLine.GetAccTypeFilterNotFreeLines());
        lICISupportTimeLogLine.SETRANGE("Support Ticket Status", lICISupportTimeLogLine."Support Ticket Status"::Closed);

        IF SupportUserFilter <> '' THEN
            lICISupportTimeLogLine.SETFILTER("Support User", SupportUserFilter);

        EXIT(NOT lICISupportTimeLogLine.ISEMPTY());
    end;

    local procedure InsertSalesHeader(ServiceDate: Date)
    var
        Contact: Record "Contact";
    begin
        // Insert Sales Header ---
        Contact.GET(GetContByCust(Customer."No."));

        SalesHeader.INIT();
        SalesHeader.SetHideValidationDialog(TRUE);
        SalesHeader."No." := '';
        SalesHeader."Document Type" := SalesHeader."Document Type"::Invoice;

        SalesHeader.VALIDATE("Posting Date", ServiceDate);
        SalesHeader.VALIDATE("Document Date", ServiceDate);

        SalesHeader.INSERT(TRUE);

        IF YourReference <> '' THEN
            SalesHeader."Your Reference" := COPYSTR(YourReference, 1, MaxStrLen(SalesHeader."Your Reference"))
        ELSE
            SalesHeader."Your Reference" := COPYSTR(STRSUBSTNO(SupportInvoiceLbl, Enddate), 1, MaxStrLen(SalesHeader."Your Reference"));

        SalesHeader.VALIDATE("Sell-to Customer No.", Customer."No.");

        SalesHeader.VALIDATE("Order Date", ServiceDate);
        SalesHeader.VALIDATE("Shipment Date", ServiceDate);

        SalesHeader.VALIDATE("Salesperson Code", SalesPersonCode);
        SalesHeader.MODIFY(TRUE);
    end;

    local procedure InsertCurrSalesLine()
    var
        ICISupportTimeLogComment: Record "ICI Support Time Log Comment";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        LineDiscount: Decimal;
    begin
        //PositionCounter += 1;

        ICISupportTimeLogLine.CalcFields("Discount %");
        LineDiscount := ICISupportTimeLogLine."Discount %";
        // IF ICISupportTimeLogLine.Accounting = ICISupportTimeLogLine.Accounting::ForFree THEN
        //     LineDiscount := 100
        // ELSE
        //     LineDiscount := 0;

        ICISupportTimeLogLine.CalcFields("Sales Payoff Type");
        ICISupportDocumentMgt.InsertSalesLine(
          SalesHeader
          , SalesLine
          , ICISupportTimeLogLine."Sales Payoff Type", ICISupportTimeLogLine."Sales Payoff No.", '', ICISupportTimeLogLine."Time Decimal", 0, ICISupportTimeLogLine.Description
          , 0, '', LineDiscount, ICISupportTimeLogLine."Work Type Code");

        SalesLine."ICI Support Ticket No." := ICISupportTimeLogLine."Support Ticket No.";
        SalesLine."ICI Time Log Line No." := ICISupportTimeLogLine."Line No.";
        SalesLine.MODIFY();

        // Insert Time Log Line Comments
        ICISupportTimeLogComment.SetRange("Support Ticket No.", ICISupportTimeLogLine."Support Ticket No.");
        ICISupportTimeLogComment.SetRange("Time Log Line No.", ICISupportTimeLogLine."Line No.");
        IF ICISupportTimeLogComment.FINDSET() THEN
            REPEAT
                ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, ICISupportTimeLogComment.Description);
            UNTIL ICISupportTimeLogComment.NEXT() = 0;

        //Kulanzzeile
        IF LineDiscount = 100 THEN BEGIN
            ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, FreeLbl);
            ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, '');
        END;
    end;

    local procedure InsertLastSalesLine(LastTicketNo: Code[20])
    var
        LastSupportTicket: Record "ICI Support Ticket";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";

    begin
        IF LastTicketNo = '' THEN
            EXIT;

        LastSupportTicket.GET(LastTicketNo);

        ICISupportTicketLog.SETRANGE("Support Ticket No.", LastTicketNo);
        ICISupportTicketLog.SETRANGE(Type, ICISupportTicketLog.Type::State);
        ICISupportTicketLog.SETRANGE("State Subtype", ICISupportTicketLog."State Subtype"::Closed);
        ICISupportTicketLog.FINDLAST();

        DeliveryText := STRSUBSTNO(ShippedOnLbl, ICISupportTicketLog."Creation Date"); // Last Closed Date

        IF LastSupportTicket."Curr. Contact Name" <> '' THEN
            ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(ReportedByLbl, LastSupportTicket."Curr. Contact Name"));

        IF LastSupportTicket."Support User Name" <> '' THEN
            ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, STRSUBSTNO(ProcessedByLbl, LastSupportTicket."Support User Name"));

        ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, DeliveryText);
        ICISupportDocumentMgt.InsertTextSalesLine(SalesHeader, SalesLine, '');

    end;

    procedure SetSettlement(var pICISupportTimeLogLine: Record "ICI Support Time Log Line"; SalesDocNo: Code[20]; ServiceDocNo: Code[20])
    var
        lICISupportTimeLogLine: Record "ICI Support Time Log Line";
    begin
        lICISupportTimeLogLine := pICISupportTimeLogLine;
        lICISupportTimeLogLine.FIND('=');
        lICISupportTimeLogLine.VALIDATE("Sales Doc No.", SalesDocNo);
        lICISupportTimeLogLine.VALIDATE("Service Doc No.", ServiceDocNo);
        lICISupportTimeLogLine.Cleared := TRUE;
        lICISupportTimeLogLine.MODIFY();
    end;


    // procedure CloseTicket(var SupportTicket: Record "ICI Support Ticket"; OrderNo: Code[20])
    // var
    //     SupportTicket2: Record "ICI Support Ticket";
    // begin
    //     WITH SupportTicket2 DO BEGIN
    //         GET(SupportTicket."No.");
    //         "Linked Order No." := SalesHeader."No.";
    //         Accounting := SupportTicket2.Accounting::ExistingOrder;
    //         MODIFY(FALSE);
    //     END;
    // end;

    local procedure GetContByCust(CustomerNo: Code[20]): Code[20]
    var
        ContactBusinessRelation: Record "Contact Business Relation";
    begin
        ContactBusinessRelation.SETRANGE("No.", CustomerNo);
        ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);
        IF ContactBusinessRelation.FINDLAST() THEN
            EXIT(ContactBusinessRelation."Contact No.");
        EXIT('');
    end;
}



