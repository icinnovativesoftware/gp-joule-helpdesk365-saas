table 56296 "ICI Reference No. Buffer"
{
    Caption = 'ICI Reference No. Lookup Buffer', Comment = 'de-DE=Support Referenznr. such Buffer';
    DataClassification = SystemMetadata;
    //TableType = Temporary;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(10; Code; Code[50])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = SystemMetadata;
        }
        field(9; "Reference No."; Code[50])
        {
            Caption = 'ReferenceNo', Comment = 'de-DE=Referenznr.';
            DataClassification = SystemMetadata;
        }
        field(11; Description; Text[250])
        {
            Caption = 'Description/Name', Comment = 'de-DE=Beschreibung/Name';
            DataClassification = SystemMetadata;
        }
        field(12; Type; Option)
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Document Search Type';
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = SystemMetadata;
            OptionMembers = "Pst.Invoice","Quote","Order","Pst.Shipment","Service Item","Service Order","Service Quote","Service Contract","Service Contract Quote","Pst. Service Shipment","Pst. Service Invoice";
            OptionCaption = 'Pst.Invoice,Quote,Order,Pst.Shipment,Service Item,Service Order,Service Quote,Service Contract,Service Contract Quote,Pst.Service Shipment,Pst. Service Invoice', Comment = 'de-DE=Geb. Rechnung,Angebot,Auftrag,Lieferschein,Service Artikel,Serviceauftrag,Serviceangebot,Sericevertrag,Servicevertragsangebot,Geb. Servicelieferung,Geb. Servicerechnung';
        }
        field(13; "Customer No."; Code[20])
        {
            Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
            DataClassification = SystemMetadata;
        }
        field(14; "Customer Name"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("Customer".Name Where("No." = FIELD("Customer No.")));
            Caption = 'Customer Name', Comment = 'de-DE=Debitorenname';
        }
        field(15; "Additional Text"; Text[250])
        {
            Caption = 'Additional Text', Comment = 'de-DE=Hinweis';
            DataClassification = SystemMetadata;
        }
        field(16; "Search Result"; Code[50])
        {
            Caption = 'Matches Code', Comment = 'de-DE=Gefundener Wert';
            DataClassification = SystemMetadata;
        }
        field(17; "Searched Field"; Text[100])
        {
            Caption = 'Matches Field', Comment = 'de-DE=Übereinstimmung in Feld';
            DataClassification = SystemMetadata;
        }
        field(18; "Document Search Type"; Enum "ICI Document Search Type")
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = SystemMetadata;
        }
        field(19; "Address"; Text[100])
        {
            Caption = 'Address', Comment = 'de-DE=Adresse';
            DataClassification = SystemMetadata;
        }
        field(20; "Address 2"; Code[50])
        {
            Caption = 'Address 2', Comment = 'de-DE=Adresse 2';
            DataClassification = SystemMetadata;
        }
        field(21; "Post Code"; Code[20])
        {
            Caption = 'Post Code', Comment = 'de-DE=PLZ';
            DataClassification = SystemMetadata;
        }
        field(22; "City"; Text[30])
        {
            Caption = 'City', Comment = 'de-DE=Stadt';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }


}
