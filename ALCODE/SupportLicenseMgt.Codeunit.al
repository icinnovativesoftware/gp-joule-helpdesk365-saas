codeunit 56276 "ICI Support License Mgt."
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    begin
        SyncLicenseStatus();
        COMMIT();
    end;

    procedure SyncLicenseStatus()
    var
        ICISupportSetup: Record "ICI Support Setup";
        Base64Convert: Codeunit "Base64 Convert";
        TenantInformation: Codeunit "Tenant Information";
        EnvironmentInformation: Codeunit "Environment Information";
        lHttpClient: HttpClient;
        AuthString: Text;
        UserName: Text;
        Password: Text;
        CallURL: Text;
        CallAction: Text;
        CallLicenseKey: Text;
        ICIHttpResponseMessage: HttpResponseMessage;
        lHttpContent: HttpContent;
        ResponseAsText: Text;
        jObj: JsonObject;
    begin
        UserName := 'Gast';
        Password := 'Gast';

        AuthString := STRSUBSTNO(AuthString1Txt, UserName, Password);
        AuthString := Base64Convert.ToBase64(AuthString);
        AuthString := STRSUBSTNO(AuthString2Txt, AuthString);
        lHttpClient.DefaultRequestHeaders().Add('Authorization', AuthString);

        CallAction := 'get_license_status';
        CallLicenseKey := '23a849f8aa7452f9a396098861f1e55a'; // Helpdesk365
        CallURL := 'https://licmgt.ic-innovative.de/license_management/navconnect.php?&action=%1&tenant_id=%2&company_name=%3&license_key=%4&is_sandbox=%5';

        CallURL := StrSubstNo(CallURL, CallAction, TenantInformation.GetTenantId(), CompanyName, CallLicenseKey, EnvironmentInformation.IsSandbox());
        IF NOT lHttpClient.GET(CallURL, ICIHttpResponseMessage) then
            //ERROR('Call Failed');
            ERROR('');

        lHttpContent := ICIHttpResponseMessage.Content;
        lHttpContent.ReadAs(ResponseAsText);
        //MESSAGE(ResponseAsText);

        if not jObj.ReadFrom(ResponseAsText) then
            Error('Invalid Response, expected an JSON array as root object');

        //MESSAGE('Licensed: %1', GetJsonToken(jObj, 'licensed').AsValue().AsText());

        ICISupportSetup.GET();
        ICISupportSetup."Document Integration" := GetJsonToken(jObj, 'licensed_1').AsValue().AsInteger() = 1;
        ICISupportSetup."Service Integration" := GetJsonToken(jObj, 'licensed_2').AsValue().AsInteger() = 1;
        ICISupportSetup."Time Registration" := GetJsonToken(jObj, 'licensed_3').AsValue().AsInteger() = 1;
        ICISupportSetup."Portal Integration" := GetJsonToken(jObj, 'licensed_4').AsValue().AsInteger() = 1;
        ICISupportSetup."Task Integration" := GetJsonToken(jObj, 'licensed_5').AsValue().AsInteger() = 1;
        ICISupportSetup."License Module 6" := GetJsonToken(jObj, 'licensed_6').AsValue().AsInteger() = 1;
        ICISupportSetup."License Module 7" := GetJsonToken(jObj, 'licensed_7').AsValue().AsInteger() = 1;
        ICISupportSetup."License Module 8" := GetJsonToken(jObj, 'licensed_8').AsValue().AsInteger() = 1;
        ICISupportSetup."License Module 9" := GetJsonToken(jObj, 'licensed_9').AsValue().AsInteger() = 1;
        ICISupportSetup."License Module 10" := GetJsonToken(jObj, 'licensed_10').AsValue().AsInteger() = 1;

        ICISupportSetup."Next Scheduled License Check" := CALCDATE('<2W>', TODAY);
        ICISupportSetup.MODIFY(TRUE);
    end;

    procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text) jToken: JsonToken;
    begin
        if not JsonObject.GET(TokenKey, jToken) then
            ERROR('Could not find a token with key %1', TokenKey);
    end;

    procedure CheckLicense(): Boolean
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF NOT ICISupportSetup.ReadPermission THEN
            EXIT(FALSE);
        IF Not ICISupportSetup.GET() THEN
            EXIT(false);

        IF TODAY <= ICISupportSetup."Next Scheduled License Check" THEN
            EXIT(true);

        EXIT(false);
    end;

    var
        AuthString1Txt: Label '%1:%2', Comment = 'Needed for Building Basig Http Auth String. %1 = Username; %2 = Password';
        AuthString2Txt: Label 'Basic %1', Comment = 'Needed for Building Basig Http Auth String. %1 is the Base64 Encoded String from AutoString1Txt ';
}
