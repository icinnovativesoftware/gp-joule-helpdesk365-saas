page 56318 "ICI Mailrobot Blacklist"
{

    ApplicationArea = All;
    Caption = 'ICI Mailrobot Blacklist', Comment = 'de-DE=Mailrobot E-Mail Blacklist';
    PageType = List;
    SourceTable = "ICI Mailrobot Blacklist";
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
                    Editable = false;
                    Visible = false;
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    ApplicationArea = All;
                    ToolTip = 'Search E-Mail', Comment = 'de-DE=Such E-Mail';
                }
                field("Search E-Mail"; Rec."Search E-Mail")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Search E-Mail', Comment = 'de-DE=Such E-Mail';
                }
                field(Active; Rec.Active)
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
            }
        }
    }

}
