page 56359 "ICI Mailrobot Setup"
{

    Caption = 'Mailrobot Setup', Comment = 'de-DE=Mailrobot Einrichtung';
    PageType = Card;
    SourceTable = "ICI Mailrobot Setup";
    UsageCategory = Administration;
    ApplicationArea = All;

    layout
    {
        area(content)
        {

            group(ConnectionSettings)
            {
                Caption = 'Connection Settings', Comment = 'de-DE=Verbindungsdaten';
                field("IMAP Connector URL"; Rec."IMAP Connector URL")
                {
                    ApplicationArea = All;
                    ToolTip = 'IMAP Connector URL. Example: https://licmgt.ic-innovative.de/Helpdesk365/IMAP_Connector.php', Comment = 'de-DE=URL des IMAP Connectors. Z.B.:https://licmgt.ic-innovative.de/Helpdesk365/IMAP_Connector.php';
                }
                field("IMAP Connector Auth. User"; Rec."IMAP Connector Auth. User")
                {
                    ApplicationArea = All;
                    ToolTip = 'IMAP Connector Auth. User', Comment = 'de-DE=IMAP Connector Auth. Benutzername';
                }
                field("IMAP Connector Auth. Password"; Rec."IMAP Connector Auth. Password")
                {
                    ApplicationArea = All;
                    ToolTip = 'IMAP Connector Auth. Password', Comment = 'de-DE=IMAP Connector Auth. Passwort';
                }
                field("EWS Connector URL"; Rec."EWS Connector URL")
                {
                    ApplicationArea = All;
                    ToolTip = 'EWS Connector URL. Example: https://licmgt.ic-innovative.de/Helpdesk365/EWS_Connector/EWS_Connector.php', Comment = 'de-DE=URL des EWS Connectors. Z.B.:https://licmgt.ic-innovative.de/Helpdesk365/EWS_Connector/EWS_Connector.php';

                }
                field("EWS Connector Auth. User"; Rec."EWS Connector Auth. User")
                {
                    ApplicationArea = All;
                    ToolTip = 'EWS Connector Auth. User', Comment = 'de-DE=EWS Connector Auth. Benutzername';
                }
                field("EWS Connector Auth. Password"; Rec."EWS Connector Auth. Password")
                {
                    ApplicationArea = All;
                    ToolTip = 'EWS Connector Auth. Password', Comment = 'de-DE=EWS Connector Auth. Passwort';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ReadMail)
            {
                ApplicationArea = All;
                Caption = 'Download and Process all E-Mail Inboxes', Comment = 'de-DE=Alle Postfächer auslesen und E-Mails verarbeiten';
                Image = SendMail;
                ToolTip = 'Synchronizes the IMAP Inboxes to the Mailrobot Log', Comment = 'de-DE=Läd die E-Mails aus allen aktiven Postfächern in das Mailrobot Log und verarbeitet diese';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    MailRobotMgt.DownloadAllMailboxes();
                    COMMIT();
                    MailRobotMgt.ProcessAllMailRobotLogs();
                end;
            }
        }
        area(Navigation)
        {
            action(Mailboxes)
            {
                ApplicationArea = All;
                Image = MailSetup;
                Caption = 'Mailboxes', Comment = 'de-DE=Postfächer';
                ToolTip = 'Mailboxes', Comment = 'de-DE=Postfächer';
                RunObject = Page "ICI Mailrobot Mailboxes";
            }
            action(MailLog)
            {
                ApplicationArea = All;
                Image = Email;
                Caption = 'Mail Log', Comment = 'de-DE=Protokoll';
                ToolTip = 'Mail Log', Comment = 'de-DE=Protokoll';
                RunObject = Page "ICI Mailrobot Inbox List";
            }
        }
    }


    trigger OnOpenPage()
    begin
        Rec.Reset();
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert();
        end;
    end;
}
