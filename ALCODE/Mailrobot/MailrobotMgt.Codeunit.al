codeunit 56285 "ICI Mailrobot Mgt."
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        IF ICIMailrobotMailbox.GET(Rec."Parameter String") THEN begin
            DownloadMailboxToLog(ICIMailrobotMailbox);
            COMMIT();
            ProcessAllMailRobotLogsForInbox(ICIMailrobotMailbox);
            COMMIT();
            CleanupMailInbox(ICIMailrobotMailbox);
            COMMIT();
            // end else begin
            // DownloadAllMailboxes();
            // COMMIT();
            // ProcessAllMailRobotLogs();
            // COMMIT();
            // CleanupAllMailInboxes();
        end;
    end;

    procedure DownloadAllMailboxes()
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        ICIMailrobotMailbox.SetRange(Active, true);
        IF ICIMailrobotMailbox.FINDSET() then
            repeat
                DownloadMailboxToLog(ICIMailrobotMailbox);
            until ICIMailrobotMailbox.Next() = 0;
    end;

    procedure DownloadMailboxToLog(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox")
    var
        ImapRange: Text;
        Sequences: List of [Text];
        Sequence: Text;
        SubSequences: List of [Text];
        SubSequence: Text;
        ProgressDialog: Dialog;
        Counter: Integer;
        Total: Integer;
        InboxName: Text;
        Yesterday: Date;
        ProgressMsg: Label 'Fetching Inbox "#3". Progress #1 of #2', Comment = '#1 = Counter; #2 = Total;#3 = Inbox Name|de-DE=Lade Postfach #3. Fortschritt: #1 von #2';
    begin
        IF not ICIMailrobotMailbox."Download Job Queue" THEN
            exit;

        ImapRange := GetSequenceBetweenStartEndDate(ICIMailrobotMailbox);
        GetSequencesFromText(ICIMailrobotMailbox."No. of E-Mails Per Request", ImapRange, Sequences, ICIMailrobotMailbox);
        Total := Sequences.Count();
        Counter := 0;
        InboxName := ICIMailrobotMailbox.Description;
        IF GUIALLOWED THEN
            ProgressDialog.Open(ProgressMsg, Counter, Total, InboxName);
        IF GuiAllowed() then
            ProgressDialog.Update(); // Wird  benötigt. Sonst sind die Zahlen nicht befüllt
        foreach Sequence in Sequences do
            IF NOT GetAndProcessEmailSequence(ICIMailrobotMailbox, Sequence, false) THEN begin
                // Request hat nicht geklappt. Versuchen, E-Mails einzeln abzuholen
                GetSequencesFromText(1, Sequence, SubSequences, ICIMailrobotMailbox); // Eine E-Mail pro SubSequence
                Total += SubSequences.Count();
                foreach SubSequence in SubSequences do begin
                    IF NOT GetAndProcessEmailSequence(ICIMailrobotMailbox, SubSequence, false) then
                        IF NOT GetAndProcessEmailSequence(ICIMailrobotMailbox, SubSequence, true) then // Try single Mail without attachments
                            IF GuiAllowed() THEN
                                Message('Error in ID: %1', SubSequence);
                    Counter += 1;
                    IF GuiAllowed() then
                        ProgressDialog.Update();
                end;
            end else begin // Request hat geklappt. Progress Hochzählen
                Counter += 1;
                IF GuiAllowed() then
                    ProgressDialog.Update();
            end;

        IF GuiAllowed() then
            ProgressDialog.Close();

        // Update Start Date
        Yesterday := CALCDATE('<-1D>', TODAY());
        ICIMailrobotMailbox."Start Date" := Yesterday;
        ICIMailrobotMailbox.Modify();

    end;

    local procedure GetAndProcessEmailSequence(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"; Sequence: Text; WithoutAttachments: Boolean): Boolean

    begin
        CASE ICIMailrobotMailbox."Connection Type" of
            ICIMailrobotMailbox."Connection Type"::IMAP:
                EXIT(GetAndProcessEmailSequenceIMAP(ICIMailrobotMailbox, Sequence, WithoutAttachments));
            ICIMailrobotMailbox."Connection Type"::EWS:
                EXIT(GetAndProcessEmailSequenceEWS(ICIMailrobotMailbox, Sequence));
        END;
    end;

    local procedure GetAndProcessEmailSequenceIMAP(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"; Sequence: Text; WithoutAttachments: Boolean): Boolean
    var
        MailrobotSetup: Record "ICI Mailrobot Setup";
        PayLoad: Text;
        jObj: JsonObject;
        Response: Text;
    begin
        MailrobotSetup.GET();

        ICIMailrobotMailbox.TestField("Connection Type", ICIMailrobotMailbox."Connection Type"::IMAP);
        MailrobotSetup.TestField("IMAP Connector URL");
        ICIMailrobotMailbox.TestField("IMAP Hostname");
        ICIMailrobotMailbox.TestField("IMAP User");
        ICIMailrobotMailbox.TestField("IMAP Password");

        jObj.Add('hostname', ICIMailrobotMailbox."IMAP Hostname");
        jObj.Add('username', ICIMailrobotMailbox."IMAP User");
        jObj.Add('password', ICIMailrobotMailbox."IMAP Password");
        jObj.Add('action', 'getMailsFromSequence');
        jObj.Add('sequence', Sequence);
        if WithoutAttachments then
            jObj.Add('withoutAttachments', '1');

        jObj.WriteTo(PayLoad);
        Response := MakeRequest(MailrobotSetup."IMAP Connector URL", PayLoad, ICIMailrobotMailbox."Request Timeout");
        EXIT(InsertResponseToMailRobotInbox(Response, ICIMailrobotMailbox, WithoutAttachments));
    end;

    local procedure GetAndProcessEmailSequenceEWS(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"; Sequence: Text): Boolean
    var
        MailrobotSetup: Record "ICI Mailrobot Setup";
        PayLoad: Text;
        jObj: JsonObject;
        Response: Text;
    begin
        MailrobotSetup.GET();

        ICIMailrobotMailbox.TestField("Connection Type", ICIMailrobotMailbox."Connection Type"::EWS);
        MailrobotSetup.TestField("EWS Connector URL");
        ICIMailrobotMailbox.TestField("EWS Public Folder Name");
        ICIMailrobotMailbox.TestField("EWS Hostname");
        ICIMailrobotMailbox.TestField("EWS User");
        ICIMailrobotMailbox.TestField("EWS Password");

        jObj.Add('hostname', ICIMailrobotMailbox."EWS Hostname");
        jObj.Add('folder_name', ICIMailrobotMailbox."EWS Public Folder Name");
        jObj.Add('username', ICIMailrobotMailbox."EWS User");
        jObj.Add('password', ICIMailrobotMailbox."EWS Password");
        jObj.Add('action', 'getMailsFromSequence');
        jObj.Add('sequence', Sequence);
        jObj.WriteTo(PayLoad);
        Response := MakeRequest(MailrobotSetup."EWS Connector URL", PayLoad, ICIMailrobotMailbox."Request Timeout");
        EXIT(InsertResponseToMailRobotInbox(Response, ICIMailrobotMailbox, false));
    end;

    local procedure GetSequenceBetweenStartEndDate(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"): Text
    begin
        CASE ICIMailrobotMailbox."Connection Type" of
            ICIMailrobotMailbox."Connection Type"::IMAP:
                EXIT(GetIMAPSequenceBetweenStartEndDate(ICIMailrobotMailbox));
            ICIMailrobotMailbox."Connection Type"::EWS:
                EXIT(GetEWSSequenceBetweenStartEndDate(ICIMailrobotMailbox));
        END;
    end;

    local procedure GetIMAPSequenceBetweenStartEndDate(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"): Text
    var
        ICIMailrobotSetup: Record "ICI Mailrobot Setup";
        Tomorrow: Date;
        StartDate: Date;
        PayLoad: Text;
        jObj: JsonObject;
        Response: Text;
        YMDLbl: Label '%1-%2-%3', Comment = '%1=Year,4;%2=Month,2;%3=Day,2';
    begin
        ICIMailrobotSetup.GET();
        ICIMailrobotMailbox.TestField("Connection Type", ICIMailrobotMailbox."Connection Type"::IMAP);
        ICIMailrobotSetup.TestField("IMAP Connector URL");
        ICIMailrobotMailbox.TestField("IMAP Hostname");
        ICIMailrobotMailbox.TestField("IMAP User");
        ICIMailrobotMailbox.TestField("IMAP Password");

        // Parameter to Send
        jObj.Add('hostname', ICIMailrobotMailbox."IMAP Hostname");
        jObj.Add('username', ICIMailrobotMailbox."IMAP User");
        jObj.Add('password', ICIMailrobotMailbox."IMAP Password");
        jObj.Add('action', 'getSequenceBetweenStartEndDate');

        Tomorrow := CALCDATE('<+1D>', TODAY());
        StartDate := Today();
        IF ICIMailrobotMailbox."Start Date" <> 0D then
            StartDate := ICIMailrobotMailbox."Start Date";

        jObj.Add('start_date', StrSubstNo(YMDLbl, Date2DMY(StartDate, 3), Date2DMY(StartDate, 2), Date2DMY(StartDate, 1))); // This Format: '2021-05-01'
        jObj.Add('end_date', StrSubstNo(YMDLbl, Date2DMY(Tomorrow, 3), Date2DMY(Tomorrow, 2), Date2DMY(Tomorrow, 1))); // This Format: '2021-05-01'
        jObj.WriteTo(PayLoad);

        Response := MakeRequest(ICIMailrobotSetup."IMAP Connector URL", PayLoad, ICIMailrobotMailbox."Request Timeout");
        EXIT(Response);
    end;

    local procedure GetEWSSequenceBetweenStartEndDate(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"): Text
    var
        ICIMailrobotSetup: Record "ICI Mailrobot Setup";
        Tomorrow: Date;
        StartDate: Date;
        PayLoad: Text;
        jObj: JsonObject;
        Response: Text;
        YMDLbl: Label '%1-%2-%3', Comment = '%1=Year,4;%2=Month,2;%3=Day,2';
    begin
        ICIMailrobotSetup.GET();

        ICIMailrobotMailbox.TestField("Connection Type", ICIMailrobotMailbox."Connection Type"::EWS);
        ICIMailrobotSetup.TestField("EWS Connector URL");
        ICIMailrobotMailbox.TestField("EWS Public Folder Name");
        ICIMailrobotMailbox.TestField("EWS Hostname");
        ICIMailrobotMailbox.TestField("EWS User");
        ICIMailrobotMailbox.TestField("EWS Password");

        // Parameter to Send
        jObj.Add('hostname', ICIMailrobotMailbox."EWS Hostname");
        jObj.Add('folder_name', ICIMailrobotMailbox."EWS Public Folder Name");

        jObj.Add('username', ICIMailrobotMailbox."EWS User");
        jObj.Add('password', ICIMailrobotMailbox."EWS Password");
        jObj.Add('action', 'getSequenceBetweenStartEndDate');

        Tomorrow := CALCDATE('<+1D>', TODAY());
        StartDate := Today();
        IF ICIMailrobotMailbox."Start Date" <> 0D then
            StartDate := ICIMailrobotMailbox."Start Date";

        jObj.Add('start_date', StrSubstNo(YMDLbl, Date2DMY(StartDate, 3), Date2DMY(StartDate, 2), Date2DMY(StartDate, 1))); // This Format: '2021-05-01'
        jObj.Add('end_date', StrSubstNo(YMDLbl, Date2DMY(Tomorrow, 3), Date2DMY(Tomorrow, 2), Date2DMY(Tomorrow, 1))); // This Format: '2021-05-01'
        jObj.WriteTo(PayLoad);

        Response := MakeRequest(ICIMailrobotSetup."EWS Connector URL", PayLoad, ICIMailrobotMailbox."Request Timeout");
        EXIT(Response);
    end;


    local procedure GetSequencesFromText(NoofEMailsPerRequest: Integer; Response: Text; var Sequences: List of [Text]; var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox")
    var
        i: Integer;
        uid: Text;
        sequence: Text;
        ignoreDouble: Boolean;
    begin
        while (Response.Contains(',') or (Strlen(Response) > 0)) do begin
            clear(sequence);
            i := 0;
            while i < NoofEMailsPerRequest DO BEGIN
                // for i := 0 to (NoofEMailsPerRequest) DO BEGIN
                uid := Explode(Response, ',');

                ignoreDouble := ICIMailrobotMailbox."Ignore Doubles";
                if ignoreDouble then
                    ignoreDouble := CheckForDouble(uid, ICIMailrobotMailbox);

                IF not ignoreDouble THEN begin
                    if uid <> '' then
                        if strlen(sequence) > 0 then
                            sequence += ',';
                    sequence += uid;
                    i += 1;
                end;

                if uid = '' then
                    i := NoofEMailsPerRequest; // Break Loop

            END;
            Sequences.Add(sequence);
        end;
    end;

    local procedure CheckForDouble(IDVariant: Variant; var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"): Boolean
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
        imapUID: Integer;
        ewsUID: Text[512];
    begin
        Case ICIMailrobotMailbox."Connection Type"
            oF
            ICIMailrobotMailbox."Connection Type"::IMAP:
                begin
                    IF NOT Evaluate(imapUID, IDVariant) THEN
                        EXIT(False); // Dont Ignore
                    ICIMailrobotLog.SetCurrentKey("Mailrobot Mailbox Code", "IMAP UID");
                    ICIMailrobotLog.SetRange("Mailrobot Mailbox Code", ICIMailrobotMailbox.Code);
                    ICIMailrobotLog.SetRange("IMAP UID", imapUID);
                    EXIT(ICIMailrobotLog.Count <> 0); // Ignore, if Count = 1
                end;
            ICIMailrobotMailbox."Connection Type"::EWS:
                begin
                    IF NOT Evaluate(ewsUID, IDVariant) THEN
                        EXIT(False); // Dont Ignore
                    ICIMailrobotLog.SetCurrentKey("Mailrobot Mailbox Code", "EWS UID");
                    ICIMailrobotLog.SetRange("Mailrobot Mailbox Code", ICIMailrobotMailbox.Code);
                    ICIMailrobotLog.SetRange("EWS UID", ewsUID);
                    EXIT(ICIMailrobotLog.Count <> 0); // Ignore, if Count = 1
                end;
        End
    end;

    procedure Explode(var TextRecord: Text; Delimiter: Text[30]) TextField: Text[250]
    var
        Pos: Integer;
    begin
        Pos := STRPOS(TextRecord, Delimiter);
        IF Pos <> 0 THEN BEGIN
            TextField := (COPYSTR(TextRecord, 1, Pos - 1));
            TextRecord := (COPYSTR(TextRecord, Pos + STRLEN(Delimiter)));
        END ELSE BEGIN
            TextField := COPYSTR(TextRecord, 1, 250);
            TextRecord := '';
        END;
    end;

    local procedure DownloadFromCloud(fileContent: Text; fileName: Text)
    var
        tempBlob: Codeunit "Temp Blob";
        lInstream: InStream;
        lOutStream: OutStream;
    begin
        // Here some code to fill fileContent and fileName
        Clear(tempBlob);
        tempBlob.CreateOutStream(lOutStream, TEXTENCODING::UTF8);
        lOutStream.WriteText(fileContent);
        tempBlob.CreateInStream(lInstream);
        DownloadFromStream(lInstream, 'Export', '', 'All Files (*.*)|*.*', fileName);
    end;

    local procedure InsertResponseToMailRobotInbox(data: text; var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"; WithoutAttachments: Boolean): Boolean
    var
        EMails: JsonObject;
        EMail: JsonObject;
        MailToken: JsonToken;
        JsonErr: Label 'Error while parsing Json Content from Response: %1', Comment = '%1=Response';
        TxtLbl: Label '%1.txt', Comment = '%1=Configuration Code';
    begin
        IF data = '[]' THEN
            EXIT(false);
        IF data = '' THEN
            EXIT(false);
        IF GuiAllowed AND ICIMailrobotMailbox."Download Response" THEN
            DownloadFromCloud(data, STRSUBSTNO(TxtLbl, ICIMailrobotMailbox.Code));
        IF NOT EMails.ReadFrom(data) THEN begin
            IF GuiAllowed then
                MESSAGE(StrSubstNo(JsonErr, data));
            EXIT(false);
        end;

        foreach MailToken in EMails.Values do begin // For Each Mail
            EMail := MailToken.AsObject();
            InsertEMailToMailRobotLog(EMail, ICIMailrobotMailbox, WithoutAttachments);
            COMMIT();
        end;
        EXIT(true);
    end;

    local procedure InsertEMailToMailRobotLog(EMail: JsonObject; var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox"; WithoutAttachments: Boolean)
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
        Contact: Record Contact;
        ImapUID: Integer;
        EwsUID: Text;
        From: Text;
        ToAddres: Text;
        ReplyTo: Text;
        EMailDateTimeAsText: Text;
        EMailDateTime: DateTime;
        Subject: Text;
        Body: Text;
        //IsAutoResponse: Boolean;
        Attachments: JsonObject;
        Attachment: JsonObject;
        AttachmentsToken: JsonToken;
        lOutStream: OutStream;
        llOutStream: OutStream;
        WithoutAttachmentsLbl: Label 'Without Attachments', Comment = 'de-DE=Fehler bei Dateianhängen. Bitte nachprüfen';
    begin
        // Retrieve Parameters from Json Object
        IF Not GetJsonToken(EMail, 'from').AsValue().IsNull THEN
            From := GetJsonToken(EMail, 'from').AsValue().AsText();
        IF ICIMailrobotMailbox."Connection Type" = ICIMailrobotMailbox."Connection Type"::IMAP THEN
            IF Not GetJsonToken(EMail, 'uid').AsValue().IsNull THEN
                ImapUID := GetJsonToken(EMail, 'uid').AsValue().AsInteger();
        IF ICIMailrobotMailbox."Connection Type" = ICIMailrobotMailbox."Connection Type"::EWS THEN
            IF Not GetJsonToken(EMail, 'uid').AsValue().IsNull THEN
                EwsUID := GetJsonToken(EMail, 'uid').AsValue().AsText();
        IF Not GetJsonToken(EMail, 'toaddress').AsValue().IsNull THEN
            ToAddres := GetJsonToken(EMail, 'toaddress').AsValue().AsText();
        IF Not GetJsonToken(EMail, 'replyto').AsValue().IsNull THEN
            ReplyTo := GetJsonToken(EMail, 'replyto').AsValue().AsText();
        IF Not GetJsonToken(EMail, 'datetime').AsValue().IsNull THEN
            EMailDateTimeAsText := GetJsonToken(EMail, 'datetime').AsValue().AsText();
        IF Not GetJsonToken(EMail, 'subject').AsValue().IsNull THEN
            Subject := GetJsonToken(EMail, 'subject').AsValue().AsText();
        IF Not GetJsonToken(EMail, 'body').AsValue().IsNull THEN
            Body := GetJsonToken(EMail, 'body').AsValue().AsText();
        Attachments := GetJsonToken(EMail, 'attachments').AsObject();
        //IsAutoResponse := GetJsonToken(EMail, 'autoreply').AsValue().AsBoolean(); // Maybe Use this to Prevent loops.

        EVALUATE(EMailDateTime, EMailDateTimeAsText);

        // Insert To MailRobotInbox Table
        IF CheckIfDuplicateEntry(From, EMailDateTime, Subject) THEN EXIT;

        // TODO - XXX
        //IF CheckIfBlackListed(From) THEN EXIT; // Insert even Blacklisted Mails

        ICIMailrobotLog.Init();
        ICIMailrobotLog.INSERT(TRUE);
        ICIMailrobotLog.Validate("Mailrobot Mailbox Code", ICIMailrobotMailbox.Code);
        ICIMailrobotLog.Validate("IMAP UID", ImapUID);
        ICIMailrobotLog.Validate("EWS UID", EwsUID);
        ICIMailrobotLog.Validate(Sender, copystr(UPPERCASE(From), 1, 80));
        ICIMailrobotLog.Validate("Send Date", EMailDateTime);
        ICIMailrobotLog.CalcFields(Subject, Body);
        ICIMailrobotLog.Subject.CreateOutStream(lOutStream);
        lOutStream.Write(Subject);
        ICIMailrobotLog.Body.CreateOutStream(llOutStream);
        llOutStream.Write(Body);
        IF WithoutAttachments THEN begin
            ICIMailrobotLog.Validate("Process State", ICIMailrobotLog."Process State"::Error);
            ICIMailrobotLog.Validate("Process Text", WithoutAttachmentsLbl);
        end;

        ICIMailrobotLog.MODIFY(TRUE);

        Contact.SETRANGE("Search E-Mail", UpperCase(ICIMailrobotLog.Sender));
        case Contact.Count() of
            0:
                ICIMailrobotLog.Validate("Contact Mapping", "ICI Mailrobot Contact Mapping"::None);
            1:
                ICIMailrobotLog.Validate("Contact Mapping", "ICI Mailrobot Contact Mapping"::Single);
            else
                ICIMailrobotLog.Validate("Contact Mapping", "ICI Mailrobot Contact Mapping"::Multiple);
        end;

        ICIMailrobotLog.MODIFY(TRUE);

        // Add Attachments
        foreach AttachmentsToken in Attachments.Values do begin // For Each Mail
            Attachment := AttachmentsToken.AsObject();
            InsertAttachmentsToMailRobotLog(Attachment, ICIMailrobotLog.RecordId);
        end;
    end;

    local procedure InsertAttachmentsToMailRobotLog(PAttachment: JsonObject; PRecordId: RecordId)
    var
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        Base64Convert: Codeunit "Base64 Convert";
        lRecordRef: RecordRef;
        IsAttachment: Boolean;
        FileName: Text;
        Name: Text;
        AttachmentText: Text;
        FileSizeByte: Integer;
    begin
        //'is_attachment' => false,
        //'filename' => '',
        //'name' => '',
        //'attachment' => ''
        //'attachmentDataURI' => ''
        //'filesize' => 0
        IsAttachment := GetJsonToken(PAttachment, 'is_attachment').AsValue().AsBoolean();
        FileName := GetJsonToken(PAttachment, 'filename').AsValue().AsText();
        FileName := Base64Convert.FromBase64(FileName);
        Name := GetJsonToken(PAttachment, 'name').AsValue().AsText();
        AttachmentText := GetJsonToken(PAttachment, 'attachmentDataURI').AsValue().AsText();
        FileSizeByte := GetJsonToken(PAttachment, 'filesize').AsValue().AsInteger();

        IF IsAttachment THEN begin
            ICISupDragboxMgt.setConfiguration('ICI MAILROBOT INBOX');
            lRecordRef := PRecordId.GETRECORD();
            ICISupDragboxMgt.setRecordRef(lRecordRef);
            ICISupDragboxMgt.InsertFile(FileName, AttachmentText, FileSizeByte, true);
        end;
    end;

    procedure ProcessAllMailRobotLogs()
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        ICIMailrobotMailbox.SetRange(Active, true);
        IF ICIMailrobotMailbox.FindSet() THEN
            repeat
                ProcessAllMailRobotLogsForInbox(ICIMailrobotMailbox)
            until ICIMailrobotMailbox.Next() = 0;
    end;

    procedure ProcessAllMailRobotLogsForInbox(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox")
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
    begin
        IF not ICIMailrobotMailbox."Process Job Queue" THEN
            exit;
        ICIMailrobotLog.SetCurrentKey("Process State");
        ICIMailrobotLog.SETRANGE("Process State", ICIMailrobotLog."Process State"::Received);
        ICIMailrobotLog.SetRange("Mailrobot Mailbox Code", ICIMailrobotMailbox.Code);

        IF ICIMailrobotLog.FINDSET() then
            repeat
                if ICIMailrobotLog.CheckProcessing() THEN
                    ProcessMailRobotLog(ICIMailrobotLog);
            until ICIMailrobotLog.Next() = 0;
    end;



    procedure ProcessMailRobotLog(var ICIMailrobotLog: Record "ICI Mailrobot Log")
    begin
        IF NOT TryProcessMailToExistingTicket(ICIMailrobotLog) THEN
            TryProcessMailToNewTicket(ICIMailrobotLog);
    end;

    procedure CleanupAllMailInboxes()
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        ICIMailrobotMailbox.SetRange(Active, true);
        IF ICIMailrobotMailbox.FINDSET() THEN
            repeat
                CleanupMailInbox(ICIMailrobotMailbox);
            until ICIMailrobotMailbox.Next() = 0;
    end;

    procedure CleanupMailInbox(var ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox")
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
        LogDateTime: DateTime;
    begin
        IF not ICIMailrobotMailbox."Cleanup Job Queue" THEN
            exit;
        ICIMailrobotLog.SetRange("Mailrobot Mailbox Code", ICIMailrobotMailbox.Code);

        IF ICIMailrobotMailbox."Log Duration" <> 0 then
            LogDateTime := CurrentDateTime() - ICIMailrobotMailbox."Log Duration";

        ICIMailrobotLog.SetFilter("Send Date", '..%1', LogDateTime);
        ICIMailrobotLog.SetFilter("Ticket No.", '<>%1', '');
        ICIMailrobotLog.DeleteAll();
    end;

    procedure TryProcessMailToNewTicket(ICIMailrobotLog: Record "ICI Mailrobot Log"): Boolean
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
        Contact: Record Contact;
        SearchContact: Record Contact;
        ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
        SubjectAsText: Text;
        BodyAsText: Text;
        lInStream: InStream;
        TicketNo: Code[20];
        ContactNo: Code[20];
        DefaultReferenceNo: Text;
        DefaultCategory1: Code[50];
        DefaultCategory2: Code[50];
        DefaultDepartmentCode: Code[10];
        MultipleContactsErr: Label 'Oops. Found more than one Contact for E-Mail Adress %1', Comment = '%1=EmailAddr|de-DE=Es wurden mehrere Kontakte zu dieser E-Mail Adresse %1 gefunden';
        MailProcessedLbl: Label 'Created Ticket %1 - %2', Comment = '%1=TicketNo Name;%2=Ticketdescription|de-DE=Ticket ''%1 - %2'' erstellt';
        ReferenceLbl: Label 'Mailrobot "%1"', Comment = '%1 = Mailrobor Log Entry No|de-DE=Mailrobot Nr. %1';
    begin
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT(FALSE);
        ICIMailrobotLog.CalcFields(Subject);
        ICIMailrobotLog.Subject.CreateInStream(lInStream);
        lInStream.Read(SubjectAsText);
        SubjectAsText := Base64Convert.FromBase64(SubjectAsText);
        SubjectAsText := CopyStr(SubjectAsText, 1, 250);

        CLEAR(lInStream);
        ICIMailrobotLog.CalcFields(Body, "Top Level Domain Known");
        ICIMailrobotLog.Body.CreateInStream(lInStream);
        lInStream.Read(BodyAsText);
        BodyAsText := Base64Convert.FromBase64(BodyAsText);

        case ICIMailrobotLog."Contact Mapping" of
            "ICI Mailrobot Contact Mapping"::Single,
            "ICI Mailrobot Contact Mapping"::Manual:
                ContactNo := ICIMailrobotLog."Contact No.";
            "ICI Mailrobot Contact Mapping"::None:
                ContactNo := GetOrCreateContact(ICIMailrobotLog) // Copystr used for compiler warning
            else // eg multiple or blacklisted contacts
                ContactNo := '';
        end;

        IF ContactNo = '' then begin // Multiple Contacts with Same Mail found
            ICIMailrobotLog."Process State" := ICIMailrobotLog."Process State"::Error;
            ICIMailrobotLog."Process Text" := Copystr(StrSubstNo(MultipleContactsErr, ICIMailrobotLog.Sender), 1, 50);
            ICIMailrobotLog."Contact Mapping" := ICIMailrobotLog."Contact Mapping"::Error;
            EXIT(false);
        end;

        // if top level domain is known -> Add contact to this company
        // IF ICIMailrobotLog."Top Level Domain Known" THEN begin
        //     Contact.GET(ContactNo);
        //     IF Contact.Type = Contact.Type::Person then begin
        //         SearchContact.SetRange("ICI E-Mail TLD", ICIMailrobotLog."Top Level Domain");
        //         IF SearchContact.FindLast() then begin // XXX - Maybe check if top level domain is not in 2 different companys
        //             Contact.VALIDATE("Company No.", SearchContact."Company No.");
        //             Contact.Modify();
        //         end;
        //     end;
        // end;

        ICIMailrobotMailbox.GET(ICIMailrobotLog."Mailrobot Mailbox Code");
        DefaultCategory1 := ICIMailrobotMailbox."Default Category 1";
        DefaultCategory2 := ICIMailrobotMailbox."Default Category 2";
        DefaultReferenceNo := STRSUBSTNO(ReferenceLbl, ICIMailrobotLog."Entry No.");
        DefaultDepartmentCode := ICIMailrobotMailbox."Default Department";

        TicketNo := CopyStr(CreateTicketFromNewMail(SubjectAsText, BodyAsText, DefaultReferenceNo, DefaultCategory1, DefaultCategory2, DefaultDepartmentCode, ContactNo, ICIMailrobotLog), 1, 20); // Copystr used for compiler warning
        AddMailAttachmentsToTicket(TicketNo, ICIMailrobotLog);
        COMMIT();

        ICIMailrobotLog.GET(ICIMailrobotLog."Entry No.");

        ICIMailrobotLog.Validate("Ticket No.", TicketNo);

        ICIMailrobotLog.VALIDATE("Process State", ICIMailrobotLog."Process State"::Processed);
        ICIMailrobotLog.VALIDATE("Process Text", COPYSTR(STRSUBSTNO(MailProcessedLbl, TicketNo, SubjectAsText), 1, 50));
        ICIMailrobotLog.Modify(true);
        COMMIT();

        IF ICISupportTicket.GET(TicketNo) THEN BEGIN
            // Set Mailrobot Default User
            IF ICIMailrobotMailbox."Support User ID" <> '' THEN
                ICISupportTicket.VALIDATE("Support User ID", ICIMailrobotMailbox."Support User ID")
            else
                ICISupportTicket.Validate("Support User ID", ICIFirstAllocationMatrix.GetFirstAllocation(ICISupportTicket, true));

            ICISupportTicket.SetModifiedByContact(ICISupportTicket."Current Contact No.");
            ICISupportTicket.Modify(true);
            IF ICIMailrobotMailbox."Open Created Tickets" then
                ICISupportProcessMgt.OpenTicket(TicketNo, true); // Hide Confirm Msg
        END;
        EXIT(TRUE);
    end;

    procedure GetOrCreateContact(ICIMailrobotLog: Record "ICI Mailrobot Log") ContactNo: Code[20]
    var
        Contact: Record Contact;
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
        ContactCompanyNo: Code[20];
        ContactCount: Integer;
        MultipleContactsErr: Label 'Oops. Found more than one Contact for E-Mail Adress %1', Comment = '%1=EmailAddr|de-DE=Es wurden mehrere Kontakte zu dieser E-Mail Adresse %1 gefunden';
        FirstName: Text;
        Surname: Text;
        EMail: Text;
        PhoneNo: Text;
        Salutation: Text;
    begin

        FirstName := COPYSTR(ICIMailrobotLog.Sender, 1, STRPOS(ICIMailrobotLog.Sender, '@') - 1); // Part of E-Mail address before the @ Symbol
        Surname := '';
        PhoneNo := '';
        EMail := ICIMailrobotLog.Sender;

        IF ICIMailrobotMailbox.GET(ICIMailrobotLog."Mailrobot Mailbox Code") THEN
            Salutation := ICIMailrobotMailbox."Default Salutation";

        ICISupportMailSetup.GET();

        Contact.SETRANGE("Search E-Mail", UpperCase(EMail));
        ContactCount := Contact.Count();

        IF GuiAllowed AND (ContactCount > 1) THEN
            ERROR(MultipleContactsErr, EMail);
        IF (ContactCount > 1) THEN
            EXIT('');

        IF ContactCount = 1 then begin
            Contact.FindFirst();
            ContactNo := Contact."No.";
            EXIT;
        end;

        CLEAR(Contact);
        Contact.INIT();
        Contact.Validate("No.", '');
        Contact.INSERT(TRUE);
        Contact.Validate(Type, Contact.Type::Person);

        ContactCompanyNo := ICIMailrobotMailbox."Contact Company No.";
        IF ContactCompanyNo = '' THEN
            ContactCompanyNo := CreateCompanyContact(ICIMailrobotLog);

        Contact.Validate("Company No.", ContactCompanyNo);

        Contact.Validate("First Name", COPYSTR(FirstName, 1, 30));
        Contact.Validate(Surname, COPYSTR(Surname, 1, 30));
        Contact.Validate("E-Mail", COPYSTR(EMail, 1, 80));
        Contact.Validate("Phone No.", COPYSTR(PhoneNo, 1, 30));
        Contact.Validate("ICI Support Active", true);
        Contact.Validate("ICI Source Mailrobot Log", ICIMailrobotLog."Entry No.");
        Contact.Validate("Mailrobot Source Mailbox Code", ICIMailrobotMailbox.Code);
        if ICIMailrobotMailbox."Default Language Code" <> '' then
            Contact.Validate("Language Code", ICIMailrobotMailbox."Default Language Code");

        case Salutation of
            'M':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code M");
            'F':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code F");
            'D':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D");
            else
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D"); //Fall back to Diverse
        end;

        Contact.Modify(true);
        ContactNo := Contact."No.";
        EXIT;
    end;

    procedure CreateCompanyContact(var ICIMailrobotLog: Record "ICI Mailrobot Log"): Code[20]
    var
        Contact: Record Contact;
        ContactCompanyName: Text;
    begin
        ContactCompanyName := CopyStr(ICIMailrobotLog.Sender, StrPos(ICIMailrobotLog.Sender, '@') + 1, 80);

        IF STRLEN(DELCHR(ContactCompanyName, '=', DELCHR(ContactCompanyName, '=', '.'))) = 1 then // Only one occurrence of . as in support@ic-innovative.de. We want the company name to be ic-innovative and not ic-innovative.de
            ContactCompanyName := CopyStr(ContactCompanyName, 1, StrPos(ContactCompanyName, '.') - 1);

        ContactCompanyName := CONVERTSTR(ContactCompanyName, '-', ' ');

        Contact.INIT();
        Contact.Validate("No.", '');
        Contact.INSERT(TRUE);
        Contact.Validate(Type, Contact.Type::Company);
        Contact.Validate(Name, COPYSTR(ContactCompanyName, 1, 100));
        Contact.Validate("ICI Support Active", true);
        Contact.Validate("ICI Source Mailrobot Log", ICIMailrobotLog."Entry No.");
        Contact.Validate("Mailrobot Source Mailbox Code", ICIMailrobotLog."Mailrobot Mailbox Code");
        Contact.Modify(true);
        Exit(Contact."No.");
    end;

    procedure CreateTicketFromNewMail(Description: Text; Message: Text; ReferenceNo: Text; Category1: Text; Category2: Text; DepartmentCode: Code[10]; ContactNo: Text; ICIMailrobotLog: Record "ICI Mailrobot Log"): Text
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
    begin
        ICISupportTicket.Init();
        ICISupportTicket.Validate("No.");
        ICISupportTicket.INSERT(TRUE);
        ICISupportTicket.Validate(Description, COPYSTR(Description, 1, 250));
        // ICISupportTicket.Validate("Reference No.", COPYSTR(ReferenceNo, 1, 50));

        Contact.GET(ContactNo);

        case Contact.Type of
            Contact.Type::Company:
                begin
                    ICISupportTicket.Validate("Company Contact No.", Contact."No.");
                    ICISupportTicket.Validate("Current Contact No.", Contact."No.");
                end;
            Contact.Type::Person:
                begin
                    ICISupportTicket.Validate("Company Contact No.", Contact."Company No.");
                    ICISupportTicket.Validate("Current Contact No.", Contact."No.");
                end;

        end;

        ICISupportTicket.Validate("Category 1 Code", COPYSTR(Category1, 1, 50));
        ICISupportTicket.Validate("Category 2 Code", COPYSTR(Category2, 1, 50));
        ICISupportTicket.Validate("Department Code", DepartmentCode);
        ICISupportTicket.Validate("Source Mailrobot Log", ICIMailrobotLog."Entry No.");
        ICISupportTicket.Validate("Mailrobot Source Mailbox Code", ICIMailrobotLog."Mailrobot Mailbox Code");

        ICISupportTicket.Modify(true);

        ICISupportTicketLogMgt.SaveTicketContactMessage(ICISupportTicket."No.", Message, Contact."No.");

        exit(ICISupportTicket."No.");
    end;

    procedure TryProcessMailToExistingTicket(ICIMailrobotLog: Record "ICI Mailrobot Log"): Boolean
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
        SubjectAsText: Text;
        BodyAsText: Text;
        lInStream: InStream;
        TicketNo: Code[20];
        CompanyNo: Code[20];
        CurrentContactNo: Code[20];
        NoTicketNoErr: Label 'Could not find a Ticket No in Subject "%1"', Comment = '%1 = Subject|de-DE=Keine Ticketnr. in Betreff "%1" gefunden';
        TicketDoesNotExistsErr: Label 'Ticket No "%1" does not exist', Comment = '%1=TicketNo|de-DE=Ticketnr. "%1" existiert nicht';
        ContactNotFoundErr: Label 'Could not find Contact from E-Mail Adress "%1"', Comment = '%1=E-Mail Address|de-DE=Konnte keinen Kontakt zur E-Mail Addresse %1 finden';
        MailProcessedLbl: Label 'Added Msg from "%1" to Ticket "%2"', Comment = '%1=Contact Name;%2=TicketNo|de-DE=Nachricht von "%1" in Ticket "%2" gespeichert';
    begin
        ICIMailrobotLog.CalcFields(Subject);
        ICIMailrobotLog.Subject.CreateInStream(lInStream);
        lInStream.Read(SubjectAsText);
        SubjectAsText := Base64Convert.FromBase64(SubjectAsText);
        SubjectAsText := CopyStr(SubjectAsText, 1, 250);

        TicketNo := GetTicketnoFromEmail(SubjectAsText);
        IF TicketNo = '' then begin
            ICIMailrobotLog.VALIDATE("Process State", ICIMailrobotLog."Process State"::Error);
            ICIMailrobotLog.VALIDATE("Process Text", COPYSTR(StrSubstNo(NoTicketNoErr, SubjectAsText), 1, 50));
            ICIMailrobotLog.Modify(true);
            EXIT(False);
        end;

        IF NOT ICISupportTicket.GET(TicketNo) THEN BEGIN
            ICIMailrobotLog.VALIDATE("Process State", ICIMailrobotLog."Process State"::Error);
            ICIMailrobotLog.VALIDATE("Process Text", COPYSTR(StrSubstNo(TicketDoesNotExistsErr, TicketNo), 1, 50));
            ICIMailrobotLog.Modify(true);
            EXIT(False);
        END;

        IF NOT GetContactfromEmail(ICIMailrobotLog.Sender, CompanyNo, CurrentContactNo) THEN begin
            ICIMailrobotLog.VALIDATE("Process State", ICIMailrobotLog."Process State"::Error);
            ICIMailrobotLog.VALIDATE("Process Text", CopyStr(StrSubstNo(ContactNotFoundErr, ICIMailrobotLog.Sender), 1, 50));
            ICIMailrobotLog.Modify(true);
            EXIT(False);
        end;

        IF NOT Contact.GET(CurrentContactNo) then
            Contact.GET(CompanyNo);

        CLEAR(lInStream);
        ICIMailrobotLog.CalcFields(Body);
        ICIMailrobotLog.Body.CreateInStream(lInStream);
        lInStream.Read(BodyAsText);
        BodyAsText := Base64Convert.FromBase64(BodyAsText);

        // Reopen Ticket if Closed
        IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Closed then
            ICISupportProcessMgt.OpenTicket(TicketNo, true); // Hide Open Confirm

        ICISupportTicketLogMgt.SaveTicketContactMessage(TicketNo, BodyAsText, Contact."No.");
        AddMailAttachmentsToTicket(TicketNo, ICIMailrobotLog);


        ICIMailrobotLog.Validate("Ticket No.", TicketNo);
        ICIMailrobotLog.VALIDATE("Process State", ICIMailrobotLog."Process State"::Processed);
        ICIMailrobotLog.VALIDATE("Process Text", COPYSTR(STRSUBSTNO(MailProcessedLbl, Contact.Name, ICISupportTicket."No."), 1, 50));
        ICIMailrobotLog.Modify(true);
        COMMIT();
        EXIT(True);

    end;

    local procedure AddMailAttachmentsToTicket(TicketNo: Code[20]; var ICIMailrobotLog: Record "ICI Mailrobot Log")
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
    begin
        ICISupDragboxFile.SetCurrentKey("Record ID");
        ICISupDragboxFile.SetRange("Record ID", ICIMailrobotLog.RecordId);
        IF ICISupDragboxFile.FindSet() THEN
            repeat
                AddMailAttachmentToTicket(TicketNo, ICISupDragboxFile)
            until ICISupDragboxFile.Next() = 0;
    end;

    local procedure AddMailAttachmentToTicket(TicketNo: Code[20]; var ICISupDragboxFile: Record "ICI Sup. Dragbox File")
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        ICISupportTicket.GET(TicketNo);
        ICISupportTicket.SetModifiedByContact(ICISupportTicket."Current Contact No.");
        ICISupportTicket.Modify();

        ICISupDragboxFile.CalcFields(Data);

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External File");
        ICISupportTicketLog.VALIDATE("Record ID", ICISupportTicket.RecordId);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);
        ICISupportTicketLog.VALIDATE("Data Text", ICISupDragboxFile.Filename);
        ICISupportTicketLog.VALIDATE("Data Text 2", ICISupDragboxFile.Filename);
        ICISupportTicketLog.VALIDATE("Additional Text", FORMAT(ICISupDragboxFile.Size));
        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::Contact);
        ICISupportTicketLog.VALIDATE("Created By", ICISupportTicket."Current Contact No.");
        ICISupportTicketLog.Data := ICISupDragboxFile.Data;
        ICISupportTicketLog.Modify(TRUE);
    end;

    local procedure CheckIfDuplicateEntry(From: Text; SendDate: DateTime; Subject: Text): Boolean
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
        lInStream: InStream;
        Subject2: Text;
    begin
        ICIMailrobotLog.SetCurrentKey(Sender, "Send Date");
        ICIMailrobotLog.SetRange("Sender", CopyStr(UPPERCASE(From), 1, 80));
        ICIMailrobotLog.SetRange("Send Date", SendDate);
        IF ICIMailrobotLog.Count = 0 THEN
            EXIT(FALSE); // Noch nicht vorhanden

        IF ICIMailrobotLog.FINDSET() THEN
            repeat
                ICIMailrobotLog.CalcFields(Subject);
                ICIMailrobotLog.Subject.CreateInStream(lInStream);
                lInStream.Read(Subject2);
                IF Subject = Subject2 THEN
                    EXIT(TRUE); // Gleicher Absender, gleiche Zeit, gleicher Betreff -> Duplikat
            until ICIMailrobotLog.Next() = 0;
        EXIT(FALSE);
    end;

    // local procedure CheckIfBlackListed(From: Text): Boolean
    // var
    //     ICIMailrobotBlacklist: Record "ICI Mailrobot Blacklist";
    // begin
    //     // Email is Blacklisted
    //     ICIMailrobotBlacklist.SetRange("Search E-Mail", copystr(UpperCase(From), 1, 80));
    //     ICIMailrobotBlacklist.SetRange(Active, true);
    //     IF ICIMailrobotBlacklist.Count() > 0 THEN
    //         EXIT(true);

    //     // Check if TLD is Blacklisted
    //     ICIMailrobotBlacklist.SetRange("Search E-Mail", CopyStr(From, StrPos(From, '@'), 80));
    //     ICIMailrobotBlacklist.SetRange(Active, true);
    //     IF ICIMailrobotBlacklist.Count() > 0 THEN
    //         EXIT(true);

    //     EXIT(false);
    // end;

    procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text) jToken: JsonToken;
    var
        ErrorLbl: Label 'Could not find a token with key %1', Comment = '%1=Key|de-DE=Konnte kein Token mit Key "%1" finden';
    begin
        if not JsonObject.GET(TokenKey, jToken) then
            ERROR(ErrorLbl, TokenKey);
    end;


    procedure MakeRequest(uri: Text; payload: Text; Timeout: Duration) responseText: Text;
    var
        ICIMailrobotSetup: Record "ICI Mailrobot Setup";
        Base64Convert: Codeunit "Base64 Convert";
        lHttpClient: HttpClient;
        lHttpRequestMessage: HttpRequestMessage;
        lHttpResponseMessage: HttpResponseMessage;
        lHttpHeaders: HttpHeaders;
        lHttpContent: HttpContent;
        UserName: Text;
        Password: Text;
        AuthString: Text;
        AuthString1Txt: Label '%1:%2', Comment = 'Needed for Building Basig Http Auth String. %1 = Username; %2 = Password';
        AuthString2Txt: Label 'Basic %1', Comment = 'Needed for Building Basig Http Auth String. %1 is the Base64 Encoded String from AutoString1Txt ';
    begin
        ICIMailrobotSetup.GET();
        // HTTP Basic Authentification to access IMAP Connector
        UserName := ICIMailrobotSetup."IMAP Connector Auth. User";
        Password := ICIMailrobotSetup."IMAP Connector Auth. Password";
        AuthString := STRSUBSTNO(AuthString1Txt, UserName, Password);
        AuthString := Base64Convert.ToBase64(AuthString);
        AuthString := STRSUBSTNO(AuthString2Txt, AuthString);
        lHttpClient.DefaultRequestHeaders().Add('Authorization', AuthString);
        IF Timeout = 0 THEN
            Timeout := 1000 * 60 * 2; // 2 Minuten Timeout
        lHttpClient.Timeout(Timeout);

        // Add the payload to the content
        lHttpContent.WriteFrom(payload);

        // Retrieve the contentHeaders associated with the content
        lHttpContent.GetHeaders(lHttpHeaders);
        lHttpHeaders.Clear();
        lHttpHeaders.Add('Content-Type', 'application/json');
        //contentHeaders.Add('Content-Type', 'application/x-www-form-urlencoded');

        // Assigning content to request.Content will actually create a copy of the content and assign it.
        // After this line, modifying the content variable or its associated headers will not reflect in 
        // the content associated with the request message
        lHttpRequestMessage.Content := lHttpContent;

        lHttpRequestMessage.SetRequestUri(uri);
        lHttpRequestMessage.Method := 'POST';

        lHttpClient.Send(lHttpRequestMessage, lHttpResponseMessage);

        // Read the response content as json.
        lHttpResponseMessage.Content().ReadAs(responseText);
    end;

    PROCEDURE GetTicketnoFromEmail(Subject: Text) No: Code[20];
    VAR
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        NoSeriesLine: Record "No. Series Line";
        NoSerieno: Code[20];
        StartingNo: Text;
        Ix: Integer;
        Fixstr: Text;
        Varstr: Text;
        Currchar: Text;
        FixStrStart: Boolean;
        LFixStr: Integer;
        LVarstr: Integer;
        LSubject: Integer;
        Testint: Integer;
        SearchticketNo: Text;
        TicketFound: Boolean;
    BEGIN
        // Diese Methode sucht im Betreff nach einer Ticketnummer

        IF (Subject <> '') THEN
            // IS - Jr - 20151121 ---
            // im Betreff nach der TicketNummer suchen
            // Aufbau : in der NUmmernserie muss die Nummer mit mindestens einem Buchstaben beginnen.
            // anschlieáend darf eine reihe von Zahlen Folgen
            // Beispielfälle
            // Nummer      OK       FixStr    Varstr
            // T123456     OK       T         123456
            // 123456      nicht ok           123456   <-- Nummernserie Fehlerhaft, da kein Fix-Teil vorhanden ist --> eventuell diese Funktion noch erweitern ist aber ungenauer
            // T111-456789 ok       T111-     456789
            // T12-B45678  ok       T12-B     45678


            // Prüfungen wenn eine TicketNummer gefunden wurde
            // 1. Die TicketNummer muss in der Datenbank vorhanden sein
            // Kontakt ud Firma m?ssen aber nicht Zwingend šberein stimmen
            // da z.B. ein Systemhaus und ein Kunde bei einem Ticket betroffen sein k”nnen
            // Firma M?ller meldet einen Fehler
            // Systemhaus Mayer (Betreut Firma M?ller) schickt Informationen zu diesem Problem
            // damit sind 2 Verschiedene Firmen/Kontakte an einem Ticket besch„ftigt damit w„re eine zusammenf?hrung eigentlich nicht mehr m”glich
            // 1. die Nummernserie Suchen

            IF ICISupportSetup.GET() THEN BEGIN
                NoSerieno := ICISupportSetup."Support Ticket Nos.";
                IF (NoSerieno <> '') THEN BEGIN

                    // 2. anhand der Nummerserie den Aufbau der Ticketnummer suchen
                    Subject := UPPERCASE(Subject);
                    LSubject := STRLEN(Subject);
                    NoSeriesLine.ASCENDING(FALSE); // Descending Order damit das Aktuellste Startdatum oben steht. hier ist die Wahrscheinlichkeit am gr”áten
                                                   // Ausserdem k”nnte die NUmmernserie erweitert worden sein
                                                   // BSP von T000 auf T000000
                    NoSeriesLine.SETCURRENTKEY("Series Code", "Starting Date");
                    NoSeriesLine.SETRANGE("Series Code", NoSerieno);

                    IF NoSeriesLine.FINDSET() THEN
                        REPEAT
                            StartingNo := NoSeriesLine."Starting No.";
                            IF StartingNo <> '' THEN BEGIN
                                // in Starting No. Steht die Startnummer dieser NUmmernserie
                                // 1. den Festen und den Nummerischen anteil Trennen
                                Fixstr := '';
                                Varstr := '';
                                FixStrStart := FALSE; // wird True wenn der Fixe Teil beginnt

                                Ix := STRLEN(StartingNo);
                                REPEAT
                                    Currchar := COPYSTR(StartingNo, Ix, 1);
                                    IF (Currchar IN ['0' .. '9']) THEN
                                        Varstr := Currchar + Varstr
                                    ELSE
                                        FixStrStart := TRUE;
                                    Ix := Ix - 1;
                                UNTIL ((Ix = 0) OR (FixStrStart));

                                IF ((FixStrStart) AND (Varstr <> '')) THEN BEGIN
                                    // BIS IX ist der FIX-String der nicht erhöht wird
                                    Fixstr := COPYSTR(StartingNo, 1, Ix + 1);

                                    // jetzt im Betreff nach dem Fix-String suchen
                                    // wenn dieser vorhanden ist, m?ssen danach STRLE(Varstr) nur nummerische Zeichen kommen
                                    LFixStr := STRLEN(Fixstr);
                                    LVarstr := STRLEN(Varstr);
                                    Fixstr := UPPERCASE(Fixstr);
                                    // der Varstr hat nur Zahlen und braucht kein Upper

                                    FixStrStart := FALSE;

                                    Ix := 1;
                                    REPEAT
                                        Currchar := COPYSTR(Subject, Ix, LFixStr);
                                        IF (Currchar = Fixstr) THEN BEGIN
                                            // der Fixe-Teil wurde gefunden
                                            // jetzt noch den Variablen Teil der Ticketnummer suchen
                                            // ix2 := IX + LFixStr-LFixStr;
                                            Currchar := COPYSTR(Subject, Ix + LFixStr, LVarstr); // nur den Zahlenanteil auslesen
                                            IF (EVALUATE(Testint, Currchar)) THEN BEGIN // es ist nur eine Zahl in dem Var-Teil
                                                CLEAR(Testint);
                                                // hier könnte eine TicketNummer erkannt worden sein
                                                SearchticketNo := COPYSTR(Subject, Ix, LFixStr + LVarstr);
                                                IF ICISupportTicket.GET(SearchticketNo) THEN
                                                    // Die Gefundene Ticketnummer aus dem Betreff wurde in der Datenbank als Ticket erkannt
                                                    // Fälle für die Prüfung im Oberen Kommentar erweitern
                                                    EXIT(COPYSTR(SearchticketNo, 1, 20));
                                            END;
                                        END; // if (Currchar = FixStr)  then
                                        Ix := Ix + 1;
                                    // IS - JR - 20150929---
                                    // UNTIL ((Ix = LSubject-LFixStr) OR (TicketFound));
                                    UNTIL ((Ix >= LSubject - LFixStr) OR (TicketFound) OR ((Ix + LFixStr) > LSubject));
                                    // IS - JR - 20150929+++
                                END; // if ((FixStrStart) and (Varstr <> '')) then
                            END; // if "Starting No." <> '' then
                        UNTIL ((NoSeriesLine.NEXT() = 0) OR (TicketFound));

                END; // if (NoSerie <> '') then
            END // if Supportsetup.get then
    END;

    PROCEDURE GetContactfromEmail(Emailadress: Text; VAR CompanyNo: Code[20]; VAR CurrentContact: Code[20]) ContactFound: Boolean;
    VAR
        Contact: Record Contact;
        Suchadress: Text;
    BEGIN
        // Diese Methode ermittelt an Hand der EMail-Adresse den Entsprechenden Kontakt
        // akt. Problem : Die Email-Adresse ist noch Case-sensitiv

        IF (Emailadress <> '') THEN BEGIN
            // Alternativ k”nnte man noch ?ber SenderName versuchen den Namen zu finden
            Suchadress := UPPERCASE(Emailadress);
            Contact.SETFILTER("Search E-Mail", Suchadress);
            ContactFound := Contact.FINDFIRST();

            IF NOT ContactFound THEN BEGIN
                // rec_contact.SETFILTER("E-Mail 2",' = @' + '''' + Emailadress + '''');
                Contact.SETRANGE("E-Mail 2", UPPERCASE(Suchadress), LOWERCASE(Suchadress));
                ContactFound := Contact.FINDFIRST();
            END;

            IF Contact.COUNT > 0 THEN
                // Company,Person
                IF Contact.Type = Contact.Type::Company THEN BEGIN
                    // Es ist ein Unternehmenskontak (Ein Unternehmen)
                    CompanyNo := Contact."Company No.";
                    CurrentContact := '';
                END ELSE BEGIN
                    // es ist ein Kontakt zu einem Unternehmen
                    CurrentContact := Contact."No.";
                    CompanyNo := Contact."Company No.";
                END;
        END;
    END;

    procedure AddToBlacklist(pEMail: Text)
    var
        ICIMailrobotBlacklist: Record "ICI Mailrobot Blacklist";
    begin
        ICIMailrobotBlacklist.Init();
        ICIMailrobotBlacklist.INSERT(true);
        ICIMailrobotBlacklist.Validate("E-Mail", pEMail);
        ICIMailrobotBlacklist.Validate(Active, true);
        ICIMailrobotBlacklist.Modify(true);
    end;

}
