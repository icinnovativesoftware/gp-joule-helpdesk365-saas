table 56309 "ICI Mailrobot Setup"
{
    Caption = 'Mailrobot Setup', Comment = 'de-DE=Mailrobot Einrichtung';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Code"; Code[10])
        {
            Caption = 'Code';
            DataClassification = ToBeClassified;
        }
        field(67; "IMAP Connector URL"; Text[100])
        {
            Caption = 'Link to IMAP Connector', Comment = 'de-DE=Link zu IMAP Connector';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(68; "IMAP Connector Auth. User"; Text[50])
        {
            Caption = 'IMAP Connector Auth. User', Comment = 'de-DE=IMAP Connector Benutzer';
            DataClassification = CustomerContent;
        }
        field(69; "IMAP Connector Auth. Password"; Text[50])
        {
            Caption = 'IMAP Connector Auth. Password', Comment = 'de-DE=IMAP Connector Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
        }

        field(70; "EWS Connector URL"; Text[100])
        {
            Caption = 'Link to EWS Connector', Comment = 'de-DE=Link zu EWS Connector';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(71; "EWS Connector Auth. User"; Text[50])
        {
            Caption = 'EWS Connector Auth. User', Comment = 'de-DE=EWS Connector Benutzer';
            DataClassification = CustomerContent;
        }
        field(72; "EWS Connector Auth. Password"; Text[50])
        {
            Caption = 'EWS Connector Auth. Password', Comment = 'de-DE=EWS Connector Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
        }
    }
    keys
    {
        key(PK; "Code")
        {
            Clustered = true;
        }
    }

}
