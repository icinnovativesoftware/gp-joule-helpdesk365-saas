page 56356 "ICI Mail Log Ticket Factbox"
{

    Caption = 'Mailrobot Log Ticket Factbox', Comment = 'de-DE=Ticket - Details';
    PageType = CardPart;
    SourceTable = "ICI Support Ticket";

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("Current Contact No."; Rec."Current Contact No.")
                {
                    ToolTip = 'Specifies the value of the Current Contact No. field', Comment = 'de-DE=Personenkontaktnr.';
                    ApplicationArea = All;
                }
                field("Current Contact Name"; Rec."Curr. Contact Name")
                {
                    ToolTip = 'Specifies the value of the Current Contact Name field', Comment = 'de-DE=Kontakt';
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    begin
                        Rec.DrillDownContact(Rec."Current Contact No.");
                    end;
                }
                field("Customer Name"; Rec."Cust. Name")
                {
                    ToolTip = 'Specifies the value of the Customer Name field', Comment = 'de-DE=Debitorenname';
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    begin
                        Rec.DrillDownCustomer(Rec."Customer No.");
                    end;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ToolTip = 'Specifies the value of the Customer No. field', Comment = 'de-DE=Debitorennr.';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("Contact Phone No."; Rec."Contact Phone No.")
                {
                    ToolTip = 'Specifies the value of the Contact Phone No. field', Comment = 'de-DE=Telefonnr. der akt. Kontaktperson';
                    ApplicationArea = All;
                }
                field("Contact Mobile Phone No."; Rec."Contact Mobile Phone No.")
                {
                    ToolTip = 'Specifies the value of the Contact Mobile Phone No. field', Comment = 'de-DE=Mobiltelefonnr. der akt. Kontaktperson';
                    ApplicationArea = All;
                }
            }
        }
    }

}
