page 56303 "ICI Mailrobot Inbox List"
{

    ApplicationArea = All;
    Caption = 'Support Mailrobot Log', Comment = 'de-DE=Support Mailrobot Protokoll';
    PageType = List;
    SourceTable = "ICI Mailrobot Log";
    UsageCategory = History;
    //Editable = false;
    DeleteAllowed = true;
    SourceTableView = sorting("Entry No.") order(descending);
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Sender; Rec.Sender)
                {
                    ApplicationArea = All;
                    ToolTip = 'Sender', Comment = 'de-DE=Der Absender der E-Mail';
                    StyleExpr = ProcessStyleExpr;
                }
                field(ContactMapping; Rec."Contact Mapping")
                {
                    ApplicationArea = All;
                    Tooltip = 'Contact Mapping', Comment = 'Zuordnung zu Kontakten';
                }
                field("Contact No."; Rec."Contact No.")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Contact No', Comment = 'de-DE=Kontaktnr.';
                    TableRelation = Contact."No.";
                    trigger OnDrillDown()
                    var
                        myContact: Record Contact;
                    begin
                        IF myContact.GET(Rec."Contact No.") then
                            PAGE.RUN(PAGE::"Contact Card", myContact);
                    end;
                }
                field(SubjectAsText; SubjectAsText)
                {
                    ApplicationArea = All;
                    Caption = 'Subject', Comment = 'de-DE=Betreff';
                    ToolTip = 'Subject', Comment = 'de-DE=Der Betreff der E-Mail';
                    StyleExpr = ProcessStyleExpr;
                }
                field("Send Date"; Rec."Send Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Send Date', Comment = 'de-DE=Das Sendedatum der E-Mail';
                }
                field("Process State"; Rec."Process State")
                {
                    ApplicationArea = All;
                    ToolTip = 'Process State', Comment = 'de-DE=Der Bearbeitungsstatus der E-Mail';
                    StyleExpr = ProcessStyleExpr;
                }
                field("Ticket No."; Rec."Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';

                    trigger OnDrillDown()
                    var
                        SupportTicket: Record "ICI Support Ticket";
                    begin
                        IF SupportTicket.GET(Rec."Ticket No.") then
                            PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
                    end;
                }
                field("Top Level Domain Known"; Rec."Top Level Domain Known")
                {
                    ApplicationArea = All;
                    ToolTip = 'Top Level Domain Known', Comment = 'de-DE=Gibt an, ob die Top Level Domain bekannt ist';
                }
                field("Is Blacklisted"; Rec."Is Blacklisted")
                {
                    ApplicationArea = All;
                    ToolTip = 'Is Blacklisted', Comment = 'de-DE=Gibt an, ob der Absender auf der Blacklist steht';
                }
                field("Mailrobot Mailbox Code"; Rec."Mailrobot Mailbox Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Mailrobot Configuration Code', Comment = 'de-DE=Gibt an, in welchem Postfach die E-Mail eingegangen ist';
                }
                field(ID; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
                    StyleExpr = ProcessStyleExpr;
                }
                field("IMAP UID"; Rec."IMAP UID")
                {
                    Visible = false;
                    ApplicationArea = All;
                    StyleExpr = ProcessStyleExpr;
                    ToolTip = 'UID', Comment = 'de-DE=Die IMAP UID ist eine eindeutige Nr. pro Postfach. Achtung, wenn eine E-Mail in ein anderes Postfach verschoben wird, kann sich auch die UID ändern';
                }
                field("Process Text"; Rec."Process Text")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Process Text', Comment = 'de-DE=Ein Kommentar zur Bearbeitung';
                }
                field("Top Level Domain"; Rec."Top Level Domain")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Top Level Domain', Comment = 'de-DE=Top Level Domain';
                }
            }
        }
        area(FactBoxes)
        {
            part("ICI Mailrobot Text Editor"; "ICI Mailrobot Text Editor")
            {
                ApplicationArea = All;
                SubPageLink = "Entry No." = field("Entry No.");
            }
            part("ICI Mail Log Ticket Factbox"; "ICI Mail Log Ticket Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Ticket No.");
            }
            part(ICIDragbox; "ICI Mailrobot Inbox Dragbox")
            {
                ApplicationArea = All;
                SubPageLink = "Entry No." = field("Entry No.");
            }

        }

    }
    actions
    {
        area(Processing)
        {
            action(ReadMail)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download Inboxes', Comment = 'de-DE=Alle Postfächer auslesen';
                Image = OutlookSyncFields;
                ToolTip = 'Download Inboxes', Comment = 'de-DE=Neue E-Mails aus allen aktiven Postfächern herunterladen';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    CurrPage.SaveRecord();
                    MailRobotMgt.DownloadAllMailboxes();
                    CurrPage.Update(false);
                end;
            }

            action(ProcessInbox)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Process all E-Mails', Comment = 'de-DE=Alle E-Mails verarbeiten';
                Image = Process;
                ToolTip = 'Process all E-Mails', Comment = 'de-DE=Alle E-Mails verarbeiten';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    //CurrPage.SaveRecord();
                    //CurrPage.Update(true);
                    CurrPage.Update(false);
                    MailRobotMgt.ProcessAllMailRobotLogs();
                    CurrPage.Update(false);
                end;
            }

            action(TryProcessSelected)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Process Selected E-Mail(s)', Comment = 'de-DE=Ausgewählte E-Mail verarbeiten';
                Image = Process;
                ToolTip = 'Process Selected E-Mail(s)', Comment = 'de-DE=Ausgewählte E-Mail verarbeiten';
                trigger OnAction()
                var
                    ICIMailrobotLog: Record "ICI Mailrobot Log";
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                    ConfirmRetryLbl: Label 'Do you want to process the E-Mail %1 from %2 again? This may lead to duplicates', Comment = '%1=ID;%2=Sender|de-DE=Wollen Sie die E-Mail %1 von %2 erneut verarbeiten? Das könnte zu Doppelten Tickets führen';
                begin
                    CurrPage.SaveRecord();
                    Currpage.SETSELECTIONFILTER(ICIMailrobotLog);
                    IF ICIMailrobotLog.FINDSET() THEN
                        REPEAT
                            IF ICIMailrobotLog."Process State" = ICIMailrobotLog."Process State"::Processed then begin
                                IF Confirm(STRSUBSTNO(ConfirmRetryLbl, ICIMailrobotLog."Entry No.", ICIMailrobotLog.Sender)) THEN
                                    MailRobotMgt.ProcessMailRobotLog(ICIMailrobotLog);
                            end else
                                MailRobotMgt.ProcessMailRobotLog(ICIMailrobotLog); // process received and error mails without confirm

                        UNTIL ICIMailrobotLog.NEXT() = 0;

                    CurrPage.Update(false);
                end;
            }
        }
        area(Navigation)
        {
            action(Blacklist)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Blacklist', Comment = 'de-DE=E-Mail Blacklist';
                Image = CancelAllLines;
                ToolTip = 'Blacklist', Comment = 'de-DE=Blacklist öffnen';
                RunObject = page "ICI Mailrobot Blacklist";
            }
            action(OpenTicket)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Open Ticket', Comment = 'de-DE=Ticket öffnen';
                ToolTip = 'Open Ticket', Comment = 'de-DE=Ticket öffnen';
                RunObject = page "ICI Support Ticket Card";
                RunPageLink = "No." = field("Ticket No.");
                Image = Task;
            }

        }
        area(Reporting)
        {
            action(DownloadEMail)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download E-Mail', Comment = 'de-DE=E-Mail Datei herunterladen';
                ToolTip = 'Download E-Mail', Comment = 'de-DE=E-Mail Datei herunterladen';
                Image = SaveasStandardJournal;

                trigger OnAction()
                var
                    ICISupDragboxFile: Record "ICI Sup. Dragbox File";
                    Base64Convert: Codeunit "Base64 Convert";
                    TempBLOB: Codeunit "Temp Blob";
                    Data: BigText;
                    MailFileContent: Text;
                    lInStream: InStream;
                    lInStream2: InStream;
                    lOutStream: OutStream;
                begin
                    ICISupDragboxFile.SetCurrentKey("Record ID");
                    ICISupDragboxFile.SetRange("Record ID", Rec.RecordId);
                    IF Not ICISupDragboxFile.Findlast() THEN
                        Error('');

                    ICISupDragboxFile.CalcFields(Data);
                    //ICISupDragboxFile.Data.Export(ICISupDragboxFile.Filename);

                    ICISupDragboxFile.Data.CreateInStream(lInStream, TextEncoding::UTF8);
                    lInStream.Readtext(MailFileContent, 27); // delete leading 27 Chars: data:message/rfc822;base64,
                    MailFileContent := '';
                    lInStream.Readtext(MailFileContent);

                    MailFileContent := Base64Convert.FromBase64(MailFileContent);
                    Data.AddText(MailFileContent);

                    TempBLOB.CreateOutStream(lOutStream, TextEncoding::UTF8);
                    Data.Write(lOutStream);
                    TempBLOB.CreateInStream(lInStream2, TextEncoding::UTF8);
                    MailFileContent := ICISupDragboxFile.Filename;
                    DownloadFromStream(
                        lInStream2,  // InStream to save
                        '',   // Not used in cloud
                        '',   // Not used in cloud
                        '',   // Not used in cloud
                        MailFileContent); // Filename is browser download folder

                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        Base64Convert: Codeunit "Base64 Convert";
        lInStream: InStream;
    begin
        Rec.CalcFields(Subject);
        Rec.Subject.CreateInStream(lInStream);
        lInStream.Read(SubjectAsText);
        SubjectAsText := Base64Convert.FromBase64(SubjectAsText);

        CLEAR(ProcessStyleExpr);
        IF Not Rec.CheckProcessing() THEN
            ProcessStyleExpr := 'Unfavorable';
    end;

    trigger OnModifyRecord(): Boolean
    begin
        if xRec."Contact No." <> Rec."Contact No." then
            if GuiAllowed then
                Rec.Validate("Contact Mapping", Rec."Contact Mapping"::Manual);
    end;


    var
        SubjectAsText: Text;
        ProcessStyleExpr: Text;

}
