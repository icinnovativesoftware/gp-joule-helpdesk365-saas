table 56308 "ICI Mailrobot Mailbox"
{
    Caption = 'Mailrobot Mailbox', Comment = 'de-DE=Mailrobot Postfach';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Mailrobot Mailboxes";
    DrillDownPageId = "ICI Mailrobot Mailboxes";


    fields
    {
        field(1; Code; Code[30])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "Connection Type"; Option)
        {
            Caption = 'Connection Type', Comment = 'de-DE=Verbindungsart';
            DataClassification = CustomerContent;
            OptionMembers = IMAP,EWS;
            OptionCaption = 'IMAP,EWS', Comment = 'de-DE=IMAP,EWS';
        }
        field(20; "IMAP Hostname"; Text[100])
        {
            Caption = 'IMAP Hostname', Comment = 'de-DE=IMAP Hostname';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(21; "IMAP User"; Text[50])
        {
            Caption = 'IMAP User', Comment = 'de-DE=IMAP Benutzer';
            DataClassification = CustomerContent;
        }
        field(22; "IMAP Password"; Text[50])
        {
            Caption = 'IMAP Password', Comment = 'de-DE=IMAP Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
        }
        // field(23; "Latest IMAP E-Mail UID"; Integer)
        // {
        //     Caption = 'Latest IMAP E-Mail UID', Comment = 'de-DE=Letzte E-Mail UID';
        //     DataClassification = CustomerContent;
        // }
        field(24; "Default Category 1"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Mailrobot Default Category 1', Comment = 'de-DE=Kategorie 1 Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''));
            trigger OnValidate()
            var
                ICISupportTicketCategory: Record "ICI Support Ticket Category";
            begin
                // Category 2 löschen, wenn Category 1 geleert wurde
                IF "Default Category 1" = '' then
                    "Default Category 2" := ''
                else
                    // Oder, wenn Category 1 nicht mehr die Parent Category von Category 2 ist
                    IF "Default Category 2" <> '' then
                        IF ICISupportTicketCategory.GET("Default Category 2") THEN
                            IF ICISupportTicketCategory."Parent Category" <> "Default Category 1" then
                                "Default Category 2" := '';
            end;
        }
        field(25; "Default Category 2"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Mailrobot Default Category 2', Comment = 'de-DE=Kategorie 2 Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Category Filter" = FIELD("Default Category 1"));
            trigger OnValidate()
            begin
                TestField("Default Category 1");
            end;
        }
        field(26; "Default Salutation"; Code[10])
        {
            Caption = 'Mailrobot Default Salutation', Comment = 'de-DE=Anredecode';
            DataClassification = CustomerContent;
            TableRelation = Salutation;
        }
        field(27; "Support User ID"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = "ICI Support User";
            Caption = 'Mailrobot Support User ID', Comment = 'de-DE=Supportbenutzer';
        }
        field(28; "Processing Type"; Option)
        {
            Caption = 'Mailrobot Processing Type', Comment = 'de-DE=Verarbeitungsregel';
            OptionMembers = All,"Known TLD",Existing;
            OptionCaption = ' All,Known TLD,Existing', Comment = 'de-DE=Alle E-Mails,Nur bekannte E-Maildomains,Nur bekannte Kontakte';
            DataClassification = CustomerContent;
        }
        field(29; "Default Department"; Code[10])
        {
            Caption = 'Default Department', Comment = 'de-DE=Abteilung';
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
        }
        field(30; "Contact Company No."; Code[20])
        {
            Caption = 'Anonymous Contact Company No.', Comment = 'de-DE=Unternehmensnr.';
            DataClassification = CustomerContent;
            TableRelation = Contact where(Type = const(Company));
        }
        field(31; "Contact Company Name"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup(Contact.Name Where("No." = FIELD("Contact Company No.")));
            Caption = 'Anonymous Contact Company Name', Comment = 'de-DE=Unternehmensname';
        }
        field(32; "No. of E-Mails Per Request"; Integer)
        {
            Caption = 'No. of E-Mails Per Request', Comment = 'de-DE=Anzahl der E-Mails pro Response';
            DataClassification = CustomerContent;
            InitValue = 25;
        }
        field(33; "Download Response"; Boolean)
        {
            Caption = 'Download Response', Comment = 'de-DE=Server Antwort herunterladen';
            DataClassification = CustomerContent;

        }
        field(34; "Request Timeout"; Duration)
        {
            Caption = 'Request Timeout', Comment = 'de-DE=Request Timeout';
            DataClassification = CustomerContent;
        }
        field(35; "Start Date"; Date)
        {
            Caption = 'Start Date', Comment = 'de-DE=Startdatum';
            DataClassification = CustomerContent;
        }
        field(36; "No. of E-Mails"; Integer)
        {
            Editable = false;
            FieldClass = FlowField;
            Caption = 'No. of E-Mails', Comment = 'de-DE=Anz. E-Mails';
            CalcFormula = count("ICI Mailrobot Log" where("Mailrobot Mailbox Code" = field(Code)));
        }
        field(37; "No. of E-Mails - Unprocessed"; Integer)
        {
            Editable = false;
            FieldClass = FlowField;
            Caption = 'No. of E-Mails - Unprocessed', Comment = 'de-DE=Anz. unverarbeiteter E-Mails';
            CalcFormula = count("ICI Mailrobot Log" where("Mailrobot Mailbox Code" = field(Code), "Process State" = const(Received)));
        }
        field(38; "No. of E-Mails - Processed"; Integer)
        {
            Editable = false;
            FieldClass = FlowField;
            Caption = 'No. of E-Mails - Processed', Comment = 'de-DE=Anz. verarbeiteter E-Mails';
            CalcFormula = count("ICI Mailrobot Log" where("Mailrobot Mailbox Code" = field(Code), "Process State" = const(Processed)));
        }
        field(39; "Log Duration"; Duration)
        {
            Caption = 'Log Duration', Comment = 'de-DE=Aufbewahrungsfrist';
            DataClassification = CustomerContent;
        }
        field(40; Active; Boolean)
        {
            Caption = 'Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(41; "EWS Hostname"; Text[100])
        {
            Caption = 'EWS Hostname', Comment = 'de-DE=EWS Hostname';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(42; "EWS User"; Text[50])
        {
            Caption = 'EWS User', Comment = 'de-DE=EWS Benutzer';
            DataClassification = CustomerContent;
        }
        field(43; "EWS Password"; Text[50])
        {
            Caption = 'EWS Password', Comment = 'de-DE=EWS Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
        }
        field(44; "EWS Public Folder Name"; Text[100])
        {
            Caption = 'Public Folder Name', Comment = 'de-DE=Öffentlicher Ordnername';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(45; "No. of Tickets"; Integer)
        {
            Editable = false;
            FieldClass = FlowField;
            Caption = 'No. of Tickets', Comment = 'de-DE=Anz. Tickets';
            CalcFormula = count("ICI Support Ticket" where("Mailrobot Source Mailbox Code" = field(Code)));
        }
        field(46; "No. of Tickets - Processing"; Integer)
        {
            Editable = false;
            FieldClass = FlowField;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Anz. Tickets - in Bearbeitung';
            CalcFormula = count("ICI Support Ticket" where("Mailrobot Source Mailbox Code" = field(Code), "Ticket State" = filter(<> Closed)));
        }
        field(47; "Send from Email Scenario"; Enum "Email Scenario")
        {
            DataClassification = CustomerContent;
            Caption = 'Send from Email Scenario', Comment = 'de-DE=Absenden über abweichendes E-Mail Szenario';
            InitValue = Default;
        }
        field(48; "Download Job Queue"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Download Job Queue', Comment = 'de-DE=Herunterladen';
        }
        field(49; "Process Job Queue"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Job Queue', Comment = 'de-DE=Verarbeiten';
        }
        field(50; "Cleanup Job Queue"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Cleanup Job Queue', Comment = 'de-DE=Bereinigen';
        }
        field(51; "Open Created Tickets"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Open Created Tickets', Comment = 'de-DE=Tickets eröffnen';
            trigger OnValidate()
            begin
                TestField("Default Category 1");
            end;
        }
        field(52; "Ignore Doubles"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Ignore Doubles', Comment = 'de-DE=Dubletten ignorieren';
        }
        field(53; "Default Language Code"; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Default Language Code', Comment = 'de-DE=Standard Sprachcode';
            TableRelation = Language.Code;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

}
