page 56354 "ICI Mailrobot Mailbox"
{

    Caption = 'Mailrobot Mailbox', Comment = 'de-DE=Mailrobot Postfach';
    PageType = Card;
    SourceTable = "ICI Mailrobot Mailbox";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code field', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field(Active; Rec.Active)
                {
                    ToolTip = 'Specifies the value of the Active field', Comment = 'de-DE=Aktiv';
                    ApplicationArea = All;
                }
                field("Processing Type"; Rec."Processing Type")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Processing Type field', Comment = 'de-DE=Mailrobot Verarbeitungsregel';
                    ApplicationArea = All;
                }
                field("Open Created Tickets"; Rec."Open Created Tickets")
                {
                    ToolTip = 'Specifies the value of the Open Created Tickets field', Comment = 'de-DE=Gibt an, ob die Erstellten Tickets direkt eröffnet werden sollen.';
                    ApplicationArea = All;
                }
                field("Log Duration"; Rec."Log Duration")
                {
                    ToolTip = 'Specifies the value of the Log Duration field', Comment = 'de-DE=Gibt die Aufbewahrungsfrist für E-Mails an, bevor sie gelöscht werden.';
                    ApplicationArea = All;
                }
                field("Send from Email Scenario"; Rec."Send from Email Scenario")
                {
                    ToolTip = 'Specifies the value of the Send from Email Scenario field', Comment = 'de-DE=Benachrichtigungsmails weden nicht mit dem Std. E-Mail-Szenario, sondern über das angegebene E-Mail Szenario versandt';
                    ApplicationArea = All;
                }
            }
            group(Defaults)
            {
                Caption = 'Defaults', Comment = 'de-DE=Vorbelegungen';

                field("Default Category 1"; Rec."Default Category 1")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Default Category 1 field', Comment = 'de-DE=Mailrobot Kategorie 1 Code';
                    ApplicationArea = All;
                }
                field("Default Category 2"; Rec."Default Category 2")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Default Category 2 field', Comment = 'de-DE=Mailrobot Kategorie 2 Code';
                    ApplicationArea = All;
                }
                field("Default Language Code"; Rec."Default Language Code")
                {
                    ToolTip = 'Specifies the default language when the Mailrobot creates a new Contact', Comment = 'de-DE=Bestimmt die Sprache, die der Mailrobot zur Kontakterstellung nutzt';
                    ApplicationArea = All;
                }
                field("Default Salutation"; Rec."Default Salutation")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Default Salutation field', Comment = 'de-DE=Mailrobot Anredecode';
                    ApplicationArea = All;
                }
                field("Support User ID"; Rec."Support User ID")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Support User ID field', Comment = 'de-DE=Mailrobot Supportbenutzer';
                    ApplicationArea = All;
                }
                field("Default Department"; Rec."Default Department")
                {
                    ToolTip = 'Specifies the value of the Default Department ', Comment = 'de-DE=Abeilung';
                    ApplicationArea = All;
                }
                field("Contact Company No."; Rec."Contact Company No.")
                {
                    ApplicationArea = All;
                    Tooltip = 'Contact Company No.', Comment = 'de-DE=Gibt an, welcher Unternehmenskontakt für Tickets dieser Konfiguration verwendet werden soll';

                    trigger OnValidate()
                    begin
                        Rec.CalcFields("Contact Company Name");
                    end;
                }
                field("Contact Company Name"; Rec."Contact Company Name")
                {
                    ApplicationArea = All;
                    Tooltip = 'Contact Company Name', Comment = 'de-DE=Gibt an, welcher Unternehmenskontakt für Tickets dieser Konfiguration verwendet werden soll';
                }
            }
            group(Connection)
            {
                Caption = 'Connection', Comment = 'de-DE=Verbindungsdaten';
                field("Connection Type"; Rec."Connection Type")
                {
                    ToolTip = 'Specifies the value of the Connection Type field', Comment = 'de-DE=Verbindungsart';
                    ApplicationArea = All;

                    trigger OnValidate()
                    begin
                        UpdateVisibility();
                    end;
                }
                group(IMAP)
                {
                    Caption = 'IMAP', Comment = 'de-DE=IMAP';
                    Visible = ShowIMAP;
                    field("IMAP Hostname"; Rec."IMAP Hostname")
                    {
                        ToolTip = 'Specifies the value of the IMAP Hostname field', Comment = 'de-DE=IMAP Hostname';
                        ApplicationArea = All;
                    }
                    field("IMAP User"; Rec."IMAP User")
                    {
                        ToolTip = 'Specifies the value of the IMAP User field', Comment = 'de-DE=IMAP Benutzer';
                        ApplicationArea = All;
                    }
                    field("IMAP Password"; Rec."IMAP Password")
                    {
                        ToolTip = 'Specifies the value of the IMAP Password field', Comment = 'de-DE=IMAP Passwort';
                        ApplicationArea = All;
                    }

                }
                group(EWS)
                {
                    Caption = 'EWS', Comment = 'de-DE=EWS';
                    Visible = ShowEWS;
                    field("EWS Hostname"; Rec."EWS Hostname")
                    {
                        ToolTip = 'Specifies the value of the EWS Hostname field', Comment = 'de-DE=EWS Hostname. Bei Office365 Konto: https://outlook.office365.com/EWS/Exchange.asmx';
                        ApplicationArea = All;
                    }
                    field("EWS Public Folder Name"; Rec."EWS Public Folder Name")
                    {
                        ToolTip = 'Specifies the value of the EWS Public Folder Name field', Comment = 'de-DE=Öffentlicher Ordner';
                        ApplicationArea = All;
                    }
                    field("EWS User"; Rec."EWS User")
                    {
                        ToolTip = 'Specifies the value of the IMAP User field', Comment = 'de-DE=IMAP Benutzer';
                        ApplicationArea = All;
                    }
                    field("EWS Password"; Rec."EWS Password")
                    {
                        ToolTip = 'Specifies the value of the IMAP Password field', Comment = 'de-DE=IMAP Passwort';
                        ApplicationArea = All;
                    }
                }


                field("No. of E-Mails Per Request"; Rec."No. of E-Mails Per Request")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails Per Request field', Comment = 'de-DE=Anzahl der E-Mails, die pro Request aus dem Postfach ausgelesen werden';
                    ApplicationArea = All;
                }
                field("Download Response"; Rec."Download Response")
                {
                    ToolTip = 'Specifies the value of the Download Response field', Comment = 'de-DE=Gibt an, ob beim Herunterladen der E-Mails die Antwort des Servers im Client zum Download bereit gestellt werden soll.';
                    ApplicationArea = All;
                }
                field("Request Timeout"; Rec."Request Timeout")
                {
                    ToolTip = 'Specifies the value of the Request Timeout field', Comment = 'de-DE=Gibt an, wie lange beim Herunterladen der E-Mails die Antwort des Servers gewartet werden soll.';
                    ApplicationArea = All;
                }
                field("Start Date"; Rec."Start Date")
                {
                    ToolTip = 'Specifies the value of the Start Date field', Comment = 'de-DE=Gibt das älteste Datum für E-Mails aus dem Postfach an, die geladen werden sollen.';
                    ApplicationArea = All;
                }
                field("Ignore Doubles"; Rec."Ignore Doubles")
                {
                    ToolTip = 'Specifies the value of the Ignore Doubles field', Comment = 'de-DE=Gibt an, ob E-Mails mit doppelter ID erneut heruntergeladen werden. Aktivieren reduziert die Datenlast drastisch. Vorsicht: Diese Option kann dazu fürhen, dass E-Mails ignoriert werden, wenn Elemente im Posteingang verschoben werden';
                    ApplicationArea = All;
                }
            }
            group(JobQueue)
            {
                Caption = 'Job Queue', Comment = 'de-DE=Aufgabenwarteschlange';
                field("Download Job Queue"; Rec."Download Job Queue")
                {
                    ToolTip = 'Specifies the value of the Download Job Queue field', Comment = 'de-DE=Gibt an, ob die Warteschlange E-Mails herunterladen soll.';
                    ApplicationArea = All;
                }
                field("Process Job Queue"; Rec."Process Job Queue")
                {
                    ToolTip = 'Specifies the value of the Process Job Queue field', Comment = 'de-DE=Gibt an, ob die Warteschlange E-Mails verarbeiten soll.';
                    ApplicationArea = All;
                }
                field("Cleanup Job Queue"; Rec."Cleanup Job Queue")
                {
                    ToolTip = 'Specifies the value of the Cleanup Job Queue field', Comment = 'de-DE=Gibt an, ob die Warteschlange E-Mails bereinigen soll.';
                    ApplicationArea = All;
                }
            }
        }
        area(FactBoxes)
        {
            part("ICI Mailrobot Mailbox Factbox"; "ICI Mailrobot Mailbox Factbox")
            {
                ApplicationArea = All;
                SubPageLink = Code = field(Code);
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ReadMail)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download Inbox', Comment = 'de-DE=Postfach auslesen';
                Image = OutlookSyncFields;
                ToolTip = 'Download Inbox', Comment = 'de-DE=Neue E-Mails aus dem aktuellen Postfach herunterladen';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    CurrPage.SaveRecord();
                    MailRobotMgt.DownloadMailboxToLog(Rec);
                    CurrPage.Update(false);
                end;
            }
            action(ClearLog)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Clear Log', Comment = 'de-DE=E-Mail Protokoll bereinigen';
                Image = ClearLog;
                ToolTip = 'Clear Log', Comment = 'de-DE=Löscht alle E-Mails des Postfachs, die älter die Aufbewahrungsfrist überschritten haben und nicht zu einem Ticket verarbeitet wurden.';
                trigger OnAction()
                var
                    ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
                    ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    ICIMailrobotMailbox.GET(Rec.Code);
                    ICIMailrobotMgt.CleanupMailInbox(ICIMailrobotMailbox);
                end;
            }
        }
        area(Creation)
        {
            action(CreateJobQueue)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Create Job Queue', Comment = 'de-DE=Aufgabenwarteschlange erzeugen';
                Image = JobTimeSheet;
                ToolTip = 'Create Job Queue', Comment = 'de-DE=Erzeugt einen Aufgabenwarteschlangenposten, der das Postfach - je nach einstellungen - periodisch ausliest/verarbeitet/bereinigt';

                trigger OnAction()
                var
                    JobQueueEntry: Record "Job Queue Entry";
                begin
                    JobQueueEntry.SETRANGE("Object ID to Run", CODEUNIT::"ICI Mailrobot Mgt.");
                    JobQueueEntry.SETRANGE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
                    JobQueueEntry.SETRANGE("Parameter String", Rec.Code);
                    IF NOT JobQueueEntry.IsEmpty THEN
                        EXIT;

                    Clear(JobQueueEntry);
                    JobQueueEntry.InitRecurringJob(10);
                    JobQueueEntry.VALIDATE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
                    JobQueueEntry.VALIDATE("Object ID to Run", CODEUNIT::"ICI Mailrobot Mgt.");
                    JobQueueEntry.Validate("Parameter String", Rec.Code);
                    JobQueueEntry.SetStatus(JobQueueEntry.Status::Ready);
                    IF NOT JobQueueEntry.Insert(true) THEN
                        JobQueueEntry.Modify();
                end;
            }
        }
        area(Navigation)
        {
            action("ICI Mailrobot Blacklist")
            {
                ApplicationArea = All;
                Image = CancelAllLines;
                Caption = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mail Blacklist';
                ToolTip = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mail Blacklist';
                RunObject = Page "ICI Mailrobot Blacklist";
            }

        }

    }

    trigger OnAfterGetRecord()
    begin
        UpdateVisibility();
    end;

    procedure UpdateVisibility()
    begin
        ShowIMAP := Rec."Connection Type" = Rec."Connection Type"::IMAP;
        ShowEWS := Rec."Connection Type" = Rec."Connection Type"::EWS;
    end;

    var
        ShowIMAP: Boolean;
        ShowEWS: Boolean;
}
