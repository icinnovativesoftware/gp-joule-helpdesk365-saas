page 56353 "ICI Mailrobot Mailboxes"
{

    ApplicationArea = All;
    Caption = 'Mailrobot Mailboxes', Comment = 'de-DE=Mailrobot Postfächer';
    PageType = List;
    Editable = false;
    SourceTable = "ICI Mailrobot Mailbox";
    UsageCategory = Administration;
    CardPageId = "ICI Mailrobot Mailbox";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code field', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("IMAP User"; Rec."IMAP User")
                {
                    ToolTip = 'Specifies the value of the IMAP User field', Comment = 'de-DE=IMAP Benutzer';
                    ApplicationArea = All;
                }
                field("Contact Company Name"; Rec."Contact Company Name")
                {
                    Tooltip = 'Contact Company No.', Comment = 'de-DE=Gibt an, welcher Unternehmenskontakt für Tickets dieser Konfiguration verwendet werden soll';
                    ApplicationArea = All;
                }
                field("Processing Type"; Rec."Processing Type")
                {
                    ToolTip = 'Specifies the value of the Mailrobot Processing Type field', Comment = 'de-DE=Mailrobot Verarbeitungsregel';
                    ApplicationArea = All;
                }
                field(Active; Rec.Active)
                {
                    ToolTip = 'Specifies the value of the Active field', Comment = 'de-DE=Aktiv';
                    ApplicationArea = All;
                }
            }
        }
        area(FactBoxes)
        {
            part("ICI Mailrobot Mailbox Factbox"; "ICI Mailrobot Mailbox Factbox")
            {
                ApplicationArea = All;
                SubPageLink = Code = field(Code);
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ReadMail)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download Inbox', Comment = 'de-DE=Postfach auslesen';
                Image = OutlookSyncFields;
                ToolTip = 'Download Inbox', Comment = 'de-DE=Postfach auslesen';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    CurrPage.SaveRecord();
                    MailRobotMgt.DownloadMailboxToLog(Rec);
                    CurrPage.Update(false);
                end;
            }
            action(ClearLog)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Clear Log', Comment = 'de-DE=E-Mail Protokoll bereinigen';
                Image = ClearLog;
                ToolTip = 'Clear Log', Comment = 'de-DE=Löscht alle E-Mails des Postfachs, die älter die Aufbewahrungsfrist überschritten haben und nicht zu einem Ticket verarbeitet wurden.';
                trigger OnAction()
                var
                    ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
                    ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    ICIMailrobotMailbox.GET(Rec.Code);
                    ICIMailrobotMgt.CleanupMailInbox(ICIMailrobotMailbox);
                end;
            }

        }
        area(Navigation)
        {
            action("ICI Mailrobot Blacklist")
            {
                ApplicationArea = All;
                Image = CancelAllLines;
                Caption = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mail Blacklist';
                ToolTip = 'ICI Mailrobot Blacklist', Comment = 'de-DE=E-Mail Blacklist';
                RunObject = Page "ICI Mailrobot Blacklist";
            }
        }
    }
}
