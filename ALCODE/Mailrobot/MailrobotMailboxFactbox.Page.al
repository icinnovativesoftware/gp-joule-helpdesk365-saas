page 56355 "ICI Mailrobot Mailbox Factbox"
{

    Caption = 'Mailrobot Mailbox Factbox', Comment = 'de-DE=Postfach - Statistik';
    PageType = CardPart;
    SourceTable = "ICI Mailrobot Mailbox";

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("No. of E-Mails"; Rec."No. of E-Mails")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails field', Comment = 'de-DE=Anz. E-Mails';
                    ApplicationArea = All;
                }
                field("No. of E-Mails - Processed"; Rec."No. of E-Mails - Processed")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails - Processed field', Comment = 'de-DE=Anz. verarbeiteter E-Mails';
                    ApplicationArea = All;
                }
                field("No. of E-Mails - Unprocessed"; Rec."No. of E-Mails - Unprocessed")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails - Unprocessed field', Comment = 'de-DE=Anz. unverarbeiteter E-Mails';
                    ApplicationArea = All;
                }
                field("No. of Tickets"; Rec."No. of Tickets")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets field', Comment = 'de-DE=Anz. Tickets';
                    ApplicationArea = All;
                }
                field("No. of Tickets - Processing"; Rec."No. of Tickets - Processing")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Processing field', Comment = 'de-DE=Anz. Tickets in Bearbeitung';
                    ApplicationArea = All;
                }
            }
        }
    }

}
