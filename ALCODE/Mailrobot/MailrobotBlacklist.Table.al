table 56297 "ICI Mailrobot Blacklist"
{
    Caption = 'Mailrobot Blacklist', Comment = 'de-DE=Mailrobot Blacklist';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(10; "Search E-Mail"; Code[80])
        {
            Caption = 'Search E-Mail', Comment = 'de-DE=Such E-Mail';
            DataClassification = SystemMetadata;
        }
        field(11; "E-Mail"; Text[80])
        {
            Caption = 'E-Mail', Comment = 'de-DE=E-Mail';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                "Search E-Mail" := UpperCase("E-Mail");
            end;
        }
        field(13; Active; Boolean)
        {
            Caption = 'Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
            InitValue = true;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

}
