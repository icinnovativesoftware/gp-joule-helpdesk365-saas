enum 56280 "ICI Mailrobot Contact Mapping"
{
    Extensible = true;

    value(0; "None")
    {
        Caption = 'None', Comment = 'de-DE=Kein';
    }
    value(1; Single)
    {
        Caption = 'Single', Comment = 'de-DE=Einzel';
    }
    value(2; Multiple)
    {
        Caption = 'Multiple', Comment = 'de-DE=Mehrfach';
    }
    value(3; Manual)
    {
        Caption = 'Manual', Comment = 'de-DE=manuell';
    }
    value(4; Error)
    {
        Caption = 'Error', Comment = 'de-DE=Fehler';
    }
    value(5; Blacklisted)
    {
        Caption = 'Blacklisted', Comment = 'de-DE=Auf Blacklist';
    }
}
