table 56288 "ICI Mailrobot Log"
{
    Caption = 'ICI Mailrobot Log', Comment = 'de-DE=Mailimportprotokoll';
    DataClassification = SystemMetadata;
    LookupPageId = "ICI Mailrobot Inbox List";
    DrillDownPageId = "ICI Mailrobot Inbox List";
    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(10; "IMAP UID"; Integer)
        {
            Caption = 'IMAP UID', Comment = 'de-DE=IMAP E-Mail UID';
            DataClassification = CustomerContent;
        }
        field(11; Sender; Text[80])
        {
            Caption = 'Sender', Comment = 'de-DE=Absender';
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                myContact: Record Contact;
            begin
                IF (Sender <> '') and (xRec.Sender <> Rec.Sender) THEN begin
                    "Top Level Domain" := CopyStr("Sender", StrPos("Sender", '@'), 80);
                    myContact.SetFilter("Search E-Mail", Sender);
                    if (myContact.Count = 1) and (myContact.FindFirst()) then
                        "Contact No." := myContact."No.";
                end;
            end;
        }
        field(12; "Send Date"; DateTime)
        {
            Caption = 'Send Date', Comment = 'de-DE=Absendedatum';
            DataClassification = CustomerContent;
        }
        field(13; Subject; Blob)
        {
            Caption = 'Subject', Comment = 'de-DE=Betreff';
            DataClassification = CustomerContent;
        }
        field(14; Body; Blob)
        {
            Caption = 'Body', Comment = 'de-DE=Inhalt';
            DataClassification = CustomerContent;
        }
        field(15; "Process State"; Option)
        {
            Caption = 'Process State', Comment = 'de-DE=Bearbeitungsstatus';
            DataClassification = SystemMetadata;
            OptionMembers = Received,Processed,Error;
            OptionCaption = 'Received,Processed,Error', Comment = 'de-DE=Empfangen,Verarbeitet,Fehler';
        }
        field(16; "Process Text"; Text[50])
        {
            Editable = false;
            Caption = 'Process Text', Comment = 'de-DE=Text';
            DataClassification = SystemMetadata;
        }
        field(17; "Ticket No."; Code[20])
        {
            Editable = false;
            Caption = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = SystemMetadata;
        }
        field(18; "Contact No."; Code[20])
        {
            Editable = true;
            Caption = 'Contact No.', Comment = 'de-DE=Kontaktnr.';
            TableRelation = Contact."No.";
            DataClassification = SystemMetadata;
        }
        field(19; "Top Level Domain"; Code[80])
        {
            // Editable = false;
            Caption = 'Top Level Domain', Comment = 'de-DE=Top Level Domain';
            DataClassification = CustomerContent;
        }
        field(20; "Top Level Domain Known"; Boolean)
        {
            Editable = false;
            Caption = 'Top Level Domain Known', Comment = 'de-DE=TLD Bekannt';
            FieldClass = FlowField;
            CalcFormula = exist(Contact where("ICI E-Mail TLD" = Field("Top Level Domain")));
        }
        field(21; "Is Blacklisted"; Boolean)
        {
            Editable = false;
            Caption = 'Is Blacklisted', Comment = 'de-DE=Auf Blacklist';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Mailrobot Blacklist" where("Search E-Mail" = Field(Sender), Active = const(true)));
        }
        field(22; "Mailrobot Mailbox Code"; Code[30])
        {
            Caption = 'Mailrobot Mailbox', Comment = 'de-DE=Mailrobot Postfach';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Mailbox";
        }
        field(23; "EWS UID"; Text[512])
        {
            //https://stackoverflow.com/questions/15770929/serviceid-uniqueid-maximum-length-and-format
            Caption = 'EWS UID', Comment = 'de-DE=EWS UID';
            DataClassification = CustomerContent;
        }
        field(24; "Is Domain Blacklisted"; Boolean)
        {
            Editable = false;
            Caption = 'Is Domain Blacklisted', Comment = 'de-DE=Domäne auf Blacklist';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Mailrobot Blacklist" where("Search E-Mail" = Field("Top Level Domain"), Active = const(true)));
        }
        field(25; "Contact Mapping"; Enum "ICI Mailrobot Contact Mapping")
        {
            Editable = false;
            Caption = 'Contact Mapping', Comment = 'de-DE=Zuordnung zu Kontakten';
            DataClassification = SystemMetadata;
        }
    }

    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
        key(SearchKey; Sender, "Send Date") { }
        key(SearchKey2; "Process State") { }
        key(SearchKeyIMAP; "Mailrobot Mailbox Code", "IMAP UID") { }
        key(SearchKeyEWS; "Mailrobot Mailbox Code", "EWS UID") { }


    }

    trigger OnDelete()
    var
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        lRecordRef: RecordRef;
    begin
        lRecordRef.GetTable(Rec);
        ICISupDragboxMgt.DeleteDragboxFilesForRec(lRecordRef);
    end;


    // Return True, if the Mail needs to be processed. False otherwise
    procedure CheckProcessing(): Boolean
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        CalcFields("Top Level Domain Known", "Is Blacklisted", "Is Domain Blacklisted");

        IF NOT ICIMailrobotMailbox.GET("Mailrobot Mailbox Code") THEN exit;
        Case ICIMailrobotMailbox."Processing Type" OF
            ICIMailrobotMailbox."Processing Type"::All:
                exit((Not "Is Blacklisted") AND (Not "Is Domain Blacklisted")); // Process if not Blacklisted
            ICIMailrobotMailbox."Processing Type"::Existing:
                exit((Not "Is Blacklisted") AND (Not "Is Domain Blacklisted") AND ("Contact No." <> '')); // Process if not Blacklisted and Contact exists
            ICIMailrobotMailbox."Processing Type"::"Known TLD":
                begin
                    if "Is Blacklisted" OR ("Is Domain Blacklisted") then exit(false);
                    exit("Top Level Domain Known" OR ("Contact No." <> '')); // Process if not Blacklisted and TLD is Known or contact already exists
                end;
        end;
    end;

}
