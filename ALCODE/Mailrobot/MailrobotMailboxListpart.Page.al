page 56358 "ICI Mailrobot Mailbox Listpart"
{

    Caption = 'Mailrobot Mailbox Listpart', Comment = 'de-DE=Mailrobot Postfächer';
    PageType = ListPart;
    SourceTable = "ICI Mailrobot Mailbox";
    SourceTableView = where(Active = const(true));
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Specifies the value of the Code field', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("No. of E-Mails"; Rec."No. of E-Mails")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails field', Comment = 'de-DE=Anz. E-Mails';
                    ApplicationArea = All;
                }
                field("No. of E-Mails - Processed"; Rec."No. of E-Mails - Processed")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails - Processed field', Comment = 'de-DE=Anz. verarbeiteter E-Mails';
                    ApplicationArea = All;
                }
                field("No. of E-Mails - Unprocessed"; Rec."No. of E-Mails - Unprocessed")
                {
                    ToolTip = 'Specifies the value of the No. of E-Mails - Unprocessed field', Comment = 'de-DE=Anz. unverarbeiteter E-Mails';
                    ApplicationArea = All;
                }
                field("No. of Tickets - Processing"; Rec."No. of Tickets - Processing")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Processing field', Comment = 'de-DE=Anz. Tickets - in Bearbeitung';
                    ApplicationArea = All;
                }
            }
        }
    }

}
