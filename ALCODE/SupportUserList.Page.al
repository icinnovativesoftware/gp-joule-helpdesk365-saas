page 56281 "ICI Support User List"
{

    ApplicationArea = All;
    Caption = 'Support User List', Comment = 'de-DE=HelpDesk-Benutzer';
    PageType = List;
    SourceTable = "ICI Support User";
    UsageCategory = Administration;
    CardPageId = "ICI Support User Card";
    Editable = false;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("User ID"; Rec."User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'User ID', Comment = 'de-DE=Benutzer-ID';

                }
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Name', Comment = 'de-DE=Name';
                }
                field("Salutation Code"; Rec."Salutation Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Salutation Code', Comment = 'de-DE=Anredecode';
                    Visible = false;
                }
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Sprachcode';
                    Visible = false;
                }
                field("Ticket Board Active"; Rec."Ticket Board Active")
                {
                    Visible = ShowTicketboard;
                    ToolTip = 'Ticketboard Active', Comment = 'de-DE=Ticketboard aktiv';
                    ApplicationArea = All;
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail', Comment = 'de-DE=E-Mail';
                }
                field("Mobile Phone No."; Rec."Mobile Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Mobile Phone No.', Comment = 'de-DE=Handynr.';
                    Visible = false;
                }
                field("Phone No."; Rec."Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Phone No.', Comment = 'de-DE=Telefonnr.';
                    Visible = false;
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Salesperson Code', Comment = 'de-DE=Verkäufercode';
                    Visible = false;
                }
                field(Queue; rec.Queue)
                {
                    ApplicationArea = All;
                }

                field("Dep. code"; Rec."Dep. Code")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Dep. Filter"; Rec."Dep. Filter")
                {
                    ApplicationArea = All;
                    ToolTip = 'Department Filter', Comment = 'Abteilungsfilter. Bei mehreren Abteilungen, das Trennzeichen | verwenden. Filtert die Kacheln und die Ticketliste. Wenn leer, wird kein Filter angewendet';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ICISendLogin)
            {
                Caption = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                ToolTip = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                ApplicationArea = All;
                Image = SendMail;
                Visible = PortalIntegration;

                trigger OnAction();
                var
                // ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                // LoginSentTxt: Label 'Logindata has been sent', Comment = 'de-DE=Zugangsdaten erfolgreich versendet.';
                // LoginNotSentTxt: Label 'Logindata has not been sent', Comment = 'de-DE=Zugangsdaten konnten nicht versendet werden.';
                begin
                    Error(NotInLicenseErr);
                    // CurrPage.SaveRecord();
                    // IF ICISupportMailMgt.SendUserLogin(Rec."User ID", GeneratePassword()) THEN
                    //     MESSAGE(LoginSentTxt)
                    // else
                    //     MESSAGE(LoginNotSentTxt);
                    // CurrPage.Update(false);
                end;
            }
            action(SentMails)
            {
                Caption = 'Sent Mails', Comment = 'de-DE=Gesendete E-Mails';
                ToolTip = 'Sent Mails', Comment = 'de-DE=Öffnet die gesendeten E-Mails zu diesem Benutzer';
                ApplicationArea = All;
                Image = Log;
                RunObject = Page "Sent Emails";
                trigger OnAction()
                var
                    EMail: Codeunit Email;
                begin
                    EMail.OpenSentEmails(Database::"ICI Support User", Rec.SystemId);
                end;
            }

            group(CreaeteSupportUser)
            {
                Caption = 'Create User', Comment = 'de-DE=Benutzer erzeugen';
                ToolTip = 'Create Supportuser from...', Comment = 'de-DE=Supportbenutzer erzeugen aus ...';
                Image = CreateMovement;
                action(CreateFromBCUser)
                {
                    Caption = 'Create from User', Comment = 'de-DE=aus BC-Benutzern';
                    ToolTip = 'Create Supportuser from BC-User Accounts', Comment = 'de-DE=Erzeugt Supportbenutzer aus BC-Benutzerkonten';
                    ApplicationArea = All;
                    Image = Create;


                    trigger OnAction();
                    var
                    begin
                        CurrPage.SaveRecord();
                        Rec.CreateFromBCUser();
                        CurrPage.update(false);
                    end;
                }
                action(CreateFromSalesperson)
                {
                    Caption = 'Create from Salesperson', Comment = 'de-DE=aus Verkäufern';
                    ToolTip = 'Create Supportuser from Salespersons', Comment = 'de-DE=Erzeugt Supportbenutzer aus Verkäufern';
                    ApplicationArea = All;
                    Image = Create;


                    trigger OnAction();
                    var
                    begin
                        CurrPage.SaveRecord();
                        Rec.CreateFromSalesperson();
                        CurrPage.update(false);
                    end;
                }
            }

        }
    }



    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF NOT ICISupportSetup.GET() THEN;
        PortalIntegration := ICISupportSetup."Portal Integration";
        ShowTicketboard := ICISupportSetup."Ticketboard active";
    end;

    var
        PortalIntegration: Boolean;
        NotInLicenseErr: Label 'Not licensed.', Comment = 'de-DE=Nicht Lizenziert.';
        ShowTicketboard: Boolean;

}
