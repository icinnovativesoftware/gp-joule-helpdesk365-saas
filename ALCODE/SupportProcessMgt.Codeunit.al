codeunit 56281 "ICI Support Process Mgt."
{


    // Try to find the right Process Stage for current Ticketstate
    // Seachorder:
    // 1. Stage with Same Ticketstate + "Initial"=True
    // 2. Stage with Same Ticketstate
    // 3. Stage with "Applies To Ticket Stage" corresponding to Ticketstate + Initial=true
    // 4. Stage with "Applies To Ticket Stage" corresponding to Ticketstate
    procedure InitTicketProcess(var ICISupportTicket: Record "ICI Support Ticket")
    var
        ICISupportProcess: Record "ICI Support Process";
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportTicket.TestField("Process Code");
        ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");

        ICISupportProcessStage.SetRange("Ticket State", ICISupportTicket."Ticket State");

        ICISupportProcessStage.SetRange(Initial, true);
        IF NOT ICISupportProcessStage.FindFirst() then begin
            ICISupportProcessStage.SetRange(Initial);
            IF NOT ICISupportProcessStage.FindFirst() THEN begin
                ICISupportProcessStage.SetRange(Initial, true);
                ICISupportProcessStage.SetRange("Ticket State");
                case ICISupportTicket."Ticket State" of
                    ICISupportTicket."Ticket State"::Preparation:
                        ICISupportProcessStage.SetRange("Applys to Ticket State", ICISupportProcessStage."Applys to Ticket State"::Prep);
                    ICISupportTicket."Ticket State"::Open, ICISupportTicket."Ticket State"::Processing:
                        ICISupportProcessStage.SetRange("Applys to Ticket State", ICISupportProcessStage."Applys to Ticket State"::"Open/Process");
                    ICISupportTicket."Ticket State"::Waiting:
                        ICISupportProcessStage.SetRange("Applys to Ticket State", ICISupportProcessStage."Applys to Ticket State"::WaitForAcceptance);
                    ICISupportTicket."Ticket State"::Closed:
                        ICISupportProcessStage.SetRange("Applys to Ticket State", ICISupportProcessStage."Applys to Ticket State"::Close);
                end;
                IF NOT ICISupportProcessStage.FindFirst() then begin
                    ICISupportProcessStage.SetRange(Initial);
                    IF NOT ICISupportProcessStage.FindFirst() THEN EXIT;
                end;
            end;
        end;
        ICISupportTicket.Validate("Process Stage", ICISupportProcessStage.Stage); // Add Initial Process Stage for Current Ticketstate

        ICISupportProcess.GET(ICISupportTicket."Process Code");
        IF (ICISupportTicket."Escalation Code" = '') AND (ICISupportProcess."Default Escalation Code" <> '') then
            ICISupportTicket.Validate("Escalation Code", ICISupportProcess."Default Escalation Code");

        IF (ICISupportProcess."Default Accounting Type" <> '') then
            ICISupportTicket.Validate("Accounting Type", ICISupportProcess."Default Accounting Type");

        // Kein Mofify, weil aufruf im Insert -> Validate Trigger
    end;

    procedure UpdateProcessStageFromStateChange(var ICISupportTicket: Record "ICI Support Ticket")
    var
        CurrentICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportTicket.TestField("Process Code");
        ICISupportTicket.TestField("Process Stage");
        CurrentICISupportProcessStage.GET(ICISupportTicket."Process Code", ICISupportTicket."Process Stage");
        IF (ICISupportTicket."Ticket State" = CurrentICISupportProcessStage."Ticket State") THEN
            exit;

        // Ausnahme bei offen/bearbeitung
        IF CurrentICISupportProcessStage."Applys to Ticket State" = CurrentICISupportProcessStage."Applys to Ticket State"::"Open/Process" then
            IF (ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Open) OR (ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation) then
                exit; // Ticket in Bearbeitung/Offen + Prozesstufe bez. auf Ticketstatus offen/bearbeitung -> keine änderung nötig

        InitTicketProcess(ICISupportTicket);
    end;

    procedure CheckMandatoryFields(var ICISupportTicket: Record "ICI Support Ticket")
    var
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        NoMsgLbl: Label 'Please write a Message before opening the Ticket', Comment = 'de-DE=Bitte erfassen Sie eine initiale Nachricht.';
    begin
        ICISupportSetup.GET();
        // Check Mandatory Fields
        ICISupportTicket.Testfield("Process Code");
        ICISupportTicket.TestField("Process Stage");
        ICISupportTicket.TestField(Description);
        ICISupportTicket.TestField("Category 1 Code");
        IF ICISupportSetup."Person Mandatory" THEN
            ICISupportTicket.TestField("Current Contact No.");
        ICISupportTicket.TestField("Support User ID");
        //ICISupportTicket.SetModifiedByUser(UserID());

        // Check E-Mail Adress Conctact
        // Check Salutation Contact
        if ICISupportTicket.Online then begin
            IF ICISupportSetup."Person Mandatory" then // Personenkontakt nehmen, wenn pflicht 
                Contact.GET(ICISupportTicket."Current Contact No.")
            else
                IF ICISupportTicket."Current Contact No." <> '' THEN // Personenkontakt nehmen, wenn angegeben, sonst unternehmen
                    Contact.GET(ICISupportTicket."Current Contact No.")
                else
                    Contact.GET(ICISupportTicket."Company Contact No.");


            Contact.TestField("E-Mail");
            Contact.TestField("Salutation Code");
        end;

        // Check E-Mail Adress User
        // Check Salutation User
        ICISupportUser.GET(ICISupportTicket."Support User ID");
        IF NOT ICISupportUser.Queue THEN begin
            ICISupportUser.TestField("E-Mail");
            ICISupportUser.TestField("Salutation Code");
        end;

        // Check for Initial Message
        IF ICISupportSetup."Initial Message Mandatory" then begin
            ICISupportTicketLog.SetRange("Support Ticket No.", ICISupportTicket."No.");
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External Message");
            IF ICISupportTicketLog.COUNT() = 0 then
                ERROR(NoMsgLbl);
        end;
    end;

    procedure ContactClosedTicket(TicketNo: Code[20]; ContactNo: Code[20])
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportTicket.GET(TicketNo);
        ICISupportTicket.SetModifiedByContact(ContactNo);

        ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        ICISupportProcessStage.SetRange("Ticket State", "ICI Ticket State"::Closed);
        IF NOT ICISupportProcessStage.FINDLAST() then begin
            ICISupportProcessStage.SetRange("Ticket State");
            ICISupportProcessStage.FindLast();
        end;

        ICISupportTicket.Validate("Process Stage", ICISupportProcessStage.Stage);
        ICISupportTicket.Modify(true);
    end;

    procedure ContactReopenedTicket(TicketNo: Code[20]; ContactNo: Code[20]; ReopenText: Text)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
    begin
        ICISupportTicket.GET(TicketNo);
        ICISupportTicket.SetModifiedByContact(ContactNo);
        ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        ICISupportProcessStage.SetRange("Ticket State", "ICI Ticket State"::Open);
        IF NOT ICISupportProcessStage.FindFirst() then begin
            ICISupportProcessStage.SetRange("Ticket State");
            ICISupportProcessStage.SetRange("Applys to Ticket State", ICISupportProcessStage."Applys to Ticket State"::"Open/Process");
            IF ICISupportProcessStage.FindFirst() THEN;
        end;

        ICISupportTicketLogMgt.SaveTicketContactMessage(TicketNo, ReopenText, ContactNo);
        ICISupportTicket.GET(TicketNo);

        IF ICISupportProcessStage.Stage <> 0 THEN
            ICISupportTicket.Validate("Process Stage", ICISupportProcessStage.Stage)
        else
            ICISupportTicket.Validate("Ticket State", "ICI Ticket State"::Open);

        ICISupportTicket.Modify(true);
    end;


    procedure OpenTicket(TicketNo: Code[20]; HideConfirm: Boolean)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ConfirmLbl: Label 'Do you want to open the Ticket?', Comment = 'de-DE=Wollen Sie das Ticket eröffnen?';
    begin
        IF GuiAllowed then
            IF NOT HideConfirm THEN
                IF NOT Confirm(ConfirmLbl) then
                    EXIT;
        ICISupportTicket.GET(TicketNo);
        ICISupportTicket.Validate("Ticket State", ICISupportTicket."Ticket State"::Open);
        ICISupportTicket.Modify(true);
    end;

    procedure AcceptTicket(TicketNo: Code[20])
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        ConfirmLbl: Label 'Do you want to accept the Ticket?', Comment = 'de-DE=Wollen Sie das Ticket annehmen?';
    begin
        IF GuiAllowed then
            IF NOT Confirm(ConfirmLbl) then
                EXIT;

        ICISupportTicket.GET(TicketNo);

        ICISupportUser.GetCurrUser(ICISupportUser);
        IF ICISupportTicket."Support User ID" <> ICISupportUser."User ID" THEN
            ICISupportTicket.Validate("Support User ID", ICISupportUser."User ID");
        ICISupportTicket.Modify(true);
        ICISupportTicket.Validate("Ticket State", ICISupportTicket."Ticket State"::Processing);
        ICISupportTicket.Modify(true);
    end;

    procedure WfATicket(TicketNo: Code[20])
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ConfirmLbl: Label 'Do you want to set the Ticket to wait for Acceptance?', Comment = 'de-DE=Wollen Sie das Ticket in Abnahmewartestellung setzen?';
    begin
        IF GuiAllowed then
            IF NOT Confirm(ConfirmLbl) then
                EXIT;
        ICISupportTicket.GET(TicketNo);
        ICISupportTicket.Validate("Ticket State", ICISupportTicket."Ticket State"::Waiting);
        ICISupportTicket.Modify(true);
    end;

    procedure CloseTicket(TicketNo: Code[20])
    var
        ICISupportTicket: Record "ICI Support Ticket";
        TodoMgt: Codeunit "ICI Todo Mgt.";
        ConfirmLbl: Label 'Do you want to close the Ticket?', Comment = 'de-DE=Wollen Sie das Ticket schließen?';
    begin
        IF GuiAllowed then
            IF NOT Confirm(ConfirmLbl) then
                EXIT;
        ICISupportTicket.GET(TicketNo);
        TodoMgt.CheckAllTasksFinished(ICISupportTicket);
        TodoMgt.InsertClosingInteractionLogEntry(ICISupportTicket);

        ICISupportTicket.Validate("Ticket State", ICISupportTicket."Ticket State"::Closed);
        ICISupportTicket.Modify(true);
        OnAfterCloseTicket(ICISupportTicket);
    end;



    [IntegrationEvent(true, false)]
    local procedure OnAfterCloseTicket(ICISupportTicket: Record "ICI Support Ticket");
    begin
    end;
}
