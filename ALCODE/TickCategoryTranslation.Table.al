table 56316 "ICI Tick. Category Translation"
{
    Caption = 'Tick. Category Translation';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Ticket Category Code"; Code[50])
        {
            Caption = 'Ticket Category Code', Comment = 'de-DE=Ticketkategoriecode';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket Category";
        }
        field(2; "Language Code"; Code[10])
        {
            Caption = 'Language Code', Comment = 'de-DE=Sprachcode';
            DataClassification = CustomerContent;
            TableRelation = Language;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Ticket Category Code", "Language Code")
        {
            Clustered = true;
        }
    }

}
