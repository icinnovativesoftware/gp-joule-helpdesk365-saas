page 56357 "ICI Process Stages"
{
    UsageCategory = None;
    Caption = 'Process States', Comment = 'de-DE=Prozessstufen';
    SourceTable = "ICI Support Process Stage";
    DelayedInsert = true;
    PageType = List;
    SourceTableView = SORTING("Process Code", Stage);

    layout
    {
        area(content)
        {
            group(Filter)
            {
                field("Only Curr. State"; OnlyCurrState)
                {
                    Caption = 'Only Curr. State', Comment = 'de-DE=Nur aktuellen Status anzeigen';
                    ToolTip = 'Only Curr. State', Comment = 'de-DE=Nur aktuellen Status anzeigen';
                    ApplicationArea = All;

                    trigger OnValidate()
                    begin
                        UpdateStateFilter();
                        CurrPage.UPDATE();
                    end;
                }
            }
            repeater(Group)
            {
                field("Process Code"; Rec."Process Code")
                {
                    Visible = false;
                    ToolTip = 'Process Code', Comment = 'de-DE=Prozesscode';
                    ApplicationArea = All;
                }
                field(Stage; Rec.Stage)
                {
                    ToolTip = 'Stage', Comment = 'de-DE=Stufe';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("Department Code"; Rec."Department Code")
                {
                    ApplicationArea = All;
                    Importance = Additional;
                    ToolTip = 'Department Code', Comment = 'de-DE=Abteilungscode';
                }
                field("Reset processing time"; Rec."Reset processing time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Reset processing time', Comment = 'de-DE=Gibt an, ob die Bearbeitungszeit zurückgesetzt werden soll';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    begin
        OnlyCurrState := TRUE;
        UpdateStateFilter();
    end;

    var
        OnlyCurrState: Boolean;

    local procedure UpdateStateFilter()
    var
        SupportTicket: Record "ICI Support Ticket";
    begin
        Rec.SETRANGE("Applys to Ticket State");

        IF OnlyCurrState THEN BEGIN
            IF Rec.GETFILTER("Ticket State Filter") <> '' THEN
                EVALUATE(SupportTicket."Ticket State", Rec.GETFILTER("Ticket State Filter"));

            CASE SupportTicket."Ticket State" OF
                SupportTicket."Ticket State"::Preparation:
                    Rec.SETRANGE("Applys to Ticket State", Rec."Applys to Ticket State"::Prep);
                SupportTicket."Ticket State"::Open, SupportTicket."Ticket State"::Processing:
                    Rec.SETRANGE("Applys to Ticket State", Rec."Applys to Ticket State"::"Open/Process");
                SupportTicket."Ticket State"::Waiting:
                    Rec.SETRANGE("Applys to Ticket State", Rec."Applys to Ticket State"::WaitForAcceptance);
                SupportTicket."Ticket State"::Closed:
                    Rec.SETRANGE("Applys to Ticket State", Rec."Applys to Ticket State"::Close);
            END;
        END;
    end;
}
