page 56350 "ICI First Allocation Matrix"
{

    ApplicationArea = All;
    Caption = 'First Allocation Matrix', Comment = 'de-DE=Ticketzuweisungsmatrix';
    PageType = List;
    SourceTable = "ICI First Allocation Matrix";
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Department Code"; Rec."Department Code")
                {
                    ToolTip = 'Specifies the value of the Department Code field', Comment = 'de-DE=Abteilungscode';
                    ApplicationArea = All;
                }
                field("Category 1 Code"; Rec."Category 1 Code")
                {
                    ToolTip = 'Specifies the value of the Category 1 Code field', Comment = 'Kategorie Code';
                    ApplicationArea = All;
                }
                field("Category 2 Code"; Rec."Category 2 Code")
                {
                    ToolTip = 'Specifies the value of the Category 2 Code field', Comment = 'Unterkategorie Code';
                    ApplicationArea = All;
                }
                field("Support User ID"; Rec."Support User ID")
                {
                    ToolTip = 'Specifies the value of the Support User ID field', Comment = 'Benutzer';
                    ApplicationArea = All;
                }
            }
        }
    }
}
