page 56284 "ICI Ticket Log CardP"
{

    Caption = 'ICI Ticket Log CardPart', Comment = 'de-DE=Log';
    ;
    PageType = CardPart;
    SourceTable = "ICI Support Ticket";

    layout
    {
        area(content)
        {
            usercontrol(Chat; "ICI Ticket Log")
            {
                ApplicationArea = All;

                trigger ControlReady()
                begin
                    ControlAddinReady := TRUE;
                    CurrPage.Chat.Init();
                    CurrPage.Chat.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
                end;
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Refresh)
            {
                ApplicationArea = All;
                ToolTip = 'Refresh', Comment = 'de-DE=Aktualisieren';
                Image = Refresh;

                trigger OnAction()
                begin
                    IF ControlAddinReady THEN begin
                        CurrPage.Chat.Init();
                        CurrPage.Chat.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
                    end;
                end;

            }


        }
    }
    trigger OnAfterGetRecord()
    begin
        IF ControlAddinReady THEN begin
            CurrPage.Chat.Init();
            CurrPage.Chat.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
        end;
    end;

    var
        ICICommunicationMgt: Codeunit "ICI Communication Mgt.";
        ControlAddinReady: Boolean;

}
