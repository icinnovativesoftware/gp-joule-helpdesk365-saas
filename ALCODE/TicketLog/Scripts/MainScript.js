function Init(){
    var div = document.getElementById("controlAddIn");
    div.innerHTML = "";
    div.style.overflowY = "auto"; 
}

function LoadChat(Data){
    debugger;
    var controlAddIn = document.getElementById("controlAddIn");

    var container = document.createElement('div');
    container.setAttribute('id', 'LogHistoryContainer');

    for (i = 0; i <= Data.length - 1; i++) {
        logDiv = createDataDiv(Data[i]);
        if (logDiv != null){
            container.appendChild(logDiv);
        }
    }
    controlAddIn.appendChild(container);
}

function createDataDiv(Data){
    //debugger;
    switch(Data.Type){
        case 0: return createStateDiv(Data); // State Change
        case 1: return createMsgDiv(Data); // Chat Message
        //case 2: return createDataMsgDiv(Data); // File
        //case 3: return createDataMsgDiv(Data); // Escalation
        //case 4: return createDataMsgDiv(Data); // Document

    }

}
function createMsgDiv(Data){
        // Data Structure:
    // ('Type', ICISupportTicketLog.Type.AsInteger());
    // ('CreatedBy', ICISupportTicketLog."Created By");
    // ('CreatedByType', ICISupportTicketLog."Created By Type");
    // ('CreationDate', ICISupportTicketLog."Creation Date");
    // ('EntryNo', ICISupportTicketLog."Entry No.");
    // ('Description',
    // CreationTime
    // Data
    // incoming or outgiong msg
    var msgTypeContainer = '';
    var msgTypeContent = '';
    switch(Data.CreatedByType){
        case 0: msgTypeContainer = 'outgoing_msg'; msgTypeContent = 'sent_msg_content'; break; // BC User has sent the Message 
        case 1: msgTypeContainer = 'incoming_msg'; msgTypeContent = 'received_msg_content'; break; // Contact has sent the Message
        default: msgTypeContainer = 'system_msg'; msgTypeContent = 'system_msg_content'; break; // System generated Message
    }

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    var MsgContent = Data.Data;
    var SenderInformation =  Data.CreatedBy + " | " + Data.CreatedByName;

    var container = document.createElement('div');
    container.className = msgTypeContainer;

    var msgDiv = document.createElement('div');
    msgDiv.className = msgTypeContent;
    
    var contentP = document.createElement('p');
    contentP.innerHTML = MsgContent;

    var timeDateSpan = document.createElement('span');
    timeDateSpan.className = 'time_date';
    timeDateSpan.innerHTML = DateString;

    var SenderInformationSpan = document.createElement('span');
    SenderInformationSpan.className = 'sender_information';
    SenderInformationSpan.innerHTML = SenderInformation;
    
    msgDiv.appendChild(SenderInformationSpan);
    msgDiv.appendChild(contentP);
    msgDiv.appendChild(timeDateSpan);
    
    container.appendChild(msgDiv);
    return container;
}
function createStateDiv(Data){
    // Data Structure:
// ('Type', ICISupportTicketLog.Type.AsInteger());
// ('CreatedBy', ICISupportTicketLog."Created By");
// ('CreatedByType', ICISupportTicketLog."Created By Type");
// ('CreationDate', ICISupportTicketLog."Creation Date");
// ('EntryNo', ICISupportTicketLog."Entry No.");
// ('Description',
    var container = document.createElement('div');
    container.className = 'state_row';

    var col1 = document.createElement('div');
    col1.className = 'state_column_1';
    var col2 = document.createElement('div');
    col2.className = 'state_column_2';
    var col3 = document.createElement('div');
    col3.className = 'state_column_3';

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    var MsgContent = Data.Data;
    var SenderInformation =  Data.CreatedByName;

    col1.innerHTML = DateString;
    col2.innerHTML = MsgContent;
    col3.innerHTML = SenderInformation;

    container.appendChild(col1);
    container.appendChild(col2);
    container.appendChild(col3);

    return container;

}