controladdin "ICI Ticket Log"
{
    Scripts = 'ALCODE\TicketLog\Scripts\MainScript.js';
    StartupScript = 'ALCODE\TicketLog\Scripts\startupScript.js';
    StyleSheets = 'ALCODE\TicketLog\Style\Chat.css';

    VerticalStretch = true;
    HorizontalStretch = true;
    MinimumHeight = 500;
    RequestedHeight = 500;

    // JS -> AL
    event ControlReady();

    // AL -> JS
    procedure Init();
    procedure LoadChat(data: JsonArray);

}
