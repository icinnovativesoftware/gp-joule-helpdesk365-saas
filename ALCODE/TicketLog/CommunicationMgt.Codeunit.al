codeunit 56278 "ICI Communication Mgt."
{

    procedure GetInitData(FullScreen: Boolean) InitData: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.Get();

        InitData.Add('FullScreen', FullScreen);

        InitData.Add('ColorContactMessage', ICISupportSetup."Color - Contact Message");
        InitData.Add('ColorUserMessage', ICISupportSetup."Color - User Message");
        InitData.Add('ColorInternalMessage', ICISupportSetup."Color - Internal Message");
        InitData.Add('DefaultInternally', ICISupportSetup."Communication - Default Intern");
    end;

    procedure GetChatDataForTicket(TicketNo: Code[20]) ChatData: JsonArray
    var
        //ICISupportTicket: Record "ICI Support Ticket";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        LogObject: JsonObject;
        lInStream: InStream;
        DataAsText: Text;
    begin
        ICISupportTicketLog.SetCurrentKey("Support Ticket No.", "Creation Timestamp");
        //ICISupportTicketLog.SetAscending("Creation Timestamp", TRUE);
        ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
        IF ICISupportTicketLog.FindSet(false) THEN
            REPEAT
                CLEAR(LogObject);
                Clear(DataAsText);

                LogObject.Add('Type', ICISupportTicketLog.Type.AsInteger());
                LogObject.Add('CreatedBy', ICISupportTicketLog."Created By");
                LogObject.Add('CreatedByName', ICISupportTicketLogMgt.GetTicketLogSenderName(ICISupportTicketLog."Entry No."));
                LogObject.Add('CreatedByType', ICISupportTicketLog."Created By Type");
                //LogObject.Add('CreationDate', ICISupportTicketLog."Creation Date");
                LogObject.Add('CreationDate', FORMAT(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'));
                //LogObject.Add('CreationTime', ICISupportTicketLog."Creation Time");
                LogObject.Add('CreationTime', FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2>'));
                LogObject.Add('EntryNo', ICISupportTicketLog."Entry No.");
                LogObject.Add('Description', ICISupportTicketLogMgt.GetTicketLogDescription(ICISupportTicketLog."Entry No."));
                LogObject.Add('SenderInformation', ICISupportTicketLogMgt.GetSenderInformation(ICISupportTicketLog."Entry No."));

                IF ICISupportTicketLog."Data Text 2" <> '' then
                    LogObject.Add('DataText', ICISupportTicketLog."Data Text 2") // Renamed Filename
                else
                    LogObject.Add('DataText', ICISupportTicketLog."Data Text");
                LogObject.Add('AdditionalText', ICISupportTicketLog."Additional Text");

                // Dont send Content of Files (Just EntryNo is needed for download process)
                IF (ICISupportTicketLog.Data.HasValue)
                AND (ICISupportTicketLog.Type <> ICISupportTicketLog.Type::"External File") THEN BEGIN
                    ICISupportTicketLog.CalcFields(Data);
                    ICISupportTicketLog.Data.CreateInStream(lInStream);
                    lInStream.Read(DataAsText);
                END;
                LogObject.Add('Data', DataAsText);

                ChatData.Add(LogObject);
            UNTIL ICISupportTicketLog.Next() = 0;
    end;

    procedure GetTicketTextModuleForTicket(TicketNo: Code[20]) TicketTextModuleData: JsonArray
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTextModule: Record "ICI Support Text Module";
        lICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lInStream: InStream;
        TextModuleBody: Text;
        LogObject: JsonObject;

    begin
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT;

        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;

        ICISupportTextModule.SetRange(Type, ICISupportTextModule.Type::Ticket);
        ICISupportTextModule.SetRange("Language Code", '');
        IF ICISupportTextModule.FINDSET() THEN
            REPEAT
                CLEAR(TextModuleBody);
                CLEAR(LogObject);
                GetTicketTextModule(ICISupportTextModule.Code, Contact."Language Code", lICISupportTextModule);

                IF lICISupportTextModule.Description <> '' then begin
                    LogObject.Add('Key', lICISupportTextModule.Description);
                    lICISupportTextModule.CalcFields(Data);
                    lICISupportTextModule.Data.CreateInStream(lInStream);
                    lInStream.Read(TextModuleBody);
                    ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
                    ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
                    ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
                    ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");
                    ICISupportMailMgt.GetUserPlaceholders(TempNameValueBuffer, ICISupportTicket."Support User ID");

                    TextModuleBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, TextModuleBody);
                    TextModuleBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, TextModuleBody); // Apply 2 Times to Resolve Footer

                    LogObject.Add('Value', TextModuleBody);

                    TicketTextModuleData.Add(LogObject);
                end;
            UNTIL ICISupportTextModule.Next() = 0;
    end;

    // XXX TODO use Dragbox Function
    procedure DownloadFile(EntryNo: Integer)
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        lInStream: InStream;
        FileName: Text;
        DownloadFileLbl: Label 'Download File ''%1''', Comment = 'de-DE=Datei ''%1'' herunterladen';

    begin
        ICISupportTicketLog.GET(EntryNo);
        ICISupportTicketLog.CalcFields(Data);
        FileName := ICISupportTicketLog."Data Text"; // Original Filename
        IF ICISupportTicketLog.Data.HasValue() THEN BEGIN
            ICISupportTicketLog.Data.CreateInStream(lInStream);
            File.DownloadFromStream(lInStream, StrSubstNo(DownloadFileLbl, FileName), '', '', FileName);
        END;
    end;

    procedure GetTicketTextModule(TextModuleCode: Code[30]; LanguageCode: Code[10]; var ICISupportTextModule: Record "ICI Support Text Module"): Boolean
    begin
        IF ICISupportTextModule.GET(TextModuleCode, LanguageCode, ICISupportTextModule.Type::"Ticket") THEN
            EXIT(TRUE)
        else
            EXIT(ICISupportTextModule.GET(TextModuleCode, '', ICISupportTextModule.Type::Ticket));
    end;
}
