table 56301 "ICI Support Time Log Comment"
{
    Caption = 'Support Time Log Line Comment';
    DrillDownPageID = "ICI Support Time Log Comments";
    LookupPageID = "ICI Support Time Log Comments";

    fields
    {
        field(1; "Support Ticket No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            Editable = false;
        }
        field(2; "Time Log Line No."; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Time Log Line No.', Comment = 'de-DE=Zeiterfassungszeilennr.';
            Editable = true;
        }
        field(3; "Line No."; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Line No', Comment = 'de-DE=Zeilennr.';
            Editable = true;
        }

        field(10; Description; Text[100])
        {
            DataClassification = CustomerContent;
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
        }
        field(11; "Employee No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Employee No.', Comment = 'de-DE=Mitarbeiternr.';

            Editable = true;
            NotBlank = true;
            TableRelation = Employee."No.";
        }
        field(12; "Create Date"; Date)
        {
            DataClassification = CustomerContent;
            Caption = 'Create Date', Comment = 'de-DE=Erstelldatum';
        }
        field(13; "ICI Support User"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'ICI Support User', Comment = 'de-DE=Supportbenutzer';
        }
    }

    keys
    {
        key(PQ; "Support Ticket No.", "Time Log Line No.", "Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
    trigger OnInsert()
    begin
        "ICI Support User" := COPYSTR(UserID(), 1, 50);
        "Create Date" := WorkDate();
    end;
}
