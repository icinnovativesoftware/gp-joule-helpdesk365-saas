page 56286 "ICI Ticket Communication CardP"
{

    Caption = 'ICI Communication CardPart', Comment = 'de-DE=Verlauf';
    PageType = CardPart;
    SourceTable = "ICI Support Ticket";

    layout
    {
        area(Content)
        {
            usercontrol(TicketCommunication; "ICI Ticket Communication CK4")
            {
                ApplicationArea = All;
                trigger ControlReady()

                begin
                    AddinReady := true;
                    CurrPage.TicketCommunication.Init(ICICommunicationMgt.GetInitData(FullScreen));

                    IF Rec."No." = '' THEN
                        CurrPage.TicketCommunication.SetReadOnly(true)
                    else begin
                        CurrPage.TicketCommunication.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
                        CurrPage.TicketCommunication.LoadTicketTextModule(ICICommunicationMgt.GetTicketTextModuleForTicket(Rec."No."));
                    end;
                end;

                trigger SaveRequested(Data: JsonObject)
                var
                    SupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                    jToken: JsonToken;
                    jVal: JsonValue;
                    dataText: Text;
                    internally: Boolean;
                    NoTextEnteredErr: Label 'No Text was Entered', Comment = 'de-DE=Keine Nachricht eingegeben';
                begin
                    // get Parameters from JSON
                    Data.Get('Text', jToken);
                    jVal := jToken.AsValue();
                    dataText := jVal.AsText();

                    Data.Get('Internally', jToken);
                    jVal := jToken.AsValue();
                    internally := jVal.AsBoolean();

                    IF dataText = '' THEN
                        Error(NoTextEnteredErr);
                    COMMIT();

                    SupportTicketLogMgt.SaveTicketMessage(Rec."No.", dataText, internally, true);
                    CurrPage.Update(false);
                end;

                trigger DownloadFile(Data: JsonObject)
                var
                    //CommunicationMgt: Codeunit "ICI Communication Mgt.";
                    DragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
                    recRef: RecordRef;
                    jToken: JsonToken;
                    jVal: JsonValue;
                    ID: Integer;
                    DragboxConfigurationCode: Code[30];
                    SendData: JsonObject;
                begin
                    // get Parameters from JSON
                    Data.Get('ID', jToken);
                    jVal := jToken.AsValue();
                    ID := jVal.AsInteger();

                    // Trigger Download
                    recRef.GetTable(Rec);
                    DragboxMgt.setRecordRef(RecRef);
                    DragboxConfigurationCode := CopyStr(Rec.TableName, 1, 30);
                    DragboxMgt.setConfiguration(DragboxConfigurationCode);
                    SendData := DragboxMgt.PrepareDownloadFile(ID);
                    CurrPage.TicketCommunication.SendFileToClient(SendData);
                end;

            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(SendMailWithEditor)
            {
                ApplicationArea = RelationshipMgmt;
                Caption = 'E-Mail Editor', Comment = 'de-DE=Nachricht Erfassen';
                Image = SendMail;

                ToolTip = 'Opens the E-Mail Editor', Comment = 'de-DE=Öffnet den E-Mail Editor';
                trigger OnAction()
                var
                    Contact: Record Contact;
                    ICISupportMailSetup: Record "ICI Support Mail Setup";
                    ICISupportTextModule: Record "ICI Support Text Module";
                    TempNameValueBuffer: Record "Name/Value Buffer" temporary;
                    ICISupportUser: Record "ICI Support User";
                    ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
                    ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    ICINotificationDialog: Page "ICI Notification Dialog";
                    SendToEMail: Text[250];
                    EMailSubject: Text;
                    EMailBody: Text;
                    lInStream: InStream;
                begin
                    ICINotificationDialogMgt.SetTicket(Rec);

                    ICISupportMailSetup.GET();
                    ICISupportUser.GET(Rec."Support User ID");
                    Contact.GET(Rec."Current Contact No.");

                    // Textvorlage Neie Nachricht Holen
                    IF ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail C New Message", Contact."Language Code", ICISupportTextModule) THEN begin
                        EMailSubject := ICISupportTextModule.Description;
                        ICISupportTextModule.CalcFields(Data);
                        ICISupportTextModule.Data.CreateInStream(lInStream);
                        lInStream.Read(EMailBody);

                        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
                        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
                        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
                        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, Rec."No.");
                        ICISupportMailMgt.GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");

                        EMailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailBody);
                        EMailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailBody); // Apply 2 Times to Resolve Footer
                        EMailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailSubject);
                    end;

                    ICINotificationDialogMgt.SetInitSubject(EMailSubject);
                    ICINotificationDialogMgt.SetInitText(EMailBody);// Textvorlage RMA setzen

                    // Create E-Mail Message
                    SendToEMail := Contact."E-Mail";
                    if Rec."Contact E-Mail" <> '' then
                        SendToEMail := Rec."Contact E-Mail";
                    IF ICISupportMailSetup."Test Mode" then
                        SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";

                    ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEmail, Enum::"Email Recipient Type"::"To", Contact.RecordId());

                    ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
                    ICINotificationDialog.Run();
                end;
            }
            action(GoFullScreen)
            {
                ApplicationArea = All;
                ToolTip = 'Go Fullscreen', Comment = 'de-DE=Vollbildmodus aktivieren';
                Caption = 'Fullscreen', Comment = 'de-DE=Vollbild';
                Image = "Invoicing-MDL-PreviewDoc";
                trigger OnAction()
                var
                    ICITicketCommunicationCardP: Page "ICI Ticket Communication CardP";
                begin
                    Rec.SetRecFilter();
                    ICITicketCommunicationCardP.SetFullScreen(true);
                    ICITicketCommunicationCardP.SetRecord(Rec);
                    ICITicketCommunicationCardP.Run();
                end;
            }
            action(Save)
            {
                ApplicationArea = All;
                ToolTip = 'Save the Content', Comment = 'de-DE=Speichern';
                Caption = 'Save the Content', Comment = 'de-DE=Speichern';
                Image = Save;

                trigger OnAction()
                begin
                    CurrPage.TicketCommunication.RequestSave();
                    CurrPage.TicketCommunication.Load('');
                end;

            }
            action(ToggleReadOnly)
            {
                ApplicationArea = All;
                ToolTip = 'ToggleReadOnly', Comment = 'de-DE=ReadOnly';
                Caption = 'ToggleReadOnly', Comment = 'de-DE=ReadOnly';
                Image = Tools;
                Visible = false;

                trigger OnAction()
                begin
                    EditorIsReadOnly := NOT EditorIsReadOnly;
                    CurrPage.TicketCommunication.SetReadOnly(EditorIsReadOnly);
                end;

            }
            action(Refresh)
            {
                ApplicationArea = All;
                ToolTip = 'Refresh', Comment = 'de-DE=Aktualisieren';
                Caption = 'Refresh', Comment = 'de-DE=Aktualisieren';
                Image = Refresh;

                trigger OnAction()
                begin
                    IF AddinReady THEN begin
                        CurrPage.TicketCommunication.Init(ICICommunicationMgt.GetInitData(FullScreen));
                        CurrPage.TicketCommunication.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
                    end;
                end;

            }

        }
    }

    trigger OnAfterGetRecord()
    begin
        if AddinReady THEN begin
            CurrPage.TicketCommunication.SetReadOnly(false);
            CurrPage.TicketCommunication.LoadChat(ICICommunicationMgt.GetChatDataForTicket(Rec."No."));
        end;

    end;

    procedure SetFullScreen(pFullScreen: Boolean)
    begin
        FullScreen := pFullScreen;
    end;

    var
        ICICommunicationMgt: Codeunit "ICI Communication Mgt.";
        AddinReady: Boolean;
        EditorIsReadOnly: Boolean;
        FullScreen: Boolean;

}
