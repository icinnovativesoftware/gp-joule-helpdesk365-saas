controladdin "ICI Ticket Communication"
{
    Scripts = 'ALCODE\TicketCommunication\Scripts\MainScript.js', 'ALCODE\TicketCommunication\Scripts\ckeditor5Classic10.js', 'https://code.jquery.com/jquery-2.1.0.min.js';
    //Scripts = 'ALCODE\TicketCommunication\Scripts\MainScript.js', 'ALCODE\TicketCommunication\Scripts\ckeditor.js.map', 'ALCODE\TicketCommunication\Scripts\ckeditor.js', 'https://code.jquery.com/jquery-2.1.0.min.js';


    StartupScript = 'ALCODE\TicketCommunication\Scripts\startupScript.js';
    RecreateScript = 'ALCODE\TicketCommunication\Scripts\recreateScript.js';
    RefreshScript = 'ALCODE\TicketCommunication\Scripts\refreshScript.js';
    StyleSheets = 'https://licmgt.ic-innovative.de/Helpdesk365/MW.css', 'ALCODE\TicketCommunication\Style\Chat.css', 'ALCODE\TicketCommunication\Style\Editor.css';

    VerticalStretch = true;
    HorizontalStretch = true;
    MinimumHeight = 600;
    RequestedHeight = 600;


    // JS -> AL
    event ControlReady();
    event SaveRequested(Data: JsonObject);
    event ContentChanged();
    event OnAfterInit();
    event DownloadFile(Data: JsonObject);

    // AL -> JS
    procedure Init(Data: JsonObject);
    procedure LoadChat(data: JsonArray);
    procedure LoadTicketTextModule(data: JsonArray);
    procedure Load(data: Text);
    procedure RequestSave();
    procedure SetReadOnly(readonly: boolean);
    procedure SendFileToClient(Data: JsonObject);


}
