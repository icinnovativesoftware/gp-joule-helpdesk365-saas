tableextension 56305 "ICI Country/Region" extends "Country/Region"
{
    fields
    {
        field(56276; "ICI Phone Area Code"; Code[20])
        {
            Caption = 'Phone Area Code', Comment = 'de-DE=Ländervorwahl';
            DataClassification = CustomerContent;
        }
        field(56277; "ICI Phone Area Code Needed"; Boolean)
        {
            Caption = 'Phone Area Code Needed', Comment = 'de-DE=Ländervorwahl für CTI noch benötigt';
            DataClassification = CustomerContent;
        }
    }
}
