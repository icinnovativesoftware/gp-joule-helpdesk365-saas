page 56376 "ICI CTI Cockpit"
{
    // IC - TH - 20190620 ---
    //  - Weblinks u. Wiki eingebunden

    Caption = 'CTI Cockpit', Comment = 'de-DE=CTI Cockpit';
    UsageCategory = Tasks;
    ApplicationArea = All;
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    PromotedActionCategories = 'New,Process,Report,Ticket,New Document,Category 6,CRM', Comment = 'de-DE=Neu,Prozess,Report,Ticket,Neues Dokument,Kategorie 6,CRM';
    ShowFilter = true;
    SourceTable = Contact;
    SourceTableView = SORTING("Company Name", "Company No.", Type, Name);

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("No."; Rec."No.")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    ApplicationArea = All;
                    ToolTip = 'No.', Comment = 'de-DE=Nr.';
                }
                field(Name; Rec.Name)
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    ApplicationArea = All;
                    ToolTip = 'Name', Comment = 'de-DE=Name';
                }
                field("Name Addition"; Rec."Name 2")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Name Addition', Comment = 'de-DE=Name Addition';
                }
                field(Address; Rec.Address)
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Address', Comment = 'de-DE=Adresse';
                }
                field("Post Code"; Rec."Post Code")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Post Code', Comment = 'de-DE=PLZ';
                }
                field(City; Rec.City)
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'City', Comment = 'de-DE=Stadt';
                }
                field("Phone No."; Rec."Phone No.")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    ApplicationArea = All;
                    ToolTip = 'Phone No.', Comment = 'de-DE=Telefonnr.';
                }
                field("Mobile Phone No."; Rec."Mobile Phone No.")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    ApplicationArea = All;
                    ToolTip = 'Mobile Phone No.', Comment = 'de-DE=Mobilfunknr.';
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    Style = Strong;
                    StyleExpr = StyleIsStrong;
                    ApplicationArea = All;
                    ToolTip = 'E-Mail', Comment = 'de-DE=E-Mail';
                }
            }
            group(Contact)
            {
                Caption = 'Contact', Comment = 'de-DE=Kontakt';
                group(GeneralContact)
                {
                    Caption = 'General', Comment = 'de-DE=Allgemein';

                    field(Company_Name_2; Rec."Company Name")
                    {
                        Caption = 'Company Name', Comment = 'de-DE=Unternehmensname';
                        ToolTip = 'Company Name';
                        ApplicationArea = All;
                    }
                    field(Salutation_Code_2; Rec."Salutation Code")
                    {
                        Caption = 'Salutation Code', Comment = 'de-DE=Anrede';
                        ToolTip = 'Salutation Code';
                        Visible = false;
                        ApplicationArea = All;
                    }
                    field(Name_2; Rec.Name)
                    {
                        Caption = 'Name', Comment = 'de-DE=Name';
                        ToolTip = 'Name';
                        ApplicationArea = All;
                    }
                    field(Name_2_2; Rec."Name 2")
                    {
                        Caption = 'Name 2', Comment = 'de-DE=Name 2';
                        ToolTip = 'Name 2';
                        Visible = false;
                        ApplicationArea = All;
                    }
                    field(Address_2; Rec.Address)
                    {
                        Caption = 'Address', Comment = 'de-DE=Adresse';
                        ToolTip = 'Address';
                        ApplicationArea = All;
                    }
                    field(Post_Code_2; Rec."Post Code")
                    {
                        Caption = 'Post Code', Comment = 'de-DE=PLZ';
                        ToolTip = 'Post Code';
                        DrillDown = false;
                        ApplicationArea = All;
                    }
                    field(City_2; Rec.City)
                    {
                        Caption = 'City', Comment = 'de-DE=Stadt';
                        ToolTip = 'City';
                        DrillDown = false;
                        ApplicationArea = All;
                    }
                }
                group(ContactData)
                {
                    Caption = 'Contact Data', Comment = 'de-DE=Kontaktdaten';
                    field(Phone_No_2; Rec."Phone No.")
                    {
                        Caption = 'Phone No.', Comment = 'de-DE=Telefonnr.';
                        ToolTip = 'Phone No.';
                        DrillDown = true;
                        ApplicationArea = All;
                    }
                    field(Mobile_Phone_No_2; Rec."Mobile Phone No.")
                    {
                        Caption = 'Mobile Phone No.', Comment = 'de-DE=Mobiltelefonnr.';
                        ToolTip = 'Mobile Phone No.';
                        DrillDown = true;
                        ApplicationArea = All;
                    }
                    field("E-Mail_2"; Rec."E-Mail")
                    {
                        Caption = 'E-Mail', Comment = 'de-DE=E-Mail';
                        ToolTip = 'E-Mail';
                        DrillDown = true;
                        ApplicationArea = All;
                    }
                    field(Home_Page_2; Rec."Home Page")
                    {
                        Caption = 'Home Page', Comment = 'de-DE=Homepage';
                        ToolTip = 'Home Page';
                        DrillDown = true;
                        ApplicationArea = All;
                    }
                }
            }
        }
        area(factboxes)
        {
            systempart(Notes; Notes)
            {
                Visible = false;
                ApplicationArea = All;
            }
            systempart(Links; Links)
            {
                Visible = false;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(OpenContact)
            {
                Caption = 'Open Contact', Comment = 'de-DE=Kontakt öffnen';
                ToolTip = 'Open Contact';
                Image = ContactPerson;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    PAGE.RUN(PAGE::"Contact Card", Rec);
                end;
            }
            action(OpenCustomer)
            {
                Caption = 'Open Customer', Comment = 'de-DE=Debitor öffnen';
                ToolTip = 'Open Customer';
                Image = Customer;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                ApplicationArea = All;

                trigger OnAction()
                var
                    Customer: Record Customer;
                begin
                    Customer.GET(Rec.GetCustomerNo());
                    PAGE.RUN(PAGE::"Customer Card", Customer);
                end;
            }
            action("Co&mments (Contact)")
            {
                Caption = 'Co&mments (Contact)', Comment = 'de-DE=Kommentare (Kontakt)';
                ToolTip = 'Co&mments';
                Image = ViewComments;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = false;
                RunObject = Page "Rlshp. Mgt. Comment Sheet";
                RunPageLink = "Table Name" = CONST(Contact),
                              "No." = FIELD("No."),
                              "Sub No." = CONST(0);
                ApplicationArea = All;
            }
            action("Co&mments (Customer)")
            {
                Caption = 'Co&mments (Customer)', Comment = 'de-DE=Kommentare (Debitor)';
                ToolTip = 'Comments';
                Image = ViewComments;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = false;
                ApplicationArea = All;

                trigger OnAction()
                var
                    CommentLine: Record "Comment Line";
                begin
                    CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Customer);
                    CommentLine.SETRANGE("No.", Rec.GetCustomerNo());
                    PAGE.RUN(0, CommentLine);
                end;
            }
            action("Clear CTI Filter")
            {
                Caption = 'Clear CTI Filter', Comment = 'de-DE=CTI Filter Löschen';
                ToolTip = 'Clear CTI Filter';
                Image = DeleteAllBreakpoints;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    GlobCTIFilter := '';
                    SETCTIFilter();
                end;
            }
            separator(Separator1)
            {
            }
            group(Ticket)
            {
                Caption = 'Ticket';
                action("New Ticket")
                {
                    Caption = 'New Ticket', Comment = 'de-DE=Neues Ticket';
                    ToolTip = 'New Ticket';
                    Image = NewDocument;
                    Promoted = true;
                    PromotedCategory = Category4;
                    PromotedIsBig = true;
                    PromotedOnly = true;
                    // RunObject = Page "ICI Support Ticket Card";
                    // RunPageLink = "Company Contact No." = FIELD("Company No."),
                    //               "Current Contact No." = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                        isHandled: Boolean;
                    begin
                        OnBeforeCreateTicketFromCTICockpit(isHandled, Rec);
                        if not isHandled then begin
                            ICISupportTicket.Init();
                            ICISupportTicket.Insert(true);
                            IF Rec.Type = Rec.Type::Company THEN
                                ICISupportTicket.Validate("Company Contact No.", Rec."Company No.")
                            ELSE
                                ICISupportTicket.Validate("Current Contact No.", Rec."No.");
                            ICISupportTicket.FillMissingContactOrCustomerInformation();
                            ICISupportTicket.Modify(true);

                            PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket);
                        end;
                    end;
                }
            }
            separator(Separator2)
            {
            }
            group(Sales)
            {
                Caption = 'Sales', Comment = 'de-DE=Verkauf';
                action(SalesQuote)
                {
                    Caption = 'Sales Quote', Comment = 'de-DE=Verkaufsangebot';
                    ToolTip = 'Sales Quote';
                    Image = Quote;
                    // Promoted = true;
                    // PromotedCategory = Category5;
                    // PromotedIsBig = true;
                    // RunObject = Page "Sales Quote";
                    // RunPageLink = "Sell-to Contact" = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Customer: Record Customer;
                        SalesHeader: Record "Sales Header";
                    begin
                        Customer.GET(Rec.GetCustomerNo());
                        SalesHeader.Init();
                        SalesHeader.Validate("Document Type", SalesHeader."Document Type"::Quote);
                        SalesHeader.Insert(true);
                        SalesHeader.Validate("Sell-to Customer No.", Customer."No.");
                        SalesHeader.Modify(true);

                        PAGE.RUN(PAGE::"Sales Quote", SalesHeader);
                    end;
                }
                action(SalesOrder)
                {
                    Caption = 'Sales Order', Comment = 'de-DE=Verkaufsauftrag';
                    ToolTip = 'Sales Order';
                    Image = Document;
                    Promoted = true;
                    PromotedCategory = Category5;
                    PromotedIsBig = true;
                    // RunObject = Page "Sales Order";
                    // RunPageLink = "Sell-to Contact" = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Customer: Record Customer;
                        SalesHeader: Record "Sales Header";
                    begin
                        Customer.GET(Rec.GetCustomerNo());
                        SalesHeader.Init();
                        SalesHeader.Validate("Document Type", SalesHeader."Document Type"::Order);
                        SalesHeader.Insert(true);
                        SalesHeader.Validate("Sell-to Customer No.", Customer."No.");
                        SalesHeader.Modify(true);

                        PAGE.RUN(PAGE::"Sales Order", SalesHeader);
                    end;
                }
                action(SalesReturnOrder)
                {
                    Caption = 'Sales Return Order', Comment = 'de-DE=Verkaufsreklamation';
                    ToolTip = 'Sales Return Order';
                    Image = ReturnOrder;
                    Promoted = true;
                    PromotedCategory = Category5;
                    PromotedIsBig = true;
                    // RunObject = Page "Sales Return Order";
                    // RunPageLink = "Sell-to Contact" = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Customer: Record Customer;
                        SalesHeader: Record "Sales Header";
                    begin
                        Customer.GET(Rec.GetCustomerNo());
                        SalesHeader.Init();
                        SalesHeader.Validate("Document Type", SalesHeader."Document Type"::"Return Order");
                        SalesHeader.Insert(true);
                        SalesHeader.Validate("Sell-to Customer No.", Customer."No.");
                        SalesHeader.Modify(true);

                        PAGE.RUN(PAGE::"Sales Return Order", SalesHeader);
                    end;
                }
            }
            separator(Separator3)
            {
            }
            group(Service)
            {
                Caption = 'Service', Comment = 'de-DE=Service';
                action(ServiceQuote)
                {
                    Caption = 'Service Quote', Comment = 'de-DE=Serviceangebot';
                    ToolTip = 'Service Quote';
                    Image = Quote;
                    // Promoted = true;
                    // PromotedCategory = Category5;
                    // PromotedIsBig = true;
                    // RunObject = Page "Service Quote";
                    // RunPageLink = "Customer No." = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Customer: Record Customer;
                        ServiceHeader: Record "Service Header";
                    begin
                        Customer.GET(Rec.GetCustomerNo());
                        ServiceHeader.Init();
                        ServiceHeader.Validate("Document Type", ServiceHeader."Document Type"::Quote);
                        ServiceHeader.Insert(true);
                        ServiceHeader.Validate("Customer No.", Customer."No.");
                        ServiceHeader.Modify(true);

                        PAGE.RUN(PAGE::"Service Quote", ServiceHeader);
                    end;
                }
                action(ServiceOrder)
                {
                    Caption = 'Service Order', Comment = 'de-DE=Serviceauftrag';
                    ToolTip = 'Service Order';
                    Image = Document;
                    Promoted = true;
                    PromotedCategory = Category5;
                    // RunObject = Page "Service Order";
                    // RunPageLink = "Customer No." = FIELD("No.");
                    // RunPageMode = Create;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Customer: Record Customer;
                        ServiceHeader: Record "Service Header";
                    begin
                        Customer.GET(Rec.GetCustomerNo());
                        ServiceHeader.Init();
                        ServiceHeader.Validate("Document Type", ServiceHeader."Document Type"::Order);
                        ServiceHeader.Insert(true);
                        ServiceHeader.Validate("Customer No.", Customer."No.");
                        ServiceHeader.Modify(true);

                        PAGE.RUN(PAGE::"Service Order", ServiceHeader);
                    end;
                }
            }
            separator(Separator4)
            {
            }
            group(CRM)
            {
                action("T&o-Dos")
                {
                    Caption = 'T&o-dos', Comment = 'de-DE=Aufgaben';
                    ToolTip = 'To-dos';
                    Image = TaskList;
                    // Promoted = true;
                    // PromotedCategory = Category7;
                    // PromotedIsBig = false;
                    RunObject = Page "Task List";
                    RunPageLink = "Contact Company No." = FIELD("Company No."),
                                  "Contact No." = FIELD(FILTER("Lookup Contact No.")),
                                  "System To-do Type" = FILTER("Contact Attendee");
                    RunPageView = SORTING("Contact Company No.", Date, "Contact No.", Closed);
                    ApplicationArea = All;
                }
                action("InteractionLogEntries")
                {
                    Caption = 'Interaction Log E&ntries', Comment = 'de-DE=Aktivitätenprotokollposten';
                    ToolTip = 'Interaction Log Entries';
                    Image = InteractionLog;
                    // Promoted = true;
                    // PromotedCategory = Category7;
                    // PromotedIsBig = false;
                    RunObject = Page "Interaction Log Entries";
                    RunPageLink = "Contact Company No." = FIELD("Company No."),
                                  "Contact No." = FILTER(<> ''),
                                  "Contact No." = FIELD(FILTER("Lookup Contact No."));
                    RunPageView = SORTING("Contact Company No.", "Contact No.");
                    ShortCutKey = 'Ctrl+F7';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnAfterGetCurrRecord()
    var
    begin
    end;

    trigger OnAfterGetRecord()
    var
    begin
        StyleIsStrong := Rec.Type = Rec.Type::Company;
    end;

    trigger OnFindRecord(Which: Text): Boolean
    begin
        FindContactByPhoneNoFilter();
        EXIT(Rec.FIND(Which));
    end;

    trigger OnOpenPage()
    begin
        CloseInternal();
    end;

    var

        StyleIsStrong: Boolean;
        GlobCTIFilter: Text[250];

    procedure FindContactByPhoneNoFilter(): Boolean
    var
        ICICTIManagement: Codeunit "ICI CTI Management";
    begin
        IF Rec.GETFILTER("CTI Phone No.") <> '' THEN BEGIN
            GlobCTIFilter := Rec.GETFILTER("CTI Phone No.");
            Rec.SETRANGE("CTI Phone No.");
        END;

        CloseInternal();

        IF GlobCTIFilter <> '' THEN BEGIN
            GlobCTIFilter := ICICTIManagement.CleanInternal(GlobCTIFilter);
            SETCTIFilter();
        END;
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeCreateTicketFromCTICockpit(var isHandled: Boolean; Contact: Record Contact)
    begin
    end;

    local procedure CloseInternal()
    var
    begin
    end;

    local procedure SETCTIFilter()
    begin
        Rec.FILTERGROUP(-1);
        Rec.SETRANGE("CTI Phone No.", GlobCTIFilter);
        Rec.SETRANGE("CTI Mobile Phone No.", GlobCTIFilter);
        Rec.FILTERGROUP(0);
    end;
}
