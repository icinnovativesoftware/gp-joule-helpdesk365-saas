pageextension 56324 "ICI Countries/Regions" extends "Countries/Regions"
{
    layout
    {
        addlast(Control1)
        {
            field("ICI Phone Area Code"; Rec."ICI Phone Area Code")
            {
                ApplicationArea = All;
                ToolTip = 'ICI Phone Area Code', Comment = 'de-DE=Ländervorwahl';
            }
        }
    }
}
