codeunit 56301 "ICI CTI Management"
{
    trigger OnRun()
    begin
        InitContact(true);
    end;

    procedure InitContact(UpdateAllCTINumbers: Boolean)
    var
        Contact: Record "Contact";
        CountryRegion: Record "Country/Region";
    begin
        Clear(CountryRegion);
        If CountryRegion.FindSet() then
            repeat
                CountryRegion."ICI Phone Area Code Needed" := false;
            until CountryRegion.Next() = 0;


        Clear(Contact);
        Contact.SetFilter("Phone No.", '<>%1', '');
        if not UpdateAllCTINumbers then
            Contact.SetFilter("CTI Phone No.", '=%1', '');
        IF Contact.FINDSET() THEN
            REPEAT
                Contact."CTI Phone No." := FormatPhoneNo(Contact."Country/Region Code", Contact."Phone No.");
                Contact.MODIFY();
            UNTIL Contact.NEXT() = 0;

        Clear(Contact);
        Contact.SetFilter("Mobile Phone No.", '<>%1', '');
        if not UpdateAllCTINumbers then
            Contact.SetFilter("CTI Mobile Phone No.", '=%1', '');
        IF Contact.FINDSET() THEN
            REPEAT
                Contact."CTI Mobile Phone No." := FormatPhoneNo(Contact."Country/Region Code", Contact."Mobile Phone No.");
                Contact.MODIFY();
            UNTIL Contact.NEXT() = 0;
    end;

    procedure FormatPhoneNo(CountryCode: Code[10]; PhoneNo: Text[50]) FormattedPhoneNo: Text[30]
    var
        CompInf: Record "Company Information";
        Country: Record "Country/Region";
    begin
        CompInf.GET();

        IF CountryCode = '' THEN
            IF CompInf."Country/Region Code" = '' THEN
                CountryCode := 'DE'
            ELSE
                CountryCode := CompInf."Country/Region Code";

        Country.GET(CountryCode);

        if Country."ICI Phone Area Code" = '' then begin
            Country."ICI Phone Area Code Needed" := true;
            exit;
        end;

        //Vorwahlnull in Klammern entfernen
        IF STRPOS(PhoneNo, '(0)') <> 0 THEN
            PhoneNo := Country."ICI Phone Area Code" + COPYSTR(PhoneNo, STRPOS(PhoneNo, '(0)') + 3, MAXSTRLEN(PhoneNo));

        //Vorwahlnull ohne Land entfernen
        IF (STRPOS(PhoneNo, '0') = 1) AND (STRPOS(PhoneNo, Country."ICI Phone Area Code") = 0) THEN
            PhoneNo := Country."ICI Phone Area Code" + COPYSTR(PhoneNo, 2, MAXSTRLEN(PhoneNo));

        //Aus + -> 00 machen
        IF STRPOS(PhoneNo, '+') = 1 THEN
            PhoneNo := '00' + COPYSTR(PhoneNo, 2, MAXSTRLEN(PhoneNo));

        FormattedPhoneNo := DELCHR(PhoneNo, '=', DELCHR(PhoneNo, '=', '0123456789'));
    end;

    procedure CleanInternal(PhoneNo: Text[50]) FormattedPhoneNo: Text[30]
    var
        SupportSetup: Record "ICI Support Setup";
    begin
        SupportSetup.GET();
        IF SupportSetup."CTI Internal Call Prefix" = '' THEN
            EXIT(PhoneNo);

        //Rufgruppe abschneiden
        IF COPYSTR(PhoneNo, 1, 1) = '#' THEN BEGIN
            IF COPYSTR(PhoneNo, 3, 1) = '0' THEN
                PhoneNo := '0049' + COPYSTR(PhoneNo, 4)
            ELSE
                PhoneNo := COPYSTR(PhoneNo, 3);

        END;

        IF COPYSTR(PhoneNo, 1, 9) = SupportSetup."CTI Internal Call Prefix" THEN
            EXIT(COPYSTR(SupportSetup."CTI Internal Call Prefix", 1, 4) + DELSTR(PhoneNo, 1, 9));

        EXIT(PhoneNo);
    end;
}
