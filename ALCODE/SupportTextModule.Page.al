page 56290 "ICI Support Text Module"
{
    Caption = 'ICI Support Text Module', Comment = 'de-DE=Support Textvorlagen';
    PageType = List;
    SourceTable = "ICI Support Text Module";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {

                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Code';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Sprachcode';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Beschreibung';
                }
            }
        }
        area(FactBoxes)
        {
            part(Editor; "ICI Text Module Editor CardP")
            {
                ApplicationArea = All;
                SubPageLink = Code = field(Code), "Language Code" = field("Language Code"), "Type" = field("Type");
                UpdatePropagation = Both;
            }
        }
    }

}
