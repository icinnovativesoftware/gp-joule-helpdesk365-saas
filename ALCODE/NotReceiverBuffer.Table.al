table 56313 "ICI Not. Receiver Buffer"
{
    Caption = 'Notification Receiver Buff';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(10; "Code"; Code[50])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(11; Description; Text[250])
        {
            Caption = 'Descrption', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(12; "Notification Method Type"; Enum "Notification Method Type")
        {
            Caption = 'Notification Method Type', Comment = 'de-DE=Benachrichtigungsart';
            DataClassification = CustomerContent;
        }
        field(13; "E-Mail"; Text[250])
        {
            Caption = 'E-Mail', Comment = 'de-DE=E-Mail';
            DataClassification = CustomerContent;
            ExtendedDatatype = EMail;
            Editable = false;
        }
        field(14; "E-Mail Recipient Type"; Enum "Email Recipient Type")
        {
            Caption = 'Email Recipient Type', Comment = 'de-DE=Empfänger Art';
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                ModifySendToErr: Label 'Cannot delete change To Recipient', Comment = 'de-DE=Der direkte Empfänger kann nicht geändert werden';
            begin
                IF xRec."E-Mail Recipient Type" = Enum::"Email Recipient Type"::"To" THEN
                    ERROR(ModifySendToErr);
            end;
        }
        field(15; "Record ID"; RecordId)
        {
            Caption = 'Record ID', Comment = 'de-DE=Datensatzverknüpfung';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnDelete()
    var
        DeleteSendToErr: Label 'Cannot delete Send To Recipient', Comment = 'de-DE=Der direkte Empfänger kann nicht gelöscht werden';
    begin
        IF "E-Mail Recipient Type" = "E-Mail Recipient Type"::"To" then
            ERROR(DeleteSendToErr);
    end;
}
