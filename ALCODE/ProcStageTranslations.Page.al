page 56370 "ICI Proc. Stage Translations"
{

    Caption = 'Proc Stage Translations', Comment = 'de-DE=Prozesstufenübersetzungen';
    PageType = List;
    SourceTable = "ICI Proc. Stage Translation";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Process Code"; Rec."Process Code")
                {
                    ToolTip = 'Specifies the value of the Process Code field.', Comment = 'de-DE=Processcode';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Process Stage"; Rec."Process Stage")
                {
                    ToolTip = 'Specifies the value of the Process Stage field.', Comment = 'de-DE=Stufe';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Language Code"; Rec."Language Code")
                {
                    ToolTip = 'Specifies the value of the Language Code field.', Comment = 'de-DE=Sprachcode';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field.', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        IF Rec.GetFilter("Process Code") <> '' THEN
            Rec."Process Code" := COPYSTR(Rec.GetFilter("Process Code"), 1, 20);
        IF Rec.GetFilter("Process Stage") <> '' then
            EVALUATE(Rec."Process Stage", Rec.GetFilter("Process Stage"));
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        Rec.TestField("Language Code");
    end;
}
