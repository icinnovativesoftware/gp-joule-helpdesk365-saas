page 56282 "ICI Text Module Editor CardP"

{

    Caption = 'ICI Editor CardPart', Comment = 'de-DE=Textvorlage bearbeiten';
    PageType = CardPart;
    SourceTable = "ICI Support Text Module";

    layout
    {
        area(Content)
        {
            usercontrol(Editor; "ICI Text Module Editor CK4")
            {
                ApplicationArea = All;

                trigger ControlReady()
                begin
                    AddinReady := true;
                    CurrPage.Editor.Init(ICINotificationDialogMgt.GetInitData(false));
                    IF Rec."Code" = '' THEN
                        CurrPage.Editor.SetReadOnly(true);
                end;

                trigger OnAfterInit()
                var
                    lInStream: InStream;
                    TextModuleContent: Text;
                begin
                    IF Rec."Code" <> '' THEN begin
                        Rec.CalcFields(Data);
                        Rec.Data.CreateInStream(lInStream);
                        lInStream.Read(TextModuleContent);
                        CurrPage.Editor.Load(TextModuleContent);
                    end;
                end;

                trigger SaveRequested(Data: JsonObject)
                var
                    ICISupportTextModule: Record "ICI Support Text Module";
                    NoTextEnteredErr: Label 'No Text was Entered', Comment = 'de-DE=Keine Nachricht eingegeben';
                    lOutStream: OutStream;
                    jToken: JsonToken;
                    jVal: JsonValue;
                    JsonTxt: Text;
                begin
                    Data.Get('Text', jToken);
                    jVal := jToken.AsValue();
                    JsonTxt := jVal.AsText();
                    if JsonTxt = '' then
                        Error(NoTextEnteredErr);
                    COMMIT();
                    /*CurrPage.SaveRecord();
                    Rec.Calcfields(Data);
                    Rec.Data.CreateOutStream(lOutStream);
                    lOutStream.Write(data);
                    Rec.Modify();
                    CurrPage.Update(false);*/
                    CurrPage.SaveRecord();
                    ICISupportTextModule.GET(Rec.Code, Rec."Language Code", Rec.Type);
                    ICISupportTextModule.Calcfields(Data);
                    ICISupportTextModule.Data.CreateOutStream(lOutStream);
                    lOutStream.Write(JsonTxt);
                    ICISupportTextModule.Modify(true);

                    CurrPage.Update(false);
                end;
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Save)
            {
                ApplicationArea = All;
                ToolTip = 'Save the Content', Comment = 'de-DE=Speichern';
                Caption = 'Save the Content', Comment = 'de-DE=Speichern';
                Image = Save;

                trigger OnAction()
                begin
                    CurrPage.Editor.RequestSave();
                end;

            }
            action(ToggleReadOnly)
            {
                ApplicationArea = All;
                ToolTip = 'ToggleReadOnly', Comment = 'de-DE=ReadOnly';
                Caption = 'ToggleReadOnly', Comment = 'de-DE=ReadOnly';
                Image = Tools;
                Visible = false;

                trigger OnAction()
                begin
                    EditorIsReadOnly := NOT EditorIsReadOnly;
                    CurrPage.Editor.SetReadOnly(EditorIsReadOnly);
                end;

            }

        }
    }

    trigger OnAfterGetRecord()
    var
        lInStream: InStream;
        TextModuleContent: Text;
    begin
        if AddinReady THEN BEGIN
            CurrPage.Editor.SetReadOnly(false);
            IF (rec.Code <> xRec.Code) OR (rec.Type <> xRec.Type) OR (rec."Language Code" <> xRec."Language Code") THEN BEGIN
                Rec.CalcFields(Data);
                Rec.Data.CreateInStream(lInStream);
                lInStream.Read(TextModuleContent);
                CurrPage.Editor.Load(TextModuleContent);
            END;
        END;
    end;

    /*procedure SetTicketNo(newTicketNo: Code[20])
    begin
        TicketNo := newTicketNo;
    end;*/

    var
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        AddinReady: Boolean;
        EditorIsReadOnly: Boolean;

}
