controladdin "ICI CKEditor"
{
    VerticalStretch = true;
    HorizontalStretch = true;
    MinimumHeight = 400;
    RequestedHeight = 400;

    Scripts = 'https://licmgt.ic-innovative.de/Helpdesk365/CKEditor_TextModule/ckeditor.js', 'ALCODE/TextModuleEditor/Scripts/MainScript.js';
    StartupScript = 'ALCODE\TextModuleEditor\Scripts\startupScript.js';
    RecreateScript = 'ALCODE\TextModuleEditor\Scripts\recreateScript.js';
    RefreshScript = 'ALCODE\TextModuleEditor\Scripts\refreshScript.js';
    Stylesheets = 'ALCode\TextModuleEditor\Style\Editor.css';

    event ControlReady();
    event SaveRequested(data: Text);
    event ContentChanged();
    event OnAfterInit();

    procedure Init();
    procedure Load(data: Text);
    procedure RequestSave();
    procedure SetReadOnly(readonly: boolean);
}
