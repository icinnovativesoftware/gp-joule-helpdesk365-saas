
var InputArea
var Editor

// Editor configuration.
// ClassicEditor.defaultConfig = {
// 	highlight: {
// 		options: [
// 			{
// 				model: 'orangePen',
// 				class: 'pen-orange',
// 				title: 'Orange',
// 				color: '#e59536',
// 				type: 'pen'
// 			},
// 			{
// 				model: 'lightBluePen',
// 				class: 'pen-light-blue',
// 				title: 'Light blue',
// 				color: '#30caf7',
// 				type: 'pen'
// 			},
// 			{
// 				model: 'darkBluePen',
// 				class: 'pen-dark-blue',
// 				title: 'Dark blue',
// 				color: '#3982a4',
// 				type: 'pen'
// 			},
// 			{
// 				model: 'greenPen',
// 				class: 'pen-green',
// 				title: 'Green',
// 				color: '#488f80',
// 				type: 'pen'
// 			},
// 			{
// 				model: 'grayPen',
// 				class: 'pen-gray',
// 				title: 'Dark gray',
// 				color: '#4d4d4c',
// 				type: 'pen'
// 			}
// 		]
// 	},
// 	heading: {
// 		options: [
// 			{ model: 'paragraph', title: 'Body copy', class: 'ck-heading_paragraph' },
// 			{ model: 'heading2', view: 'h2', title: 'Sub Header', class: 'ck-heading_heading2' }
// 		]
// 	},
// 	toolbar: {
// 		items: [
// 			'heading',
// 			'|',
// 			'highlight',
// 			'|',
// 			'bold',
// 			'italic',
// 			'underline',
// 			'|',
// 			'numberedList',
// 			'bulletedList',
// 			'|',
// 			'blockQuote',
// 			'|',
// 			'link',
// 			'|',
// 			'imageUpload',
// 			'|'
// 		]
// 	},
// 	image: {
// 		toolbar: [
// 			'imageStyle:full',
// 			'imageStyle:side',
// 			'|',
// 			'imageTextAlternative'
// 		]
// 	},
// 	// This value must be kept in sync with the language defined in webpack.config.js.
// 	language: 'en'
// };

function Init() {

    var div = document.getElementById("controlAddIn");
    div.innerHTML = "";
    InputArea = document.createElement("textarea");
    InputArea.id = "Comment";
    InputArea.name = "Comment";
    div.appendChild(InputArea);
    
	CKEDITOR.replace( 'Comment', {
    });

    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("OnAfterInit",[]);
}

function Load(data) {
    //Editor.setData(data);
	CKEDITOR.instances.Comment.setData(data);
}

function RequestSave() {
    //debugger;
	// var editorData = Editor.getData();
	var editorData = CKEDITOR.instances.Comment.getData();
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("SaveRequested",[editorData]);
}
function SetReadOnly(readonly)
{
    // setReadOnly
    // Editor.isReadOnly = readonly;

}
