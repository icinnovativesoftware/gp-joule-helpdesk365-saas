page 56291 "ICI Text Module Editor Dialog"
{

    Caption = 'Editor', Comment = 'de-DE=Editor';
    PageType = StandardDialog;
    SourceTable = "ICI Support Text Module";
    DataCaptionFields = Code;

    layout
    {
        area(Content)
        {
            usercontrol(Editor; "ICI CKEditor")
            {
                ApplicationArea = All;

                trigger ControlReady()
                begin
                    AddinReady := true;
                    CurrPage.Editor.Init();
                end;

                trigger OnAfterInit()
                var
                    lInStream: InStream;
                    TextModuleContent: Text;
                begin
                    IF Rec."Code" <> '' THEN begin
                        Rec.CalcFields(Data);
                        Rec.Data.CreateInStream(lInStream);
                        lInStream.Read(TextModuleContent);
                        CurrPage.Editor.Load(TextModuleContent);
                    end;
                end;

                trigger SaveRequested(Data: Text)
                var
                    ICISupportTextModule: Record "ICI Support Text Module";
                    NoTextEnteredErr: Label 'No Text was Entered', Comment = 'de-DE=Keine Nachricht eingegeben';
                    lOutStream: OutStream;
                    JsonTxt: Text;
                begin
                    if Data = '' then
                        Error(NoTextEnteredErr);

                    //CurrPage.SaveRecord();
                    ICISupportTextModule.GET(Rec.Code, Rec."Language Code", Rec.Type);
                    ICISupportTextModule.Calcfields(Data);
                    ICISupportTextModule.Data.CreateOutStream(lOutStream);
                    lOutStream.Write(data);
                    ICISupportTextModule.Modify(true);

                    WizardCompleted := TRUE;
                    CurrPage.CLOSE();
                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    var
        lInStream: InStream;
        TextModuleContent: Text;
    begin
        if AddinReady THEN BEGIN
            CurrPage.Editor.SetReadOnly(false);
            IF (rec.Code <> xRec.Code) OR (rec.Type <> xRec.Type) OR (rec."Language Code" <> xRec."Language Code") THEN BEGIN
                Rec.CalcFields(Data);
                Rec.Data.CreateInStream(lInStream);
                lInStream.Read(TextModuleContent);
                CurrPage.Editor.Load(TextModuleContent);
            END;
        END;
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    VAR
        ConfirmTxt: Label 'Do you really want to exit without saving?', Comment = 'de-DE=Der Inhalt wurde noch nicht gespeichert, wollen Sie den Assistenten wirklich beenden?';
    BEGIN
        CASE CloseAction OF
            ACTION::OK:
                BEGIN
                    CurrPage.Editor.RequestSave();
                    EXIT(FALSE);
                END;
            ACTION::Cancel:
                IF WizardCompleted THEN
                    EXIT(TRUE)
                ELSE
                    EXIT(CONFIRM(ConfirmTxt));

        END;
    END;

    var
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        AddinReady: Boolean;
        WizardCompleted: Boolean;
}
