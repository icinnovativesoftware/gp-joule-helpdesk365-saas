page 56367 "ICI Not. Receiver Buffer Listp"
{

    Caption = 'Receivers', Comment = 'de-DE=Empfänger';
    PageType = List;
    SourceTable = "ICI Not. Receiver Buffer";
    SourceTableTemporary = true;
    ModifyAllowed = true;
    InsertAllowed = false;
    DeleteAllowed = true;
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Type"; Rec."Notification Method Type")
                {
                    ToolTip = 'Specifies the value of the Type field.', Comment = 'de-DE=Gibt die Benachrichtigungsmethode des Empfängers an';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(Descrption; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Descrption field.', Comment = 'de-DE=Gibt den Namen/die Beschreibung des Empfängers an';
                    ApplicationArea = All;
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    ToolTip = 'Specifies the value of the E-Mail field.', Comment = 'de-DE=Gibt E-Mailadresse des Empfängers an';
                    ApplicationArea = All;
                }
                field("E-Mail Recipient Type"; Rec."E-Mail Recipient Type")
                {
                    ToolTip = 'Specifies the value of the E-Mail Recipient Type field.', Comment = 'de-DE=Gibt Empfangsart des Empfängers an';
                    ApplicationArea = All;
                }
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Specifies the value of the Code field.', Comment = 'de-DE=Gibt den Code des Empfängers an';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(AddNewContact)
            {
                Image = AddContacts;
                ApplicationArea = All;
                Caption = 'Adds Contact to Receivers', Comment = 'de-DE=Kontakt hinzufügen';
                ToolTip = 'Adds Contact to Receivers', Comment = 'de-DE=Kontakt hinzufügen';
                Promoted = true;
                PromotedIsBig = true;

                trigger OnAction()
                var
                    Contact: Record Contact;
                    ContactList: Page "Contact List";
                begin
                    Rec.FilterGroup(4);
                    IF ContactCompanyFilter <> '' THEN
                        Contact.SetFilter("Company No.", ContactCompanyFilter); // z.B. Nur aus Unternehmen

                    ContactList.LOOKUPMODE(TRUE);
                    ContactList.SetTableView(Contact);
                    IF ContactList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        ContactList.SetSelectionFilter(Contact);
                        Contact.MARKEDONLY(TRUE);
                        IF Contact.FINDSET() THEN
                            REPEAT
                                AddContactReceiver(Contact);
                            UNTIL Contact.NEXT() <= 0;
                        CurrPage.Update(false);
                    END;
                end;
            }
            action(AddNewUser)
            {
                Image = AddContacts;
                ApplicationArea = All;
                Caption = 'Adds User to Receivers', Comment = 'de-DE=Supportbenutzer hinzufügen';
                ToolTip = 'Adds User to Receivers', Comment = 'de-DE=Supportbenutzer hinzufügen';
                Promoted = true;
                PromotedIsBig = true;

                trigger OnAction()
                var
                    ICISupportUser: Record "ICI Support User";
                    ICISupportUserList: Page "ICI Support User List";
                begin
                    ICISupportUserList.LOOKUPMODE(TRUE);
                    IF ICISupportUserList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        ICISupportUserList.SetSelectionFilter(ICISupportUser);
                        ICISupportUser.MARKEDONLY(TRUE);
                        IF ICISupportUser.FINDSET() THEN
                            REPEAT
                                AddUserReceiver(ICISupportUser);
                            UNTIL ICISupportUser.NEXT() <= 0;
                        CurrPage.Update(false);
                    END;
                end;
            }
        }
    }
    trigger OnDeleteRecord(): Boolean
    begin
        ICINotificationDialogMgt.DeleteReceiver(Rec);
        EXIT(true);
    end;

    trigger OnModifyRecord(): Boolean
    begin
        ICINotificationDialogMgt.ModifyReceiver(Rec);
        EXIT(true);
    end;

    procedure InitReceivers(Receivers: JsonArray)
    var
        NoOfReceivers: Integer;
        Counter: Integer;
        JTok: JsonToken;
    begin
        NoOfReceivers := Receivers.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF Receivers.GET(Counter, JTok) THEN
                AddReceiver(JTok.AsObject());

        CurrPage.Update(false);
    end;

    procedure AddReceiver(Receiver: JsonObject)
    var
        jTok: JsonToken;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", Rec.COUNT() + 1);

        IF Receiver.Get('Code', jTok) then
            Rec.Code := jTok.AsValue().AsText();
        IF Receiver.Get('Name', jTok) then
            Rec.Description := jTok.AsValue().AsText();
        IF Receiver.Get('NotificationMethodType', jTok) then
            Rec."Notification Method Type" := Enum::"Notification Method Type".FromInteger(jTok.AsValue().AsInteger());
        IF Receiver.Get('EMail', jTok) then
            Rec."E-Mail" := jTok.AsValue().AsText();
        IF Receiver.Get('EMailRecipientType', jTok) then
            Rec."E-Mail Recipient Type" := Enum::"EMail Recipient Type".FromInteger(jTok.AsValue().AsInteger());
        IF Receiver.Get('RecordID', jTok) then
            EVALUATE(Rec."Record ID", jTok.AsValue().AsText());

        Rec.Insert(true);
    end;

    procedure AddContactReceiver(var Contact: Record Contact)
    begin
        Rec.Init();
        Rec.Validate("Entry No.", Rec.COUNT() + 1);

        Rec.Code := Contact."No.";
        Rec.Description := Contact.Name;
        Rec."Notification Method Type" := Enum::"Notification Method Type"::Email;
        Rec."E-Mail" := Contact."E-Mail";
        Rec."E-Mail Recipient Type" := "Email Recipient Type"::Cc;
        Rec."Record ID" := Contact.RecordId();
        Rec.Insert(true);

        ICINotificationDialogMgt.AddReceiver(Rec.Code, Rec."Notification Method Type", Rec.Description, Rec."E-Mail", Rec."E-Mail Recipient Type", Contact.RecordId());
    end;

    procedure AddUserReceiver(var ICISupportUser: Record "ICI Support User")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", Rec.COUNT() + 1);

        Rec.Code := ICISupportUser."User ID";
        Rec.Description := ICISupportUser.Name;
        Rec."Notification Method Type" := Enum::"Notification Method Type"::Email;
        Rec."E-Mail" := ICISupportUser."E-Mail";
        Rec."E-Mail Recipient Type" := "Email Recipient Type"::Cc;
        Rec."Record ID" := ICISupportUser.RecordId();
        Rec.Insert(true);

        ICINotificationDialogMgt.AddReceiver(Rec.Code, Rec."Notification Method Type", Rec.Description, Rec."E-Mail", Rec."E-Mail Recipient Type", ICISupportUser.RecordId());
    end;

    procedure SetContactCompanyFilter(pContactCompanyFilter: Text)
    begin
        ContactCompanyFilter := pContactCompanyFilter;
    end;

    procedure SetNotificationDialog(var pICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Values: JsonObject;
        jTok: JsonToken;
    begin
        ICINotificationDialogMgt := pICINotificationDialogMgt;
        Values := ICINotificationDialogMgt.CreateNotificationValues();

        if Values.GET('TicketNo', jTok) then
            IF ICISupportTicket.Get(JTok.AsValue().AsText()) then
                IF ICISupportTicket."Company Contact No." <> '' then
                    SetContactCompanyFilter(Strsubstno('=%1', ICISupportTicket."Company Contact No."));

        IF Values.GET('Receivers', JTok) then
            InitReceivers(JTok.AsArray());
    end;

    var
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ContactCompanyFilter: Text;
}
