pageextension 56299 "ICI Service Item List" extends "Service Item List"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Serv. Item Det. Factbox"; "ICI Serv. Item Ticket Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission() THEN
            IF ICISupportSetup.GET() then
                ShowServiceFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowServiceFactbox: Boolean;
}
