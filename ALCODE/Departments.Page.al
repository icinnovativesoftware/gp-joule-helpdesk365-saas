page 56313 "ICI Departments"
{
    ApplicationArea = All;
    Caption = 'ICI Departments', Comment = 'de-DE=Support Abteilungen';
    PageType = List;
    SourceTable = "ICI Department";
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Description';
                }
            }
        }
    }

}
