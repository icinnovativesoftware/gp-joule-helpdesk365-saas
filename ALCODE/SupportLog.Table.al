table 56283 "ICI Support Log"
{
    Caption = 'ICI Support Log', Comment = 'de-DE=Support Log';
    DataClassification = SystemMetadata;
    LookupPageId = "ICI Support Log List";
    DrillDownPageId = "ICI Support Log List";

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = True;
        }
        field(10; Type; Option)
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = SystemMetadata;
            OptionMembers = Activity,Warning,Error;
            OptionCaption = 'Activity,Warning,Error', Comment = 'de-DE=Aktivität,Warnung,Fehler';
        }
        field(11; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = SystemMetadata;
        }
        field(12; Data; Blob)
        {
            Caption = 'Data', Comment = 'de-DE=Daten';
            DataClassification = SystemMetadata;
        }
        field(13; "Created By Type"; Option)
        {
            DataClassification = CustomerContent;
            OptionCaption = 'User,Contact,System', Comment = 'de-DE=Benutzer,Kontakt,System';
            OptionMembers = User,Contact,System;
            //Editable = false;
        }
        field(14; "Created By"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = if ("Created By Type" = const(User)) "ICI Support User" else
            if ("Created By Type" = const(Contact)) Contact;
            Caption = 'Created By', Comment = 'de-DE=Erstellt von';
            //Editable = false;
        }
        field(15; "Creation Date"; Date)
        {
            DataClassification = SystemMetadata;
            //Editable = false;
            Caption = 'Creation Date', Comment = 'de-DE=Erstellt am';
        }
        field(16; "Creation Time"; Time)
        {
            DataClassification = SystemMetadata;
            //Editable = false;
            Caption = 'Creation Time', Comment = 'de-DE=Erstellt um';
        }
        field(17; "Creation Timestamp"; DateTime)
        {
            Caption = 'Creation Timestamp', Comment = 'de-DE=Erstelltungszeitpunkt';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin
        Validate("Creation Date", DT2Date(CurrentDateTime));
        Validate("Creation Time", DT2Time(CurrentDateTime));
        Validate("Creation Timestamp", CurrentDateTime);
    end;

    procedure DownloadLogInformation()
    var
        lInStream: InStream;
        FileName: Text;
    begin
        CalcFields(Data);
        IF Data.HasValue() THEN BEGIN
            FileName := FORMAT("Entry No.") + '_LogInformation.html';
            Data.CreateInStream(lInStream);
            File.DownloadFromStream(lInStream, FileName, '', '', FileName);
        END;
    end;

}
