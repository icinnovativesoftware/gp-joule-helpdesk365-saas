page 56285 "ICI Support Ticket Log List"
{

    ApplicationArea = All;
    Caption = 'Support Ticket Log List', Comment = 'de-DE=Support Ticket Logs';
    PageType = List;
    SourceTable = "ICI Support Ticket Log";
    UsageCategory = History;
    ObsoleteState = Pending;
    ObsoleteReason = 'Just for Developing Purpose';

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Created By Type"; Rec."Created By Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Created By Type', Comment = 'de-DE=Erstellt von Art';
                }
                field("Created By"; Rec."Created By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Created By', Comment = 'de-DE=Erstellt von';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Date', Comment = 'de-DE=Erstellt am';
                }
                field("Creation Time"; Rec."Creation Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Time', Comment = 'de-DE=Erstellt um';
                }
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Entry No.';
                }
                field("Record ID"; Rec."Record ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Record ID';
                }
                field("Support Ticket No."; Rec."Support Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Support Ticket No.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Type';
                }
            }
        }
    }
}
