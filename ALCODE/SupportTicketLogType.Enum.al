enum 56276 "ICI Support Ticket Log Type"
{
    Extensible = true;

    value(0; State)
    {
        Caption = 'State', Comment = 'de-DE=Status';
    }
    value(1; "External Message")
    {
        Caption = 'Message', Comment = 'de-DE=Nachricht';
    }
    value(2; "External File")
    {
        Caption = 'File', Comment = 'de-DE=Datei';
    }
    value(3; Escalation)
    {
        Caption = 'Escalation', Comment = 'de-DE=Eskalation';
    }
    value(4; Document)
    {
        Caption = 'Document', Comment = 'de-DE=Beleg';
    }
    value(5; External)
    {
        Caption = 'External', Comment = 'de-DE=Extern';
    }
    value(6; "Internal Message")
    {
        Caption = 'InternalMessage', Comment = 'de-DE=Interne Nachricht';
    }
    value(7; "Internal File")
    {
        Caption = 'InternalFile', Comment = 'de-DE=Interne Datei';
    }
    value(8; "Time Registration")
    {
        Caption = 'Timeregistration', Comment = 'de-DE=Zeiterfassung';
    }
    value(9; Forwarding)
    {
        Caption = 'Forwarding', Comment = 'de-DE=Weiterleitung';
    }
    value(10; TimeReset)
    {
        Caption = 'Time Reset', Comment = 'de-DE=Zeit zurückgesetzt';
    }

}
