table 56278 "ICI Support Ticket Log"
{
    Caption = 'ICI Support Ticket Log', Comment = 'de-DE=Support Ticket Log';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
        }
        field(9; "Support Ticket No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
        }
        field(10; Type; Enum "ICI Support Ticket Log Type")
        {
            DataClassification = CustomerContent;
            Caption = 'Type', Comment = 'de-DE=Art';
            //Editable = false;
        }
        field(11; "Created By Type"; Option)
        {
            DataClassification = CustomerContent;
            OptionCaption = 'User,Contact,System', Comment = 'de-DE=Benutzer,Kontakt,System';
            OptionMembers = User,Contact,System;
            //Editable = false;
        }
        field(12; "Created By"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = if ("Created By Type" = const(User)) "ICI Support User" else
            if ("Created By Type" = const(Contact)) Contact;
            Caption = 'Created By', Comment = 'de-DE=Erstellt von';
            //Editable = false;
        }
        field(13; "Creation Date"; Date)
        {
            DataClassification = SystemMetadata;
            //Editable = false;
            Caption = 'Creation Date', Comment = 'de-DE=Erstellt am';
        }
        field(14; "Creation Time"; Time)
        {
            DataClassification = SystemMetadata;
            //Editable = false;
            Caption = 'Creation Time', Comment = 'de-DE=Erstellt um';
        }
        field(15; Data; Blob)
        {
            Caption = 'Data', Comment = 'de-DE=Daten';
            DataClassification = CustomerContent;
        }
        field(16; "Record ID"; RecordID)
        {
            Caption = 'Record ID', Comment = 'de-DE=Datensatz ID';
            DataClassification = CustomerContent;
        }
        field(17; "Creation Timestamp"; DateTime)
        {
            Caption = 'Creation Timestamp', Comment = 'de-DE=Erstelltungszeitpunkt';
            DataClassification = CustomerContent;
        }
        field(18; "Data Text"; Text[250])
        {
            Caption = 'Data Text', Comment = 'de-DE=Data Text';
            DataClassification = CustomerContent;
        }
        field(19; "Additional Text"; Text[50])
        {
            Caption = 'Additional Text', Comment = 'de-DE=Zusätzlicher Text';
            DataClassification = CustomerContent;
        }
        field(20; "Data Text 2"; Text[250])
        {
            Caption = 'Data Text 2', Comment = 'de-DE=Data Text 2';
            DataClassification = CustomerContent;
        }
        field(21; "State Subtype"; Enum "ICI Ticket State")
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = CustomerContent;
        }
        field(22; "Time Registration Subtype"; Option)
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = CustomerContent;
            OptionMembers = Item,Resource;
            OptionCaption = 'Item,Resource', Comment = 'de-DE=Artikel,Ressource';
        }
        field(23; "Time Registration No."; Code[20])
        {
            Caption = 'Time Registration No.', Comment = 'de-DE=Zeiterfassung';
            DataClassification = CustomerContent;
            TableRelation = IF ("Time Registration Subtype" = const(Item)) Item else
            if ("Time Registration Subtype" = const(Resource)) Resource;
        }
        field(24; "Time Reg. Unit of Measure"; Code[10])
        {
            Caption = 'Time Registration Unit of Measure', Comment = 'de-DE=Zeiterfassungseinheit';
            DataClassification = CustomerContent;
            TableRelation = "Unit of Measure";
        }
        field(25; "Time Registration Qty. Hours"; Decimal)
        {
            Caption = 'Time Registration Qty. Hours', Comment = 'de-DE=Anzahl Stunden';
            DataClassification = CustomerContent;
        }
        field(26; "Time Registration User"; Code[50])
        {
            Caption = 'Time Registration User', Comment = 'de-DE=Benutzer';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support User";
        }
        field(27; "Escalation User"; Code[50])
        {
            Caption = 'Escalation User', Comment = 'de-DE=Esckalationsbenutzer';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support User";
        }


    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
        key(Chat; "Support Ticket No.", "Creation Date", "Created By Type") { }
        key(Chat2; "Support Ticket No.", "Creation Timestamp") { }
        key(TicketDuration; "Support Ticket No.", "Type", "State Subtype", "Creation Timestamp") { MaintainSiftIndex = true; MaintainSqlIndex = true; }
        key(TimeRegistration; "Support Ticket No.", "Type", "Time Registration Subtype", "Time Registration No.", "Time Registration Qty. Hours")
        {
            MaintainSiftIndex = true;
            MaintainSqlIndex = true;
            SumIndexFields = "Time Registration Qty. Hours";
        }
    }
    trigger OnInsert()
    var
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        lCurrentDateTime: DateTime;
    begin
        lCurrentDateTime := ICISupportTicketLogMgt.GetCurrentDateTime();

        Validate("Creation Date", DT2Date(lCurrentDateTime));
        Validate("Creation Time", DT2Time(lCurrentDateTime));
        Validate("Creation Timestamp", lCurrentDateTime);

        // Validate("Creation Date", DT2Date(CurrentDateTime()));
        // Validate("Creation Time", DT2Time(CurrentDateTime()));
        // Validate("Creation Timestamp", CurrentDateTime());
    end;
}
