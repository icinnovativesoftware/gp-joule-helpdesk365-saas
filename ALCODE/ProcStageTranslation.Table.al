table 56315 "ICI Proc. Stage Translation"
{
    Caption = 'Proc. Stage Translation', Comment = 'de-DE=Prozessstufenübersetzung';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Process Code"; Code[20])
        {
            Caption = 'Process Code', Comment = 'de-DE=Processcode';
            DataClassification = ToBeClassified;
        }
        field(2; "Process Stage"; Integer)
        {
            Caption = 'Process Stage', Comment = 'de-DE=Stufe';
            DataClassification = ToBeClassified;
        }
        field(3; "Language Code"; Code[10])
        {
            Caption = 'Language Code', Comment = 'de-DE=Sprachcode';
            DataClassification = CustomerContent;
            TableRelation = Language;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Process Code", "Process Stage", "Language Code")
        {
            Clustered = true;
        }
    }

}
