tableextension 56293 "ICI Service Item" extends "Service Item"
{
    fields
    {
        field(56277; "ICI Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56278; "ICI Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56279; "ICI Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56280; "ICI Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
        field(56281; "ICI Tickets - Total"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No.")));
            Editable = false;
            Caption = 'No. of Tickets - Total', Comment = 'de-DE=Tickets - Gesamt';
        }
        field(56282; "ICI Tickets - Preparation"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Service Item No." = field("No."), "Ticket State" = const(Preparation)));
            Editable = false;
            Caption = 'No. of Tickets - Preparation', Comment = 'de-DE=Tickets - Vorbereitung';
        }
        field(56283; "ICI Address"; Text[100])
        {
            Caption = 'Address', Comment = 'de-DE=Adresse';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56284; "ICI Address 2"; Text[50])
        {
            Caption = 'Address 2', Comment = 'de-DE=Adresse 2';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56285; "ICI Post Code"; Code[20])
        {
            Caption = 'Post Code', Comment = 'de-DE=PLZ';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56286; "ICI City"; Text[30])
        {
            Caption = 'City', Comment = 'de-DE=Stadt';
            Editable = false;
            DataClassification = CustomerContent;
        }

        field(56287; "ICI Ship-to Address"; Text[100])
        {
            Caption = 'Ship-to Address', Comment = 'de-DE=Lieferadresse';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56288; "ICI Ship-to Address 2"; Text[50])
        {
            Caption = 'Ship-to Address 2', Comment = 'de-DE=Lieferadresse 2';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56289; "ICI Ship-to Post Code"; Code[20])
        {
            Caption = 'Ship-to Post Code', Comment = 'de-DE=Liefer-PLZ';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56290; "ICI Ship-to City"; Text[30])
        {
            Caption = 'Ship-to City', Comment = 'de-DE=Lieferstadt';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56291; "ICI Name"; Text[100])
        {
            Caption = 'Name', Comment = 'de-DE=Name';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56292; "ICI Name 2"; Text[50])
        {
            Caption = 'Name 2', Comment = 'de-DE=Name 2';
            Editable = false;
            DataClassification = CustomerContent;
        }

        field(56293; "ICI Ship-to Name"; Text[100])
        {
            Caption = 'Ship-to Name', Comment = 'de-DE=Liefername';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56294; "ICI Ship-to Name 2"; Text[50])
        {
            Caption = 'Ship-to Name 2', Comment = 'de-DE=Liefername 2';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(56300; "ICI HD No. 2"; Text[50])
        {
            Caption = 'HD No. 2', Comment = 'de-DE=Nr. 2 (HelpDesk)';
            DataClassification = CustomerContent;
        }
        modify("Customer No.")
        {
            trigger OnAfterValidate()
            begin
                CalcFields(Address, "Address 2", "Post Code", City);
                "ICI Address" := Address;
                "ICI Address 2" := "Address 2";
                "ICI Post Code" := "Post Code";
                "ICI Name" := Name;
                "ICI Name 2" := "Name 2";
                "ICI City" := City;
                CalcFields("Ship-To Address", "Ship-To Address 2", "Ship-To Post Code", "Ship-To City");
                "ICI Ship-To Address" := "Ship-To Address";
                "ICI Ship-To Address 2" := "Ship-To Address 2";
                "ICI Ship-To Post Code" := "Ship-To Post Code";
                "ICI Ship-To Name" := "Ship-To Name";
                "ICI Ship-To Name 2" := "Ship-To Name 2";
                "ICI Ship-To City" := "Ship-To City";
            end;
        }
        modify("Ship-to Code")
        {
            trigger OnAfterValidate()
            begin
                CalcFields("Ship-To Address", "Ship-To Address 2", "Ship-To Post Code", "Ship-To City");
                "ICI Ship-To Address" := "Ship-To Address";
                "ICI Ship-To Address 2" := "Ship-To Address 2";
                "ICI Ship-To Post Code" := "Ship-To Post Code";
                "ICI Ship-To Name" := "Ship-To Name";
                "ICI Ship-To Name 2" := "Ship-To Name 2";
                "ICI Ship-To City" := "Ship-To City";
                CalcFields(Address, "Address 2", "Post Code", City);
                "ICI Address" := Address;
                "ICI Address 2" := "Address 2";
                "ICI Post Code" := "Post Code";
                "ICI Name" := Name;
                "ICI Name 2" := "Name 2";
                "ICI City" := City;
            end;
        }
    }
    keys
    {
        key(AddressSearch; "ICI Address", "ICI Address 2", "ICI City", "ICI Name", "ICI Name 2", "ICI Post Code") { }
        key(ShipToAddressSearch; "ICI Ship-to Address", "ICI Ship-to Address 2", "ICI Ship-to City", "ICI Ship-to Name", "ICI Ship-to Name 2", "ICI Ship-to Post Code") { }

        key(SearchDescription; "Search Description") { }
        key(ICINo2; "ICI HD No. 2") { }
    }

    fieldgroups
    {
        addlast(DropDown; "Serial No.") { }
    }
}
