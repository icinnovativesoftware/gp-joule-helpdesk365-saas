page 56289 "ICI Support Ticket Text Module"
{
    ApplicationArea = All;
    Caption = 'ICI Support Ticket Text Module', Comment = 'de-DE=Support Ticket Textvorlagen';
    PageType = List;
    SourceTable = "ICI Support Text Module";
    UsageCategory = Lists;
    SourceTableView = where(Type = const(Ticket));

    layout
    {
        area(content)
        {
            repeater(General)
            {

                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Code';
                    Visible = Not LanguageEditing;
                }
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Sprachcode';
                    Visible = LanguageEditing;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Language Code', Comment = 'de-DE=Beschreibung';
                }
            }
        }
        area(FactBoxes)
        {
            part(Editor; "ICI Text Module Editor CardP")
            {
                ApplicationArea = All;
                SubPageLink = Code = field(Code), "Language Code" = field("Language Code"), "Type" = field("Type");
            }
        }
    }
    actions
    {
        area(Navigation)
        {
            action(Translation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Translations', Comment = 'de-DE=Übersetzungen';
                Image = Transactions;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                ToolTip = 'Shows the Translations of this Textmodule', Comment = 'de-DE=Zeigt die Übersetzungen dieser Textvorlage an';

                trigger OnAction()
                var
                    ICISupportTextModule: Record "ICI Support Text Module";
                    ICISupportTicketTextModule: Page "ICI Support Ticket Text Module";
                begin
                    CurrPage.SaveRecord();
                    ICISupportTextModule.SetRange(Code, Rec.Code);
                    ICISupportTextModule.SetRange(Type, Rec.Type);
                    ICISupportTextModule.SetFilter("Language Code", '<>%1', '');
                    ICISupportTicketTextModule.SetLanguageEditMode(true);

                    ICISupportTicketTextModule.SetTableView(ICISupportTextModule);
                    ICISupportTicketTextModule.SetRecord(ICISupportTextModule);

                    ICISupportTicketTextModule.RUN();

                end;
            }
        }
    }
    trigger OnInit()
    begin
        Rec.VALIDATE(Type, Rec.Type::Ticket);
        IF Rec.GetFilter(Code) <> '' then
            Rec.VALIDATE(Code, Rec.GetFilter(Code));
    end;

    trigger OnOpenPage()
    begin
        IF NOT LanguageEditing THEN
            Rec.SetRange("Language Code", '');
    end;

    procedure SetLanguageEditMode(pShowLanguageCode: Boolean)

    begin
        LanguageEditing := pShowLanguageCode;
    end;

    var
        LanguageEditing: Boolean;

}
