tableextension 56300 "ICI Interaction Log Entry" extends "Interaction Log Entry"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket";
        }
    }
}
