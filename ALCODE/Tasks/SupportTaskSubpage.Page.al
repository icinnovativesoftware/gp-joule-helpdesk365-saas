page 56349 "ICI Support Task Subpage"
{

    Caption = 'Support Task Subpage', Comment = 'de-DE=Aufgaben';

    SourceTable = "to-do";
    DeleteAllowed = true;
    InsertAllowed = false;
    ModifyAllowed = true;
    PageType = ListPart;
    SourceTableView = WHERE("System To-do Type" = CONST(Organizer));

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    ToolTip = 'Date', Comment = 'de-DE=Datum';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    Visible = false;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field(Closed; Rec.Closed)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    ToolTip = 'Closed', Comment = 'de-DE=Abgeschlossen';

                    trigger OnValidate()
                    begin
                        SetStyle();
                    end;
                }
                field(Canceled; Rec.Canceled)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    ToolTip = 'Canceled', Comment = 'de-DE=Abgebrochen';

                    trigger OnValidate()
                    begin
                        SetStyle();
                    end;
                }
                field("Activity Code"; Rec."Activity Code")
                {
                    ApplicationArea = All;
                    Editable = false;
                    StyleExpr = LineStyle;
                    Visible = false;
                    ToolTip = 'Activity Code', Comment = 'de-DE=Aktivitätencode';
                }
                field(Status; Rec.Status)
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyle;
                    ToolTip = 'Status', Comment = 'de-DE=Status';

                    trigger OnValidate()
                    begin
                        SetStyle();
                    end;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(CreateTask)
            {
                ApplicationArea = RelationshipMgmt;
                Caption = '&Create Task', Comment = 'de-DE=Aufgabe erstellen';
                Image = NewToDo;
                ToolTip = 'Create a new task.', Comment = 'de-DE=Aufgabe erstellen';

                trigger OnAction()
                var
                    SupportTicket: Record "ICI Support Ticket";
                    ICITodoMgt: Codeunit "ICI Todo Mgt.";
                begin
                    Rec.FILTERGROUP(4);
                    SupportTicket.FILTERGROUP(4);
                    Rec.COPYFILTER("ICI Support Ticket No.", SupportTicket."No.");
                    SupportTicket.FINDLAST();
                    ICITodoMgt.CreateTicketToDo(SupportTicket);
                end;
            }
            action(Edit)
            {
                ApplicationArea = All;
                Caption = 'Edit', Comment = 'de-DE=Bearbeiten';
                ToolTip = 'Edit', Comment = 'de-DE=Bearbeiten';
                Image = Edit;

                trigger OnAction()
                begin
                    PAGE.RUN(PAGE::"Task Card", Rec);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        SetStyle();
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec."ICI Support Ticket No." := SupportTicketNo;
    end;

    var
        SupportTicketNo: Code[20];
        LineStyle: Text[30];

    procedure SetSupportTicketNo(SuppTicketNo: Code[20])
    begin
        SupportTicketNo := SuppTicketNo;
    end;

    local procedure SetStyle()
    begin
        LineStyle := '';
        IF Rec.Status IN [Rec.Status::Completed, Rec.Status::Postponed] THEN
            LineStyle := 'Subordinate';
    end;

}
