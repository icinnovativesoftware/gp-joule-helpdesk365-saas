codeunit 56295 "ICI Todo Mgt."
{
    procedure CopyActivitiesToTasks(SupportTicket: Record "ICI Support Ticket")
    var
        SupportSetup: Record "ICI Support Setup";

        ActivityStep: Record "Activity Step";
        SupportUser: Record "ICI Support User";
        Todo: Record "To-do";
        OldTodo: Record "To-do";
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        SupportSetup.GET();
        IF NOT SupportSetup."Task Integration" THEN
            EXIT;

        IF NOT ICISupportProcessStage.GET(SupportTicket."Process Code", SupportTicket."Process Stage") THEN
            EXIT;


        IF ICISupportProcessStage."Activity Code" = '' THEN
            EXIT;
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            EXIT;

        SupportUser.Testfield("Salesperson Code");


        // Cancel old Todos
        OldTodo.SETRANGE("ICI Support Ticket No.", SupportTicket."No.");
        OldTodo.SetRange(Closed, false);
        IF OldTodo.FINDSET() THEN
            repeat
                OldTodo.VALIDATE(Canceled, TRUE);
                OldTodo.MODIFY();
            until OldTodo.next() = 0;


        // Creaste Todos from given Activity Code
        ActivityStep.SETRANGE("Activity Code", ICISupportProcessStage."Activity Code");
        IF ActivityStep.FINDSET() THEN
            REPEAT
                CLEAR(Todo);
                Todo.INIT();
                Todo.VALIDATE("ICI Support Ticket No.", SupportTicket."No.");

                Todo.VALIDATE("Salesperson Code", SupportUser."Salesperson Code");
                Todo.VALIDATE("Contact No.", SupportTicket."Current Contact No.");
                Todo.VALIDATE(Type, ActivityStep.Type);
                Todo.VALIDATE(Date, WORKDATE());
                Todo.VALIDATE(Priority, ActivityStep.Priority);
                Todo.VALIDATE(Description, ActivityStep.Description);
                Todo.VALIDATE("Activity Code", ActivityStep."Activity Code");
                Todo.INSERT(TRUE);
            UNTIL ActivityStep.NEXT() = 0;
    end;

    procedure CreateTicketToDo(var SupportTicket: Record "ICI Support Ticket")
    var
        SupportUser: Record "ICI Support User";
        //TempToDo: Record "To-Do" temporary;
        ToDo: Record "To-Do";
        CreateTaskWizard: Page "Create Task";
    begin
        SupportTicket.TESTFIELD("No.");
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            EXIT;

        SupportUser.Testfield("Salesperson Code");

        CLEAR(ToDo);
        ToDo.INIT();
        ToDo.VALIDATE("No.", '');
        ToDo.INSERT(true);
        ToDo.VALIDATE("Contact No.", SupportTicket."Current Contact No.");

        ToDo.VALIDATE("ICI Support Ticket No.", SupportTicket."No.");
        ToDo.VALIDATE("Salesperson Code", SupportUser."Salesperson Code");

        ToDo."Wizard Contact Name" := SupportTicket."Curr. Contact Name";
        ToDo."Wizard Step" := ToDo."Wizard Step"::"1";

        Todo.Duration := 1440 * 1000 * 60;
        Todo.Date := Today;
        Todo.GetEndDateTime();
        ToDo.MODIFY();

        //ToDo.StartWizard();
        //PAGE.RUN(PAGE::"Create Task", ToDo); // XXX

        ToDo.SetRecFilter();
        CreateTaskWizard.SetRecord(ToDo);
        CreateTaskWizard.Editable := true;
        CreateTaskWizard.Run();
    end;

    procedure SupportUserChanged(var SupportTicket: Record "ICI Support Ticket")
    var
        Todo: Record "To-do";

        SupportUser: Record "ICI Support User";
        SalespersonCode: Code[20];
    begin
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            exit;
        IF SupportUser."Salesperson Code" = '' THEN
            exit;
        SalespersonCode := SupportUser."Salesperson Code";

        Todo.SETRANGE("ICI Support Ticket No.", SupportTicket."No.");
        IF Todo.FINDSET() THEN
            REPEAT
                Todo.VALIDATE("Salesperson Code", SalespersonCode);
                Todo.MODIFY();
            UNTIL Todo.NEXT() = 0;
    end;

    procedure AllTasksFinished(var SupportTicket: Record "ICI Support Ticket"): Boolean
    var
        Todo: Record "To-do";
    begin
        Todo.SETRANGE("ICI Support Ticket No.", SupportTicket."No.");
        Todo.SETRANGE(Closed, FALSE);
        EXIT(Todo.IsEmpty());
    end;

    procedure CheckAllTasksFinished(var SupportTicket: Record "ICI Support Ticket"): Boolean
    var
        SupportSetup: Record "ICI Support Setup";
        TasksNotFinishedErr: Label 'Not all Tasks for the Ticket %1 have been finished', Comment = '%1 = Ticketno.|de-DE=Es gibt noch offene Aufgaben für Ticket %1';
    begin
        SupportSetup.Get();
        IF SupportSetup."Task Integration" then
            IF NOT AllTasksFinished(SupportTicket) then
                IF GuiAllowed THEN
                    Error(TasksNotFinishedErr, SupportTicket."No.");
    end;

    procedure MakePhoneCallFromTicket(var SupportTicket: Record "ICI Support Ticket"; InteractionTemplateCode: Code[20])
    var
        Contact: Record Contact;
        SupportUser: Record "ICI Support User";
        TempSegmentLine: Record "Segment Line" temporary;
    begin
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            EXIT;
        SupportUser.Testfield("Salesperson Code");

        TempSegmentLine.DELETEALL();
        TempSegmentLine.INIT();

        IF SupportTicket."Current Contact No." <> '' THEN
            Contact.GET(SupportTicket."Current Contact No.")
        ELSE
            Contact.GET(SupportTicket."Company Contact No.");

        TempSegmentLine.VALIDATE("Contact Via", Contact."Phone No.");
        TempSegmentLine.VALIDATE("Contact No.", Contact."No.");

        TempSegmentLine."ICI Support Ticket No." := SupportTicket."No.";
        TempSegmentLine.VALIDATE("Salesperson Code", SupportUser."Salesperson Code");

        TempSegmentLine.VALIDATE(Date, TODAY);

        TempSegmentLine."Wizard Step" := TempSegmentLine."Wizard Step"::"1";
        IF TempSegmentLine.Date = 0D THEN
            TempSegmentLine.Date := TODAY;
        TempSegmentLine."Time of Interaction" := TIME;
        TempSegmentLine."Interaction Successful" := TRUE;
        TempSegmentLine."Dial Contact" := TRUE;

        TempSegmentLine."Wizard Contact Name" := Contact.Name;


        TempSegmentLine.INSERT();
        TempSegmentLine.VALIDATE("Interaction Template Code", InteractionTemplateCode); // Can only validate after insert
        TempSegmentLine.Modify();
        PAGE.RUN(PAGE::"Make Phone Call", TempSegmentLine, TempSegmentLine."Contact Via");
    end;

    procedure CreateInteractionFromTicket(var SupportTicket: Record "ICI Support Ticket"; InteractionTemplateCode: Code[20])
    var
        TempSegmentLine: Record "Segment Line" temporary;
        Contact: Record "Contact";
        SupportUser: Record "ICI Support User";
    begin
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            EXIT;
        SupportUser.Testfield("Salesperson Code");

        TempSegmentLine.DELETEALL();
        TempSegmentLine.INIT();

        TempSegmentLine.Insert();
        TempSegmentLine."ICI Support Ticket No." := SupportTicket."No.";

        IF SupportTicket."Current Contact No." <> '' THEN
            Contact.GET(SupportTicket."Current Contact No.")
        ELSE
            Contact.GET(SupportTicket."Company Contact No.");

        IF Contact.Type = Contact.Type::Person THEN
            TempSegmentLine.SETRANGE("Contact Company No.", Contact."Company No.");

        TempSegmentLine.SETRANGE("Contact No.", Contact."No.");
        TempSegmentLine.VALIDATE("Contact No.", Contact."No.");

        TempSegmentLine."Salesperson Code" := SupportUser."Salesperson Code";

        TempSegmentLine."Wizard Contact Name" := Contact.Name;
        TempSegmentLine."Wizard Step" := TempSegmentLine."Wizard Step"::"1";
        TempSegmentLine."Interaction Successful" := TRUE;
        TempSegmentLine.VALIDATE(Date, WORKDATE());
        TempSegmentLine."Time of Interaction" := DT2TIME(ROUNDDATETIME(CURRENTDATETIME + 1000, 60000, '>'));

        IF InteractionTemplateCode <> '' then
            TempSegmentLine.VALIDATE("Interaction Template Code", InteractionTemplateCode);
        TempSegmentLine.modify();

        PAGE.RUN(PAGE::"Create Interaction", TempSegmentLine, TempSegmentLine."Interaction Template Code");
    end;

    procedure InsertClosingInteractionLogEntry(var SupportTicket: Record "ICI Support Ticket")
    var
        SupportSetup: Record "ICI Support Setup";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        InteractionTemplateCode: Code[20];
    begin
        SupportSetup.GET();
        IF NOT SupportSetup."Task Integration" THEN
            exit;
        IF SupportSetup."Closing Interaction Template" = '' THEN
            EXIT;

        InteractionTemplateCode := SupportSetup."Closing Interaction Template";

        IF NOT ICISupportProcessStage.GET(SupportTicket."Process Code", SupportTicket."Process Stage") THEN
            IF ICISupportProcessStage."Interaction Template Code" <> '' THEN
                InteractionTemplateCode := ICISupportProcessStage."Interaction Template Code";

        IF InteractionTemplateCode <> '' THEN
            InsertInteractionLogEntry(SupportTicket, InteractionTemplateCode);
    end;

    procedure InsertInteractionLogEntry(var SupportTicket: Record "ICI Support Ticket"; InteractionTemplateCode: Code[20])
    var
        InteractionLogEntry: Record "Interaction Log Entry";
        SupportUser: Record "ICI Support User";
        EntryNo: Integer;
    begin
        IF NOT SupportUser.Get(SupportTicket."Support User ID") THEN
            EXIT;
        SupportUser.Testfield("Salesperson Code");



        InteractionLogEntry.FINDLAST();
        EntryNo := InteractionLogEntry."Entry No." + 1;

        IF InteractionTemplateCode = '' THEN
            EXIT;

        InteractionLogEntry.INIT();
        InteractionLogEntry.VALIDATE("Entry No.", EntryNo);
        InteractionLogEntry.VALIDATE(Date, WORKDATE());
        InteractionLogEntry.VALIDATE("Contact Company No.", SupportTicket."Company Contact No.");
        IF SupportTicket."Current Contact No." <> '' THEN
            InteractionLogEntry.VALIDATE("Contact No.", SupportTicket."Current Contact No.");
        InteractionLogEntry.VALIDATE(Description, COPYSTR(SupportTicket.Description, 1, MAXSTRLEN(InteractionLogEntry.Description)));
        InteractionLogEntry.VALIDATE("User ID", USERID);
        InteractionLogEntry.VALIDATE("Salesperson Code", SupportUser."Salesperson Code");
        InteractionLogEntry.VALIDATE("ICI Support Ticket No.", SupportTicket."No.");
        InteractionLogEntry.VALIDATE("Interaction Template Code", InteractionTemplateCode);
        InteractionLogEntry.VALIDATE("Duration (Min.)", ROUND(SupportTicket.GetTicketProcessingTime() / 60000, 1));
        InteractionLogEntry.INSERT(TRUE);

    end;
}
