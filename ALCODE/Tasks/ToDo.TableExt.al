tableextension 56298 "ICI To-Do" extends "To-do"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket";
        }
        field(56277; "ICI Support User ID"; Code[50])
        {
            Caption = 'Support User', Comment = 'de-DE=Support Benutzer';
            TableRelation = "ICI Support User";
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = Lookup("ICI Support Ticket"."Support User ID" WHERE("No." = FIELD("ICI Support Ticket No.")));
        }
    }
}
