pageextension 56302 "ICI Interaction Log Entries" extends "Interaction Log Entries"
{
    layout
    {
        addlast(Control1)
        {
            field("ICI Support Ticket No."; Rec."ICI Support Ticket No.")
            {
                ApplicationArea = All;
                ToolTip = 'ICI Support Ticket No', Comment = 'de-DE=Ticketnr.';
            }
        }
    }
}
