tableextension 56299 "ICI Segment Line" extends "Segment Line"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket";
        }

    }
}
