page 56319 "ICI Support User Card"
{

    Caption = 'ICI Support User Card', Comment = 'de-DE=HelpDesk-Benutzerkarte';
    PageType = Card;
    SourceTable = "ICI Support User";
    UsageCategory = None;

    layout
    {
        area(content)
        {


            group(BCUSer)
            {
                Caption = 'Personal Information', Comment = 'de-DE=Business Central User';
                field("Business Central User"; Rec."Business Central User")
                {
                    ToolTip = 'Specifies the value of the User ID', Comment = 'de-DE=Benutzer ID';
                    ApplicationArea = All;
                    Importance = Promoted;
                    trigger OnAssistEdit()
                    var
                        UserSetup: Record "User Setup";
                    begin
                        if UserSetup.Get(Rec."Business Central User") then
                            page.Run(0, UserSetup);
                    end;
                }

            }
            group(PersonalInformation)
            {
                Caption = 'Personal Information', Comment = 'de-DE=Persönliche Daten';
                field("User ID"; Rec."User ID")
                {
                    ToolTip = 'Specifies the value of the User ID', Comment = 'de-DE=Benutzer ID field';
                    ApplicationArea = All;
                    NotBlank = true;
                    ShowMandatory = true;
                    Importance = Promoted;
                }
                group(UserName)
                {
                    field("Salutation Code"; Rec."Salutation Code")
                    {
                        ToolTip = 'Specifies the value of the Salutation Code', Comment = 'de-DE=Anredecode field';
                        ApplicationArea = All;
                    }
                    field("First Name"; Rec."First Name")
                    {
                        ToolTip = 'Specifies the value of the First Name', Comment = 'de-DE=Vorname field';
                        ApplicationArea = All;
                        Importance = Additional;
                    }
                    field("Middle Name"; Rec."Middle Name")
                    {
                        ToolTip = 'Specifies the value of the Middle Name', Comment = 'de-DE=Vorname 2 field';
                        ApplicationArea = All;
                        Importance = Additional;
                    }
                    field(Surname; Rec.Surname)
                    {
                        ToolTip = 'Specifies the value of the Surname', Comment = 'de-DE=Nachname field';
                        ApplicationArea = All;
                        Importance = Additional;
                    }
                    field(Name; Rec.Name)
                    {
                        ToolTip = 'Specifies the value of the User Name', Comment = 'de-DE=Benutzername field';
                        ApplicationArea = All;
                        Importance = Promoted;
                        ShowMandatory = true;
                    }

                    field("Job Title"; Rec."Job Title")
                    {
                        ToolTip = 'Specifies the value of the Job Title', Comment = 'de-DE=Job Titel field';
                        ApplicationArea = All;
                    }
                }

                field(Queue; Rec.Queue)
                {
                    ToolTip = 'Specifies the value of the Queue', Comment = 'de-DE=Gibt an, ob es sich bei dem Benutzer um einen Warteschlangenbenutzer handelt. Diese werden im Rollencenter in Kacheln angezeigt';
                    ApplicationArea = All;
                    Importance = Promoted;
                }
                field("TicketBoard active"; Rec."Ticket Board Active")
                {
                    Visible = ShowTicketboard;
                    ToolTip = 'Ticketboard Active', Comment = 'de-DE=Ticketboard aktiv';
                    ApplicationArea = All;
                }

            }
            group(Communication)
            {
                Caption = 'Communication', Comment = 'de-DE=Kommunikation';
                field("Language Code"; Rec."Language Code")
                {
                    ToolTip = 'Specifies the value of the Language Code', Comment = 'de-DE=Sprachcode field';
                    ApplicationArea = All;
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    ToolTip = 'Specifies the value of the E-Mail', Comment = 'de-DE=E-Mail field';
                    ApplicationArea = All;
                    Importance = Promoted;
                    ShowMandatory = not Rec.Queue;
                }
                field("Mobile Phone No."; Rec."Mobile Phone No.")
                {
                    ToolTip = 'Specifies the value of the Mobile Phone No.', Comment = 'de-DE=Mobiltelefonnr. field';
                    ApplicationArea = All;
                }
                field("Phone No."; Rec."Phone No.")
                {
                    ToolTip = 'Specifies the value of the Phone No.', Comment = 'de-DE=Telefonnr. field';
                    ApplicationArea = All;
                }
            }

            group(Config)
            {
                Caption = 'CompanyIntern', Comment = 'de-DE=Konfiguration';

                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    ToolTip = 'Specifies the value of the Salesperson Code', Comment = 'de-DE=Verkäufercode field';
                    ApplicationArea = All;
                    trigger OnAssistEdit()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        if salesperson.Get(rec."Salesperson Code") then
                            page.Run(0, Salesperson);
                    end;
                }
                field("Employee No."; Rec."Employee No.")
                {
                    ToolTip = 'Specifies the value of the Employee No.', Comment = 'de-DE=Mitarbeiternr.';
                    ApplicationArea = All;
                    Importance = Additional;
                    trigger OnAssistEdit()
                    var
                        Employee: Record Employee;
                    begin
                        if Employee.Get(rec."Employee No.") then
                            page.Run(0, Employee);
                    end;
                }
                group(TimeRegistration)
                {
                    Caption = 'Time Registration', Comment = 'de-DE=Zeiterfassung';

                    field("Payoff Type"; Rec."Sales Payoff Type")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Payoff Type', Comment = 'de-DE=Abrechnungsart';
                    }
                    field("Payoff No."; Rec."Sales Payoff No.")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Payoff No.', Comment = 'de-DE=Abrechnungsnr.';
                    }
                }
                field("Dep. Code"; Rec."Dep. Code")
                {
                    ToolTip = 'Specifies the value of the Department Filter', Comment = 'de-DE=Der Abteilungscode steuert die Abteilungsvorbelegung der erstellten Tickets';
                    ApplicationArea = All;
                }
                field("Dep. Filter"; Rec."Dep. Filter")
                {
                    ToolTip = 'Specifies the value of the Department Filter', Comment = 'de-DE=Der Abteilungsfilter steuert, welche Abteilungen der Benutzer im Ticketcenter stehen darf';
                    ApplicationArea = All;
                }
            }

            group(Webportal)
            {
                Caption = 'Webportal', Comment = 'de-DE=Kundenportal';
                Visible = PortalIntegration;
                field(Login; Rec.Login)
                {
                    ToolTip = 'Specifies the value of the Login', Comment = 'de-DE=Login';
                    ApplicationArea = All;
                    ShowMandatory = true;
                    Importance = Promoted;
                }
            }
            group(Rolcecenter)
            {

                Caption = 'Cues', Comment = 'de-DE=Rollencenterkonfiguration';
                group(Cues)
                {
                    Caption = 'Cues', Comment = 'de-DE=Kacheln';
                    field("Show Mailrobot Cues"; Rec."Show Mailrobot Cues")
                    {
                        ToolTip = 'Specifies the value of the Show Mailrobot Cues', Comment = 'de-DE=Gibt an, ob die Mailrobot Kacheln für diesen Benutzer angezeigt werden sollen.';
                        ApplicationArea = All;
                    }
                    field("Show Category Cues"; Rec."Show Category Cues")
                    {
                        ToolTip = 'Specifies the value of the Show Category Cues', Comment = 'de-DE=Gibt an, ob die Kategorie Kacheln für diesen Benutzer angezeigt werden sollen.';
                        ApplicationArea = All;
                    }
                    field("Show Process Cues"; Rec."Show Process Cues")
                    {
                        ToolTip = 'Specifies the value of the Show Process Cues', Comment = 'de-DE=Gibt an, ob die Prozess Kacheln für diesen Benutzer angezeigt werden sollen.';
                        ApplicationArea = All;
                    }
                    field("Show Queue Cues"; Rec."Show Queue Cues")
                    {
                        ToolTip = 'Specifies the value of the Show Queue Cues', Comment = 'de-DE=Gibt an, ob die Ticketwarteschlangen Kacheln für diesen Benutzer angezeigt werden sollen.';
                        ApplicationArea = All;
                    }
                    field("Show ToDo Cues"; Rec."Show ToDo Cues")
                    {
                        ToolTip = 'Specifies the value of the Show ToDo Cues', Comment = 'de-DE=Gibt an, ob die Aufgaben Kacheln für diesen Benutzer angezeigt werden sollen.';
                        ApplicationArea = All;
                    }
                }

                group(Charts)
                {
                    Caption = 'Charts', Comment = 'de-DE=Diagrammeoptionen';
                    field("Tickets by User Chart Type"; Rec."Tickets by User Chart Type")
                    {
                        ToolTip = 'Tickets by User Chart Type', Comment = 'de-DE=Tickets pro Benutzer Chartart';
                        ApplicationArea = All;
                    }
                    field("Tickets by Company Chart Type"; Rec."Tickets by Company Chart Type")
                    {
                        ToolTip = 'Tickets by Company Chart Type', Comment = 'de-DE=Tickets pro Kunde Diagrammart';
                        ApplicationArea = All;
                    }
                }
            }

        }
    }
    actions
    {

        area(Processing)
        {
            action(ICISendLogin)
            {
                Caption = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                ToolTip = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                ApplicationArea = All;
                Image = SendMail;
                Visible = PortalIntegration;

                trigger OnAction();
                var
                    ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    LoginSentTxt: Label 'Logindata has been sent', Comment = 'de-DE=Zugangsdaten erfolgreich versendet.';
                    LoginNotSentTxt: Label 'Logindata has not been sent', Comment = 'de-DE=Zugangsdaten konnten nicht versendet werden.';
                begin
                    CurrPage.SaveRecord();
                    IF ICISupportMailMgt.SendUserLogin(Rec."User ID", Rec.GeneratePassword()) THEN
                        MESSAGE(LoginSentTxt)
                    else
                        MESSAGE(LoginNotSentTxt);
                    CurrPage.Update(false);
                end;
            }
            action(SentMails)
            {
                Caption = 'Sent Mails', Comment = 'de-DE=Gesendete E-Mails';
                ToolTip = 'Sent Mails', Comment = 'de-DE=Öffnet die gesendeten E-Mails zu diesem Benutzer';
                ApplicationArea = All;
                Image = Log;
                RunObject = Page "Sent Emails";
                trigger OnAction()
                var
                    EMail: Codeunit Email;
                begin
                    EMail.OpenSentEmails(Database::"ICI Support User", Rec.SystemId);
                end;
            }


        }
        area(Navigation)
        {
            action(SearchConfig)
            {
                Caption = 'SearchConfig', Comment = 'de-DE=Suchoptionen einstellen';
                ToolTip = 'Search Config', Comment = 'de-DE=Legen Sie die Standard-Suchparameter für den Benutzer fest';
                ApplicationArea = All;
                Image = Find;
                RunObject = Page "ICI Support User Search Option";

            }
        }

    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF NOT ICISupportSetup.GET() THEN;
        PortalIntegration := ICISupportSetup."Portal Integration";
        ShowTicketboard := ICISupportSetup."Ticketboard active";
    end;

    var
        PortalIntegration: Boolean;
        ShowTicketboard: Boolean;
}

