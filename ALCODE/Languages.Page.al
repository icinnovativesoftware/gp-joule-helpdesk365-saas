page 56369 "ICI Languages"
{

    Caption = 'Languages', Comment = 'de-DE=Sprachen - RMA-Berichtslayouts';
    PageType = List;
    SourceTable = Language;
    DeleteAllowed = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Code"; Rec."Code")
                {

                    ToolTip = 'Specifies the value of the Code field.', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(Name; Rec.Name)
                {
                    ToolTip = 'Specifies the value of the Name field.', Comment = 'de-DE=Name';
                    ApplicationArea = All;
                    Editable = false;
                }

                field("ICI RMA Custom Layout Code"; Rec."ICI RMA Custom Layout Code")
                {
                    ToolTip = 'Specifies the value of the RMA Custom Layout Code field.', Comment = 'de-DE=RMA Berichtslayout';
                    ApplicationArea = All;
                }
            }
        }
    }

}
