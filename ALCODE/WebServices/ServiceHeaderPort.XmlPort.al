xmlport 56288 "ICI Service Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceHeader; "Service Header")
            {
                fieldelement(No; ServiceHeader."No.") { }
                fieldelement(Decription; ServiceHeader.Description) { }
                fieldelement(DocumentType; ServiceHeader."Document Type") { }
                fieldelement(ShiptoAddress; ServiceHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; ServiceHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; ServiceHeader."Ship-to City") { }
                fieldelement(ShiptoCode; ServiceHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; ServiceHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; ServiceHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; ServiceHeader."Ship-to County") { }
                fieldelement(ShiptoName; ServiceHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; ServiceHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; ServiceHeader."Ship-to Post Code") { }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(ServiceHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(StartingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        StartingDate := Format(ServiceHeader."Starting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(FinishingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        FinishingDate := Format(ServiceHeader."Finishing Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(SalespersonCode; ServiceHeader."Salesperson Code") { }
                fieldelement(SupportTicketNo; ServiceHeader."ICI Support Ticket No.") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(ServiceHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(ServiceHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(DueDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DueDate := Format(ServiceHeader."Due Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(YourReference; ServiceHeader."Your Reference") { }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(ServiceHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(ServiceHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Service Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentType; Lines."Document Type") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineContractNo; Lines."Contract No.") { }
                    fieldelement(LineServiceItemNo; Lines."Service Item No.") { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document Type", ServiceHeader."Document Type");
                        Lines.Setrange("Document No.", ServiceHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }


    trigger OnPreXmlPort()
    begin
        ServiceHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
