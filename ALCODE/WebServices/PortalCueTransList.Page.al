page 56351 "ICI Portal Cue Trans. List"
{

    Caption = 'Portal Cue Translation List', Comment = 'de-DE=Portal Kachelübersetzungen';
    PageType = List;
    UsageCategory = None;
    SourceTable = "ICI Portal Cue Translation";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Portal Cue Set Code"; Rec."Portal Cue Set Code")
                {
                    ToolTip = 'Specifies the value of the Portal Cue Set Code field', Comment = 'de-DE=Kundenportal Kachel Set Code';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Language Code"; Rec."Language Code")
                {
                    ToolTip = 'Specifies the value of the Language Code field', Comment = 'de-DE=Sprachcode';
                    ApplicationArea = All;
                }
                field("Portal Cue Line No."; Rec."Portal Cue Line No.")
                {
                    ToolTip = 'Specifies the value of the Line No. field', Comment = 'de-DE=Zeilennr.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        IF Rec.GetFilter("Portal Cue Set Code") <> '' then
            Rec."Portal Cue Set Code" := COPYSTR(Rec.GetFilter("Portal Cue Set Code"), 1, 20);

        IF Rec.GetFilter("Portal Cue Line No.") <> '0' then
            IF Evaluate(Rec."Portal Cue Line No.", Rec.GetFilter("Portal Cue Line No.")) THEN;
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        Rec.TestField("Language Code");
    end;
}
