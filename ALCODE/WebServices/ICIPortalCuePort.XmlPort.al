xmlport 56282 "ICI Portal Cue Port"
{
    Caption = 'Portal Cue Port';
    UseDefaultNamespace = True;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ICIPortalCue; "ICI Portal Cue")
            {
                fieldelement(PortalCueSetCode; ICIPortalCue."Portal Cue Set Code") { }
                fieldelement(LineNo; ICIPortalCue."Line No.") { }
                // fieldelement(Description; ICIPortalCue.Description) { }
                textelement(Description)
                {
                    trigger OnBeforePassVariable()
                    var
                        PersonContact: Record Contact;
                        ICIPoralCueMgt: Codeunit "ICI Portal Cue Mgt.";
                    begin
                        PersonContact.GET(PersonContactNo);
                        Description := ICIPoralCueMgt.GetCueTranslation(ICIPortalCue, PersonContact."Language Code");


                    end;
                }
                textelement(NoOfTickets)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICIPortalCueMgt: Codeunit "ICI Portal Cue Mgt.";
                    begin
                        NoOfTickets := FORMAT(ICIPortalCueMgt.CalcNoOfTickets(ICIPortalCue, CompanyContactNo, PersonContactNo));
                    end;
                }
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    procedure SetContactNo(pCompanyContactNo: Code[20]; pPersonContactNo: Code[20])
    begin
        CompanyContactNo := pCompanyContactNo;
        PersonContactNo := PPersonContactNo;
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
        CompanyContactNo: Code[20];
        PersonContactNo: Code[20];
}
