xmlport 56276 "ICI Ticket Details Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {

            tableelement(ICISupportTicket; "ICI Support Ticket")
            {
                fieldelement(Category1Code; ICISupportTicket."Category 1 Code")
                {
                }
                textelement(Category1Description)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportTicketCategory: Record "ICI Support Ticket Category";
                    begin
                        Category1Description := ICISupportTicket."Category 1 Description";
                        IF ICISupportTicketCategory.GET(ICISupportTicket."Category 1 Code") THEN
                            Category1Description := ICISupportTicketCategory.GetDescription();
                    end;
                }
                fieldelement(Category2Code; ICISupportTicket."Category 2 Code")
                {
                }
                textelement(Category2Description)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportTicketCategory: Record "ICI Support Ticket Category";
                    begin
                        Category2Description := ICISupportTicket."Category 2 Description";
                        IF ICISupportTicketCategory.GET(ICISupportTicket."Category 2 Code") THEN
                            Category2Description := ICISupportTicketCategory.GetDescription();
                    end;
                }
                fieldelement(CompanyContactName; ICISupportTicket."Comp. Contact Name")
                {
                }
                fieldelement(CompanyContactNo; ICISupportTicket."Company Contact No.")
                {
                }
                fieldelement(CurrentContactName; ICISupportTicket."Curr. Contact Name")
                {
                }
                fieldelement(CurrentContactNo; ICISupportTicket."Current Contact No.")
                {
                }
                fieldelement(CustomerName; ICISupportTicket."Cust. Name")
                {
                }
                fieldelement(CustomerNo; ICISupportTicket."Customer No.")
                {
                }
                fieldelement(ItemDescription; ICISupportTicket."Item Description")
                {
                }
                fieldelement(ItemNo; ICISupportTicket."Item No.")
                {
                }
                fieldelement(No; ICISupportTicket."No.")
                {
                }
                fieldelement(ProcessCode; ICISupportTicket."Process Code")
                {
                }
                fieldelement(ProcessLineDescription; ICISupportTicket."Process Stage Description")
                {
                }
                fieldelement(ProcessLineNo; ICISupportTicket."Process Stage")
                {
                }
                fieldelement(ProcessState; ICISupportTicket."Ticket State")
                {
                }
                fieldelement(SerialNo; ICISupportTicket."Serial No.")
                {
                }
                fieldelement(ServiceItemNo; ICISupportTicket."Service Item No.")
                {
                }
                fieldelement(SupportUserID; ICISupportTicket."Support User ID")
                {
                }
                fieldelement(SupportUserName; ICISupportTicket."Support User Name")
                {
                }
                fieldelement(Description; ICISupportTicket.Description)
                {
                }
                textelement(ProcessStageDescription)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportProcessStage: Record "ICI Support Process Stage";
                    begin
                        ProcessStageDescription := ICISupportTicket."Process Stage Description";
                        IF ICISupportProcessStage.GET(ICISupportTicket."Process Code", ICISupportTicket."Process Stage") THEN
                            ProcessStageDescription := ICISupportProcessStage.GetDescription();
                    end;
                }
                fieldelement(LastActivityDate; ICISupportTicket."Last Activity Date") { }
                textelement(TicketIsClosed)
                {
                    trigger OnBeforePassVariable()
                    begin
                        TicketIsClosed := '0';
                        IF ICISupportTicket."Ticket State" = "ICI Ticket State"::Closed then
                            TicketIsClosed := '1';
                    end;
                }

                tableelement(ICISupportTicketLog; "ICI Support Ticket Log")
                {
                    textelement(PortalGUID)
                    {
                        trigger OnBeforePassVariable()

                        begin
                            PortalGuid := DELCHr(ICISupportTicket."Portal GUID", '=', '{}-');
                        end;
                    }
                    fieldelement(EntryNo; ICISupportTicketLog."Entry No.") { }

                    fieldelement(CreatedBy; ICISupportTicketLog."Created By") { }
                    fieldelement(Type; ICISupportTicketLog."Type") { }
                    textelement(TypeAsInt)
                    {
                        trigger OnBeforePassVariable()
                        var
                            asInt: Integer;
                        begin
                            asInt := ICISupportTicketLog."Type".AsInteger();
                            TypeAsInt := FORMAT(asInt);
                        end;
                    }
                    fieldelement(CreatedByType; ICISupportTicketLog."Created By Type") { }

                    fieldelement(DataText; ICISupportTicketLog."Data Text") { }
                    fieldelement(DataText2; ICISupportTicketLog."Data Text 2") { }

                    fieldelement(CreationDate; ICISupportTicketLog."Creation Date") { }
                    fieldelement(CreationTime; ICISupportTicketLog."Creation Time") { }

                    textelement(LogDescription)
                    {
                        trigger OnBeforePassVariable()
                        var
                            ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                        begin
                            LogDescription := ICISupportTicketLogMgt.GetSenderInformation(ICISupportTicketLog."Entry No.");
                        end;
                    }
                    textelement(CreatedByName)
                    {
                        trigger OnBeforePassVariable()
                        var
                            ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                        begin
                            CreatedByName := ICISupportTicketLogMgt.GetTicketLogSenderName(ICISupportTicketLog."Entry No.");
                        end;
                    }
                    textelement(DataBlob)
                    {
                        trigger OnBeforePassVariable()
                        var
                            lInStream: InStream;
                        begin
                            if ICISupportTicketLog.Type = ICISupportTicketLog.Type::"External Message" THEN begin
                                IF ICISupportTicketLog.Data.HasValue() THEN
                                    ICISupportTicketLog.CalcFields(Data);
                                ICISupportTicketLog.Data.CreateInStream(lInStream, TextEncoding::Windows);
                                lInStream.ReadText(DataBlob); // Nachricht steht in Blob Feld
                            end;
                        end;
                    }

                    tableelement(Parts; Integer)
                    {

                        fieldelement(Number; Parts.Number) { }
                        textelement(PartData)
                        {
                            trigger OnBeforePassVariable()
                            begin
                                PartData := COPYSTR(FullDataText, (Parts.Number * MaxElementLength) + 1, MaxElementLength);
                            end;
                        }

                        trigger OnPreXmlItem()
                        var
                        // ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
                        // lInStream: InStream;
                        // FileExt: Text;
                        begin
                            // Moved to Ticket Log Port
                            Parts.SetRange(Number, 0, 0);
                            // Parts.SetRange(Number, 0, 0);

                            // if ICISupportTicketLog.Type = ICISupportTicketLog.Type::"External File" THEN begin
                            //     // Only for spported Image Files
                            //     FileExt := ICISupDragboxMgt.GetFileExtension(ICISupportTicketLog."Data Text");
                            //     IF ICISupportSetup."Show Filetypes as Image".Contains(FileExt) THEN // substring could also match. E.G. JPG is supported but file.jp would return true
                            //         IF ICISupportTicketLog.Data.HasValue() THEN
                            //             ICISupportTicketLog.CalcFields(Data);
                            //     ICISupportTicketLog.Data.CreateInStream(lInStream, TextEncoding::Windows);
                            //     lInStream.ReadText(FullDataText);

                            //     Parts.SetRange(Number, 0, 0);
                            //     if StrLen(FullDataText) > MaxElementLength then
                            //         Parts.SetRange(Number, 0, (strlen(FullDataText) DIV MaxElementLength));
                            // end;

                        end;
                    }


                    trigger OnPreXmlItem()
                    var
                    begin
                        ICISupportTicketLog.SetRange("Support Ticket No.", ICISupportTicket."No.");
                        ICISupportTicketLog.SetAscending("Entry No.", false);
                    end;
                }
                tableelement(ServiceItem; "Service Item")
                {
                    fieldelement(ServiceItemNo; ServiceItem."No.") { }
                    fieldelement(ServiceItemSerialNo; ServiceItem."Serial No.") { }
                    fieldelement(ServiceItemName; ServiceItem.Name) { }
                    fieldelement(ServiceItemDescription; ServiceItem.Description) { }

                    trigger OnPreXmlItem()
                    var
                    begin
                        IF ICISupportTicket."Service Item No." = '' then
                            currXMLport.Skip();
                        ServiceItem.SetRange("No.", ICISupportTicket."Service Item No.");
                    end;
                }
            }

            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }

    trigger OnPreXmlPort()
    begin
        MaxElementLength := 2048;
        ICISupportSetup.GET();
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        ICISupportSetup: Record "ICI Support Setup";
        FullDataText: Text;
        MaxElementLength: Integer;
        Authorized: Boolean;
}
