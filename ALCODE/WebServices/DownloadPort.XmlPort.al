xmlport 56293 "ICI Download Port"
{
    UseDefaultNamespace = true;
    Caption = 'Webarchiv Download Port';
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(Download; "Integer")
            {
                fieldelement(ID; Download.Number)
                {
                }
                textelement(FileName)
                {

                }
                tableelement(Parts; Integer)
                {

                    fieldelement(Number; Parts.Number) { }
                    textelement(PartData)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            PartData := COPYSTR(FullDataText, (Parts.Number * MaxElementLength) + 1, MaxElementLength);
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        //FullDataText := TempNameValueBuffer.GetValue();
                        DataBigText.GetSubText(FullDataText, 1);
                        Parts.SetRange(Number, 0, 0);
                        if StrLen(FullDataText) > MaxElementLength then
                            Parts.SetRange(Number, 0, (strlen(FullDataText) DIV MaxElementLength));
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
                Language: Codeunit Language;
            begin
                ICISupportSetup.GET();
                GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }

    trigger OnPreXmlPort()
    begin
        MaxElementLength := 2048;
        Download.SetRange(Number, 0, 0);
    end;

    procedure SetDownloadFile(pFileName: Text[250]; DataAsText: Text)
    begin
        DataBigText.AddText(DataAsText);
        //FullDataText := DataAsText;
        FileName := pFileName;
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;


    var
        DataBigText: BigText;
        FullDataText: Text;
        MaxElementLength: Integer;

        Authorized: Boolean;

}
