xmlport 56287 "ICI Sales Cr.Memo Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(SalesCrMemoHeader; "Sales Cr.Memo Header")
            {
                fieldelement(No; SalesCrMemoHeader."No.") { }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(SalesCrMemoHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; SalesCrMemoHeader."Posting Description") { }
                fieldelement(SelltoAddress; SalesCrMemoHeader."Sell-to Address") { }
                fieldelement(SelltoAddress2; SalesCrMemoHeader."Sell-to Address 2") { }
                fieldelement(SelltoCity; SalesCrMemoHeader."Sell-to City") { }
                fieldelement(SelltoContact; SalesCrMemoHeader."Sell-to Contact") { }
                fieldelement(SelltoContactNo; SalesCrMemoHeader."Sell-to Contact No.") { }
                fieldelement(SelltoCountryRegionCode; SalesCrMemoHeader."Sell-to Country/Region Code") { }
                fieldelement(SelltoCounty; SalesCrMemoHeader."Sell-to County") { }
                fieldelement(SelltoCustomerName; SalesCrMemoHeader."Sell-to Customer Name") { }
                fieldelement(SelltoCustomerName2; SalesCrMemoHeader."Sell-to Customer Name 2") { }
                fieldelement(SelltoCustomerNo; SalesCrMemoHeader."Sell-to Customer No.") { }
                fieldelement(SelltoEMail; SalesCrMemoHeader."Sell-to E-Mail") { }
                fieldelement(SelltoPhoneNo; SalesCrMemoHeader."Sell-to Phone No.") { }
                fieldelement(SelltoPostCode; SalesCrMemoHeader."Sell-to Post Code") { }
                fieldelement(SalespersonCode; SalesCrMemoHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(SalesCrMemoHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; SalesCrMemoHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; SalesCrMemoHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; SalesCrMemoHeader."Ship-to City") { }
                fieldelement(ShiptoCode; SalesCrMemoHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; SalesCrMemoHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; SalesCrMemoHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; SalesCrMemoHeader."Ship-to County") { }
                fieldelement(ShiptoName; SalesCrMemoHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; SalesCrMemoHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; SalesCrMemoHeader."Ship-to Post Code") { }
                textelement(ShipmentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        ShipmentDate := Format(SalesCrMemoHeader."Shipment Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(YourReference; SalesCrMemoHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(SalesCrMemoHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(ExternalDocumentNo; SalesCrMemoHeader."External Document No.") { }
                fieldelement(ShippingAgentCode; SalesCrMemoHeader."Shipping Agent Code") { }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    begin
                        RecID := FORMAT(SalesCrMemoHeader.RecordId);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(SalesCrMemoHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Sales Cr.Memo Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", SalesCrMemoHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        SalesCrMemoHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
