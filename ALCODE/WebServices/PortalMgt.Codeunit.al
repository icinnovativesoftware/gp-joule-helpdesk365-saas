codeunit 56297 "ICI Portal Mgt."
{
    // Maybe someday this will be put into use
    procedure UpdateAllTables()
    var
        PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
    begin

        UpdateAllCues(true, PortalUpdateMgt);
        UpdateAllCueTranslations(true, PortalUpdateMgt);
        UpdateAllContacts(true, PortalUpdateMgt);
        UpdateAllCategories(true, PortalUpdateMgt);
        UpdateAllCategorieTranslations(true, PortalUpdateMgt);
        UpdateAllDownloadCategories(true, PortalUpdateMgt);
        UpdateSetup(true, PortalUpdateMgt);

        UpdateAllCues(false, PortalUpdateMgt);
        UpdateAllCueTranslations(false, PortalUpdateMgt);
        UpdateAllContacts(false, PortalUpdateMgt);
        UpdateAllCategories(false, PortalUpdateMgt);
        UpdateAllCategorieTranslations(false, PortalUpdateMgt);
        UpdateAllDownloadCategories(false, PortalUpdateMgt);
        UpdateSetup(false, PortalUpdateMgt);

        PortalUpdateMgt.SendUpdateTableBuffered();  // Flush remaining data
    end;

    procedure ConnectionTest() Ok: Boolean
    var
        PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
    begin
        Ok := PortalUpdateMgt.SendRequest('connection_test', '');
        IF GuiAllowed then
            IF Ok then
                MESSAGE('Connected')
    end;

    procedure UpdateTest()
    var
        PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
        ICICommunicationMgt: Codeunit "ICI Communication Mgt.";
        Data: Text;
    begin
        ICICommunicationMgt.GetChatDataForTicket('T19-00142').WriteTo(Data);
        IF NOT PortalUpdateMgt.SendRequest('data_test', Data) then
            IF GuiAllowed then
                MESSAGE(PortalUpdateMgt.GetResonseDescription());
    end;

    procedure UpdateAllContacts(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        Contact: Record Contact;
    begin
        Contact.SetCurrentKey("ICI Support Active", "ICI E-Mail TLD");
        Contact.SetRange("ICI Support Active", true);

        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(Contact.Count());
            EXIT;
        end;

        IF Contact.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetContactJSON(Contact));
            until Contact.Next() = 0;

    end;

    procedure UpdateAllCues(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICIPortalCue: Record "ICI Portal Cue";
    begin

        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICIPortalCue.Count());
            EXIT;
        end;

        IF ICIPortalCue.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetCueJSON(ICIPortalCue));
            until ICIPortalCue.Next() = 0;
    end;

    procedure UpdateAllCueTranslations(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICIPortalCueTranslation: Record "ICI Portal Cue Translation";
    begin

        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICIPortalCueTranslation.Count());
            EXIT;
        end;

        IF ICIPortalCueTranslation.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetCueTranslationJSON(ICIPortalCueTranslation));
            until ICIPortalCueTranslation.Next() = 0;
    end;

    procedure UpdateAllCategories(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
    begin
        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICISupportTicketCategory.Count());
            EXIT;
        end;

        IF ICISupportTicketCategory.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetCategoryJSON(ICISupportTicketCategory));
            until ICISupportTicketCategory.Next() = 0;
    end;

    procedure UpdateAllCategorieTranslations(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICITickCategoryTranslation: Record "ICI Tick. Category Translation";
    begin
        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICITickCategoryTranslation.Count());
            EXIT;
        end;

        IF ICITickCategoryTranslation.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetCategoryTranslationJSON(ICITickCategoryTranslation));
            until ICITickCategoryTranslation.Next() = 0;
    end;

    procedure UpdateAllDownloadCategories(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICIDownloadCategory: Record "ICI Download Category";
    begin
        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICIDownloadCategory.Count());
            EXIT;
        end;

        IF ICIDownloadCategory.Findset() THEN
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetDownloadCategoryJSON(ICIDownloadCategory));
            until ICIDownloadCategory.Next() = 0;
    end;

    local procedure GetContactJSON(var Contact: Record Contact) JContact: JsonObject
    var
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
    begin

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support_contact');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('contact_no', Contact."No.");

        // Add fields (key must be a mysql field)
        JFields.Add('company_no', Contact."Company No.");
        JFields.Add('type', Contact.Type.AsInteger()); // Enums as Integers
        JFields.Add('company_name', Contact."Company Name");
        JFields.Add('name', Contact.Name);
        JFields.Add('name_2', Contact."Name 2");
        JFields.Add('address', Contact.Address);
        JFields.Add('post_code', Contact."Post Code");
        JFields.Add('city', Contact.City);
        JFields.Add('country', Contact."Country/Region Code");
        JFields.Add('phone_no', Contact."Phone No.");
        JFields.Add('mobile_phone_no', Contact."Mobile Phone No.");
        JFields.Add('fax_no', Contact."Fax No.");
        JFields.Add('email', Contact."E-Mail");
        JFields.Add('homepage', Contact."Home Page");
        JFields.Add('salutation', Contact.GetSalutation("Salutation Formula Salutation Type"::Formal, Contact."Language Code")); // Formal Salutation in Contacts Language 
        JFields.Add('salutation_code', Contact."Salutation Code");
        JFields.Add('salesperson_code', Contact."Salesperson Code");
        JFields.Add('login', Contact."ICI Login");
        JFields.Add('password', Contact."ICI Support Password Hash");
        JFields.Add('active', Contact."ICI Support Active");
        JFields.Add('languagecode', Contact."Language Code");
        JFields.Add('customer_no', Contact.GetCustomerNo());
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContact.Add('primary', JPrimary);
        JContact.Add('fields', JFields);
        JContact.Add('table', JTable);
    end;


    local procedure GetCueJSON(var ICIPortalCue: Record "ICI Portal Cue") JContainer: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
        EmptyTxt: Text;
    begin
        ICISupportSetup.GET();

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_cue');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('set_code', ICIPortalCue."Portal Cue Set Code");
        JPrimary.Add('line_no', ICIPortalCue."Line No.");
        JPrimary.Add('language_code', EmptyTxt);


        // Add fields (key must be a mysql field)
        JFields.Add('description', ICIPortalCue.Description);
        JFields.Add('default_cue', (ICIPortalCue."Portal Cue Set Code" = ICISupportSetup."Default Portal Cue Set Code"));
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;

    local procedure GetCueTranslationJSON(var ICIPortalCueTranslation: Record "ICI Portal Cue Translation") JContainer: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
    begin
        ICISupportSetup.GET();

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_cue');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('set_code', ICIPortalCueTranslation."Portal Cue Set Code");
        JPrimary.Add('line_no', ICIPortalCueTranslation."Portal Cue Line No.");
        JPrimary.Add('language_code', ICIPortalCueTranslation."Language Code");


        // Add fields (key must be a mysql field)
        JFields.Add('description', ICIPortalCueTranslation.Description);
        JFields.Add('default_cue', (ICIPortalCueTranslation."Portal Cue Set Code" = ICISupportSetup."Default Portal Cue Set Code"));
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;


    local procedure GetCategoryJSON(var ICISupportTicketCategory: Record "ICI Support Ticket Category") JContainer: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
    begin
        ICISupportSetup.GET();

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_ticket_category');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('code', ICISupportTicketCategory."code");
        JPrimary.Add('language_code', '');


        // Add fields (key must be a mysql field)
        JFields.Add('description', ICISupportTicketCategory.Description);
        JFields.Add('online', (ICISupportTicketCategory.Online));
        JFields.Add('inactive', (ICISupportTicketCategory.Inactive));
        JFields.Add('layer', (ICISupportTicketCategory.Layer));
        JFields.Add('parent_category', ICISupportTicketCategory."Parent Category");
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;

    local procedure GetCategoryTranslationJSON(var ICITickCategoryTranslation: Record "ICI Tick. Category Translation") JContainer: JsonObject
    var
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
        ICISupportSetup: Record "ICI Support Setup";
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
    begin
        ICISupportSetup.GET();

        ICISupportTicketCategory.GET(ICITickCategoryTranslation."Ticket Category Code");

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_ticket_category');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('code', ICISupportTicketCategory."code");
        JPrimary.Add('language_code', ICITickCategoryTranslation."Language Code");


        // Add fields (key must be a mysql field)
        JFields.Add('description', ICITickCategoryTranslation.Description);
        JFields.Add('online', (ICISupportTicketCategory.Online));
        JFields.Add('inactive', (ICISupportTicketCategory.Inactive));
        JFields.Add('layer', (ICISupportTicketCategory.Layer));
        JFields.Add('parent_category', ICISupportTicketCategory."Parent Category");
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;

    procedure UpdateSetup(CountRecords: Boolean; var PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.")
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF CountRecords THEN begin
            PortalUpdateMgt.AddNoOfRec(ICISupportSetup.Count()); // This should always be 1
            EXIT;
        end;

        IF ICISupportSetup.Findset() THEN // Only one Record
            repeat
                PortalUpdateMgt.UpdateTableBuffered(GetSetupJSON(ICISupportSetup));
            until ICISupportSetup.Next() = 0;
    end;

    local procedure GetSetupJSON(var ICISupportSetup: Record "ICI Support Setup") JContainer: JsonObject
    var
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
    begin

        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_setup');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());

        // Add fields (key must be a mysql field)
        JFields.Add('show_filetypes_as_image', ICISupportSetup."Show Filetypes as Image");
        JFields.Add('change_data_allowed', ICISupportSetup."Portal - Change Data Allowed");
        JFields.Add('url_agb', ICISupportSetup."Url - AGB");
        JFields.Add('url_dsgvo', ICISupportSetup."Url - DSGVO");
        JFields.Add('url_imprint', ICISupportSetup."Url - Imprint");
        JFields.Add('pass_use_numbers', ICISupportSetup."Password Use Numbers");
        JFields.Add('pass_use_lowercase', ICISupportSetup."Password Use Lowercase");
        JFields.Add('pass_use_uppercase', ICISupportSetup."Password Use Uppercase");
        JFields.Add('pass_use_special', ICISupportSetup."Password Use Special");
        JFields.Add('webarchive', ICISupportSetup."Document Integration");
        JFields.Add('category_connection', ICISupportSetup."Ticket Category Connection");
        JFields.Add('allow_ship_to_address_modify', ICISupportSetup."Allow Ship-To Address modify");


        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;

    local procedure GetDownloadCategoryJSON(var ICIDownloadCategory: Record "ICI Download Category") JContainer: JsonObject
    var
        JPrimary: JsonObject;
        JFields: JsonObject;
        JTable: JsonObject;
        CategoryLayer0: Text;
        CategoryLayer1: Text;
        CategoryLayer2: Text;
    begin
        // Add Tablename for generic update (Value must be the mysql tablename)
        JTable.Add('tablename', 'support365_download_category');

        // Add Companyname and Primary Keys (key must be a mysql field)
        JPrimary.Add('companyname', CompanyName());
        JPrimary.Add('code', ICIDownloadCategory."code");


        // Add fields (key must be a mysql field)
        JFields.Add('description', ICIDownloadCategory.Description);
        JFields.Add('online', (ICIDownloadCategory.Online));
        JFields.Add('inactive', (ICIDownloadCategory.Inactive));
        JFields.Add('layer', (ICIDownloadCategory.Layer));
        JFields.Add('parent_category', ICIDownloadCategory."Parent Category");

        ICIDownloadCategory.CalcFields("No. of Files");
        JFields.Add('no_of_files', (ICIDownloadCategory."No. of Files"));

        JFields.Add('total_no_of_files', ICIDownloadCategory.GetNoOfChildrenFiles());

        ICIDownloadCategory.GetLayerCodes(CategoryLayer0, CategoryLayer1, CategoryLayer2);
        JFields.Add('category_layer_0', CategoryLayer0);
        JFields.Add('category_layer_1', CategoryLayer1);
        JFields.Add('category_layer_2', CategoryLayer2);
        // Add fields here...


        // Add primary, fields and table to returned JsonObject
        JContainer.Add('primary', JPrimary);
        JContainer.Add('fields', JFields);
        JContainer.Add('table', JTable);
    end;
}
