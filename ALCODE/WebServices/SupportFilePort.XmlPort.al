xmlport 56281 "ICI Support File Port"
{
    UseDefaultNamespace = true;

    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ICISupportTicketLog; "ICI Support Ticket Log")
            {
                fieldelement(TicketNo; ICISupportTicketLog."Support Ticket No.") { }
                fieldelement(EntryNo; ICISupportTicketLog."Entry No.") { }
                fieldelement(FileName; ICISupportTicketLog."Data Text") { }


                tableelement(Parts; Integer)
                {

                    fieldelement(Number; Parts.Number) { }
                    textelement(PartData)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            PartData := COPYSTR(FullDataText, (Parts.Number * MaxElementLength) + 1, MaxElementLength);
                        end;
                    }

                    trigger OnPreXmlItem()
                    var
                        lInStream: InStream;
                    begin
                        IF ICISupportTicketLog.Data.HasValue() THEN
                            ICISupportTicketLog.CalcFields(Data);
                        ICISupportTicketLog.Data.CreateInStream(lInStream, TextEncoding::Windows);
                        lInStream.ReadText(FullDataText);

                        Parts.SetRange(Number, 0, 0);
                        if StrLen(FullDataText) > MaxElementLength then
                            Parts.SetRange(Number, 0, (strlen(FullDataText) DIV MaxElementLength));
                    end;
                }
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }


    trigger OnPreXmlPort()
    begin
        MaxElementLength := 2048;
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
        FullDataText: Text;
        MaxElementLength: Integer;
}
