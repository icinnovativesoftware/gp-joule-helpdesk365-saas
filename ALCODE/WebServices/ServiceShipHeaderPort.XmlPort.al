xmlport 56289 "ICI Service Ship. Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceShipmentHeader; "Service Shipment Header")
            {
                fieldelement(No; ServiceShipmentHeader."No.") { }
                fieldelement(Description; ServiceShipmentHeader.Description) { }

                fieldelement(SupportTicketNo; ServiceShipmentHeader."ICI Support Ticket No.") { }
                fieldelement(OrderNo; ServiceShipmentHeader."Order No.") { }
                textelement(OrderDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        OrderDate := Format(ServiceShipmentHeader."Order Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(ServiceShipmentHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(StartingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        StartingDate := Format(ServiceShipmentHeader."Starting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(FinishingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        FinishingDate := Format(ServiceShipmentHeader."Finishing Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; ServiceShipmentHeader."Posting Description") { }
                fieldelement(Address; ServiceShipmentHeader.Address) { }
                fieldelement(Address2; ServiceShipmentHeader."Address 2") { }
                fieldelement(City; ServiceShipmentHeader."City") { }
                fieldelement(ContactNo; ServiceShipmentHeader."Contact No.") { }
                fieldelement(CountryRegionCode; ServiceShipmentHeader."Country/Region Code") { }
                fieldelement(County; ServiceShipmentHeader."County") { }
                fieldelement(CustomerNo; ServiceShipmentHeader."Customer No.") { }
                fieldelement(EMail; ServiceShipmentHeader."E-Mail") { }
                fieldelement(PhoneNo; ServiceShipmentHeader."Phone No.") { }
                fieldelement(PostCode; ServiceShipmentHeader."Post Code") { }
                fieldelement(SalespersonCode; ServiceShipmentHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(ServiceShipmentHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; ServiceShipmentHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; ServiceShipmentHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; ServiceShipmentHeader."Ship-to City") { }
                fieldelement(ShiptoCode; ServiceShipmentHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; ServiceShipmentHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; ServiceShipmentHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; ServiceShipmentHeader."Ship-to County") { }
                fieldelement(ShiptoName; ServiceShipmentHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; ServiceShipmentHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; ServiceShipmentHeader."Ship-to Post Code") { }

                fieldelement(YourReference; ServiceShipmentHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(ServiceShipmentHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }

                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(ServiceShipmentHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(ServiceShipmentHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Service Shipment Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }
                    fieldelement(LineServiceItemNo; Lines."Service Item No.") { }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", ServiceShipmentHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }

            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ServiceShipmentHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
