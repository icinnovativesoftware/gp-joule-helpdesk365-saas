xmlport 56286 "ICI Sales Invoice Header Port"
{

    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(SalesInvoiceHeader; "Sales Invoice Header")
            {
                fieldelement(No; SalesInvoiceHeader."No.") { }
                fieldelement(QuoteNo; SalesInvoiceHeader."Quote No.") { }
                fieldelement(OrderNo; SalesInvoiceHeader."Order No.") { }
                textelement(OrderDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        OrderDate := Format(SalesInvoiceHeader."Order Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(SalesInvoiceHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; SalesInvoiceHeader."Posting Description") { }
                fieldelement(SelltoAddress; SalesInvoiceHeader."Sell-to Address") { }
                fieldelement(SelltoAddress2; SalesInvoiceHeader."Sell-to Address 2") { }
                fieldelement(SelltoCity; SalesInvoiceHeader."Sell-to City") { }
                fieldelement(SelltoContact; SalesInvoiceHeader."Sell-to Contact") { }
                fieldelement(SelltoContactNo; SalesInvoiceHeader."Sell-to Contact No.") { }
                fieldelement(SelltoCountryRegionCode; SalesInvoiceHeader."Sell-to Country/Region Code") { }
                fieldelement(SelltoCounty; SalesInvoiceHeader."Sell-to County") { }
                fieldelement(SelltoCustomerName; SalesInvoiceHeader."Sell-to Customer Name") { }
                fieldelement(SelltoCustomerName2; SalesInvoiceHeader."Sell-to Customer Name 2") { }
                fieldelement(SelltoCustomerNo; SalesInvoiceHeader."Sell-to Customer No.") { }
                fieldelement(SelltoEMail; SalesInvoiceHeader."Sell-to E-Mail") { }
                fieldelement(SelltoPhoneNo; SalesInvoiceHeader."Sell-to Phone No.") { }
                fieldelement(SelltoPostCode; SalesInvoiceHeader."Sell-to Post Code") { }
                fieldelement(SalespersonCode; SalesInvoiceHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(SalesInvoiceHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; SalesInvoiceHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; SalesInvoiceHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; SalesInvoiceHeader."Ship-to City") { }
                fieldelement(ShiptoCode; SalesInvoiceHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; SalesInvoiceHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; SalesInvoiceHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; SalesInvoiceHeader."Ship-to County") { }
                fieldelement(ShiptoName; SalesInvoiceHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; SalesInvoiceHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; SalesInvoiceHeader."Ship-to Post Code") { }
                textelement(ShipmentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        ShipmentDate := Format(SalesInvoiceHeader."Shipment Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(YourReference; SalesInvoiceHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(SalesInvoiceHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(DueDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DueDate := Format(SalesInvoiceHeader."Due Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(ExternalDocumentNo; SalesInvoiceHeader."External Document No.") { }
                fieldelement(ShippingAgentCode; SalesInvoiceHeader."Shipping Agent Code") { }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(SalesInvoiceHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                fieldelement(Amount; SalesInvoiceHeader.Amount)
                {
                    trigger OnBeforePassField()
                    begin
                        SalesInvoiceHeader.CalcFields(Amount);
                    end;
                }
                fieldelement(AmountIncludingVAT; SalesInvoiceHeader."Amount Including VAT")
                {
                    trigger OnBeforePassField()
                    begin
                        SalesInvoiceHeader.CalcFields("Amount Including VAT");
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(SalesInvoiceHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Sales Invoice Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", SalesInvoiceHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        SalesInvoiceHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
