xmlport 56279 "ICI Contact Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(Contact; Contact)
            {
                fieldelement(No; Contact."No.")
                {
                }
                fieldelement(Name; Contact.Name)
                {
                }
                fieldelement(Name2; Contact."Name 2")
                {
                }
                fieldelement(EMail; Contact."E-Mail")
                {
                }
                fieldelement(ICILogin; Contact."ICI Login")
                {
                }
                fieldelement(ICISupportActive; Contact."ICI Support Active")
                {
                }
                fieldelement(ICISupportPassword; Contact."ICI Support Password Hash")
                {
                }
                fieldelement(Address; Contact.Address)
                {
                }
                fieldelement(PostCode; Contact."Post Code") { }
                fieldelement(City; Contact.City)
                {
                }
                fieldelement(CompanyName; Contact."Company Name")
                {
                }
                fieldelement(CompanyNo; Contact."Company No.")
                {
                }
                fieldelement(County; Contact.County)
                {
                }
                fieldelement(Country; Contact."Country/Region Code")
                {
                }
                fieldelement(LanguageCode; Contact."Language Code")
                {
                }
                textelement(BCCompanyName)
                {
                    trigger OnBeforePassVariable()
                    begin
                        BCCompanyName := CompanyName();
                    end;
                }

                textelement(ContactType)
                {
                    trigger OnBeforePassVariable()
                    var
                        P1Lbl: Label '%1', Comment = '%1=Type as Int';
                    begin
                        ContactType := StrSubstNo(P1Lbl, Contact.Type.AsInteger())
                    end;
                }
                fieldelement(PhoneNo; Contact."Phone No.") { }
                fieldelement(MobilePhoneNo; Contact."Mobile Phone No.") { }
                fieldelement(FaxNo; Contact."Fax No.") { }
                fieldelement(HomePage; Contact."Home Page") { }
                textelement(Salutation)
                {
                    trigger OnBeforePassVariable()
                    begin
                        Salutation := Contact.GetSalutation("Salutation Formula Salutation Type"::Formal, Contact."Language Code");
                    end;
                }
                fieldelement(SalutationCode; Contact."Salutation Code") { }
                fieldelement(SalespersonCode; Contact."Salesperson Code") { }
                textelement(CustomerNo)
                {
                    trigger OnBeforePassVariable()
                    begin
                        CustomerNo := Contact.GetCustomerNo();
                    end;
                }
                textelement(Active)
                {
                    trigger OnBeforePassVariable()
                    begin
                        Active := '0';
                        if Contact."ICI Support Active" then
                            Active := '1';
                    end;
                }
                fieldelement(CueSetCode; Contact."ICI Portal Cue Set Code") { }
                textelement(NoOfTickets)
                {
                    trigger OnBeforePassVariable()
                    var
                        NoOfTicketsLbl: Label '%1', Comment = '%1=No of Tickets|de-DE=%1';
                    begin
                        Contact.CalcFields("ICI Tickets - Open", "ICI Tickets - Processing", "ICI Tickets - Waiting");

                        NoOfTickets := StrSubstNo(NoOfTicketsLbl, (Contact."ICI Tickets - Open" + Contact."ICI Tickets - Processing" + Contact."ICI Tickets - Waiting"));
                    end;
                }
                textelement(ShowSalesQuote)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Quote") then
                            ShowSalesQuote := '1'
                        else
                            ShowSalesQuote := '0';
                    end;
                }
                textelement(NoOfSalesQuote)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesQuote := '0';
                        if (Contact."ICI Sales Quote") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Quote);
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesQuote := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowSalesOrder)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Order") then
                            ShowSalesOrder := '1'
                        else
                            ShowSalesOrder := '0';
                    end;
                }
                textelement(NoOfSalesOrder)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesOrder := '0';
                        if (Contact."ICI Sales Order") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Order);
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesOrder := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowSalesInvoice)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Invoice") then
                            ShowSalesInvoice := '1'
                        else
                            ShowSalesInvoice := '0';
                    end;
                }
                textelement(NoOfSalesInvoice)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesInvoice := '0';
                        if (Contact."ICI Sales Invoice") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Invoice);
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesInvoice := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowSalesCrMemo)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Credit Memo") then
                            ShowSalesCrMemo := '1'
                        else
                            ShowSalesCrMemo := '0';
                    end;
                }
                textelement(NoOfSalesCrMemo)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesCrMemo := '0';
                        if (Contact."ICI Sales Credit Memo") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Credit Memo");
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesCrMemo := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }

                textelement(ShowSalesBlanketOrder)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Blanket Order") then
                            ShowSalesBlanketOrder := '1'
                        else
                            ShowSalesBlanketOrder := '0';
                    end;
                }
                textelement(NoOfSalesBlanketOrder)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesBlanketOrder := '0';
                        if (Contact."ICI Sales Blanket Order") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Blanket Order");
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesBlanketOrder := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowSalesReturnOrder)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Sales Return Order") then
                            ShowSalesReturnOrder := '1'
                        else
                            ShowSalesReturnOrder := '0';
                    end;
                }
                textelement(NoOfSalesReturnOrder)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesHeader: Record "Sales Header";
                        Customer: Record Customer;
                    begin
                        NoOfSalesReturnOrder := '0';
                        if (Contact."ICI Sales Return Order") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Return Order");
                                    SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfSalesReturnOrder := FORMAT(SalesHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedSalesShipment)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Sales Shipment") then
                            ShowPostedSalesShipment := '1'
                        else
                            ShowPostedSalesShipment := '0';
                    end;
                }
                textelement(NoOfPostedSalesShipment)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesShipmentHeader: Record "Sales Shipment Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedSalesShipment := '0';
                        if (Contact."ICI Posted Sales Shipment") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesShipmentHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesShipmentHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedSalesShipment := FORMAT(SalesShipmentHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedSalesInvoice)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Sales Invoice") then
                            ShowPostedSalesInvoice := '1'
                        else
                            ShowPostedSalesInvoice := '0';
                    end;
                }
                textelement(NoOfPostedSalesInvoice)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesInvoiceHeader: Record "Sales Invoice Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedSalesInvoice := '0';
                        if (Contact."ICI Posted Sales Invoice") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesInvoiceHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesInvoiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedSalesInvoice := FORMAT(SalesInvoiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedSalesCreditMemo)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Sales Cr.Memo") then
                            ShowPostedSalesCreditMemo := '1'
                        else
                            ShowPostedSalesCreditMemo := '0';
                    end;
                }
                textelement(NoOfPostedSalesCreditMemo)
                {
                    trigger OnBeforePassVariable()
                    var
                        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedSalesCreditMemo := '0';
                        if (Contact."ICI Posted Sales Cr.Memo") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    SalesCrMemoHeader.SetRange("Sell-to Customer No.", CustomerNo);
                                    SalesCrMemoHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedSalesCreditMemo := FORMAT(SalesCrMemoHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowServiceQuote)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Quote") then
                            ShowServiceQuote := '1'
                        else
                            ShowServiceQuote := '0';
                    end;
                }
                textelement(NoOfServiceQuote)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceHeader: Record "Service Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceQuote := '0';
                        if (Contact."ICI Service Quote") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::"Quote");
                                    ServiceHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceQuote := FORMAT(ServiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowServiceOrder)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Order") then
                            ShowServiceOrder := '1'
                        else
                            ShowServiceOrder := '0';
                    end;
                }
                textelement(NoOfServiceOrder)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceHeader: Record "Service Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceOrder := '0';
                        if (Contact."ICI Service Order") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::"Order");
                                    ServiceHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceOrder := FORMAT(ServiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowServiceInvoice)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Invoice") then
                            ShowServiceInvoice := '1'
                        else
                            ShowServiceInvoice := '0';
                    end;
                }
                textelement(NoOfServiceInvoice)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceHeader: Record "Service Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceInvoice := '0';
                        if (Contact."ICI Service Invoice") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::"Invoice");
                                    ServiceHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceInvoice := FORMAT(ServiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowServiceCrMemo)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Credit Memo") then
                            ShowServiceCrMemo := '1'
                        else
                            ShowServiceCrMemo := '0';
                    end;
                }
                textelement(NoOfServiceCrMemo)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceHeader: Record "Service Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceCrMemo := '0';
                        if (Contact."ICI Service Credit Memo") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::"Credit Memo");
                                    ServiceHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceCrMemo := FORMAT(ServiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedServiceShipment)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Service Shipment") then
                            ShowPostedServiceShipment := '1'
                        else
                            ShowPostedServiceShipment := '0';
                    end;
                }
                textelement(NoOfPostedServiceShipment)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceShipmentHeader: Record "Service Shipment Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedServiceShipment := '0';
                        if (Contact."ICI Posted Service Shipment") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceShipmentHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceShipmentHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedServiceShipment := FORMAT(ServiceShipmentHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedServiceInvoice)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Service Invoice") then
                            ShowPostedServiceInvoice := '1'
                        else
                            ShowPostedServiceInvoice := '0';
                    end;
                }
                textelement(NoOfPostedServiceInvoice)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceInvoiceHeader: Record "Service Invoice Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedServiceInvoice := '0';
                        if (Contact."ICI Posted Service Invoice") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceInvoiceHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceInvoiceHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedServiceInvoice := FORMAT(ServiceInvoiceHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowPostedServiceCreditMemo)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Posted Service Cr.Memo") then
                            ShowPostedServiceCreditMemo := '1'
                        else
                            ShowPostedServiceCreditMemo := '0';
                    end;
                }
                textelement(NoOfPostedServiceCreditMemo)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
                        Customer: Record Customer;
                    begin
                        NoOfPostedServiceCreditMemo := '0';
                        if (Contact."ICI Posted Service Cr.Memo") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceCrMemoHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceCrMemoHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfPostedServiceCreditMemo := FORMAT(ServiceCrMemoHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(ShowServiceContract)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Contract") then
                            ShowServiceContract := '1'
                        else
                            ShowServiceContract := '0';
                    end;
                }
                textelement(NoOfServiceContract)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceContractHeader: Record "Service Contract Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceContract := '0';
                        if (Contact."ICI Service Contract") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceContractHeader.SETRANGE("Contract Type", ServiceContractHeader."Contract Type"::Contract);
                                    ServiceContractHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceContractHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceContract := FORMAT(ServiceContractHeader.Count());
                                end;
                        end;

                    end;
                }
                textelement(ShowServiceContractQuote)
                {
                    trigger OnBeforePassVariable()
                    begin
                        if (Contact."ICI Service Contract Quote") then
                            ShowServiceContractQuote := '1'
                        else
                            ShowServiceContractQuote := '0';
                    end;
                }
                textelement(NoOfServiceContractQuote)
                {
                    trigger OnBeforePassVariable()
                    var
                        ServiceContractHeader: Record "Service Contract Header";
                        Customer: Record Customer;
                    begin
                        NoOfServiceContractQuote := '0';
                        if (Contact."ICI Service Contract Quote") then begin
                            CustomerNo := Contact.GetCustomerNo();
                            IF CustomerNo <> '' THEN
                                if Customer.GET(CustomerNo) THEN begin
                                    ServiceContractHeader.SETRANGE("Contract Type", ServiceContractHeader."Contract Type"::Quote);
                                    ServiceContractHeader.SetRange("Customer No.", CustomerNo);
                                    ServiceContractHeader.SetRange("ICI Hidden from Webarchive", false);
                                    NoOfServiceContractQuote := FORMAT(ServiceContractHeader.Count());
                                end;
                        end;
                    end;
                }
                textelement(TicketFromToken)
                {
                    trigger OnBeforePassVariable()
                    begin
                        TicketFromToken := EditTicketNo;
                    end;
                }
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        //Contact.SetCurrentKey("ICI Support Active", "ICI E-Mail TLD");
        Contact.SetRange("ICI Support Active", true);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure SetTicketNo(TicketNo: Code[20])
    begin
        EditTicketNo := TicketNo;
    end;

    var
        Authorized: Boolean;
        EditTicketNo: Code[20];
}
