xmlport 56291 "ICI Service Cr.M. Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceCrMemoHeader; "Service Cr.Memo Header")
            {
                fieldelement(No; ServiceCrMemoHeader."No.") { }
                fieldelement(ContactName; ServiceCrMemoHeader."Contact Name") { }
                fieldelement(Description; ServiceCrMemoHeader.Description) { }

                fieldelement(SupportTicketNo; ServiceCrMemoHeader."ICI Support Ticket No.") { }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(ServiceCrMemoHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; ServiceCrMemoHeader."Posting Description") { }
                fieldelement(Address; ServiceCrMemoHeader."Address") { }
                fieldelement(Address2; ServiceCrMemoHeader."Address 2") { }
                fieldelement(City; ServiceCrMemoHeader."City") { }
                fieldelement(ContactNo; ServiceCrMemoHeader."Contact No.") { }
                fieldelement(CountryRegionCode; ServiceCrMemoHeader."Country/Region Code") { }
                fieldelement(County; ServiceCrMemoHeader."County") { }
                fieldelement(CustomerNo; ServiceCrMemoHeader."Customer No.") { }
                fieldelement(EMail; ServiceCrMemoHeader."E-Mail") { }
                fieldelement(PhoneNo; ServiceCrMemoHeader."Phone No.") { }
                fieldelement(PostCode; ServiceCrMemoHeader."Post Code") { }
                fieldelement(SalespersonCode; ServiceCrMemoHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(ServiceCrMemoHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; ServiceCrMemoHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; ServiceCrMemoHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; ServiceCrMemoHeader."Ship-to City") { }
                fieldelement(ShiptoCode; ServiceCrMemoHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; ServiceCrMemoHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; ServiceCrMemoHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; ServiceCrMemoHeader."Ship-to County") { }
                fieldelement(ShiptoName; ServiceCrMemoHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; ServiceCrMemoHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; ServiceCrMemoHeader."Ship-to Post Code") { }
                fieldelement(YourReference; ServiceCrMemoHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(ServiceCrMemoHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(ServiceCrMemoHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(ServiceCrMemoHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Service Cr.Memo Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    fieldelement(LineServiceItemNo; Lines."Service Item No.") { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", ServiceCrMemoHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ServiceCrMemoHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
