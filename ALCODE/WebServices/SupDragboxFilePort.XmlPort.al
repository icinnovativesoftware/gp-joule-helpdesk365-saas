xmlport 56295 "ICI Sup Dragbox File Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ICISupDragboxFile; "ICI Sup. Dragbox File")
            {
                fieldelement(EntryNo; ICISupDragboxFile."Entry No.") { }
                fieldelement(Filename; ICISupDragboxFile.Filename) { }
                fieldelement(ShownFilename; ICISupDragboxFile."Shown Filename") { }
                fieldelement(Size; ICISupDragboxFile.Size) { }
                fieldelement(CreatedBy; ICISupDragboxFile."Created By") { }
                textelement(CreatedByName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportUser: Record "ICI Support User";
                    begin
                        IF ICISupportUser.GET(ICISupDragboxFile."Created By") THEN
                            CreatedByName := ICISupportUser.Name;
                    end;
                }
                textelement(CreatedOn)
                {
                    trigger OnBeforePassVariable()
                    begin
                        CreatedOn := Format(DT2Date(ICISupDragboxFile."Created On"), 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    begin
                        RecID := FORMAT(ICISupDragboxFile.RecordId());
                    end;
                }
                textelement(CategoryLayer0) { }
                textelement(CategoryLayer1) { }
                textelement(CategoryLayer2) { }
                textelement(CategoryLayer0Description) { }
                textelement(CategoryLayer1Description) { }
                textelement(CategoryLayer2Description) { }

                trigger OnAfterGetRecord()
                var
                    ICIDownloadCategory: Record "ICI Download Category";
                begin
                    // Get Layer 0,1,2
                    CLEAR(CategoryLayer0);
                    CLEAR(CategoryLayer1);
                    CLEAR(CategoryLayer2);
                    IF ICIDownloadCategory.GET(ICISupDragboxFile."ICI Download Category Code") THEN
                        ICIDownloadCategory.GetLayerCodes(CategoryLayer0, CategoryLayer1, CategoryLayer2);

                    CLEAR(CategoryLayer0Description);
                    CLEAR(CategoryLayer1Description);
                    CLEAR(CategoryLayer2Description);
                    IF ICIDownloadCategory.GET(CategoryLayer0) then
                        CategoryLayer0Description := ICIDownloadCategory.Description;
                    IF ICIDownloadCategory.GET(CategoryLayer1) then
                        CategoryLayer1Description := ICIDownloadCategory.Description;
                    IF ICIDownloadCategory.GET(CategoryLayer2) then
                        CategoryLayer2Description := ICIDownloadCategory.Description;
                end;

            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        // ICISupDragboxFile.SetRange(Status, ICISupDragboxFile.Status::Installed);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
}
