page 56342 "ICI Portal Cue Set Card"
{
    UsageCategory = None;
    Caption = 'Portal Cue Set Card', Comment = 'de-DE=Portalkachelset';
    PageType = Card;
    SourceTable = "ICI Portal Cue Set";

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code'', Comment = ''de-DE=Code field';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description'', Comment = ''de-DE=Beschreibung field';
                    ApplicationArea = All;
                }
            }
            part("ICI Portal Cue Listpart"; "ICI Portal Cue Listpart")
            {
                ApplicationArea = All;
                SubPageLink = "Portal Cue Set Code" = field(Code);
            }
        }
    }



}
