xmlport 56294 "ICI Download Category Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {

            tableelement(ICIDownloadCategory; "ICI Download Category")
            {
                fieldelement(ParentCategory; ICIDownloadCategory."Parent Category") { }
                fieldelement(Code; ICIDownloadCategory.Code) { }
                fieldelement(Description; ICIDownloadCategory.Description) { }
                fieldelement(Layer; ICIDownloadCategory.Layer) { }
                textelement(NoOfFiles)
                {
                    trigger OnBeforePassVariable()
                    begin
                        NoOfFiles := FORMAT(ICIDownloadCategory.GetNoOfChildrenFiles());
                    end;
                }
                textelement(CategoryLayer0) { }
                textelement(CategoryLayer1) { }
                textelement(CategoryLayer2) { }

                tableelement(Files; "ICI Sup. Dragbox File")
                {
                    fieldelement(FileName; Files.Filename) { }
                    fieldelement(Size; Files.Size) { }
                    fieldelement(CreatedBy; Files."Created By") { }
                    fieldelement(CreatedOn; Files."Created On") { }
                    textelement(CreatedOnDate)
                    {
                        trigger OnBeforePassVariable()
                        var
                            CreatedDate: Date;
                        begin
                            CreatedDate := DT2Date(Files."Created On");
                            CreatedOnDate := Format(CreatedDate, 0, '<Day,2>.<Month,2>.<Year4>')
                        end;
                    }
                    textelement(RecID)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            RecID := FORMAT(Files.RecordId());
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Files.SetRange("Record ID", ICIDownloadCategory.RecordId());
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    // Get Layer 0,1,2
                    CLEAR(CategoryLayer0);
                    CLEAR(CategoryLayer1);
                    CLEAR(CategoryLayer2);
                    ICIDownloadCategory.GetLayerCodes(CategoryLayer0, CategoryLayer1, CategoryLayer2);
                end;
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ICIDownloadCategory.SetRange(Online, true);
        ICIDownloadCategory.SetRange(Inactive, false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
}
