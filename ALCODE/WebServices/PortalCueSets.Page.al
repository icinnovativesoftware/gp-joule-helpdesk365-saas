page 56340 "ICI Portal Cue Sets"
{
    ApplicationArea = All;
    Caption = 'Portal Cue Sets', Comment = 'de-DE=Portal Kacheln';
    PageType = List;
    SourceTable = "ICI Portal Cue Set";
    UsageCategory = Administration;
    CardPageId = "ICI Portal Cue Set Card";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code', Comment = 'de-DE=Code field';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description', Comment = 'de-DE=Beschreibung field';
                    ApplicationArea = All;
                }
                field("No. of Portal Cues"; Rec."No. of Portal Cues")
                {
                    ToolTip = 'Specifies the value of the No. of Portal Cues', Comment = 'de-DE=Anzahl Kacheln field';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(PortalSync)
            {
                ApplicationArea = All;
                Image = LinkWeb;
                ToolTip = 'Portal Sync.', Comment = 'de-DE=Mit Portal synchronisieren';
                Caption = 'Portal Sync.', Comment = 'de-DE=Synchronisieren';
                trigger OnAction()
                var
                    ICIPortalMgt: Codeunit "ICI Portal Mgt.";
                    PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
                begin
                    ICIPortalMgt.UpdateAllCues(true, PortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCueTranslations(true, PortalUpdateMgt);

                    ICIPortalMgt.UpdateAllCues(false, PortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCueTranslations(false, PortalUpdateMgt);
                    PortalUpdateMgt.SendUpdateTableBuffered();  // Flush remaining data

                    CurrPage.Update(FALSE);
                end;
            }
        }
    }
}
