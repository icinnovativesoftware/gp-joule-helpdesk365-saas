page 56341 "ICI Portal Cue Listpart"
{

    Caption = 'Portal Cue Listpart', Comment = 'de-DE=Kacheln';
    PageType = ListPart;
    SourceTable = "ICI Portal Cue";
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Portal Cue Set Code"; Rec."Portal Cue Set Code")
                {
                    Visible = false;
                    ToolTip = 'Specifies the value of the Portal Cue Set Code', Comment = 'de-DE=Portal Kachel Set Code field';
                    ApplicationArea = All;
                }
                field("Line No."; Rec."Line No.")
                {
                    Visible = false;
                    ToolTip = 'Specifies the value of the Line No.', Comment = 'de-DE=Zeilennr. field';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description', Comment = 'de-DE=Beschreibung field';
                    ApplicationArea = All;
                }
                field("No. of Translations"; Rec."No. of Translations")
                {
                    ToolTip = 'Specifies the value of the No. of Translations', Comment = 'de-DE=Anzahl der Übersetzungen';
                    ApplicationArea = All;
                }
                field("Process Filter"; Rec."Process Filter")
                {
                    ToolTip = 'Specifies the value of the Process Filter', Comment = 'de-DE=Prozess Filter';
                    ApplicationArea = All;
                }
                field("Process Stage Filter"; Rec."Process Stage Filter")
                {
                    ToolTip = 'Specifies the value of the Process Stage Filter', Comment = 'de-DE=Prozessstufen Filter';
                    ApplicationArea = All;
                }
                field("State Filter"; Rec."State Filter")
                {
                    ToolTip = 'Specifies the value of the State Filter', Comment = 'de-DE=Status Filter';
                    ApplicationArea = All;
                }
                field("Only Own"; Rec."Only Own")
                {
                    ToolTip = 'Only Own', Comment = 'de-DE=Nur Eigene';
                    ApplicationArea = All;
                }
                field(Inactive; Rec.Inactive)
                {
                    ToolTip = 'Inactive', Comment = 'de-DE=Inaktiv';
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(Translation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Translations', Comment = 'de-DE=Übersetzungen';
                Image = Transactions;
                ToolTip = 'Shows the Translations of this Cue', Comment = 'de-DE=Zeigt die Übersetzungen dieser Kachel an';

                trigger OnAction()
                var
                    ICIPortalCueTranslation: Record "ICI Portal Cue Translation";
                    ICIPortalCueTransList: Page "ICI Portal Cue Trans. List";
                begin
                    CurrPage.SaveRecord();
                    ICIPortalCueTranslation.SetRange("Portal Cue Set Code", Rec."Portal Cue Set Code");
                    ICIPortalCueTranslation.SetRange("Portal Cue Line No.", Rec."Line No.");
                    ICIPortalCueTranslation.SetFilter("Language Code", '<>%1', '');

                    ICIPortalCueTransList.SETRECORD(ICIPortalCueTranslation);
                    ICIPortalCueTransList.SETTABLEVIEW(ICIPortalCueTranslation);
                    ICIPortalCueTransList.RUN();

                end;
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    var
        CueSetFilter: Code[20];
    begin
        Rec.FilterGroup(0); // From Lookup Flowfield or Subpagelink in Button
        IF Rec.GetFilter("Portal Cue Set Code") <> '' then begin
            CueSetFilter := COPYSTR(Rec.GetFilter("Portal Cue Set Code"), 1, 20);
            Rec.VALIDATE("Portal Cue Set Code", CueSetFilter);
        end;

    END;





}
