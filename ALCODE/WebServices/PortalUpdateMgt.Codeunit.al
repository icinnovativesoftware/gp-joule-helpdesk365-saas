codeunit 56298 "ICI Portal Update Mgt."
{
    procedure UpdateTableBuffered(DataObject: JsonObject)
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.GET();
        JRecords.Add(DataObject);
        IF JRecords.Count > ICISupportSetup."Portal Sync. No Of Recs." then
            SendUpdateTableBuffered();
    end;

    procedure SendUpdateTableBuffered()
    var
        JRecordsText: Text;
    begin
        JRecords.WriteTo(JRecordsText);
        IF NOT SendRequest('update', JRecordsText) then
            IF GuiAllowed then
                Error('Fehler: %1 \Fehlertext: %2', response_description, response_data);

        CLEAR(JRecords);
    end;

    procedure SendRequest(ActionName: Text; Data: Text): Boolean
    var
        ICISupportSetup: Record "ICI Support Setup";
        Base64Convert: Codeunit "Base64 Convert";
        lHttpClient: HttpClient;
        AuthString: Text;
        UserName: Text;
        Password: Text;
        CallURL: Text;
        lHttpResponseMessage: HttpResponseMessage;
        lHttpRequestMessage: HttpRequestMessage;
        lHttpContent: HttpContent;
        lHttpHeaders: HttpHeaders;
        ResponseAsText: Text;
        AuthString1Txt: Label '%1:%2', Comment = 'Needed for Building Basig Http Auth String. %1 = Username; %2 = Password';
        AuthString2Txt: Label 'Basic %1', Comment = 'Needed for Building Basig Http Auth String. %1 is the Base64 Encoded String from AutoString1Txt ';
        jObj: JsonObject;
        lresponse_description: Text;
        lresponse_data: Text;
    begin
        clear(response_code);
        clear(response_description);
        clear(response_description);

        ICISupportSetup.GET();
        ICISupportSetup.TestField("Link to Portal Backend");
        CallURL := ICISupportSetup."Link to Portal Backend" + '/module/support/bcconnect/bcconnect.php?action=' + ActionName;

        lHttpContent.GetHeaders(lHttpHeaders);
        lHttpHeaders.Clear();
        lHttpHeaders.Add('Content-Type', 'application/json'); // Send Json

        UserName := 'Gast';
        Password := 'Gast';

        AuthString := STRSUBSTNO(AuthString1Txt, UserName, Password); // Basic HTTP Auth
        AuthString := Base64Convert.ToBase64(AuthString);
        AuthString := STRSUBSTNO(AuthString2Txt, AuthString);
        lHttpClient.DefaultRequestHeaders().Add('Authorization', AuthString);


        Data := Base64Convert.ToBase64(Data);
        lHttpContent.WriteFrom(Data); // Add Payload to Request

        lHttpRequestMessage.Content := lHttpContent;

        lHttpRequestMessage.SetRequestUri(CallURL);
        lHttpRequestMessage.Method := 'POST';

        lHttpClient.Send(lHttpRequestMessage, lHttpResponseMessage);

        // Read the response content as json.
        lHttpResponseMessage.Content().ReadAs(ResponseAsText);

        if not jObj.ReadFrom(ResponseAsText) then
            if GuiAllowed then
                Error(ResponseAsText);

        response_code := GetJsonToken(jObj, 'response_code').AsValue().AsInteger();
        lresponse_description := GetJsonToken(jObj, 'response_description').AsValue().AsText();
        response_description := Base64Convert.FromBase64(lresponse_description);

        lresponse_data := GetJsonToken(jObj, 'response_data').AsValue().AsText();
        response_data := Base64Convert.FromBase64(lresponse_data);

        EXIT(response_code = 100);
    end;

    procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text) jToken: JsonToken;
    begin
        if not JsonObject.GET(TokenKey, jToken) then
            ERROR('Could not find a token with key %1', TokenKey);
    end;


    procedure GetResponseCode(): Integer
    begin
        EXIT(response_code);
    end;

    procedure GetResonseDescription(): Text
    begin
        EXIT(response_description);
    end;

    procedure GetResponseData(): Text
    begin
        EXIT(response_data);
    end;

    procedure AddNoOfRec(pNoOfRecs: Integer)
    begin
        //NoOfRecords += pNoOfRecs; // Added for Statusindicator - not yet implemented
    end;

    var
        JRecords: JsonArray;
        response_code: Integer;
        response_description: Text;
        response_data: Text;
    // NoOfRecords: Integer;


}
