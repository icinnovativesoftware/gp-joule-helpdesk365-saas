codeunit 56283 "ICI Support Webservice API"
{
    // Exit codes:
    // 100 = OK
    // 400 = Found more than one Contact with this login an pw
    // 402 = Found no Contact
    // 403 = Wrong PW
    // 404 = Login Locked
    procedure GetContactLogin(Login: Text[100]; PasswordHash: Text[50]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
    begin
        ICISupportSetup.GET();
        if (login = '') or (PasswordHash = '') then
            ERROR('402');

        Contact.SetCurrentKey("ICI Login", "ICI Support Active");
        Contact.setfilter("ICI Login", '%1', Login);
        Contact.SetRange("ICI Support Active", true);

        IF Contact.COUNT() = 0 then
            ERROR('402');

        IF Contact.Count() > 1 THEN
            ERROR('400');

        Contact.FindFirst();

        IF Contact."Login Failures" > ICISupportSetup."Max. Login Failures" then
            ERROR('404');

        IF Contact."ICI Support Password Hash" <> UpperCase(PasswordHash) then begin
            // Failure Count
            Contact."Login Failures" += 1;
            Contact.Modify();
            ERROR('403');
        end;

        ICIContactPort.SetTableView(Contact);
        ICIContactPort.Authorize();

        Contact."Login Failures" := 0;
        Contact.Modify();

        EXIT(100);
    end;

    // Exit codes:
    // 100 = OK
    // 400 = token not found or expired
    // 401 = no contact for token

    procedure GetContactLoginByToken(Token: Text[32]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIPortalToken: Record "ICI Portal Token";
        Contact: Record "Contact";
    begin
        ICISupportSetup.GET();

        if (Token = '') then
            Exit(400);

        ICIPortalToken.SetRange(Token, Token);
        ICIPortalToken.SetFilter("Create Datetime", '%1..', CurrentDateTime() - ICISupportSetup."Token Duration");
        IF NOT ICIPortalToken.FindLast() then
            EXIT(400);

        IF ICIPortalToken."Contact No." = '' THEN
            EXIT(401);

        IF ICIPortalToken."Ticket No." <> '' then
            ICIContactPort.SetTicketNo(ICIPortalToken."Ticket No.");

        Contact.GET(ICIPortalToken."Contact No.");
        Contact.SetRecFilter();

        ICIContactPort.SetTableView(Contact);
        ICIContactPort.Authorize();

        EXIT(100);
    end;

    // Exit codes:
    // 100 = OK
    // 400 = Found more than one User with this login an pw
    // 402 = Found no User
    procedure GetUserLogin(Login: Text[100]; PasswordHash: Text[50]; var ICISupportUserPort: XmlPort "ICI Support User Port"): Integer
    var
        ICISupportUser: Record "ICI Support User";
    begin
        EXIT(400); // Benutzerzugang nicht möglich

        if (login = '') or (PasswordHash = '') then
            Exit(402);

        ICISupportUser.setfilter("Login", '%1', Login);
        ICISupportUser.SetRange("Password Hash", UpperCase(PasswordHash));
        ICISupportUserPort.SetTableView(ICISupportUser);

        IF ICISupportUser.COUNT() = 0 then
            EXIT(402);

        IF ICISupportUser.Count() > 1 THEN
            EXIT(400);

        ICISupportUserPort.Authorize();
        EXIT(100);
    end;

    // Exit codes:
    // 100 = User Found
    // 400 = No User Found
    procedure GetSupportUser(pUserID: Code[50]; var ICISupportUserPort: XmlPort "ICI Support User Port"): Integer
    var
        ICISupportUser: Record "ICI Support User";
    begin
        ICISupportUser.SetRange("User ID", pUserID);
        ICISupportUserPort.SetTableView(ICISupportUser);
        IF Not ICISupportUser.GET(pUserID) then
            Exit(400);
        ICISupportUserPort.Authorize();
        EXIT(100);
    end;

    // Exit codes:
    // 100 = Contact Found
    // 400 = No Contact Found
    procedure GetContact(pContactNo: Code[20]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        Contact: Record "Contact";
    begin

        if (pContactNo = '') then
            exit(400);

        Contact.SetRange("No.", pContactNo);
        Contact.SetRange("ICI Support Active", true);
        ICIContactPort.SetTableView(Contact);

        IF Contact.Count() <> 1 then
            Exit(400);

        ICIContactPort.Authorize();
        EXIT(100);
    end;

    // 100 - OK
    // 402 Contact not found
    procedure GetTickets(ContactNo: Code[20]; PortalCueLineNo: Text[30]; var ICITicketPort: XmlPort "ICI Ticket Port"): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICIPortalCueMgt: Codeunit "ICI Portal Cue Mgt.";
        ICISupportSetup: Record "ICI Support Setup";
    begin

        if not ICISupportSetup.Get() then
            exit(403);

        if (ContactNo = '') then
            exit(402);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(402);

        SetLanguage(Contact."Language Code");

        if PortalCueLineNo <> '' THEN
            ICIPortalCueMgt.ApplyCueFilters(ICISupportTicket, ContactNo, PortalCueLineNo)
        ELSE begin
            // Apply general Filters for Tickets
            ICISupportTicket.SetCurrentKey("Company Contact No.", "Ticket State");
            ICISupportTicket.SetRange("Company Contact No.", Contact."Company No.");
            ICISupportTicket.SetFilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Preparation);
        end;

        ICISupportTicket.SetRange(Online, true);
        if ICISupportSetup."Guest Contact Company No." <> '' then
            if Contact."Company No." = ICISupportSetup."Guest Contact Company No." then
                ICISupportTicket.SetRange("Current Contact No.", ContactNo);
        ICITicketPort.SetTableView(ICISupportTicket);
        ICITicketPort.Authorize();
        EXIT(100);
    end;

    // DEPRICATED!!!
    // 100 - OK
    // 400 - Pageno is NaN
    // 401 Paginationsize is NaN
    // 402 Contact not found
    // 403 Setup not found
    [Obsolete('Pending removal use GetTickets instead', '17.0')]
    procedure GetTicketsPaginated(ContactNo: Code[20]; PortalCueLineNo: Text; PageNo: Text; PaginationSize: Text; var ICITicketPort: XmlPort "ICI Ticket Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportTicket2: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICIPortalCueMgt: Codeunit "ICI Portal Cue Mgt.";
        TicketCounter: Integer;
        PageNoInt: Integer;
        PaginationSizeInt: Integer;
        NoOfTickets: Integer;
        Offset: Integer;
        NoOfPages: Integer;

    begin
        IF NOT Evaluate(PageNoInt, PageNo) THEN EXIT(400);
        IF NOT Evaluate(PaginationSizeInt, PaginationSize) THEN EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN EXIT(402);
        if not ICISupportSetup.Get() then
            exit(403);

        SetLanguage(Contact."Language Code");

        if PortalCueLineNo <> '' THEN
            ICIPortalCueMgt.ApplyCueFilters(ICISupportTicket, ContactNo, PortalCueLineNo)
        ELSE begin
            // Apply general Filters for Tickets
            ICISupportTicket.SetFilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Preparation);
            ICISupportTicket.SetRange("Company Contact No.", Contact."Company No.");
        end;
        ICISupportTicket.SetRange(Online, true);
        if ICISupportSetup."Guest Contact Company No." <> '' then
            if Contact."Company No." = ICISupportSetup."Guest Contact Company No." then
                ICISupportTicket.SetRange("Current Contact No.", ContactNo);
        NoOfTickets := ICISupportTicket.COUNT();
        IF (NoOfTickets MOD PaginationSizeInt) <> 0 then
            Offset := 1;
        NoOfPages := (NoOfTickets div PaginationSizeInt) + Offset;
        ICITicketPort.SetNoOfPages(NoOfPages);
        ICITicketPort.SetNoOfTickets(NoOfTickets);

        TicketCounter := PaginationSizeInt;
        ICISupportTicket.FINDSET();
        IF (PageNoInt - 1) > 0 THEN
            ICISupportTicket.NEXT(PaginationSizeInt * (PageNoInt - 1)); // Jump to start of page
        repeat
            TicketCounter -= 1;
            ICISupportTicket2 := ICISupportTicket;
            ICISupportTicket2.Mark(true);
        until (ICISupportTicket.NEXT() = 0) OR (TicketCounter = 0); // Mark #PaginationSize Tickets
        ICISupportTicket2.MarkedOnly(TRUE);
        ICITicketPort.SetTableView(ICISupportTicket2);

        ICITicketPort.Authorize();
        EXIT(100);
    end;

    procedure GetTicketDetails(TicketNo: Code[20]; ContactNo: Code[20]; var ICITicketDetailsPort: XmlPort "ICI Ticket Details Port"): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
    begin

        if (ContactNo = '') or (TicketNo = '') then
            exit(400);

        IF Contact.GET(ContactNo) THEN
            SetLanguage(Contact."Language Code");

        ICISupportTicket.SetRange("No.", TicketNo);
        ICITicketDetailsPort.SetTableView(ICISupportTicket);
        ICITicketDetailsPort.Authorize();
        EXIT(100);
    end;


    // procedure GetSupportUsers(UserID: Code[20]; var ICISupportUserPort: XmlPort "ICI Support User Port")
    // var
    //     ICISupportUser: Record "ICI SUpport User";
    // begin
    //     IF UserID <> '' then
    //         ICISupportUser.SetRange("User ID", UserID);
    //     ICISupportUserPort.SetTableView(ICISupportUser);
    //     ICISupportUserPort.Export();
    // end;


    // Exit Codes:
    // 100 = OK
    // 400 = no ticket found
    // 401 = no contact found
    // 402 = company mismatch
    // 403 = File not Found
    procedure GetTicketFile(EntryNo: Integer; ContactNo: Code[20]; var ICISupportFilePort: XmlPort "ICI Support File Port"): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        Contact: Record Contact;
    begin

        if (ContactNo = '') or (EntryNo = 0) then
            EXIT(400);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);

        IF NOT ICISupportTicketLog.GET(EntryNo) THEN
            EXIT(403);

        IF NOT ICISupportTicket.GET(ICISupportTicketLog."Support Ticket No.") THEN
            EXIT(400);

        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        SetLanguage(Contact."Language Code");

        ICISupportTicketLog.SetRange("Entry No.", EntryNo);
        ICISupportFilePort.SetTableView(ICISupportTicketLog);
        ICISupportFilePort.Authorize();
        EXIT(100);
    end;

    procedure CreateTicketFromNewTicketForm(Description: Text[250]; Message: Text; ReferenceNo: Text[50]; Category1: Text[50]; Category2: Text[50]; ContactNo: Text[20]; SerialNo: Text[50]; AdditionalFieldsJsonB64: Text): code[20]
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        FirstAllocationMatrix: Record "ICI First Allocation Matrix";
        ServiceItem: Record "Service Item";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
        Base64Convert: Codeunit "Base64 Convert";
        AdditionalFieldsJsonObject: JsonObject;
        AdditionalFieldsJsonText: Text;
    begin
        ICISupportSetup.GET();
        // ICISupportSetup.TestField("License Module 6", true);

        ICISupportTicket.Init();
        ICISupportTicket.Validate("No.");
        ICISupportTicket.INSERT(TRUE);
        ICISupportTicket.Validate(Description, Description);
        ICISupportTicket.Validate("Reference No.", ReferenceNo);
        ICISupportTicket.Online := true;

        Contact.GET(ContactNo);
        ICISupportTicket.VALIDATE("Company Contact No.", Contact."Company No.");
        ICISupportTicket.Validate("Current Contact No.", Contact."No.");

        ICISupportTicket.Validate("Category 1 Code", Category1);
        if Category2 <> '' then
            ICISupportTicket.Validate("Category 2 Code", Category2);

        ICISupportTicket.Validate("Support User ID", FirstAllocationMatrix.GetFirstAllocation(ICISupportTicket, true));

        if StrLen(SerialNo) > 0 then begin // Do it only if there is actually a SerialNo Input
            ICISupportTicket."Serial No." := SerialNo; // Write serial no to ticket - even if it does not exist
            ServiceItem.SetRange("Serial No.", SerialNo);
            IF ServiceItem.FindFirst() then
                ICISupportTicket.validate("Service Item No.", ServiceItem."No.");
        end;
        ICISupportTicket.Modify(true);

        AdditionalFieldsJsonText := Base64Convert.FromBase64(AdditionalFieldsJsonB64);
        AdditionalFieldsJsonObject.ReadFrom(AdditionalFieldsJsonText);
        ICISupportTicket.OnProcessNewTicketJson(ICISupportTicket, AdditionalFieldsJsonObject);
        ICISupportTicket.Modify(true);

        ICISupportTicketLogMgt.SaveTicketContactMessage(ICISupportTicket."No.", Message, Contact."No.");
        ICISupportProcessMgt.OpenTicket(ICISupportTicket."No.", true); // Hide Confirm (Gui is not allowed)

        OnAfterCreateTicketFromNewTicketForm(ICISupportTicket, ContactNo);

        if ICISupportTicket."Company Contact No." = ICISupportSetup."Guest Contact Company No." then
            SendGuestTicketResponseToGuest(ICISupportTicket, ContactNo);

        exit(ICISupportTicket."No.");
    end;

    procedure SendGuestTicketResponseToGuest(ICISupportTicket: Record "ICI Support Ticket"; ContactNo: Code[20])
    var
        SupportSetup: Record "ICI Support Setup";
        Contact: Record Contact;
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        //ICISupportLog: Record "ICI Support Log";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        EmptyRecID: RecordId;
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        SupportSetup.Get();
        if SupportSetup."Guest Form Text Module Code" <> '' then begin
            IF NOT Contact.GET(ContactNo) THEN
                EXIT;
            IF Contact."E-Mail" = '' THEN
                EXIT;
            IF NOT ICISupportMailSetup.GET() THEN
                EXIT;
            IF NOT ICISupportMailSetup."E-Mail C Login Active" THEN
                EXIT;
            IF ICISupportMailSetup."E-Mail C Login" = '' THEN
                EXIT;
            ICISupportMailMgt.GetEMailTextModule(SupportSetup."Guest Form Text Module Code", Contact."Language Code", ICISupportTextModule);
            MailSubject := ICISupportTextModule.Description;
            ICISupportTextModule.CalcFields(Data);
            ICISupportTextModule.Data.CreateInStream(lInStream);
            lInStream.Read(MailBody);
            ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
            ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
            ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);
            ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");
            MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
            MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
            MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);
            if ICISupportTicket.Online then
                ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ICISupportTicket."No.", '', '', '', Contact.RecordId(), EmptyRecID);
        end;
    end;


    // Exit Codes:
    // 100 = ok
    // 400 = no ticket found
    // 401 = no contact found
    // 402 = company mismatch
    // 403 = setup missing
    procedure SaveTicketContactMessage(TicketNo: Code[20]; CommunicationTxt: Text; ContactNo: Code[20]): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
    begin

        if (TicketNo = '') or (ContactNo = '') then
            exit(400);

        IF NOT ICISupportSetup.GET() THEN
            EXIT(403);

        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);

        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        ICISupportTicketLogMgt.SaveTicketContactMessage(ICISupportTicket."No.", CommunicationTxt, Contact."No.");
        EXIT(100);
    end;

    procedure GetOrCreateContact(FirstName: Text[30]; Surname: Text[30]; EMail: Text[80]; PhoneNo: Text[30]; Salutation: Text[10]) ContactNo: Code[20]
    var
        Contact: Record Contact;
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ContactCount: Integer;
        MultipleContactsErr: Label 'Oops. Found more than one Contact for E-Mail Adress %1', Comment = '%1=EmailAddr|de-DE=Es wurden mehrere Kontakte zu dieser E-Mail Adresse %1 gefunden';
    begin
        ICISupportMailSetup.GET();
        ICISupportSetup.GET();

        Contact.SETRANGE("Search E-Mail", UpperCase(EMail));
        ContactCount := Contact.Count();

        // If Multiple Contact found, Create new one so GuestForm submit doesn´t throw Error
        IF ContactCount > 1 THEN begin
            CLEAR(Contact);
            Contact.INIT();
            Contact."No." := '';
            Contact.INSERT(TRUE);
            Contact.Validate(Type, Contact.Type::Person);
            Contact.Validate("Company No.", ICISupportSetup."Guest Contact Company No.");
            Contact.Validate("First Name", FirstName);
            Contact.Validate(Surname, Surname);
            Contact.Validate("E-Mail", EMail);
            Contact.Validate("Phone No.", PhoneNo);
            Contact.Validate("ICI Support Active", true);
            case Salutation of
                'M':
                    Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code M");
                'F':
                    Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code F");
                'D':
                    Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D");
                else
                    Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D"); //Fall back to Diverse
            end;

            Contact.Modify(true);
            ContactNo := Contact."No.";
            EXIT;
        end;

        IF ContactCount = 1 then begin
            Contact.FindFirst();
            ContactNo := Contact."No.";
            EXIT;
        end;

        CLEAR(Contact);
        Contact.INIT();
        Contact."No." := '';
        Contact.INSERT(TRUE);
        Contact.Validate(Type, Contact.Type::Person);
        Contact.Validate("Company No.", ICISupportSetup."Guest Contact Company No.");
        Contact.Validate("First Name", FirstName);
        Contact.Validate(Surname, Surname);
        Contact.Validate("E-Mail", EMail);
        Contact.Validate("Phone No.", PhoneNo);
        Contact.Validate("ICI Support Active", true);
        case Salutation of
            'M':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code M");
            'F':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code F");
            'D':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D");
            else
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D"); //Fall back to Diverse
        end;

        Contact.Modify(true);
        ContactNo := Contact."No.";
        EXIT;
    end;

    procedure GetTicketCategories(var ICITicketCategoryPort: XmlPort "ICI Ticket Category Port")
    begin
        ICITicketCategoryPort.Authorize();
        ICITicketCategoryPort.Export();
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Ticket not found
    // 401 = no contact found
    // 402 = company mismatch
    // 403 = Ticket already closed

    procedure CloseTicket(TicketNo: Code[20]; ContactNo: Code[20]): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
    begin

        if (TicketNo = '') or (ContactNo = '') then
            Exit(400);

        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);

        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        IF ICISupportTicket."Ticket State" = "ICI Ticket State"::Closed then
            EXIT(403);

        ICISupportProcessMgt.ContactClosedTicket(TicketNo, ContactNo);
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Ticket not found
    // 401 = no contact found
    // 402 = company mismatch
    // 403 = Ticket is not closed -> no reopen

    procedure ReopenTicket(TicketNo: Code[20]; ContactNo: Code[20]; ReopenText: Text): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
    begin
        if (TicketNo = '') or (ContactNo = '') then
            Exit(400);

        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);

        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        IF ICISupportTicket."Ticket State" <> "ICI Ticket State"::Closed then
            EXIT(403);

        ICISupportProcessMgt.ContactReopenedTicket(TicketNo, ContactNo, ReopenText);
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Ticket not found
    // 401 = no contact found
    // 402 = company mismatch
    procedure InsertFile(TicketNo: Code[20]; ContactNo: Code[20]; FileName: Text[250]; B64: Text; FileSize: Integer; FileDescription: Text[250]): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        lOutStream: OutStream;
    begin
        if (TicketNo = '') or (ContactNo = '') then
            Exit(400);
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);
        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        ICISupportTicket.SetModifiedByContact(ContactNo);
        ICISupportTicket.Modify();

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External File");
        ICISupportTicketLog.VALIDATE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketLog.VALIDATE("Record ID", ICISupportTicket.RecordId());
        ICISupportTicketLog.VALIDATE("Data Text", filename);
        ICISupportTicketLog.VALIDATE("Data Text 2", FileDescription); // "Renamed File"
        ICISupportTicketLog.VALIDATE("Additional Text", FORMAT(fileSize));

        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::Contact);
        ICISupportTicketLog.VALIDATE("Created By", ContactNo);
        ICISupportTicketLog.Calcfields(Data);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream, TextEncoding::Windows);
        lOutStream.WriteText(B64);
        ICISupportTicketLog.Modify(TRUE);
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Ticket not found
    // 401 = no contact found
    // 402 = company mismatch
    procedure GetTicketForwardContacts(TicketNo: Code[20]; ContactNo: Code[20]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
    begin
        if (TicketNo = '') or (ContactNo = '') then
            Exit(400);
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);
        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);

        Contact.SetRange("Company No.", ICISupportTicket."Company Contact No.");
        Contact.SetRange(Type, Contact.Type::Person);
        ICIContactPort.SetTableView(Contact);
        ICIContactPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Ticket not found
    // 401 = no contact found
    // 402 = company mismatch
    // 403 = new Contact not found
    // 404 = new contact - company mismatch
    // 405 = Contact not changed
    procedure ForwardTicketToContact(TicketNo: Code[20]; ContactNo: Code[20]; NewContactNo: Code[20]): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        NewContact: Record Contact;
    begin
        if (TicketNo = '') or (ContactNo = '') then
            Exit(400);

        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT(400);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);
        IF Contact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(402);
        IF NOT NewContact.GET(NewContactNo) THEN
            EXIT(403);
        IF NewContact."Company No." <> ICISupportTicket."Company Contact No." then
            EXIT(404);
        IF ICISupportTicket."Current Contact No." = NewContactNo THEN
            EXIT(405);

        ICISupportSetup.GET();
        Contact.GET(ICISupportTicket."Current Contact No.");

        SetLanguage(Contact."Language Code");

        ICISupportTicket.SetModifiedByContact(Contact."No.");
        ICISupportTicket.Validate("Current Contact No.", NewContactNo);
        ICISupportTicket.Modify();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - No Cue Set defined
    // 401 = no contact found
    // 402 - no cues in cueset
    procedure GetPortalCues(ContactNo: Code[20]; var ICIPortalCuePort: XmlPort "ICI Portal Cue Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record Contact;
        CompanyContact: Record Contact;
        ICIPortalCueSet: Record "ICI Portal Cue Set";
        ICIPortalCue: Record "ICI Portal Cue";
        CueSetCode: Code[20];
    begin
        if (ContactNo = '') then
            Exit(401);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);

        ICISupportSetup.GET();
        CueSetCode := ICISupportSetup."Default Portal Cue Set Code"; // Std Kachelset

        IF CompanyContact.GET(Contact."Company No.") THEN
            IF CompanyContact."ICI Portal Cue Set Code" <> '' then
                CueSetCode := CompanyContact."ICI Portal Cue Set Code"; // Kachelset des Unternehmens

        IF Contact."ICI Portal Cue Set Code" <> '' then
            CueSetCode := Contact."ICI Portal Cue Set Code"; // Kachelset der Person

        IF CueSetCode = '' THEN
            EXIT(400);

        ICIPortalCueSet.GET(CueSetCode);
        ICIPortalCueSet.CalcFields("No. of Portal Cues");
        IF ICIPortalCueSet."No. of Portal Cues" = 0 THEN
            EXIT(402);

        ICIPortalCue.SetRange(Inactive, false);
        ICIPortalCue.SetRange("Portal Cue Set Code", CueSetCode);

        ICIPortalCuePort.SetContactNo(Contact."Company No.", Contact."No.");
        ICIPortalCuePort.SetTableView(ICIPortalCue);
        ICIPortalCuePort.Authorize();
        Exit(100);
    end;

    // Exit codes:
    // 100 = Service Item Found
    // 400 = Serial No not Found
    procedure GetServiceItem(SerialNo: Code[50]; var ICIServiceItemPort: XmlPort "ICI Service Item Port"): Integer
    var
        ServiceItem: Record "Service Item";
    begin
        ServiceItem.SetRange("Serial No.", SerialNo);
        ICIServiceItemPort.SetTableView(ServiceItem);
        IF ServiceItem.IsEmpty() then
            Exit(400);
        ICIServiceItemPort.Authorize();
        EXIT(100);
    end;

    // Exit codes:
    // 100 = OK
    procedure GetServiceItemsForCustomer(CustomerNo: Code[20]; ContactNo: Code[20]; var ICIServiceItemPort: XmlPort "ICI Service Item Port"): Integer
    var
        ServiceItem: Record "Service Item";
        IsHandled: Boolean;
    begin
        OnBeforeServiceItemFilter(IsHandled, CustomerNo, ContactNo, ICIServiceItemPort);
        if not IsHandled then begin
            ServiceItem.SetRange("Customer No.", CustomerNo);
            ICIServiceItemPort.SetTableView(ServiceItem);
            ICIServiceItemPort.Authorize();
        end;
        EXIT(100);
    end;

    // Exit codes:
    // 100 = Service Item Found
    // 400 = Not Found
    procedure GetServiceItemByNo(ServiceItemNo: Code[20]; var ICIServiceItemPort: XmlPort "ICI Service Item Port"): Integer
    var
        ServiceItem: Record "Service Item";
    begin
        if (ServiceItemNo = '') then
            exit(400);

        ServiceItem.SetRange("No.", ServiceItemNo);
        ICIServiceItemPort.SetTableView(ServiceItem);
        IF ServiceItem.IsEmpty() then
            Exit(400);
        ICIServiceItemPort.Authorize();
        EXIT(100);
    end;

    // Exit codes:
    // 100 = OK
    // 401 = No Company No
    // 402 = No Users found
    procedure GetTicketUserDashboard(CompanyNo: Code[20]; var ICISupportUserPort: XmlPort "ICI Support User Port"): Integer
    var
        ICISupportUser: Record "ICI Support User";
    begin
        IF CompanyNo = '' then
            EXIT(401);

        ICISupportUserPort.SetCompanyNoFilter(CompanyNo);
        ICISupportUserPort.SetTableView(ICISupportUser);

        IF ICISupportUser.COUNT() = 0 then
            EXIT(402);

        ICISupportUserPort.Authorize();
        EXIT(100);
    end;


    // Exit codes:
    // 100 = OK
    // 400 = Login not distinguishable
    // 401 = Login could not be sent
    procedure PasswordReminder(Login: Text[100]): Integer
    var
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
    begin
        if (Login = '') then
            exit(400);

        Contact.SetCurrentKey("ICI Login", "ICI Support Active");
        Contact.setfilter("ICI Login", '%1', Login);
        Contact.SetRange("ICI Support Active", true);

        ICISupportUser.setfilter("Login", '%1', Login);

        IF ((Contact.Count() + ICISupportUser.Count()) <> 1) then
            EXIT(400);

        IF Contact.Count() = 1 then begin
            Contact.FindFirst();
            IF ICISupportMailMgt.SendContactLogin(Contact."No.", Contact.ICIGeneratePassword()) THEN
                EXIT(100);
        end;

        IF ICISupportUser.Count() = 1 THEN begin
            ICISupportUser.FindFirst();
            IF ICISupportMailMgt.SendUserLogin(ICISupportUser."User ID", ICISupportUser.GeneratePassword()) THEN
                EXIT(100);
        end;

        EXIT(401);
    end;

    // Exit codes:
    // 100 = OK
    // 400 = Contact not found
    // 401 = Old Password Wrong
    procedure ChangeContactPassword(ContactNo: Code[20]; OldPassword: Text[50]; NewPassword: Text[50]): Integer
    var
        Contact: Record Contact;
    begin
        if (ContactNo = '') then
            exit(400);

        IF NOT Contact.GET(ContactNo) then
            Exit(400);
        IF Contact."ICI Support Password Hash" <> UpperCase(OldPassword) then
            Exit(401);
        Contact.Validate("ICI Support Password Hash", UpperCase(NewPassword));
        Contact.Modify(true);
        Exit(100);
    end;

    // Exit codes:
    // 100 = OK
    // 400 = JSON not Valid
    // 401 = Contact not Found
    // 402 = Contact not Support Active
    // 403 = EMail empty
    procedure SaveContact(ContactJSON: Text): Integer
    var
        Contact: Record Contact;
        ICISupportSetup: Record "ICI Support Setup";
        ContactData: JsonObject;
        ContactNo: Code[20];
        PhoneNo: Text[30];
        MobilePhoneNo: Text[30];
        FaxNo: Text[30];
        EMail: Text[80];
        HomePage: Text[80];
    begin
        ICISupportSetup.GET();
        ICISupportSetup.TestField("Portal - Change Data Allowed");

        IF NOT ContactData.ReadFrom(ContactJSON) THEN
            EXIT(400);

        ContactNo := COPYSTR(GetJsonToken(ContactData, 'contact_no').AsValue().AsText(), 1, 20);
        if (ContactNo = '') then
            exit(402);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);
        IF NOT Contact."ICI Support Active" THEN
            EXIT(402);

        PhoneNo := COPYSTR(GetJsonToken(ContactData, 'phone_no').AsValue().AsText(), 1, MaxStrLen(PhoneNo));
        MobilePhoneNo := COPYSTR(GetJsonToken(ContactData, 'mobile_phone_no').AsValue().AsText(), 1, MaxStrLen(MobilePhoneNo));
        FaxNo := COPYSTR(GetJsonToken(ContactData, 'fax_no').AsValue().AsText(), 1, MaxStrLen(FaxNo));
        EMail := COPYSTR(GetJsonToken(ContactData, 'email').AsValue().AsText(), 1, MaxStrLen(EMail));
        HomePage := COPYSTR(GetJsonToken(ContactData, 'homepage').AsValue().AsText(), 1, MaxStrLen(HomePage));

        IF EMail = '' THEN
            EXIT(403);

        Contact.Validate("Phone No.", PhoneNo);
        Contact.Validate("Mobile Phone No.", MobilePhoneNo);
        Contact.Validate("Fax No.", FaxNo);
        Contact.Validate("E-Mail", EMail);
        Contact.Validate("Home Page", HomePage);

        Contact.MODIFY(true);

        EXIT(100);

    end;


    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    procedure GetDashboardContacts(ContactNo: Code[20]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        Contact: Record Contact;
        Contact2: Record Contact;
    begin
        if (ContactNo = '') then
            exit(400);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);

        Contact2.SetCurrentKey("Company no.");
        Contact2.SetRange("Company No.", Contact."Company No.");
        Contact2.SetRange(Type, Contact.Type::Person);

        ICIContactPort.SetTableView(Contact2);
        ICIContactPort.Authorize();
        EXIT(100);
    end;


    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    procedure GetContactsByCompanyNo(CompanyContactNo: Code[20]; var ICIContactPort: XmlPort "ICI Contact Port"): Integer
    var
        Contact: Record Contact;
    begin
        if (CompanyContactNo = '') then
            exit(400);

        IF NOT Contact.GET(CompanyContactNo) THEN
            EXIT(400);

        Contact.SetCurrentKey("Company no.");
        Contact.SetRange("Company No.", CompanyContactNo);
        Contact.SetRange(Type, Contact.Type::Person);

        ICIContactPort.SetTableView(Contact);
        ICIContactPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesQuotes(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();

        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Quote") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Quote" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Quote);
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesOrders(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Order") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Order" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Order);
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesInvoices(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin
        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Invoice") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Invoice" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::Invoice);
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesCrMemos(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Credit Memo") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Credit Memo" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Credit Memo");
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.Authorize();
        ICISalesHeaderPort.SetTableView(SalesHeader);
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesBlanketOrders(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Blanket Order") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Blanket Order" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Blanket Order");
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetSalesReturnOrders(ContactNo: Code[20]; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();

        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Sales Return Order") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Sales Return Order" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesHeader.SetCurrentKey("Document Type", "Sell-to Customer No.");
        SalesHeader.SetRange("Document Type", SalesHeader."Document Type"::"Return Order");
        SalesHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedSalesShipments(ContactNo: Code[20]; var ICISalesShipmentHeaderPort: XmlPort "ICI Sales Shipment Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesShipmentHeader: Record "Sales Shipment Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Posted Sales Shipment") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Sales Shipment" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesShipmentHeader.SetCurrentKey("Sell-to Customer No.");
        SalesShipmentHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesShipmentHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesShipmentHeaderPort.SetTableView(SalesShipmentHeader);
        ICISalesShipmentHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedSalesInvoices(ContactNo: Code[20]; var ICISalesInvoiceHeaderPort: XmlPort "ICI Sales Invoice Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Posted Sales Invoice") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Sales Invoice" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);


        SalesInvoiceHeader.SetCurrentKey("Sell-to Customer No.");
        SalesInvoiceHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesInvoiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesInvoiceHeaderPort.SetTableView(SalesInvoiceHeader);
        ICISalesInvoiceHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedSalesCrMemos(ContactNo: Code[20]; var ICISalesCrMemoHeaderPort: XmlPort "ICI Sales Cr.Memo Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Posted Sales Cr.Memo") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Sales Cr.Memo" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        SalesCrMemoHeader.SetCurrentKey("Sell-to Customer No.");
        SalesCrMemoHeader.SetRange("Sell-to Customer No.", CustomerNo);
        SalesCrMemoHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICISalesCrMemoHeaderPort.SetTableView(SalesCrMemoHeader);
        ICISalesCrMemoHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceQuotes(ContactNo: Code[20]; var ICIServiceHeaderPort: XmlPort "ICI Service Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Quote") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Quote" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceHeader.SetCurrentKey("Document Type", "Customer No.");
        ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::Quote);
        ServiceHeader.SetRange("Customer No.", CustomerNo);
        ServiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceHeaderPort.SetTableView(ServiceHeader);
        ICIServiceHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceOrders(ContactNo: Code[20]; var ICIServiceHeaderPort: XmlPort "ICI Service Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Order") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Order" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceHeader.SetCurrentKey("Document Type", "Customer No.");
        ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::Order);
        ServiceHeader.SetRange("Customer No.", CustomerNo);
        ServiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceHeaderPort.SetTableView(ServiceHeader);
        ICIServiceHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceInvoices(ContactNo: Code[20]; var ICIServiceHeaderPort: XmlPort "ICI Service Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Invoice") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Invoice" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::Invoice);
        ServiceHeader.SetRange("Customer No.", CustomerNo);
        ServiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceHeaderPort.SetTableView(ServiceHeader);
        ICIServiceHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceCrMemos(ContactNo: Code[20]; var ICIServiceHeaderPort: XmlPort "ICI Service Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Credit Memo") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Credit Memo" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceHeader.SetCurrentKey("Document Type", "Customer No.");
        ServiceHeader.SetRange("Document Type", ServiceHeader."Document Type"::"Credit Memo");
        ServiceHeader.SetRange("Customer No.", CustomerNo);
        ServiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceHeaderPort.Authorize();
        ICIServiceHeaderPort.SetTableView(ServiceHeader);
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedServiceShipments(ContactNo: Code[20]; var ICIServiceShipHeaderPort: XmlPort "ICI Service Ship. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceShipmentHeader: Record "Service Shipment Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Posted Service Shipment") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Service Shipment" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceShipmentHeader.SetCurrentKey("Customer No.");
        ServiceShipmentHeader.SetRange("Customer No.", CustomerNo);
        ServiceShipmentHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceShipHeaderPort.SetTableView(ServiceShipmentHeader);
        ICIServiceShipHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedServiceInvoices(ContactNo: Code[20]; var ICIServiceInvHeaderPort: XmlPort "ICI Service Inv. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Posted Service Invoice") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Service Invoice" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceInvoiceHeader.SetCurrentKey("Customer No.");
        ServiceInvoiceHeader.SetRange("Customer No.", CustomerNo);
        ServiceInvoiceHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceInvHeaderPort.SetTableView(ServiceInvoiceHeader);
        ICIServiceInvHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetPostedServiceCrMemos(ContactNo: Code[20]; var ICIServiceCrMHeaderPort: XmlPort "ICI Service Cr.M. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Posted Service Cr.Memo") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Posted Service Cr.Memo" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceCrMemoHeader.SetCurrentKey("Customer No.");
        ServiceCrMemoHeader.SetRange("Customer No.", CustomerNo);
        ServiceCrMemoHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceCrMHeaderPort.SetTableView(ServiceCrMemoHeader);
        ICIServiceCrMHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceContracts(ContactNo: Code[20]; var ICIServiceContrHeaderPort: XmlPort "ICI Service Contr. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceContractHeader: Record "Service Contract Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webarchiv Service Contract") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Contract" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceContractHeader.SetCurrentKey("Contract Type", "Customer No.");
        ServiceContractHeader.SetRange("Contract Type", ServiceContractHeader."Contract Type"::Contract);
        ServiceContractHeader.SetRange("Customer No.", CustomerNo);
        ServiceContractHeader.SetRange("ICI Hidden from Webarchive", FALSE);

        ICIServiceContrHeaderPort.SetTableView(ServiceContractHeader);
        ICIServiceContrHeaderPort.Authorize();
        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 402 - Contact has no Permission to see Document
    // 403 - No CustomerNo for Contact
    procedure GetServiceContractQuotes(ContactNo: Code[20]; var ICIServiceContrHeaderPort: XmlPort "ICI Service Contr. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
        ServiceContractHeader: Record "Service Contract Header";
        Contact: Record Contact;
        CustomerNo: Code[20];
    begin


        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICIWebarchiveSetup.GET();
        IF NOT (ICISupportSetup."Document Integration" AND ICIWebarchiveSetup."Webar. Service Contract Quote") THEN // Must both be true
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        IF NOT Contact."ICI Service Contract Quote" THEN
            EXIT(402);

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        ServiceContractHeader.SetCurrentKey("Contract Type", "Customer No.");
        ServiceContractHeader.SetRange("Contract Type", ServiceContractHeader."Contract Type"::Quote);
        ServiceContractHeader.SetRange("Customer No.", CustomerNo);
        ServiceContractHeader.SetRange("ICI Hidden from Webarchive", FALSE);
        ICIServiceContrHeaderPort.Authorize();
        ICIServiceContrHeaderPort.SetTableView(ServiceContractHeader);

        EXIT(100);
    end;


    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetSalesHeader(ContactNo: Code[20]; RecordIDText: Text; var ICISalesHeaderPort: XmlPort "ICI Sales Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);

        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);

        IF lRecordID.TableNo <> DATABASE::"Sales Header" THEN
            EXIT(405);

        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(SalesHeader);

        IF SalesHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        case SalesHeader."Document Type" OF
            SalesHeader."Document Type"::Quote:
                IF NOT Contact."ICI Sales Quote" THEN
                    exit(401);
            "Sales Document Type"::Order:
                IF NOT Contact."ICI Sales Order" THEN
                    exit(401);
            "Sales Document Type"::Invoice:
                IF NOT Contact."ICI Sales Invoice" THEN
                    exit(401);
            "Sales Document Type"::"Credit Memo":
                IF NOT Contact."ICI Sales Credit Memo" THEN
                    exit(401);
            "Sales Document Type"::"Blanket Order":
                IF NOT Contact."ICI Sales Blanket Order" THEN
                    exit(401);
            "Sales Document Type"::"Return Order":
                IF NOT Contact."ICI Sales Return Order" THEN
                    exit(401);
        end;

        SalesHeader.SetRecFilter();
        ICISalesHeaderPort.SetTableView(SalesHeader);
        ICISalesHeaderPort.ShowDocumentLines();
        ICISalesHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetSalesShipmentHeader(ContactNo: Code[20]; RecordIDText: Text; var ICISalesShipmentHeaderPort: XmlPort "ICI Sales Shipment Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesShipmentHeader: Record "Sales Shipment Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin
        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Shipment Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(SalesShipmentHeader);

        IF SalesShipmentHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesShipmentHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Sales Shipment" THEN
            exit(401);

        SalesShipmentHeader.SetRecFilter();
        ICISalesShipmentHeaderPort.SetTableView(SalesShipmentHeader);
        ICISalesShipmentHeaderPort.ShowDocumentLines();
        ICISalesShipmentHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetSalesInvoiceHeader(ContactNo: Code[20]; RecordIDText: Text; var ICISalesInvoiceHeaderPort: XmlPort "ICI Sales Invoice Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Invoice Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(SalesInvoiceHeader);

        IF SalesInvoiceHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesInvoiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Sales Invoice" THEN
            exit(401);

        SalesInvoiceHeader.SetRecFilter();
        ICISalesInvoiceHeaderPort.SetTableView(SalesInvoiceHeader);
        ICISalesInvoiceHeaderPort.ShowDocumentLines();
        ICISalesInvoiceHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetSalesCrMemoHeader(ContactNo: Code[20]; RecordIDText: Text; var ICISalesCrMemoHeaderPort: XmlPort "ICI Sales Cr.Memo Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Cr.Memo Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(SalesCrMemoHeader);

        IF SalesCrMemoHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesCrMemoHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Sales Cr.Memo" THEN
            exit(401);

        SalesCrMemoHeader.SetRecFilter();
        ICISalesCrMemoHeaderPort.SetTableView(SalesCrMemoHeader);
        ICISalesCrMemoHeaderPort.ShowDocumentLines();
        ICISalesCrMemoHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetServiceHeader(ContactNo: Code[20]; RecordIDText: Text; var ICIServiceHeaderPort: XmlPort "ICI Service Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ServiceHeader);

        IF ServiceHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        case ServiceHeader."Document Type" OF
            ServiceHeader."Document Type"::Quote:
                IF NOT Contact."ICI Service Quote" THEN
                    exit(401);
            ServiceHeader."Document Type"::Order:
                IF NOT Contact."ICI Service Order" THEN
                    exit(401);
            ServiceHeader."Document Type"::Invoice:
                IF NOT Contact."ICI Service Invoice" THEN
                    exit(401);
            ServiceHeader."Document Type"::"Credit Memo":
                IF NOT Contact."ICI Service Credit Memo" THEN
                    exit(401);
        end;

        ServiceHeader.SetRecFilter();
        ICIServiceHeaderPort.SetTableView(ServiceHeader);
        ICIServiceHeaderPort.ShowDocumentLines();
        ICIServiceHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetServiceShipmentHeader(ContactNo: Code[20]; RecordIDText: Text; var ICIServiceShipHeaderPort: XmlPort "ICI Service Ship. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceShipmentHeader: Record "Service Shipment Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin


        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Shipment Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ServiceShipmentHeader);

        IF ServiceShipmentHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceShipmentHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Service Shipment" THEN
            exit(401);

        ServiceShipmentHeader.SetRecFilter();
        ICIServiceShipHeaderPort.SetTableView(ServiceShipmentHeader);
        ICIServiceShipHeaderPort.ShowDocumentLines();
        ICIServiceShipHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetServiceInvoiceHeader(ContactNo: Code[20]; RecordIDText: Text; var ICIServiceInvHeaderPort: XmlPort "ICI Service Inv. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Invoice Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ServiceInvoiceHeader);

        IF ServiceInvoiceHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceInvoiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Service Invoice" THEN
            exit(401);

        ServiceInvoiceHeader.SetRecFilter();
        ICIServiceInvHeaderPort.SetTableView(ServiceInvoiceHeader);
        ICIServiceInvHeaderPort.ShowDocumentLines();
        ICIServiceInvHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetServiceCrMemoHeader(ContactNo: Code[20]; RecordIDText: Text; var ICIServiceCrMHeaderPort: XmlPort "ICI Service Cr.M. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Cr.Memo Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ServiceCrMemoHeader);

        IF ServiceCrMemoHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceCrMemoHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Posted Service Cr.Memo" THEN
            exit(401);

        ServiceCrMemoHeader.SetRecFilter();
        ICIServiceCrMHeaderPort.SetTableView(ServiceCrMemoHeader);
        ICIServiceCrMHeaderPort.ShowDocumentLines();
        ICIServiceCrMHeaderPort.Authorize();

        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Hidden
    procedure GetServiceContractHeader(ContactNo: Code[20]; RecordIDText: Text; var ICIServiceContrHeaderPort: XmlPort "ICI Service Contr. Header Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceContractHeader: Record "Service Contract Header";
        Contact: Record Contact;
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Document Integration" THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Contract Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ServiceContractHeader);

        IF ServiceContractHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceContractHeader."ICI Hidden from Webarchive" THEN
            EXIT(409);

        IF NOT Contact."ICI Service Contract" THEN
            exit(401);

        ServiceContractHeader.SetRecFilter();
        ICIServiceContrHeaderPort.Authorize();
        ICIServiceContrHeaderPort.SetTableView(ServiceContractHeader);
        ICIServiceContrHeaderPort.ShowDocumentLines();


        EXIT(100);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Reportselection missing
    // 410 - error in saveaspdf
    // 411 - Hidden
    procedure RequestSalesHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesHeader: Record "Sales Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin

        if (ContactNo = '') then
            exit(400);

        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);

        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);

        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);

        IF lRecordID.TableNo <> DATABASE::"Sales Header" THEN
            EXIT(405);

        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesHeader);

        IF SalesHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case SalesHeader."Document Type" OF
            SalesHeader."Document Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Quote");
            "Sales Document Type"::Order:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Order");
            "Sales Document Type"::Invoice:
                ERROR('Invoices can not be printed');
            "Sales Document Type"::"Credit Memo":
                ERROR('Credit Memos can not be printed');
            "Sales Document Type"::"Blanket Order":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Blanket");
            "Sales Document Type"::"Return Order":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Return");
        End;
        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        case SalesHeader."Document Type" of
            "Sales Document Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Quote", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::Order:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Order", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::Invoice:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Invoice", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Credit Memo":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Cr.Memo", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Blanket Order":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Blanket Order", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Return Order":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Return Order", Contact."Language Code", ICISupportTextModule);
        end;
        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, SalesHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestSalesShipmentHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesShipmentHeader: Record "Sales Shipment Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Shipment Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesShipmentHeader);
        IF SalesShipmentHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesShipmentHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Shipment");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Shipment", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, SalesShipmentHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestSalesInvoiceHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Invoice Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesInvoiceHeader);
        IF SalesInvoiceHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesInvoiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Invoice");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Invoice", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, SalesInvoiceHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestSalesCrMemoHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Sales Cr.Memo Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesCrMemoHeader);
        IF SalesCrMemoHeader."Sell-to Customer No." <> CustomerNo THEN
            EXIT(408);
        IF SalesCrMemoHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Cr.Memo");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Cr.Memo", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, SalesCrMemoHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    // Exit Codes:
    // 100 - OK
    // 400 - Contact not found
    // 401 - Document Type Deactivated
    // 403 - No CustomerNo for Contact
    // 404 - RecordID maleformed
    // 405 - RecordID wrong Table
    // 406 - Record does not Exist
    // 408 - Wrong Customer
    // 409 - Reportselection missing
    // 410 - error in saveaspdf
    // 411 - Hidden
    procedure RequestServiceHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceHeader: Record "Service Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceHeader);
        IF ServiceHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case ServiceHeader."Document Type" OF
            ServiceHeader."Document Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Quote");
            ServiceHeader."Document Type"::Order:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Order");
            ServiceHeader."Document Type"::Invoice:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");
            ServiceHeader."Document Type"::"Credit Memo":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");
        End;
        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        case ServiceHeader."Document Type" of
            ServiceHeader."Document Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Quote", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::Order:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Order", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::Invoice:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Invoice", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::"Credit Memo":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Cr.Memo", Contact."Language Code", ICISupportTextModule);
        end;
        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ServiceHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestServiceShipmentHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceShipmentHeader: Record "Service Shipment Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Shipment Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceShipmentHeader);
        IF ServiceShipmentHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceShipmentHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Shipment");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Shipment", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ServiceShipmentHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestServiceInvoiceHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Invoice Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceInvoiceHeader);
        IF ServiceInvoiceHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceInvoiceHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Invoice", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ServiceInvoiceHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestServiceCrMemoHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Cr.Memo Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceCrMemoHeader);
        IF ServiceCrMemoHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceCrMemoHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Cr.Memo", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ServiceCrMemoHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure RequestServiceContractHeaderPDF(ContactNo: Code[20]; RecordIDText: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        ServiceContractHeader: Record "Service Contract Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailSubject: Text;
        MailBody: Text;
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Contract Header" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceContractHeader);
        IF ServiceContractHeader."Customer No." <> CustomerNo THEN
            EXIT(408);
        IF ServiceContractHeader."ICI Hidden from Webarchive" THEN
            EXIT(411);

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case ServiceContractHeader."Contract Type" OF
            ServiceContractHeader."Contract Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract Quote");
            ServiceContractHeader."Contract Type"::Contract:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract");
        End;
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract");

        IF NOT ReportSelections.FindFirst() THEN
            EXIT(409);

        // Get Mail Subject and Body
        Case ServiceContractHeader."Contract Type" OF
            ServiceContractHeader."Contract Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Contract Quote", Contact."Language Code", ICISupportTextModule);
            ServiceContractHeader."Contract Type"::Contract:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Contract", Contact."Language Code", ICISupportTextModule);
        End;

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            // Send Mail with PDF Attachment
            ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, ServiceContractHeader."ICI Support Ticket No.", ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID), 'application/pdf', txtB64, Contact.RecordId(), EmptyRecordId);
            EXIT(100);
        end;

        EXIT(410);
    end;

    // Exit Codes:
    // 100 = OK
    procedure RequestDataUpdate(Scope: Text): Integer
    var
        ICIPortalMgt: Codeunit "ICI Portal Mgt.";
        ICIPortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
    begin
        case Scope of
            'category':
                begin
                    ICIPortalMgt.UpdateAllCategories(true, ICIPortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCategories(false, ICIPortalUpdateMgt);
                end;
            'cue':
                begin
                    ICIPortalMgt.UpdateAllCues(true, ICIPortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCues(false, ICIPortalUpdateMgt);
                end;
            else
                ICIPortalMgt.UpdateAllTables();
        end;
        ICIPortalUpdateMgt.SendUpdateTableBuffered();  // Flush remaining data
        Exit(100);
    end;

    procedure DownloadWebarchivePDF(ContactNo: Code[20]; RecordIDText: Text; var ICIDownloadPort: XmlPort "ICI Download Port"): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        SalesHeader: Record "Sales Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        ServiceHeader: Record "Service Header";
        ServiceShipmentHeader: Record "Service Shipment Header";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        ServiceCrMemoHeader: Record "Service Cr.Memo Header";
        ServiceContractHeader: Record "Service Contract Header";
        Contact: Record Contact;
        ReportSelections: Record "Report Selections";
        lInteger: Record Integer;
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        CustomerNo: Code[20];
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        FileName: Text;
        DataUriLbl: Label 'data:application/pdf;base64,%1', Comment = '%1=Base64 Content';
    begin
        ICISupportSetup.GET();
        IF NOT (ICISupportSetup."Document Integration") THEN
            EXIT(401);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        SetLanguage(Contact."Language Code");

        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(403);
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);

        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);
        lRecordRef.SetRecFilter();

        // Filename
        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(lRecordID);

        case lRecordID.TableNo of
            DATABASE::"Sales Header":
                begin
                    lRecordRef.SetTable(SalesHeader);
                    IF SalesHeader."Sell-to Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF SalesHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    Case SalesHeader."Document Type" OF
                        SalesHeader."Document Type"::Quote:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Quote");
                        "Sales Document Type"::Order:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Order");
                        "Sales Document Type"::Invoice:
                            ERROR('Invoices can not be printed');
                        "Sales Document Type"::"Credit Memo":
                            ERROR('Credit Memos can not be printed');
                        "Sales Document Type"::"Blanket Order":
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Blanket");
                        "Sales Document Type"::"Return Order":
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Return");
                    End;
                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);

                end;
            DATABASE::"Sales Shipment Header":
                begin
                    lRecordRef.SetTable(SalesShipmentHeader);
                    IF SalesShipmentHeader."Sell-to Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF SalesShipmentHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Shipment");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);

                end;
            DATABASE::"Sales Invoice Header":
                begin
                    lRecordRef.SetTable(SalesInvoiceHeader);
                    IF SalesInvoiceHeader."Sell-to Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF SalesInvoiceHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Invoice");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;
            DATABASE::"Sales Cr.Memo Header":
                begin
                    lRecordRef.SetTable(SalesCrMemoHeader);
                    IF SalesCrMemoHeader."Sell-to Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF SalesCrMemoHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Cr.Memo");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;
            DATABASE::"Service Header":
                begin
                    lRecordRef.SetTable(ServiceHeader);
                    IF ServiceHeader."Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF ServiceHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    Case ServiceHeader."Document Type" OF
                        ServiceHeader."Document Type"::Quote:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Quote");
                        ServiceHeader."Document Type"::Order:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Order");
                        ServiceHeader."Document Type"::Invoice:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");
                        ServiceHeader."Document Type"::"Credit Memo":
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");
                    End;
                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;
            DATABASE::"Service Shipment Header":
                begin
                    lRecordRef.SetTable(ServiceShipmentHeader);
                    IF ServiceShipmentHeader."Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF ServiceShipmentHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Shipment");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);

                end;
            DATABASE::"Service Invoice Header":
                begin
                    lRecordRef.SetTable(ServiceInvoiceHeader);
                    IF ServiceInvoiceHeader."Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF ServiceInvoiceHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;
            DATABASE::"Service Cr.Memo Header":
                begin
                    lRecordRef.SetTable(ServiceCrMemoHeader);
                    IF ServiceCrMemoHeader."Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF ServiceCrMemoHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;
            DATABASE::"Service Contract Header":
                begin
                    lRecordRef.SetTable(ServiceContractHeader);
                    IF ServiceContractHeader."Customer No." <> CustomerNo THEN
                        EXIT(408);
                    IF ServiceContractHeader."ICI Hidden from Webarchive" THEN
                        EXIT(411);

                    // Get Report ID to Print
                    ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
                    Case ServiceContractHeader."Contract Type" OF
                        ServiceContractHeader."Contract Type"::Quote:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract Quote");
                        ServiceContractHeader."Contract Type"::Contract:
                            ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract");
                    End;
                    ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract");

                    IF NOT ReportSelections.FindFirst() THEN
                        EXIT(409);
                end;

        end;
        lRecordRef.SetRecFilter();
        // Print Report to PDF
        TempBlob.CreateOutStream(lOutStream);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);
            txtB64 := Base64Convert.ToBase64(lInStream, true);

            lInteger.SetRange(Number, 0, 0);
            ICIDownloadPort.SetTableView(lInteger); // Webservice Calls must use SetTableView or Triggers wont work. For Magic ~~~ 

            //ICIWebarchivDownloadPort.SetDownloadFile(COPYSTR(Filename, 1, 250), txtB64);

            ICIDownloadPort.SetDownloadFile(COPYSTR(Filename, 1, 250), STRSUBSTNO(DataUriLbl, txtB64));// data:[<mediatype>][;base64],<data>

            ICIDownloadPort.Authorize();
            // Send Mail with PDF Attachment
            // ICISupportMailMgt.SendMail(Contact."E-Mail", MailSubject, MailBody, SalesShipmentHeader."ICI Support Ticket No.", StrSubstNo(PDFNameLbl, 'Lieferschein', SalesShipmentHeader."No."), 'application/pdf', txtB64);
            EXIT(100);
        end;

        EXIT(410);
    end;

    procedure DownloadServiceCenterFile(ContactNo: Code[20]; RecordIDText: Text; var ICIDownloadPort: XmlPort "ICI Download Port"): Integer
    var
        ServiceItem: Record "Service Item";
        Item: Record "Item";
        Contact: Record Contact;
        Customer: Record Customer;
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        lInteger: Record Integer;
        Base64Convert: Codeunit "Base64 Convert";
        lRecordRef: RecordRef;
        lRecordID: RecordId;
        lInstream: InStream;
        CustomerNo: Code[20];
        txtB64: Text;
    begin

        lInteger.SetRange(Number, 0, 0);
        ICIDownloadPort.SetTableView(lInteger); // Webservice Calls must use SetTableView or Triggers wont work. For Magic ~~~ 

        IF NOT Contact.GET(ContactNo) THEN
            EXIT(400);
        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(421);
        IF NOT Customer.GET(CustomerNo) THEN
            EXIT(421);

        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(401);

        IF lRecordID.TableNo <> DATABASE::"ICI Sup. Dragbox File" THEN
            EXIT(402);

        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ICISupDragboxFile);
        ICISupDragboxFile.GET(ICISupDragboxFile."Entry No.");

        case ICISupDragboxFile."Record ID".TableNo of
            DATABASE::"Service Item":
                begin
                    IF NOT lRecordRef.GET(ICISupDragboxFile."Record ID") THEN
                        EXIT(420);
                    lRecordRef.SetTable(ServiceItem);
                    ServiceItem.GET(ServiceItem."No.");

                    IF ServiceItem."Customer No." <> Customer."No." then
                        Exit(422);


                end;
            DATABASE::"Item":
                begin
                    IF NOT lRecordRef.GET(ICISupDragboxFile."Record ID") THEN
                        EXIT(420);
                    lRecordRef.SetTable(Item);
                    Item.GET(Item."No.");
                    // Verify if ContactNo has View rights for this Item
                    // ContactNo -> Find CustomerNo -> Find ServiceItems with ItemNo
                    // IF count = 0 -> not verified
                    ServiceItem.SetRange("Customer No.", Customer."No.");
                    ServiceItem.SetRange("Item No.", Item."No.");
                    IF ServiceItem.COUNT() = 0 THEN
                        EXIT(423);

                end;
        end;

        ICISupDragboxFile.CalcFields(Data);
        ICISupDragboxFile.Data.CreateInStream(lInstream, TextEncoding::Windows);
        txtB64 := Base64Convert.ToBase64(lInStream, true);
        ICIDownloadPort.Authorize();
        ICIDownloadPort.SetDownloadFile(ICISupDragboxFile.Filename, txtB64);

        EXIT(100);
    end;

    procedure SaveServiceItemShipToAddress(ContactNo: Code[20]; RecordIDText: Text; FieldsJsonB64: Text): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record Contact;
        Customer: Record Customer;
        ServiceItem: Record "Service Item";
        ShiptoAddress: Record "Ship-to Address";
        Base64Convert: Codeunit "Base64 Convert";
        lRecordRef: RecordRef;
        lRecordID: Recordid;
        lJsonObject: JsonObject;
        FieldsJsonText: Text;
        CustomerNo: Code[20];
        FoundShipToCode: Code[10];
        CreatedShipToCode: Code[10];
    begin
        // Check if Change allowed
        ICISupportSetup.Get();
        IF NOT ICISupportSetup."Service Integration" THEN
            EXIT(400);
        IF NOT ICISupportSetup."Allow Ship-To Address modify" THEN
            EXIT(400);

        // Get Customer
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(401);
        CustomerNo := Contact.GetCustomerNo();
        IF CustomerNo = '' THEN
            EXIT(402);
        IF NOt Customer.GET(CustomerNo) THEN
            EXIT(403);

        // Get JSON
        FieldsJsonText := Base64Convert.FromBase64(FieldsJsonB64);
        lJsonObject.ReadFrom(FieldsJsonText);

        // Get Service Item
        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(404);
        IF lRecordID.TableNo <> DATABASE::"Service Item" THEN
            EXIT(405);
        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);
        lRecordRef.SetTable(ServiceItem);

        // Check if Customer matches
        IF CustomerNo <> ServiceItem."Customer No." THEN
            EXIT(407);

        // Wenn keine Lieferadresse, dann IMMER eine anlegen
        IF ServiceItem."Ship-to Code" = '' THEN begin
            CreatedShipToCode := ShiptoAddress.CreateShipToAddressFromServiceItemCenter(ServiceItem, lJsonObject);
            IF CreatedShipToCode <> '' then begin
                ServiceItem.VALIDATE("Ship-to Code", CreatedShipToCode); // Neuen Code eintragen
                ServiceItem.Modify();
                EXIT(100); // erstellt
            end;
            EXIT(410);
        end ELSE
            // Wenn Lieferadresse vorhanden, dann entweder:
            CASE ICISupportSetup."Ship-To Address modify" OF
                ICISupportSetup."Ship-To Address modify"::"Overwrite Current":
                    begin
                        // aktuelle überschreiben
                        IF NOT ShiptoAddress.GET(ServiceItem."Customer No.", ServiceItem."Ship-to Code") THEN
                            EXIT(408);
                        ShiptoAddress.OverwriteAddressFromServiceItemCenter(ShipToAddress, ServiceItem, lJsonObject);
                        EXIT(100);
                    end;
                ICISupportSetup."Ship-To Address modify"::"Create New if needed":
                    begin
                        // Vorhanden?
                        FoundShipToCode := ShipToAddress.FindShipToAddressFromServiceItemCenter(ServiceItem, lJsonObject);
                        IF FoundShipToCode <> '' then begin
                            IF ServiceItem."Ship-to Code" <> FoundShipToCode then begin
                                ServiceItem.VALIDATE("Ship-to Code", FoundShipToCode); // Gefundenen Code eintragen
                                ServiceItem.Modify();
                            end;
                            EXIT(100); // Gefunden
                        end;

                        // neu anlegen, wenn nicht vorhanden + eintragen in Serviceartikel
                        CreatedShipToCode := ShiptoAddress.CreateShipToAddressFromServiceItemCenter(ServiceItem, lJsonObject);
                        IF CreatedShipToCode <> '' then begin
                            ServiceItem.VALIDATE("Ship-to Code", CreatedShipToCode); // Neuen Code eintragen
                            ServiceItem.Modify();
                            EXIT(100); // erstellt
                        end;
                        EXIT(410);
                    end;
            END;

        EXIT(410); // Unreachable
    end;

    procedure GetDownloadCategory(CategoryCode: Code[30]; var ICIDownloadCategoryPort: XmlPort "ICI Download Category Port"): Integer
    var
        ICIDownloadCategory: Record "ICI Download Category";
    begin
        IF NOT ICIDownloadCategory.GET(CategoryCode) THEN
            EXIT(400);

        ICIDownloadCategory.SetFilter(Code, ICIDownloadCategory.GetChildrenFilter());

        //ICIDownloadCategory.SetRecFilter();
        ICIDownloadCategoryPort.SetTableView(ICIDownloadCategory);

        ICIDownloadCategoryPort.Authorize();
        ICIDownloadCategoryPort.Export();

        EXIT(100);
    end;

    procedure GetDataroomFilesForCategory(ContactNo: Code[20]; CategoryCode: Code[30]; var ICISupDragboxFilePort: XmlPort "ICI Sup Dragbox File Port"): Integer
    var
        ICIDownloadCategory: Record "ICI Download Category";
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        CategoryFilter: Text;
    begin
        IF CategoryCode = '' then
            ICISupDragboxFile.SetFilter("ICI Download Category Code", '<>%1', '') // Alle Dateien mit Download Kategorie anzeigen. Sonst werden Dateien aus anderen Dragboxen angezeigt (z.B. Mailrobot Import, Serviceartikel oder Artikel)
        else begin
            IF NOT ICIDownloadCategory.GET(CategoryCode) THEN
                EXIT(401);
            CategoryFilter := ICIDownloadCategory.GetChildrenFilter();
            ICISupDragboxFile.Setfilter("ICI Download Category Code", CategoryFilter);
        end;

        ICISupDragboxFilePort.SetTableView(ICISupDragboxFile);
        ICISupDragboxFilePort.Authorize();
        EXIT(100);
    end;

    procedure DownloadDataroomFile(RecordIDText: Text; var ICIDownloadPort: XmlPort "ICI Download Port"): Integer
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        lInteger: Record Integer;
        Base64Convert: Codeunit "Base64 Convert";
        lRecordRef: RecordRef;
        lRecordID: RecordId;
        lInstream: InStream;
        txtB64: Text;
    begin
        lInteger.SetRange(Number, 0, 0);
        ICIDownloadPort.Authorize();
        ICIDownloadPort.SetTableView(lInteger); // Webservice Calls must use SetTableView or Triggers wont work. For Magic ~~~ 

        IF NOT EVALUATE(lRecordID, RecordIDText) then
            EXIT(401);

        IF lRecordID.TableNo <> DATABASE::"ICI Sup. Dragbox File" THEN
            EXIT(402);

        IF NOT lRecordRef.GET(lRecordID) THEN
            EXIT(406);

        lRecordRef.SetTable(ICISupDragboxFile);
        ICISupDragboxFile.GET(ICISupDragboxFile."Entry No.");

        //IF ICISupDragboxFile."Record ID".TableNo <> Database::"ICI Download Category" THEN
        //    EXIT(407);
        IF ICISupDragboxFile."ICI Download Category Code" = '' THEN
            exit(408);

        ICISupDragboxFile.CalcFields(Data);
        ICISupDragboxFile.Data.CreateInStream(lInstream, TextEncoding::Windows);
        txtB64 := Base64Convert.ToBase64(lInStream, true);

        ICIDownloadPort.SetDownloadFile(ICISupDragboxFile.Filename, txtB64);

        EXIT(100);
    end;

    procedure GetLoginTokenForContact(ContactNo: Code[20]): Text
    var
        ICIPortalToken: Record "ICI Portal Token";
        Contact: Record Contact;
        Token: Text[32];
    begin
        IF ContactNo <> '' THEN
            IF Contact.GET(ContactNo) THEN BEGIN
                Token := ICIPortalToken.GetCurrentContactToken(ContactNo);
                IF Token <> '' THEN
                    EXIT(Token);

                ICIPortalToken.CreateForContact(ICIPortalToken, Contact);
                EXIT(ICIPortalToken.Token);
            end;
    end;

    procedure GetLoginUrlForContact(ContactNo: Code[20]): Text
    var
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
    begin
        EXIT(ICISupportMailMgt.GenerateContactLink(ContactNo));
    end;

    local procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text) jToken: JsonToken;
    begin
        if not JsonObject.GET(TokenKey, jToken) then
            ERROR('Could not find a token with key %1', TokenKey);
    end;


    procedure SetLanguage(LanguageCode: Code[10])
    var
        ICISupportSetup: Record "ICI Support Setup";
        Language: Codeunit Language;
    begin
        LanguageID := GLOBALLANGUAGE; // Save Language before
        GLOBALLANGUAGE := Language.GetLanguageIdOrDefault(LanguageCode);
        IF LanguageCode = '' then begin
            ICISupportSetup.GET();
            GLOBALLANGUAGE := Language.GetLanguageIdOrDefault(ICISupportSetup."Webservice Language Code");
        end;
    end;

    procedure ResetLanguage()
    begin
        GLOBALLANGUAGE := LanguageID;
    end;

    procedure getServiceIntegration(): Boolean
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.Get();
        exit(ICISupportSetup."Service Integration");
    end;

    procedure getServiceIntegrationType(): Integer
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.Get();
        exit(ICISupportSetup."Service Registration Type");
    end;

    [IntegrationEvent(true, false)]
    local procedure OnBeforeServiceItemFilter(var IsHandled: Boolean; CustomerNo: Code[20]; ContactNo: Code[20]; var ICIServiceItemPort: XmlPort "ICI Service Item Port");
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterCreateTicketFromNewTicketForm(ICISupportTicket: Record "ICI Support Ticket"; ContactNo: Code[20]);
    begin
    end;

    var
        EmptyRecordId: RecordId;
        LanguageID: Integer;
}
