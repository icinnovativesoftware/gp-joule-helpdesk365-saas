xmlport 56290 "ICI Service Inv. Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceInvoiceHeader; "Service Invoice Header")
            {
                fieldelement(No; ServiceInvoiceHeader."No.") { }
                fieldelement(Description; ServiceInvoiceHeader.Description) { }

                fieldelement(SupportTicketNo; ServiceInvoiceHeader."ICI Support Ticket No.") { }
                fieldelement(OrderNo; ServiceInvoiceHeader."Order No.") { }
                textelement(OrderDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        OrderDate := Format(ServiceInvoiceHeader."Order Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(ServiceInvoiceHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(DueDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DueDate := Format(ServiceInvoiceHeader."Due Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(StartingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        StartingDate := Format(ServiceInvoiceHeader."Starting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(FinishingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        FinishingDate := Format(ServiceInvoiceHeader."Finishing Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; ServiceInvoiceHeader."Posting Description") { }
                fieldelement(Address; ServiceInvoiceHeader."Address") { }
                fieldelement(Address2; ServiceInvoiceHeader."Address 2") { }
                fieldelement(City; ServiceInvoiceHeader."City") { }
                fieldelement(ContactNo; ServiceInvoiceHeader."Contact No.") { }
                fieldelement(CountryRegionCode; ServiceInvoiceHeader."Country/Region Code") { }
                fieldelement(County; ServiceInvoiceHeader."County") { }
                fieldelement(CustomerNo; ServiceInvoiceHeader."Customer No.") { }
                fieldelement(EMail; ServiceInvoiceHeader."E-Mail") { }
                fieldelement(PhoneNo; ServiceInvoiceHeader."Phone No.") { }
                fieldelement(PostCode; ServiceInvoiceHeader."Post Code") { }
                fieldelement(SalespersonCode; ServiceInvoiceHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(ServiceInvoiceHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; ServiceInvoiceHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; ServiceInvoiceHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; ServiceInvoiceHeader."Ship-to City") { }
                fieldelement(ShiptoCode; ServiceInvoiceHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; ServiceInvoiceHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; ServiceInvoiceHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; ServiceInvoiceHeader."Ship-to County") { }
                fieldelement(ShiptoName; ServiceInvoiceHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; ServiceInvoiceHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; ServiceInvoiceHeader."Ship-to Post Code") { }
                fieldelement(YourReference; ServiceInvoiceHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(ServiceInvoiceHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(ServiceInvoiceHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(ServiceInvoiceHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Service Invoice Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    fieldelement(LineServiceItemNo; Lines."Service Item No.") { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", ServiceInvoiceHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ServiceInvoiceHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
