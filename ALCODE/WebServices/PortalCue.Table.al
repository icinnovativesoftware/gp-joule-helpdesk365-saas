table 56303 "ICI Portal Cue"
{
    Caption = 'Portal Cue', Comment = 'de-DE=Portal Kachel';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Portal Cue Set Code"; Code[20])
        {
            Caption = 'Portal Cue Set Code', Comment = 'de-DE=Portal Kachel Set Code';
            DataClassification = SystemMetadata;
            TableRelation = "ICI Portal Cue Set";
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No.', Comment = 'de-DE=Zeilennr.';
            DataClassification = SystemMetadata;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "State Filter"; Text[100])
        {
            Caption = 'State Filter', Comment = 'de-DE=Status Filter';
            trigger OnLookup()
            begin
                "State Filter" := ICIPortalCueMgt.LookUpStateFilter();
            end;
        }
        field(12; "Process Filter"; Text[100])
        {
            //TableRelation = "ICI Support Process";
            Caption = 'Process Filter', Comment = 'de-DE=Process Filter';
            trigger OnLookup()
            begin
                "Process Filter" := ICIPortalCueMgt.LookupProcessFilter();
            end;
        }
        field(13; "Process Stage Filter"; Code[100])
        {
            Caption = 'Process Stage Filter', Comment = 'de-DE=Processstufen Filter';

            trigger OnLookup()
            begin
                "Process Stage Filter" := ICIPortalCueMgt.LookUpProcessStageFilter(Rec);
            end;
        }
        field(14; "Only Own"; Boolean)
        {
            Caption = 'Only Own', Comment = 'de-DE=Nur Eigene';
        }
        field(15; Inactive; Boolean)
        {
            Caption = 'Inactive', Comment = 'de-DE=Inaktiv';
            DataClassification = CustomerContent;
        }
        field(16; "No. of Translations"; Integer)
        {
            Editable = false;
            Caption = 'No. of Translations', Comment = 'de-DE=Anz. Übersetzungen';
            FieldClass = FlowField;
            CalcFormula = Count("ICI Portal Cue Translation" where("Portal Cue Set Code" = field("Portal Cue Set Code"), "Portal Cue Line No." = field("Line No.")));
        }
    }
    keys
    {
        key(PK; "Portal Cue Set Code", "Line No.")
        {
            Clustered = true;
        }
    }
    var
        ICIPortalCueMgt: Codeunit "ICI Portal Cue Mgt.";
}
