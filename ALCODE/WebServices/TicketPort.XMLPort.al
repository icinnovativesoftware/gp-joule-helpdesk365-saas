xmlport 56280 "ICI Ticket Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {

            tableelement(ICISupportTicket; "ICI Support Ticket")
            {
                textelement(NoOfPages)
                {

                }
                textelement(NoOfTickets)
                {

                }
                fieldelement(No; ICISupportTicket."No.")
                {
                }
                fieldelement(InputTicketNo; ICISupportTicket."No.")
                {
                }
                fieldelement(Category1Code; ICISupportTicket."Category 1 Code")
                {
                }
                textelement(Category1Description)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportTicketCategory: Record "ICI Support Ticket Category";
                    begin
                        Category1Description := ICISupportTicket."Category 1 Description";
                        IF ICISupportTicketCategory.GET(ICISupportTicket."Category 1 Code") THEN
                            Category1Description := ICISupportTicketCategory.GetDescription();
                    end;
                }
                fieldelement(Category2Code; ICISupportTicket."Category 2 Code")
                {
                }
                textelement(Category2Description)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportTicketCategory: Record "ICI Support Ticket Category";
                    begin
                        Category2Description := ICISupportTicket."Category 2 Description";
                        IF ICISupportTicketCategory.GET(ICISupportTicket."Category 2 Code") THEN
                            Category2Description := ICISupportTicketCategory.GetDescription();
                    end;
                }
                fieldelement(CompanyContactNo; ICISupportTicket."Company Contact No.")
                {
                }
                fieldelement(CompanyContactName; ICISupportTicket."Comp. Contact Name")
                {
                }
                fieldelement(CreatedOn; ICISupportTicket."Created On")
                {
                }
                fieldelement(CurrentContactNo; ICISupportTicket."Current Contact No.")
                {
                }
                fieldelement(CurrentContactName; ICISupportTicket."Curr. Contact Name")
                {
                }
                fieldelement(DepartmentCode; ICISupportTicket."Department Code")
                {
                }
                fieldelement(DepartmentDescription; ICISupportTicket."Department Description")
                {
                }
                fieldelement(SerialNo; ICISupportTicket."Serial No.")
                {
                }
                fieldelement(ServiceItemNo; ICISupportTicket."Service Item No.")
                {
                }
                fieldelement(ProcessStage; ICISupportTicket."Process Stage")
                {
                }
                fieldelement(ProcessState; ICISupportTicket."Ticket State")
                {
                }
                fieldelement(ProcessCode; ICISupportTicket."Process Code")
                {
                }
                textelement(ProcessStageDescription)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportProcessStage: Record "ICI Support Process Stage";
                    begin
                        ProcessStageDescription := ICISupportTicket."Process Stage Description";
                        IF ICISupportProcessStage.GET(ICISupportTicket."Process Code", ICISupportTicket."Process Stage") THEN
                            ProcessStageDescription := ICISupportProcessStage.GetDescription();
                    end;
                }
                fieldelement(HighlightforContact; ICISupportTicket."Highlight for Contact")
                {
                }
                fieldelement(HighlightforUser; ICISupportTicket."Highlight for User")
                {
                }
                fieldelement(Description; ICISupportTicket.Description)
                {
                }
                fieldelement(SupportUserID; ICISupportTicket."Support User ID") { }
                fieldelement(SupportUserName; ICISupportTicket."Support User Name") { }

            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }

    // Needed for Pagination - maybe removed later on
    procedure SetNoOfTickets(NewNoOfTickets: Integer)
    begin
        NoOfTickets := FORMAT(NewNoOfTickets);
    end;
    // Needed for Pagination - maybe removed later on
    procedure SetNoOfPages(NewNoOfPages: Integer)
    begin
        NoOfPages := FORMAT(NewNoOfPages);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
}
