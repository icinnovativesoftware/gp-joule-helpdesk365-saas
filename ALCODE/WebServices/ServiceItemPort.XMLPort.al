xmlport 56283 "ICI Service Item Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceItem; "Service Item")
            {
                fieldelement(No; ServiceItem."No.") { }
                fieldelement(SerialNo; ServiceItem."Serial No.") { }
                fieldelement(Name; ServiceItem.Name) { }
                fieldelement(CountryRegionCode; ServiceItem."Country/Region Code") { }
                fieldelement(Description; ServiceItem.Description) { }
                fieldelement(ItemNo; ServiceItem."Item No.") { }
                fieldelement(ShipToCountryRegion; ServiceItem."Ship-to Country/Region Code") { }
                textelement(ShipToName) { }
                textelement(ShipToAddress) { }
                textelement(ShipToAddress2) { }
                textelement(ShipToPostCode) { }
                textelement(ShipToCity) { }
                textelement(ShipToCounty) { }
                textelement(ShipToPhoneNo) { }
                textelement(ShipToContact) { }

                textelement(ServiceItemRecID)
                {
                    trigger OnBeforePassVariable()
                    begin
                        ServiceItemRecID := FORMAT(ServiceItem.RecordId());
                    end;
                }

                tableelement(Files; "ICI Sup. Dragbox File")
                {
                    fieldelement(FileName; Files.Filename) { }
                    fieldelement(Size; Files.Size) { }
                    fieldelement(CreatedBy; Files."Created By") { }
                    fieldelement(CreatedOn; Files."Created On") { }
                    textelement(CreatedOnDate)
                    {
                        trigger OnBeforePassVariable()
                        var
                            CreatedDate: Date;
                        begin
                            CreatedDate := DT2Date(Files."Created On");
                            CreatedOnDate := Format(CreatedDate, 0, '<Day,2>.<Month,2>.<Year4>')
                        end;
                    }
                    textelement(RecID)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            RecID := FORMAT(Files.RecordId());
                        end;
                    }

                    trigger OnPreXmlItem()
                    var
                        Item: Record Item;
                    // NoOfFiles: Integer;
                    begin
                        Files.SetRange("Record ID", ServiceItem.RecordId());
                        IF Item.GET(ServiceItem."Item No.") THEN
                            Files.SetFilter("Record ID", '%1|%2', ServiceItem.RecordId(), Item.RecordId());
                        // NoOfFiles := Files.Count();
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    IF ServiceItem."Ship-to Code" = '' THEN BEGIN
                        ServiceItem.CALCFIELDS(
                          Name, "Name 2", Address, "Address 2", "Post Code", City,
                          County, "Country/Region Code", Contact, "Phone No.");

                        ShipToName := ServiceItem.Name;
                        ShipToAddress := ServiceItem.Address;
                        ShipToAddress2 := ServiceItem."Address 2";
                        ShipToPostCode := ServiceItem."Post Code";
                        ShipToCity := ServiceItem.City;
                        ShipToCounty := ServiceItem.County;
                        ShipToPhoneNo := ServiceItem."Phone No.";
                        ShipToContact := ServiceItem.Contact;
                    END ELSE begin
                        ServiceItem.CALCFIELDS(
                            "Ship-to Name", "Ship-to Name 2", "Ship-to Address", "Ship-to Address 2", "Ship-to Post Code",
                            "Ship-to City", "Ship-to County", "Ship-to Country/Region Code", "Ship-to Contact", "Ship-to Phone No.");
                        ShipToName := ServiceItem."Ship-to Name";
                        ShipToAddress := ServiceItem."Ship-to Address";
                        ShipToAddress2 := ServiceItem."Ship-to Address 2";
                        ShipToPostCode := ServiceItem."Ship-to Post Code";
                        ShipToCity := ServiceItem."Ship-to City";
                        ShipToCounty := ServiceItem."Ship-to County";
                        ShipToPhoneNo := ServiceItem."Ship-to Phone No.";
                        ShipToContact := ServiceItem."Ship-to Contact";
                    end;
                end;

            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ServiceItem.SetRange(Status, ServiceItem.Status::Installed);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;
}
