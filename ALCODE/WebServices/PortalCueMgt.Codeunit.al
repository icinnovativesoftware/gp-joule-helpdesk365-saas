codeunit 56292 "ICI Portal Cue Mgt."
{
    procedure CalcNoOfTickets(var PortalCue: Record "ICI Portal Cue"; CompanyContactNo: Code[20]; PersonContactNo: Code[20]): Integer
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportSetup: Record "ICI Support Setup";
    begin
        if not ICISupportSetup.Get() then
            exit(403);

        ApplyPortalCueFilterToTicket(ICISupportTicket, PortalCue, CompanyContactNo, PersonContactNo);
        if CompanyContactNo = ICISupportSetup."Guest Contact Company No." then
            ICISupportTicket.SetRange("Current Contact No.", PersonContactNo);
        ICISupportTicket.SetRange(Online, true);
        EXIT(ICISupportTicket.Count());
    end;

    procedure LookUpStateFilter(): Text[100]
    var
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        NameValueLookup: Page "Name/Value Lookup";
        ICITicketState: Enum "ICI Ticket State";
        StateName: Text;
        StateOrdinal: Integer;
        FilterString: Text;
    begin
        foreach StateOrdinal in ICITicketState.Ordinals do begin
            StateName := FORMAT(Enum::"ICI Ticket State".FromInteger(StateOrdinal));

            TempNameValueBuffer.AddNewEntry(FORMAT(StateOrdinal), COPYSTR(StateName, 1, 250));
            NameValueLookup.AddItem(FORMAT(StateOrdinal), COPYSTR(StateName, 1, 250));
        end;
        NameValueLookup.EDITABLE(FALSE);
        NameValueLookup.LOOKUPMODE(TRUE);
        IF NameValueLookup.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            NameValueLookup.SetSelectionFilter(TempNameValueBuffer);
            TempNameValueBuffer.MARKEDONLY(TRUE);
            IF TempNameValueBuffer.FINDSET() THEN BEGIN
                CLEAR(FilterString);
                REPEAT
                    IF FilterString = '' THEN
                        FilterString := TempNameValueBuffer.Name
                    ELSE
                        FilterString := (FilterString + '|' + TempNameValueBuffer.Name);
                UNTIL TempNameValueBuffer.NEXT() <= 0;
            END;

            EXIT(COPYSTR(FilterString, 1, 100));
        end
    end;

    procedure LookUpProcessFilter(): Text[100]
    var
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportProcess: Record "ICI Support Process";
        NameValueLookup: PAge "Name/Value Lookup";
        FilterString: Text;
    begin
        IF ICISupportProcess.Findset() THEN
            REPEAT
                TempNameValueBuffer.AddNewEntry(ICISupportProcess.Code, ICISupportProcess.Description);
                NameValueLookup.AddItem(ICISupportProcess.Code, ICISupportProcess.Description);
            UNTIL ICISupportProcess.NEXT() = 0;

        NameValueLookup.EDITABLE(FALSE);
        NameValueLookup.LOOKUPMODE(TRUE);
        IF NameValueLookup.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            NameValueLookup.SetSelectionFilter(TempNameValueBuffer);
            TempNameValueBuffer.MARKEDONLY(TRUE);
            IF TempNameValueBuffer.FINDSET() THEN BEGIN
                CLEAR(FilterString);
                REPEAT
                    IF FilterString = '' THEN
                        FilterString := TempNameValueBuffer.Name
                    ELSE
                        FilterString := (FilterString + '|' + TempNameValueBuffer.Name);
                UNTIL TempNameValueBuffer.NEXT() <= 0;
            END;

            EXIT(COPYSTR(FilterString, 1, 100));
        end;
    end;

    procedure LookUpProcessStageFilter(var PortalCue: Record "ICI Portal Cue"): Text[100]
    var
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportProcessStage: Record "ICI Support Process Stage";
        NameValueLookup: PAge "Name/Value Lookup";
        FilterString: Text;
    begin
        ICISupportProcessStage.SetFilter("Process Code", PortalCue."Process Filter");
        IF ICISupportProcessStage.Findset() THEN
            REPEAT
                TempNameValueBuffer.AddNewEntry(FORMAT(ICISupportProcessStage.Stage), ICISupportProcessStage.Description);
                NameValueLookup.AddItem(FORMAT(ICISupportProcessStage.Stage), ICISupportProcessStage.Description);
            UNTIL ICISupportProcessStage.NEXT() = 0;

        NameValueLookup.EDITABLE(FALSE);
        NameValueLookup.LOOKUPMODE(TRUE);
        IF NameValueLookup.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            NameValueLookup.SetSelectionFilter(TempNameValueBuffer);
            TempNameValueBuffer.MARKEDONLY(TRUE);
            IF TempNameValueBuffer.FINDSET() THEN BEGIN
                CLEAR(FilterString);
                REPEAT
                    IF FilterString = '' THEN
                        FilterString := TempNameValueBuffer.Name
                    ELSE
                        FilterString := (FilterString + '|' + TempNameValueBuffer.Name);
                UNTIL TempNameValueBuffer.NEXT() <= 0;
            END;

            EXIT(COPYSTR(FilterString, 1, 100));
        end;
    end;

    // Use Contact to get PortalCueSet Code
    procedure ApplyCueFilters(var ICISupportTicket: Record "ICI Support Ticket"; ContactNo: Code[20]; PortalCueLineNo: Text[30])
    var
        Contact: Record Contact;
        CompanyContact: Record Contact;
        ICISupportSetup: Record "ICI Support Setup";
        ICIPortalCue: Record "ICI Portal Cue";
        CueSetCode: Code[20];
        PortalCueLineNoInt: Integer;
    begin
        Contact.GET(ContactNo);

        ICISupportSetup.GET();
        CueSetCode := ICISupportSetup."Default Portal Cue Set Code"; // Std Kachelset

        IF CompanyContact.GET(Contact."Company No.") THEN
            IF CompanyContact."ICI Portal Cue Set Code" <> '' then
                CueSetCode := CompanyContact."ICI Portal Cue Set Code"; // Kachelset des Unternehmens

        IF Contact."ICI Portal Cue Set Code" <> '' then
            CueSetCode := Contact."ICI Portal Cue Set Code"; // Kachelset der Person

        IF CueSetCode = '' THEN
            exit;

        EVALUATE(PortalCueLineNoInt, PortalCueLineNo);
        IF not ICIPortalCue.GET(CueSetCode, PortalCueLineNoInt) THEN
            exit;

        ApplyPortalCueFilterToTicket(ICISupportTicket, ICIPortalCue, CompanyContact."No.", Contact."No.");

    end;

    procedure ApplyPortalCueFilterToTicket(var ICISupportTicket: Record "ICI Support Ticket"; var PortalCue: Record "ICI Portal Cue"; CompanyContactNo: Code[20]; PersonContactNo: Code[20])
    begin
        ICISupportTicket.SetCurrentKey("Company Contact No.", "Ticket State");
        ICISupportTicket.SetFilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Preparation);
        if (PortalCue."Process Filter" <> '') then
            ICISupportTicket.SetFilter("Process Code", PortalCue."Process Filter");
        if (PortalCue."Process Stage Filter" <> '') THEN
            ICISupportTicket.SetFilter("Process Stage", PortalCue."Process Stage Filter");
        if (PortalCue."State Filter" <> '') THEN
            ICISupportTicket.SetFilter("Ticket State", PortalCue."State Filter");

        if (CompanyContactNo <> '') then
            ICISupportTicket.SetRange("Company Contact No.", CompanyContactNo);
        if ((PersonContactNo <> '') AND PortalCue."Only Own") then
            ICISupportTicket.SetRange("Current Contact No.", PersonContactNo);
    end;

    procedure GetCueTranslation(var ICIPortalCue: Record "ICI Portal Cue"; LanguageCode: Code[20]): Text
    var
        ICIPortalCueTranslation: Record "ICI Portal Cue Translation";
    begin
        IF LanguageCode = '' THEN
            EXIT(ICIPortalCue.Description);

        IF ICIPortalCueTranslation.GET(ICIPortalCue."Portal Cue Set Code", ICIPortalCue."Line No.", LanguageCode) THEN
            EXIT(ICIPortalCueTranslation.Description);

        EXIT(ICIPortalCue.Description);
    end;

}
