xmlport 56278 "ICI Ticket Category Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ICISupportTicketCategory; "ICI Support Ticket Category")
            {
                fieldelement(ParentCategory; ICISupportTicketCategory."Parent Category") { }
                fieldelement(Code; ICISupportTicketCategory.Code) { }
                textelement(Description)
                {
                    trigger OnBeforePassVariable()
                    begin
                        Description := ICISupportTicketCategory.GetDescription();
                    end;
                }
                fieldelement(Layer; ICISupportTicketCategory.Layer) { }
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ICISupportTicketCategory.SetRange(Online, true);
        ICISupportTicketCategory.SetRange(Inactive, false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    var
        Authorized: Boolean;

}
