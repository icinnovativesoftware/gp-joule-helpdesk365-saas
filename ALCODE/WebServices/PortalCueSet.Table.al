table 56302 "ICI Portal Cue Set"
{
    Caption = 'Portal Cue Set', Comment = 'de-DE=Portal Kachelset';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Portal Cue Sets";
    DrillDownPageId = "ICI Portal Cue Sets";
    fields
    {
        field(1; Code; Code[20])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "No. of Portal Cues"; Integer)
        {
            Caption = 'No. of Portal Cues', Comment = 'de-DE=Anzahl Kacheln';
            FieldClass = FlowField;
            CalcFormula = count("ICI Portal Cue" where("Portal Cue Set Code" = field(code), Inactive = const(false)));
            Editable = false;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

}
