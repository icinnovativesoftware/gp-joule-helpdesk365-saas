table 56307 "ICI Portal Cue Translation"
{
    Caption = 'Portal Cue Translation', Comment = 'de-DE=Kundenportal Kachel Übersetzung';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Portal Cue Trans. List";
    DrillDownPageId = "ICI Portal Cue Trans. List";

    fields
    {
        field(1; "Portal Cue Set Code"; Code[20])
        {
            Caption = 'Portal Cue Set Code', Comment = 'de-DE=Kundenportalkachelsetcode';
            DataClassification = SystemMetadata;
            TableRelation = "ICI Portal Cue Set";
        }
        field(2; "Portal Cue Line No."; Integer)
        {
            Caption = 'Line No.', Comment = 'de-DE=Zeilennr.';
            DataClassification = SystemMetadata;
        }
        field(3; "Language Code"; Code[10])
        {
            Caption = 'Language Code', Comment = 'de-DE=Sprachcode';
            TableRelation = Language;
            DataClassification = CustomerContent;
        }

        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Portal Cue Set Code", "Portal Cue Line No.", "Language Code")
        {
            Clustered = true;
        }
    }

}
