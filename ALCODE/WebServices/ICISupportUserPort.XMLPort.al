xmlport 56277 "ICI Support User Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ICISupportUser; "ICI Support User")
            {
                fieldelement(EMail; ICISupportUser."E-Mail")
                {
                }
                fieldelement(FirstName; ICISupportUser."First Name")
                {
                }
                fieldelement(JobTitle; ICISupportUser."Job Title")
                {
                }
                fieldelement(LanguageCode; ICISupportUser."Language Code")
                {
                }
                fieldelement(MiddleName; ICISupportUser."Middle Name")
                {
                }
                fieldelement(MobilePhoneNo; ICISupportUser."Mobile Phone No.")
                {
                }
                fieldelement(PhoneNo; ICISupportUser."Phone No.")
                {
                }
                fieldelement(SalespersonCode; ICISupportUser."Salesperson Code")
                {
                }
                fieldelement(SalutationCode; ICISupportUser."Salutation Code")
                {
                }
                fieldelement(UserID; ICISupportUser."User ID")
                {
                }
                fieldelement(Name; ICISupportUser.Name)
                {
                }
                fieldelement(Password; ICISupportUser."Password Hash")
                {
                }
                fieldelement(Surname; ICISupportUser.Surname)
                {
                }
                textelement(NoOfTickets)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                        Contact: Record Contact;
                        NoOfTicketsLbl: Label '%1', Comment = '%1=No of Tickets|de-DE=%1';
                    begin
                        ICISupportTicket.SetRange("Support User ID", ICISupportUser."User ID");
                        ICISupportTicket.SetRange("Ticket State", "ICI Ticket State"::Open, "ICI Ticket State"::Waiting);

                        IF CompanyNo <> '' THEN begin
                            Contact.GET(CompanyNo);
                            ICISupportTicket.SetRange("Company Contact No.", Contact."Company No.");
                        end;

                        NoOfTickets := StrSubstNo(NoOfTicketsLbl, (ICISupportTicket.Count()));
                    end;
                }
                textelement(CompanyName)
                {
                    trigger OnBeforePassVariable()
                    begin
                        CompanyName := CompanyName();
                    end;
                }
                fieldelement(Login; ICISupportUser.Login) { }
            }
            trigger OnBeforePassVariable()
            begin
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure SetCompanyNoFilter(pCompanyNo: Code[20])
    begin
        CompanyNo := pCompanyNo;
    end;

    var
        Authorized: Boolean;
        CompanyNo: Code[20];


}
