xmlport 56284 "ICI Sales Header Port"
{
    UseDefaultNamespace = true;

    schema
    {
        textelement(RootNodeName)
        {
            tableelement(SalesHeader; "Sales Header")
            {
                fieldelement(No; SalesHeader."No.") { }
                fieldelement(QuoteNo; SalesHeader."Quote No.") { }
                fieldelement(DocumentType; SalesHeader."Document Type") { }
                fieldelement(SelltoAddress; SalesHeader."Sell-to Address") { }
                fieldelement(SelltoAddress2; SalesHeader."Sell-to Address 2") { }
                fieldelement(SelltoCity; SalesHeader."Sell-to City") { }
                fieldelement(SelltoContact; SalesHeader."Sell-to Contact") { }
                fieldelement(SelltoContactNo; SalesHeader."Sell-to Contact No.") { }
                fieldelement(SelltoCountryRegionCode; SalesHeader."Sell-to Country/Region Code") { }
                fieldelement(SelltoCounty; SalesHeader."Sell-to County") { }
                fieldelement(SelltoCustomerName; SalesHeader."Sell-to Customer Name") { }
                fieldelement(SelltoCustomerNo; SalesHeader."Sell-to Customer No.") { }
                fieldelement(SelltoEMail; SalesHeader."Sell-to E-Mail") { }
                fieldelement(SelltoPhoneNo; SalesHeader."Sell-to Phone No.") { }
                fieldelement(SelltoPostCode; SalesHeader."Sell-to Post Code") { }
                fieldelement(ShiptoAddress; SalesHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; SalesHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; SalesHeader."Ship-to City") { }
                fieldelement(ShiptoCode; SalesHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; SalesHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; SalesHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; SalesHeader."Ship-to County") { }
                fieldelement(ShiptoName; SalesHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; SalesHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; SalesHeader."Ship-to Post Code") { }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(SalesHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(SalespersonCode; SalesHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(SalesHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                textelement(ShipmentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        ShipmentDate := Format(SalesHeader."Shipment Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(ExternalDocumentNo; SalesHeader."External Document No.") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(SalesHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(DueDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DueDate := Format(SalesHeader."Due Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(YourReference; SalesHeader."Your Reference") { }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(SalesHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                fieldelement(Amount; SalesHeader.Amount)
                {
                    trigger OnBeforePassField()
                    begin
                        SalesHeader.CalcFields(Amount);
                    end;
                }
                fieldelement(AmountIncludingVAT; SalesHeader."Amount Including VAT")
                {
                    trigger OnBeforePassField()
                    begin
                        SalesHeader.CalcFields(Amount);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(SalesHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Sales Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentType; Lines."Document Type") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }
                    fieldelement(LineAmount; Lines.Amount) { }
                    fieldelement(LineAmountIncludingVAT; Lines."Amount Including VAT") { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document Type", SalesHeader."Document Type");
                        Lines.Setrange("Document No.", SalesHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        SalesHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
