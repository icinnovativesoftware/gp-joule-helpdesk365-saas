xmlport 56292 "ICI Service Contr. Header Port"
{
    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(ServiceContractHeader; "Service Contract Header")
            {
                fieldelement(YourReference; ServiceContractHeader."Your Reference") { }

                fieldelement(SupportTicketNo; ServiceContractHeader."ICI Support Ticket No.") { }

                fieldelement(Address; ServiceContractHeader.Address) { }
                fieldelement(Address2; ServiceContractHeader."Address 2") { }
                fieldelement(AmountperPeriod; ServiceContractHeader."Amount per Period") { }
                fieldelement(AnnualAmount; ServiceContractHeader."Annual Amount") { }
                fieldelement(ContactName; ServiceContractHeader."Contact Name") { }
                fieldelement(ContactNo; ServiceContractHeader."Contact No.") { }
                fieldelement(ContractCostAmount; ServiceContractHeader."Contract Cost Amount") { }
                fieldelement(ContractDiscountAmount; ServiceContractHeader."Contract Discount Amount") { }
                fieldelement(ContractNo; ServiceContractHeader."Contract No.") { }
                fieldelement(ContractType; ServiceContractHeader."Contract Type") { }
                fieldelement(County; ServiceContractHeader.County) { }
                fieldelement(Description; ServiceContractHeader.Description) { }
                fieldelement(EMail; ServiceContractHeader."E-Mail") { }
                fieldelement(SalespersonCode; ServiceContractHeader."Salesperson Code") { }
                fieldelement(ShiptoAddress; ServiceContractHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; ServiceContractHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; ServiceContractHeader."Ship-to City") { }
                fieldelement(ShiptoCode; ServiceContractHeader."Ship-to Code") { }
                fieldelement(ShiptoCountryRegionCode; ServiceContractHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; ServiceContractHeader."Ship-to County") { }
                fieldelement(ShiptoName; ServiceContractHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; ServiceContractHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; ServiceContractHeader."Ship-to Post Code") { }
                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(ServiceContractHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(ServiceContractHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Service Contract Line")
                {
                    fieldelement(LineNo; Lines."Line No.") { }
                    fieldelement(LineDocumentNo; Lines."Contract No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineServiceItemNo; Lines."Service Item No.") { }
                    fieldelement(LineItemNo; Lines."Item No.") { }
                    fieldelement(LineSerialNo; Lines."Serial No.") { }
                    fieldelement(LineAmount; Lines."Line Amount") { }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Contract No.", ServiceContractHeader."Contract No.");
                        Lines.Setrange("Contract Type", ServiceContractHeader."Contract Type");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }
            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        ServiceContractHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
