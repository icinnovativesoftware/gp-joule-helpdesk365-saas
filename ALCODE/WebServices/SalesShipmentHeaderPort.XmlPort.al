xmlport 56285 "ICI Sales Shipment Header Port"
{

    UseDefaultNamespace = true;
    schema
    {
        textelement(RootNodeName)
        {
            tableelement(SalesShipmentHeader; "Sales Shipment Header")
            {
                fieldelement(No; SalesShipmentHeader."No.") { }
                fieldelement(SupportTicketNo; SalesShipmentHeader."ICI Support Ticket No.") { }
                fieldelement(QuoteNo; SalesShipmentHeader."Quote No.") { }
                fieldelement(OrderNo; SalesShipmentHeader."Order No.") { }
                textelement(OrderDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        OrderDate := Format(SalesShipmentHeader."Order Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                textelement(PostingDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        PostingDate := Format(SalesShipmentHeader."Posting Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(PostingDescription; SalesShipmentHeader."Posting Description") { }
                fieldelement(SelltoAddress; SalesShipmentHeader."Sell-to Address") { }
                fieldelement(SelltoAddress2; SalesShipmentHeader."Sell-to Address 2") { }
                fieldelement(SelltoCity; SalesShipmentHeader."Sell-to City") { }
                fieldelement(SelltoContact; SalesShipmentHeader."Sell-to Contact") { }
                fieldelement(SelltoContactNo; SalesShipmentHeader."Sell-to Contact No.") { }
                fieldelement(SelltoCountryRegionCode; SalesShipmentHeader."Sell-to Country/Region Code") { }
                fieldelement(SelltoCounty; SalesShipmentHeader."Sell-to County") { }
                fieldelement(SelltoCustomerName; SalesShipmentHeader."Sell-to Customer Name") { }
                fieldelement(SelltoCustomerName2; SalesShipmentHeader."Sell-to Customer Name 2") { }
                fieldelement(SelltoCustomerNo; SalesShipmentHeader."Sell-to Customer No.") { }
                fieldelement(SelltoEMail; SalesShipmentHeader."Sell-to E-Mail") { }
                fieldelement(SelltoPhoneNo; SalesShipmentHeader."Sell-to Phone No.") { }
                fieldelement(SelltoPostCode; SalesShipmentHeader."Sell-to Post Code") { }
                fieldelement(SalespersonCode; SalesShipmentHeader."Salesperson Code") { }
                textelement(SalespersonName)
                {
                    trigger OnBeforePassVariable()
                    var
                        Salesperson: Record "Salesperson/Purchaser";
                    begin
                        IF Salesperson.GET(SalesShipmentHeader."Salesperson Code") THEN
                            SalespersonName := Salesperson.Name;
                    end;
                }
                fieldelement(ShiptoAddress; SalesShipmentHeader."Ship-to Address") { }
                fieldelement(ShiptoAddress2; SalesShipmentHeader."Ship-to Address 2") { }
                fieldelement(ShiptoCity; SalesShipmentHeader."Ship-to City") { }
                fieldelement(ShiptoCode; SalesShipmentHeader."Ship-to Code") { }
                fieldelement(ShiptoContact; SalesShipmentHeader."Ship-to Contact") { }
                fieldelement(ShiptoCountryRegionCode; SalesShipmentHeader."Ship-to Country/Region Code") { }
                fieldelement(ShiptoCounty; SalesShipmentHeader."Ship-to County") { }
                fieldelement(ShiptoName; SalesShipmentHeader."Ship-to Name") { }
                fieldelement(ShiptoName2; SalesShipmentHeader."Ship-to Name 2") { }
                fieldelement(ShiptoPostCode; SalesShipmentHeader."Ship-to Post Code") { }
                textelement(ShipmentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        ShipmentDate := Format(SalesShipmentHeader."Shipment Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(YourReference; SalesShipmentHeader."Your Reference") { }
                textelement(DocumentDate)
                {
                    trigger OnBeforePassVariable()
                    begin
                        DocumentDate := Format(SalesShipmentHeader."Document Date", 0, '<Day,2>.<Month,2>.<Year4>')
                    end;
                }
                fieldelement(ExternalDocumentNo; SalesShipmentHeader."External Document No.") { }
                fieldelement(ShippingAgentCode; SalesShipmentHeader."Shipping Agent Code") { }

                textelement(RecID)
                {
                    trigger OnBeforePassVariable()
                    var
                        RecID2: RecordId;
                    begin
                        RecID := FORMAT(SalesShipmentHeader.RecordId);

                        Evaluate(RecID2, RecID);
                    end;
                }
                textelement(FileName)
                {
                    trigger OnBeforePassVariable()
                    var
                        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
                    begin
                        FileName := ICISupportDocumentMgt.GetFileNameForDocumentPDF(SalesShipmentHeader.RecordId());
                    end;
                }
                tableelement(Lines; "Sales Shipment Line")
                {
                    fieldelement(LineNo; Lines."No.") { }
                    fieldelement(LineDocumentNo; Lines."Document No.") { }
                    fieldelement(LineDescription; Lines.Description) { }
                    fieldelement(LineQuantity; Lines.Quantity) { }
                    fieldelement(LineUnitOfMeasure; Lines."Unit of Measure") { }

                    textelement(LineDescription1and2)
                    {
                        trigger OnBeforePassVariable()
                        begin
                            LineDescription1and2 := Lines.Description;
                            IF Lines."Description 2" <> '' then
                                LineDescription1and2 += '' + Lines."Description 2";
                        end;
                    }

                    trigger OnPreXmlItem()
                    begin
                        Lines.Setrange("Document No.", SalesShipmentHeader."No.");
                        if not ShowLines THEN
                            currXMLport.Skip();
                    end;
                }

            }
            trigger OnBeforePassVariable()
            var
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();
                // GLOBALLANGUAGE := Language.GetLanguageID(ICISupportSetup."Webservice Language Code");
                IF NOT Authorized then
                    Error('Not Authorized');
            end;
        }
    }
    trigger OnPreXmlPort()
    begin
        SalesShipmentHeader.SetRange("ICI Hidden from Webarchive", false);
    end;

    procedure Authorize()
    begin
        Authorized := true;
    end;

    procedure ShowDocumentLines()
    begin
        ShowLines := true;
    end;

    var
        Authorized: Boolean;
        ShowLines: Boolean;
}
