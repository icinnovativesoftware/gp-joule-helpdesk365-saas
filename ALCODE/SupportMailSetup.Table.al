table 56289 "ICI Support Mail Setup"
{
    Caption = 'Support Mail Setup', Comment = 'de-DE=Support E-Mail Einrichtung';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Code; Code[10])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = SystemMetadata;
        }
        field(10; "Test Mode"; Boolean)
        {
            Caption = 'Test Mode', Comment = 'de-DE=Testmodus';
            DataClassification = CustomerContent;
        }
        field(11; "Test Mode E-Mail Receiver"; Text[250])
        {
            Caption = 'Test Mode E-Mail Receiver', Comment = 'de-DE=Testmodus E-Mail Empfänger';
            DataClassification = CustomerContent;
        }
        field(33; "E-Mail BCC"; Text[250])
        {
            Caption = 'E-Mail BCC', Comment = 'de-DE=BCC';
            DataClassification = CustomerContent;
        }
        field(35; "E-Mail Footer"; Code[30])
        {
            Caption = 'E-Mail Footer', Comment = 'de-DE=Footer';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(36; "E-Mail C PW forgotten Active"; Boolean)
        {
            Caption = 'E-Mail C Password forgotten Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(37; "E-Mail C Password forgotten"; Code[30])
        {
            Caption = 'E-Mail C Password forgotten', Comment = 'de-DE=Passwort vergessen';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(38; "E-Mail U PW forgotten Active"; Boolean)
        {
            Caption = 'E-Mail U Password forgotten Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(39; "E-Mail U Password forgotten"; Code[30])
        {
            Caption = 'E-Mail U Password forgotten', Comment = 'de-DE=Passwort vergessen';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(40; "E-Mail C Login"; Code[30])
        {
            Caption = 'E-Mail C Login', Comment = 'de-DE=Zugangsdaten an Kontakt versenden';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(41; "E-Mail C Login Active"; Boolean)
        {
            Caption = 'E-Mail C Login Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(42; "E-Mail U Login"; Code[30])
        {
            Caption = 'E-Mail U Login', Comment = 'de-DE=Zugangsdaten an Benutzer versenden';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(43; "E-Mail U Login Active"; Boolean)
        {
            Caption = 'E-Mail U Login Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(44; "E-Mail C New Message"; Code[30])
        {
            Caption = 'E-Mail C New Message', Comment = 'de-DE=Neue Nachricht in Support Ticket';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(45; "E-Mail C New Message Active"; Boolean)
        {
            Caption = 'E-Mail C New Message Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(46; "E-Mail U New Message"; Code[30])
        {
            Caption = 'E-Mail U New Message', Comment = 'de-DE=Neue Nachricht in Support Ticket';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(47; "E-Mail U New Message Active"; Boolean)
        {
            Caption = 'E-Mail U New Message Active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }
        field(61; "Anonymous Contact Company No."; Code[20])
        {
            Caption = 'Anonymous Contact Company No.', Comment = 'de-DE=Unternehmensnr. Gastformular';
            DataClassification = CustomerContent;
            TableRelation = Contact where(Type = const(Company));
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Support Setup';
        }
        field(62; "Anonymous Contact Company Name"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup(Contact.Name Where("No." = FIELD("Anonymous Contact Company No.")));
            Caption = 'Anonymous Contact Company Name', Comment = 'de-DE=Unternehmensname Gastformular';
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Support Setup';
        }
        field(63; "Link to Anonymous Form"; Text[250])
        {
            Caption = 'Link to Anonymous Form', Comment = 'de-DE=Link zu Gastformular';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Support Setup';
        }
        field(64; "Salutation Code M"; Code[10])
        {
            Caption = 'Salutation Code Male', Comment = 'de-DE=Anredecode männlich';
            DataClassification = CustomerContent;
            TableRelation = Salutation;
        }
        field(65; "Salutation Code F"; Code[10])
        {
            Caption = 'Salutation Code Female', Comment = 'de-DE=Anredecode weiblich';
            DataClassification = CustomerContent;
            TableRelation = Salutation;
        }
        field(66; "Salutation Code D"; Code[10])
        {
            Caption = 'Salutation Code Divers', Comment = 'de-DE=Anredecode divers';
            DataClassification = CustomerContent;
            TableRelation = Salutation;
        }

        field(67; "IMAP Connector URL"; Text[100])
        {
            Caption = 'Link to IMAP Connector', Comment = 'de-DE=Link zu IMAP Connector';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }
        field(68; "IMAP Connector Auth. User"; Text[50])
        {
            Caption = 'IMAP Connector Auth. User', Comment = 'de-DE=IMAP Connector Benutzer';
            DataClassification = CustomerContent;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }
        field(69; "IMAP Connector Auth. Password"; Text[50])
        {
            Caption = 'IMAP Connector Auth. Password', Comment = 'de-DE=IMAP Connector Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }

        field(70; "EWS Connector URL"; Text[100])
        {
            Caption = 'Link to EWS Connector', Comment = 'de-DE=Link zu EWS Connector';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }
        field(71; "EWS Connector Auth. User"; Text[50])
        {
            Caption = 'EWS Connector Auth. User', Comment = 'de-DE=EWS Connector Benutzer';
            DataClassification = CustomerContent;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }
        field(72; "EWS Connector Auth. Password"; Text[50])
        {
            Caption = 'EWS Connector Auth. Password', Comment = 'de-DE=EWS Connector Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Mailrobot Setup';
        }

        field(78; "E-Mail U Escalation"; Code[30])
        {
            Caption = 'E-Mail U Escalation', Comment = 'de-DE=Eskalation';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }

        field(80; "E-Mail U Forwarded Active"; Boolean)
        {
            Caption = 'E-Mail U Forwarded active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }

        field(81; "E-Mail U Forwarded"; Code[30])
        {
            Caption = 'E-Mail U Forwarded', Comment = 'de-DE=Ticket weitergeletet';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(82; "E-Mail C Forwarded Active"; Boolean)
        {
            Caption = 'E-Mail C Forwarded active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }

        field(83; "E-Mail C Forwarded"; Code[30])
        {
            Caption = 'E-Mail C Forwarded', Comment = 'de-DE=Ticket weitergeletet';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(84; "E-Mail C New File Active"; Boolean)
        {
            Caption = 'E-Mail C New File active', Comment = 'de-DE=Aktiv';
            DataClassification = CustomerContent;
        }

        field(85; "E-Mail C New File"; Code[30])
        {
            Caption = 'E-Mail C New File', Comment = 'de-DE=Neue Datei';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(86; "E-Mail Sales Quote"; Code[30])
        {
            Caption = 'E-Mail Requested S. Quote', Comment = 'de-DE=VK-Auftrag angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(87; "E-Mail Sales Order"; Code[30])
        {
            Caption = 'E-Mail Requested S. Order', Comment = 'de-DE=VK-Auftrag angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(88; "E-Mail Sales Invoice"; Code[30])
        {
            Caption = 'E-Mail Requested S. Invoice', Comment = 'de-DE=VK-Rechnung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(89; "E-Mail Sales Cr.Memo"; Code[30])
        {
            Caption = 'E-Mail Requested S. Cr.Memo', Comment = 'de-DE=VK-Gutschrift angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(90; "E-Mail Sales Blanket Order"; Code[30])
        {
            Caption = 'E-Mail Requested S. Blanket Order', Comment = 'de-DE=VK-Rahmenauftrag angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(91; "E-Mail Sales Return Order"; Code[30])
        {
            Caption = 'E-Mail Requested S. Return Order', Comment = 'de-DE=VK-Reklamation angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(92; "E-Mail Pst. Sales Shipment"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(93; "E-Mail Pst. Sales Invoice"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(94; "E-Mail Pst. Sales Cr.Memo"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Sales Cr.Memo', Comment = 'de-DE=Geb. Gutschrift angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(95; "E-Mail Service Quote"; Code[30])
        {
            Caption = 'E-Mail Requested Service Quote', Comment = 'de-DE=Serviceangebot angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(96; "E-Mail Service Order"; Code[30])
        {
            Caption = 'E-Mail Requested Service Order', Comment = 'de-DE=Serviceauftrag angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(97; "E-Mail Service Invoice"; Code[30])
        {
            Caption = 'E-Mail Requested Service Invoice', Comment = 'de-DE=Servicerechnung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(98; "E-Mail Service Cr.Memo"; Code[30])
        {
            Caption = 'E-Mail Requested Service Credit Memo', Comment = 'de-DE=Servicegutschrift angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(99; "E-Mail Posted Service Shipment"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Service Shipment', Comment = 'de-DE=Geb. Servicelieferung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(100; "E-Mail Posted Service Invoice"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Service Invoice', Comment = 'de-DE=Geb. Servicerechnung angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(101; "E-Mail Posted Service Cr.Memo"; Code[30])
        {
            Caption = 'E-Mail Requested Posted Service Credit Memo', Comment = 'de-DE=Geb. Servicegutschrift angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(102; "E-Mail Service Contract"; Code[30])
        {
            Caption = 'E-Mail Requested Service Contract', Comment = 'de-DE=Servicevertrag angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(103; "E-Mail Service Contract Quote"; Code[30])
        {
            Caption = 'E-Mail Requested Service Contract Quote', Comment = 'de-DE=Servicevertragsangebot angefordert';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(104; "E-Mail C RMA"; Code[30])
        {
            Caption = 'E-Mail C RMA', Comment = 'de-DE=RMA-Formular';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
    }
    keys
    {
        key(PK; "Code")
        {
            Clustered = true;
        }
    }

}
