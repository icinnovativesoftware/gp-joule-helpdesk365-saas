page 56322 "ICI Support Time Log Comments"
{

    Caption = 'ICI Support Time Log Comments', Comment = 'de-DE=Kommentare';
    PageType = List;
    SourceTable = "ICI Support Time Log Comment";
    AutoSplitKey = true;
    UsageCategory = None;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Support Ticket No."; Rec."Support Ticket No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
                    Visible = false;
                }
                field("Time Log Line No."; Rec."Time Log Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Time Log Line No.', Comment = 'de-DE=Zeiterfassungszeilennr.';
                    Visible = false;
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Line No', Comment = 'de-DE=Zeilennr.';
                    Visible = false;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Create Date"; Rec."Create Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Create Date', Comment = 'de-DE=Erstelldatum';
                }
                field("Employee No."; Rec."Employee No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Employee No.', Comment = 'de-DE=Mitarbeiternr.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    var
        ICISupportUser: Record "ICI Support User";
    begin
        Rec.FilterGroup(0); // From Lookup Flowfield or Runpagelink in Button
        IF Rec.GetFilter("Support Ticket No.") <> '' then
            TicketNoFilter := COPYSTR(Rec.GetFilter("Support Ticket No."), 1, 20);

        IF Rec.GetFilter("Time Log Line No.") <> '' then
            EVALUATE(TimeLogLineNoFilter, Rec.GetFilter("Time Log Line No."));

        Rec.VALIDATE("Support Ticket No.", TicketNoFilter);
        Rec.Validate("Time Log Line No.", TimeLogLineNoFilter);

        ICISupportUser.GetCurrUser(ICISupportUser);

        Rec.Validate("Employee No.", ICISupportUser."Employee No.");
        Rec.Validate("Create Date", Today());
    END;

    var
        TicketNoFilter: Code[20];
        TimeLogLineNoFilter: Integer;


}
