page 56362 "ICI Service Item Dragbox"
{

    Caption = 'Service Item Dragbox', Comment = 'de-DE=Dragbox';
    SourceTable = "Service Item";
    PageType = CardPart;
    layout
    {
        area(content)
        {
            usercontrol(Dragbox; "ICI Sup. Dragbox")
            {
                ApplicationArea = All;

                trigger DeleteFile(AddinData: JsonObject)
                var
                    ID: Integer;
                    jToken: JsonToken;
                    jVal: JsonValue;
                begin
                    // get Parameters from JSON
                    AddinData.Get('ID', jToken);
                    jVal := jToken.AsValue();
                    ID := jVal.AsInteger();

                    GlobRecordRef.GetTable(Rec);
                    ICISupDragboxMgt.setRecordRef(GlobRecordRef);
                    ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);
                    ICISupDragboxMgt.DeleteFile(ID);

                    CurrPage.UPDATE(FALSE);
                end;

                trigger InsertFile(Data: JsonObject)
                var
                    b64: Text;
                    filename: Text;
                    fileSize: Integer;
                    jToken: JsonToken;
                    jVal: JsonValue;
                begin
                    // get Parameters from JSON
                    Data.Get('b64', jToken);
                    jVal := jToken.AsValue();
                    b64 := jVal.AsText();

                    Data.Get('filename', jToken);
                    jVal := jToken.AsValue();
                    filename := jVal.AsText();

                    Data.Get('fileSize', jToken);
                    jVal := jToken.AsValue();
                    fileSize := jVal.AsInteger();

                    // decode B64 and save The File
                    GlobRecordRef.GetTable(Rec);
                    ICISupDragboxMgt.setRecordRef(GlobRecordRef);
                    ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);
                    ICISupDragboxMgt.InsertFile(filename, b64, fileSize, true);

                    CurrPage.Update(FALSE);
                end;

                trigger DownloadFile(Data: JsonObject)
                var
                    jToken: JsonToken;
                    jVal: JsonValue;
                    ID: Integer;
                    SendData: JsonObject;
                begin
                    // get Parameters from JSON
                    Data.Get('ID', jToken);
                    jVal := jToken.AsValue();
                    ID := jVal.AsInteger();

                    // Trigger Download
                    GlobRecordRef.GetTable(Rec);
                    ICISupDragboxMgt.setRecordRef(GlobRecordRef);
                    ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);
                    SendData := ICISupDragboxMgt.PrepareDownloadFile(ID);
                    CurrPage.Dragbox.SendFileToClient(SendData);
                end;

                trigger DragboxReady()
                begin
                    DragboxIsReady := TRUE;
                    Rec.Mark(TRUE);
                    Rec.MarkedOnly(TRUE);
                    IF Rec.Count > 0 then
                        loadFiles();

                    Rec.ClearMarks();
                    Rec.MarkedOnly(FALSE);
                end;

                trigger HandleMailAttachments(Data: JsonObject; MailInfo: JsonObject)
                var
                    TempICISupDragboxSelection: Record "ICI Sup. Dragbox Selection" temporary;
                    DragboxSelectionPage: Page "ICI Sup. Dragbox Select. List";
                    attachmentsToken: JsonToken;
                    attachmentObj: JsonObject;
                    jVal: JsonValue;
                    jTok: JsonToken;
                    i: Integer;
                    filesize: Integer;
                    fileName: Text;
                    DataUri: Text;
                    mimeType: Text;
                    b64: Text;
                    oStream: OutStream;
                    iStream: InStream;
                    ConfirmTxt: Label 'We detected an E-Mail file with Attachments. Do you want to add any of them to the Dragbox?';
                begin

                    if Data.Keys.Count > 0 THEN BEGIN // No. of Attachments > 0
                        if not Confirm(ConfirmTxt) THEN EXIT;
                        for i := 0 to (Data.Keys.Count - 1) DO BEGIN
                            Data.get(FORMAT(i), attachmentsToken);

                            attachmentObj := attachmentsToken.AsObject();

                            // get Attachment Name
                            IF attachmentObj.Get('filename', jTok) THEN begin
                                jVal := jTok.AsValue();
                                fileName := jVal.AsText();
                            end;

                            CLEAR(jTok);
                            Clear(jVal);
                            // Get Attachment B64 DataUrl
                            IF attachmentObj.Get('DataURI', jTok) THEN BEGIN
                                jVal := jTok.AsValue();
                                DataUri := jVal.AsText();
                            END;

                            CLEAR(jTok);
                            Clear(jVal);
                            // Get Filesize
                            IF attachmentObj.Get('fileSize', jTok) THEN BEGIN
                                jVal := jTok.AsValue();
                                filesize := jVal.AsInteger();
                            END;

                            CLEAR(jTok);
                            Clear(jVal);
                            IF attachmentObj.Get('mimetype', jTok) THEN BEGIN
                                jVal := jTok.AsValue();
                                mimeType := jVal.AsText();
                            END;

                            Clear(TempICISupDragboxSelection);
                            // Insert to Temp Record
                            TempICISupDragboxSelection."Entry No." := i;
                            TempICISupDragboxSelection.Selected := TRUE;
                            TempICISupDragboxSelection.FileName := CopySTR(fileName, 1, MAXSTRLEN(TempICISupDragboxSelection.FileName));
                            TempICISupDragboxSelection.Size := filesize;

                            TempICISupDragboxSelection.CalcFields(Content);
                            TempICISupDragboxSelection.Content.CreateOutStream(oStream);
                            oStream.WriteText(DataUri);

                            TempICISupDragboxSelection.Insert(true);

                        END;
                        // Prepare and Run Selection Page
                        GlobRecordRef.GetTable(Rec);
                        DragboxSelectionPage.setRecordRef(GlobRecordRef);
                        DragboxSelectionPage.SetTemporaryRecord(TempICISupDragboxSelection);
                        DragboxSelectionPage.setConfigurationCode(DragboxConfigurationCode);
                        DragboxSelectionPage.LookupMode(true);
                        DragboxSelectionPage.Editable := true;

                        if DragboxSelectionPage.RunModal() = Action::LookupOK then begin
                            DragboxSelectionPage.GetRecord(TempICISupDragboxSelection);
                            TempICISupDragboxSelection.SetRange(Selected, TRUE);

                            IF TempICISupDragboxSelection.FINDSET() THEN
                                REPEAT
                                    TempICISupDragboxSelection.CalcFields(Content);
                                    TempICISupDragboxSelection.Content.CreateInStream(iStream);
                                    iStream.ReadText(b64);

                                    ICISupDragboxMgt.setRecordRef(GlobRecordRef);
                                    ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);
                                    ICISupDragboxMgt.InsertFile(TempICISupDragboxSelection.FileName, b64, TempICISupDragboxSelection.Size, true);

                                UNTIL TempICISupDragboxSelection.NEXT() = 0;
                        end;
                    END;
                end;

                trigger RaiseError(ErrorInformation: JsonObject)
                var
                    jVal: JsonValue;
                    jTok: JsonToken;
                    ErrorID: Integer;
                begin

                    ErrorInformation.GET('ErrorID', jTok);
                    jVal := jTok.AsValue();
                    ErrorID := jVal.AsInteger();
                    ICISupDragboxMgt.ShowDragboxMessage(ErrorID);
                end;
            }


        }
    }

    actions
    {
        area(Processing)
        {
            action(Download)
            {
                ApplicationArea = All;
                Image = Save;
                Caption = 'Download', Comment = 'de-DE=Herunterladen';
                ToolTip = 'Download the selected File(s) to the Computer', Comment = 'de-DE=Läd die ausgewählten Dateien herunter';
                trigger OnAction();
                begin
                    CurrPage.Dragbox.DownloadSelectedElements();
                end;
            }
            action(Delete)
            {
                ApplicationArea = All;
                Image = Delete;
                Caption = 'Delete', Comment = 'de-DE=Löschen';
                ToolTip = 'Delete the selected File(s)', Comment = 'de-DE=Löscht die ausgewählten Dateien';

                trigger OnAction();
                begin
                    CurrPage.Dragbox.DeleteSelectedElements();
                end;
            }
            action(Config)
            {
                ApplicationArea = All;
                Image = Setup;
                Caption = 'Open Configuration', Comment = 'de-DE=Konfiguration';
                ToolTip = 'Change the Dragbox Settings for this Dragbox', Comment = 'de-DE=Öffnet die Konfiguration dieser Dragbox';

                trigger OnAction();
                begin
                    IF Page.RUNMODAL(PAGE::"ICI Sup. Config. Card", ICISupDragboxConfig) = Action::OK then
                        CurrPage.UPDATE(false);
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        DragboxConfigurationCode := COPYSTR(Rec.TABLENAME(), 1, 30);
        IF ICISupDragboxSetup.GET() THEN
            IF NOT ICISupDragboxConfig.GET(DragboxConfigurationCode) THEN BEGIN
                ICISupDragboxConfig.INIT();
                ICISupDragboxConfig.VALIDATE(Code, DragboxConfigurationCode);
                ICISupDragboxConfig.VALIDATE("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::Ticket);
                ICISupDragboxConfig.INSERT();
                ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);
            END;
    end;


    trigger OnAfterGetRecord()
    begin
        if DragboxIsReady then
            loadFiles();
    end;

    trigger OnAfterGetCurrRecord()
    begin
        if DragboxIsReady then
            loadFiles();
    end;

    procedure loadFiles()
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        FileRecordRef: RecordRef;
        lFieldRef: FieldRef;
        IDFieldRef: FieldRef;
        FileNameFieldRef: FieldRef;
        ShownFilenameFieldRef: FieldRef;
        CreatedByFieldRef: FieldRef;
        CreatedOnFieldRef: FieldRef;
        FileSizeFieldRef: FieldRef;
        ID: Text;
        FileName: Text;
        ShownFilename: Text;
        SendData: JsonObject;
        CreatedByName: Text;
        CreatedOn: DateTime;
        FileSize: Text;
    begin
        if ICISupDragboxSetup.GET() THEN
            IF ICISupDragboxSetup."Max. Upload Size" > 0 THEN BEGIN
                CLEAR(SendData);
                SendData.Add('LicenseText', ICISupDragboxMgt.GetLicenseLabel());
                SendData.Add('maxSize', ICISupDragboxSetup."Max. Upload Size");
                SendData.Add('viewType', ICISupDragboxConfig."View Type");
                SendData.Add('outlookIntegration', ICISupDragboxConfig."Outlook Mail Integration");
                CurrPage.Dragbox.InitDragboxValues(SendData);
            END;

        CurrPage.Dragbox.ClearList();
        IF ICISupDragboxConfig."Dragbox Type" = ICISupDragboxConfig."Dragbox Type"::Ticket THEN BEGIN
            // Ticket ´
            ICISupportTicketLog.SetCurrentKey("Record ID");
            ICISupportTicketLog.SetRange("Record ID", Rec.RecordId);
            ICISupportTicketLog.SetFilter(Type, '%1|%2', ICISupportTicketLog.Type::"External File", ICISupportTicketLog.Type::"Internal File");

            IF ICISupportTicketLog.FindSet() THEN
                repeat
                    FileName := ICISupportTicketLog."Data Text";
                    IF ICISupportTicketLog."Data Text 2" <> '' THEN
                        ShownFilename := ICISupportTicketLog."Data Text 2" // Renamed Filename
                    else
                        ShownFilename := ICISupportTicketLog."Data Text";
                    ID := FORMAT(ICISupportTicketLog."Entry No.");
                    CreatedByName := ICISupportTicketLogMgt.GetTicketLogSenderName(ICISupportTicketLog."Entry No.");
                    CreatedOn := ICISupportTicketLog."Creation Timestamp";
                    FileSize := ICISupportTicketLog."Additional Text";

                    CLEAR(SendData);
                    SendData.Add('id', ID);
                    SendData.Add('filename', FileName);
                    SendData.Add('showFilename', ShownFilename);
                    SendData.Add('createdBy', CreatedByName);
                    SendData.Add('createdOn', FORMAT(CreatedOn));
                    SendData.Add('fileSize', FORMAT(FileSize));

                    CurrPage.Dragbox.AddFileToList(SendData);
                until ICISupportTicketLog.Next() = 0;

        END ELSE BEGIN
            // Dragbox Std.
            GlobRecordRef.GetTable(Rec);
            ICISupDragboxMgt.setRecordRef(GlobRecordRef);
            ICISupDragboxMgt.setConfiguration(DragboxConfigurationCode);

            FileRecordRef.OPEN(ICISupDragboxMgt.GetFileTableNo());
            lFieldRef := FileRecordRef.FIELD(3); // Record ID
            lFieldRef.SetRange(Rec.RecordId());

            FileNameFieldRef := FileRecordRef.FIELD(10); //  10 = FileName
            ShownFilenameFieldRef := FileRecordRef.FIELD(14); //  14 = Shown FileName
            IDFieldRef := FileRecordRef.FIELD(1); //  1 = ID
            CreatedByFieldRef := FileRecordRef.FIELD(12); // 12 = Created By
            CreatedOnFieldRef := FileRecordRef.FIELD(13); // 13 = Created On
            FileSizeFieldRef := FileRecordRef.FIELD(15); // 15 = Filesize

            IF FileRecordRef.FindSet(false) THEN
                REPEAT
                    FileName := FileNameFieldRef.VALUE; //  10 = FileName
                    ShownFilename := ShownFilenameFieldRef.VALUE; //  14 = Shown FileName
                    ID := IDFieldRef.Value; //  1 = ID
                    CreatedByName := CreatedByFieldRef.Value; // 12 = Created By
                    CreatedOn := CreatedOnFieldRef.Value; // 13 = Created On
                    FileSize := FileSizeFieldRef.Value; // 15 = Filesize

                    CLEAR(SendData);
                    SendData.Add('id', ID);
                    SendData.Add('filename', FileName);
                    SendData.Add('showFilename', ShownFilename);
                    SendData.Add('createdBy', CreatedByName);
                    SendData.Add('createdOn', FORMAT(CreatedOn));
                    SendData.Add('fileSize', FORMAT(FileSize));

                    CurrPage.Dragbox.AddFileToList(SendData);
                UNTIL FileRecordRef.NEXT() = 0;

        END;
        COMMIT();

    end;

    var
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        ICISupDragboxSetup: Record "ICI Sup. Dragbox Setup";
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        GlobRecordRef: RecordRef;
        DragboxConfigurationCode: Code[30];
        DragboxIsReady: Boolean;
}
