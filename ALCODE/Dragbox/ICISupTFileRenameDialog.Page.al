page 56314 "ICI Sup. T. File Rename Dialog"
{
    Caption = 'ICI Support Ticket File Rename Dialog', Comment = 'de-DE=Datei umbenennen';
    PageType = ConfirmationDialog;
    SourceTable = "ICI SUpport Ticket Log";

    layout
    {
        area(content)
        {
            field(Filename; Rec."Data Text")
            {
                ApplicationArea = All;
                Editable = False;
                ToolTip = 'Shows the original Filename', Comment = 'de-DE=Ursprünglicher Dateiname';
                Caption = 'Original Filename', Comment = 'de-DE=Ursprünglicher Dateiname';

            }

            field("Shown Filename"; Rec."Data Text 2")
            {
                ApplicationArea = All;
                ToolTip = 'Input the new Filename here', Comment = 'de-DE=Den neuen Dateinamen hier eingeben';
                Caption = 'New File Name', Comment = 'de-DE=Neuer Dateiname';
            }
        }
    }
}
