table 56290 "ICI Sup. Dragbox Setup"
{
    Caption = 'Support Dragbox Setup', Comment = 'de-DE=Support Dragbox Einrichtung';
    fields
    {
        field(1; "Code"; Code[10])
        {
            DataClassification = SystemMetadata;
            Caption = 'Code', Comment = 'de-DE=Code';
        }

        field(23; "Max. Upload Size"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Max. Upload Size', Comment = 'de-DE=Maximale Dateigröße';
            InitValue = 10;
        }
    }
    keys
    {
        key(Key1; "Code")
        {
        }
    }
}
