function AddFileToList(Data){
    $( document ).ready(function() {
        //debugger;
        //console.log(Data);
        addFileToDragbox(Data.id,Data.filename,Data.showFilename,Data.createdOn,Data.createdBy,Data.fileSize);
        $('#Loader')[0].classList.remove('loader');
    });
}
function  DeleteFileFromList(Data){
    alert('DeleteFileFromList');
}
function ClearList(Data){
    $( document ).ready(function() {
        $('#ListArea').empty();
        setupListView();
        //$('#Loader')[0].classList.remove('loader');
    });
}



function InitDragboxValues(Data){
    maxFileSize = Data.maxSize * 1024 * 1024;
    viewType = Data.viewType;
    outlookIntegration = Data.outlookIntegration;
    $("#LicenseSpan").text(Data.LicenseText);
    deactivated = false;
    $('#DropZone').css('background-color','');
    $("#LicenseExpiredSpan").text('');
    setupListView();
}
function setupListView(){
    switch(viewType){
        case 0: setupSimpleList(); break; // Simple List
        case 1: setupDetailedList(); break; // Detailed List
        case 2: setupIconView();break; // Icons
    }
}

function setupSimpleList(){
    listArea = $('#ListArea')[0];
    
    var DetailedListView = document.createElement('ul');
    DetailedListView.id = "SimpleListView";
    listArea.appendChild(DetailedListView);

    var hlDropzone = document.createElement('div');
    hlDropzone.id = "hl-dropzone";
    DetailedListView.appendChild(hlDropzone);

    var hlDropzoneP = document.createElement('p');
    hlDropzoneP.id = "hlDropzoneP";
    hlDropzoneP.innerText = "Datei hier ablegen";
    hlDropzone.appendChild(hlDropzoneP);
}

function setupDetailedList(){
    
    listArea = $('#ListArea')[0];
    
    var DetailedListView = document.createElement('ul');
    DetailedListView.id = "DetailedListView";
    listArea.appendChild(DetailedListView);

    var hlDropzone = document.createElement('div');
    hlDropzone.id = "hl-dropzone";
    DetailedListView.appendChild(hlDropzone);

    var hlDropzoneP = document.createElement('p');
    hlDropzoneP.id = "hlDropzoneP";
    hlDropzoneP.innerText = "Datei hier ablegen";
    hlDropzone.appendChild(hlDropzoneP);
}
function setupIconView(){
    /* Generate this:
    <div id="ListArea">
    <ul id="IconListView">
        <div id="hl-dropzone">
            <p id="hlDropzoneP">Datei hier ablegen</p>
        </div>
    </ul>
    <div class="drag-data-info-wrap" id="DragDataInfoWrapper">
        <div class="drag-data-info" id="DragDataInfo">
            <p id="InformationP">Informationen</p>
            <ul id="InformationUl">
                <li id="DataInfoFilename">
                    <p id="DataInfoFilenameLiP1">Dateiname:</p>
                    <p id="DataInfoFilenameLiP2"></p>
                </li>
                <li id="DataInfoUploadDate">
                    <p id="DataInfoUploadDateP1">Upload-Datum:</p>
                    <p id="DataInfoUploadDateLiP2"></p>
                </li>
                <li id="DataInfoFileSize">
                    <p id="DataInfoFileSizeLiP1">Größe:</p>
                    <p id="DataInfoFileSizeLiP2"></p>
                </li>
            </ul>
        </div>
    </div>
    </div>
*/

    listArea = $('#ListArea')[0];
    
    var IconListView = document.createElement('ul');
    //$(IconListView).addClass('IconListView');
    IconListView.id = "IconListView";
    listArea.appendChild(IconListView);

    var hlDropzone = document.createElement('div');
    hlDropzone.id = "hl-dropzone";
    IconListView.appendChild(hlDropzone);

    var hlDropzoneP = document.createElement('p');
    hlDropzoneP.id = "hlDropzoneP";
    hlDropzoneP.innerText = "Datei hier ablegen";
    hlDropzone.appendChild(hlDropzoneP);

    var DragDataInfoWrapper = document.createElement('div');
    $(DragDataInfoWrapper).addClass('drag-data-info-wrap');
    DragDataInfoWrapper.id = "DragDataInfoWrapper";
    listArea.appendChild(DragDataInfoWrapper);

    var DragDataInfo = document.createElement('div');
    $(DragDataInfo).addClass('drag-data-info');
    DragDataInfo.id = "DragDataInfo";
    DragDataInfoWrapper.appendChild(DragDataInfo);

    var InformationP = document.createElement('p');
    InformationP.innerText = "Informationen";
    InformationP.id = "InformationP";
    DragDataInfo.appendChild(InformationP);

    var InformationUl = document.createElement('ul');
    InformationUl.id = "InformationUl";
    DragDataInfo.appendChild(InformationUl);

    var DataInfoFilenameLi = document.createElement('li');
    DataInfoFilenameLi.id = "DataInfoFilename";
    InformationUl.appendChild(DataInfoFilenameLi);

    // Dateiname
    var DataInfoFilenameLiP1 = document.createElement('p');
    DataInfoFilenameLiP1.innerText = "Dateiname:";
    DataInfoFilenameLiP1.id = "DataInfoFilenameLiP1";
    DataInfoFilenameLi.appendChild(DataInfoFilenameLiP1);
    var DataInfoFilenameLiP2 = document.createElement('p');
    DataInfoFilenameLiP2.innerText = "";
    DataInfoFilenameLiP2.id = "DataInfoFilenameLiP2";
    DataInfoFilenameLi.appendChild(DataInfoFilenameLiP2);

    // Upload Datum
    var DataInfoUploadDateLi = document.createElement('li');
    DataInfoUploadDateLi.id = "DataInfoUploadDate";
    InformationUl.appendChild(DataInfoUploadDateLi);

    var DataInfoUploadDateLiP1 = document.createElement('p');
    DataInfoUploadDateLiP1.innerText = "Upload-Datum:";
    DataInfoUploadDateLiP1.id = "DataInfoUploadDateP1";
    DataInfoUploadDateLi.appendChild(DataInfoUploadDateLiP1);
    var DataInfoUploadDateLiP2 = document.createElement('p');
    DataInfoUploadDateLiP2.innerText = "";
    DataInfoUploadDateLiP2.id = "DataInfoUploadDateLiP2";
    DataInfoUploadDateLi.appendChild(DataInfoUploadDateLiP2);

    // Dateigröße
    var DataInfoFileSizeLi = document.createElement('li');
    DataInfoFileSizeLi.id = "DataInfoFileSize";
    InformationUl.appendChild(DataInfoFileSizeLi);

    var DataInfoFileSizeLiP1 = document.createElement('p');
    DataInfoFileSizeLiP1.innerText = decodeURIComponent("Gr%C3%B6%C3%9Fe:");
    DataInfoFileSizeLiP1.id = "DataInfoFileSizeLiP1";
    DataInfoFileSizeLi.appendChild(DataInfoFileSizeLiP1);
    var DataInfoFileSizeLiP2 = document.createElement('p');
    DataInfoFileSizeLiP2.innerText = "";
    DataInfoFileSizeLiP2.id = "DataInfoFileSizeLiP2";
    DataInfoFileSizeLi.appendChild(DataInfoFileSizeLiP2);
}

function onIconHover(e){
    
    console.log(e);
    console.log($(this));

    filename = $(this).attr("DataInfoFilename");
    uploadDate = $(this).attr("DataInfoUploadDate");
    fileSize = $(this).attr("DataInfoFileSize");

    $('#DataInfoFilenameLiP2').text(filename);
    $('#DataInfoUploadDateLiP2').text(uploadDate);
    $('#DataInfoFileSizeLiP2').text(fileSize);

}
function onIconLeave(e){
    showFileInfo();
}

function showFileInfo(){

    filename = "";
    uploadDate = "";
    fileSize = "";

    // no of Marked Elements = 1 -> set to marked
    var noOfMarkedElements = $('#IconListView .marked').length;
    if( noOfMarkedElements == 1){
        filename = $('#IconListView .marked').attr("DataInfoFilename");
        uploadDate = $('#IconListView .marked').attr("DataInfoUploadDate");
        fileSize = $('#IconListView .marked').attr("DataInfoFileSize");
    } 
    if( noOfMarkedElements > 1){
        filename = "...";
        uploadDate = "...";
        fileSize = "...";
    }

    $('#DataInfoFilenameLiP2').text(filename);
    $('#DataInfoUploadDateLiP2').text(uploadDate);
    $('#DataInfoFileSizeLiP2').text(fileSize);
}

function SendFileToClient(Data){
    // debugger;
    // Anything but IE works here
    if (undefined === window.navigator.msSaveOrOpenBlob) {
        var e = document.createElement('a');
        var href = Data.DataURI;
        e.setAttribute('href', href);
        e.setAttribute('download', Data.filename);
        document.body.appendChild(e);
        e.click();
        document.body.removeChild(e);
    }
    // IE-specific code
    else {
        var binary = atob(Data.DataURI.split(',')[1]);
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        var blob = new Blob([new Uint8Array(array)], {type: Data.DataURI.split(',')[0]});

        window.navigator.msSaveOrOpenBlob(blob, Data.filename);
    }

}

function DownloadSelectedElements(Data){
    $( document ).ready(function() {
        $("li.marked").each(function(index, element) {
            $(this).remove();
            Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DownloadFile', [{ID: element.id }]);
        });
    });
}

function DeleteSelectedElements(Data){
    $( document ).ready(function() {
        $("li.marked").each(function(index, element) {
            $(this).remove();
            Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DeleteFile', [{ID: element.id }]);
        });
    });
}

function SetDragboxDisabled(Data){
    $( document ).ready(function() {
        if (Data.Disable){
            deactivated = true;
            $("#LicenseSpan").text(Data.LicenseText);
            $('#DropZone').css('background-color','lightgray');
            $("#LicenseExpiredSpan").text(Data.message);
        } else{
            deactivated = false;
            $('#DropZone').css('background-color','');
            $("#LicenseExpiredSpan").text('');
        }

    });
}
var outlookIntegration = false;
var maxFileSize = -1;
var viewType = -1;
var folderInputID = "folderInput";
var fileInputID = "fileInputID";
var dropZoneID = "DropZone";
var DELAY = 300,
clicks = 0,
timer = null;
var deactivated = false;

function addFileToDragbox(id,origname,name,createdOn,createdBy,fileSize){
    // debugger;
    switch(viewType){
        case 0:
            addFileToSimpleList(id,origname,name,createdOn,createdBy,fileSize);
            break;
        case 1:
            addFileToDetailedList(id,origname,name,createdOn,createdBy,fileSize);
            break;
        case 2:
            addFileToIconView(id,origname,name,createdOn,createdBy,fileSize);
            break;
        case 3:
            // Nothing
            return;
    }  
}

function addFileToDetailedList(id,origname,name,createdOn,createdBy,fileSize){
    // Soll struktur:
    /*<div class="image">
    <a href="#">8IMmP4.jpg</a>
    <div class="columnDivContainer">
        <div class="column1">10.08.20 09:11</div>
        <div class="column2">856.84 KB</div>
        <div class="column3">MEDERER</div>
    </div>
    </div>*/
    var li = document.createElement("li");
    li.id = id;

    var iconDiv = document.createElement("div");

    var a = document.createElement("a");
    shownText = name;
    if (name.length > 12){
        shownText = name.substring(0, 12) + '...';
    }
    var text = document.createTextNode(shownText);
    a.href = "#";
    a.appendChild(text);
    a.innerText = shownText;

    li.appendChild(iconDiv);
    iconDiv.appendChild(a);
    if($("#DetailedListView").length == 0){
        setupListView();
    }
    $("#DetailedListView")[0].appendChild(li);

    $(li).addClass("DetailedListElement");

    //console.log("pop: "+ origname.split('.').pop().toLocaleLowerCase());
    switch(origname.split('.').pop().toLocaleLowerCase()){
        case 'txt':  
        case 'doc': 
        case 'docx': 
            iconDiv.setAttribute("class", "DetailedListElementDiv document");
            break;
        case 'xls':  
        case 'xlsx': 
        case 'xlsxc': 
            iconDiv.setAttribute("class", "DetailedListElementDiv excel");
            break;
        case 'mpg':  
        case 'mpeg': 
        case 'mp4': 
            iconDiv.setAttribute("class", "DetailedListElementDiv video");
            break;
        case 'pdf':  
            iconDiv.setAttribute("class", "DetailedListElementDiv pdf");
        break;
        case 'mp3':  
        case 'wav': 
            iconDiv.setAttribute("class", "DetailedListElementDiv audio");
            break;
        case 'jpg':  
        case 'gif': 
        case 'png': 
        case 'jpeg':
        case 'bmp':
            iconDiv.setAttribute("class", "DetailedListElementDiv image");
            break;
        case 'zip':
        case 'rar':
            iconDiv.setAttribute("class", "DetailedListElementDiv zip");
            break;
        default: 
            iconDiv.setAttribute("class", "DetailedListElementDiv file");
            break;
    }
    var columnDivContainer = document.createElement("div");
    columnDivContainer.classList = 'columnDivContainer';
    var columnDiv1 = document.createElement("div");
    var columnDiv2 = document.createElement("div");
    var columnDiv3 = document.createElement("div");

    columnDiv1.innerText = createdOn;
    columnDiv2.innerText = formatBytes(fileSize);
    columnDiv3.innerText = createdBy;


    $(li).attr("DataInfoFilename",name);
    $(li).attr("DataInfoFileSize",formatBytes(fileSize));
    $(li).attr("DataInfoUploadDate",createdOn);

    $(li).on("click", liClicked);
    $(li).hover(onIconHover);
    $(li).mouseleave(onIconLeave);

    $(columnDiv1).addClass('column1');
    $(columnDiv2).addClass('column2');
    $(columnDiv3).addClass('column3');

    columnDivContainer.appendChild(columnDiv1);
    columnDivContainer.appendChild(columnDiv2);
    columnDivContainer.appendChild(columnDiv3);
    
    iconDiv.appendChild(columnDivContainer);

}
function addFileToSimpleList(id,origname,name,createdOn,createdBy,fileSize){
    // Soll struktur:
    /*<div class="image">
        <a href="#">8IMmP4.jpg</a>
    </div>*/    
    var li = document.createElement("li");
    li.id = id;

    var iconDiv = document.createElement("div");

    var a = document.createElement("a");
    shownText = name;
    if (name.length > 12){
        shownText = name.substring(0, 24) + '...'; // Simple list offers more space
    }
    var text = document.createTextNode(shownText);
    a.href = "#";
    a.appendChild(text);
    a.innerText = shownText;

    li.appendChild(iconDiv);
    iconDiv.appendChild(a);
    if($("#SimpleListView").length == 0){
        setupListView();
    }
    $("#SimpleListView")[0].appendChild(li);

    $(li).addClass("SimpleListElement");

    //console.log("pop: "+ origname.split('.').pop().toLocaleLowerCase());
    switch(origname.split('.').pop().toLocaleLowerCase()){
        case 'txt':  
        case 'doc': 
        case 'docx': 
            iconDiv.setAttribute("class", "SimpleListElementDiv document");
            break;
        case 'xls':  
        case 'xlsx': 
        case 'xlsxc': 
            iconDiv.setAttribute("class", "SimpleListElementDiv excel");
            break;
        case 'mpg':  
        case 'mpeg': 
        case 'mp4': 
            iconDiv.setAttribute("class", "SimpleListElementDiv video");
            break;
        case 'pdf':  
            iconDiv.setAttribute("class", "SimpleListElementDiv pdf");
        break;
        case 'mp3':  
        case 'wav': 
            iconDiv.setAttribute("class", "SimpleListElementDiv audio");
            break;
        case 'jpg':  
        case 'gif': 
        case 'png': 
        case 'jpeg':
        case 'bmp':
            iconDiv.setAttribute("class", "SimpleListElementDiv image");
            break;
        case 'zip':
        case 'rar':
            iconDiv.setAttribute("class", "SimpleListElementDiv zip");
            break;
        default: 
            iconDiv.setAttribute("class", "SimpleListElementDiv file");
            break;
    }

    $(li).on("click", liClicked);
    $(li).hover(onIconHover);
    $(li).mouseleave(onIconLeave);
}
function addFileToIconView(id,origname,name,createdOn,createdBy,fileSize){
    
    $("#DataInfoWrapper").show();

    var li = document.createElement("li");
    li.id = id;

    var iconDiv = document.createElement("div");

    var a = document.createElement("a");
    shownText = name;
    if (name.length > 12){
        shownText = name.substring(0, 12) + '...';
    }
    var text = document.createTextNode(shownText);
    a.href = "#";
    a.appendChild(text);

    li.appendChild(iconDiv);
    li.appendChild(a);
    if($("#" + dropZoneID + " ul").length == 0){
        return
    }
    $("#" + dropZoneID + " ul")[0].appendChild(li);

    $(li).addClass("iconListElement");

    //console.log("pop: "+ origname.split('.').pop().toLocaleLowerCase());
    switch(origname.split('.').pop().toLocaleLowerCase()){
        case 'txt':  
        case 'doc': 
        case 'docx': 
            iconDiv.setAttribute("id", "documentDiv");
            break;
        case 'xls':  
        case 'xlsx': 
        case 'xlsxc': 
            iconDiv.setAttribute("id", "excelDiv");
            break;
        case 'mpg':  
        case 'mpeg': 
        case 'mp4': 
            iconDiv.setAttribute("id", "videoDiv");
            break;
        case 'pdf':  
            iconDiv.setAttribute("id", "pdfDiv");
        break;
        case 'mp3':  
        case 'wav': 
            iconDiv.setAttribute("id", "audioDiv");
            break;
        case 'jpg':  
        case 'gif': 
        case 'png': 
        case 'jpeg':
        case 'bmp':
            iconDiv.setAttribute("id", "imageDiv");
            break;
        case 'zip':
        case 'rar':
            iconDiv.setAttribute("id", "zipDiv");
            break;
        default: 
            iconDiv.setAttribute("id", "fileDiv");
            break;
    }

    $(li).attr("DataInfoFilename",name);
    $(li).attr("DataInfoFileSize",formatBytes(fileSize));
    $(li).attr("DataInfoUploadDate",createdOn);

    
    $(li).on("click", liClicked);
    $(li).hover(onIconHover);
    $(li).mouseleave(onIconLeave);

}

function liClicked() {
    // Toggle "Marked"

    if ($(this).attr('class').includes("marked")) {
        $(this).removeClass('marked');
    } else {
        $(this).addClass('marked');
    }

    
    clicks++;  //count clicks
    if(clicks === 1) {
        timer = setTimeout(function() {
        clicks = 0;  //after action performed, reset counter
        }, DELAY);
    } else {
        clearTimeout(timer);  //prevent single-click action
        clicks = 0;  //after action performed, reset counter
        //console.log("click " + this.value);
        //console.log("dbclick " + this.innerHTML);
                //Call NAV Download with FILEID
        if (!$(this).attr('class').includes("marked")) {
            $(this).addClass('marked');
        }       
        Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DownloadFile', [{ID: this.id }]);

    } 
  }
$( document ).ready(function() {
    //debugger;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DragboxReady', []);
});

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function parseHeaders(headers) {
    var parsedHeaders = {};
    if (!headers) {
      return parsedHeaders;
    }
    var headerRegEx = /(.*)\: (.*)/g;
    while (m = headerRegEx.exec(headers)) {
      // todo: Pay attention! Header can be presented many times (e.g. Received). Handle it, if needed!
      parsedHeaders[m[1]] = m[2];
    }
    return parsedHeaders;
  }

  function getMsgDate(rawHeaders) {
    // Example for the Date header
    var headers = parseHeaders(rawHeaders);
    if (!headers['Date']){
      return '-';
    }
    return new Date(headers['Date']);
  }