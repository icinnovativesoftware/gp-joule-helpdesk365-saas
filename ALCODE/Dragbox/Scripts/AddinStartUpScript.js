$('#controlAddIn').html(
    '<div id="DropZone">' +
        '<div id="Loader"></div>' +
        '<span id="LicenseSpan"></span>' +
        '<span id="LicenseExpiredSpan"></span>' +
        '<div id="ListArea">' +
            /*'<ul class="list">' +
                '<div class="hl-dropzone" style="text-align: center;"><p style="margin: 5px 0 10px 0; color: #bbb;">Datei hier ablegen</p></div>' +
            '</ul>' +
            '<div id="DataInfoWrapper" style="display:none" class="drag-data-info-wrap">' + 
                '<div class="drag-data-info">' +
                '<p>Informationen</p>' +
                '<ul>' +
                    '<li id="DataInfoFilename"><p>Dateiname:</p><p id="DataInfoFilenameP"></p></li>' +
                    '<li id="DataInfoUploadDate"><p>Upload-Datum:</p><p id="DataInfoUploadDateP"></p></li>' +
                    '<li id="DataInfoFileSize"><p>Dateigröße:</p><p id="DataInfoFileSizeP"></p></li>' +
                '</ul>' +
                '</div>' +
            '</div>' +*/
        '</div>' +
    '</div>' +
    '<input class="inputfile" type="file" multiple id="fileInputID"></input>'
    
    /*'<div id="FolderUpload">' +
        '<input class="inputfile" type="file" multiple webkitdirectory="true" id="folderInput"></input>' +
        '<input class="inputfile" type="file" multiple id="fileInputID"></input>' +   
    '</div>'*/);

function inputChanged(e) {
    var input = e.target;
    var files = input.files;
    var file;
    
    for (var i = 0, len = files.length; i < len; i++) {
        file = files[i];
        if ((!file.type && file.size % 4096 == 0) && (input.id == fileInputID)) {
          console.log("folders not supported");
          Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 3 }]);
          input.value = "";
          return;
        }
        
        if(deactivated){
            // deactivated
            Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 1 }]);
            return;
        }
        else if (file.size == 0){
            //alert('File is empty!');
            Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 2 }]);
            return
        }else {
            // Call NAV File Insert
            //debugger;
            if (outlookIntegration && (file.name.indexOf('.msg') != -1)) {
                handleOutlookMail(file);
                return;
            }
            convertToB64andDownload(file);
        }
    }
}

function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault(); // Necessary. Allows us to drop.
    }
    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    return false;
}

function handleDrop(e) {
    e.stopPropagation(); // Stops some browsers from redirecting.
    e.preventDefault();
    var files = e.dataTransfer.files;
    var items = e.dataTransfer.items;
    for (var i = 0, len = items.length; i < len; i++) {
        // f = files[i];

        /*
        Outlook dragin fügt immer zwei Einträge ein (Text/File) der Text ist hierbei immer die Information aus Outlook
        z.B. Betreff, Absender etc. / für Dragbox nicht relevant
        */
        if (!(items[i].kind === "string")) { 
            item = items[i].webkitGetAsEntry();
            f = items[i].getAsFile()

            if (item.isDirectory) {
                console.log("folders not supported");
                Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 3 }]);
            } else if (item.isFile) {
                if(deactivated){
                    // deactivated
                    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 1 }]);
                    return;
                }
                else if (f.size == 0){
                    //alert('File is empty!');
                    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('RaiseError', [{"ErrorID": 2 }]);
                    return
                }else {
                    // Call NAV File Insert
                    //console.log("outlook mail insert");
                    if (outlookIntegration && f.name.indexOf('.msg') != -1) {
                        handleOutlookMail(f);
                        return;
                    }
                    convertToB64andDownload(f);
                }
                /*document.getElementById(fileInputID).files = e.dataTransfer.files;
                var event = new Event('change');
                document.getElementById(fileInputID).dispatchEvent(event);*/
            }
            
        }
    }
    return false;
}

/*function diableDrop(e) {
    if ((e.target != document.getElementById(folderInputID)) && (e.target != document.getElementById(fileInputID))) {
        e.stopPropagation(); // Stops some browsers from redirecting.
        e.preventDefault();
    }
}*/

//var folderInputID = "folderInput";
var fileInputID = "fileInputID";
var dropZoneID = "DropZone";

//document.getElementById(folderInputID).addEventListener("change", inputChanged, false);
document.getElementById(fileInputID).addEventListener("change", inputChanged, false);
document.getElementById(dropZoneID).addEventListener('dragover', handleDragOver, false);
document.getElementById(dropZoneID).addEventListener('drop', handleDrop, false);
//document.addEventListener('dragover', diableDrop, false);
//document.addEventListener('drop', diableDrop, false);


//var DELAY = 300,
//clicks = 0,
//timer = null;

$(function() {
    $("li").on("click", liClicked);
});

$('html').keyup(function(e) {
    if (e.keyCode == 46) {

        $("li.marked").each(function(index, element) {
            //console.log($(this).text());
            $(this).remove();
            // Call NAV Delete FILE
            Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DeleteFile', [{ID: element.id }]);
        });

    }
});

function convertToB64andDownload(file) {
    

    var reader = new FileReader();

    if ((maxFileSize > 0) && (file.size > maxFileSize)){
        console.log('File too Big allowed Size: ' + maxFileSize + "Bytes, actual Size: " + file.size + "Bytes");
        console.log(file);
        alert('File too Big');
        return;
    }

    reader.readAsDataURL(file);
    reader.onload = function() {
        // Call NAV Insert with B64 Data
        //console.log(reader.result);
        // reader.result.split(',')[1] split the type at the beginning e.g. : "data:image/png;base64,"
        //Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('InsertFile', [{"filename": file.name,"b64":reader.result.split(',')[1]}]);
        //debugger;
        $('#Loader')[0].classList.add('loader');
        Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('InsertFile', [{"filename": file.name,"b64":reader.result,"fileSize":file.size}]);
    };
    reader.onerror = function(error) {
        alert('Error: ' + error);
        console.log('Error: ', error);
    };
}


// MSG FILEHANDLERS
function isSupportedFileAPI() {
    return window.File && window.FileReader && window.FileList && window.Blob;
}


function handleOutlookMail(file){
    if (isSupportedFileAPI() && outlookIntegration) {
        // read file...
        var fileReader = new FileReader();
        fileReader.onload = function (evt) {
            //console.log('FileReader');
            var buffer = evt.target.result;
            var msgReader = new MSGReader(buffer); // use msgReader Api to get E-Mail Contents
            var fileData = msgReader.getFileData();
            if (!fileData.error) {
                //console.log('Filedata');
                //str = JSON.stringify(fileData, null, 4); // (Optional) beautiful indented output.
                //console.log("filedata str:"+str); // Logs output to dev tools console.

                var files = {};
                jQuery.map(fileData.attachments, function (attachment, i) {
                    // Add Attachments to fileObj and send them to NAV
                    var file = msgReader.getAttachment(i);

                    var AttachmentFile;
                    //console.log(attachment);
                    var mimeType = attachment.mimeType ? attachment.mimeType : "application/octet-stream";
                    if (!navigator.msSaveBlob) { // detect if not Edge
                        AttachmentFile = new File([file.content], attachment.fileName, { type: mimeType });
                     } else {
                         // Edge does not have File Constructor
                        AttachmentFile = new Blob([file.content], { type: mimeType });
                        AttachmentFile.lastModifiedDate = new Date();
                        AttachmentFile.name = attachment.fileName;
                     }
                    
                    //console.log("fileUri :"+btoa(String.fromCharCode.apply(null, new Uint8Array(file.content))));
                
                    fileObj = {
                        "filename" : attachment.fileName,
                        "mimetype" : mimeType,
                        "fileSize" : attachment.contentLength,
                        //"DataURI": "data:" + mimeType + ";base64," + btoa(String.fromCharCode.apply(null, new Uint8Array(file.content)))
                        //"DataURI": btoa(String.fromCharCode.apply(null, new Uint8Array(file.content)))
                        "DataURI": "data:" + mimeType + ";base64," + btoa(new Uint8Array(file.content).reduce(function (data, byte) {
                            return data + String.fromCharCode(byte);
                        }, ''))            
                    };
                    
                    //SendFileToClient(fileObj);
                    files[i] = fileObj;
                    
                });
                //str = JSON.stringify(files, null, 4); // (Optional) beautiful indented output.
                //console.log("files str:"+str);

                // Send Mail Attachments to NAV
                mailInfo = {
                    "name": fileData.senderName,
                    "email": fileData.senderEmail,
                    "recipients": fileData.recipients,
                    "msg-date":getMsgDate(fileData.headers),
                    "subject": fileData.subject.replace(/(?:\r\n\r\n)/g, '<br>'), // see https://stackoverflow.com/questions/784539/how-do-i-replace-all-line-breaks-in-a-string-with-br-elements
                    "body": fileData.body.replace(/(?:\r\n\r\n)/g, '<br>') // linebreaks from msgreader = \r\n\r\n - i dont know why
                };
                
                //debugger;
                addMsgFileToSelectionAndSendToNav(files,file,fileData.attachments.length,mailInfo);
                

            } else {
                console.log("error while reading mail");
            }
        };
        fileReader.readAsArrayBuffer(file);
    }
  }

function addMsgFileToSelectionAndSendToNav(filesArr,file,idx,mailInfo){
    var reader = new FileReader();

    if ((maxFileSize > 0) && (file.size > maxFileSize)){
        console.log('File too Big allowed Size: ' + maxFileSize + "Bytes, actual Size: " + file.size + "Bytes");
        console.log(file);
        //alert('File too Big');
        return;
    }

    reader.readAsDataURL(file);
    reader.onload = function() {
        // Call NAV Insert with B64 Data
        
        fileObj = {
            "filename" : file.name,
            "mimetype" : reader.result.split(',')[0],
            "fileSize" : file.size,
            "DataURI": reader.result
        };
        debugger;

        filesArr[idx] = fileObj;
        Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('HandleMailAttachments', [filesArr,mailInfo]);
    };
    reader.onerror = function(error) {
        alert('Error: ' + error);
        console.log('Error: ', error);
    };
}