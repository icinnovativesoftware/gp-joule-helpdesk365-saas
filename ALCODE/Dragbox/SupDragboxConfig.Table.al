table 56291 "ICI Sup. Dragbox Config."
{
    Caption = 'Dragbox Configuration', Comment = 'de-DE=Support Dragbox Konfiguration';

    fields
    {
        field(1; "Code"; Code[30])
        {
            DataClassification = SystemMetadata;
            Caption = 'Code', Comment = 'de-DE=Code';
        }
        field(10; Description; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
        }
        field(11; "Log active"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Log active', Comment = 'de-DE=Protokollierung aktiv';
        }
        field(12; "View Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'View Type', Comment = 'de-DE=Ansichtsart';
            OptionMembers = "Simple List","Detailed List","Small Icons","None";
            OptionCaption = 'Simple List,Detailed List,Small Icons,None', Comment = 'de-DE=Einfache Liste,Detailierte Liste,Icons,Keine';
            InitValue = "Simple List";
        }
        field(13; "Rename on Upload"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Rename on Upload', Comment = 'de-DE=Umbenennen beim Hochladen';
        }

        field(14; "Dragbox Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Dragbox Type', Comment = 'de-DE=Dragbox Art';
            OptionMembers = "Store in Database","Activity","Record Link","One Drive",Ticket;
            OptionCaption = 'Store in Database,Activity,Record Link,One Drive,Ticket', Comment = 'de-DE=In Datenbank Speichern,Aktivität,Record Link,One Drive,Ticket';
            InitValue = "Ticket";
        }
        field(15; "Outlook Mail Integration"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Outlook Mail Integration', Comment = 'de-DE=Outlook Integration';
        }
        field(16; "Delete Allowed"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Delete Allowed', Comment = 'de-DE=Löschen erlaubt';
        }
        field(50000; "ICI Ticket Notification Dialog"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Ticket Notification Dialog', Comment = 'de-DE=Datei in "Nachricht Erfassen" Fenster öffnen';
        }

    }

    keys
    {
        key(Key1; "Code")
        {
        }
    }

    fieldgroups
    {
    }
}
