page 56304 "ICI Sup. Dragbox Setup"
{
    PageType = Card;
    SourceTable = "ICI Sup. Dragbox Setup";
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Support Dragbox Setup', Comment = 'de-DE=Support Dragbox Einrichtung';
    DeleteAllowed = false;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("Max. Upload Size"; Rec."Max. Upload Size")
                {
                    ApplicationArea = All;
                    ToolTip = 'The Maximum Filesize for Dragbox Files', Comment = 'de-DE=Die maximale Dateigröße für Dragbox Dateien in MB';
                }
            }
        }
    }
    actions
    {
        area(Navigation)
        {
            action(Config)
            {
                ApplicationArea = All;
                Image = Setup;
                Caption = 'Configurations', Comment = 'de-DE=Konfigurationen';
                ToolTip = 'Shows the Dragbox Configurations', Comment = 'de-DE=Konfigurationen';

                trigger OnAction()
                begin
                    PAGE.Run(PAGE::"ICI Sup. Dragbox Config. List");
                end;
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupDragboxSetup: Record "ICI Sup. Dragbox Setup";
    begin
        if not ICISupDragboxSetup.GET() THEN
            ICISupDragboxSetup.INSERT(true);
    end;
}
