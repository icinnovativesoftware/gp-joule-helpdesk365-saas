page 56306 "ICI Sup. Dragbox Files Subpage"
{
    PageType = ListPart;
    SourceTable = "ICI Sup. Dragbox File";
    InsertAllowed = false;
    Caption = 'Dragbox Files', Comment = 'de-DE=Dateien';
    layout
    {
        area(content)
        {

            repeater(Group)
            {

                field("Shown Filename"; Rec."Shown Filename")
                {
                    ApplicationArea = All;
                    ToolTip = 'The Filename that is shown in the Dragbox Add-In', Comment = 'de-DE=Angezeigter Name';
                }

                field("Created By"; Rec."Created By")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'The User that Created the Record', Comment = 'de-DE=Erstellt von Benutzer';
                }
                field("Created On"; Rec."Created On")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the Date that the Record was created on', Comment = 'de-DE=Erstellt am';
                }
                field("Dragbox Configuration Code"; Rec."Dragbox Configuration Code")
                {
                    ApplicationArea = All;
                    Editable = False;
                    Visible = false;
                    ToolTip = 'The Configuration Code of this Dragbox', Comment = 'de-DE=Support Dragbox Konfigurationscode';
                }
                field(Filename; Rec.Filename)
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'The original Filename', Comment = 'de-DE=Originaler Dateiname';
                }
                field(Size; Rec.Size)
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'The Filesize', Comment = 'de-DE=Dateigröße';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Rename)
            {
                ApplicationArea = All;
                Image = Edit;
                Caption = 'Rename', Comment = 'de-DE=Beschreibung ändern';
                ToolTip = 'Rename', Comment = 'de-DE=Beschreibung ändern';

                trigger OnAction()
                var
                    SupportFile: Record "ICI Sup. Dragbox File";
                    ICIDragboxFileRenameDialog: Page "ICI Dragbox File Rename Dialog";
                begin
                    SupportFile.GET(Rec."Entry No.");
                    SupportFile.SetRecFilter();
                    ICIDragboxFileRenameDialog.SetRecord(SupportFile);
                    ICIDragboxFileRenameDialog.Run();
                end;
            }
        }
    }
}
