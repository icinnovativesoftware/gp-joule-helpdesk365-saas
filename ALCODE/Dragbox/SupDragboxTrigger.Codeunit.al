codeunit 56286 "ICI Sup. Dragbox Trigger"
{
    EventSubscriberInstance = StaticAutomatic;

    Permissions = TableData "ICI Sup. Dragbox Setup" = RIMD, TableData "ICI Support User" = rimd;


    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"System Initialization", 'OnAfterLogin', '', true, true)]
    local procedure OnAfterLogin()
    var
        // ICISupportUser: Record "ICI Support User";
        // UserSetup: Record "User Setup";
        ICISupDragboxSetup: Record "ICI Sup. Dragbox Setup";
    begin
        // IF NOT ICISupportUser.GetUser(ICISupportUser) then
        //     IF NOT UserSetup.GET(UserId()) THEN begin
        //         UserSetup.Init();
        //         UserSetup.Validate("User ID", UserID());
        //         UserSetup.INSERT(TRUE);
        //     END;
        if ICISupDragboxSetup.READPERMISSION THEN
            if not ICISupDragboxSetup.GET() THEN
                ICISupDragboxSetup.INSERT(true);

        // IF (UserID() <> '') then
        //     if ICISupportUser.READPERMISSION THEN
        //         IF NOT ICISupportUser.GetUser(ICISupportUser) then begin
        //             ICISupportUser.INIT();
        //             ICISupportUser.VALIDATE("User ID", UserID());
        //             ICISupportUser.Insert(TRUE);
        //         end;
    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"ICI Sup. Dragbox Mgt.", 'onAfterInsertFile', '', true, true)]
    local procedure RenameOnAfterInsertFile(filename: Text; var B64: Text; var pRecordRef: RecordRef)
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        ICISupDragboxRenameDialog: Page "ICI Dragbox File Rename Dialog";
    begin
        if pRecordRef.Number <> Database::"ICI Sup. Dragbox File" THEN
            EXIT;

        pRecordRef.SetTable(ICISupDragboxFile);
        ICISupDragboxConfig.GET(ICISupDragboxFile."Dragbox Configuration Code");
        IF ICISupDragboxConfig."Rename on Upload" THEN
            IF GuiAllowed THEN begin
                ICISupDragboxRenameDialog.SetRecord(ICISupDragboxFile);
                ICISupDragboxRenameDialog.Run();
                //Filerec.VALIDATE("Shown Filename", newFileName);
                //FileRec.Modify(TRUE);
            end;

    end;

    [EventSubscriber(ObjectType::"Codeunit", Codeunit::"ICI Sup. Dragbox Mgt.", 'onAfterInsertFile', '', true, true)]
    local procedure RenameOnAfterInsertTicketFile(filename: Text; var B64: Text; var pRecordRef: RecordRef)
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        ICISupTFileRenameDialog: Page "ICI Sup. T. File Rename Dialog";
    begin
        if pRecordRef.Number <> Database::"ICI Support Ticket Log" THEN
            EXIT;

        pRecordRef.SetTable(ICISupportTicketLog);
        ICISupDragboxConfig.GET('ICI SUPPORT TICKET');
        IF ICISupDragboxConfig."Rename on Upload" THEN
            IF GuiAllowed THEN begin
                ICISupTFileRenameDialog.SetRecord(ICISupportTicketLog);
                ICISupTFileRenameDialog.Run();
                //Filerec.VALIDATE("Shown Filename", newFileName);
                //FileRec.Modify(TRUE);
            end;
    end;

}
