table 56292 "ICI Sup. Dragbox File"
{
    Caption = 'Dragbox Item File', Comment = 'de-DE=Support Dragbox Datei';

    fields
    {
        field(1; "Entry No."; Integer)
        {
            AutoIncrement = true;
            DataClassification = SystemMetadata;
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';

        }
        field(2; "Dragbox Configuration Code"; Code[30])
        {
            DataClassification = SystemMetadata;
            TableRelation = "ICI Sup. Dragbox Config.";
            Caption = 'Dragbox Configuration Code', Comment = 'de-DE=Support Dragbox Konfigurationscode';
        }
        field(3; "Record ID"; RecordId)
        {
            DataClassification = CustomerContent;
            Caption = 'Record ID', Comment = 'de-DE=Datensatz';
        }
        field(10; Filename; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'original Filename', Comment = 'de-DE=Dateiname';
            trigger OnValidate();
            begin
                VALIDATE("Shown Filename", Filename);
            end;
        }
        field(11; Data; Blob)
        {
            DataClassification = CustomerContent;
            Caption = 'Data', Comment = 'de-DE=Daten';
        }

        field(12; "Created By"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = "ICI Support User";
            Caption = 'Created By', Comment = 'de-DE=Erstellt von';
        }
        field(13; "Created On"; DateTime)
        {
            DataClassification = SystemMetadata;
            Caption = 'Created On', Comment = 'de-DE=Erstellt am';
        }
        field(14; "Shown Filename"; Text[250]) { DataClassification = CustomerContent; Caption = 'Filename', Comment = 'de-DE=Beschreibung'; }
        field(15; "Size"; Integer) { DataClassification = CustomerContent; Caption = 'Filesize', Comment = 'de-DE=Größe (KB)'; }

        field(50000; "ICI Download Category Code"; Code[30])
        {
            DataClassification = SystemMetadata;
            Caption = 'Download Category Code', Comment = 'de-DE=Downloadkateogorie';
            TableRelation = "ICI Download Category";
        }


    }

    keys
    {
        key(Key1; "Entry No.")
        {
        }
        key(Key2; "Record ID")
        {
        }
        key(Key3; "Record ID", "Dragbox Configuration Code")
        {
        }
        key(DownloadCenter; "ICI Download Category Code") { }
    }

    fieldgroups
    {
        fieldgroup(Dropdown; "Entry No.", Filename)
        {
            Caption = 'Dropdown';
        }
        fieldgroup(Brick; "Entry No.", Filename)
        {
            Caption = 'Brick';
        }
    }
    trigger OnInsert()
    begin
        Validate("Created On", CurrentDateTime());
    end;

}
