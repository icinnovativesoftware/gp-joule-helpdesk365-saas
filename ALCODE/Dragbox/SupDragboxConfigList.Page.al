page 56305 "ICI Sup. Dragbox Config. List"
{
    PageType = List;
    SourceTable = "ICI Sup. Dragbox Config.";
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Dragbox Configurations', Comment = 'de-DE=Support Dragbox Konfigurationen';
    CardPageId = "ICI Sup. Config. Card";
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Code of the Record', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Description of the Record', Comment = 'de-DE=Beschreibung';
                }
                field("Log active"; Rec."Log active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies if the Log is Active for this Record', Comment = 'de-DE=Protokollierung aktiv';
                }
                field("Rename on Upload"; Rec."Rename on Upload")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies if files that are Dragged into the Dragbox will be prompted for Rename', Comment = 'de-DE=Umbenennen beim Hochladen';
                }
                field("View Type"; Rec."View Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Dragbox View Type', Comment = 'de-DE=Ansicht';
                }

            }
        }
    }
}
