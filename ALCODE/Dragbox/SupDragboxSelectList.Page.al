page 56307 "ICI Sup. Dragbox Select. List"
{
    PageType = List;
    SourceTable = "ICI Sup. Dragbox Selection";
    SourceTableTemporary = true;
    Caption = 'Dragbox Selection List', Comment = 'de-DE=Datei Auswahlliste';
    UsageCategory = None;
    Editable = true;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field(Selected; Rec.Selected)
                {
                    ApplicationArea = All;
                    ToolTip = 'Check if you want to select and save this File', Comment = 'de-DE=Häkchen setzen, wenn diese Datei gespeichert werden soll';

                }
                field(FileName; Rec.FileName)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Filename', Comment = 'de-DE=Dateiname';
                }

                /*
                Maybe use this again with media type in the future
                field(FileContent; Content)
                {
                    ApplicationArea = All;
                    Visible = false;
                    ToolTip = 'Preview of the content if available';

                }*/
            }
        }

    }
    trigger OnInit()
    begin
        CurrPage.Editable := true;
    end;

    trigger OnOpenPage()
    begin
        Rec.COPY(TempGlobICISupDragboxSelection, TRUE);
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        b64: Text;
        lInStream: InStream;
    begin

        Rec.SetRange(Selected, TRUE);
        IF Rec.FINDSET() THEN
            REPEAT
                Rec.CalcFields(Content);
                Rec.Content.CreateInStream(lInStream);
                lInStream.ReadText(b64);

                ICISupDragboxMgt.setRecordRef(GlobRecordRef);
                ICISupDragboxMgt.setConfiguration(ConfigurationCode);
                ICISupDragboxMgt.InsertFile(rec.FileName, b64, Rec.Size, true);

            UNTIL rec.NEXT() = 0;
    end;



    procedure SetTemporaryRecord(var TempICISupDragboxSelection: Record "ICI Sup. Dragbox Selection" temporary)
    begin
        TempGlobICISupDragboxSelection.COPY(TempICISupDragboxSelection, TRUE);
    end;

    procedure SetRecordRef(var newRecordRef: RecordRef)
    begin
        GlobRecordRef := newRecordRef;
    end;

    procedure SetConfigurationCode(newConfigurationCode: Code[30])
    begin
        ConfigurationCode := newConfigurationCode;
    end;

    var
        TempGlobICISupDragboxSelection: Record "ICI Sup. Dragbox Selection" temporary;
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        GlobRecordRef: RecordRef;
        ConfigurationCode: Code[30];
}
