controladdin "ICI Sup. Dragbox"
{


    Stylesheets = 'ALCode/Dragbox/Style/style.css';

    // The Scripts property can reference both external and local scripts.
    Scripts = 'https://code.jquery.com/jquery-2.1.0.min.js',
              'ALCode/Dragbox/Scripts/DataStream.js',
              'ALCode/Dragbox/Scripts/msg.reader.js',
              'ALCode/Dragbox/Scripts/main.js';

    // The StartupScript is a special script that the webclient calls once the page is loaded.
    StartupScript = 'ALCode/Dragbox/Scripts/AddinStartUpScript.js';

    // Images and StyleSheets can be referenced in a similar fashion.

    // The layout properties define how control add-in are displayed on the page.
    VerticalShrink = true;
    VerticalStretch = true;

    HorizontalStretch = true;

    RequestedHeight = 400;
    RequestedWidth = 300;

    // The procedure declarations specify what JavaScript methods could be called from AL.
    // AL -> JS
    procedure AddFileToList(Data: JsonObject);
    procedure DeleteFileFromList(Data: JsonObject);
    procedure ClearList();
    procedure DownloadSelectedElements();
    procedure DeleteSelectedElements();

    procedure InitDragboxValues(Data: JsonObject);

    procedure SendFileToClient(Data: JsonObject);

    procedure SetDragboxDisabled(Data: JsonObject);



    // The event declarations specify what callbacks could be raised from JavaScript by using the webclient API:
    // Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('CountryClicked', [{country: 'M}])
    //event CountryClicked(Country: JsonObject);
    // JS -> AL
    event DeleteFile(Data: JsonObject);
    event InsertFile(Data: JsonObject);
    event DownloadFile(Data: JsonObject);
    event DragboxReady();
    event HandleMailAttachments(Data: JsonObject; MailInfo: JsonObject);
    event RaiseError(Data: JsonObject);

}
