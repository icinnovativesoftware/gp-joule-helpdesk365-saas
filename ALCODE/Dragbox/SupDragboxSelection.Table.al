table 56293 "ICI Sup. Dragbox Selection"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            DataClassification = CustomerContent;
            AutoIncrement = true;

        }
        field(10; FileName; Text[250])
        {
            DataClassification = CustomerContent;
        }
        field(11; Content; Blob)
        {
            DataClassification = CustomerContent;
        }
        field(12; Selected; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(15; Size; Integer)
        {
            DataClassification = CustomerContent;
        }
        field(16; MimeType; Text[250])
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(ID; "Entry No.")
        {
            Clustered = true;
        }
    }
}
