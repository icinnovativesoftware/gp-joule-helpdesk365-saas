codeunit 56287 "ICI Sup. Dragbox Mgt."
{

    trigger OnRun()
    begin
    end;
    // Handles -----------
    procedure InsertFile(filename: text; var b64: text; fileSize: Integer; SendMail: Boolean)
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        //TableNo: integer;
        lRecordRef: RecordRef;
        lOutStream: OutStream;
    begin
        lRecordRef := GlobRecordId.GetRecord();
        ICISupDragboxConfig.GET(GlobCongifCode);
        IF ICISupDragboxConfig."Dragbox Type" = ICISupDragboxConfig."Dragbox Type"::Ticket THEN
            if ICISupDragboxConfig."ICI Ticket Notification Dialog" THEN
                AddSupportTicketFileLogWithDialog(lRecordRef, filename, b64, fileSize, SendMail)
            ELSE
                AddSupportTicketFileLog(lRecordRef, filename, b64, fileSize, SendMail, false)

        else begin

            lRecordRef.GetTable(ICISupDragboxFile);
            onBeforeInsertFile(filename, B64, lRecordRef);

            ICISupDragboxFile.INIT();
            ICISupDragboxFile.VALIDATE(Filename, COPYSTR(filename, 1, 250));
            ICISupDragboxFile.VALIDATE(Size, fileSize);
            ICISupDragboxFile.Data.CreateOutStream(lOutStream, TextEncoding::Windows);
            lOutStream.WriteText(B64);
            ICISupDragboxFile.VALIDATE("Record ID", GlobRecordId);
            ICISupDragboxFile.VALIDATE("Dragbox Configuration Code", GlobCongifCode);
            ICISupDragboxFile.INSERT(TRUE);

            lRecordRef.GetTable(ICISupDragboxFile);
            onAfterInsertFile(filename, B64, lRecordRef);

        end;
    end;

    procedure AddSupportTicketFileLog(lRecordRef: RecordRef; filename: text; var b64: text; fileSize: Integer; SendMail: Boolean; Internally: Boolean)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lOutStream: OutStream;
    begin
        lRecordRef.SetTable(ICISupportTicket);
        ICISupportTicket.GET(ICISupportTicket."No."); // Ooopsies. SetTable is sneaky ~.~
        ICISupportUser.GetCurrUser(ICISupportUser);
        ICISupportTicket.SetModifiedByUser(ICISupportUser."User ID");
        ICISupportTicket.Modify();

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        IF Internally THEN
            ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"Internal File")
        ELSE
            ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External File");
        ICISupportTicketLog.VALIDATE("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketLog.VALIDATE("Record ID", GlobRecordId);
        ICISupportTicketLog.VALIDATE("Data Text", filename);
        ICISupportTicketLog.VALIDATE("Data Text 2", filename);
        ICISupportTicketLog.VALIDATE("Additional Text", FORMAT(fileSize));
        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
        ICISupportTicketLog.VALIDATE("Created By", UserId);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream, TextEncoding::Windows);
        lOutStream.WriteText(B64);
        ICISupportTicketLog.Modify(TRUE);

        clear(lRecordRef);
        lRecordRef.GET(ICISupportTicketLog."Record ID");
        onAfterInsertFile(filename, B64, lRecordRef); // Needed for Rename on Upload function
        IF SendMail THEN
            ICISupportMailMgt.NewSupportFileForContact(ICISupportTicket."No.", ICISupportTicketLog."Entry No.");
    end;

    procedure AddSupportTicketFileLogWithDialog(lRecordRef: RecordRef; filename: text; var b64: text; fileSize: Integer; SendMail: Boolean)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTextModule: Record "ICI Support Text Module";
        Contact: Record Contact;
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lInStream: InStream;
        ContentType: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        b64Text: Text;
    begin
        lRecordRef.SetTable(ICISupportTicket);
        ICISupportTicket.GET(ICISupportTicket."No."); // Ooopsies. SetTable is sneaky ~.~
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICISupportMailSetup.Get();

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail C New File", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        // IF ICISupportMailSetup."Test Mode" then
        //     SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen


        // b64 data looks loke this: data:text/plain;base64,L2h0ZG9jcy9...
        ContentType := ICIMailrobotMgt.Explode(b64, ';'); // data looks loke this:base64,L2h0ZG9jcy9...
        ICIMailrobotMgt.Explode(b64, ',');// data looks loke this: L2h0ZG9jcy9...
        b64Text := b64;

        ContentType := '';
        ICINotificationDialogMgt.AddAttachment(filename, ContentType, b64Text); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure DeleteFile(ID: Integer)
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        lRecordRef: RecordRef;
        ConfirmDeleteLbl: Label 'Do you really want to delete the file "%1"? The File may have already been sent to the Customer as an E-Mail attachment', Comment = '%1=Filename|de-DE=Wollen Sie die Datei "%1" wirklich löschen? Eventuell wurde sie bereits als Mailanhang an den Kunden versendet';
    begin
        ICISupDragboxConfig.GET(GlobCongifCode);
        ICISupDragboxConfig.TestField("Delete Allowed");

        CASE ICISupDragboxConfig."Dragbox Type" of
            ICISupDragboxConfig."Dragbox Type"::"Store in Database":
                IF ICISupDragboxFile.GET(ID) THEN BEGIN
                    lRecordRef.GetTable(ICISupDragboxFile);
                    onBeforeDeleteFile(ID, lRecordRef);
                    ICISupDragboxFile.DELETE(TRUE);
                    onAfterDeleteFile(ID, lRecordRef);
                END;
            ICISupDragboxConfig."Dragbox Type"::Ticket:
                begin
                    ICISupportSetup.GET();
                    ICISupportTicketLog.GET(ID);
                    lRecordRef.GetTable(ICISupportTicketLog);

                    IF ICISupportSetup."Send new File as Attachment" then
                        IF NOT Confirm(ConfirmDeleteLbl, True, ICISupportTicketLog."Data Text") THEN
                            exit;
                    onBeforeDeleteFile(ID, lRecordRef);
                    ICISupportTicketLog.DELETE(TRUE);
                    onAfterDeleteFile(ID, lRecordRef);
                end;
        END
    end;

    procedure PrepareDownloadFile(ID: Integer) DataObject: JsonObject
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
        lRecordRef: RecordRef;
        lInStream: InStream;
        SaveAsName: Text;
        DataURI: Text;
    begin
        ICISupDragboxConfig.GET(GlobCongifCode);
        IF ICISupDragboxConfig."Dragbox Type" = ICISupDragboxConfig."Dragbox Type"::Ticket THEN begin
            IF ICISupportTicketLog.GET(ID) THEN begin
                onAfterBeforeDownloadFile(ID, lRecordRef);
                IF ICISupportTicketLog.Data.HasValue() THEN BEGIN
                    DataObject.Add('filename', GetSaveAsName(ICISupportTicketLog."Data Text", ICISupportTicketLog."Data Text 2"));
                    ICISupportTicketLog.CalcFields(Data);
                    ICISupportTicketLog.Data.CreateInStream(lInStream, TextEncoding::Windows);
                    lInStream.ReadText(DataURI);
                    DataObject.Add('DataURI', DataURI);

                    onAfterAfterDownloadFile(ID, lRecordRef);
                END;
            end;
        end ELSE
            IF ICISupDragboxFile.GET(ID) THEN BEGIN
                //See if Shown Filename has extension if not get FileExtesnison from Original Filename(Filename)
                SaveAsName := GetSaveAsName(ICISupDragboxFile.Filename, ICISupDragboxFile."Shown Filename");

                lRecordRef.GetTable(ICISupDragboxFile);
                onAfterBeforeDownloadFile(ID, lRecordRef);

                IF ICISupDragboxFile.Data.HasValue() THEN BEGIN
                    DataObject.Add('filename', SaveAsName);
                    ICISupDragboxFile.CalcFields(Data);
                    ICISupDragboxFile.Data.CreateInStream(lInStream, TextEncoding::Windows);
                    lInStream.ReadText(DataURI);
                    DataObject.Add('DataURI', DataURI);

                    onAfterAfterDownloadFile(ID, lRecordRef);
                END;
            END;
    end;

    procedure GetFileTableNo(): Integer
    begin
        EXIT(Database::"ICI Sup. Dragbox File");
    end;

    procedure ShowDragboxMessage(ErrorID: Integer)
    var
        Error1Lbl: Label 'License Expired';
        Error2Lbl: Label 'File is empty';
        Error3Lbl: Label 'Folder Dragin is not supported';
    begin
        case ErrorID OF
            1:
                MESSAGE(Error1Lbl);
            2:
                MESSAGE(Error2Lbl);
            3:
                MESSAGE(Error3Lbl);
        END;
    end;

    // Handles +++++++++++

    [IntegrationEvent(false, false)]
    local procedure onBeforeInsertFile(filename: Text; var B64: Text; var pRecordRef: RecordRef)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure onAfterInsertFile(filename: Text; var B64: Text; var pRecordRef: RecordRef)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure onBeforeDeleteFile(ID: Integer; var pRecordRef: RecordRef)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure onAfterDeleteFile(ID: Integer; var pRecordRef: RecordRef)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure onAfterBeforeDownloadFile(ID: Integer; var pRecordRef: RecordRef)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure onAfterAfterDownloadFile(ID: Integer; var pRecordRef: RecordRef)
    begin
    end;

    // Helpers

    procedure setRecordRef(pRecordRef: RecordRef);
    begin
        GlobRecordId := pRecordRef.RecordId();
    end;

    procedure setConfiguration(Config: Code[30]);
    begin
        GlobCongifCode := Config;
    end;


    procedure GetSaveAsName(OrigFilename: Text; ShownFilename: Text): Text;
    var
        FileExtOrg: Text;
        FileExtShown: Text;
    begin
        FileExtOrg := GetFileExtension(OrigFilename);
        FileExtShown := GetFileExtension(ShownFilename);
        IF FileExtShown = '' then
            EXIT(ShownFilename + FileExtOrg);
        Exit(ShownFilename);
    end;

    procedure GetFileExtension(filename: Text): Text;
    var
        I: Integer;

    begin
        FOR I := STRLEN(filename) downto 1 do
            IF COPYSTR(filename, I, 1) = '.' THEN
                EXIT(COPYSTR(filename, I, STRLEN(filename)));
    end;

    procedure GetFileContentType(fileextension: Text): Text
    begin
        Case fileextension of
            '.323':
                EXIT('text/h323');
            '.3g2':
                EXIT('video/3gpp2');
            '.3gp':
                EXIT('video/3gpp');
            '.3gp2':
                EXIT('video/3gpp2');
            '.3gpp':
                EXIT('video/3gpp');
            '.7z':
                EXIT('application/x-7z-compressed');
            '.aa':
                EXIT('audio/audible');
            '.AAC':
                EXIT('audio/aac');
            '.aaf':
                EXIT('application/octet-stream');
            '.aax':
                EXIT('audio/vnd.audible.aax');
            '.ac3':
                EXIT('audio/ac3');
            '.aca':
                EXIT('application/octet-stream');
            '.accda':
                EXIT('application/msaccess.addin');
            '.accdb':
                EXIT('application/msaccess');
            '.accdc':
                EXIT('application/msaccess.cab');
            '.accde':
                EXIT('application/msaccess');
            '.accdr':
                EXIT('application/msaccess.runtime');
            '.accdt':
                EXIT('application/msaccess');
            '.accdw':
                EXIT('application/msaccess.webapplication');
            '.accft':
                EXIT('application/msaccess.ftemplate');
            '.acx':
                EXIT('application/internet-property-stream');
            '.AddIn':
                EXIT('text/xml');
            '.ade':
                EXIT('application/msaccess');
            '.adobebridge':
                EXIT('application/x-bridge-url');
            '.adp':
                EXIT('application/msaccess');
            '.ADT':
                EXIT('audio/vnd.dlna.adts');
            '.ADTS':
                EXIT('audio/aac');
            '.afm':
                EXIT('application/octet-stream');
            '.ai':
                EXIT('application/postscript');
            '.aif':
                EXIT('audio/x-aiff');
            '.aifc':
                EXIT('audio/aiff');
            '.aiff':
                EXIT('audio/aiff');
            '.air':
                EXIT('application/vnd.adobe.air-application-installer-package+zip');
            '.amc':
                EXIT('application/x-mpeg');
            '.application':
                EXIT('application/x-ms-application');
            '.art':
                EXIT('image/x-jg');
            '.asa':
                EXIT('application/xml');
            '.asax':
                EXIT('application/xml');
            '.ascx':
                EXIT('application/xml');
            '.asd':
                EXIT('application/octet-stream');
            '.asf':
                EXIT('video/x-ms-asf');
            '.ashx':
                EXIT('application/xml');
            '.asi':
                EXIT('application/octet-stream');
            '.asm':
                EXIT('text/plain');
            '.asmx':
                EXIT('application/xml');
            '.aspx':
                EXIT('application/xml');
            '.asr':
                EXIT('video/x-ms-asf');
            '.asx':
                EXIT('video/x-ms-asf');
            '.atom':
                EXIT('application/atom+xml');
            '.au':
                EXIT('audio/basic');
            '.avi':
                EXIT('video/x-msvideo');
            '.axs':
                EXIT('application/olescript');
            '.bas':
                EXIT('text/plain');
            '.bcpio':
                EXIT('application/x-bcpio');
            '.bin':
                EXIT('application/octet-stream');
            '.bmp':
                EXIT('image/bmp');
            '.c':
                EXIT('text/plain');
            '.cab':
                EXIT('application/octet-stream');
            '.caf':
                EXIT('audio/x-caf');
            '.calx':
                EXIT('application/vnd.ms-fice.calx');
            '.cat':
                EXIT('application/vnd.ms-pki.seccat');
            '.cc':
                EXIT('text/plain');
            '.cd':
                EXIT('text/plain');
            '.cdda':
                EXIT('audio/aiff');
            '.cdf':
                EXIT('application/x-cdf');
            '.cer':
                EXIT('application/x-x509-ca-cert');
            '.chm':
                EXIT('application/octet-stream');
            '.class':
                EXIT('application/x-java-applet');
            '.clp':
                EXIT('application/x-msclip');
            '.cmx':
                EXIT('image/x-cmx');
            '.cnf':
                EXIT('text/plain');
            '.cod':
                EXIT('image/cis-cod');
            '.config':
                EXIT('application/xml');
            '.contact':
                EXIT('text/x-ms-contact');
            '.coverage':
                EXIT('application/xml');
            '.cpio':
                EXIT('application/x-cpio');
            '.cpp':
                EXIT('text/plain');
            '.crd':
                EXIT('application/x-mscardfile');
            '.crl':
                EXIT('application/pkix-crl');
            '.crt':
                EXIT('application/x-x509-ca-cert');
            '.cs':
                EXIT('text/plain');
            '.csdproj':
                EXIT('text/plain');
            '.csh':
                EXIT('application/x-csh');
            '.csproj':
                EXIT('text/plain');
            '.css':
                EXIT('text/css');
            '.csv':
                EXIT('text/csv');
            '.cur':
                EXIT('application/octet-stream');
            '.cxx':
                EXIT('text/plain');
            '.dat':
                EXIT('application/octet-stream');
            '.datasource':
                EXIT('application/xml');
            '.dbproj':
                EXIT('text/plain');
            '.dcr':
                EXIT('application/x-director');
            '.def':
                EXIT('text/plain');
            '.deploy':
                EXIT('application/octet-stream');
            '.der':
                EXIT('application/x-x509-ca-cert');
            '.dgml':
                EXIT('application/xml');
            '.dib':
                EXIT('image/bmp');
            '.dif':
                EXIT('video/x-dv');
            '.dir':
                EXIT('application/x-director');
            '.disco':
                EXIT('text/xml');
            '.dll':
                EXIT('application/x-msdownload');
            '.dll.config':
                EXIT('text/xml');
            '.dlm':
                EXIT('text/dlm');
            '.doc':
                EXIT('application/msword');
            '.docm':
                EXIT('application/vnd.ms-word.document.macroEnabled.12');
            '.docx':
                EXIT('application/vnd.openxmlformats-ficedocument.wordprocessingml.document');
            '.dot':
                EXIT('application/msword');
            '.dotm':
                EXIT('application/vnd.ms-word.template.macroEnabled.12');
            '.dotx':
                EXIT('application/vnd.openxmlformats-ficedocument.wordprocessingml.template');
            '.dsp':
                EXIT('application/octet-stream');
            '.dsw':
                EXIT('text/plain');
            '.dtd':
                EXIT('text/xml');
            '.dtsConfig':
                EXIT('text/xml');
            '.dv':
                EXIT('video/x-dv');
            '.dvi':
                EXIT('application/x-dvi');
            '.dwf':
                EXIT('drawing/x-dwf');
            '.dwp':
                EXIT('application/octet-stream');
            '.dxr':
                EXIT('application/x-director');
            '.eml':
                EXIT('message/rfc822');
            '.emz':
                EXIT('application/octet-stream');
            '.eot':
                EXIT('application/octet-stream');
            '.eps':
                EXIT('application/postscript');
            '.etl':
                EXIT('application/etl');
            '.etx':
                EXIT('text/x-setext');
            '.evy':
                EXIT('application/envoy');
            '.exe':
                EXIT('application/octet-stream');
            '.exe.config':
                EXIT('text/xml');
            '.fdf':
                EXIT('application/vnd.fdf');
            '.fif':
                EXIT('application/fractals');
            '.filters':
                EXIT('Application/xml');
            '.fla':
                EXIT('application/octet-stream');
            '.flr':
                EXIT('x-world/x-vrml');
            '.flv':
                EXIT('video/x-flv');
            '.fsscript':
                EXIT('application/fsharp-script');
            '.fsx':
                EXIT('application/fsharp-script');
            '.generictest':
                EXIT('application/xml');
            '.gif':
                EXIT('image/gif');
            '.group':
                EXIT('text/x-ms-group');
            '.gsm':
                EXIT('audio/x-gsm');
            '.gtar':
                EXIT('application/x-gtar');
            '.gz':
                EXIT('application/x-gzip');
            '.h':
                EXIT('text/plain');
            '.hdf':
                EXIT('application/x-hdf');
            '.hdml':
                EXIT('text/x-hdml');
            '.hhc':
                EXIT('application/x-oleobject');
            '.hhk':
                EXIT('application/octet-stream');
            '.hhp':
                EXIT('application/octet-stream');
            '.hlp':
                EXIT('application/winhlp');
            '.hpp':
                EXIT('text/plain');
            '.hqx':
                EXIT('application/mac-binhex40');
            '.hta':
                EXIT('application/hta');
            '.htc':
                EXIT('text/x-component');
            '.htm':
                EXIT('text/html');
            '.html':
                EXIT('text/html');
            '.htt':
                EXIT('text/webviewhtml');
            '.hxa':
                EXIT('application/xml');
            '.hxc':
                EXIT('application/xml');
            '.hxd':
                EXIT('application/octet-stream');
            '.hxe':
                EXIT('application/xml');
            '.hxf':
                EXIT('application/xml');
            '.hxh':
                EXIT('application/octet-stream');
            '.hxi':
                EXIT('application/octet-stream');
            '.hxk':
                EXIT('application/xml');
            '.hxq':
                EXIT('application/octet-stream');
            '.hxr':
                EXIT('application/octet-stream');
            '.hxs':
                EXIT('application/octet-stream');
            '.hxt':
                EXIT('text/html');
            '.hxv':
                EXIT('application/xml');
            '.hxw':
                EXIT('application/octet-stream');
            '.hxx':
                EXIT('text/plain');
            '.i':
                EXIT('text/plain');
            '.ico':
                EXIT('image/x-icon');
            '.ics':
                EXIT('application/octet-stream');
            '.idl':
                EXIT('text/plain');
            '.ief':
                EXIT('image/ief');
            '.iii':
                EXIT('application/x-iphone');
            '.inc':
                EXIT('text/plain');
            '.inf':
                EXIT('application/octet-stream');
            '.inl':
                EXIT('text/plain');
            '.ins':
                EXIT('application/x-internet-signup');
            '.ipa':
                EXIT('application/x-itunes-ipa');
            '.ipg':
                EXIT('application/x-itunes-ipg');
            '.ipproj':
                EXIT('text/plain');
            '.ipsw':
                EXIT('application/x-itunes-ipsw');
            '.iqy':
                EXIT('text/x-ms-iqy');
            '.isp':
                EXIT('application/x-internet-signup');
            '.ite':
                EXIT('application/x-itunes-ite');
            '.itlp':
                EXIT('application/x-itunes-itlp');
            '.itms':
                EXIT('application/x-itunes-itms');
            '.itpc':
                EXIT('application/x-itunes-itpc');
            '.IVF':
                EXIT('video/x-ivf');
            '.jar':
                EXIT('application/java-archive');
            '.java':
                EXIT('application/octet-stream');
            '.jck':
                EXIT('application/liquidmotion');
            '.jcz':
                EXIT('application/liquidmotion');
            '.jfif':
                EXIT('image/pjpeg');
            '.jnlp':
                EXIT('application/x-java-jnlp-file');
            '.jpb':
                EXIT('application/octet-stream');
            '.jpe':
                EXIT('image/jpeg');
            '.jpeg':
                EXIT('image/jpeg');
            '.jpg':
                EXIT('image/jpeg');
            '.js':
                EXIT('application/x-javascript');
            '.json':
                EXIT('application/json');
            '.jsx':
                EXIT('text/jscript');
            '.jsxbin':
                EXIT('text/plain');
            '.latex':
                EXIT('application/x-latex');
            '.library-ms':
                EXIT('application/windows-library+xml');
            '.lit':
                EXIT('application/x-ms-reader');
            '.loadtest':
                EXIT('application/xml');
            '.lpk':
                EXIT('application/octet-stream');
            '.lsf':
                EXIT('video/x-la-asf');
            '.lst':
                EXIT('text/plain');
            '.lsx':
                EXIT('video/x-la-asf');
            '.lzh':
                EXIT('application/octet-stream');
            '.m13':
                EXIT('application/x-msmediaview');
            '.m14':
                EXIT('application/x-msmediaview');
            '.m1v':
                EXIT('video/mpeg');
            '.m2t':
                EXIT('video/vnd.dlna.mpeg-tts');
            '.m2ts':
                EXIT('video/vnd.dlna.mpeg-tts');
            '.m2v':
                EXIT('video/mpeg');
            '.m3u':
                EXIT('audio/x-mpegurl');
            '.m3u8':
                EXIT('audio/x-mpegurl');
            '.m4a':
                EXIT('audio/m4a');
            '.m4b':
                EXIT('audio/m4b');
            '.m4p':
                EXIT('audio/m4p');
            '.m4r':
                EXIT('audio/x-m4r');
            '.m4v':
                EXIT('video/x-m4v');
            '.mac':
                EXIT('image/x-macpaint');
            '.mak':
                EXIT('text/plain');
            '.man':
                EXIT('application/x-trf-man');
            '.manifest':
                EXIT('application/x-ms-manifest');
            '.map':
                EXIT('text/plain');
            '.master':
                EXIT('application/xml');
            '.mda':
                EXIT('application/msaccess');
            '.mdb':
                EXIT('application/x-msaccess');
            '.mde':
                EXIT('application/msaccess');
            '.mdp':
                EXIT('application/octet-stream');
            '.me':
                EXIT('application/x-trf-me');
            '.mfp':
                EXIT('application/x-shockwave-flash');
            '.mht':
                EXIT('message/rfc822');
            '.mhtml':
                EXIT('message/rfc822');
            '.mid':
                EXIT('audio/mid');
            '.midi':
                EXIT('audio/mid');
            '.mix':
                EXIT('application/octet-stream');
            '.mk':
                EXIT('text/plain');
            '.mmf':
                EXIT('application/x-smaf');
            '.mno':
                EXIT('text/xml');
            '.mny':
                EXIT('application/x-msmoney');
            '.mod':
                EXIT('video/mpeg');
            '.mov':
                EXIT('video/quicktime');
            '.movie':
                EXIT('video/x-sgi-movie');
            '.mp2':
                EXIT('video/mpeg');
            '.mp2v':
                EXIT('video/mpeg');
            '.mp3':
                EXIT('audio/mpeg');
            '.mp4':
                EXIT('video/mp4');
            '.mp4v':
                EXIT('video/mp4');
            '.mpa':
                EXIT('video/mpeg');
            '.mpe':
                EXIT('video/mpeg');
            '.mpeg':
                EXIT('video/mpeg');
            '.mpf':
                EXIT('application/vnd.ms-mediapackage');
            '.mpg':
                EXIT('video/mpeg');
            '.mpp':
                EXIT('application/vnd.ms-project');
            '.mpv2':
                EXIT('video/mpeg');
            '.mqv':
                EXIT('video/quicktime');
            '.ms':
                EXIT('application/x-trf-ms');
            '.msi':
                EXIT('application/octet-stream');
            '.mso':
                EXIT('application/octet-stream');
            '.mts':
                EXIT('video/vnd.dlna.mpeg-tts');
            '.mtx':
                EXIT('application/xml');
            '.mvb':
                EXIT('application/x-msmediaview');
            '.mvc':
                EXIT('application/x-miva-compiled');
            '.mxp':
                EXIT('application/x-mmxp');
            '.nc':
                EXIT('application/x-netcdf');
            '.nsc':
                EXIT('video/x-ms-asf');
            '.nws':
                EXIT('message/rfc822');
            '.ocx':
                EXIT('application/octet-stream');
            '.oda':
                EXIT('application/oda');
            '.odc':
                EXIT('text/x-ms-odc');
            '.odh':
                EXIT('text/plain');
            '.odl':
                EXIT('text/plain');
            '.odp':
                EXIT('application/vnd.oasis.opendocument.presentation');
            '.ods':
                EXIT('application/oleobject');
            '.odt':
                EXIT('application/vnd.oasis.opendocument.text');
            '.one':
                EXIT('application/onenote');
            '.onea':
                EXIT('application/onenote');
            '.onepkg':
                EXIT('application/onenote');
            '.onetmp':
                EXIT('application/onenote');
            '.onetoc':
                EXIT('application/onenote');
            '.onetoc2':
                EXIT('application/onenote');
            '.orderedtest':
                EXIT('application/xml');
            '.osdx':
                EXIT('application/opensearchdescription+xml');
            '.p10':
                EXIT('application/pkcs10');
            '.p12':
                EXIT('application/x-pkcs12');
            '.p7b':
                EXIT('application/x-pkcs7-certificates');
            '.p7c':
                EXIT('application/pkcs7-mime');
            '.p7m':
                EXIT('application/pkcs7-mime');
            '.p7r':
                EXIT('application/x-pkcs7-certreqresp');
            '.p7s':
                EXIT('application/pkcs7-signature');
            '.pbm':
                EXIT('image/x-portable-bitmap');
            '.pcast':
                EXIT('application/x-podcast');
            '.pct':
                EXIT('image/pict');
            '.pcx':
                EXIT('application/octet-stream');
            '.pcz':
                EXIT('application/octet-stream');
            '.pdf':
                EXIT('application/pdf');
            '.pfb':
                EXIT('application/octet-stream');
            '.pfm':
                EXIT('application/octet-stream');
            '.pfx':
                EXIT('application/x-pkcs12');
            '.pgm':
                EXIT('image/x-portable-graymap');
            '.pic':
                EXIT('image/pict');
            '.pict':
                EXIT('image/pict');
            '.pkgdef':
                EXIT('text/plain');
            '.pkgundef':
                EXIT('text/plain');
            '.pko':
                EXIT('application/vnd.ms-pki.pko');
            '.pls':
                EXIT('audio/scpls');
            '.pma':
                EXIT('application/x-perfmon');
            '.pmc':
                EXIT('application/x-perfmon');
            '.pml':
                EXIT('application/x-perfmon');
            '.pmr':
                EXIT('application/x-perfmon');
            '.pmw':
                EXIT('application/x-perfmon');
            '.png':
                EXIT('image/png');
            '.pnm':
                EXIT('image/x-portable-anymap');
            '.pnt':
                EXIT('image/x-macpaint');
            '.pntg':
                EXIT('image/x-macpaint');
            '.pnz':
                EXIT('image/png');
            '.pot':
                EXIT('application/vnd.ms-powerpoint');
            '.potm':
                EXIT('application/vnd.ms-powerpoint.template.macroEnabled.12');
            '.potx':
                EXIT('application/vnd.openxmlformats-ficedocument.presentationml.template');
            '.ppa':
                EXIT('application/vnd.ms-powerpoint');
            '.ppam':
                EXIT('application/vnd.ms-powerpoint.addin.macroEnabled.12');
            '.ppm':
                EXIT('image/x-portable-pixmap');
            '.pps':
                EXIT('application/vnd.ms-powerpoint');
            '.ppsm':
                EXIT('application/vnd.ms-powerpoint.slideshow.macroEnabled.12');
            '.ppsx':
                EXIT('application/vnd.openxmlformats-ficedocument.presentationml.slideshow');
            '.ppt':
                EXIT('application/vnd.ms-powerpoint');
            '.pptm':
                EXIT('application/vnd.ms-powerpoint.presentation.macroEnabled.12');
            '.pptx':
                EXIT('application/vnd.openxmlformats-ficedocument.presentationml.presentation');
            '.prf':
                EXIT('application/pics-rules');
            '.prm':
                EXIT('application/octet-stream');
            '.prx':
                EXIT('application/octet-stream');
            '.ps':
                EXIT('application/postscript');
            '.psc1':
                EXIT('application/PowerShell');
            '.psd':
                EXIT('application/octet-stream');
            '.psess':
                EXIT('application/xml');
            '.psm':
                EXIT('application/octet-stream');
            '.psp':
                EXIT('application/octet-stream');
            '.pub':
                EXIT('application/x-mspublisher');
            '.pwz':
                EXIT('application/vnd.ms-powerpoint');
            '.qht':
                EXIT('text/x-html-insertion');
            '.qhtm':
                EXIT('text/x-html-insertion');
            '.qt':
                EXIT('video/quicktime');
            '.qti':
                EXIT('image/x-quicktime');
            '.qtif':
                EXIT('image/x-quicktime');
            '.qtl':
                EXIT('application/x-quicktimeplayer');
            '.qxd':
                EXIT('application/octet-stream');
            '.ra':
                EXIT('audio/x-pn-realaudio');
            '.ram':
                EXIT('audio/x-pn-realaudio');
            '.rar':
                EXIT('application/octet-stream');
            '.ras':
                EXIT('image/x-cmu-raster');
            '.rat':
                EXIT('application/rat-file');
            '.rc':
                EXIT('text/plain');
            '.rc2':
                EXIT('text/plain');
            '.rct':
                EXIT('text/plain');
            '.rdlc':
                EXIT('application/xml');
            '.resx':
                EXIT('application/xml');
            '.rf':
                EXIT('image/vnd.rn-realflash');
            '.rgb':
                EXIT('image/x-rgb');
            '.rgs':
                EXIT('text/plain');
            '.rm':
                EXIT('application/vnd.rn-realmedia');
            '.rmi':
                EXIT('audio/mid');
            '.rmp':
                EXIT('application/vnd.rn-rn_music_package');
            '.rpm':
                EXIT('audio/x-pn-realaudio-plugin');
            '.rqy':
                EXIT('text/x-ms-rqy');
            '.rtf':
                EXIT('application/rtf');
            '.rtx':
                EXIT('text/richtext');
            '.ruleset':
                EXIT('application/xml');
            '.s':
                EXIT('text/plain');
            '.safariextz':
                EXIT('application/x-safari-safariextz');
            '.scd':
                EXIT('application/x-msschedule');
            '.sct':
                EXIT('text/scriptlet');
            '.sd2':
                EXIT('audio/x-sd2');
            '.sdp':
                EXIT('application/sdp');
            '.sea':
                EXIT('application/octet-stream');
            '.searchConnector-ms':
                EXIT('application/windows-search-connector+xml');
            '.setpay':
                EXIT('application/set-payment-initiation');
            '.setreg':
                EXIT('application/set-registration-initiation');
            '.settings':
                EXIT('application/xml');
            '.sgimb':
                EXIT('application/x-sgimb');
            '.sgml':
                EXIT('text/sgml');
            '.sh':
                EXIT('application/x-sh');
            '.shar':
                EXIT('application/x-shar');
            '.shtml':
                EXIT('text/html');
            '.sit':
                EXIT('application/x-stuffit');
            '.sitemap':
                EXIT('application/xml');
            '.skin':
                EXIT('application/xml');
            '.sldm':
                EXIT('application/vnd.ms-powerpoint.slide.macroEnabled.12');
            '.sldx':
                EXIT('application/vnd.openxmlformats-ficedocument.presentationml.slide');
            '.slk':
                EXIT('application/vnd.ms-excel');
            '.sln':
                EXIT('text/plain');
            '.slupkg-ms':
                EXIT('application/x-ms-license');
            '.smd':
                EXIT('audio/x-smd');
            '.smi':
                EXIT('application/octet-stream');
            '.smx':
                EXIT('audio/x-smd');
            '.smz':
                EXIT('audio/x-smd');
            '.snd':
                EXIT('audio/basic');
            '.snippet':
                EXIT('application/xml');
            '.snp':
                EXIT('application/octet-stream');
            '.sol':
                EXIT('text/plain');
            '.sor':
                EXIT('text/plain');
            '.spc':
                EXIT('application/x-pkcs7-certificates');
            '.spl':
                EXIT('application/futuresplash');
            '.src':
                EXIT('application/x-wais-source');
            '.srf':
                EXIT('text/plain');
            '.SSISDeploymentManifest':
                EXIT('text/xml');
            '.ssm':
                EXIT('application/streamingmedia');
            '.sst':
                EXIT('application/vnd.ms-pki.certstore');
            '.stl':
                EXIT('application/vnd.ms-pki.stl');
            '.sv4cpio':
                EXIT('application/x-sv4cpio');
            '.sv4crc':
                EXIT('application/x-sv4crc');
            '.svc':
                EXIT('application/xml');
            '.swf':
                EXIT('application/x-shockwave-flash');
            '.t':
                EXIT('application/x-trf');
            '.tar':
                EXIT('application/x-tar');
            '.tcl':
                EXIT('application/x-tcl');
            '.testrunconfig':
                EXIT('application/xml');
            '.testsettings':
                EXIT('application/xml');
            '.tex':
                EXIT('application/x-tex');
            '.texi':
                EXIT('application/x-texinfo');
            '.texinfo':
                EXIT('application/x-texinfo');
            '.tgz':
                EXIT('application/x-compressed');
            '.thmx':
                EXIT('application/vnd.ms-ficetheme');
            '.thn':
                EXIT('application/octet-stream');
            '.tif':
                EXIT('image/tiff');
            '.tiff':
                EXIT('image/tiff');
            '.tlh':
                EXIT('text/plain');
            '.tli':
                EXIT('text/plain');
            '.toc':
                EXIT('application/octet-stream');
            '.tr':
                EXIT('application/x-trf');
            '.trm':
                EXIT('application/x-msterminal');
            '.trx':
                EXIT('application/xml');
            '.ts':
                EXIT('video/vnd.dlna.mpeg-tts');
            '.tsv':
                EXIT('text/tab-separated-values');
            '.ttf':
                EXIT('application/octet-stream');
            '.tts':
                EXIT('video/vnd.dlna.mpeg-tts');
            '.txt':
                EXIT('text/plain');
            '.u32':
                EXIT('application/octet-stream');
            '.uls':
                EXIT('text/iuls');
            '.user':
                EXIT('text/plain');
            '.ustar':
                EXIT('application/x-ustar');
            '.vb':
                EXIT('text/plain');
            '.vbdproj':
                EXIT('text/plain');
            '.vbk':
                EXIT('video/mpeg');
            '.vbproj':
                EXIT('text/plain');
            '.vbs':
                EXIT('text/vbscript');
            '.vcf':
                EXIT('text/x-vcard');
            '.vcproj':
                EXIT('Application/xml');
            '.vcs':
                EXIT('text/plain');
            '.vcxproj':
                EXIT('Application/xml');
            '.vddproj':
                EXIT('text/plain');
            '.vdp':
                EXIT('text/plain');
            '.vdproj':
                EXIT('text/plain');
            '.vdx':
                EXIT('application/vnd.ms-visio.viewer');
            '.vml':
                EXIT('text/xml');
            '.vscontent':
                EXIT('application/xml');
            '.vsct':
                EXIT('text/xml');
            '.vsd':
                EXIT('application/vnd.visio');
            '.vsi':
                EXIT('application/ms-vsi');
            '.vsix':
                EXIT('application/vsix');
            '.vsixlangpack':
                EXIT('text/xml');
            '.vsixmanifest':
                EXIT('text/xml');
            '.vsmdi':
                EXIT('application/xml');
            '.vspscc':
                EXIT('text/plain');
            '.vss':
                EXIT('application/vnd.visio');
            '.vsscc':
                EXIT('text/plain');
            '.vssettings':
                EXIT('text/xml');
            '.vssscc':
                EXIT('text/plain');
            '.vst':
                EXIT('application/vnd.visio');
            '.vstemplate':
                EXIT('text/xml');
            '.vsto':
                EXIT('application/x-ms-vsto');
            '.vsw':
                EXIT('application/vnd.visio');
            '.vsx':
                EXIT('application/vnd.visio');
            '.vtx':
                EXIT('application/vnd.visio');
            '.wav':
                EXIT('audio/wav');
            '.wave':
                EXIT('audio/wav');
            '.wax':
                EXIT('audio/x-ms-wax');
            '.wbk':
                EXIT('application/msword');
            '.wbmp':
                EXIT('image/vnd.wap.wbmp');
            '.wcm':
                EXIT('application/vnd.ms-works');
            '.wdb':
                EXIT('application/vnd.ms-works');
            '.wdp':
                EXIT('image/vnd.ms-photo');
            '.webarchive':
                EXIT('application/x-safari-webarchive');
            '.webtest':
                EXIT('application/xml');
            '.wiq':
                EXIT('application/xml');
            '.wiz':
                EXIT('application/msword');
            '.wks':
                EXIT('application/vnd.ms-works');
            '.WLMP':
                EXIT('application/wlmoviemaker');
            '.wlpginstall':
                EXIT('application/x-wlpg-detect');
            '.wlpginstall3':
                EXIT('application/x-wlpg3-detect');
            '.wm':
                EXIT('video/x-ms-wm');
            '.wma':
                EXIT('audio/x-ms-wma');
            '.wmd':
                EXIT('application/x-ms-wmd');
            '.wmf':
                EXIT('application/x-msmetafile');
            '.wml':
                EXIT('text/vnd.wap.wml');
            '.wmlc':
                EXIT('application/vnd.wap.wmlc');
            '.wmls':
                EXIT('text/vnd.wap.wmlscript');
            '.wmlsc':
                EXIT('application/vnd.wap.wmlscriptc');
            '.wmp':
                EXIT('video/x-ms-wmp');
            '.wmv':
                EXIT('video/x-ms-wmv');
            '.wmx':
                EXIT('video/x-ms-wmx');
            '.wmz':
                EXIT('application/x-ms-wmz');
            '.wpl':
                EXIT('application/vnd.ms-wpl');
            '.wps':
                EXIT('application/vnd.ms-works');
            '.wri':
                EXIT('application/x-mswrite');
            '.wrl':
                EXIT('x-world/x-vrml');
            '.wrz':
                EXIT('x-world/x-vrml');
            '.wsc':
                EXIT('text/scriptlet');
            '.wsdl':
                EXIT('text/xml');
            '.wvx':
                EXIT('video/x-ms-wvx');
            '.xaf':
                EXIT('x-world/x-vrml');
            '.xaml':
                EXIT('application/xaml+xml');
            '.xap':
                EXIT('application/x-silverlight-app');
            '.xbap':
                EXIT('application/x-ms-xbap');
            '.xbm':
                EXIT('image/x-xbitmap');
            '.xdr':
                EXIT('text/plain');
            '.xht':
                EXIT('application/xhtml+xml');
            '.xhtml':
                EXIT('application/xhtml+xml');
            '.xla':
                EXIT('application/vnd.ms-excel');
            '.xlam':
                EXIT('application/vnd.ms-excel.addin.macroEnabled.12');
            '.xlc':
                EXIT('application/vnd.ms-excel');
            '.xld':
                EXIT('application/vnd.ms-excel');
            '.xlk':
                EXIT('application/vnd.ms-excel');
            '.xll':
                EXIT('application/vnd.ms-excel');
            '.xlm':
                EXIT('application/vnd.ms-excel');
            '.xls':
                EXIT('application/vnd.ms-excel');
            '.xlsb':
                EXIT('application/vnd.ms-excel.sheet.binary.macroEnabled.12');
            '.xlsm':
                EXIT('application/vnd.ms-excel.sheet.macroEnabled.12');
            '.xlsx':
                EXIT('application/vnd.openxmlformats-ficedocument.spreadsheetml.sheet');
            '.xlt':
                EXIT('application/vnd.ms-excel');
            '.xltm':
                EXIT('application/vnd.ms-excel.template.macroEnabled.12');
            '.xltx':
                EXIT('application/vnd.openxmlformats-ficedocument.spreadsheetml.template');
            '.xlw':
                EXIT('application/vnd.ms-excel');
            '.xml':
                EXIT('text/xml');
            '.xmta':
                EXIT('application/xml');
            '.x':
                EXIT('x-world/x-vrml');
            '.XOML':
                EXIT('text/plain');
            '.xpm':
                EXIT('image/x-xpixmap');
            '.xps':
                EXIT('application/vnd.ms-xpsdocument');
            '.xrm-ms':
                EXIT('text/xml');
            '.xsc':
                EXIT('application/xml');
            '.xsd':
                EXIT('text/xml');
            '.xsf':
                EXIT('text/xml');
            '.xsl':
                EXIT('text/xml');
            '.xslt':
                EXIT('text/xml');
            '.xsn':
                EXIT('application/octet-stream');
            '.xss':
                EXIT('application/xml');
            '.xtp':
                EXIT('application/octet-stream');
            '.xwd':
                EXIT('image/x-xwindowdump');
            '.z':
                EXIT('application/x-compress');
            '.zip':
                EXIT('application/x-zip-compressed');
        End
    end;

    procedure DeleteDragboxFilesForRec(PRecordRef: RecordRef)
    var
        ICISupDragboxFile: Record "ICI Sup. Dragbox File";
        lRecordID: RecordId;

    begin
        lRecordID := PRecordRef.RecordId;
        ICISupDragboxFile.SetCurrentKey("Record ID");
        ICISupDragboxFile.SetRange("Record ID", lRecordID);
        IF ICISupDragboxFile.FindFirst() then
            ICISupDragboxFile.DeleteAll();
    end;


    procedure ShowDragboxMailProcessDialog(TicketNo: Code[20]; var TempICISupDragboxSelection: Record "ICI Sup. Dragbox Selection" temporary; MailInfo: JsonObject)
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        jTok: JsonToken;
        jVal: JsonValue;
        MailBody: Text;
        MailSubject: Text;
        MailSenderEMail: Text;
        lInStream: InStream;
        DataURI: Text;
        B64: Text;
        SendToEMail: Text[250];
        CompanyNo: Code[20];
        ContactNo: Code[20];
    begin
        ICISupportMailSetup.GET();

        // Get Mail Info
        IF MailInfo.Get('subject', jTok) THEN begin
            jVal := jTok.AsValue();
            MailSubject := jVal.AsText();
        end;

        IF MailInfo.Get('body', jTok) THEN begin
            jVal := jTok.AsValue();
            MailBody := jVal.AsText();
        end;

        IF MailInfo.Get('email', jTok) THEN begin
            jVal := jTok.AsValue();
            MailSenderEMail := jVal.AsText();
        end;

        ICISupportTicket.GET(TicketNo);
        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        IF TempICISupDragboxSelection.FINDSET() THEN
            repeat
                TempICISupDragboxSelection.CalcFields(Content);
                TempICISupDragboxSelection.Content.CreateInStream(lInStream);
                lInStream.Read(DataURI);

                // DataURI looks loke this: data:text/plain;base64,L2h0ZG9jcy9..
                ICIMailrobotMgt.Explode(DataURI, ';'); // data looks loke this:base64,L2h0ZG9jcy9...
                ICIMailrobotMgt.Explode(DataURI, ',');// data looks loke this: L2h0ZG9jcy9...
                b64 := DataURI;
                ICINotificationDialogMgt.AddAttachment(TempICISupDragboxSelection.FileName, TempICISupDragboxSelection.MimeType, b64); // PDF Anhang Setzen
            until TempICISupDragboxSelection.Next() = 0;


        IF NOT ICIMailrobotMgt.GetContactfromEmail(MailSenderEMail, CompanyNo, ContactNo) THEN begin
            IF CompanyNo <> '' THEN Contact.GET(CompanyNo);
            IF ContactNo <> '' THEN Contact.GET(ContactNo);
        end;
        IF Contact."No." = '' then begin // Mail wurde von Unbekanntem Kontakt abgesendet -> Aktuellen Ticketkontakt benachrichtigen
            Contact.GET(ICISupportTicket."Current Contact No.");
            SendToEMail := Contact."E-Mail";
            if ICISupportTicket."Contact E-Mail" <> '' then
                SendToEMail := ICISupportTicket."Contact E-Mail";
            IF ICISupportMailSetup."Test Mode" then
                SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
            ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen
        end else begin
            // Mail wurde von anderem als aktuellen Kontakt abgesendet -> Kontakt benachrichtigen
            IF Contact."No." <> ICISupportTicket."Current Contact No." THEN begin
                SendToEMail := Contact."E-Mail";
                IF ICISupportMailSetup."Test Mode" then
                    SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
                ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen
            end;

            // Ticketbenutzer ist nicht aktueller benutzer -> Benutzer benachrichtigen
            ICISupportUser.GetCurrUser(ICISupportUser);
            IF ICISupportTicket."Support User ID" <> ICISupportUser."User ID" then begin
                SendToEMail := ICISupportUser."E-Mail";
                IF ICISupportMailSetup."Test Mode" then
                    SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
                ICINotificationDialogMgt.AddReceiver(ICISupportUser."User ID", Enum::"Notification Method Type"::Email, ICISupportUser.Name, SendToEMail, Enum::"Email Recipient Type"::"To", ICISupportUser.RecordId()); // Empfänger setzen
            end;
        end;

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure TryProcessMailToNewTicket(MailInfo: JsonObject): Code[20]
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
        ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
        SubjectAsText: Text;
        BodyAsText: Text;
        TicketNo: Code[20];
        ContactNo: Code[20];
        DefaultReferenceNo: Text;
        DefaultCategory1: Code[50];
        DefaultCategory2: Code[50];
        DefaultDepartmentCode: Code[10];
        jTok: JsonToken;
        jVal: JsonValue;
        MailSenderEMail: Text;
    begin
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT('');

        // Get Mail Info
        IF MailInfo.Get('subject', jTok) THEN begin
            jVal := jTok.AsValue();
            SubjectAsText := jVal.AsText();
        end;

        IF MailInfo.Get('body', jTok) THEN begin
            jVal := jTok.AsValue();
            BodyAsText := jVal.AsText();
        end;

        IF MailInfo.Get('email', jTok) THEN begin
            jVal := jTok.AsValue();
            MailSenderEMail := jVal.AsText();
        end;

        ContactNo := GetOrCreateContact(MailSenderEMail); // Copystr used for compiler warning
        IF ContactNo = '' then  // Multiple Contacts with Same Mail found
            EXIT('');


        IF ICIMailrobotMailbox.FindFirst() THEN;
        DefaultCategory1 := ICIMailrobotMailbox."Default Category 1";
        DefaultCategory2 := ICIMailrobotMailbox."Default Category 2";
        DefaultDepartmentCode := ICIMailrobotMailbox."Default Department";

        TicketNo := CopyStr(CreateTicketFromNewMail(SubjectAsText, BodyAsText, DefaultReferenceNo, DefaultCategory1, DefaultCategory2, DefaultDepartmentCode, ContactNo), 1, 20); // Copystr used for compiler warning
        COMMIT();

        IF ICISupportTicket.GET(TicketNo) THEN BEGIN
            // Set Mailrobot Default User
            IF ICIMailrobotMailbox."Support User ID" <> '' THEN
                ICISupportTicket.VALIDATE("Support User ID", ICIMailrobotMailbox."Support User ID")
            else
                ICISupportTicket.Validate("Support User ID", ICIFirstAllocationMatrix.GetFirstAllocation(ICISupportTicket, true));

            ICISupportTicket.SetModifiedByContact(ICISupportTicket."Current Contact No.");
            ICISupportTicket.Modify(true);
        END;
        EXIT(TicketNo);
    end;

    procedure CreateTicketFromNewMail(Description: Text; Message: Text; ReferenceNo: Text; Category1: Text; Category2: Text; DepartmentCode: Code[10]; ContactNo: Text): Text
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
    begin
        ICISupportTicket.Init();
        ICISupportTicket.Validate("No.");
        ICISupportTicket.INSERT(TRUE);
        ICISupportTicket.Validate(Description, COPYSTR(Description, 1, 250));
        // ICISupportTicket.Validate("Reference No.", COPYSTR(ReferenceNo, 1, 50));

        Contact.GET(ContactNo);
        ICISupportTicket.VALIDATE("Company Contact No.", Contact."Company No.");
        ICISupportTicket.Validate("Current Contact No.", Contact."No.");

        IF Category1 <> '' THEN
            ICISupportTicket.Validate("Category 1 Code", COPYSTR(Category1, 1, 50));
        IF Category2 <> '' THEN
            ICISupportTicket.Validate("Category 2 Code", COPYSTR(Category2, 1, 50));
        IF DepartmentCode <> '' THEN
            ICISupportTicket.Validate("Department Code", DepartmentCode);

        ICISupportTicket.Modify(true);

        //ICISupportTicketLogMgt.SaveTicketContactMessage(ICISupportTicket."No.", Message, Contact."No.");

        exit(ICISupportTicket."No.");
    end;

    procedure GetOrCreateContact(SenderEMail: Text) ContactNo: Code[20]
    var
        Contact: Record Contact;
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
        ContactCompanyNo: Code[20];
        ContactCount: Integer;
        MultipleContactsErr: Label 'Oops. Found more than one Contact for E-Mail Adress %1', Comment = '%1=EmailAddr|de-DE=Es wurden mehrere Kontakte zu dieser E-Mail Adresse %1 gefunden';
        FirstName: Text;
        Surname: Text;
        EMail: Text;
        PhoneNo: Text;
        Salutation: Text;
    begin

        FirstName := COPYSTR(SenderEMail, 1, STRPOS(SenderEMail, '@') - 1); // Part of E-Mail address before the @ Symbol
        Surname := '';
        PhoneNo := '';
        EMail := SenderEMail;

        IF ICIMailrobotMailbox.FINDFIRST() THEN
            Salutation := ICIMailrobotMailbox."Default Salutation";

        ICISupportMailSetup.GET();

        Contact.SETRANGE("Search E-Mail", UpperCase(EMail));
        ContactCount := Contact.Count();

        IF GuiAllowed AND (ContactCount > 1) THEN
            ERROR(MultipleContactsErr, EMail);
        IF (ContactCount > 1) THEN
            EXIT('');

        IF ContactCount = 1 then begin
            Contact.FindFirst();
            ContactNo := Contact."No.";
            EXIT;
        end;

        CLEAR(Contact);
        Contact.INIT();
        Contact.Validate("No.", '');
        Contact.INSERT(TRUE);
        Contact.Validate(Type, Contact.Type::Person);

        ContactCompanyNo := ICIMailrobotMailbox."Contact Company No.";
        IF ContactCompanyNo = '' THEN
            ContactCompanyNo := CreateCompanyContact(SenderEMail);

        Contact.Validate("Company No.", ContactCompanyNo);

        Contact.Validate("First Name", COPYSTR(FirstName, 1, 30));
        Contact.Validate(Surname, COPYSTR(Surname, 1, 30));
        Contact.Validate("E-Mail", COPYSTR(EMail, 1, 80));
        Contact.Validate("Phone No.", COPYSTR(PhoneNo, 1, 30));
        Contact.Validate("ICI Support Active", true);

        case Salutation of
            'M':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code M");
            'F':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code F");
            'D':
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D");
            else
                Contact.VALIDATE("Salutation Code", ICISupportMailSetup."Salutation Code D"); //Fall back to Diverse
        end;

        Contact.Modify(true);
        ContactNo := Contact."No.";
        EXIT;
    end;

    procedure CreateCompanyContact(SenderEMail: Text): Code[20]
    var
        Contact: Record Contact;
        ContactCompanyName: Text;
    begin
        ContactCompanyName := CopyStr(SenderEMail, StrPos(SenderEMail, '@') + 1, 80);

        IF STRLEN(DELCHR(ContactCompanyName, '=', DELCHR(ContactCompanyName, '=', '.'))) = 1 then // Only one occurrence of . as in support@ic-innovative.de. We want the company name to be ic-innovative and not ic-innovative.de
            ContactCompanyName := CopyStr(ContactCompanyName, 1, StrPos(ContactCompanyName, '.') - 1);

        ContactCompanyName := CONVERTSTR(ContactCompanyName, '-', ' ');

        Contact.INIT();
        Contact.Validate("No.", '');
        Contact.INSERT(TRUE);
        Contact.Validate(Type, Contact.Type::Company);
        Contact.Validate(Name, COPYSTR(ContactCompanyName, 1, 100));
        Contact.Validate("ICI Support Active", true);
        Contact.Modify(true);
        Exit(Contact."No.");
    end;

    procedure GetLicenseLabel(): Text
    begin
        EXIT(StrSubstNo(LicensedTxt, CompanyName));
    end;

    var
        GlobRecordId: RecordId;
        LicensedTxt: Label 'Licensed for %1', Comment = '%1=CompayName|de-DE=Lizenziert für %1';

        GlobCongifCode: Code[30];

}
