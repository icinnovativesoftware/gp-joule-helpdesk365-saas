page 56308 "ICI Dragbox File Rename Dialog"
{

    Caption = 'ICI Dragbox File Rename Dialog', Comment = 'de-DE=Datei umbenennen';
    PageType = ConfirmationDialog;
    SourceTable = "ICI Sup. Dragbox File";

    layout
    {
        area(content)
        {
            field(Filename; Rec.Filename)
            {
                ApplicationArea = All;
                Editable = False;
                ToolTip = 'Shows the original Filename', Comment = 'de-DE=Ursprünglicher Dateiname';
                Caption = 'Original Filename', Comment = 'de-DE=Ursprünglicher Dateiname';

            }

            field("Shown Filename"; Rec."Shown Filename")
            {
                ApplicationArea = All;
                ToolTip = 'Input the new Filename here', Comment = 'de-DE=Den neuen Dateinamen hier eingeben';
                Caption = 'New File Name', Comment = 'de-DE=Neuer Dateiname';
            }


        }
    }
}
