page 56310 "ICI Sup. Config. Card"
{

    Caption = 'ICI Sup. Config. Card', Comment = 'de-DE=Support Dragbox Konfiguration';
    PageType = Card;
    SourceTable = "ICI Sup. Dragbox Config.";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'The Code of this Record', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'The Description of this Record', Comment = 'de-DE=Beschreibung';
                }
                field("Dragbox Type"; Rec."Dragbox Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'The Dragbox Type of this Record. Choose between the Options "Store in Database","Activity","Record Link","One Drive",Ticket', Comment = 'de-DE=Die Dragbox Art. Für Mailrobot und Ticket sollte "Ticket" ausgewählt werden';
                }
                field("Outlook Mail Integration"; Rec."Outlook Mail Integration")
                {
                    ApplicationArea = All;
                    ToolTip = 'Choose to process E-Mail Attachments when a .msg File or a Mail from Outlook (Desktop Version) is drag&dropped. Drag&Drop from Outlook not supported in all Browsers', Comment = 'de-DE=Wählen Sie diese Option, um E-Mail-Anhänge zu verarbeiten, wenn eine MSG-Datei oder eine E-Mail aus Outlook (Desktop-Version) per Drag & Drop verschoben wird. Drag & Drop aus Outlook wird nicht in allen Browsern unterstützt';
                }
                field("Rename on Upload"; Rec."Rename on Upload")
                {
                    ApplicationArea = All;
                    ToolTip = 'Choose Rename on Upload to rename Files, after Drag&Dropping them', Comment = 'de-DE=Auswählen, um Dateien nach dem Drag&Drop per Hand umzubenennen';
                }
                field("View Type"; Rec."View Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Choose how the Elements in the Dragbox are shown', Comment = 'de-DE=Ansichtsart';
                }
                field("Log active"; Rec."Log active")
                {
                    ApplicationArea = All;
                    ToolTip = 'If you check this option, then a Dragbox Log entry will be created for every User Interaction with the Dragbox and its Files', Comment = 'de-DE=Wenn Sie diese Option ausgewählt haben, wird für jede Benutzerinteraktion mit der Dragbox und ihren Dateien ein Dragbox-Protokolleintrag erstellt';
                }
                field("Delete Allowed"; Rec."Delete Allowed")
                {
                    ApplicationArea = All;
                    ToolTip = 'If you check this option, then a Dragbox Log entry will be created for every User Interaction with the Dragbox and its Files', Comment = 'de-DE=Wenn Sie diese Option ausgewählt haben, können Dateien aus der Dragbox nicht gelöscht werden';
                }
                field("ICI Ticket Notification Dialog"; Rec."ICI Ticket Notification Dialog")
                {
                    ApplicationArea = All;
                    ToolTip = 'If you check this option, then the Create Ticket Notification Dialog is added when a File is added to the Dragbox', Comment = 'de-DE=Wenn Sie diese Option ausgewählt haben, öffnet das Hineinziehen von Dateien das "Nachricht erfassen" Fenster';
                }
            }
        }
    }
}
