page 56292 "ICI Support Log List"
{

    ApplicationArea = All;
    Caption = 'ICI Support Log List', Comment = 'de-DE=Support Log';
    PageType = List;
    SourceTable = "ICI Support Log";
    UsageCategory = History;
    DeleteAllowed = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Created By Type"; Rec."Created By Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Created By Type', Comment = 'de-DE=Erstellt von Art';
                }
                field("Created By"; Rec."Created By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Created By', Comment = 'de-DE=Erstellt von';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Date', Comment = 'de-DE=Erstellt am';
                }
                field("Creation Time"; Rec."Creation Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Creation Time', Comment = 'de-DE=Erstellt um';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(DownloadInformation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download Information', Comment = 'de-DE=Informationen herunterladen';
                Image = ExportMessage;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                ToolTip = 'Download Information', Comment = 'de-DE=Informationen herunterladen';

                trigger OnAction()
                begin
                    Rec.DownloadLogInformation();
                end;
            }
        }
    }
}
