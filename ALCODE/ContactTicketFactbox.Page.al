page 56360 "ICI Contact Ticket Factbox"
{

    Caption = 'Contact Ticket Factbox', Comment = 'de-DE=Kontakt - Tickets';
    PageType = CardPart;
    SourceTable = Contact;

    layout
    {
        area(content)
        {
            group(Company)
            {
                Caption = 'Company', Comment = 'de-DE=Unternehmen';

                field("ICI Comp. Tickets - Open"; Rec."ICI Comp. Tickets - Open")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Open field', Comment = 'de-DE=Tickets - Offen';
                    ApplicationArea = All;
                }
                field("ICI Comp. Tickets - Processing"; Rec."ICI Comp. Tickets - Processing")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Processing field', Comment = 'de-DE=Tickets - In Bearbeitung';
                    ApplicationArea = All;
                }
                field("ICI Comp. Tickets - Waiting"; Rec."ICI Comp. Tickets - Waiting")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Waiting field', Comment = 'de-DE=Tickets - Warten';
                    ApplicationArea = All;
                }
                field("ICI Comp. Tickets - Closed"; Rec."ICI Comp. Tickets - Closed")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Closed field', Comment = 'de-DE=Tickets - Geschlossen';
                    ApplicationArea = All;
                }
            }
            group(Person)
            {
                Caption = 'Person', Comment = 'de-DE=Person';
                field("ICI Tickets - Open"; Rec."ICI Tickets - Open")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Open field', Comment = 'de-DE=Tickets - Offen';
                    ApplicationArea = All;
                }
                field("ICI Tickets - Processing"; Rec."ICI Tickets - Processing")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Processing field', Comment = 'de-DE=Tickets - In Bearbeitung';
                    ApplicationArea = All;
                }
                field("ICI Tickets - Waiting"; Rec."ICI Tickets - Waiting")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Waiting field', Comment = 'de-DE=Tickets - Warten';
                    ApplicationArea = All;
                }
                field("ICI Tickets - Closed"; Rec."ICI Tickets - Closed")
                {
                    ToolTip = 'Specifies the value of the No. of Tickets - Closed field', Comment = 'de-DE=Tickets - Geschlossen';
                    ApplicationArea = All;
                }
            }
        }
    }
}
