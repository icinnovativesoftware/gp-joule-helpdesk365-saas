codeunit 56288 "ICI Event Subscriber"
{
    EventSubscriberInstance = StaticAutomatic;
    Permissions = tabledata "Interaction Log Entry" = RMI, tabledata "ICI Support Ticket" = RMI, tabledata "ICI Sup. Dragbox File" = RMI, tabledata "ICI Support Time Log Line" = rmi;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"UI Helper Triggers", 'GetCueStyle', '', true, true)]
    local procedure GetCueStyleSub(TableId: Integer;
    FieldNo: Integer;
    CueValue: Decimal;

    VAR
        StyleText: Text);
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportCue: Record "ICI Support Cue";
        Green: Text;
        Yellow: Text;
        Red: Text;
    begin
        Green := 'Favorable';
        Yellow := 'Ambiguous';
        Red := 'Unfavorable';

        IF TableId <> Database::"ICI Support Cue" THEN
            exit;

        IF NOT ICISupportSetup.GET() THEN
            exit;

        IF (ICISupportSetup."Cue Indicator High" = 0) And (ICISupportSetup."Cue Indicator Low" = 0) THEN
            EXIT;

        IF NOT ICISupportCue.GET() THEN
            exit;

        Case (FieldNo)
        OF
            ICISupportCue.FieldNo("My Tickets - Closed"),
            ICISupportCue.FieldNo("Tickets - Closed Month"),
            ICISupportCue.FieldNo("Tickets - Closed"),
            ICISupportCue.FieldNo("Tickets - Closed Today"),
            ICISupportCue.FieldNo("My Dep. Tickets - Closed"),
            ICISupportCue.FieldNo("Tickets - Last ch. by User"):
                begin
                    // Closed: Zero is Yellow, Positive is Good
                    // Last changed by User
                    StyleText := Yellow;
                    IF CueValue > 0 then
                        StyleText := Green;
                end;
            //ICISupportCue.FieldNo("Tickets -  Escalated"),
            ICISupportCue.FieldNo("Tickets - Last ch. by Contact"),
            ICISupportCue.FieldNo("Mailrobot - Errors"):
                begin
                    // Escalation: Zero is Good, Positive is Bad
                    // Last changed by Contact
                    // Mailrobot Errors
                    StyleText := Green;
                    IF CueValue > 0 then
                        StyleText := Red;
                end;
            ELSE begin
                // Default: Low Numbers = Good, High Numbers = Bad
                StyleText := Yellow;
                IF CueValue < ICISupportSetup."Cue Indicator Low" then
                    StyleText := Green;
                IF CueValue > ICISupportSetup."Cue Indicator High" then
                    StyleText := Red;
            end;
        End;
    end;



    [EventSubscriber(ObjectType::Table, DATABASE::"Sales Line", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure OnBeforeDeleteEventsSalesLine(var Rec: Record "Sales Line"; RunTrigger: Boolean)
    var
        ICISupportTimeLogLine: Record "ICI Support Time Log Line";
    begin
        IF NOT RunTrigger THEN
            EXIT;
        IF Rec."ICI Support Ticket No." = '' THEN
            EXIT;
        IF Rec."ICI Time Log Line No." = 0 THEN
            EXIT;

        IF NOT ICISupportTimeLogLine.GET(Rec."ICI Support Ticket No.", Rec."ICI Time Log Line No.") then
            Exit;
        ICISupportTimeLogLine.VALIDATE(Cleared, FALSE);
        ICISupportTimeLogLine.MODIFY();
    end;


    [EventSubscriber(ObjectType::Table, DATABASE::"Service Line", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure OnBeforeDeleteEvent(var Rec: Record "Service Line"; RunTrigger: Boolean)
    var
        ICISupportTimeLogLine: Record "ICI Support Time Log Line";
    begin
        IF NOT RunTrigger THEN
            EXIT;

        IF Rec."ICI Support Ticket No." = '' THEN
            EXIT;
        IF Rec."ICI Time Log Line No." = 0 THEN
            EXIT;

        IF NOT ICISupportTimeLogLine.GET(Rec."ICI Support Ticket No.", Rec."ICI Time Log Line No.") then
            Exit;
        ICISupportTimeLogLine.VALIDATE(Cleared, FALSE);
        ICISupportTimeLogLine.MODIFY();
    end;


    [EventSubscriber(ObjectType::Table, DATABASE::"To-Do", 'OnBeforeRunCreateTaskPage', '', true, false)]
    local procedure OnBeforeRunCreateTaskPage(var Task: Record "To-do")
    begin
        if Task."ICI Support Ticket No." <> '' THEN
            Commit();
    end;

    [EventSubscriber(ObjectType::Table, DATABASE::"Segment Line", 'OnAfterFinishWizard', '', true, false)]
    local procedure OnAfterFinishWizard(var SegmentLine: Record "Segment Line"; InteractionLogEntry: Record "Interaction Log Entry"; IsFinish: Boolean; Flag: Boolean)
    var
        InteractionLogEntry2: Record "Interaction Log Entry";
    begin
        if SegmentLine."ICI Support Ticket No." <> '' then begin
            InteractionLogEntry2.GET(InteractionLogEntry."Entry No.");
            InteractionLogEntry2."ICI Support Ticket No." := SegmentLine."ICI Support Ticket No.";
            InteractionLogEntry2.MODIFY();
        end;
    end;


    [EventSubscriber(ObjectType::Codeunit, CODEUNIT::SegManagement, 'OnAfterLogInteraction', '', true, false)]
    local procedure OnAfterLogInteraction(var SegmentLine: Record "Segment Line"; var InteractionLogEntry: Record "Interaction Log Entry")
    var
        InteractionLogEntry2: Record "Interaction Log Entry";
    begin
        if SegmentLine."ICI Support Ticket No." <> '' then begin
            InteractionLogEntry2.GET(InteractionLogEntry."Entry No.");
            InteractionLogEntry2."ICI Support Ticket No." := SegmentLine."ICI Support Ticket No.";
            InteractionLogEntry2.MODIFY();
        end;
    end;


    [EventSubscriber(ObjectType::Table, DATABASE::Opportunity, 'OnCreateQuoteOnBeforeSalesHeaderInsert', '', true, false)]
    local procedure OnCreateQuoteOnBeforeSalesHeaderInsert(var SalesHeader: Record "Sales Header"; Opportunity: Record Opportunity)
    begin
        IF Opportunity."ICI Support Ticket No." <> '' then
            SalesHeader.VAlidate("ICI Support Ticket No.", Opportunity."ICI Support Ticket No.");
    end;



    [EventSubscriber(ObjectType::Table, DATABASE::"ICI Support Ticket", 'OnProcessNewTicketJson', '', true, true)]
    local procedure OnProcessNewTicketJson(var ICISupportTIcket: Record "ICI Support Ticket"; JSON: JsonObject);
    var
        lJsonToken: JsonToken;
        DocumentType: Text;
        DocumentNo: Code[20];
    begin
        IF GetJsonToken(Json, 'document_no', lJsonToken) then
            DocumentNo := CopyStr(lJsonToken.AsValue().AsText(), 1, MaxStrLen(DocumentNo));
        IF GetJsonToken(Json, 'document_type', lJsonToken) then
            DocumentType := lJsonToken.AsValue().AsText();

        IF (DocumentNo = '') OR (DocumentType = '') THEN
            EXIT;
        case DocumentType of
            'show_sales_quote':
                ICISupportTIcket.Validate("Sales Quote No.", DocumentNo);
            'show_sales_order':
                ICISupportTIcket.Validate("Sales Order No.", DocumentNo);
            'show_sales_invoice':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_sales_credit_memo':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_sales_blanket_order':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_sales_return_order':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_posted_sales_shipment':
                ICISupportTIcket.Validate("Pst. Sales Shipment No.", DocumentNo);
            'show_posted_sales_invoice':
                ICISupportTIcket.Validate("Pst. Sales Invoice No.", DocumentNo);
            'show_posted_sales_credit_memo':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_service_quote':
                ICISupportTIcket.Validate("Service Quote No.", DocumentNo);
            'show_service_order':
                ICISupportTIcket.Validate("Service Order No.", DocumentNo);
            'show_service_invoice':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_service_credit_memo':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_posted_service_shipment':
                ICISupportTIcket.Validate("Pst. Service Shipment No.", DocumentNo);
            'show_posted_service_invoice':
                ICISupportTIcket.Validate("Pst. Service Invoice No.", DocumentNo);
            'show_posted_service_credit_memo':
                ICISupportTIcket.Validate("Reference No.", DocumentNo); // XXX
            'show_service_contract':
                ICISupportTIcket.Validate("Service Contract No.", DocumentNo);
            'show_service_contract_quote':
                ICISupportTIcket.Validate("Service Contract Quote No.", DocumentNo);
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, CODEUNIT::"ICI Sup. Dragbox Mgt.", 'onAfterInsertFile', '', true, true)]
    local procedure onAfterInsertFile(filename: Text; var B64: Text; var pRecordRef: RecordRef)
    var
        ICIDragboxFile: Record "ICI Sup. Dragbox File";
        ICIDownloadCategory: Record "ICI Download Category";
        lRecordRef: RecordRef;
    begin
        IF pRecordRef.Number() <> DATABASE::"ICI Sup. Dragbox File" THEN
            EXIT;
        pRecordRef.SetTable(ICIDragboxFile);
        ICIDragboxFile.GET(ICIDragboxFile."Entry No."); // Settable is sneaky

        IF ICIDragboxFile."Dragbox Configuration Code" <> UpperCase(ICIDownloadCategory.TableName()) THEN // Code is uppercase
            exit;

        lRecordRef := ICIDragboxFile."Record ID".GetRecord();
        IF lRecordRef.Number() <> DATABASE::"ICI Download Category" THEN
            EXIT;

        lRecordRef.SetTable(ICIDownloadCategory);
        // Code = PQ. Kein Get nötig
        IF lRecordRef.Number() <> DATABASE::"ICI Download Category" THEN
            EXIT;

        ICIDragboxFile."ICI Download Category Code" := ICIDownloadCategory.Code; // Downloadkategorie in Dragboxfile set
        ICIDragboxFile.Modify();

    end;

    [EventSubscriber(ObjectType::Table, DATABASE::Contact, 'OnAfterValidateEvent', 'Name', true, true)]
    local procedure OnAfterValidateContactName(Rec: Record Contact; xRec: Record Contact; CurrFieldNo: Integer)
    var
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        if Rec."No." <> '' then begin
            ICISupportTicket.SetCurrentKey("Current Contact No.", "Company Contact No.", "Ticket State", "Customer No.");
            ICISupportTicket.SetRange("Current Contact No.", Rec."No.");
            IF ICISupportTicket.Count() > 0 then
                ICISupportTicket.MODIFYALL("Curr. Contact Name", Rec.Name, false);
        end;

        CLEAR(ICISupportTicket);
        ICISupportTicket.SetCurrentKey("Current Contact No.", "Company Contact No.", "Ticket State", "Customer No.");
        ICISupportTicket.SetRange("Company Contact No.", Rec."No.");
        IF ICISupportTicket.Count() > 0 then
            ICISupportTicket.MODIFYALL("Comp. Contact Name", Rec.Name, false);
    end;

    [EventSubscriber(ObjectType::Table, DATABASE::Contact, 'OnAfterValidateEvent', 'E-Mail', true, true)]
    local procedure OnAfterValidateContactEMail(Rec: Record Contact; xRec: Record Contact; CurrFieldNo: Integer)
    var
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        ICISupportTicket.SetCurrentKey("Current Contact No.", "Company Contact No.", "Ticket State", "Customer No.");
        ICISupportTicket.SetRange("Current Contact No.", Rec."No.");
        ICISupportTicket.SetRange("Contact E-Mail", xRec."E-Mail");

        IF ICISupportTicket.Count() > 0 then
            ICISupportTicket.MODIFYALL("Contact E-Mail", Rec."E-Mail", false);
    end;

    procedure GetJsonToken(JsonObject: JsonObject;
            TokenKey:
                Text; var jToken: JsonToken):
            Boolean
    begin
        EXIT(JsonObject.GET(TokenKey, jToken));
    end;
}
