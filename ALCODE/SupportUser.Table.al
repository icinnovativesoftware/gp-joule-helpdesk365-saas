table 56279 "ICI Support User"
{
    Caption = 'ICI Support User', Comment = 'de-DE=HelpDesk-Benutzer';
    DataClassification = EndUserIdentifiableInformation;
    LookupPageId = "ICI Support User List";
    DrillDownPageId = "ICI Support User List";

    fields
    {
        field(1; "User ID"; Code[50])
        {
            Caption = 'User ID', Comment = 'de-DE=ID';
            DataClassification = EndUserIdentifiableInformation;

            trigger OnValidate()
            var
                ICISupportMailSetup: Record "ICI Support Mail Setup";
            begin
                IF ICISupportMailSetup.GET() THEN
                    IF ICISupportMailSetup."Salutation Code D" <> '' then
                        Validate("Salutation Code", ICISupportMailSetup."Salutation Code D");
            end;
        }

        field(10; "Name"; Text[250])
        {
            Caption = 'User Name', Comment = 'de-DE=Benutzername';
            DataClassification = CustomerContent;
        }
        field(11; "E-Mail"; Text[250])
        {
            Caption = 'E-Mail', Comment = 'de-DE=E-Mail';
            DataClassification = CustomerContent;
            ExtendedDatatype = EMail;
            trigger OnValidate()
            var
            begin
                IF Login = '' then
                    Validate(Login, "E-Mail");
            end;
        }
        field(12; "Phone No."; Text[250])
        {
            Caption = 'Phone No.', Comment = 'de-DE=Telefonnr.';
            DataClassification = CustomerContent;
            ExtendedDatatype = PhoneNo;
        }
        field(13; "Mobile Phone No."; Text[250])
        {
            Caption = 'Mobile Phone No.', Comment = 'de-DE=Mobiltelefonnr.';
            DataClassification = CustomerContent;
            ExtendedDatatype = PhoneNo;
        }
        field(14; "Salutation Code"; Code[10])
        {
            Caption = 'Salutation Code', Comment = 'de-DE=Anredecode';
            DataClassification = CustomerContent;
            TableRelation = Salutation;
        }
        field(15; "Job Title"; Text[30])
        {
            Caption = 'Job Title', Comment = 'de-DE=Funktion';
            DataClassification = CustomerContent;
        }
        field(16; "First Name"; Text[30])
        {
            Caption = 'First Name', Comment = 'de-DE=Vorname';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                BuildName();
            end;
        }
        field(17; "Middle Name"; Text[30])
        {
            Caption = 'Middle Name', Comment = 'de-DE=Vorname 2';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                BuildName();
            end;
        }
        field(18; Surname; Text[30])
        {
            Caption = 'Surname', Comment = 'de-DE=Nachname';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                BuildName();
            end;
        }
        field(19; "Salesperson Code"; Code[20])
        {
            Caption = 'Salesperson Code', Comment = 'de-DE=Verkäufercode';
            DataClassification = CustomerContent;
            TableRelation = "Salesperson/Purchaser";
            trigger OnValidate()
            begin
                FillDataFromSalesperson();
            end;
        }
        field(20; "Language Code"; Code[10])
        {
            Caption = 'Language Code', Comment = 'de-DE=Sprachcode';
            DataClassification = CustomerContent;
            TableRelation = Language;
        }
        field(21; "Password"; Text[50])
        {
            Caption = 'Password', Comment = 'de-DE=Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
            ObsoleteState = Removed;
            ObsoleteReason = 'Security Update - Only store Password Hash';
        }
        field(22; "Department Filter"; Code[30])
        {
            ObsoleteReason = 'Moved to Dep. Filter';
            ObsoleteState = Removed;
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
            ValidateTableRelation = false;
            Caption = 'Department Filter', Comment = 'de-DE=Abteilungsfilter';
        }

        field(45; "Employee No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Employee No.', Comment = 'de-DE=Mitarbeiternr.';
            TableRelation = Employee;
            trigger OnValidate()
            var
            begin
                FillDataFromEmployee();
            end;
        }
        field(46; Login; Text[100])
        {
            DataClassification = CustomerContent;
            Caption = 'Login', Comment = 'de-DE=Login';
            trigger OnValidate()
            var
                lContact: Record Contact;
                ICISupportUser: Record "ICI Support User";
            begin
                ICISupportUser.SetRange(Login, "Login");
                IF ICISupportUser.Count() > 0 then
                    "Login" := "User ID";

                lContact.SETRANGE("ICI Login", "Login");
                IF lContact.Count() > 0 then
                    "Login" := "User ID";
            end;
        }
        field(47; "Password Hash"; Text[50])
        {
            Caption = 'Password Hash', Comment = 'de-DE=Hash';
            DataClassification = CustomerContent;
        }

        field(48; "Ticket Board Active"; Boolean)
        {
            Caption = 'Ticket Board Active', Comment = 'de-DE=Ticketboard';
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                TicketBoardMgt: Codeunit "ICI Ticket Board Mgt.";
            begin
                IF "Ticket Board Active" then
                    TicketBoardMgt.DefragTicketOrder("User ID");
            end;
        }
        field(49; "Tickets by User Chart Type"; Option)
        {
            Caption = 'Tickets by User Chart Type', Comment = 'de-DE=Tickets pro Benutzer Diagramm Art';
            DataClassification = CustomerContent;
            OptionCaption = 'Point,,Bubble,Line,,StepLine,,,,,Column,StackedColumn,StackedColumn100,Area,,StackedArea,StackedArea100,Pie,Doughnut,,,Range,,,,Radar,,,,,,,,Funnel';
            OptionMembers = Point,,Bubble,Line,,StepLine,,,,,Column,StackedColumn,StackedColumn100,"Area",,StackedArea,StackedArea100,Pie,Doughnut,,,Range,,,,Radar,,,,,,,,Funnel;
        }
        field(50; "Tickets by Company Chart Type"; Option)
        {
            Caption = 'Tickets by Copmany Chart Type', Comment = 'de-DE=Tickets pro Kunde Diagramm Art';
            DataClassification = CustomerContent;
            OptionCaption = 'Point,,Bubble,Line,,StepLine,,,,,Column,StackedColumn,StackedColumn100,Area,,StackedArea,StackedArea100,Pie,Doughnut,,,Range,,,,Radar,,,,,,,,Funnel';
            OptionMembers = Point,,Bubble,Line,,StepLine,,,,,Column,StackedColumn,StackedColumn100,"Area",,StackedArea,StackedArea100,Pie,Doughnut,,,Range,,,,Radar,,,,,,,,Funnel;
        }
        field(51; "Show Mailrobot Cues"; Boolean)
        {
            Caption = 'Show Mailrobot Cues', Comment = 'de-DE=Mailrobot Kacheln anzeigen';
            DataClassification = CustomerContent;
        }
        field(52; "Show Category Cues"; Boolean)
        {
            Caption = 'Show Category Cues', Comment = 'de-DE=Kategorie Kacheln anzeigen';
            DataClassification = CustomerContent;
        }
        field(53; "Show Process Cues"; Boolean)
        {
            Caption = 'Show Process Cues', Comment = 'de-DE=Prozess Kacheln anzeigen';
            DataClassification = CustomerContent;
        }
        field(54; "Business Central User"; Code[50])
        {
            Caption = 'Business Central ID', Comment = 'de-DE=Benutzer ID';
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = "User Setup";

            trigger OnValidate()
            var
            begin

                IF Rec."Business Central User" <> '' THEN begin

                    if Rec."User ID" = '' then
                        Rec."User id" := Rec."Business Central User";

                    FillDataFromUserSetup();
                end;

            end;
        }
        field(55; "Queue"; Boolean)
        {
            Caption = 'Queue', Comment = 'de-DE=Warteschlange';
            DataClassification = CustomerContent;
        }
        field(56; "Show Queue Cues"; Boolean)
        {
            Caption = 'Show Queue Cues', Comment = 'de-DE=Warteschlangen Kacheln anzeigen';
            DataClassification = CustomerContent;
        }
        field(57; "Sales Payoff Type"; Enum "Sales Line Type")
        {
            DataClassification = CustomerContent;
            ValuesAllowed = " ", Item, "G/L Account", Resource;
            Caption = 'Payoff Type', Comment = 'de-DE=Abrechnungsart';

            trigger OnValidate()
            var
                Employee: Record Employee;
                FoundRessourceInEmployeeLbl: Label 'Found Ressource %1 in connected Employee %2. Update Payoff No.?', Comment = '%1 = Found Ressource No;%2=Employee No.|de-DE=Ressource %1 im verknüpften Mitarbeiter %2 gefunden. Soll diese Ressource als Abrechnungsnummer übernommen werden?';
            begin
                IF "Sales Payoff Type" <> xRec."Sales Payoff Type" THEN
                    "Sales Payoff No." := '';

                IF ("Sales Payoff Type" = "Sales Payoff Type"::Resource) AND ("Employee No." <> '') then
                    IF Employee.GET("Employee No.") then
                        IF Employee."Resource No." <> '' then
                            IF Confirm(FoundRessourceInEmployeeLbl, true, Employee."Resource No.", "Employee No.") then
                                "Sales Payoff No." := Employee."Resource No.";
            end;
        }
        field(58; "Sales Payoff No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Payoff No.', Comment = 'de-DE=Abrechnungsnr.';
            TableRelation = IF ("Sales Payoff Type" = CONST(" ")) "Standard Text"
            ELSE
            IF ("Sales Payoff Type" = CONST("G/L Account")) "G/L Account"
            ELSE
            IF ("Sales Payoff Type" = CONST("Resource")) Resource
            ELSE
            IF ("Sales Payoff Type" = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF ("Sales Payoff Type" = CONST("Charge (Item)")) "Item Charge"
            ELSE
            IF ("Sales Payoff Type" = CONST(Item)) Item;
            ValidateTableRelation = false;
        }
        field(59; "Dep. Filter"; Text[100])
        {
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
            ValidateTableRelation = false;
            Caption = 'Department Filter', Comment = 'de-DE=Abteilungsfilter';
            trigger OnLookup()
            begin
                "Dep. Filter" := LookupDepartmentFilterFilter();
            end;
        }
        field(60; "Dep. Code"; Code[30])
        {
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
            ValidateTableRelation = false;
            Caption = 'Department Code', Comment = 'de-DE=Abteilungscode';
        }
        field(61; "Show ToDo Cues"; Boolean)
        {
            Caption = 'Show ToDo Cues', Comment = 'de-DE=Aufgaben Kacheln anzeigen';
            DataClassification = CustomerContent;
        }

        #region Search-Fields
        field(1000; "Search S.O. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order by No.', Comment = 'de-DE=Verkaufsaufträge nach Belegnr.';
            InitValue = true;
        }
        field(1001; "Search S.O. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order by External Document No.', Comment = 'de-DE=Verkaufsaufträge nach Externer Belegnr.';
            InitValue = true;
        }
        field(1002; "Search S.O. by Quote No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order by Quote No.', Comment = 'de-DE=Verkaufsaufträge nach Angebotsgnr.';
            InitValue = true;
        }
        field(1003; "Search S.Q. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Quote by No.', Comment = 'de-DE=Verkaufsangebote nach Belegnr.';
            InitValue = true;
        }
        field(1004; "Search S.Q. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Quote by External Document No.', Comment = 'de-DE=Verkaufsangebote nach Externer Belegnr.';
            InitValue = true;
        }
        field(1005; "Search S.I. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Invoices by No.', Comment = 'de-DE=Geb. Rechnungen nach Belegnr.';
            InitValue = true;
        }
        field(1006; "Search S.I. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Invoices by External Document No.', Comment = 'de-DE=Geb. Rechnungen nach Externer Belegnr.';
            InitValue = true;
        }
        field(1007; "Search S.I. by Order No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Invoices by Order No.', Comment = 'de-DE=Geb. Rechnungen nach Auftragsnr.';
            InitValue = true;
        }
        field(1008; "Search S.I. by Quote No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Invoices by Quote No.', Comment = 'de-DE=Geb. Rechnungen nach Angebotsnr.';
            InitValue = true;
        }

        field(1009; "Search S.S. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Shipment by No.', Comment = 'de-DE=Geb. Lieferungen nach Belegnr.';
            InitValue = true;
        }
        field(1010; "Search S.S. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Shipment by External Document No.', Comment = 'de-DE=Geb. Lieferungen nach Externer Belegnr.';
            InitValue = true;
        }
        field(1011; "Search S.S. by Order No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Shipment by Order No.', Comment = 'de-DE=Geb. Lieferungen nach Auftragsnr.';
            InitValue = true;
        }
        field(1012; "Search S.S. by Quote No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Posted Sales Shipment by Quote No.', Comment = 'de-DE=Geb. Lieferungen nach Angebotsnr.';
            InitValue = true;
        }
        field(1100; "Search Se. It. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Items by No.', Comment = 'de-DE=Serviceartikel nach Nr.';
            InitValue = true;
        }
        field(1101; "Search Se. It. by Serial No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Items by Serial No.', Comment = 'de-DE=Serviceartikel nach Seriennr.';
            InitValue = true;
        }
        field(1102; "Search Se. It. by Item No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Items by Item No.', Comment = 'de-DE=Serviceartikel nach Artikelnr.';
            InitValue = true;
        }
        field(1103; "Search Se. It. by Search Desc."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Items by Search Desc.', Comment = 'de-DE=Serviceartikel nach Suchbegriff';
            InitValue = true;
        }
        field(1104; "Search Se. It. by No. 2"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Items by No. 2', Comment = 'de-DE=Serviceartikel nach Nr. 2 (HelpDesk)';
            InitValue = true;
        }

        field(1200; "Search Serv. Q. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Quote by No.', Comment = 'de-DE=Serviceangebote nach Belegnr.';
            InitValue = true;
        }
        field(1201; "Search Serv. O. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Order by No.', Comment = 'de-DE=Serviceaufträge nach Belegnr.';
            InitValue = true;
        }
        field(1202; "Search Serv. O. by Quote No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Order by Quote No.', Comment = 'de-DE=Serviceaufträge nach Angebotsnr.';
            InitValue = true;
        }
        field(1203; "Search Serv. I. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Invoice by No.', Comment = 'de-DE=Geb. Servicerechnung nach Belegnr.';
            InitValue = true;
        }
        field(1204; "Search Serv. S. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Shipment by No.', Comment = 'de-DE=Geb. Servicelieferung nach Belegnr.';
            InitValue = true;
        }
        field(1205; "Search Serv. C. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Contract by No.', Comment = 'de-DE=Servicevertrag nach Vertragsnr.';
            InitValue = true;
        }
        field(1206; "Search Serv. C. Q. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Service Contract Quote by No.', Comment = 'de-DE=Servicevertragsangebot nach Vertragsnr.';
            InitValue = true;
        }

        field(1013; "Search S.O.A. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order Archive by No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Belegnr.';
            InitValue = true;
        }
        field(1014; "Search S.O.A. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order Archive by External Document No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Externer Belegnr.';
            InitValue = true;
        }
        field(1015; "Search S.O.A. by Quote No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Order Archive by Quote No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Angebotsgnr.';
            InitValue = true;
        }
        field(1016; "Search S.Q.A. by No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Quote Archive by No.', Comment = 'de-DE=Verkaufsangebotsarchiv nach Belegnr.';
            InitValue = true;
        }
        field(1017; "Search S.Q.A. by Ext. Doc. No."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Sales Quote Archive by External Document No.', Comment = 'de-DE=Verkaufsangebotsarchiv nach Externer Belegnr.';
            InitValue = true;
        }
        field(1300; "Search Cust. by Addr."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Customer by Address', Comment = 'de-DE=Debitor nach Adresse suchen';
            InitValue = true;
        }
        field(1301; "Search Contacts by Addr."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search Contacts by Address', Comment = 'de-DE=Kontakte nach Adresse suchen';
            InitValue = true;
        }
        field(1302; "Search ServiceItems by Addr."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search ServiceItems by Address', Comment = 'de-DE=Serviceartikel nach Adresse suchen';
            InitValue = true;
        }
        field(1303; "Search SI by Ship-To Addr."; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search ServiceItems by Ship To Address', Comment = 'de-DE=Serviceartikel nach Lieferadresse suchen';
            InitValue = true;
        }
        field(1304; "Search Ship-To Address"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Search in Ship To Address', Comment = 'de-DE=Lieferadressen suchen';
            InitValue = true;
        }
        #endregion
    }
    keys
    {
        key(PK; "User ID")
        {
            Clustered = true;
        }
        key(BCUser; "Business Central User") { }


    }
    trigger OnInsert()
    var
        ICISupportSetup: Record "ICI Support Setup";
        MailrobotMailBox: Record "ICI Mailrobot Mailbox";
    begin

        IF NOT ICISupportSetup.GET() THEN
            EXIT;

        "Sales Payoff No." := ICISupportSetup."Sales Payoff No.";
        "Sales Payoff Type" := ICISupportSetup."Sales Payoff Type";

        "Tickets by User Chart Type" := "Tickets by User Chart Type"::Column;
        "Tickets by Company Chart Type" := "Tickets by Company Chart Type"::Pie;

        "Show ToDo Cues" := ICISupportSetup."Task Integration";
        "Show Mailrobot Cues" := Not MailrobotMailBox.IsEmpty;
        "Show Queue Cues" := true;


    end;

    procedure CreateFromBCUser()
    var
        BCUser: Record "User Setup";
        User: Record User;
        SupportUser: Record "ICI Support User";
    begin
        IF BCUser.findset then
            repeat
                user.setrange("User Name", bcuser."User ID");
                user.SetRange(State, user.state::Enabled);
                if user.FindFirst() then begin
                    SupportUser.init;
                    SupportUser.validate("Business Central User", bcuser."User ID");
                    SupportUser.validate("User ID", BCUser."User ID");
                    SupportUser.Validate(Name, user."Full Name");
                    if supportuser.insert then;

                end;

            until BCUser.Next() = 0;

    end;

    procedure CreateFromSalesperson()
    var
        Salesperson: Record "Salesperson/Purchaser";
        SupportUser: Record "ICI Support User";
    begin
        IF Salesperson.findset then
            repeat
                SupportUser.init;
                SupportUser.Validate("Salesperson Code", Salesperson.Code);
                SupportUser.validate("User ID", Salesperson.Code);
                SupportUser.Validate(Name, Salesperson.Name);
                if supportuser.insert then;
            until Salesperson.Next() = 0;

    end;


    procedure GetSalutation(SalutationType: Enum "Salutation Formula Salutation Type"; LanguageCode: Code[10]) Salutation: Text[260]
    var
        SalutationFormula: Record "Salutation Formula";
        NamePart: array[5] of Text[100];
    begin

        if not SalutationFormula.Get("Salutation Code", LanguageCode, SalutationType) then
            Error(SalutationErr, LanguageCode, "User ID");
        SalutationFormula.TestField(Salutation);

        case SalutationFormula."Name 1" of
            SalutationFormula."Name 1"::"Job Title":
                NamePart[1] := "Job Title";
            SalutationFormula."Name 1"::"First Name":
                NamePart[1] := "First Name";
            SalutationFormula."Name 1"::"Middle Name":
                NamePart[1] := "Middle Name";
            SalutationFormula."Name 1"::Surname:
                NamePart[1] := Surname;
            SalutationFormula."Name 1"::Initials:
                NamePart[1] := '';
            SalutationFormula."Name 1"::"Company Name":
                NamePart[1] := CopyStr(CompanyName(), 1, 100);
        end;

        case SalutationFormula."Name 2" of
            SalutationFormula."Name 2"::"Job Title":
                NamePart[2] := "Job Title";
            SalutationFormula."Name 2"::"First Name":
                NamePart[2] := "First Name";
            SalutationFormula."Name 2"::"Middle Name":
                NamePart[2] := "Middle Name";
            SalutationFormula."Name 2"::Surname:
                NamePart[2] := Surname;
            SalutationFormula."Name 2"::Initials:
                NamePart[2] := '';
            SalutationFormula."Name 2"::"Company Name":
                NamePart[2] := CopyStr(CompanyName(), 1, 100);
        end;

        case SalutationFormula."Name 3" of
            SalutationFormula."Name 3"::"Job Title":
                NamePart[3] := "Job Title";
            SalutationFormula."Name 3"::"First Name":
                NamePart[3] := "First Name";
            SalutationFormula."Name 3"::"Middle Name":
                NamePart[3] := "Middle Name";
            SalutationFormula."Name 3"::Surname:
                NamePart[3] := Surname;
            SalutationFormula."Name 3"::Initials:
                NamePart[3] := '';
            SalutationFormula."Name 3"::"Company Name":
                NamePart[3] := CopyStr(CompanyName(), 1, 100);
        end;

        case SalutationFormula."Name 4" of
            SalutationFormula."Name 4"::"Job Title":
                NamePart[4] := "Job Title";
            SalutationFormula."Name 4"::"First Name":
                NamePart[4] := "First Name";
            SalutationFormula."Name 4"::"Middle Name":
                NamePart[4] := "Middle Name";
            SalutationFormula."Name 4"::Surname:
                NamePart[4] := Surname;
            SalutationFormula."Name 4"::Initials:
                NamePart[4] := '';
            SalutationFormula."Name 4"::"Company Name":
                NamePart[4] := CopyStr(CompanyName(), 1, 100);
        end;

        case SalutationFormula."Name 5" of
            SalutationFormula."Name 5"::"Job Title":
                NamePart[5] := "Job Title";
            SalutationFormula."Name 5"::"First Name":
                NamePart[5] := "First Name";
            SalutationFormula."Name 5"::"Middle Name":
                NamePart[5] := "Middle Name";
            SalutationFormula."Name 5"::Surname:
                NamePart[5] := Surname;
            SalutationFormula."Name 5"::Initials:
                NamePart[5] := '';
            SalutationFormula."Name 5"::"Company Name":
                NamePart[5] := CopyStr(CompanyName(), 1, 100);
        end;


        exit(GetSalutationString(SalutationFormula, NamePart));
    end;

    local procedure GetSalutationString(SalutationFormula: Record "Salutation Formula"; NamePart: array[5] of Text[100]) SalutationString: Text[260]
    var
        SubStr: Text;
        i: Integer;
    begin
        for i := 1 to 5 do
            if NamePart[i] = '' then begin
                SubStr := '%' + Format(i) + ' ';
                if StrPos(SalutationFormula.Salutation, SubStr) > 0 then
                    SalutationFormula.Salutation :=
                      DelStr(SalutationFormula.Salutation, StrPos(SalutationFormula.Salutation, SubStr), 3);
            end;
        SalutationString := CopyStr(StrSubstNo(SalutationFormula.Salutation, NamePart[1], NamePart[2], NamePart[3], NamePart[4], NamePart[5]), 1, MaxStrLen(SalutationString));

    end;

    local procedure BuildName()
    var
        NameText: Text;
    begin
        IF "First Name" <> '' then
            NameText := "First Name";
        IF "Middle Name" <> '' then
            IF NameText = '' then
                NameText := "Middle Name"
            else
                NameText += ' ' + "Middle Name";
        IF Surname <> '' THEN
            IF NameText = '' then
                NameText := Surname
            else
                NameText += ' ' + Surname;

        Validate(Name, NameText);
    end;

    procedure GeneratePassword() RandomPass: Text[50]
    var
        CryptographyManagement: Codeunit "Cryptography Management";
        HashAlgorithmType: Option MD5,SHA1,SHA256,SHA384,SHA512;
    begin
        RandomPass := CREATEGUID();
        RandomPass := DELCHr(RandomPass, '=', '{}-');
        RandomPass := copystr(RandomPass, 1, 12);

        VALIDATE("Password Hash", CryptographyManagement.GenerateHash(RandomPass, HashAlgorithmType::MD5));
        Modify(TRUE);
    end;

    procedure GetCurrUser(var ICISupportUser: Record "ICI Support User"): Boolean
    var
        ICISupportUser2: Record "ICI Support User";
    begin
        IF NOT Rec.ReadPermission THEN
            EXIT(false);

        ICISupportUser.SetCurrentKey("Business Central User");
        ICISupportUser.SetRange("Business Central User", Userid());
        exit(ICISupportUser.FindFirst());
    end;

    procedure FillDataFromEmployee()
    var
        Employee: Record Employee;
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        if "Employee No." = '' then
            exit;

        Employee.GET("Employee No.");

        IF (Name = '') then begin
            Validate("First Name", Employee."First Name");
            validate("Middle Name", Employee."Middle Name");
            Validate(Surname, Employee."Last Name");
            Validate(Name, employee.FullName());
        end;

        if "Job Title" = '' then
            validate("Job Title", employee."Job Title");

        IF ("E-Mail" = '') THEN
            Validate("E-Mail", Employee."E-Mail");

        IF ("Phone No." = '') THEN
            Validate("Phone No.", Employee."Phone No.");

        IF ("Mobile Phone No." = '') THEN
            Validate("Mobile Phone No.", Employee."Mobile Phone No.");

        IF ("Salesperson Code" = '') THEN
            Validate("Salesperson Code", Employee."Salespers./Purch. Code");

        IF ("Salutation Code" = '') then begin

            case Employee.Gender of
                Employee.Gender::Male:
                    Validate("Salutation Code", ICISupportMailSetup."Salutation Code M");
                Employee.Gender::Female:
                    Validate("Salutation Code", ICISupportMailSetup."Salutation Code F");
                Employee.Gender::" ":
                    Validate("Salutation Code", ICISupportMailSetup."Salutation Code D");
            end;
        end;

    end;

    procedure FillDataFromSalesperson()
    var
        SalespersonPurchaser: Record "Salesperson/Purchaser";
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF "Salesperson Code" = '' THEN
            EXIT;

        SalespersonPurchaser.GET("Salesperson Code");

        IF (Name = '') then
            Validate("Name", SalespersonPurchaser."Name");

        IF ("E-Mail" = '') THEN
            Validate("E-Mail", SalespersonPurchaser."E-Mail");

        IF ("Phone No." = '') THEN
            Validate("Phone No.", SalespersonPurchaser."Phone No.");

        IF ("Job Title" = '') THEN
            Validate("Job Title", SalespersonPurchaser."Job Title");

    end;

    procedure FillDataFromUserSetup()
    var
        UserSetup: Record "User Setup";
        user: Record user;
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        IF "Business Central User" = '' THEN
            exit;


        IF UserSetup.GET(Rec."Business Central User") THEN begin

            IF "E-Mail" = '' THEN
                Validate("E-Mail", UserSetup."E-Mail");

            IF "Phone No." = '' then
                Validate("Phone No.", UserSetup."Phone No.");

            IF "Salesperson Code" = '' then
                Validate("Salesperson Code", UserSetup."Salespers./Purch. Code");

        end;

        User.SetRange("User Name", "User ID");
        IF User.FindFirst() then begin

            IF name = '' then
                Validate(Name, User."Full Name");

            IF ("E-Mail" = '') THEN
                Validate("E-Mail", User."Contact Email");
        end;


    end;

    local procedure LookupDepartmentFilterFilter(): Text[100]
    var

        ICIDepartment: Record "ICI Department";
        ICIDepartments: Page "ICI Departments";
        FilterString: Text;
    begin


        ICIDepartments.EDITABLE(FALSE);
        ICIDepartments.LOOKUPMODE(TRUE);
        IF ICIDepartments.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            ICIDepartments.SetSelectionFilter(ICIDepartment);
            ICIDepartment.MARKEDONLY(TRUE);
            IF ICIDepartment.FINDSET() THEN BEGIN
                CLEAR(FilterString);
                REPEAT
                    IF FilterString = '' THEN
                        FilterString := ICIDepartment.Code
                    ELSE
                        FilterString := (FilterString + '|' + ICIDepartment.Code);
                UNTIL ICIDepartment.NEXT() <= 0;
            END;

            EXIT(COPYSTR(FilterString, 1, 100));
        end;
    end;

    var
        SalutationErr: Label 'You have to set up formal and informal salutation formulas in %1  language for the %2 support user.', Comment = '%1=Language Code;%2=UserID;de-DE=Bitte setzten Sie formelle und informelle Anredecodes für die Sprache %1 für User %2';
}
