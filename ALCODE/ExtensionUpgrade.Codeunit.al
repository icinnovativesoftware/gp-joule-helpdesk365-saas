codeunit 56299 "ICI Extension Upgrade"

{
    Subtype = Upgrade;

    trigger OnUpgradePerCompany()
    begin
        PerformUpgradeTo10015();
        PerformUpgradeTo10017();
        //PerformUpgradeTo10020();
        PerformUpgradeTo10025();
        // Add new Upgrades here
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Upgrade Tag", 'OnGetPerCompanyUpgradeTags', '', false, false)]
    local procedure OnGetPerCompanyUpgradeTags(var PerCompanyUpgradeTags: List of [Code[250]]);
    begin
        PerCompanyUpgradeTags.Add("Reason10015Lbl");
        PerCompanyUpgradeTags.Add("Reason10017Lbl");
        PerCompanyUpgradeTags.Add("Reason10020Lbl");
        PerCompanyUpgradeTags.Add("Reason10025Lbl");
        // Add new Upgrade Tags here
    end;

    local procedure PerformUpgradeTo10015()
    var
        ICISupportTicket: Record "ICI Support Ticket";
        UpgradeTag: Codeunit "Upgrade Tag";
    begin
        if UpgradeTag.HasUpgradeTag(Reason10015Lbl) then exit; // Check for Upgrade Tag - dont execute twice
        ICISupportTicket.SetAutoCalcFields("Current Contact Name", "Company Contact Name", "Customer Name");
        if ICISupportTicket.FindSet() then
            repeat
                    ICISupportTicket."Curr. Contact Name" := ICISupportTicket."Current Contact Name";
                ICISupportTicket."Comp. Contact Name" := ICISupportTicket."Company Contact Name";
                ICISupportTicket."Cust. Name" := ICISupportTicket."Customer Name";

                ICISupportTicket.modify(false);
            until ICISupportTicket.Next() < 1;

        UpgradeTag.SetUpgradeTag(Reason10015Lbl);// Register Upgrade Tag as successfully run
    end;

    local procedure PerformUpgradeTo10017()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportUser: Record "ICI Support User";
        UpgradeTag: Codeunit "Upgrade Tag";
    begin
        if UpgradeTag.HasUpgradeTag(Reason10017Lbl) then exit; // Check for Upgrade Tag - dont execute twice
        IF ICISupportSetup.GET() THEN begin // Dont exit, else SetUpgradeTag is not called
            ICISupportSetup."Cue 1 - Category 1 Filter" := ICISupportSetup."Cue 1 - Category 1 Code";
            ICISupportSetup."Cue 1 - Category 2 Filter" := ICISupportSetup."Cue 1 - Category 2 Code";

            ICISupportSetup."Cue 2 - Category 1 Filter" := ICISupportSetup."Cue 2 - Category 1 Code";
            ICISupportSetup."Cue 2 - Category 2 Filter" := ICISupportSetup."Cue 2 - Category 2 Code";

            ICISupportSetup."Cue 3 - Category 1 Filter" := ICISupportSetup."Cue 3 - Category 1 Code";
            ICISupportSetup."Cue 3 - Category 2 Filter" := ICISupportSetup."Cue 3 - Category 2 Code";

            ICISupportSetup."Cue 4 - Category 1 Filter" := ICISupportSetup."Cue 4 - Category 1 Code";
            ICISupportSetup."Cue 4 - Category 2 Filter" := ICISupportSetup."Cue 4 - Category 2 Code";

            ICISupportSetup.Modify(false);
        end;

        IF ICISupportUser.FindSet() THEN
            repeat
                    ICISupportUser."Dep. Filter" := ICISupportUser."Department Filter";
                ICISupportUser."Dep. Code" := ICISupportUser."Department Filter";

                ICISupportUser.Modify(false);
            until ICISupportUser.Next() = 0;

        UpgradeTag.SetUpgradeTag(Reason10017Lbl);// Register Upgrade Tag as successfully run
    end;

    local procedure PerformUpgradeTo10020()
    var
        EscalationLevel: Record "ICI Escalation Level";
        EscalationGroup: Record "ICI Escalation Group";
        SupportProcessStage: Record "ICI Support Process Stage";
        UpgradeTag: Codeunit "Upgrade Tag";
        UpgradeLbl: Label 'UPG%1%2', Comment = '%1 = EscalationCode; %2=EscalationLineNo';
    begin
        if UpgradeTag.HasUpgradeTag(Reason10020Lbl) then exit; // Check for Upgrade Tag - dont execute twice
        // By State
        EscalationLevel.SetRange(Condition, EscalationLevel.Condition::Processstate);
        IF EscalationLevel.FindSet() THEN
            repeat
                    CLEAR(SupportProcessStage);
                CLEAR(EscalationGroup);
                // Create Escalation Group
                EscalationGroup.Init();
                EscalationGroup.VALIDATE(Code, COPYSTR(STRSUBSTNO(UpgradeLbl, EscalationLevel."Escalation Code", EscalationLevel."Line No."), 1, 20));
                EscalationGroup.VALIDATE(Description, COPYSTR(Format(EscalationLevel), 1, 100));
                EscalationGroup.Insert();
                // Add Lines To Escalation Group
                SupportProcessStage.SetRange("Ticket State", EscalationLevel."Ticket State");
                IF SupportProcessStage.FindSet() THEN
                    repeat
                            EscalationGroup.AddStage(SupportProcessStage);
                    until SupportProcessStage.Next() = 0;

                // Set Group Code
                EscalationLevel."Escalation Group Code" := EscalationGroup.Code;
                EscalationLevel.Modify();
            until EscalationLevel.Next() = 0;

        // By Last Action
        EscalationLevel.SetRange(Condition, EscalationLevel.Condition::Response);
        IF EscalationLevel.FindSet() THEN
                repeat
                    CLEAR(SupportProcessStage);
                    CLEAR(EscalationGroup);
                    // Create Escalation Group
                    EscalationGroup.Init();
                    EscalationGroup.VALIDATE(Code, COPYSTR(STRSUBSTNO(UpgradeLbl, EscalationLevel."Escalation Code", EscalationLevel."Line No."), 1, 20));
                    EscalationGroup.VALIDATE(Description, COPYSTR(Format(EscalationLevel), 1, 100));
                    EscalationGroup.Insert();
                    // Add Lines To Escalation Group
                    IF SupportProcessStage.FindSet() THEN // Add all Process Stages
                            repeat
                                EscalationGroup.AddStage(SupportProcessStage);
                            until SupportProcessStage.Next() = 0;

                    // Set Group Code
                    EscalationLevel."Escalation Group Code" := EscalationGroup.Code;
                    EscalationLevel."Reset with Ticket Action" := true;
                    EscalationLevel.Modify();
                until EscalationLevel.Next() = 0;

        // By Stage
        EscalationLevel.SetRange(Condition, EscalationLevel.Condition::Processstage);
        IF EscalationLevel.FindSet() THEN
            repeat
                    CLEAR(SupportProcessStage);
                CLEAR(EscalationGroup);
                // Create Escalation Group
                EscalationGroup.Init();
                EscalationGroup.VALIDATE(Code, COPYSTR(STRSUBSTNO(UpgradeLbl, EscalationLevel."Escalation Code", EscalationLevel."Line No."), 1, 20));
                EscalationGroup.VALIDATE(Description, COPYSTR(Format(EscalationLevel), 1, 100));
                EscalationGroup.Insert();
                // Add Lines To Escalation Group
                SupportProcessStage.SetRange("Process Code", EscalationLevel."Ticket Process");
                SupportProcessStage.SetRange(Stage, EscalationLevel."Ticket Process Stage");
                IF SupportProcessStage.FindSet() THEN
                        repeat
                            EscalationGroup.AddStage(SupportProcessStage);
                        until SupportProcessStage.Next() = 0;
                // Set Group Code
                EscalationLevel."Escalation Group Code" := EscalationGroup.Code;
                EscalationLevel.Modify();
            until EscalationLevel.Next() = 0;

        UpgradeTag.SetUpgradeTag(Reason10020Lbl);// Register Upgrade Tag as successfully run
    end;

    local procedure PerformUpgradeTo10025()
    var
        ServiceItem: Record "Service Item";
        UpgradeTag: Codeunit "Upgrade Tag";
    begin
        if UpgradeTag.HasUpgradeTag(Reason10025Lbl) then exit; // Check for Upgrade Tag - dont execute twice
        ServiceItem.SetAutoCalcFields(Address, "Address 2", "Post Code", City, "Ship-to Address", "Ship-to Address 2", "Ship-to Post Code", "Ship-to City");
        if ServiceItem.FindSet() then
                repeat
                    ServiceItem."ICI Address" := ServiceItem.Address;
                    ServiceItem."ICI Address 2" := ServiceItem."Address 2";
                    ServiceItem."ICI Post Code" := ServiceItem."Post Code";
                    ServiceItem."ICI City" := ServiceItem.City;
                    ServiceItem."ICI Ship-to Address" := ServiceItem."Ship-to Address";
                    ServiceItem."ICI Ship-to Address 2" := ServiceItem."Ship-to Address 2";
                    ServiceItem."ICI Ship-to Post Code" := ServiceItem."Ship-to Post Code";
                    ServiceItem."ICI Ship-to City" := ServiceItem."Ship-to City";

                    ServiceItem.modify(false);
                until ServiceItem.Next() < 1;

        UpgradeTag.SetUpgradeTag(Reason10025Lbl);// Register Upgrade Tag as successfully run
    end;



    var
        "Reason10015Lbl": Label 'HelpDesk-1.0.0.15-TicketContactNames', Locked = true;
        "Reason10017Lbl": Label 'HelpDesk-1.0.0.17-TicketCategoryCueCodeToFilter', Locked = true;
        "Reason10020Lbl": Label 'HelpDesk-1.0.0.20-EscalationConditionAndGroups', Locked = true;
        "Reason10025Lbl": Label 'HelpDesk-1.0.0.25-ServiceItemAddressSearch', Locked = true;
}
