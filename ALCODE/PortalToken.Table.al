table 56311 "ICI Portal Token"
{
    Caption = 'Portal Token', Comment = 'de-DE=Kundenportal Token';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.', Comment = 'de-DE=Lfd. Nr.';
            DataClassification = SystemMetadata;
            AutoIncrement = true;
            Editable = false;
        }
        field(10; "Ticket No."; Code[20])
        {
            Caption = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket";
        }
        field(11; "Contact No."; Code[20])
        {
            Caption = 'Contact No.', Comment = 'de-DE=Kontaktnr.';
            DataClassification = CustomerContent;
            TableRelation = Contact;
        }
        field(12; "Create Datetime"; DateTime)
        {
            Caption = 'Create Datetime', Comment = 'de-DE=Erstellt am';
            DataClassification = SystemMetadata;
        }
        field(13; Token; Text[32])
        {
            Caption = 'Token', Comment = 'de-DE=Token';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
        key(SearchContacts; "Contact No.", "Create Datetime")
        {

        }
        key(SearchContacts2; "Contact No.", "Ticket No.", "Create Datetime")
        {

        }
        key(SearchTokens; "Token")
        {

        }
    }

    trigger OnInsert()
    var

    begin
        Token := COPYSTR(DELCHR(FORMAT(CreateGuid()), '=', '{}-'), 1, 32); // GUID 32 Zeichen lang ohne sonderzeichen
        "Create Datetime" := CurrentDateTime();
    end;

    procedure CreateForTicket(var ICIPortalToken: Record "ICI Portal Token"; var ICISupportTicket: Record "ICI Support Ticket");
    begin
        ICIPortalToken.Init();
        ICIPortalToken.Insert(true);
        ICIPortalToken.Validate("Ticket No.", ICISupportTicket."No.");
        ICIPortalToken.Validate("Contact No.", ICISupportTicket."Current Contact No.");
        ICIPortalToken.Modify(true);
    end;

    procedure CreateForContact(var ICIPortalToken: Record "ICI Portal Token"; var Contact: Record Contact);
    begin
        ICIPortalToken.Init();
        ICIPortalToken.Insert(true);
        ICIPortalToken.Validate("Contact No.", Contact."No.");
        ICIPortalToken.Modify(true);
    end;

    procedure GetCurrentContactToken(ContactNo: Code[20]): Text[32]
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIPortalToken: Record "ICI Portal Token";
    begin
        ICISupportSetup.GET();
        ICIPortalToken.SetRange("Contact No.", ContactNo);
        ICIPortalToken.SetFilter("Ticket No.", '=%1', '');
        IF NOT ICIPortalToken.FindLast() then
            EXIT('');

        IF FORMAT(ICISupportSetup."Token Duration") = '' THEN // Wenn Tokens nicht ablaufen nur zurückgeben
            EXIT(ICIPortalToken.Token);

        IF (ICIPortalToken."Create Datetime" + ICISupportSetup."Token Duration") > CurrentDateTime() THEN // Token ist noch gültig
            Exit(ICIPortalToken.Token);

        Exit('')
    end;
}
