query 56276 "ICI Tickets Per Item"
{
    elements
    {
        dataitem(Item; Item)
        {
            SqlJoinType = InnerJoin;
            column(No; "No.")
            {
            }
            column(Description; Description)
            {
            }
            dataitem(ServiceItem; "Service Item")
            {
                DataItemLink = "Item No." = Item."No.";
                SqlJoinType = InnerJoin;
                dataitem(QueryElement1000000004; "ICI Support Ticket")
                {
                    DataItemLink = "Service Item No." = ServiceItem."No.";
                    column(State; "Ticket State")
                    {
                    }
                    column(Count_)
                    {
                        ColumnFilter = Count_ = FILTER(> 0);
                        Method = Count;
                    }
                }
            }
        }
    }
}



