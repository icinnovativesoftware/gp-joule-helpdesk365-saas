pageextension 56323 "ICI Item Card" extends "Item Card"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Item Dragbox"; "ICI Item Dragbox")
            {
                SubPageLink = "No." = field("No.");
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
    }
}
