page 56379 "ICI Support Portal Setup"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Support Portal Setup', Comment = 'de-DE=Portal Einrichtung';
    PageType = Card;
    SourceTable = "ICI Support Setup";

    layout
    {
        area(content)
        {
            group(Portal)
            {
                Visible = Rec."Portal Integration";
                Caption = 'Portal', Comment = 'de-DE=Kundenportal allgemein';
                group(URLS)
                {
                    Caption = 'URLS', Comment = 'de-DE=Link zum Portal';
                    field("Link to Portal"; Rec."Link to Portal")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Link to Portal', Comment = 'de-DE=Kundenportal';
                        ExtendedDatatype = URL;
                        Importance = Promoted;
                        ShowMandatory = true;
                    }
                    field("Link to CMS"; Rec."Link to CMS")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Link to CMS', Comment = 'de-DE=Portal-CMS';
                        ExtendedDatatype = URL;
                        ShowMandatory = true;
                    }
                }
                group(ExtURLS)
                {
                    Caption = 'URLS', Comment = 'de-DE=Eingebundene Links';
                    field("Url - AGB"; Rec."Url - AGB")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Url - AGB', Comment = 'de-DE=Url - AGB';
                        ExtendedDatatype = URL;
                        ShowMandatory = true;
                    }
                    field("Url - DSGVO"; Rec."Url - DSGVO")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Url - GDPR', Comment = 'de-DE=Url - DSGVO';
                        ExtendedDatatype = URL;
                        ShowMandatory = true;
                    }
                    field("Url - Imprint"; Rec."Url - Imprint")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Url - Imprint', Comment = 'de-DE=Url - Imprint';
                        ExtendedDatatype = URL;
                        ShowMandatory = true;
                    }
                }

                group(PortalOptions)
                {
                    Caption = 'PortalOptions', Comment = 'de-DE=Portaloptionen';
                    field("Portal - Change Data Allowed"; Rec."Portal - Change Data Allowed")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Portal - Change Data Allowed', Comment = 'de-DE=Gibt an, ob die Kunden im Kundenportal ihre Stammdaten ändern können. Hinweis: Passwort ändern ist nicht von dieser Funktion betroffen und stets möglich';
                        Importance = Promoted;

                    }

                    field("Portal - Change Pass. allowed"; Rec."Portal - Change Pass. allowed")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Portal - Change Pass. allowed', Comment = 'de-DE=Gibt an, ob das Passwort vom Kunden geändert werden kann';
                        Importance = Promoted;

                        trigger OnValidate()
                        begin
                            CurrPage.Update();
                        end;
                    }
                    field("ICI Portal Cue Set Code"; Rec."Default Portal Cue Set Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Default Portal Cue Set Code', Comment = 'de-DE=Std. Kundenportal Kacheln';
                    }
                    field("Service Registration Type"; Rec."Service Registration Type")
                    {
                        ApplicationArea = All;
                        ToolTip = 'selects whether the customer inserts Service Items by DropDown (Option Choice) or by serial number', Comment = 'de-DE=legt fest, ob der Kunde Serviceartikel über ein DropDown (Option Auswahl) oder über Seriennummer eingibt';
                    }

                    field("Max. Login Failures"; Rec."Max. Login Failures")
                    {
                        ApplicationArea = All;
                        Importance = Promoted;

                        ToolTip = 'Max. Login Failures', Comment = 'de-DE=Maximale Anzahl an Fehlerhaften Anmeldeversuchen, bis der Zugang gesperrt wird.';
                    }
                    field("Token Duration"; Rec."Token Duration")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Token Duration', Comment = 'de-DE=Gibt an, wie lange ein Login-Token gültig ist.';
                    }

                }
            }


            group(Password)
            {
                Caption = 'Password', Comment = 'de-DE=Passwortrichtlinien';
                Visible = Rec."Portal - Change Pass. allowed";

                group(uses)
                {

                    field("Password Use Uppercase"; Rec."Password Use Uppercase")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Password Use Lowercase', Comment = 'de-DE=Gibt an, ob das Passwort - wenn es über das Kundenportal geändert wird - Großbuchstaben enthalten muss';
                        Caption = 'Upper Case Characters', Comment = 'de-DE=Großbuchstaben';
                        Importance = Promoted;
                    }
                    field("Password Use Lowercase"; Rec."Password Use Lowercase")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Password Use Lowercase', Comment = 'de-DE=Gibt an, ob das Passwort - wenn es über das Kundenportal geändert wird - Kleinbuchstaben enthalten muss';
                        Caption = 'Upper Case Characters', Comment = 'de-DE=Kleinbuchstaben';
                        Importance = Promoted;
                    }

                    field("Password Use Numbers"; Rec."Password Use Numbers")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Password Use Lowercase', Comment = 'de-DE=Gibt an, ob das Passwort - wenn es über das Kundenportal geändert wird - Zahlen enthalten muss';
                        Caption = 'Numbers', Comment = 'de-DE=Zahlen';
                        Importance = Promoted;
                    }
                    field("Password Use Special"; Rec."Password Use Special")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Password Use Lowercase', Comment = 'de-DE=Gibt an, ob das Passwort - wenn es über das Kundenportal geändert wird - Sonderzeichen enthalten muss';
                        Caption = 'Special Characters', Comment = 'de-DE=Sonderzeichen';
                        Importance = Promoted;

                    }
                }


            }

            group(ServiceCenter)
            {
                Caption = 'ServiceCenter', Comment = 'de-DE=Gerätecenter';
                field("Allow Ship-To Address modify"; Rec."Allow Ship-To Address modify")
                {
                    ApplicationArea = All;
                    ToolTip = 'Allow Ship-To Address modify', Comment = 'de-DE=Gibt an, ob die Lieferadresse im Geräte Bereich des Kundenportals geändert werden darf';
                }
            }
            group(DataroomCenter)
            {
                Caption = 'DataroomCenter', Comment = 'de-DE=Downloadcenter';
            }

            group(Sync)
            {
                Caption = 'URLS', Comment = 'de-DE=Link zum Portal';
                field("Link to Portal Backend"; Rec."Link to Portal Backend")
                {
                    ApplicationArea = All;
                    ToolTip = 'Link to Backend', Comment = 'de-DE=Portal-Backend';
                    ExtendedDatatype = URL;
                    ShowMandatory = true;
                }
                field("Portal Sync. No Of Recs."; Rec."Portal Sync. No Of Recs.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Portal Sync. No Of Recs', Comment = 'de-DE=Anzahl Datensätze pro Request';
                }
                field("Webservice Language Code"; Rec."Webservice Language Code")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Webservice Language Code', Comment = 'de-DE=Gibt die Sprache an, die bei Webdiensten verwendet wird. Wenn Leer, wird Englisch verwendet.';
                }
            }



            group(GuestForm)
            {
                Caption = 'Guest Form', Comment = 'de-DE=Gastformular';
                Visible = Rec."License Module 6";

                field("Anonymous Contact Company No."; Rec."Guest Contact Company No.")
                {
                    ApplicationArea = All;
                    Caption = 'Guest Contact Company No.', Comment = 'de-DE=Sammelkontaktnr.';
                    Tooltip = 'Anonymous Contact Company No.', Comment = 'de-DE=Gibt an, welcher Unternehmenskontakt für Tickets aus dem Gastformular ausgewählt wird';
                    Importance = Promoted;
                    ShowMandatory = true;

                    trigger OnValidate()
                    begin
                        Rec.CalcFields("Guest Contact Company Name");
                    end;
                }
                field("Anonymous Contact Company Name"; Rec."Guest Contact Company Name")
                {
                    ApplicationArea = All;
                    Caption = 'Guest Contact Company Name', Comment = 'de-DE=Sammelkontaktname';
                    Tooltip = 'Anonymous Contact Company Name', Comment = 'de-DE=Gibt an, welcher Unternehmenskontakt für Tickets aus dem Gastformular ausgewählt wird';
                }
                field("Link to Anonymous Form"; Rec."Link to Guest Form")
                {
                    ApplicationArea = All;
                    ShowMandatory = true;
                    Tooltip = 'Link to Anonymous Form', Comment = 'de-DE=Der Link, der - wenn die Platzhalter gefüllt sind - zum Ticketformular führt';
                    ExtendedDatatype = url;
                }
                field("Guest Form Text Module Code"; Rec."Guest Form Text Module Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Select what Text Module should be sent as a Response to someone submitting a Guest Form - The Text Module must contain the ticketlink placeholder!', Comment = 'de-DE=Wählen Sie, welche Textvorlage verwendet werden soll, wenn ein Gast-Ticket erstellt wird - Die Textvorlage muss den ticketlink Platzhalter enthalten!|en-US=Select what Text Module should be sent as a Response to someone submitting a Guest Form - The Text Module must contain the ticketlink placeholder!';
                }
            }

        }
    }

    actions
    {
        area(Processing)
        {
            action(PortalSync)
            {
                ApplicationArea = All;
                Image = LinkWeb;
                ToolTip = 'Portal Sync.', Comment = 'de-DE=Kundenportal Initialisieren';
                Caption = 'Portal Sync.', Comment = 'de-DE=Kundenportal Initialisieren';
                Visible = Rec."Portal Integration";
                trigger OnAction()
                var
                    ICIPortalMgt: Codeunit "ICI Portal Mgt.";
                begin
                    ICIPortalMgt.UpdateAllTables();
                    CurrPage.Update(FALSE);
                end;
            }
            action(Test)
            {
                ApplicationArea = All;
                Image = TestDatabase;
                ToolTip = 'Test', Comment = 'de-DE=Test';
                Caption = 'Test', Comment = 'de-DE=Test';
                // Visible = "Portal Integration";
                Visible = false;
                trigger OnAction()
                var
                    ICISupportWebserviceAPI: Codeunit "ICI Support Webservice API";
                    LanguageTestLbl: Label 'This is default Label content', Comment = 'de-DE=Das ist der Deutsche Labelinhalt';
                begin

                    MESSAGE(LanguageTestLbl);
                    ICISupportWebserviceAPI.SetLanguage('ENG');
                    MESSAGE(LanguageTestLbl);
                    ICISupportWebserviceAPI.setlanguage('ENU');
                    MESSAGE(LanguageTestLbl);

                end;
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        if not ICISupportSetup.GET() THEN
            ICISupportSetup.INSERT(true);
    end;

}
