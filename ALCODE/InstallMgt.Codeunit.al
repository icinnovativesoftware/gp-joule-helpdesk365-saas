codeunit 56282 "ICI Install Mgt."
{
    Subtype = Install;
    trigger OnInstallAppPerCompany()
    var
    //TenantWebService: Record "Tenant Web Service";
    begin
        //'56283'
        //TenantWebService.Init();
        //TenantWebService.VALIDATE("Object Type", TenantWebService."Object Type"::Codeunit);
        //TenantWebService.Validate("Object ID", 56283);
        // Webservice is added with XML File

        // init some things like 
        // setup
        // Mail and Ticket Text modules
        // no-series
        // Process Stage
        // Initial License Check
        // Assign User Permissionsets

        InitSupportModuleDefaults();
    end;

    procedure InitSupportModuleDefaults()
    var
    begin
        InitSetups();
        InitTicketNoSeries();
        InitTicketCategories();
        InitEMailTextModules();
        InitEscalations();
        InitTicketProcesses();
        InitDragboxConfig();
        InitDepartments();
        InitTimeAccTypes();
        InitPortalCues();
        InitPortalCueTranslations();
        InitJobQueues();
    end;

    local procedure InitSetups()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICIMailrobotSetup: Record "ICI Mailrobot Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin
        IF not ICISupportSetup.GET() THEN
            ICISupportSetup.Insert(TRUE);
        IF NOT ICISupportMailSetup.GET() then
            ICISupportMailSetup.Insert(TRUE);
        IF NOT ICIMailrobotSetup.GET() then
            ICIMailrobotSetup.Insert(TRUE);
        IF NOT ICIWebarchiveSetup.GET() then
            ICIWebarchiveSetup.Insert(TRUE);


        IF ICIMailrobotSetup."IMAP Connector URL" = '' THEN
            ICIMailrobotSetup."IMAP Connector URL" := 'https://licmgt.ic-innovative.de/Helpdesk365/IMAP_Connector.php';
        IF ICIMailrobotSetup."IMAP Connector Auth. Password" = '' THEN
            ICIMailrobotSetup."IMAP Connector Auth. Password" := 'Gast';
        IF ICIMailrobotSetup."IMAP Connector Auth. User" = '' THEN
            ICIMailrobotSetup."IMAP Connector Auth. User" := 'Gast';

        IF ICIMailrobotSetup."EWS Connector URL" = '' THEN
            ICIMailrobotSetup."EWS Connector URL" := 'https://licmgt.ic-innovative.de/Helpdesk365/EWS_Connector/EWS_Connector.php';
        IF ICIMailrobotSetup."EWS Connector Auth. Password" = '' THEN
            ICIMailrobotSetup."EWS Connector Auth. Password" := 'Gast';
        IF ICIMailrobotSetup."EWS Connector Auth. User" = '' THEN
            ICIMailrobotSetup."EWS Connector Auth. User" := 'Gast';

        ICIMailrobotSetup.Modify();

        IF ICISupportSetup."Webservice Language Code" = '' THEN
            ICISupportSetup."Webservice Language Code" := 'DEU';
        IF FORMAT(ICISupportSetup."Token Duration") = '' THEN
            EVALUATE(ICISupportSetup."Token Duration", '14T');

        IF ICISupportSetup."Max. Login Failures" = 0 THEN
            ICISupportSetup.Validate("Max. Login Failures", 5);

        IF ICISupportSetup."Show Filetypes as Image" = '' THEN
            ICISupportSetup.VALIDATE("Show Filetypes as Image", 'apng.avif.gif.jpg.jpeg.jfif.pjpeg.pjp.png.svg.webp.bmp.ico.cur.tif.tiff');// Taken from here: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img

        IF ICISupportSetup."RMA-Report ID" = 0 then
            ICISupportSetup."RMA-Report ID" := REPORT::"ICI RMA Bill";

        ICISupportSetup.Modify();
    end;

    local procedure InitTicketNoSeries()
    var
        ICISupportSetup: Record "ICI Support Setup";
        NoSeries: Record "No. Series";
        NoSeriesLine: Record "No. Series Line";
        TLbl: Label 'T%1-00000', Comment = '%1=Year';
    begin
        ICISupportSetup.GET();
        IF ICISupportSetup."Support Ticket Nos." <> '' THEN
            EXIT;

        NoSeries.SetRange(Code, 'TICKET');
        IF NoSeries.COUNT = 0 THEN begin
            CLEAR(NoSeries);
            NoSeries.Init();
            NoSeries.Validate(Code, 'TICKET');
            NoSeries.Validate(Description, 'Helpdesk 365 Ticketnummern');
            NoSeries.Validate("Default Nos.", TRUE);
            NoSeries.INSERT(true);
        end;
        ICISupportSetup.VALIDATE("Support Ticket Nos.", 'TICKET');
        ICISupportSetup.Modify(true);

        NoSeriesLine.SETRANGE("Series Code", 'TICKET');
        NoseriesLine.SETRANGE("Line No.", 10000);
        IF NoseriesLine.COUNT = 0 THEN begin
            CLEAR(NoseriesLine);
            NoSeriesLine.Init();
            NoSeriesLine.VALIDATE("Series Code", NoSeries.Code);
            NoseriesLine.Validate("Line No.", 10000);
            NoSeriesLine.Insert(true);
            NoSeriesLine.VALIDATE("Starting Date", WorkDate());
            // Soll TXX-00000. XX = Jahreszahl z.B. 21
            NoSeriesLine.Validate("Starting No.", STRSUBSTNO(TLbl, Date2DMY(WorkDate(), 3) - 2000));
            NoSeriesLine.Modify(true);
        END;
    end;

    local procedure InitTicketCategories()
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
    begin
        IF ICISupportTicketCategory.COUNT() > 0 THEN
            EXIT;
        InsertTicketCategory('FEHLER3', 'Fehler 3: normaler Fehler', '', true);
        InsertTicketCategoryTranslation('FEHLER3', 'Error 3: normal Error');

        InsertTicketCategory('FEHLER2', 'Fehler 2: Ablaufbehindernd', '', true);
        InsertTicketCategoryTranslation('FEHLER2', 'Error 2: process obstructing Error');

        InsertTicketCategory('FEHLER1', 'Fehler 1: Systemstillstand', '', true);
        InsertTicketCategoryTranslation('FEHLER1', 'Error 1: system standstill Error');

        InsertTicketCategory('PROBLEM', 'Problem', '', true);
        InsertTicketCategoryTranslation('PROBLEM', 'Problem');

        InsertTicketCategory('PROBLEM-HW', 'Hardwareproblem', 'PROBLEM', true);
        InsertTicketCategoryTranslation('PROBLEM-HW', 'Problem with Hardware');

        InsertTicketCategory('PROBLEM-SW', 'Softwareproblem', 'PROBLEM', true);
        InsertTicketCategoryTranslation('PROBLEM-SW', 'Problem with Software');

        InsertTicketCategory('FRAGE', 'Frage', '', true);
        InsertTicketCategoryTranslation('FRAGE', 'Question');

        InsertTicketCategory('FRAGE-HW', 'Frage zu Hardware', 'FRAGE', true);
        InsertTicketCategoryTranslation('FRAGE-HW', 'Question regarding Hardware');

        InsertTicketCategory('FRAGE-SW', 'Frage zu Software', 'FRAGE', true);
        InsertTicketCategoryTranslation('FRAGE-SW', 'Question regarding Software');

        InsertTicketCategory('FRAGE-AU', 'Frage zu Auftrag', 'FRAGE', true);
        InsertTicketCategoryTranslation('FRAGE-AU', 'Question regarding Order');

        InsertTicketCategory('FRAGE-RE', 'Frage zu Rechnung', 'FRAGE', true);
        InsertTicketCategoryTranslation('FRAGE-RE', 'Question regarding Invoice');

        InsertTicketCategory('FRAGE-LF', 'Frage zu Lieferung', 'FRAGE', true);
        InsertTicketCategoryTranslation('FRAGE-LF', 'Question regarding Shipment');


        InsertTicketCategory('ANFRAGE', 'Anfragen', '', true);
        InsertTicketCategoryTranslation('ANFRAGE', 'Requests');

        InsertTicketCategory('ANFRAGE-RR', 'Rückrufanfrage', 'ANFRAGE', true);
        InsertTicketCategoryTranslation('ANFRAGE-RR', 'Requested Callback');

        InsertTicketCategory('ANFRAGE-MODUL', 'Modulanfrage', 'ANFRAGE', true);
        InsertTicketCategoryTranslation('ANFRAGE-MODUL', 'Requested Module');

        InsertTicketCategory('ANFRAGE-PROJEKT', 'Projektanfrage', 'ANFRAGE', true);
        InsertTicketCategoryTranslation('ANFRAGE-PROJEKT', 'Requested Project');

        InsertTicketCategory('ANFRAGE-BESTELLUNG', 'Bestellung', 'ANFRAGE', true);
        InsertTicketCategoryTranslation('ANFRAGE-BESTELLUNG', 'Requested Purchase');

        ICISupportMailSetup.GET();
    end;

    local procedure InsertTicketCategory(PCode: Code[50]; PDescription: Text[100]; PParentCategory: Code[50]; POnline: Boolean)
    var
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
    begin
        ICISupportTicketCategory.SetRange(Code, PCode);
        IF ICISupportTicketCategory.Count > 0 THEN
            EXIT;
        CLEAR(ICISupportTicketCategory);
        ICISupportTicketCategory.Init();
        ICISupportTicketCategory.VALIDATE(Code, PCode);
        ICISupportTicketCategory.Validate(Description, PDescription);
        ICISupportTicketCategory.Validate("Parent Category", PParentCategory);
        ICISupportTicketCategory.Validate(Online, POnline);
        ICISupportTicketCategory.INSERT(true);
    end;

    local procedure InsertTicketCategoryTranslation(PCode: Code[50]; PDescription: Text[100])
    var
        ICITickCategoryTranslation: Record "ICI Tick. Category Translation";
    begin
        ICITickCategoryTranslation.SetRange("Ticket Category Code", PCode);
        ICITickCategoryTranslation.SetRange("Language Code", 'ENU');

        IF ICITickCategoryTranslation.Count > 0 THEN
            EXIT;

        CLEAR(ICITickCategoryTranslation);
        ICITickCategoryTranslation.Init();
        ICITickCategoryTranslation.VALIDATE("Ticket Category Code", PCode);
        ICITickCategoryTranslation.VALIDATE("Language Code", 'ENU');
        ICITickCategoryTranslation.Validate(Description, PDescription);

        ICITickCategoryTranslation.INSERT(true);
    end;

    local procedure InitEMailTextModules()
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
    begin
        ICISupportMailSetup.GET();
        IF ICISupportMailSetup."E-Mail Footer" = '' THEN BEGIN
            InitEMailTextModule('FOOTER', 'Footer', '<p><strong>%company_information_name%&nbsp;</strong></p><p>E-Mail: %company_information_email%&nbsp;</p><p>Fax: %company_information_fax_no%&nbsp;</p><p>Homepage: %company_information_home_page%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail Footer", 'FOOTER');
        END;

        // Todo:
        // PW Vergessen Benutzer + Kontakt
        // Zugangsdaten Benutzer

        IF ICISupportMailSetup."E-Mail C Login" = '' THEN BEGIN
            InitEMailTextModule('KONTAKT-ZUGANGSDATEN', 'Ihre Zugangsdaten für das Helpdesk365 Kundenportal', '<p>%contact_salutation%</p><p>Herzlich willkommen im <a href="%setup_link_to_portal%">Helpdesk365 Support Kundenportal</a>. Ihre Zugangsdaten lauten:</p><p>Login: %contact_login%</p><p>Password: %contact_password%</p><p><br data-cke-filler="true"></p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail C Login", 'KONTAKT-ZUGANGSDATEN');
            ICISupportMailSetup.Validate("E-Mail C Login Active", true);
        END;

        IF ICISupportMailSetup."E-Mail U Login" = '' THEN BEGIN
            InitEMailTextModule('BENUTZER-ZUGANGSDATEN', 'Ihre Zugangsdaten für das Helpdesk365 Kundenportal', '<p>%support_user_salutation%</p><p>Herzlich willkommen im <a href="%setup_link_to_portal%">Helpdesk365 Support Kundenportal</a>. Ihre Zugangsdaten lauten:</p><p>Login: %support_user_login%</p><p>Password: %support_user_password%</p><p><br data-cke-filler="true"></p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail U Login", 'BENUTZER-ZUGANGSDATEN');
            ICISupportMailSetup.Validate("E-Mail U Login Active", true);
        END;

        IF ICISupportMailSetup."E-Mail C New Message" = '' THEN BEGIN
            InitEMailTextModule('KONTAKT-TICKETNACHRICHT', 'Neue Nachricht in Ticket %ticket_no% - ''%ticket_description%''', '<p>%contact_salutation%</p><p>es gibt eine neue Nachricht für Sie. %last_support_message_sender_name% schrieb:</p><p>%last_support_message_data%</p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail C New Message", 'KONTAKT-TICKETNACHRICHT');
            ICISupportMailSetup.Validate("E-Mail C New Message Active", true);
        END;

        IF ICISupportMailSetup."E-Mail U New Message" = '' THEN BEGIN
            InitEMailTextModule('BENUTZER-TICKETNACHRICHT', 'Neue Nachricht in Ticket %ticket_no% - ''%ticket_description%''', '<p>%support_user_salutation%</p><p>es gibt eine neue Nachricht für Sie. %last_support_message_sender_name% schrieb:</p><p>%last_support_message_data%</p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail U New Message", 'BENUTZER-TICKETNACHRICHT');
            ICISupportMailSetup.Validate("E-Mail U New Message Active", true);
        END;

        IF ICISupportMailSetup."E-Mail U Escalation" = '' THEN BEGIN
            InitEMailTextModule('BENUTZER-ESKALATION', 'Ihr Ticket %ticket_no% - %ticket_description% bei %contact_company_name% eskaliert!', '<p>%support_user_salutation%</p><p>Das Ticket <a href="%link_to_ticket%">%ticket_no%</a> - "%ticket_description%" eskaliert! Ihr Ansprechpartner ist %contact_name%. </p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail U Escalation", 'BENUTZER-ESKALATION');
        END;

        IF ICISupportMailSetup."E-Mail U Forwarded" = '' THEN BEGIN
            InitEMailTextModule('BENUTZER-WEITERLEITUNG', 'Ihnen wurde das Ticket %ticket_no% - "%ticket_description%" bei %contact_company_name% weitergeleitet!', '<p>%support_user_salutation%</p><p>Das Ticket <a href="%link_to_ticket%">%ticket_no%</a> - "%ticket_description%" wurde Ihnen zugewiesen. Ihr Ansprechpartner ist %contact_name%. </p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail U Forwarded", 'BENUTZER-WEITERLEITUNG');
            ICISupportMailSetup.VALIDATE("E-Mail U Forwarded Active", true);
        END;

        IF ICISupportMailSetup."E-Mail C Forwarded" = '' THEN BEGIN
            InitEMailTextModule('KONTAKT-WEITERLEITUNG', 'Ihnen wurde das Ticket %ticket_no% - "%ticket_description%" weitergeleitet!', '<p>%contact_salutation%</p><p>Das Ticket %ticket_no% - "%ticket_description%" wurde Ihnen zugewiesen. Ihr Ansprechpartner ist %support_user_name%. </p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail C Forwarded", 'KONTAKT-WEITERLEITUNG');
            ICISupportMailSetup.VALIDATE("E-Mail C Forwarded Active", true);
        END;

        InitEMailTextModule('KONTAKT-TICKET-OPEN', 'Wir haben das Ticket %ticket_no% - ''%ticket_description%'' für sie eröffnet', '<p>%contact_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wurde für Sie eröffnet. Letzte Nachricht %last_support_message_data%</p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
        InitEMailTextModule('KONTAKT-TICKET-PROCESS', 'Ihr Ticket %ticket_no% - ''%ticket_description%'' wird bearbeitet', '<p>%contact_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wird von uns bearbeitet. Ihr Ansprechpartner ist %support_user_name%. Die neuste Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
        InitEMailTextModule('KONTAKT-TICKET-WAIT', 'Wir benötigen ihre Rückmeldung zum Ticket %ticket_no% - ''%ticket_description%''', '<p>%contact_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' befindet sich in Wartestellung. Ihr Ansprechpartner ist %support_user_name%. Die neuste Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
        InitEMailTextModule('KONTAKT-TICKET-CLOSED', 'Ihr Ticket %ticket_no% - ''%ticket_description%'' wurde geschlossen', '<p>%contact_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wurde geschlossen. Es betreute Sie %support_user_name%. Die letzte Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');

        InitEMailTextModule('BENUTZER-TICKET-OPEN', 'Wir haben das Ticket %ticket_no% - ''%ticket_description%'' für sie eröffnet', '<p>%support_user_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wurde für Sie eröffnet. Letzte Nachricht %last_support_message_data%</p><p>%footer%</p>');
        InitEMailTextModule('BENUTZER-TICKET-PROCESS', 'Ihr Ticket %ticket_no% - ''%ticket_description%'' wird bearbeitet', '<p>%support_user_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wird von uns bearbeitet. Ihr Ansprechpartner ist %contact_name%. Die neuste Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>%footer%</p>');
        InitEMailTextModule('BENUTZER-TICKET-WAIT', 'Wir benötigen ihre Rückmeldung zum Ticket %ticket_no% - ''%ticket_description%''', '<p>%support_user_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' befindet sich in Wartestellung. Ihr Ansprechpartner ist %contact_name%. Die neuste Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>%footer%</p>');
        InitEMailTextModule('BENUTZER-TICKET-CLOSED', 'Ihr Ticket %ticket_no% - ''%ticket_description%'' wurde geschlossen', '<p>%support_user_salutation%</p><p>Das Ticket  %ticket_no% - ''%ticket_description%'' wurde geschlossen. Es betreute Sie %contact_name%. Die letzte Nachricht schrieb %last_support_message_sender_name%:  %last_support_message_data%</p><p>%footer%</p>');

        IF ICISupportMailSetup."E-Mail C New File" = '' THEN BEGIN
            InitEMailTextModule('KONTAKT-TICKETDATEI', 'Neue Datei in Ticket %ticket_no% - ''%ticket_description%''', '<p>%contact_salutation%</p><p>es gibt eine neue Datei für Sie. </p><p>Klicken Sie <a href="%ticket_link%">hier</a>, um in das Kundenportal zu gelangen</p><p>%footer%</p>');
            ICISupportMailSetup.VALIDATE("E-Mail C New File", 'KONTAKT-TICKETDATEI');
            ICISupportMailSetup.Validate("E-Mail C New File Active", true);
        END;

        // VK Belege - Ungebucht
        IF ICISupportMailSetup."E-Mail Sales Quote" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-ANGEBOT', 'Ihr angefordertes Angebot', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihr angefordertes Angebot.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Quote", 'BELEG-VK-ANGEBOT');
        END;
        IF ICISupportMailSetup."E-Mail Sales Order" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-AUFTRAG', 'Ihr angeforderter Auftrag', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Auftrag.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Order", 'BELEG-VK-AUFTRAG');
        END;
        IF ICISupportMailSetup."E-Mail Sales Invoice" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-RECHNUNG', 'Ihre angeforderte Rechnung', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Rechnung.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Invoice", 'BELEG-VK-RECHNUNG');
        END;
        IF ICISupportMailSetup."E-Mail Sales Cr.Memo" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-GUTSCHRIFT', 'Ihre angeforderte Gutschrift', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Gutschrift.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Cr.Memo", 'BELEG-VK-GUTSCHRIFT');
        END;
        IF ICISupportMailSetup."E-Mail Sales Blanket Order" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-RAHMENAUFTRAG', 'Ihr angeforderter Rahmenauftrag', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Rahmenauftrag.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Blanket Order", 'BELEG-VK-RAHMENAUFTRAG');
        END;
        IF ICISupportMailSetup."E-Mail Sales Return Order" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-REKLAMATION', 'Ihre angeforderte Reklamation', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Reklamation.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Sales Return Order", 'BELEG-VK-REKLAMATION');
        END;

        // VK Belege - Gebucht
        IF ICISupportMailSetup."E-Mail Pst. Sales Shipment" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-GEB-LIEFERUNG', 'Ihre angeforderter Lieferschein', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Lieferschein.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Pst. Sales Shipment", 'BELEG-VK-GEB-LIEFERUNG');
        END;
        IF ICISupportMailSetup."E-Mail Pst. Sales Invoice" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-GEB-RECHNUNG', 'Ihre angeforderte gebuchte Rechnung', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte gebuchte Rechnung.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Pst. Sales Invoice", 'BELEG-VK-GEB-RECHNUNG');
        END;
        IF ICISupportMailSetup."E-Mail Pst. Sales Cr.Memo" = '' THEN BEGIN
            InitEMailTextModule('BELEG-VK-GEB-GUTSCHRIFT', 'Ihre angeforderte gebuchte Gutschrift', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte gebuchte Gutschrift.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Pst. Sales Cr.Memo", 'BELEG-VK-GEB-GUTSCHRIFT');
        END;

        // Service Belege - Ungebucht
        IF ICISupportMailSetup."E-Mail Service Quote" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-ANGEBOT', 'Ihr angefordertes Serviceangebot', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihr angefordertes Serviceangebot.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Quote", 'BELEG-SERV-ANGEBOT');
        END;
        IF ICISupportMailSetup."E-Mail Service Order" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-AUFTRAG', 'Ihr angeforderter Serviceauftrag', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Serviceauftrag.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Order", 'BELEG-SERV-AUFTRAG');
        END;
        IF ICISupportMailSetup."E-Mail Service Invoice" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-RECHNUNG', 'Ihre angeforderte Servicerechnung', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Servicerechnung.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Invoice", 'BELEG-SERV-RECHNUNG');
        END;
        IF ICISupportMailSetup."E-Mail Service Cr.Memo" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-GUTSCHRIFT', 'Ihre angeforderte Servicegutschrift', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Servicegutschrift.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Cr.Memo", 'BELEG-SERV-GUTSCHRIFT');
        END;

        // Service Belege - Gebucht
        IF ICISupportMailSetup."E-Mail Posted Service Shipment" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-GEB-LIEFERUNG', 'Ihre angeforderter Servicelieferschein', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Servicelieferschein.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Posted Service Shipment", 'BELEG-SERV-GEB-LIEFERUNG');
        END;
        IF ICISupportMailSetup."E-Mail Posted Service Invoice" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-GEB-RECHNUNG', 'Ihre angeforderte gebuchte Servicerechnung', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte gebuchte Servicerechnung.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Posted Service Invoice", 'BELEG-SERV-GEB-RECHNUNG');
        END;
        IF ICISupportMailSetup."E-Mail Posted Service Cr.Memo" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-GEB-GUTSCHRIFT', 'Ihre angeforderte gebuchte Servicegutschrift', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte gebuchte Servicegutschrift.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Posted Service Cr.Memo", 'BELEG-SERV-GEB-GUTSCHRIFT');
        END;

        // Service Vertrag
        IF ICISupportMailSetup."E-Mail Service Contract" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-VERTRAG', 'Ihre angeforderter Servicevertrag', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihren angeforderten Servicevertrag.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Contract", 'BELEG-SERV-VERTRAG');
        END;
        IF ICISupportMailSetup."E-Mail Service Contract Quote" = '' THEN BEGIN
            InitEMailTextModule('BELEG-SERV-VERTRAGSANGEBOT', 'Ihre angeforderter Servicevertragsangebot', '<p>%contact_salutation%</p><p>im Anhang finden Sie Ihre angeforderte Servicevertragsangebot.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail Service Contract Quote", 'BELEG-SERV-VERTRAGSANGEBOT');
        END;

        // RMA
        IF ICISupportMailSetup."E-Mail C RMA" = '' THEN BEGIN
            InitEMailTextModule('RMA-FORMULAR', 'Ihr RMA-Formular %ticket_no%', '<p>%contact_salutation%</p><p>im Anhang finden Sie das RMA-Formular %ticket_no%.</p><p>%footer%</p></div>');
            ICISupportMailSetup.VALIDATE("E-Mail C RMA", 'RMA-FORMULAR');
        END;


        ICISupportMailSetup.Modify(true);
    end;

    procedure InitEMailTextModule(lCode: Code[30]; lDescription: Text[250]; lText: Text)
    var
        ICISupportTextModule: Record "ICI Support Text Module";
        lOutStream: OutStream;
    begin
        ICISupportTextModule.SetRange(Type, ICISupportTextModule.Type::"E-Mail");
        ICISupportTextModule.SetRange(Code, lCode);
        IF ICISupportTextModule.Count > 0 then
            EXIT;
        Clear(ICISupportTextModule);
        ICISupportTextModule.Init();
        ICISupportTextModule.Validate(Code, lCode);
        ICISupportTextModule.Validate(Description, lDescription);
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateOutStream(lOutStream);
        lOutStream.Write(lText);
        ICISupportTextModule.Insert(true);
    end;

    local procedure InitEscalations()
    var
        ICIEscalation: Record "ICI Escalation";
    begin
        ICIEscalation.Init();
        ICIEscalation.VALIDATE(Code, 'NORMAL');
        Evaluate(ICIEscalation."Target Time", '4T');
        IF NOT ICIEscalation.Insert() THEN;

        // Mail, nach 2 Tagen Offen
        InitEscalationLevel('NORMAL', '2T', "ICI Ticket State"::Open, 10000);

        // Mail, nach 2 Tagen Bearbeitung
        InitEscalationLevel('NORMAL', '2T', "ICI Ticket State"::Processing, 20000);

        // Mail, nach 2 Tagen Warten
        InitEscalationLevel('NORMAL', '2T', "ICI Ticket State"::Waiting, 30000);
    end;

    local procedure InitEscalationLevel(PEscalationCode: Code[20]; pStartTime: Text; pTicketState: Enum "ICI Ticket State"; LineNo: Integer)
    var
        ICIEscalationLevel: Record "ICI Escalation Level";
    begin

        ICIEscalationLevel.SetRange("Escalation Code", PEscalationCode);

        ICIEscalationLevel.SetRange("Send E-Mail", True);

        ICIEscalationLevel.SetRange(ICIEscalationLevel."Receiver Type", ICIEscalationLevel."Receiver Type"::"Current User");
        ICIEscalationLevel.SetRange(Condition, ICIEscalationLevel.Condition::Processstate);
        ICIEscalationLevel.SetRange("Ticket State", pTicketState);
        IF ICIEscalationLevel.Count() > 0 THEN
            exit;

        CLEAR(ICIEscalationLevel);
        ICIEscalationLevel.Init();
        ICIEscalationLevel.Validate("Escalation Code", PEscalationCode);
        ICIEscalationLevel.Validate("Line No.", LineNo);
        IF NOT ICIEscalationLevel.Insert(true) THEN;
        EVALUATE(ICIEscalationLevel."Start Time", pStartTime);
        ICIEscalationLevel.Validate("Send E-Mail", True);
        ICIEscalationLevel.Validate(ICIEscalationLevel."Receiver Type", ICIEscalationLevel."Receiver Type"::"Current User");
        ICIEscalationLevel.Validate(Condition, ICIEscalationLevel.Condition::Processstate);
        ICIEscalationLevel.Validate("Ticket State", pTicketState);
        ICIEscalationLevel.Modify(true);

    end;

    local procedure InitTicketProcesses()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.GET();
        IF ICISupportSetup."Default Ticket Process" <> '' THEN
            EXIT;

        // Type:  " ",Preparation,Open,Processing,Waiting,Closed
        InitTicketProcess('NORMAL', 'Normaler Ticketprozess', 'NORMAL');
        InitTicketProcessStage('NORMAL', 10, "ICI Ticket State"::Preparation, 'In Vorbereitung', '', '', false, false, true, false);
        InitTicketProcessStageTranslation('NORMAL', 10, 'In Preparation');

        InitTicketProcessStage('NORMAL', 20, "ICI Ticket State"::Open, 'Eröffnet', 'KONTAKT-TICKET-OPEN', 'BENUTZER-TICKET-OPEN', false, false, false, false);
        InitTicketProcessStageTranslation('NORMAL', 20, 'Opened');

        InitTicketProcessStage('NORMAL', 30, "ICI Ticket State"::Processing, 'In Bearbeitung', 'KONTAKT-TICKET-PROCESS', 'BENUTZER-TICKET-PROCESS', false, false, false, false);
        InitTicketProcessStageTranslation('NORMAL', 30, 'In Process');

        InitTicketProcessStage('NORMAL', 40, "ICI Ticket State"::Closed, 'Geschlossen', 'KONTAKT-TICKET-CLOSED', 'BENUTZER-TICKET-CLOSED', true, false, true, false);
        InitTicketProcessStageTranslation('NORMAL', 40, 'Closed');

        ICISupportSetup.VAlidate("Default Ticket Process", 'NORMAL');
        ICISupportSetup.Modify(true);

        InitTicketProcess('ABNAHME', 'Ticketprozess mit Abnahme', 'NORMAL');
        InitTicketProcessStage('ABNAHME', 10, "ICI Ticket State"::Preparation, 'In Vorbereitung', '', '', false, false, true, false);
        InitTicketProcessStageTranslation('ABNAHME', 10, 'In Preparation');

        InitTicketProcessStage('ABNAHME', 20, "ICI Ticket State"::Open, 'Eröffnet', 'KONTAKT-TICKET-OPEN', 'BENUTZER-TICKET-OPEN', false, false, false, false);
        InitTicketProcessStageTranslation('ABNAHME', 20, 'Opened');

        InitTicketProcessStage('ABNAHME', 30, "ICI Ticket State"::Processing, 'In Bearbeitung', 'KONTAKT-TICKET-PROCESS', 'BENUTZER-TICKET-PROCESS', false, false, false, false);
        InitTicketProcessStageTranslation('ABNAHME', 30, 'In Process');

        InitTicketProcessStage('ABNAHME', 40, "ICI Ticket State"::Waiting, 'Warten auf Freigabe', 'KONTAKT-TICKET-WAIT', 'BENUTZER-TICKET-WAIT', false, false, true, false);
        InitTicketProcessStageTranslation('ABNAHME', 40, 'Waiting');

        InitTicketProcessStage('ABNAHME', 50, "ICI Ticket State"::Closed, 'Geschlossen', 'KONTAKT-TICKET-CLOSED', 'BENUTZER-TICKET-CLOSED', false, false, true, false);
        InitTicketProcessStageTranslation('ABNAHME', 50, 'Closed');
    end;

    local procedure InitTicketProcess(PCode: Code[20]; PDescription: Text[50]; PEscalationCode: Code[20])
    var
        ICISupportProcess: Record "ICI Support Process";
    begin
        ICISupportProcess.SetRange(Code, PCode);
        IF ICISupportProcess.Count > 0 THEN
            EXIT;
        CLEAR(ICISupportProcess);

        ICISupportProcess.Init();
        ICISupportProcess.VALIDATE(Code, PCode);
        ICISupportProcess.Validate(Description, PDescription);
        ICISupportProcess.Validate("Default Escalation Code", PEscalationCode);
        ICISupportProcess.INSERT(true);
    end;

    local procedure InitTicketProcessStage(PCode: Code[20]; PStage: Integer; PType: Enum "ICI Ticket State"; PDescription: Text[100];
                                                                                        TextModuleC: Code[30];
                                                                                        TextModuleU: Code[30];
                                                                                        Prev: Boolean;
                                                                                        Skip: Boolean;
                                                                                        Jump: Boolean;
                                                                                        Service: Boolean)
    var
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportProcessStage.SetRange("Process Code", PCode);
        ICISupportProcessStage.SetRange(Stage, PStage);
        IF ICISupportProcessStage.Count > 0 THEN
            EXIT;
        CLEAR(ICISupportProcessStage);

        ICISupportProcessStage.Init();
        ICISupportProcessStage.Validate("Process Code", PCode);
        ICISupportProcessStage.Validate(Stage, PStage);
        ICISupportProcessStage.Validate("Ticket State", PType);
        ICISupportProcessStage.Validate(Description, PDescription);
        ICISupportProcessStage.Validate("E-Mail C Text Module Code", TextModuleC);
        ICISupportProcessStage.Validate("E-Mail U Text Module Code", TextModuleU);
        ICISupportProcessStage.Validate("Previous Allowed", Prev);
        ICISupportProcessStage.Validate("Skip Allowed", Skip);
        ICISupportProcessStage.Validate("Jump To Allowed", Jump);
        ICISupportProcessStage.Validate("Service Item Mandatory", Service);
        ICISupportProcessStage.INSERT(true);
    end;

    local procedure InitTicketProcessStageTranslation(PCode: Code[20]; PStage: Integer; PDescription: Text[100])
    var
        ICIProcStageTranslation: Record "ICI Proc. Stage Translation";
    begin
        ICIProcStageTranslation.init();
        ICIProcStageTranslation."Process Code" := PCode;
        ICIProcStageTranslation."Process Stage" := PStage;
        ICIProcStageTranslation."Language Code" := 'ENU';
        ICIProcStageTranslation.Description := PDescription;

        IF NOT ICIProcStageTranslation.Insert() then
            ICIProcStageTranslation.Modify();
    end;

    local procedure InitDragboxConfig()
    var
        ICISupDragboxConfig: Record "ICI Sup. Dragbox Config.";
    begin
        IF NOT ICISupDragboxConfig.GET('ICI MAILROBOT INBOX') THEN begin
            Clear(ICISupDragboxConfig);
            ICISupDragboxConfig.Init();
            ICISupDragboxConfig.Validate(Code, 'ICI MAILROBOT INBOX');
            ICISupDragboxConfig.Validate(Description, 'Mailrobot');
            ICISupDragboxConfig.Validate("View Type", ICISupDragboxConfig."View Type"::"Small Icons");
            ICISupDragboxConfig.Validate("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::"Store in Database");
            ICISupDragboxConfig.INSERT(TRUE);
        end;
        IF NOT ICISupDragboxConfig.GET('ICI SUPPORT TICKET') THEN begin
            Clear(ICISupDragboxConfig);
            ICISupDragboxConfig.Init();
            ICISupDragboxConfig.Validate(Code, 'ICI SUPPORT TICKET');
            ICISupDragboxConfig.Validate(Description, 'Tickets');
            ICISupDragboxConfig.Validate("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::Ticket);
            ICISupDragboxConfig.Validate("View Type", ICISupDragboxConfig."View Type"::"Small Icons");
            ICISupDragboxConfig.INSERT(TRUE);
        end;
        IF NOT ICISupDragboxConfig.GET('SERVICE ITEM') THEN begin
            Clear(ICISupDragboxConfig);
            ICISupDragboxConfig.Init();
            ICISupDragboxConfig.Validate(Code, 'SERVICE ITEM');
            ICISupDragboxConfig.Validate(Description, 'Serviceartikel');
            ICISupDragboxConfig.Validate("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::"Store in Database");
            ICISupDragboxConfig.Validate("View Type", ICISupDragboxConfig."View Type"::"Small Icons");
            ICISupDragboxConfig.INSERT(TRUE);
        end;
        IF NOT ICISupDragboxConfig.GET('ITEM') THEN begin
            Clear(ICISupDragboxConfig);
            ICISupDragboxConfig.Init();
            ICISupDragboxConfig.Validate(Code, 'ITEM');
            ICISupDragboxConfig.Validate(Description, 'Artikel');
            ICISupDragboxConfig.Validate("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::"Store in Database");
            ICISupDragboxConfig.Validate("View Type", ICISupDragboxConfig."View Type"::"Small Icons");
            ICISupDragboxConfig.INSERT(TRUE);
        end;
        IF NOT ICISupDragboxConfig.GET('ICI DOWNLOAD CATEGORY') THEN begin
            Clear(ICISupDragboxConfig);
            ICISupDragboxConfig.Init();
            ICISupDragboxConfig.Validate(Code, 'ICI DOWNLOAD CATEGORY');
            ICISupDragboxConfig.Validate(Description, 'Downloadkategorien');
            ICISupDragboxConfig.Validate("Dragbox Type", ICISupDragboxConfig."Dragbox Type"::"Store in Database");
            ICISupDragboxConfig.Validate("View Type", ICISupDragboxConfig."View Type"::"Small Icons");
            ICISupDragboxConfig.INSERT(TRUE);
        end;
    end;

    local procedure InitDepartments()
    begin
        InitDepartment('SUP-1', 'First Level Support');
        InitDepartment('SUP-2', 'Second Level Support');
        InitDepartment('SUP-3', 'Third Level Support');
        InitDepartment('Sales', 'Verkauf');
        InitDepartment('Purchase', 'Einkauf');
        InitDepartment('HR', 'Human Ressources');
        InitDepartment('FIBU', 'Finanzbuchhaltung');
        InitDepartment('SERVICE', 'Service-Techniker');
    end;

    local procedure InitDepartment(PCode: Code[10]; PDescription: Text[100])
    var
        ICIDepartment: Record "ICI Department";
    begin
        ICIDepartment.Init();
        ICIDepartment.Validate(Code, PCode);
        ICIDepartment.Validate(Description, PDescription);
        IF NOT ICIDepartment.Insert(true) THEN
            exit;
    end;

    local procedure InitTimeAccTypes()

    begin
        //,Payoff,ExistingOrder,ForFree,Error,Info,Sales
        InitTimeAccType('PAYOFF', 'Abrechnen', true, 0);
        InitTimeAccType('EXISTINGORDER', 'Auftrag', false, 0);
        InitTimeAccType('FORFREE', 'Kulanz', true, 100);
        InitTimeAccType('ERROR', 'Fehlerbehebung', false, 0);
        InitTimeAccType('INFO', 'Info', false, 0);
        InitTimeAccType('SALES', 'Vertrieb', false, 0);
    end;

    local procedure InitTimeAccType(pCode: Code[20]; pDescription: Text[100]; pAccounting: Boolean; pDiscount: Decimal)
    var
        ICISupportTimeAccType: Record "ICI Support Time Acc. Type";
    begin
        ICISupportTimeAccType.Init();
        ICISupportTimeAccType.Validate(Code, pCode);
        ICISupportTimeAccType.Validate(Description, pDescription);
        ICISupportTimeAccType.Validate(Accounting, pAccounting);
        ICISupportTimeAccType.Validate("Discount %", pDiscount);
        IF NOT ICISupportTimeAccType.INSERT(true) then
            ICISupportTimeAccType.Modify(true);
    end;

    procedure InitService()

    begin
        InitServiceNoSeries();
        InitBaseCalendarForService();
        InitMandatoryServiceSetupFields();
    end;

    local procedure InitServiceNoSeries()
    var
        ServiceMgtSetup: Record "Service Mgt. Setup";
    begin
        IF NOT ServiceMgtSetup.GET() THEN
            ServiceMgtSetup.Insert(true);

        IF ServiceMgtSetup."Service Item Nos." = '' THEN begin
            InitServiceNoSerie('SERVICEITEM', 'Serviceartikel', 'SI-00000');
            ServiceMgtSetup.VALIDATE("Service Item Nos.", 'SERVICEITEM');
        end;
        IF ServiceMgtSetup."Service Order Nos." = '' THEN begin
            InitServiceNoSerie('SERVICEORDER', 'Serviceauftrag', 'SO-00000');
            ServiceMgtSetup.VALIDATE("Service Order Nos.", 'SERVICEORDER');
        end;
        IF ServiceMgtSetup."Service Invoice Nos." = '' THEN begin
            InitServiceNoSerie('SERVICEINVOICE', 'Service Rechnung', 'SINV-00000');
            ServiceMgtSetup.VALIDATE("Service Invoice Nos.", 'SERVICEINVOICE');
        end;
        IF ServiceMgtSetup."Posted Service Invoice Nos." = '' THEN begin
            InitServiceNoSerie('PST-SERVICEINVOICE', 'Geb. Service Rechnung', 'PSINV-00000');
            ServiceMgtSetup.VALIDATE("Posted Service Invoice Nos.", 'PST-SERVICEINVOICE');
        end;
        IF ServiceMgtSetup."Posted Service Shipment Nos." = '' THEN begin
            InitServiceNoSerie('PST-SERVICESHIP', 'Geb. Service Lieferung', 'P-SSH-00000');
            ServiceMgtSetup.VALIDATE("Posted Service Shipment Nos.", 'PST-SERVICESHIP');
        end;
        IF ServiceMgtSetup."Service Contract Nos." = '' THEN begin
            InitServiceNoSerie('SERVICECONTRACT', 'Servicevertrag', 'SC-00000');
            ServiceMgtSetup.VALIDATE("Service Contract Nos.", 'SERVICECONTRACT');
        end;
        IF ServiceMgtSetup."Service Quote Nos." = '' THEN begin
            InitServiceNoSerie('SERVICEQUOTE', 'Serviceangebot', 'SQ-00000');
            ServiceMgtSetup.VALIDATE("Service Quote Nos.", 'SERVICEQUOTE');
        end;
        IF ServiceMgtSetup."Service Credit Memo Nos." = '' THEN begin
            InitServiceNoSerie('SERVICECRMEMO', 'Servicegutschrift', 'SCM-00000');
            ServiceMgtSetup.VALIDATE("Service Credit Memo Nos.", 'SERVICECRMEMO');
        end;
        IF ServiceMgtSetup."Posted Serv. Credit Memo Nos." = '' THEN begin
            InitServiceNoSerie('PST-SERVICECRMEMO', 'Geb. Servicegutschrift', 'PSCM-00000');
            ServiceMgtSetup.VALIDATE("Posted Serv. Credit Memo Nos.", 'PST-SERVICECRMEMO');
        end;

        ServiceMgtSetup.Modify(true);
    end;

    local procedure InitServiceNoSerie(NoSeriesCode: Code[20]; NoSeriesDescription: Text[100]; NoSeriesStartingNo: Code[20])
    var
        NoSeries: Record "No. Series";
        NoSeriesLine: Record "No. Series Line";
    begin
        IF NoSeries.GET(NoSeriesCode) THEN
            EXIT;

        // Nr Serie Erstellen
        NoSeries.Init();
        NoSeries.Validate(Code, NoSeriesCode);
        NoSeries.Validate(Description, NoSeriesDescription);
        NoSeries.Validate("Default Nos.", TRUE);
        IF NOT NoSeries.INSERT(true) THEN
            NoSeries.Modify(true);


        // eile Erstellen
        NoSeriesLine.SETRANGE("Series Code", NoSeriesCode);
        NoseriesLine.SETRANGE("Line No.", 10000);
        IF NoseriesLine.COUNT = 0 THEN begin
            CLEAR(NoseriesLine);
            NoSeriesLine.Init();
            NoSeriesLine.VALIDATE("Series Code", NoSeries.Code);
            NoseriesLine.Validate("Line No.", 10000);
            NoSeriesLine.Insert(true);
            NoSeriesLine.VALIDATE("Starting Date", WorkDate());
            NoSeriesLine.Validate("Starting No.", NoSeriesStartingNo);
            NoSeriesLine.Modify(true);
        END;
    end;

    local procedure InitBaseCalendarForService()
    var
        ServiceMgtSetup: Record "Service Mgt. Setup";
        BaseCalendar: Record "Base Calendar";
    begin
        IF NOT ServiceMgtSetup.GET() THEN
            ServiceMgtSetup.Insert(true);

        IF ServiceMgtSetup."Base Calendar Code" <> '' THEN
            EXIT;

        IF NOT BaseCalendar.GET('CALENDAR') THEN begin
            BaseCalendar.Init();
            BaseCalendar.VALIDATE(Code, 'CALENDAR');
            BaseCalendar.Insert(true);
            BaseCalendar.Validate(Name, 'Basiskalender');
            BaseCalendar.Modify(true);
        end;
        ServiceMgtSetup.Validate("Base Calendar Code", 'CALENDAR');
        ServiceMgtSetup.MODIFY(true);
    end;

    local procedure InitMandatoryServiceSetupFields()
    var
        ServiceMgtSetup: Record "Service Mgt. Setup";

    begin
        IF NOT ServiceMgtSetup.GET() THEN
            ServiceMgtSetup.Insert(true);

        EVALUATE(ServiceMgtSetup."Default Warranty Duration", '2J');
        ServiceMgtSetup.MODIFY(true);
    end;

    local procedure InitPortalCues()
    var
        PortalCueSet: Record "ICI Portal Cue Set";
        ICISupportSetup: Record "ICI Support Setup";
        PortalCue: Record "ICI Portal Cue";
        LineNo: Integer;
    begin
        PortalCueSet.VALIDATE(Code, 'STD');
        PortalCueSet.Validate(Description, 'Std. Kundenportal Kacheln');
        IF NOT PortalCueSet.Insert() THEN;

        ICISupportSetup.GET();
        IF ICISupportSetup."Default Portal Cue Set Code" = '' then begin
            ICISupportSetup.Validate("Default Portal Cue Set Code", PortalCueSet.Code);
            ICISupportSetup.Modify();
        end;

        LineNo := 10000;
        CLEAR(PortalCue);
        PortalCue."Portal Cue Set Code" := PortalCueSet.Code;
        PortalCue."Line No." := LineNo;
        IF PortalCue.Insert() THEN begin
            LineNo += 10000;
            PortalCue.Description := 'Tickets - Offen';
            PortalCue."State Filter" := '2|3|4'; // Nicht geschlossen
            PortalCue.Modify();
        end;

        CLEAR(PortalCue);
        PortalCue."Portal Cue Set Code" := PortalCueSet.Code;
        PortalCue."Line No." := LineNo;
        IF PortalCue.Insert() THEN begin
            LineNo += 10000;
            PortalCue.Description := 'Meine Tickets - Offen';
            PortalCue."State Filter" := '2|3|4'; // Nicht geschlossen
            PortalCue."Only Own" := true;
            PortalCue.Modify();
        end;

        CLEAR(PortalCue);
        PortalCue."Portal Cue Set Code" := PortalCueSet.Code;
        PortalCue."Line No." := LineNo;
        IF PortalCue.Insert() THEN begin
            LineNo += 10000;
            PortalCue.Description := 'Archiv';
            PortalCue."State Filter" := '5'; // geschlossen
            PortalCue.Modify();
        end;
    end;

    local procedure InitPortalCueTranslations()
    var
        PortalCueTranslation: Record "ICI Portal Cue Translation";
        LineNo: Integer;
    begin
        LineNo := 10000;
        CLEAR(PortalCueTranslation);
        PortalCueTranslation."Portal Cue Set Code" := 'STD';
        PortalCueTranslation."Portal Cue Line No." := LineNo;
        PortalCueTranslation."Language Code" := 'ENU';
        IF PortalCueTranslation.Insert() THEN begin
            LineNo += 10000;
            PortalCueTranslation.Description := 'Tickets - Open';
            PortalCueTranslation.Modify();
        end;

        CLEAR(PortalCueTranslation);
        PortalCueTranslation."Portal Cue Set Code" := 'STD';
        PortalCueTranslation."Portal Cue Line No." := LineNo;
        PortalCueTranslation."Language Code" := 'ENU';
        IF PortalCueTranslation.Insert() THEN begin
            LineNo += 10000;
            PortalCueTranslation.Description := 'My Tickets - Open';
            PortalCueTranslation.Modify();
        end;

        CLEAR(PortalCueTranslation);
        PortalCueTranslation."Portal Cue Set Code" := 'STD';
        PortalCueTranslation."Portal Cue Line No." := LineNo;
        PortalCueTranslation."Language Code" := 'ENU';
        IF PortalCueTranslation.Insert() THEN begin
            LineNo += 10000;
            PortalCueTranslation.Description := 'Archive';
            PortalCueTranslation.Modify();
        end;
    end;

    local procedure InitJobQueues()
    var
        JobQueueEntry: Record "Job Queue Entry";
    begin
        JobQueueEntry.SETRANGE("Object ID to Run", CODEUNIT::"ICI Escalation Mgt.");
        JobQueueEntry.SETRANGE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
        IF NOT JobQueueEntry.IsEmpty THEN
            EXIT;

        Clear(JobQueueEntry);
        JobQueueEntry.InitRecurringJob(10);
        JobQueueEntry.VALIDATE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
        JobQueueEntry.VALIDATE("Object ID to Run", CODEUNIT::"ICI Escalation Mgt.");
        JobQueueEntry.SetStatus(JobQueueEntry.Status::Ready);
        IF NOT JobQueueEntry.Insert(true) THEN
            JobQueueEntry.Modify();

        CLEAR(JobQueueEntry);
        JobQueueEntry.SETRANGE("Object ID to Run", CODEUNIT::"ICI Support License Mgt.");
        JobQueueEntry.SETRANGE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
        IF NOT JobQueueEntry.IsEmpty THEN
            EXIT;

        Clear(JobQueueEntry);
        JobQueueEntry.InitRecurringJob(10);
        JobQueueEntry.VALIDATE("Object Type to Run", JobQueueEntry."Object Type to Run"::Codeunit);
        JobQueueEntry.VALIDATE("Object ID to Run", CODEUNIT::"ICI Support License Mgt.");
        JobQueueEntry.SetStatus(JobQueueEntry.Status::Ready);
        IF NOT JobQueueEntry.Insert(true) THEN
            JobQueueEntry.Modify();

    end;

}
