report 56276 "ICI Support Ticket Payoff"
{
    Caption = 'Ticket Payoff', Comment = 'de-DE=Ticketabrechnung';
    ProcessingOnly = true;
    UsageCategory = Tasks;
    ApplicationArea = All;

    dataset
    {
        dataitem(Customer; "Customer")
        {
            RequestFilterFields = "No.", Name;

            trigger OnAfterGetRecord()
            begin
                CurrReport.BREAK();
            end;

            trigger OnPostDataItem()
            begin
                MESSAGE(NoOfDocsCreatedTxt, NoOfDocuments);
            end;

            trigger OnPreDataItem()
            begin
                SupportPayoffMgt.Inizialize(Customer, PayoffDate, PostingDate, SalespersonCode);
                SupportPayoffMgt.SetSupportUserFilter(SupportUserFilter);
                SupportPayoffMgt.SetReferenceText(YourReferenceText);

                NoOfDocuments := SupportPayoffMgt.Payoff();
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    Caption = 'Options', Comment = 'de-DE=Optionen';
                    field(_PayoffDate; PayoffDate)
                    {
                        Caption = 'Payoff Date', Comment = 'de-DE=Abrechnungsdatum';
                        ToolTip = 'Payoff Date', Comment = 'de-DE=Abrechnungsdatum';
                        ApplicationArea = All;

                        trigger OnValidate()
                        begin
                            UpdateYourReferenceText();
                        end;
                    }
                    /*field(_PostingDate; PostingDate)
                    {
                        Caption = 'Posting Date', Comment = 'de-DE=Buchungsdatum';
                        ToolTip = 'Posting Date', Comment = 'de-DE=Buchungsdatum';
                        ApplicationArea = All;
                    }*/
                    field(_SupportUserFilter; SupportUserFilter)
                    {
                        Caption = 'Salesperson Filter', Comment = 'de-DE=Mitarbeiter-Abrechnungsfilter';
                        ToolTip = 'Salesperson Filter', Comment = 'de-DE=Mitarbeiter-Abrechnungsfilter';
                        TableRelation = "ICI Support User";
                        ApplicationArea = All;

                        trigger OnValidate()
                        begin
                            UpdateYourReferenceText();
                        end;
                    }
                    field(_SalespersonCode; SalespersonCode)
                    {
                        Caption = 'Salesperson', Comment = 'de-DE=Verkäufer';
                        ToolTip = 'Salesperson', Comment = 'de-DE=Verkäufer';
                        TableRelation = "Salesperson/Purchaser";
                        ApplicationArea = All;
                    }
                    field(_YourReferenceText; YourReferenceText)
                    {
                        Caption = 'Your Reference Text', Comment = 'de-DE=Ihre Referenz';
                        ToolTip = 'Your Reference Text', Comment = 'de-DE=Ihre Referenz';
                        ApplicationArea = All;
                    }
                }
            }
        }

        trigger OnOpenPage()
        var
            ICISupportUser: Record "ICI Support User";
        begin
            PostingDate := WORKDATE();
            PayoffDate := WORKDATE();

            UpdateYourReferenceText();

            IF ICISupportUser.GetCurrUser(ICISupportUser) THEN
                IF ICISupportUser."Salesperson Code" <> '' then
                    SalespersonCode := ICISupportUser."Salesperson Code";
        end;
    }

    trigger OnInitReport()
    var
        ICISupportSetup: Record "ICI Support Setup";
        TimeRegistrationNotLicensedErr: Label 'Time Registration is not licensed', Comment = 'de-DE=Zeiterfassung ist nicht lizenziert';
    begin
        ICISupportSetup.GET();
        IF NOT ICISupportSetup."Time Registration" then
            ERROR(TimeRegistrationNotLicensedErr);
    end;

    local procedure UpdateYourReferenceText()
    var
        PayoffTxt: Label 'Support-Payoff for %1', Comment = '%1=Date|de-DE=Support-Abrechnung zum %1';
    begin
        YourReferenceText := STRSUBSTNO(PayoffTxt, PayoffDate);
    end;

    var
        SupportPayoffMgt: Codeunit "ICI Support Payoff Mgt.";
        PostingDate: Date;
        PayoffDate: Date;
        NoOfDocsCreatedTxt: Label '%1 Documents were created.', Comment = '%1= No of Docs created|de-DE=Es wurden %1 Belege erzeugt.';
        NoOfDocuments: Integer;
        SupportUserFilter: Text[50];
        YourReferenceText: Text[80];
        SalespersonCode: Code[20];
}
