page 56315 "ICI Ticket Doc. Search Dialog"
{
    Caption = 'ICI Ticket Reference Dialog', Comment = 'de-DE=Belegnummernsuche';
    PageType = StandardDialog;
    ApplicationArea = All;
    UsageCategory = Tasks;

    layout
    {
        area(content)
        {
            group(Search)
            {
                Caption = 'Search', Comment = 'de-DE=Suche';
                field(ReferenceNo; ReferenceNo)
                {
                    ApplicationArea = All;
                    Caption = 'Reference No.', Comment = 'de-DE=Belegnr.';
                    ToolTip = 'Reference No.', Comment = 'de-DE=Referenznr.';
                    trigger OnValidate()
                    begin
                        CurrPage."ICI Reference Listpart".Page.FillPageFromReferenceNo(ReferenceNo, TicketNo, 0); // 0 is Document Search only
                        CurrPage."ICI Reference Listpart".Page.Update(false);
                    end;
                }
            }

            group(TicketValues)
            {
                Caption = 'Linked Ticket', Comment = 'de-DE=Ticket';
                Visible = ShowTicketNo;
                Editable = false;

                field(TicketNo; TicketNo)
                {
                    ApplicationArea = All;
                    Caption = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    Visible = ShowTicketNo;
                }
                field(TicketCustomerNo; TicketCustomerNo)
                {
                    ApplicationArea = All;
                    Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    Visible = ShowTicketCustomerNo;

                }
            }
            group("Options-Sales")
            {
                Caption = 'Sales', Comment = 'de-DE=Verkauf';

                group(SalesQuote)
                {
                    Caption = 'Salesquotes', Comment = 'de-DE=Angebote';
                    field("Search S.Q. by Ext. Doc. No."; ICISupportUser."Search S.Q. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote by External Document No.', Comment = 'de-DE=Verkaufsangebote nach Externer Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Quote by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.Q. by Ext. Doc. No.", ICISupportUser."Search S.Q. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.Q. by No."; ICISupportUser."Search S.Q. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote by No.', Comment = 'de-DE=Verkaufsangebote nach Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Quote by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.Q. by No.", ICISupportUser."Search S.Q. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group(SalesOrder)
                {
                    Caption = 'Salesorders', Comment = 'de-DE=Aufträge';
                    field("Search S.O. by Ext. Doc. No."; ICISupportUser."Search S.O. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by External Document No.', Comment = 'de-DE=Verkaufsaufträge nach Externer Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Order by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O. by Ext. Doc. No.", ICISupportUser."Search S.O. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.O. by No."; ICISupportUser."Search S.O. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by No.', Comment = 'de-DE=Verkaufsaufträge nach Belegnr.';
                        Caption = 'Specifies the value of the Search Sales Order by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O. by No.", ICISupportUser."Search S.O. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.O. by Quote No."; ICISupportUser."Search S.O. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order by Quote No.', Comment = 'de-DE=Verkaufsaufträge nach Angebotsgnr.';
                        Caption = 'Specifies the value of the Search Sales Order by Quote No.', Comment = 'de-DE=Angebotsgnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O. by Quote No.", ICISupportUser."Search S.O. by Quote No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }

                group(SalesQuoteArchive)
                {
                    Caption = 'Salesquotesarchive', Comment = 'de-DE=Angebotesarchiv';
                    field("Search S.Q.A. by Ext. Doc. No."; ICISupportUser."Search S.Q.A. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote Archive by External Document No.', Comment = 'de-DE=Verkaufsangebotsarchiv nach Externer Belegnr.';
                        Caption = 'External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.Q.A. by Ext. Doc. No.", ICISupportUser."Search S.Q.A. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.Q.A. by No."; ICISupportUser."Search S.Q.A. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Quote Archive by No.', Comment = 'de-DE=Verkaufsangebotsarchiv nach Belegnr.';
                        Caption = 'No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.Q.A. by No.", ICISupportUser."Search S.Q.A. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group(SalesOrderArchive)
                {
                    Caption = 'Salesorderarchive', Comment = 'de-DE=Auftragsarchiv';
                    field("Search S.O.A. by Ext. Doc. No."; ICISupportUser."Search S.O.A. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by External Document No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Externer Belegnr.';
                        Caption = 'External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O.A. by Ext. Doc. No.", ICISupportUser."Search S.O.A. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.O.A. by No."; ICISupportUser."Search S.O.A. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Belegnr.';
                        Caption = 'No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O.A. by No.", ICISupportUser."Search S.O.A. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.O.A. by Quote No."; ICISupportUser."Search S.O.A. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Sales Order Archive by Quote No.', Comment = 'de-DE=Verkaufsauftragsarchiv nach Angebotsgnr.';
                        Caption = 'Quote No.', Comment = 'de-DE=Angebotsgnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.O.A. by Quote No.", ICISupportUser."Search S.O.A. by Quote No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }

                group(PstSalesInv)
                {
                    Caption = 'Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnungen';
                    field("Search S.I. by Ext. Doc. No."; ICISupportUser."Search S.I. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by External Document No.', Comment = 'de-DE=Geb. Rechnungen nach Externer Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.I. by Ext. Doc. No.", ICISupportUser."Search S.I. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.I. by No."; ICISupportUser."Search S.I. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by No.', Comment = 'de-DE=Geb. Rechnungen nach Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.I. by No.", ICISupportUser."Search S.I. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.I. by Order No."; ICISupportUser."Search S.I. by Order No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by Order No.', Comment = 'de-DE=Geb. Rechnungen nach Auftragsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by Order No.', Comment = 'de-DE=Auftragsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.I. by Order No.", ICISupportUser."Search S.I. by Order No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.I. by Quote No."; ICISupportUser."Search S.I. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Invoices by Quote No.', Comment = 'de-DE=Geb. Rechnungen nach Angebotsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Invoices by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.I. by Quote No.", ICISupportUser."Search S.I. by Quote No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }

                }
                group(PstSalesShip)
                {
                    Caption = 'Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferungen';
                    field("Search S.S. by Ext. Doc. No."; ICISupportUser."Search S.S. by Ext. Doc. No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by External Document No.', Comment = 'de-DE=Geb. Lieferungen nach Externer Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by External Document No.', Comment = 'de-DE=Externe Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.S. by Ext. Doc. No.", ICISupportUser."Search S.S. by Ext. Doc. No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.S. by No."; ICISupportUser."Search S.S. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by No.', Comment = 'de-DE=Geb. Lieferungen nach Belegnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.S. by No.", ICISupportUser."Search S.S. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search S.S. by Order No."; ICISupportUser."Search S.S. by Order No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by Order No.', Comment = 'de-DE=Geb. Lieferungen nach Auftragsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by Order No.', Comment = 'de-DE=Auftragsnr.';
                        ApplicationArea = All;
                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.S. by Order No.", ICISupportUser."Search S.S. by Order No.");
                            ICISupportUser2.MODIFY();
                        end;

                    }
                    field("Search S.S. by Quote No."; ICISupportUser."Search S.S. by Quote No.")
                    {
                        ToolTip = 'Specifies the value of the Search Posted Sales Shipment by Quote No.', Comment = 'de-DE=Geb. Lieferungen nach Angebotsnr.';
                        Caption = 'Specifies the value of the Search Posted Sales Shipment by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search S.S. by Quote No.", ICISupportUser."Search S.S. by Quote No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
            }
            group("Options-Service")
            {
                Caption = 'Service', Comment = 'de-DE=Service';
                Visible = ShowService;
                group("Service-Quote")
                {
                    Caption = 'Servicequotes', Comment = 'de-DE=Angebote';
                    field("Search Serv. Q. by No."; ICISupportUser."Search Serv. Q. by No.")
                    {
                        ToolTip = 'Search Service Quote by No.', Comment = 'de-DE=Serviceangebote nach Belegnr.';
                        Caption = 'Search Service Quote by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. Q. by No.", ICISupportUser."Search Serv. Q. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group("Service-Order")
                {
                    Caption = 'Serviceorders', Comment = 'de-DE=Aufträge';
                    field("Search Serv. O. by No."; ICISupportUser."Search Serv. O. by No.")
                    {
                        ToolTip = 'Search Service Order by No.', Comment = 'de-DE=Serviceaufträge nach Belegnr.';
                        Caption = 'Search Service Order by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. O. by No.", ICISupportUser."Search Serv. O. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search Serv. O. by Quote No."; ICISupportUser."Search Serv. O. by Quote No.")
                    {
                        ToolTip = 'Search Service Order by Quote No.', Comment = 'de-DE=Serviceaufträge nach Angebotsnr.';
                        Caption = 'Search Service Order by Quote No.', Comment = 'de-DE=Angebotsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. O. by Quote No.", ICISupportUser."Search Serv. O. by Quote No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group("PstServiceInv")
                {
                    Caption = 'Posted Service Invoices', Comment = 'de-DE=Geb. Rechnungen';
                    field("Search Serv. I. by No."; ICISupportUser."Search Serv. I. by No.")
                    {
                        ToolTip = 'Search Service Invoice by No.', Comment = 'de-DE=Geb. Servicerechnung nach Belegnr.';
                        Caption = 'Search Service Invoice by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. I. by No.", ICISupportUser."Search Serv. I. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group("PstServiceShipments")
                {
                    Caption = 'Posted Service Shipments', Comment = 'de-DE=Geb. Lieferungen';
                    field("Search Serv. S. by No."; ICISupportUser."Search Serv. S. by No.")
                    {
                        ToolTip = 'Search Service Shipment by No.', Comment = 'de-DE=Geb. Servicelieferung nach Belegnr.';
                        Caption = 'Search Service Shipment by No.', Comment = 'de-DE=Belegnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. S. by No.", ICISupportUser."Search Serv. S. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group("Servicecontractquotes")
                {
                    Caption = 'Servicecontractquotes', Comment = 'de-DE=Vertragsangebote';
                    field("Search Serv. C. Q. by No."; ICISupportUser."Search Serv. C. Q. by No.")
                    {
                        ToolTip = 'Search Service Contract Quote by No.', Comment = 'de-DE=Servicevertragsangebot nach Vertragsnr.';
                        Caption = 'Search Service Contract Quote by No.', Comment = 'de-DE=Servicevertragsangebot nach Vertragsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. C. Q. by No.", ICISupportUser."Search Serv. C. Q. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
                group("Servicecontracts")
                {
                    Caption = 'Servicecontracts', Comment = 'de-DE=Verträge';
                    field("Search Serv. C. by No."; ICISupportUser."Search Serv. C. by No.")
                    {
                        ToolTip = 'Search Service Contract by No.', Comment = 'de-DE=Servicevertrag nach Vertragsnr.';
                        Caption = 'Search Service Contract by No.', Comment = 'de-DE=Vertragsnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Serv. C. by No.", ICISupportUser."Search Serv. C. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
            }
            part("ICI Reference Listpart"; "ICI Reference Listpart")
            {
                Caption = 'Search Results', Comment = 'de-DE=Suchergebnisse';
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
    }

    trigger OnOpenPage()
    var
        SupportSetup: Record "ICI Support Setup";
    begin
        ICISupportUser.GetCurrUser(ICISupportUser);

        CurrPage."ICI Reference Listpart".Page.SetAddressSearch();

        SupportSetup.Get();
        ShowService := SupportSetup."Service Integration";
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        ICIReferenceNoBuffer: Record "ICI Reference No. Buffer";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        TicketCreated: Boolean;
        ConfirmCloseWithUnsavedChangesLbl: Label 'The Site contains unsaved changes. Do you want to close the Wizard anyways?', Comment = 'de-DE=Die Seite enthält ungespeicherte Änderungen. Wollen Sie die Seite dennoch schließen?';
        CloseWithoutCreateMsg: Label 'No Reference No. was selected. Do you want to close the Wizard anyways?', Comment = 'de-DE=Sie haben keine Referenznr ausgewählt. Wollen Sie die Seite dennoch schließen?';
    begin
        IF CloseAction in [ACTION::OK, ACTION::LookupOK] THEN begin
            // Priority: 1. Use ICIReferenceNoBuffer.Code, 2. No ICIReferenceNoBuffer.Code then The Typed Reference No
            CurrPage."ICI Reference Listpart".Page.GetRecord(ICIReferenceNoBuffer);

            IF ReferenceNo = '' THEN
                EXIT(CONFIRM(CloseWithoutCreateMsg));

            TicketCreated := (ICISupportTicket."No." = '');
            ICISupportDocumentMgt.ProcessReferenceNoBuffer(ICIReferenceNoBuffer, ICISupportTicket, ReferenceNo);
            //ICISupportDocumentMgt.SearchAndProcessReferenceNo(ReferenceNo, Ticket);
            IF TicketCreated THEN
                Page.Run(PAGE::"ICI Support Ticket Card", ICISupportTicket);
        end else
            if ReferenceNo <> '' THEN EXIT(CONFIRM(ConfirmCloseWithUnsavedChangesLbl))


    end;

    procedure SetValues(pTicketNo: Code[20]; pReferenceNo: Code[50])
    begin
        TicketNo := pTicketNo;
        ShowTicketNo := (TicketNo <> '');

        ICISupportTicket.GET(TicketNo);
        TicketCustomerNo := ICISupportTicket."Customer No.";

        ShowTicketCustomerNo := (ICISupportTicket."Customer No." <> '');

        ReferenceNo := pReferenceNo;
        CurrPage."ICI Reference Listpart".Page.FillPageFromReferenceNo(ReferenceNo, TicketNo, 0); // 0 = Documentsearch
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        TicketNo: Code[20];
        ReferenceNo: Code[50];
        TicketCustomerNo: Code[20];
        ShowTicketNo: Boolean;
        ShowTicketCustomerNo: Boolean;
        ShowService: Boolean;
}
