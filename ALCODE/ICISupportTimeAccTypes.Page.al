page 56317 "ICI Support Time Acc. Types"
{

    ApplicationArea = All;
    Caption = 'ICI Support Time Acc. Types', Comment = 'de-DE=Zeiterfassungsabrechnungsarten';
    PageType = List;
    SourceTable = "ICI Support Time Acc. Type";
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field(Accounting; Rec.Accounting)
                {
                    ToolTip = 'Specifies the value of the Accounting', Comment = 'de-DE=Abrechnen';
                    ApplicationArea = All;
                }
                field("Discount %"; Rec."Discount %")
                {
                    ToolTip = 'Specifies the value of the Discount %', Comment = 'de-DE=Rabatt %';
                    ApplicationArea = All;
                }
            }
        }
    }

}
