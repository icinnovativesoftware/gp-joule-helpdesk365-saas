codeunit 56289 "ICI Support Action Handler"
{
    procedure OpenContactCard(pNotification: Notification)
    var
        Contact: Record Contact;
        ContactCard: Page "Contact Card";
        ContactNo: Code[20];
    begin
        ContactNo := COPYSTR(pNotification.GetData('ContactNo'), 1, 20);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT;

        ContactCard.SetRecord(Contact);
        ContactCard.Run();
    end;

    procedure OpenUserList(pNotification: Notification)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportUserList: Page "ICI Support User List";
        lUserID: Code[50];
    begin
        lUserID := COPYSTR(pNotification.GetData('UserID'), 1, 50);
        IF NOT ICISupportUser.GET(lUserID) THEN
            EXIT;

        ICISupportUserList.SetRecord(ICISupportUser);
        ICISupportUserList.Run();
    end;

    procedure SetUserSalutationM(pNotification: Notification)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        lUserID: Code[50];
        SalutationCode: Code[10];
    begin
        lUserID := COPYSTR(pNotification.GetData('UserID'), 1, 50);
        IF NOT ICISupportUser.GET(lUserID) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code M";
        ICISupportUser.Validate("Salutation Code", SalutationCode);
        ICISupportUser.Modify();
    end;

    procedure SetUserSalutationF(pNotification: Notification)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        lUserID: Code[50];
        SalutationCode: Code[10];
    begin
        lUserID := COPYSTR(pNotification.GetData('UserID'), 1, 50);
        IF NOT ICISupportUser.GET(lUserID) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code F";
        ICISupportUser.Validate("Salutation Code", SalutationCode);
        ICISupportUser.Modify();
    end;

    procedure SetUserSalutationD(pNotification: Notification)
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        lUserID: Code[50];
        SalutationCode: Code[10];
    begin
        lUserID := COPYSTR(pNotification.GetData('UserID'), 1, 50);
        IF NOT ICISupportUser.GET(lUserID) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code D";
        ICISupportUser.Validate("Salutation Code", SalutationCode);
        ICISupportUser.Modify();
    end;

    procedure SetContactSalutationM(pNotification: Notification)
    var
        Contact: Record Contact;
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ContactNo: Code[20];
        SalutationCode: Code[10];
    begin
        ContactNo := COPYSTR(pNotification.GetData('ContactNo'), 1, 20);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code M";
        Contact.Validate("Salutation Code", SalutationCode);
        Contact.Modify();
    end;

    procedure SetContactSalutationF(pNotification: Notification)
    var
        Contact: Record Contact;
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ContactNo: Code[20];
        SalutationCode: Code[10];
    begin
        ContactNo := COPYSTR(pNotification.GetData('ContactNo'), 1, 20);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code F";
        Contact.Validate("Salutation Code", SalutationCode);
        Contact.Modify();
    end;

    procedure SetContactSalutationD(pNotification: Notification)
    var
        Contact: Record Contact;
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ContactNo: Code[20];
        SalutationCode: Code[10];
    begin
        ContactNo := COPYSTR(pNotification.GetData('ContactNo'), 1, 20);
        IF NOT Contact.GET(ContactNo) THEN
            EXIT;
        ICISupportMailSetup.GET();
        SalutationCode := ICISupportMailSetup."Salutation Code D";
        Contact.Validate("Salutation Code", SalutationCode);
        Contact.Modify();
    end;

    procedure OpenTicketCard(pNotification: Notification)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportTicketCard: Page "ICI Support Ticket Card";
        TicketNo: Code[20];
    begin
        TicketNo := COPYSTR(pNotification.GetData('TicketNo'), 1, 20);
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT;

        ICISupportTicketCard.SetRecord(ICISupportTicket);
        ICISupportTicketCard.Run();
    end;
}
