page 56280 "ICI Support Ticket Cat. List"
{

    ApplicationArea = All;
    Caption = 'Support Ticket Category List', Comment = 'de-DE=Support Ticket Kategorien';
    PageType = List;
    SourceTable = "ICI Support Ticket Category";
    //SourceTableView = sorting(Layer, "Parent Category", Code);
    UsageCategory = Administration;
    CardPageId = "ICI Ticket Category Card";


    layout
    {
        area(content)
        {
            repeater(General)
            {
                IndentationColumn = Indent;// IndentationColumn = IndentVariable;
                IndentationControls = Code;
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Parent Category"; Rec."Parent Category")
                {
                    Visible = ShowParent;
                    ApplicationArea = All;
                    ToolTip = 'Parent Category', Comment = 'de-DE=Gehört zu Kategorie';
                }
                field(Layer; Rec.Layer)
                {
                    Visible = ShowLayer;
                    ApplicationArea = All;
                    ToolTip = 'Layer', Comment = 'de-DE=Ebene';
                }
                field(Inactive; Rec.Inactive)
                {
                    ApplicationArea = All;
                    ToolTip = 'Inactive', Comment = 'de-DE=Inaktiv';
                }
                field(Online; Rec.Online)
                {
                    ApplicationArea = All;
                    ToolTip = 'Online', Comment = 'de-DE=Online';
                    Visible = ShowPortal;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Translation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Translations', Comment = 'de-DE=Übersetzungen';
                Image = Transactions;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                RunObject = page "ICI Tick. Cat. Translations";
                RunPageLink = "Ticket Category Code" = field("Code");

                ToolTip = 'Shows the Translations of this Category', Comment = 'de-DE=Zeigt die Übersetzungen dieser Kategorie an';
            }
            action(PortalSync)
            {
                ApplicationArea = All;
                Image = LinkWeb;
                ToolTip = 'Portal Sync.', Comment = 'de-DE=Mit Portal synchronisieren';
                Caption = 'Portal Sync.', Comment = 'de-DE=Synchronisieren';
                trigger OnAction()
                var
                    ICIPortalMgt: Codeunit "ICI Portal Mgt.";
                    PortalUpdateMgt: Codeunit "ICI Portal Update Mgt.";
                begin
                    ICIPortalMgt.UpdateAllCategories(true, PortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCategorieTranslations(true, PortalUpdateMgt);

                    ICIPortalMgt.UpdateAllCategories(false, PortalUpdateMgt);
                    ICIPortalMgt.UpdateAllCategorieTranslations(false, PortalUpdateMgt);
                    PortalUpdateMgt.SendUpdateTableBuffered();  // Flush remaining data

                    CurrPage.Update(FALSE);
                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        IF ShowLayer THEN
            Indent := Rec.Layer; // Layer has min 0 and max 1

        IF ShowParent THEN begin
            Indent := 0;
            IF Rec."Parent Category" <> '' THEN
                Indent := 1;
        end;
    end;

    trigger OnOpenPage()
    begin
        ICISupportSetup.GET();
        ShowLayer := ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Two-Layers";
        ShowParent := ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers";
        // ShowTicketboard := ICISupportSetup."Ticketboard active";
        ShowPortal := ICISupportSetup."Portal Integration";
    end;

    var

        ICISupportSetup: Record "ICI Support Setup";
        Indent: Integer;
        ShowLayer: Boolean;
        ShowParent: Boolean;
        ShowPortal: Boolean;
}
