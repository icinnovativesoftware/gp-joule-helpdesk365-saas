page 56366 "ICI Notification Dialog"
{

    Caption = 'Notification Dialog', Comment = 'de-DE=Nachricht erfassen';
    PageType = StandardDialog;
    DataCaptionExpression = NotificationDialogDataCaptionExpr;

    layout
    {
        area(content)
        {
            field(ToRecipients; ToRecipients)
            {
                ToolTip = 'Recipients', Comment = 'de-DE=Empfänger';
                Caption = 'Recipients', Comment = 'de-DE=Empfänger';
                ApplicationArea = All;
                Editable = false;
                MultiLine = true;

                trigger OnAssistEdit()
                var
                    ICINotReceiverBufferListp: Page "ICI Not. Receiver Buffer Listp";
                begin
                    ICINotReceiverBufferListp.SetNotificationDialog(ICINotificationDialogMgt);

                    ICINotReceiverBufferListp.LOOKUPMODE(TRUE);
                    IF ICINotReceiverBufferListp.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        // Update ToRecipient
                        ToRecipients := ICINotificationDialogMgt.GetRecipientsAsText();
                        CurrPage.Update(false);
                    END;
                end;
            }
            field(Attachments; Attachments)
            {
                ToolTip = 'Attachments', Comment = 'de-DE=Anhänge';
                Caption = 'Attachments', Comment = 'de-DE=Anhänge';
                ApplicationArea = All;
                Editable = false;

                trigger OnAssistEdit()
                var
                    ICINotAttachmentBufferListp: Page "ICI Not. Attach. Buffer Listp";
                begin
                    ICINotAttachmentBufferListp.SetNotificationDialog(ICINotificationDialogMgt);

                    ICINotAttachmentBufferListp.LOOKUPMODE(TRUE);
                    IF ICINotAttachmentBufferListp.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        // Update ToRecipient
                        Attachments := ICINotificationDialogMgt.GetAttachmentsAsText();
                        CurrPage.Update(false);
                    END;
                end;
            }

            field(Subject; Subject)
            {
                ToolTip = 'Subject', Comment = 'de-DE=Betreff';
                Caption = 'Subject', Comment = 'de-DE=Betreff';
                ApplicationArea = All;
            }
            field(Internally; Internally)
            {
                ToolTip = 'Internally', Comment = 'de-DE=Intern';
                Caption = 'Internally', Comment = 'de-DE=Intern';
                ApplicationArea = All;
                trigger OnValidate()
                begin
                    NotificationDialogDataCaptionExpr := NotificationDialogDataCaptionExprLbl;
                    IF Internally THEN
                        NotificationDialogDataCaptionExpr := NotificationDialogDataCaptionExprInternallyLbl;

                    ICINotificationDialogMgt.SetInternally(Internally);
                    CurrPage.Update();
                end;
            }

            group(NotificationText)
            {
                Caption = 'Text', Comment = 'de-DE=Inhalt';

                usercontrol(Editor; "ICI Text Module Editor CK4")
                {
                    ApplicationArea = All;

                    trigger ControlReady()
                    begin
                        AddinReady := true;
                        CurrPage.Editor.Init(ICINotificationDialogMgt.GetInitData(false));
                    end;

                    trigger OnAfterInit()
                    var
                        JTok: JsonToken;
                        NotificationText: Text;
                    begin
                        IF Values.GET('InitText', JTok) then
                            NotificationText := JTok.AsValue().AsText();

                        CurrPage.Editor.Load(NotificationText);
                    end;

                    trigger SaveRequested(Data: JsonObject)
                    var
                        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
                        SupportMailmgt: Codeunit "ICI Support Mail Mgt.";
                        NoTextEnteredErr: Label 'No Text was Entered', Comment = 'de-DE=Keine Nachricht eingegeben';
                        jToken: JsonToken;
                        jVal: JsonValue;
                        JsonTxt: Text;
                    begin
                        Data.Get('Text', jToken);
                        jVal := jToken.AsValue();
                        JsonTxt := jVal.AsText();
                        if JsonTxt = '' then
                            Error(NoTextEnteredErr);

                        COMMIT();

                        ICINotificationDialogMgt.SetInitText(JsonTxt);
                        ICINotificationDialogMgt.SetInitSubject(Subject);
                        NotificationDialogCloseAction();
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        JTok: JsonToken;

    begin
        // Werte aus Values JSON setzen
        // Außer InitText - der erst bei geladenem AddIn
        //CurrPage."ICI Not. Receiver Buffer Listp".Page.SetNotificationDialog(ICINotificationDialogMgt);
        //CurrPage."ICI Not. Attach. Buffer Listp".Page.SetNotificationDialog(ICINotificationDialogMgt);

        IF Values.GET('InitSubject', JTok) then
            Subject := JTok.AsValue().AsText();

        ToRecipients := ICINotificationDialogMgt.GetRecipientsAsText();
        Attachments := ICINotificationDialogMgt.GetAttachmentsAsText();

        Internally := ICINotificationDialogMgt.GetInternally();
        NotificationDialogDataCaptionExpr := NotificationDialogDataCaptionExprLbl;
        IF Internally THEN
            NotificationDialogDataCaptionExpr := NotificationDialogDataCaptionExprInternallyLbl;
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var

        ConfirmCloseWithUnsavedChangesLbl: Label 'The Site contains unsaved changes. Do you want to close the Wizard anyways?', Comment = 'de-DE=Die Seite enthält ungespeicherte Änderungen. Wollen Sie die Seite dennoch schließen?';
    begin
        IF WizardCompleted THEN
            exit(true);
        IF CloseAction in [ACTION::OK, ACTION::LookupOK] THEN begin
            // OK gedrückt 
            CurrPage.Editor.RequestSave();
            EXIT(FALSE);
        end else
            EXIT(CONFIRM(ConfirmCloseWithUnsavedChangesLbl))
    end;

    procedure SetNotificationDialog(var pICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    begin
        ICINotificationDialogMgt := pICINotificationDialogMgt;
        Values := ICINotificationDialogMgt.CreateNotificationValues();
    END;

    local procedure NotificationDialogCloseAction()
    var
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
    begin
        ICISupportTicketLogMgt.SaveTicketMessage(ICINotificationDialogMgt);
        ICISupportTicketLogMgt.AddSupportTicketFileLog(ICINotificationDialogMgt);
        ICISupportMailMgt.SendMail(ICINotificationDialogMgt);
        WizardCompleted := true;
        CurrPage.Close();
    end;

    var
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        Subject: Text;
        Internally: Boolean;
        ToRecipients: Text;
        Attachments: Text;
        AddinReady: Boolean;
        Values: JsonObject;
        WizardCompleted: Boolean;
        NotificationDialogDataCaptionExpr: Text;
        NotificationDialogDataCaptionExprLbl: Label 'Message', Comment = 'de-DE=Nachricht';
        NotificationDialogDataCaptionExprInternallyLbl: Label 'Internal Message', Comment = 'de-DE=Interne Nachricht';
}
