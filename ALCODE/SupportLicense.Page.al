page 56378 "ICI Support License"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Support License', Comment = 'de-DE=HelpDesk Lizenz';
    PageType = Card;
    SourceTable = "ICI Support Setup";
    Editable = false;

    layout
    {
        area(content)
        {

            group(License)
            {
                Caption = 'License', Comment = 'de-DE=Lizenz';

                field("Next Scheduled License Check"; Rec."Next Scheduled License Check")
                {
                    ApplicationArea = All;
                    Tooltip = 'Next Scheduled License Check', Comment = 'de-DE=Nächster Lizenzcheck';
                    Importance = Promoted;
                }
                group(Moduls)
                {

                    Caption = 'Moduls', Comment = 'de-DE=Lizenzierte BC-Module';
                    field("Portal Integration"; Rec."Portal Integration")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Portal Integration', Comment = 'de-DE=Kundenportal';
                    }
                    field("Guest Form"; Rec."License Module 6")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Guest Form', Comment = 'de-DE=Gastformular';
                    }
                    field("Ticketboard active"; Rec."Ticketboard active")
                    {
                        ApplicationArea = All;
                        Tooltip = 'Ticketboard active', Comment = 'de-DE=Gibt an, ob das Ticketboard angezeigt wird';
                    }
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {


            action(SyncLicense)
            {
                ApplicationArea = All;
                Image = EncryptionKeys;
                ToolTip = 'Synchronize the License for this Company', Comment = 'de-DE=Lizenzinformation abrufen';
                Caption = 'Synchronize the License for this Company', Comment = 'de-DE=Lizenzinformation abrufen';
                trigger OnAction()
                var
                    LicMgt: Codeunit "ICI Support License Mgt.";
                begin
                    LicMgt.SyncLicenseStatus();
                    CurrPage.Update(FALSE);
                end;
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        if not ICISupportSetup.GET() THEN
            ICISupportSetup.INSERT(true);
    end;

}
