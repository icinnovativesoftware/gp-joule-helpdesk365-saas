table 56300 "ICI Support Time Acc. Type"
{
    Caption = 'ICI Support Time Accounting Type', Comment = 'de-DE=Zeiterfassungsabrechnungsart';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Support Time Acc. Types";
    DrillDownPageId = "ICI Support Time Acc. Types";

    fields
    {
        field(1; Code; Code[20])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "Accounting"; Boolean)
        {
            Caption = 'Accounting', Comment = 'de-DE=Abrechnen';
            DataClassification = CustomerContent;
        }
        field(12; "Discount %"; Decimal)
        {
            Caption = 'Discount %', Comment = 'de-DE=Rabatt %';
            DataClassification = CustomerContent;
            MinValue = 0;
            MaxValue = 100;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; "Code", Description, Accounting, "Discount %")
        {
        }
        fieldgroup(Brick; "Code", Description, Accounting, "Discount %")
        {
        }
    }
}
