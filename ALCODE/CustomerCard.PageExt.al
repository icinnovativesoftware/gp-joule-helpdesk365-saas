pageextension 56300 "ICI Customer Card" extends "Customer Card"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Customer Factbox"; "ICI Customer Factbox")
            {
                ApplicationArea = All;
                Visible = ShowCustomerFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                action(CreateTicket)
                {
                    Caption = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ToolTip = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ApplicationArea = All;
                    Image = New;
                    trigger OnAction()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                    begin
                        ICISupportTicket.Init();
                        ICISupportTicket.Insert(true);
                        ICISupportTicket.Validate(Description, Rec.Name);

                        ICISupportTicket.Validate("Customer No.", Rec."No.");
                        ICISupportTicket.FillMissingContactOrCustomerInformation();
                        ICISupportTicket.Modify(true);

                        PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket);
                    end;
                }
            }
        }
    }


    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() then
                ShowCustomerFactbox := true;
    end;

    var
        ShowCustomerFactbox: Boolean;
}
