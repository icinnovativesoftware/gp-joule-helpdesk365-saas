enum 56278 "ICI Document Search Type"
{
    Extensible = true;

    value(0; "Pst.Invoice")
    {
        Caption = 'Pst.Invoice', Comment = 'de-DE=Geb. VK-Rechnung';
    }
    value(1; "Quote")
    {
        Caption = 'Quote', Comment = 'de-DE=VK-Angebot';
    }
    value(2; "Order")
    {
        Caption = 'Order', Comment = 'de-DE=VK-Auftrag';
    }
    value(3; "Pst.Shipment")
    {
        Caption = 'Pst.Shipment', Comment = 'de-DE=Geb. VK-Lieferung';
    }
    value(4; "Service Item")
    {
        Caption = 'Service Item', Comment = 'de-DE=Serviceartikel';
    }
    value(5; "Service Order")
    {
        Caption = 'Service Order', Comment = 'de-DE=Serviceauftrag';
    }
    value(6; "Service Quote")
    {
        Caption = 'Service Quote', Comment = 'de-DE=Serviceangebot';
    }
    value(7; "Service Contract")
    {
        Caption = 'Service Contract', Comment = 'de-DE=Servicevertrag';
    }
    value(8; "Service Contract Quote")
    {
        Caption = 'Service Contract Quote', Comment = 'de-DE=Servicevertragsangebot';
    }
    value(9; "Pst. Service Shipment")
    {
        Caption = 'Pst. Service Shipment', Comment = 'de-DE=Geb. Servicelieferung';
    }
    value(10; "Pst. Service Invoice")
    {
        Caption = 'Pst. Service Invoice', Comment = 'de-DE=Geb. Servicerechnung';
    }
    value(11; "Quote Archive")
    {
        Caption = 'Quote Archive', Comment = 'de-DE=VK-Angebotsarchiv';
    }
    value(12; "Order Archive")
    {
        Caption = 'Order Archive', Comment = 'de-DE=VK-Auftragsarchiv';
    }
    value(13; "Customer Address")
    {
        Caption = 'Customer Address', Comment = 'de-DE=Debitorenadresse';
    }
    value(14; "Contact Address")
    {
        Caption = 'Contact Address', Comment = 'de-DE=Kontaktadresse';
    }
    value(15; "Ship-To Address")
    {
        Caption = 'Ship-To Address', Comment = 'de-DE=Lieferadresse';
    }
}
