tableextension 56289 "ICI Service Line" extends "Service Line"
{
    fields
    {
        field(56276; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'ICI Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                SupportTicket: Record "ICI Support Ticket";
            begin
                IF SupportTicket.GET("ICI Support Ticket No.") then
                    PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
            end;
        }
        field(56277; "ICI Time Log Line No."; Integer)
        {
            Caption = 'ICI Time Log Line No.', Comment = 'de-DE=Zeiterfassungszeilennr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(Support; "ICI Support Ticket No.") { }
    }
}
