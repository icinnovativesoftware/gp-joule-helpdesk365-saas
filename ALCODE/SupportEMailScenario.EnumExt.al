enumextension 56276 "ICI Supoprt E-Mail Scenario" extends "Email Scenario"
{

    value(56276; Helpdesk365)
    {
        Caption = 'Helpdesk365', Comment = 'Helpdesk365';
    }
    value(56277; "Helpdesk365 - Mailbox 1")
    {
        Caption = 'Helpdesk365 - Mailbox 1', Comment = 'Helpdesk365 - Mailbox 1';
    }
    value(56278; "Helpdesk365 - Mailbox 2")
    {
        Caption = 'Helpdesk365 - Mailbox 2', Comment = 'Helpdesk365 - Mailbox 2';
    }
    value(56279; "Helpdesk365 - Mailbox 3")
    {
        Caption = 'Helpdesk365 - Mailbox 3', Comment = 'Helpdesk365 - Mailbox 3';
    }
    value(56280; "Helpdesk365 - Mailbox 4")
    {
        Caption = 'Helpdesk365 - Mailbox 4', Comment = 'Helpdesk365 - Mailbox 4';
    }
    value(56281; "Helpdesk365 - Mailbox 5")
    {
        Caption = 'Helpdesk365 - Mailbox 5', Comment = 'Helpdesk365 - Mailbox 5';
    }
}
