page 56297 "ICI OAuth2 Client Log"
{
    ApplicationArea = All;
    Caption = 'OAuth2 Client Log',
        Comment = 'de-DE=OAuth2 Client Log|en-US=OAuth2 Client Log';
    PageType = List;
    SourceTable = "ICI OAuth2 Client Log";
    UsageCategory = Administration;
    Editable = False;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Entry No.', Comment = 'de-DE=Lfd. Nr.|en-US=Entry No.';
                }
                field("Request at"; Rec."Request at")
                {
                    ApplicationArea = All;
                    ToolTip = 'Request at', Comment = 'de-DE=Request am|en-US=Request at';
                }
                field("Request Type"; Rec."Request Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Request Type', Comment = 'de-DE=Requesttyp|en-US=Request Type';
                }
                field("Request URI"; Rec."Request URI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Request URI', Comment = 'de-DE=Request URI|en-US=Request URI';
                }
                field(Response; Rec.Response)
                {
                    ApplicationArea = All;
                    ToolTip = 'Response', Comment = 'de-DE=Antwort|en-US=Response';
                }
            }
        }
    }

}
