page 56298 "ICI OAuth2 Client Setup"
{

    Caption = 'OAuth2 Client Setup',
        Comment = 'de-DE=Support OAuth2 Client Einrichtung|en-US=Support OAuth2 Client Setup';
    PageType = Card;
    SourceTable = "ICI OAuth2 Client Setup";
    ApplicationArea = ALL;
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General',
                    Comment = 'de-DE=Allgemein|en-US=General';
                field("Is Active"; Rec."Is Active")
                {
                    ApplicationArea = All;
                    //Editable = false;
                    ToolTip = 'Is Active', Comment = 'de-DE=Ist Aktiv|en-US=Is Active';
                }
                field("Client ID"; Rec."Client ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Client ID', Comment = 'de-DE=Client ID|en-US=Client ID';
                }
                field("Client Secret"; Rec."Client Secret")
                {
                    ApplicationArea = All;
                    ToolTip = 'Client Secret', Comment = 'de-DE=Client Passwort|en-US=Client Secret';
                }
                field("Tenant ID"; Rec."Tenant ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Tenant ID', Comment = 'de-DE=Mandanten ID|en-US=Tenant ID';
                }
                field("Log Connection"; Rec."Log Connection")
                {
                    ApplicationArea = All;
                    ToolTip = 'Log Connection', Comment = 'de-DE=Verbindungsprotokollierung|en-US=Log Connection';
                }

            }
            group(Authorization)
            {
                Caption = 'Authorization',
                    Comment = 'de-DE=Authorisierung|en-US=Authorization';

                field("Authorization Scope"; Rec."Authorization Scope")
                {
                    ApplicationArea = All;
                    ToolTip = 'Authorization Scope', Comment = 'de-DE=Authorisierungsscope|en-US=Authorization Scope';
                }
                field("Authentication URI"; Rec."Authentication URI")
                {
                    ToolTip = 'Authentication URI', Comment = 'de-DE=Authentication-URI|en-US=Authentication URI';
                    ApplicationArea = All;
                }

                field("Redirect URI"; Rec."Redirect URI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Redirect URI', Comment = 'de-DE=Weiterleitungs URI|en-US=Redirect URI';
                }
                field("Adminconsent URI"; Rec."Adminconsent URI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Adminconsent URI', Comment = 'de-DE=Administrator URI|en-US=Adminconsent URI';
                }
                field("Authentication Body"; Rec."Authentication Body")
                {
                    ApplicationArea = All;
                    MultiLine = true;
                    ToolTip = 'Authentication Body', Comment = 'de-DE=Authentication-Body|en-US=Authentication Body';
                }
            }
            usercontrol(oa; "ICI OAuth2Redirect")
            {
                ApplicationArea = All;
                trigger ControlReady()
                begin

                end;
            }
        }
    }
    actions
    {
        area(Creation)
        {
            action(AdminAccess)
            {
                Caption = 'Get Admin Access',
                    Comment = 'de-DE=Administratorzugang anfragen|en-US=Get Admin Access';
                ToolTip = 'Get Admin Access', Comment = 'de-DE=Administratorzugang anfragen|en-US=Get Admin Access';

                Image = SendApprovalRequest;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                PromotedOnly = true;
                Enabled = NOT Rec."Is Active";

                trigger OnAction()
                var
                    EnvInfo: Codeunit "Environment Information";
                    URL: Text;
                begin
                    URL := STRSUBSTNO(Rec."Adminconsent URI", Rec."Tenant ID", FORMAT(Rec."Client ID", 0, 4), EnvInfo.GetEnvironmentName(), Rec."Redirect URI");
                    CurrPage.oa.LaunchURLInNewWindow(URL);
                end;

            }
            action(RequestToken)
            {
                Caption = 'Request Token',
                    Comment = 'de-DE=Token anfragen|en-US=Request Token';
                ToolTip = 'Request Token', Comment = 'de-DE=Token anfragen|en-US=Request Token';
                Image = SendApprovalRequest;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                Enabled = Rec."Is Active";
                PromotedOnly = true;

                trigger OnAction()
                var
                    OA2CMgt: Codeunit "ICI OAuth2 Client Mgt.";
                begin
                    OA2CMgt.RequestAccessToken();
                end;
            }
            Action(ShowTokens)
            {
                Caption = 'Show Tokens',
                    Comment = 'de-DE=Tokens anzeigen|en-US=Show Tokens';
                ToolTip = 'Show Tokens', Comment = 'de-DE=Tokens anzeigen|en-US=Show Tokens';
                Image = EncryptionKeys;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;

                RunObject = page "ICI OAuth2 Client Token List";

            }
            Action(ShowLog)
            {
                Caption = 'Show Log',
                    Comment = 'de-DE=Verbindungsprotokoll anzeigen|en-US=Show Log';
                ToolTip = 'Show Log', Comment = 'de-DE=Verbindungsprotokoll anzeigen|en-US=Show Log';
                Image = ExchProdBOMItem;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;

                RunObject = page "ICI OAuth2 Client Log";
            }
            Action(DeleteLog)
            {
                Caption = 'Delete Log',
                    Comment = 'de-DE=Verbindungsprotokoll löschen|en-US=Delete Log';
                ToolTip = 'Delete Log', Comment = 'de-DE=Verbindungsprotokoll löschen|en-US=Delete Log';
                Image = Delete;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;

                trigger OnAction()
                var
                    OA2CLog: Record "ICI OAuth2 Client Log";
                begin
                    OA2CLog.DeleteLog();
                end;
            }

            Action(TestApi)
            {
                Caption = 'Test API',
                    Comment = 'de-DE=Schnittstelle testen|en-US=Test API';
                ToolTip = 'Test API', Comment = 'de-DE=Schnittstelle testen|en-US=Test API';
                Image = TestFile;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;

                trigger OnAction()
                var
                    OA2CMgt: Codeunit "ICI OAuth2 Client Mgt.";
                    //OA2CToken: Record "ICI OAuth2 Client Token";
                    lInStream: InStream;
                    //ostr: OutStream;
                    URI: Text;
                begin
                    URI := Rec."Graph URI" + Rec."API Tester";
                    OA2CMgt.SendRequest(Enum::"Http Request Type"::GET, URI, lInStream, false);
                end;
            }


        }
    }

    trigger OnOpenPage()
    begin
        if not Rec.GET() then begin
            Rec.INIT();
            Rec.InitValues();
            Rec.INSERT();
        end;
    end;

}

