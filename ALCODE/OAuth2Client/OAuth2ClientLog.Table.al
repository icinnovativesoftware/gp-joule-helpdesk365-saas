table 56285 "ICI OAuth2 Client Log"
{
    DataClassification = OrganizationIdentifiableInformation;
    Caption = 'OAuth2 Client Log',
        Comment = 'de-DE=OAuth2 Client Log|en-US=OAuth2 Client Log';

    fields
    {
        field(1; "Entry No."; Integer)
        {
            DataClassification = SystemMetadata;
            Caption = 'Entry No.',
                Comment = 'de-DE=Lfd Nr.|en-US=Entry No.';
            AutoIncrement = true;
        }
        field(10; "Request at"; DateTime)
        {
            DataClassification = SystemMetadata;
            Caption = 'Request at',
                Comment = 'de-DE=Request am|en-US=Request at';
        }
        field(11; "Request Type"; Enum "Http Request Type")
        {
            DataClassification = SystemMetadata;
            Caption = 'Request Type',
                Comment = 'de-DE=Requesttyp|en-US=Request Type';
        }
        field(12; "Response"; Text[2048])
        {
            DataClassification = SystemMetadata;
            Caption = 'Response',
                Comment = 'de-DE=Antwort|en-US=Response';
        }
        field(13; "Request URI"; Text[2048])
        {
            DataClassification = SystemMetadata;
            Caption = 'Request URI',
                Comment = 'de-DE=Request URI|en-US=Request URI';
        }

    }

    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }

    }

    procedure DeleteLog()
    begin
        Reset();
        DeleteAll();
    end;

    procedure InsertLogEntry(Type: Enum "Http Request Type"; RequestUri: Text; Resp: Text)
    begin

        RESET();
        INIT();
        VALIDATE("Request at", CurrentDateTime);
        VALIDATE("Request Type", Type);
        VALIDATE("Request URI", RequestUri);
        VALIDATE(Response, Resp);
        if not INSERT(true) then
            repeat
                "Entry No." += 1;
            UNTIL INSERT(TRUE);
    end;

}
