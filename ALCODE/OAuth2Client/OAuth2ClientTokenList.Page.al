page 56299 "ICI OAuth2 Client Token List"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "ICI OAuth2 Client Token";
    Editable = False;

    layout
    {
        area(Content)
        {
            repeater(General)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'No.', Comment = 'de-DE=Nr.|en-US=No.';
                }
                field("Access Token"; Rec."Access Token")
                {
                    ApplicationArea = All;
                    ToolTip = 'Access Token', Comment = 'de-DE=Zugriffs Token|en-US=Access Token';
                }
                field("Authentication Token"; Rec."Authentication Token")
                {
                    ApplicationArea = All;
                    ToolTip = 'Authentication Token', Comment = 'de-DE=Authorisierungs Token|en-US=Authentication Token';
                }
                field("Expires on"; Rec."Expires on")
                {
                    ApplicationArea = All;
                    ToolTip = 'Expires On', Comment = 'de-DE=Erlischt am/um|en-US=Expires on';
                }
            }
        }
    }

}
