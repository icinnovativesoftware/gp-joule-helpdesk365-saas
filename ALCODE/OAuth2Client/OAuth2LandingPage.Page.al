page 56296 "ICI OAuth2 Landing Page"
{
    Caption = 'Authentication',
        Comment = 'de-DE=Authentifizierung|en-US=Authentication';
    UsageCategory = None;
    layout
    {
        area(Content)
        {
            label(Done)
            {
                ApplicationArea = ALl;
                Caption = 'Authentication done! Please close this page!',
                    Comment = 'de-DE=Authentifizierung abgeschlossen! Bitte schließen Sie diese Seite!|en-US=Authentication done! Please close this page!';
            }
            usercontrol(oa; "ICI OAuth2Redirect")
            {
                ApplicationArea = all;
                trigger RedirectReceived(AdminConsent: Text; state: Text; Tenant: Text)
                var
                    OA2CSetup: Record "ICI OAuth2 Client Setup";
                begin
                    OA2CSetup.Get();
                    if AdminConsent = 'True' then begin
                        OA2CSetup.VALIDATE("Is Active", true);
                        //OA2CSetup.VALIDATE("Tenant ID", Tenant);
                        OA2CSetup.Modify(true);
                    end;
                end;
            }
        }
    }


}
