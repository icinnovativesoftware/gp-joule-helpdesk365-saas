table 56286 "ICI OAuth2 Client Setup"
{
    DataClassification = SystemMetadata;
    Caption = 'OAuth2 Client Setup',
        Comment = 'de-DE=Support OAuth2 Client Einrichtung|en-US=Support OAuth2 Client Setup';

    fields
    {
        field(1; Code; Code[10])
        {
            Caption = 'Code',
                Comment = 'de-DE=Code|en-US=Code';
            DataClassification = SystemMetadata;
        }
        field(10; "Client ID"; GUID)
        {
            DataClassification = SystemMetadata;
            Caption = 'Client ID',
                Comment = 'de-DE=Client ID|en-US=Client ID';

        }
        field(11; "Authorization Scope"; Text[250])
        {
            DataClassification = SystemMetadata;
            Caption = 'Authorization Scope',
                Comment = 'de-DE=Authorisierungsscope|en-US=Authorization Scope';
        }
        field(12; "Client Secret"; Text[250])
        {
            DataClassification = SystemMetadata;
            Caption = 'Client Secret',
                Comment = 'de-DE=Client Passwort|en-US=Client Secret';
            ExtendedDatatype = Masked;
        }
        field(13; "Graph URI"; Text[250])
        {
            DataClassification = SystemMetadata;
            Caption = 'Graph URI',
                Comment = 'de-DE=Graph-URI|en-US=Graph URI';
        }
        field(14; "Authentication URI"; Text[250])
        {
            DataClassification = SystemMetadata;
            Caption = 'Authentication URI',
                Comment = 'de-DE=Authentication-URI|en-US=Authentication URI';
        }
        field(15; "Authentication Body"; Text[2048])
        {
            DataClassification = SystemMetadata;
            Caption = 'Authentication Body',
                Comment = 'de-DE=Authentication-Body|en-US=Authentication Body';
        }
        field(16; "Is Active"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Is Active',
                Comment = 'de-DE=Ist Aktiv|en-US=Is Active';
            //Editable = false;
        }
        field(17; "Adminconsent URI"; Text[1024])
        {
            DataClassification = SystemMetadata;
            Caption = 'Adminconsent URI',
                Comment = 'de-DE=Administrator URI|en-US=Adminconsent URI';
        }
        field(18; "Tenant ID"; Text[1024])
        {
            DataClassification = SystemMetadata;
            Caption = 'Tenant ID',
                Comment = 'de-DE=Mandanten ID|en-US=Tenant ID';
        }
        field(19; "Redirect URI"; Text[1024])
        {
            DataClassification = SystemMetadata;
            Caption = 'Redirect URI',
                Comment = 'de-DE=Weiterleitungs URI|en-US=Redirect URI';
        }
        field(20; "API Tester"; Text[1024])
        {
            DataClassification = SystemMetadata;
            Caption = 'API Tester',
                Comment = 'de-DE=API Tester|en-US=API Tester';
        }
        field(21; "Log Connection"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Log Connection',
                Comment = 'de-DE=Verbindungsprotokollierung|en-US=Log Connection';
        }
        field(22; "File Download Root Folder"; Text[200])
        {
            DataClassification = SystemMetadata;
            Caption = 'File Download Root Folder',
                Comment = 'de-DE=Datei Stammverzeichnis|en-US=File Download Root Folder';
        }


    }

    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

    procedure InitValues()
    var
    //TenantInformation: Codeunit "Tenant Information";
    begin
        VALIDATE("Client ID", 'f85f7c96-1f36-4614-8803-d659bfb9284d');
        VALIDATE("Graph URI", 'https://graph.microsoft.com/v1.0/');
        VALIDATE("Authentication URI", 'https://login.microsoftonline.com/%1/oauth2/v2.0/token');
        VALIDATE("Authorization Scope", 'https://graph.microsoft.com/.default');
        VALIDATE("Client Secret", '-CAg5h~367h02VG~9GG.bWfPYw-ZHRY~.s');
        VALIDATE("Redirect URI", 'https://licmgt.ic-innovative.de/HelpDesk365Connector.php');
        VALIDATE("Authentication Body", 'client_id=%1&scope=%2&client_secret=%3&grant_type=client_credentials');
        Validate("Adminconsent URI", 'https://login.microsoftonline.com/%1/adminconsent?client_id=%2&state=%3&redirect_uri=%4');
        //Validate("Tenant ID", Database.TenantId());
    end;
}
