codeunit 56284 "ICI OAuth2 Client Mgt."
{
    trigger OnRun()
    begin

    end;

    procedure RequestAccessToken()
    var
        ICIOAuth2ClientToken: Record "ICI OAuth2 Client Token";
    begin
        SendAuthenticationRequest(ICIOAuth2ClientToken);
    end;

    local procedure SendAuthenticationRequest(var ICIOAuth2ClientToken: Record "ICI OAuth2 Client Token")
    var
        ICIOAuth2ClientSetup: Record "ICI OAuth2 Client Setup";
        //TenantInformation: Codeunit "Tenant Information";
        ICIHttpClient: HttpClient;
        ICIHttpContent: HttpContent;
        //ICIHttpRequestMessage: HttpRequestMessage;
        ICIHttpHeaders: HttpHeaders;
        ICIHttpResponseMessage: HttpResponseMessage;
        URI: Text;
        Content: Text;
        ResponseAsText: Text;
        //URITEST: Text;
        jObj: JsonObject;
        //jVal: JsonValue;
        //jTok: JsonToken;
        //jArr: JsonArray;
        ExpiresIn: Integer;
    begin
        ICIOAuth2ClientSetup.GET();

        URI := STRSUBSTNO(ICIOAuth2ClientSetup."Authentication URI", ICIOAuth2ClientSetup."Tenant ID");

        Content := STRSUBSTNO(ICIOAuth2ClientSetup."Authentication Body", ICIOAuth2ClientSetup."Client ID", ICIOAuth2ClientSetup."Authorization Scope", ICIOAuth2ClientSetup."Client Secret");

        ICIHttpContent.WriteFrom(Content);
        ICIHttpContent.GetHeaders(ICIHttpHeaders);
        ICIHttpHeaders.Remove('Content-Type');

        ICIHttpHeaders.Add('Content-Type', 'application/x-www-form-urlencoded');
        ICIHttpClient.SetBaseAddress(URI);
        ICIHttpClient.DefaultRequestHeaders.Add('Host', 'login.microsoftonline.com');
        ICIHttpClient.Post(URI, ICIHttpContent, ICIHttpResponseMessage);

        if ICIOAuth2ClientSetup."Log Connection" then
            ICIOAuth2ClientLog.InsertLogEntry(Enum::"Http Request Type"::POST, URI, ICIHttpResponseMessage.ReasonPhrase);

        ICIHttpContent := ICIHttpResponseMessage.Content;
        ICIHttpContent.ReadAs(ResponseAsText);

        if not jObj.ReadFrom(ResponseAsText) then
            Error('Invalid Response, expected an JSON array as root object');

        if GetJsonToken(jObj, 'token_type').AsValue().AsText() <> 'Bearer' then
            Error('Only Bearer Token allowed');

        if GetJsonToken(jObj, 'expires_in').AsValue().AsText() = '' then
            Error('Expiration Time is wrong');

        if GetJsonToken(jObj, 'ext_expires_in').AsValue().AsText() = '' then
            Error('Expiration Time is wrong');

        if GetJsonToken(jObj, 'access_token').AsValue().AsText() = '' then
            Error('Access Token is empty');

        EVALUATE(ExpiresIn, GetJsonToken(jObj, 'expires_in').AsValue().AsText());

        ICIOAuth2ClientToken.InsertNewToken(GetJsonToken(jObj, 'access_token').AsValue().AsText(), '', ExpiresIn);

    end;

    procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text) jToken: JsonToken;
    begin
        if not JsonObject.GET(TokenKey, jToken) then
            ERROR('Could not find a token with key %1', TokenKey);
    end;

    procedure SendRequest(Type: Enum "Http Request Type"; URI: Text; var ContentInStream: InStream; Download: Boolean)
    var
        ICIOAuth2ClientSetup: Record "ICI OAuth2 Client Setup";
        ICIOAuth2ClientToken: Record "ICI OAuth2 Client Token";
        ICIHttpClient: HttpClient;
        ICIHttpHeaders: HttpHeaders;
        ICIHttpContent: HttpContent;
        ICIHttpResponseMessage: HttpResponseMessage;
        ResponseAsText: Text;
        //ToFile: Text;
        jObj: JsonObject;
    begin
        ICIOAuth2ClientSetup.GET();

        ICIOAuth2ClientToken.SETFILTER("Expires on", '>%1', CurrentDateTime);
        IF Not ICIOAuth2ClientToken.FindLast() then begin
            RequestAccessToken();
            ICIOAuth2ClientToken.FindLast()
        end;
        ICIOAuth2ClientSetup.Get();
        ICIHttpContent.GetHeaders(ICIHttpHeaders);

        ICIHttpClient.SetBaseAddress(URI);
        ICIHttpClient.DefaultRequestHeaders.Add('Authorization', 'Bearer ' + ICIOAuth2ClientToken."Access Token");
        ICIHttpClient.DefaultRequestHeaders.Add('Host', 'graph.microsoft.com');

        case Type of
            Enum::"Http Request Type"::POST:
                ICIHttpClient.Post(URI, ICIHttpContent, ICIHttpResponseMessage);

            Enum::"Http Request Type"::Get:
                if Download THEN begin //wird eigentlich nur für "verschlüsselte Dateien" benötigt (z.B. Docx)
                    ICIHttpClient.Get(URI, ICIHttpResponseMessage);
                    ICIHttpContent := ICIHttpResponseMessage.Content;
                    ICIHttpContent.ReadAs(ResponseAsText);
                    jObj.ReadFrom(ResponseAsText);

                    DownloadFileFromSharepoint(URI, ICIOAuth2ClientToken, GetJsonToken(jObj, 'name').AsValue().AsText());
                end else begin
                    ICIHttpClient.Get(URI, ICIHttpResponseMessage);
                    ICIHttpContent := ICIHttpResponseMessage.Content;
                    ICIHttpContent.ReadAs(ContentInStream);
                end;
            Enum::"Http Request Type"::PUT:
                begin
                    ICIHttpContent.WriteFrom(ContentInStream);
                    ICIHttpClient.Put(URI, ICIHttpContent, ICIHttpResponseMessage);
                end;
        end;
        if ICIOAuth2ClientSetup."Log Connection" then
            ICIOAuth2ClientLog.InsertLogEntry(Type, URI, ICIHttpResponseMessage.ReasonPhrase);

    end;

    procedure DownloadFileFromSharepoint(PathToFile: Text; var ICIOAuth2ClientToken: Record "ICI OAuth2 Client Token"; FileName: Text)
    var
        //ICIOAuth2ClientSetup: Record "ICI OAuth2 Client Setup";
        ICIHttpClient: HttpClient;
        ICIHttpHeaders: HttpHeaders;
        ICIHttpContent: HttpContent;
        ICIHttpResponseMessage: HttpResponseMessage;
        lInStream: InStream;
    //ToFile: Text;
    begin
        ICIHttpContent.GetHeaders(ICIHttpHeaders);

        ICIHttpClient.SetBaseAddress(PathToFile);
        ICIHttpClient.DefaultRequestHeaders.Add('Authorization', 'Bearer ' + ICIOAuth2ClientToken."Access Token");
        ICIHttpClient.DefaultRequestHeaders.Add('Host', 'graph.microsoft.com');

        ICIHttpClient.Get(PathToFile, ICIHttpResponseMessage);

        ICIHttpContent := ICIHttpResponseMessage.Content;

        ICIHttpContent.ReadAs(lInStream);
        DOWNLOADFROMSTREAM(lInStream, '', '%DOWNLOADS%', '', FileName);
    end;

    var
        ICIOAuth2ClientLog: Record "ICI OAuth2 Client Log";
}
