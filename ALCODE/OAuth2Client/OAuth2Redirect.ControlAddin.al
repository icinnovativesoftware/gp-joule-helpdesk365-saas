controladdin "ICI OAuth2Redirect"
{
    MinimumHeight = 1;
    MaximumHeight = 1;

    MinimumWidth = 1;
    MaximumWidth = 1;
    StartupScript = 'ALCODE/OAuth2Client/OAuth2ClientStartup.js';
    Scripts = 'ALCODE/OAuth2Client/OAuth2Client.js';

    event RedirectReceived(AdminConsent: Text; State: Text; Tenant: Text);
    event ControlReady();
    event TimerTic();
    procedure LaunchURLInNewWindow(URL: Text);
    procedure StartTimer();
    procedure CloseCurrentWindow();
    procedure CloseWindow();
}
