table 56287 "ICI OAuth2 Client Token"
{
    DataClassification = SystemMetadata;
    Caption = 'OAuth2 Client Token',
        Comment = 'de-DE=OAuth2 Client Token|en-US=OAuth2 Client Token';
    LookupPageId = "ICI OAuth2 Client Token List";
    DrillDownPageId = "ICI OAuth2 Client Token List";

    fields
    {
        field(1; "No."; Integer)
        {
            DataClassification = SystemMetadata;
            Caption = 'No.',
                Comment = 'de-DE=Nr.|en-US=No.';
            AutoIncrement = true;
        }
        field(10; "Access Token"; Text[2048])
        {
            DataClassification = SystemMetadata;
            Caption = 'Access Token',
                Comment = 'de-DE=Zugriffs Token|en-US=Access Token';
        }
        field(11; "Authentication Token"; Text[2048])
        {
            DataClassification = SystemMetadata;
            Caption = 'Authentication Token',
                Comment = 'de-DE=Authorisierungs Token|en-US=Authentication Token';
        }
        field(12; "Expires on"; DateTime)
        {
            DataClassification = SystemMetadata;
            Caption = 'Expires On',
                Comment = 'de-DE=Erlischt am/um|en-US=Expires on';
        }
    }

    keys
    {
        key(PK; "No.")
        {
            Clustered = true;
        }
    }

    procedure InsertNewToken(AccessToken: Text; AuthenticationToken: Text; ExpiresAt: Integer)
    var
        DurationVar: Duration;
    begin
        INIT();
        VALIDATE("Access Token", AccessToken);
        VALIDATE("Authentication Token", AuthenticationToken);

        DurationVar := ExpiresAt * 1000; //ExpiresAt needed in Milliseconds not Seconds

        VALIDATE("Expires on", CurrentDateTime() + DurationVar);

        INSERT(True);
    end;
}
