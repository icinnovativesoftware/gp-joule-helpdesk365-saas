Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("ControlReady",[]);

var urlParams = new URLSearchParams(window.location.search);

if (urlParams.has('admin_consent')){
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod(
        'RedirectReceived',
        [urlParams.get('admin_consent'), 'Sandbox', urlParams.get('env')]
    );
}
