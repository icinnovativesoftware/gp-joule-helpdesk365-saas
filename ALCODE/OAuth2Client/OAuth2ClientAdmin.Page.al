page 56295 "ICI OAuth2 Client Admin"
{
    Caption = 'Admin Consent',
        Comment = 'de-DE=Administratorzugriff|en-US=Admin Consent';
    UsageCategory = None;
    layout
    {
        area(Content)
        {
            label(Warning)
            {
                Caption = 'Please allow popups for this page!',
                    Comment = 'de-DE=Für diese Seite Popups erlauben!|en-US=Please allow popups for this page!';
                ApplicationArea = All;
            }

        }
    }
}
