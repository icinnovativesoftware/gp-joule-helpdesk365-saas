page 56371 "ICI Tick. Cat. Translations"
{

    Caption = 'Tick. Cat. Translations', Comment = 'de-DE=Kategorieübersetzungen';
    PageType = List;
    SourceTable = "ICI Tick. Category Translation";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Ticket Category Code"; Rec."Ticket Category Code")
                {
                    ToolTip = 'Specifies the value of the Ticket Category Code field.', Comment = 'de-DE=Ticketkategoriecode';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Language Code"; Rec."Language Code")
                {
                    ToolTip = 'Specifies the value of the Language Code field.', Comment = 'de-DE=Sprachcode';
                    ApplicationArea = All;
                }

                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field.', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }

            }
        }
    }
    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        IF Rec.GetFilter("Ticket Category Code") <> '' THEN
            Rec."Ticket Category Code" := COPYSTR(Rec.GetFilter("Ticket Category Code"), 1, 20);
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        Rec.TestField("Language Code");
    end;
}
