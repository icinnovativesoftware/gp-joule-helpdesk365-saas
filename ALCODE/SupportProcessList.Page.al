page 56279 "ICI Support Process List"
{

    ApplicationArea = All;
    Caption = 'ICI Support Process List', Comment = 'de-DE=Support Ticket Prozesse';
    PageType = List;
    SourceTable = "ICI Support Process";
    UsageCategory = Lists;
    CardPageId = "ICI Support Process Card";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("No. of Process Lines"; Rec."No. of Process Lines")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of Process Lines', Comment = 'de-DE=Anzahl der Prozess Zeilen';
                }
            }
        }
    }
}
