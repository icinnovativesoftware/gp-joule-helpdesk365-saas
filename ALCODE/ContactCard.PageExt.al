pageextension 56277 "ICI Contact Card" extends "Contact Card"
{
    layout
    {
        addlast(content)
        {
            group(ICIHelpdeskGroup)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                field("ICI Support Active"; Rec."ICI Support Active")
                {
                    ApplicationArea = All;
                    Importance = Promoted;
                    ToolTip = 'Support Active', Comment = 'de-DE=Support Aktiv';
                }
                group(ICIPortal)
                {
                    Visible = PortalIntegration;
                    Caption = 'Portal', Comment = 'de-DE=Kundenportal';
                    field("ICI Login"; Rec."ICI Login")
                    {
                        ApplicationArea = All;
                        Importance = Promoted;
                        ToolTip = 'Support Login', Comment = 'de-DE=Login';
                    }
                    field("ICI Portal Cue Set Code"; Rec."ICI Portal Cue Set Code")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Portal Cue Set Code', Comment = 'de-DE=Kundenportalkacheln';
                    }
                    field("Login Failures"; Rec."Login Failures")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Login Failures', Comment = 'de-DE=Zeigt die Anzahl der Fehlgeschlagenen Anmeldeversuche an. Wird beim Versenden von neuen Zugangsdaten zurückgesetzt. Bei dem Erreichen des Maximallimits wird der Zugang gesperrt';
                    }

                    group(DocumentCenterSales)
                    {
                        Caption = 'Documents - Sales', Comment = 'de-DE=Belegarchiv-Verkauf';
                        Visible = ShowSalesDocuments;

                        field("ICI Sales Quote"; Rec."ICI Sales Quote")
                        {
                            Visible = ShowSalesQuote;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Sales Order"; Rec."ICI Sales Order")
                        {
                            Visible = ShowSalesOrder;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Sales Invoice"; Rec."ICI Sales Invoice")
                        {
                            Visible = ShowSalesInvoice;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Sales Credit Memo"; Rec."ICI Sales Credit Memo")
                        {
                            Visible = ShowSalesCreditMemo;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Sales Blanket Order"; Rec."ICI Sales Blanket Order")
                        {
                            Visible = ShowSalesBlanketOrder;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Sales Return Order"; Rec."ICI Sales Return Order")
                        {
                            Visible = ShowSalesReturnOrder;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Posted Sales Shipment"; Rec."ICI Posted Sales Shipment")
                        {
                            Visible = ShowPostedSalesShipment;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Posted Sales Invoice"; Rec."ICI Posted Sales Invoice")
                        {
                            Visible = ShowPostedSalesInvoice;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        field("ICI Posted Sales Cr.Memo"; Rec."ICI Posted Sales Cr.Memo")
                        {
                            Visible = ShowPostedSalesCrMemo;
                            ApplicationArea = All;
                            ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                        }
                        // SERVICE
                        group(DocumentCenterService)
                        {
                            Caption = 'Documents - Service', Comment = 'de-DE=Belegarchiv-Service';
                            Visible = ShowServiceDocuments;
                            field("ICI Service Quote"; Rec."ICI Service Quote")
                            {
                                Visible = ShowServiceQuote;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Service Order"; Rec."ICI Service Order")
                            {
                                Visible = ShowServiceOrder;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Service Invoice"; Rec."ICI Service Invoice")
                            {
                                Visible = ShowServiceInvoice;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Service Credit Memo"; Rec."ICI Service Credit Memo")
                            {
                                Visible = ShowServiceCreditMemo;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Posted Service Shipment"; Rec."ICI Posted Service Shipment")
                            {
                                Visible = ShowPostedServiceShipment;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Posted Service Invoice"; Rec."ICI Posted Service Invoice")
                            {
                                Visible = ShowPostedServiceInvoice;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Posted Service Cr.Memo"; Rec."ICI Posted Service Cr.Memo")
                            {
                                Visible = ShowPostedServiceCrMemo;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Service Contract"; Rec."ICI Service Contract")
                            {
                                Visible = ShowServiceContract;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                            field("ICI Service Contract Quote"; Rec."ICI Service Contract Quote")
                            {
                                Visible = ShowServiceContractQuote;
                                Importance = Additional;
                                ApplicationArea = All;
                                ToolTip = 'Specify if the specified Document Type is shown in the Portal', Comment = 'de-DE=Gibt an, ob diesem Kontakt die gewählte Belegart Kundenportal angezeigt wird.';
                            }
                        }
                    }
                }
                group(ICIMailrobot)
                {
                    Caption = 'Mailrobot', Comment = 'de-DE=Mailrobot';
                    field("ICI E-Mail TLD"; Rec."ICI E-Mail TLD")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'ICI E-Mail TLD', Comment = 'de-DE=E-Mail Top Level Domain';
                    }
                    field("Mailrobot Source Mailbox Code"; Rec."Mailrobot Source Mailbox Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Mailrobot Source Mailbox Code', Comment = 'de-DE=Ursprüngliches Postfach';
                    }
                    field("ICI Source Mailrobot Log"; Rec."ICI Source Mailrobot Log")
                    {
                        ApplicationArea = All;
                        ToolTip = 'ICI Source Mailrobot Log', Comment = 'de-DE=Erstellt aus Nachricht';
                    }
                }
            }
        }
        addfirst(factboxes)
        {
            part("ICI Contact Ticket Factbox"; "ICI Contact Ticket Factbox")
            {
                ApplicationArea = All;
                Visible = ShowContactFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                action(CreateTicket)
                {
                    Caption = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ToolTip = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ApplicationArea = All;
                    Image = New;
                    trigger OnAction()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                    begin
                        ICISupportTicket.Init();
                        ICISupportTicket.Insert(true);
                        IF Rec.Type = Rec.Type::Company THEN
                            ICISupportTicket.Validate("Company Contact No.", Rec."Company No.")
                        ELSE
                            ICISupportTicket.Validate("Current Contact No.", Rec."No.");
                        ICISupportTicket.FillMissingContactOrCustomerInformation();
                        ICISupportTicket.Modify(true);

                        PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket);
                    end;
                }

                action(ICISendLogin)
                {
                    Caption = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                    ToolTip = 'Send Login', Comment = 'de-DE=Zugangsdaten versenden';
                    ApplicationArea = All;
                    Image = SendMail;
                    Visible = PortalIntegration;

                    trigger OnAction();
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                        LoginSentTxt: Label 'Logindata has been sent', Comment = 'de-DE=Zugangsdaten erfolgreich versendet.';
                        LoginNotSentTxt: Label 'Logindata has not been sent', Comment = 'de-DE=Zugangsdaten konnten nicht versendet werden.';
                    begin
                        CurrPage.SaveRecord();
                        IF ICISupportMailMgt.SendContactLogin(Rec."No.", Rec.ICIGeneratePassword()) THEN
                            MESSAGE(LoginSentTxt)
                        ELSE
                            MESSAGE(LoginNotSentTxt);
                        CurrPage.Update(false);
                    end;
                }
                action(ICIOpenPortal)
                {
                    Caption = 'Open Portal', Comment = 'de-DE=Zugang öffnen';
                    ToolTip = 'Open Portal', Comment = 'de-DE=Zugang öffnen';
                    ApplicationArea = All;
                    Image = LaunchWeb;
                    Visible = PortalIntegration;

                    trigger OnAction();
                    var
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        Hyperlink(ICISupportMailMgt.GenerateContactLink(Rec."No."));
                    end;
                }
                action(ICISentMails)
                {
                    Caption = 'Sent Mails', Comment = 'de-DE=Gesendete E-Mails';
                    ToolTip = 'Sent Mails', Comment = 'de-DE=Öffnet die gesendeten E-Mails zu diesem Kontakt';
                    ApplicationArea = All;
                    Image = Log;
                    RunObject = Page "Sent Emails";
                    trigger OnAction()
                    var
                        EMail: Codeunit Email;
                    begin
                        EMail.OpenSentEmails(Database::"Contact", Rec.SystemId);
                    end;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin

        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() THEN begin
                PortalIntegration := ICISupportSetup."Portal Integration";
                ShowContactFactbox := true;
            end;
        IF ICIWebarchiveSetup.ReadPermission Then
            IF ICIWebarchiveSetup.GET() THEN begin
                ShowSalesQuote := ICIWebarchiveSetup."Webarchiv Sales Quote";
                ShowSalesOrder := ICIWebarchiveSetup."Webarchiv Sales Order";
                ShowSalesInvoice := ICIWebarchiveSetup."Webarchiv Sales Invoice";
                ShowSalesCreditMemo := ICIWebarchiveSetup."Webarchiv Sales Credit Memo";
                ShowSalesBlanketOrder := ICIWebarchiveSetup."Webarchiv Sales Blanket Order";
                ShowSalesReturnOrder := ICIWebarchiveSetup."Webarchiv Sales Return Order";
                ShowPostedSalesShipment := ICIWebarchiveSetup."Webar. Posted Sales Shipment";
                ShowPostedSalesInvoice := ICIWebarchiveSetup."Webarchiv Posted Sales Invoice";
                ShowPostedSalesCrMemo := ICIWebarchiveSetup."Webarchiv Posted Sales Cr.Memo";

                ShowSalesDocuments := ShowSalesQuote OR ShowSalesOrder OR ShowSalesInvoice OR ShowSalesCreditMemo OR ShowSalesBlanketOrder OR ShowSalesReturnOrder OR ShowPostedSalesShipment OR ShowPostedSalesInvoice OR ShowPostedSalesCrMemo;

                ShowServiceQuote := ICIWebarchiveSetup."Webarchiv Service Quote";
                ShowServiceOrder := ICIWebarchiveSetup."Webarchiv Service Order";
                ShowServiceInvoice := ICIWebarchiveSetup."Webarchiv Service Invoice";
                ShowServiceCreditMemo := ICIWebarchiveSetup."Webarchiv Service Credit Memo";
                ShowPostedServiceShipment := ICIWebarchiveSetup."Webar. Posted Service Shipment";
                ShowPostedServiceInvoice := ICIWebarchiveSetup."Webar. Posted Service Invoice";
                ShowPostedServiceCrMemo := ICIWebarchiveSetup."Webar. Posted Service Cr.Memo";
                ShowServiceContract := ICIWebarchiveSetup."Webarchiv Service Contract";
                ShowServiceContractQuote := ICIWebarchiveSetup."Webar. Service Contract Quote";

                ShowServiceDocuments := ShowServiceQuote OR ShowServiceOrder OR ShowServiceInvoice OR ShowServiceCreditMemo OR ShowPostedServiceShipment OR ShowPostedServiceInvoice OR ShowPostedServiceCrMemo OR ShowServiceContract OR ShowServiceContractQuote;

            end;
    end;

    var
        ShowSalesDocuments: Boolean;
        ShowServiceDocuments: Boolean;

        PortalIntegration: Boolean;
        ShowContactFactbox: Boolean;
        ShowSalesQuote: Boolean;
        ShowSalesOrder: Boolean;
        ShowSalesInvoice: Boolean;
        ShowSalesCreditMemo: Boolean;
        ShowSalesBlanketOrder: Boolean;
        ShowSalesReturnOrder: Boolean;
        ShowPostedSalesShipment: Boolean;
        ShowPostedSalesInvoice: Boolean;
        ShowPostedSalesCrMemo: Boolean;
        ShowServiceQuote: Boolean;
        ShowServiceOrder: Boolean;
        ShowServiceInvoice: Boolean;
        ShowServiceCreditMemo: Boolean;
        ShowPostedServiceShipment: Boolean;
        ShowPostedServiceInvoice: Boolean;
        ShowPostedServiceCrMemo: Boolean;
        ShowServiceContract: Boolean;
        ShowServiceContractQuote: Boolean;
}
