page 56312 "ICI Support Activities"
{

    Caption = 'ICI Support Activities', Comment = 'de-DE=Helpdesk365';
    PageType = CardPart;
    SourceTable = "ICI Support Cue";

    layout
    {
        area(content)
        {
            cuegroup(TicketActions)
            {
                Caption = 'Actions', Comment = 'de-DE=Aktionen';
                actions
                {
                    action(TicketFromDocumentsearch)
                    {
                        ApplicationArea = All;
                        Caption = 'Create Ticket from Document No.', Comment = 'de-DE=Ticket (m. Belegsuche)';
                        ToolTip = 'Create Ticket from Document No.', Comment = 'de-DE=Neues Ticket mittels Belegsuche';
                        Image = TileNew;

                        trigger OnAction()
                        var
                            ICITicketDocSearchDialog: Page "ICI Ticket Doc. Search Dialog";
                        begin
                            CurrPage.SaveRecord();
                            IF ICITicketDocSearchDialog.RunModal() = Action::OK THEN
                                CurrPage.Update(False);
                        end;
                    }
                    action(TicketFromServiceitemsearch)
                    {
                        ApplicationArea = All;
                        Caption = 'Create Ticket from Serial No.', Comment = 'de-DE=Ticket (Serv.-Art.-Suche)';
                        ToolTip = 'Create Ticket from Serial No.', Comment = 'de-DE=Neues Ticket mittels Seriennummernsuche';
                        Image = TileNew;

                        trigger OnAction()
                        var
                            ICITicketSItemSearchDialog: Page "ICI Ticket SItem Search Dialog";
                        begin
                            CurrPage.SaveRecord();
                            IF ICITicketSItemSearchDialog.RunModal() = Action::OK THEN
                                CurrPage.Update(False);
                        end;
                    }
                    action(TicketFromAddresssearch)
                    {
                        ApplicationArea = All;
                        Caption = 'Create Ticket from Address', Comment = 'de-DE=Ticket (m. Adresssuche)';
                        ToolTip = 'Create Ticket from Address', Comment = 'de-DE=Neues Ticket mittels Adresssuche';
                        Image = TileNew;

                        trigger OnAction()
                        var
                            ICITicketAddrSearchDialog: Page "ICI Ticket Addr. Search Dialog";
                        begin
                            CurrPage.SaveRecord();
                            IF ICITicketAddrSearchDialog.RunModal() = Action::OK THEN
                                CurrPage.Update(False);
                        end;
                    }
                    action("New Ticket")
                    {

                        ApplicationArea = Basic, Suite;
                        Caption = 'New Ticket', Comment = 'de-DE=Ticket';
                        RunObject = Page "ICI Support Ticket Card";
                        RunPageMode = Create;
                        Image = TileNew;
                        ToolTip = 'Create a new Ticket', Comment = 'de-DE=Neues Ticket erstellen';
                    }

                    action(OpenNextOrderedTicket)
                    {
                        ApplicationArea = Basic, Suite;
                        Visible = ShowTicketBoard;
                        Caption = 'My next Ticket', Comment = 'de-DE=Mein nächstes Ticket';
                        ToolTip = 'Open next Ticket with Ticketboard Priority', Comment = 'de-DE=Nächstes Ticket nach der Priorität aus dem Ticketboard öffnen';
                        Image = TileReport;

                        trigger OnAction()
                        var
                            ICISupportUser: Record "ICI Support User";
                            ICISupportTicket: Record "ICI Support Ticket";
                        begin
                            IF NOT ICISupportUser.GetCurrUser(ICISupportUser) then
                                exit;
                            IF NOT ICISupportUser."Ticket Board Active" then begin
                                ICISupportUser.VALIDATE("Ticket Board Active", true);
                                ICISupportUser.MODIFY();
                            end;

                            ICISupportTicket.SetCurrentKey("Ticket Board Order");
                            ICISupportTicket.SetRange("Support User ID", ICISupportUser."User ID");
                            ICISupportTicket.SetFilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
                            IF NOT ICISupportTicket.FindFirst() THEN
                                exit;

                            PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket); // Run page on rec with smalles "ticket board order"
                        end;
                    }
                }
            }
            cuegroup(LastChangedBy)
            {
                Caption = 'Last Changed By', Comment = 'de-DE=Letzte Aktivität';
                field("Tickets - Last ch. by Contact"; Rec."Tickets - Last ch. by Contact")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'Tickets - Last ch. by Contact', Comment = 'de-DE=Kunde';
                    ToolTip = 'Tickets - Last ch. by Contact', Comment = 'de-DE=Letzte Änderung wurde von Kunden/Kontakten vorgenommen';
                }
                field("Tickets - Last ch. by User"; Rec."Tickets - Last ch. by User")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'Tickets - Last ch. by User', Comment = 'de-DE=Benutzer';
                    ToolTip = 'Tickets - Last ch. by User', Comment = 'de-DE=Letzte Änderung wurde von Mitarbeitern/Benutzern vorgenommen';
                }
            }

            cuegroup("My Tickets")
            {
                Caption = 'My Tickets', Comment = 'de-DE=Meine Tickets';
                field("No. of My Tickets"; Rec."My Tickets - All")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'No. of My Tickets', Comment = 'de-DE=Meine Tickets, die nicht geschlossen sind';
                    Caption = 'No. of My Tickets', Comment = 'de-DE=Alle Tickets';
                }
                field("My Tickets - Preparation"; Rec."My Tickets - Preparation")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Preparation', Comment = 'de-DE=In Vorbereitung';
                    ToolTip = 'My Tickets - Preparation', Comment = 'de-DE=In Vorbereitung';
                }
                field("My Tickets - Open"; Rec."My Tickets - Open")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Open', Comment = 'de-DE=Offen';
                    ToolTip = 'My Tickets - Open', Comment = 'de-DE=Offen';
                }
                field("My Tickets - Processing"; Rec."My Tickets - Processing")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Processing', Comment = 'de-DE=In Bearbeitung';
                    ToolTip = 'My Tickets - Processing', Comment = 'de-DE=In Bearbeitung';
                }
                field("My Tickets - Waiting"; Rec."My Tickets - Waiting")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Dep. Tickets - Waiting', Comment = 'de-DE=In Wartestellung';
                    ToolTip = 'My Dep. Tickets - Waiting', Comment = 'de-DE=In Wartestellung';
                }
                field("My Tickets - Closed"; Rec."My Tickets - Closed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Visible = false;
                    Caption = 'My Tickets - Closed', Comment = 'de-DE=Geschlossen';
                    ToolTip = 'My Tickets - Closed', Comment = 'de-DE=Geschlossen';
                }
            }
            cuegroup("My Dept. Tickets")
            {
                Visible = ShowDepartments;
                Caption = 'My Department Tickets', Comment = 'de-DE=Meine Abteilung';
                field("No. of Department Tickets"; Rec."My Dep. Tickets - All")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'No. of Department Tickets', Comment = 'de-DE=Tickets meiner Abteilung, die nicht geschlossen sind';
                    Caption = 'No. of Department Tickets', Comment = 'de-DE=Alle Tickets';
                }
                field("My Dep. Tickets - Preparation"; Rec."My Dep. Tickets - Preparation")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Preparation', Comment = 'de-DE=In Vorbereitung';
                    ToolTip = 'My Tickets - Preparation', Comment = 'de-DE=In Vorbereitung';
                }
                field("My Dep. Tickets - Open"; Rec."My Dep. Tickets - Open")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Open', Comment = 'de-DE=Offen';
                    ToolTip = 'My Tickets - Open', Comment = 'de-DE=Offen';
                }
                field("My Dep. Tickets - Processing"; Rec."My Dep. Tickets - Processing")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Tickets - Processing', Comment = 'de-DE=In Bearbeitung';
                    ToolTip = 'My Tickets - Processing', Comment = 'de-DE=In Bearbeitung';
                }
                field("My Dep. Tickets - Waiting"; Rec."My Dep. Tickets - Waiting")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'My Dep. Tickets - Waiting', Comment = 'de-DE=In Wartestellung';
                    ToolTip = 'My Dep. Tickets - Waiting', Comment = 'de-DE=In Wartestellung';
                }
                field("My Dep. Tickets - Closed"; Rec."My Dep. Tickets - Closed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Visible = false;
                    Caption = 'My Tickets - Closed', Comment = 'de-DE=Geschlossen';
                    ToolTip = 'My Tickets - Closed', Comment = 'de-DE=Geschlossen';
                }
            }
            cuegroup("All Tickets")
            {
                Caption = 'All Tickets', Comment = 'de-DE=Alle Tickets';
                field("No. of Tickets"; Rec."Tickets - All")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'No. of Tickets', Comment = 'de-DE=Alle Tickets, die nicht geschlossen sind';
                    Caption = 'No. of Tickets', Comment = 'de-DE=Alle Tickets';
                }


                field("Tickets -  Escalated"; NoOfTicketsEscalated)
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    Caption = 'Tickets -  Escalated', Comment = 'de-DE=Eskalierte Tickets';
                    ToolTip = 'Tickets -  Escalated', Comment = 'de-DE=Eskalierte Tickets';
                    StyleExpr = EscalatedStyleExpr;

                    trigger OnDrillDown()
                    begin

                        PAGE.RUN(PAGE::"ICI Support Ticket List", EscalatedICISupportTicket);
                    end;
                }
            }
            cuegroup("Mailrobot")
            {
                Caption = 'Mailrobot', Comment = 'de-DE=Mailrobot';
                Visible = ShowMailobot;
                field("Mails - Unprocessed"; Rec."Mails - Unprocessed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'E-Mails - Unprocessed', Comment = 'de-DE=E-Mails - unverarbeitet';
                    Caption = 'E-Mails - Unprocessed', Comment = 'de-DE=E-Mails - unverarbeitet';
                }
                field(NoOfSpamMailRobotInbox; NoOfSpamMailRobotInbox)
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    Caption = 'E-Mails - Spam', Comment = 'de-DE=Davon Spam';
                    ToolTip = 'E-Mails - Spam', Comment = 'de-DE=Davon Spam';
                    // StyleExpr = EscalatedStyleExpr;
                    trigger OnDrillDown()
                    begin
                        PAGE.RUN(PAGE::"ICI Mailrobot Inbox List", SpamICIMailrobotLog);
                    end;
                }
                field("Mailrobot - Errors"; Rec."Mailrobot - Errors")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'E-Mails - Errors', Comment = 'de-DE=E-Mails - Fehler bei verarbeitung';
                    Caption = 'E-Mails - Errors', Comment = 'de-DE=E-Mails - Fehler';
                }
                group(UnprocessedMails)
                {
                    Caption = 'Unprocessed', Comment = 'de-DE=Unverarbeitet';
                }
                field("Mailbox 1 - Unprocessed"; Rec."Mailbox 1 - Unprocessed")
                {
                    ApplicationArea = All;
                    // DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'Mailbox - Unprocessed', Comment = 'de-DE=E-Mails Postfach - unverarbeitet';
                    CaptionClass = Mailbox1Unprocessed;
                    Visible = Mailbox1Visible;
                }
                field("Mailbox 2 - Unprocessed"; Rec."Mailbox 2 - Unprocessed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'Mailbox - Unprocessed', Comment = 'de-DE=E-Mails Postfach - unverarbeitet';
                    CaptionClass = Mailbox2Unprocessed;
                    Visible = Mailbox2Visible;
                }
                field("Mailbox 3 - Unprocessed"; Rec."Mailbox 3 - Unprocessed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'Mailbox - Unprocessed', Comment = 'de-DE=E-Mails Postfach - unverarbeitet';
                    CaptionClass = Mailbox3Unprocessed;
                    Visible = Mailbox3Visible;
                }
                field("Mailbox 4 - Unprocessed"; Rec."Mailbox 4 - Unprocessed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'Mailbox - Unprocessed', Comment = 'de-DE=E-Mails Postfach - unverarbeitet';
                    CaptionClass = Mailbox4Unprocessed;
                    Visible = Mailbox4Visible;
                }
                field("Mailbox 5 - Unprocessed"; Rec."Mailbox 5 - Unprocessed")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Mailrobot Inbox List";
                    ToolTip = 'Mailbox - Unprocessed', Comment = 'de-DE=E-Mails Postfach - unverarbeitet';
                    CaptionClass = Mailbox5Unprocessed;
                    Visible = Mailbox5Visible;
                }
            }
            cuegroup(Category)
            {
                Caption = 'Category', Comment = 'de-DE=Tickets nach Kategorien';
                Visible = ShowCategory;
                field("Tickets - Open - Category 1"; Rec."Tickets - Open - Category 1")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Category 1', Comment = 'de-DE=Nach Kategorie 1 aus der Einrichtung';
                    CaptionClass = CategoryField1Text;
                    Visible = CategoryField1Visible;
                }
                field("Tickets - Open - Category 2"; Rec."Tickets - Open - Category 2")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Category 2', Comment = 'de-DE=Nach Kategorie 2 aus der Einrichtung';
                    CaptionClass = CategoryField2Text;
                    Visible = CategoryField2Visible;
                }
                field("Tickets - Open - Category 3"; Rec."Tickets - Open - Category 3")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Category 3', Comment = 'de-DE=Nach Kategorie 3 aus der Einrichtung';
                    CaptionClass = CategoryField3Text;
                    Visible = CategoryField3Visible;
                }
                field("Tickets - Open - Category 4"; Rec."Tickets - Open - Category 4")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Category 4', Comment = 'de-DE=Nach Kategorie 4 aus der Einrichtung';
                    CaptionClass = CategoryField4Text;
                    Visible = CategoryField4Visible;
                }
            }
            cuegroup(ProcessStage)
            {
                Caption = 'ProcessStage', Comment = 'de-DE=Tickets nach Prozess';
                Visible = ShowProcess;
                field("Tickets - Open - Process 1"; Rec."Tickets - Open - Process 1")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Process 1', Comment = 'de-DE=Nach Prozess 1 aus der Einrichtung';
                    CaptionClass = ProcessField1Text;
                    Visible = ProcessField1Visible;
                }
                field("Tickets - Open - Process 2"; Rec."Tickets - Open - Process 2")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Process 2', Comment = 'de-DE=Nach Prozess 2 aus der Einrichtung';
                    CaptionClass = ProcessField2Text;
                    Visible = ProcessField2Visible;
                }
                field("Tickets - Open - Process 3"; Rec."Tickets - Open - Process 3")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Process 3', Comment = 'de-DE=Nach Prozess 3 aus der Einrichtung';
                    CaptionClass = ProcessField3Text;
                    Visible = ProcessField3Visible;
                }
                field("Tickets - Open - Process 4"; Rec."Tickets - Open - Process 4")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Process 4', Comment = 'de-DE=Nach Prozess 4 aus der Einrichtung';
                    CaptionClass = ProcessField4Text;
                    Visible = ProcessField4Visible;
                }
            }

            cuegroup(TicketQueues)
            {
                Caption = 'TicketQueues', Comment = 'de-DE=Ticket-Warteschlangen';
                Visible = ShowQueue;

                field("Tickets - Open - Queue - 1"; Rec."Tickets - Open - Queue - 1")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 1', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue1Text;
                    Visible = Queue1Visible;
                }

                field("Tickets - Open - Queue - 2"; Rec."Tickets - Open - Queue - 2")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 2', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue2Text;
                    Visible = Queue2Visible;
                }
                field("Tickets - Open - Queue - 3"; Rec."Tickets - Open - Queue - 3")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 3', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue3Text;
                    Visible = Queue3Visible;
                }
                field("Tickets - Open - Queue - 4"; Rec."Tickets - Open - Queue - 4")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 4', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue4Text;
                    Visible = Queue4Visible;
                }
                field("Tickets - Open - Queue - 5"; Rec."Tickets - Open - Queue - 5")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 5', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue5Text;
                    Visible = Queue5Visible;
                }
                field("Tickets - Open - Queue - 6"; Rec."Tickets - Open - Queue - 6")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 6', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue6Text;
                    Visible = Queue6Visible;
                }
                field("Tickets - Open - Queue - 7"; Rec."Tickets - Open - Queue - 7")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 7', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue7Text;
                    Visible = Queue7Visible;
                }
                field("Tickets - Open - Queue - 8"; Rec."Tickets - Open - Queue - 8")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 8', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue8Text;
                    Visible = Queue8Visible;
                }
                field("Tickets - Open - Queue - 9"; Rec."Tickets - Open - Queue - 9")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 9', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue9Text;
                    Visible = Queue9Visible;
                }
                field("Tickets - Open - Queue - 10"; Rec."Tickets - Open - Queue - 10")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 10', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue10Text;
                    Visible = Queue10Visible;
                }
                field("Tickets - Open - Queue - 11"; Rec."Tickets - Open - Queue - 11")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 11', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue11Text;
                    Visible = Queue11Visible;
                }
                field("Tickets - Open - Queue - 12"; Rec."Tickets - Open - Queue - 12")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 12', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue12Text;
                    Visible = Queue12Visible;
                }
                field("Tickets - Open - Queue - 13"; Rec."Tickets - Open - Queue - 13")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 13', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue13Text;
                    Visible = Queue13Visible;
                }
                field("Tickets - Open - Queue - 14"; Rec."Tickets - Open - Queue - 14")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 14', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue14Text;
                    Visible = Queue14Visible;
                }
                field("Tickets - Open - Queue - 15"; Rec."Tickets - Open - Queue - 15")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 15', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue15Text;
                    Visible = Queue15Visible;
                }
                field("Tickets - Open - Queue - 16"; Rec."Tickets - Open - Queue - 16")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 16', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue16Text;
                    Visible = Queue16Visible;
                }
                field("Tickets - Open - Queue - 17"; Rec."Tickets - Open - Queue - 17")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 17', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue17Text;
                    Visible = Queue17Visible;
                }
                field("Tickets - Open - Queue - 18"; Rec."Tickets - Open - Queue - 18")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 18', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue18Text;
                    Visible = Queue18Visible;
                }
                field("Tickets - Open - Queue - 19"; Rec."Tickets - Open - Queue - 19")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 19', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue19Text;
                    Visible = Queue19Visible;
                }
                field("Tickets - Open - Queue - 20"; Rec."Tickets - Open - Queue - 20")
                {
                    ApplicationArea = All;
                    DrillDownPageID = "ICI Support Ticket List";
                    ToolTip = 'Tickets - Open - Queue - 20', Comment = 'de-DE=Nach Warteschlangenbenutzer';
                    CaptionClass = Queue20Text;
                    Visible = Queue20Visible;
                }
            }
            cuegroup(MyTodos)
            {
                Caption = 'My ToDos', Comment = 'de-DE=Meine Aufgaben';
                Visible = ShowToDos AND ShowPersonalToDos;
                field("No. of My Open ToDo"; Rec."No. of My Open ToDo")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of My Open ToDos', Comment = 'de-DE=Meine Off. Aufgaben';
                }
                field("No. of My Duty ToDo"; Rec."No. of My Duty ToDo")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of My Duty ToDo', Comment = 'de-DE=Meine fälligen Aufgaben';
                }
            }
            cuegroup(OpenTodos)
            {
                Caption = 'ToDos', Comment = 'de-DE=Aufgaben';
                Visible = ShowToDos;
                field("No. of Open ToDo"; Rec."No. of Open ToDo")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of Open ToDos', Comment = 'de-DE=Offene Aufgaben';
                }
                field("No. of Open ToDo to My Ticket"; Rec."No. of Open ToDo to My Ticket")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of Open ToDo to My Ticket', Comment = 'de-DE=Of. Aufgaben zu m. Tickets';
                }
                field("No. of Duty ToDo to My Ticket"; Rec."No. of Duty ToDo to My Ticket")
                {
                    ApplicationArea = All;
                    ToolTip = 'No. of Duty ToDo to My Ticket', Comment = 'de-DE=Fällige Aufgaben zu m. Ticket';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(TicketBoard)
            {
                ApplicationArea = All;
                Visible = ShowTicketBoard;
                Caption = 'Ticketboard', Comment = 'de-DE=Ticketboard';
                Image = MachineCenterCalendar;
                ToolTip = 'Opens the Ticketboard', Comment = 'de-DE=Ticketboard';
                RunObject = Page "ICI Ticket Board";
            }
        }
    }
    trigger OnOpenPage()
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportSetup: Record "ICI Support Setup";
    begin
        Rec.RESET();
        if not Rec.get() then begin
            Rec.INIT();
            Rec.INSERT();
        end;

        Rec.SETFILTER("Date Filter 1", '%1..', CREATEDATETIME(WorkDate(), 0T), CREATEDATETIME(WORKDATE(), 235959T));
        Rec.SETFILTER("Date Filter 2", '%1..%2', CREATEDATETIME(CALCDATE('<-30D>', TODAY()), 0T), CREATEDATETIME(TODAY, 235959T));
        Rec.SETFILTER("Date Filter 3", '..%1', CREATEDATETIME(CALCDATE('<-2D>', TODAY()), 0T));

        IF ICISupportUser.GetCurrUser(ICISupportUser) then begin
            Rec.SetFilter("User Filter", ICISupportUser."User ID");
            Rec.CalcFields("My Tickets - All");

            IF ICISupportUser."Dep. Filter" <> '' then begin
                ShowDepartments := true;
                Rec.SETFILTER("Department Filter", ICISupportUser."Dep. Filter");
                Rec.CalcFields("My Dep. Tickets - All");
            end;
        end;

        CalcNoOfTicketsEscalated();
        CalcNoOfSpamMails();
        CalcMailboxTiles();
        CalcCategoryTiles();
        CalcProcessTiles();
        CalcQueueTiles();
        CalcToDoTiles();

        IF ICISupportSetup.GET() THEN
            ShowTicketBoard := ICISupportSetup."Ticketboard active"; // XXX - Todo - Get this right
    end;

    local procedure CalcMailboxTiles()
    var
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
        ICISupportUser: Record "ICI Support User";
        counter: Integer;
    begin
        IF ICISupportUser.GetCurrUser(ICISupportUser) THEN begin
            ShowMailobot := ICISupportUser."Show Mailrobot Cues";
            ShowCategory := ICISupportUser."Show Category Cues";
            ShowProcess := ICISupportUser."Show Process Cues";
            ShowQueue := ICISupportUser."Show Queue Cues";
            ShowToDos := ICISupportUser."Show ToDo Cues";
        end;

        ICIMailrobotMailbox.SetRange(Active, true);
        IF ICIMailrobotMailbox.FindSet() then
            repeat
                counter += 1;
                case counter of
                    1:
                        begin
                            Rec.SetFilter("Mailbox 1 - Filter", '=%1', ICIMailrobotMailbox.Code);
                            Mailbox1Visible := true;
                            Mailbox1Unprocessed := StrSubstNo(MailboxUnprocessedLbl, ICIMailrobotMailbox.Description);
                            Rec.CALCFIELDS("Mailbox 1 - Unprocessed");
                        end;
                    2:
                        begin
                            Rec.SetFilter("Mailbox 2 - Filter", '=%1', ICIMailrobotMailbox.Code);
                            Mailbox2Visible := true;
                            Mailbox2Unprocessed := StrSubstNo(MailboxUnprocessedLbl, ICIMailrobotMailbox.Description);
                            Rec.CALCFIELDS("Mailbox 2 - Unprocessed");
                        end;
                    3:
                        begin
                            Rec.SetFilter("Mailbox 3 - Filter", '=%1', ICIMailrobotMailbox.Code);
                            Mailbox3Visible := true;
                            Mailbox3Unprocessed := StrSubstNo(MailboxUnprocessedLbl, ICIMailrobotMailbox.Description);
                            Rec.CALCFIELDS("Mailbox 3 - Unprocessed");
                        end;
                    4:
                        begin
                            Rec.SetFilter("Mailbox 4 - Filter", '=%1', ICIMailrobotMailbox.Code);
                            Mailbox4Visible := true;
                            Mailbox4Unprocessed := StrSubstNo(MailboxUnprocessedLbl, ICIMailrobotMailbox.Description);
                            Rec.CALCFIELDS("Mailbox 4 - Unprocessed");
                        end;
                    5:
                        begin
                            Rec.SetFilter("Mailbox 5 - Filter", '=%1', ICIMailrobotMailbox.Code);
                            Mailbox5Visible := true;
                            Mailbox5Unprocessed := StrSubstNo(MailboxUnprocessedLbl, ICIMailrobotMailbox.Description);
                            Rec.CALCFIELDS("Mailbox 5 - Unprocessed");
                        end;
                end

            until ICIMailrobotMailbox.Next() = 0;
    end;

    procedure CalcNoOfSpamMails()
    var
        ICIMailrobotLog: Record "ICI Mailrobot Log";
        Green: Text;
        Red: Text;
    begin
        Green := 'Favorable';
        Red := 'Unfavorable';

        CLEAR(NoOfSpamMailRobotInbox);
        CLEAR(SpamICIMailrobotLog);

        ICIMailrobotLog.SetRange("Process State", SpamICIMailrobotLog."Process State"::Received);
        IF ICIMailrobotLog.FINDSET() THEN
            repeat
                IF Not ICIMailrobotLog.CheckProcessing() then begin
                    NoOfSpamMailRobotInbox += 1;
                    SpamICIMailrobotLog.GET(ICIMailrobotLog."Entry No.");
                    SpamICIMailrobotLog.Mark(TRUE);
                end;

            until ICIMailrobotLog.Next() = 0;
        SpamICIMailrobotLog.MarkedOnly(TRUE);

        // EscalatedStyleExpr := Green;
        // IF NoOfTicketsEscalated > 0 then
        //     EscalatedStyleExpr := Red;
    end;

    procedure CalcNoOfTicketsEscalated()
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Green: Text;
        Red: Text;
    begin
        Green := 'Favorable';
        Red := 'Unfavorable';

        CLEAR(NoOfTicketsEscalated);
        CLEAR(EscalatedICISupportTicket);

        ICISupportTicket.SetFilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
        IF ICISupportTicket.FINDSET() THEN
            repeat
                IF ICISupportTicket.isOverdue() then begin
                    NoOfTicketsEscalated += 1;
                    EscalatedICISupportTicket.GET(ICISupportTicket."No.");
                    EscalatedICISupportTicket.Mark(TRUE);
                end;

            until ICISupportTicket.Next() = 0;

        EscalatedStyleExpr := Green;
        IF NoOfTicketsEscalated > 0 then
            EscalatedStyleExpr := Red;

        EscalatedICISupportTicket.MarkedOnly(TRUE);

    end;

    local procedure CalcCategoryTiles()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
    begin
        ICISupportSetup.GET();

        CategoryField1Visible := (ICISupportSetup."Cue 1 - Category 1 Filter" <> '');
        CategoryField2Visible := (ICISupportSetup."Cue 2 - Category 1 Filter" <> '');
        CategoryField3Visible := (ICISupportSetup."Cue 3 - Category 1 Filter" <> '');
        CategoryField4Visible := (ICISupportSetup."Cue 4 - Category 1 Filter" <> '');

        // Tile 1
        CLEAR(ICISupportTicketCategory);
        IF ICISupportSetup."Cue 1 - Category 1 Filter" <> '' THEN begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 1 - Category 1 Filter") then;
            Rec.SETFILTER("Cue 1 - Category 1 Filter", ICISupportSetup."Cue 1 - Category 1 Filter");
        end;
        IF ICISupportSetup."Cue 1 - Category 2 Filter" <> '' then begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 1 - Category 2 Filter") then;
            Rec.SETFILTER("Cue 1 - Category 2 Filter", ICISupportSetup."Cue 1 - Category 2 Filter");
        end;
        CategoryField1Text := ICISupportTicketCategory.Description;
        IF CategoryField1Text = '' then
            CategoryField1Text := ICISupportSetup."Cue 1 - Category 2 Filter";
        IF CategoryField1Text = '' then
            CategoryField1Text := ICISupportSetup."Cue 1 - Category 1 Filter";
        Rec.CalcFields("Tickets - Open - Category 1");


        // Tile 2
        CLEAR(ICISupportTicketCategory);
        IF ICISupportSetup."Cue 2 - Category 1 Filter" <> '' THEN begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 2 - Category 1 Filter") then;
            Rec.SETFILTER("Cue 2 - Category 1 Filter", ICISupportSetup."Cue 2 - Category 1 Filter");
        end;
        IF ICISupportSetup."Cue 2 - Category 2 Filter" <> '' then begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 2 - Category 2 Filter") then;
            Rec.SETFILTER("Cue 2 - Category 2 Filter", ICISupportSetup."Cue 2 - Category 2 Filter");
        end;
        CategoryField2Text := ICISupportTicketCategory.Description;
        IF CategoryField2Text = '' then
            CategoryField2Text := ICISupportSetup."Cue 2 - Category 2 Filter";
        IF CategoryField2Text = '' then
            CategoryField2Text := ICISupportSetup."Cue 2 - Category 1 Filter";
        Rec.CalcFields("Tickets - Open - Category 2");

        // Tile 3
        CLEAR(ICISupportTicketCategory);
        IF ICISupportSetup."Cue 3 - Category 1 Filter" <> '' THEN begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 3 - Category 1 Filter") then;
            Rec.SETFILTER("Cue 3 - Category 1 Filter", ICISupportSetup."Cue 3 - Category 1 Filter");
        end;
        IF ICISupportSetup."Cue 3 - Category 2 Filter" <> '' then begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 3 - Category 2 Filter") then;
            Rec.SETFILTER("Cue 3 - Category 2 Filter", ICISupportSetup."Cue 3 - Category 2 Filter");
        end;
        CategoryField3Text := ICISupportTicketCategory.Description;
        IF CategoryField3Text = '' then
            CategoryField3Text := ICISupportSetup."Cue 3 - Category 2 Filter";
        IF CategoryField3Text = '' then
            CategoryField3Text := ICISupportSetup."Cue 3 - Category 1 Filter";
        Rec.CalcFields("Tickets - Open - Category 3");

        // Tile 4
        CLEAR(ICISupportTicketCategory);
        IF ICISupportSetup."Cue 4 - Category 1 Filter" <> '' THEN begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 4 - Category 1 Filter") then;
            Rec.SETFILTER("Cue 4 - Category 1 Filter", ICISupportSetup."Cue 4 - Category 1 Filter");
        end;
        IF ICISupportSetup."Cue 4 - Category 2 Filter" <> '' then begin
            IF ICISupportTicketCategory.GET(ICISupportSetup."Cue 4 - Category 2 Filter") then;
            Rec.SETFILTER("Cue 4 - Category 2 Filter", ICISupportSetup."Cue 4 - Category 2 Filter");
        end;
        CategoryField4Text := ICISupportTicketCategory.Description;
        IF CategoryField4Text = '' then
            CategoryField4Text := ICISupportSetup."Cue 4 - Category 2 Filter";
        IF CategoryField4Text = '' then
            CategoryField4Text := ICISupportSetup."Cue 4 - Category 1 Filter";
        Rec.CalcFields("Tickets - Open - Category 4");

    end;

    local procedure CalcProcessTiles()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportProcess: Record "ICI Support Process";
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportSetup.GET();

        ProcessField1Visible := (ICISupportSetup."Cue 1 - Process Code" <> '');
        ProcessField2Visible := (ICISupportSetup."Cue 2 - Process Code" <> '');
        ProcessField3Visible := (ICISupportSetup."Cue 3 - Process Code" <> '');
        ProcessField4Visible := (ICISupportSetup."Cue 4 - Process Code" <> '');

        // Tile 1
        CLEAR(ICISupportProcess);
        IF ICISupportSetup."Cue 1 - Process Code" <> '' THEN begin
            ICISupportProcess.GET(ICISupportSetup."Cue 1 - Process Code");
            Rec.SETFILTER("Cue 1 - Process Code Filter", '=%1', ICISupportSetup."Cue 1 - Process Code");
            ProcessField1Text := ICISupportProcess.Description;
        end;
        IF ICISupportSetup."Cue 1 - Process Stage" <> 0 then begin
            ICISupportProcessStage.GET(ICISupportSetup."Cue 1 - Process Code", ICISupportSetup."Cue 1 - Process Stage");
            Rec.SETFILTER("Cue 1 - Process Stage Filter", '=%1', ICISupportSetup."Cue 1 - Process Stage");
            ProcessField1Text := ICISupportProcessStage.Description;
        end;
        Rec.CalcFields("Tickets - Open - Process 1");

        // Tile 2
        CLEAR(ICISupportProcess);
        IF ICISupportSetup."Cue 2 - Process Code" <> '' THEN begin
            ICISupportProcess.GET(ICISupportSetup."Cue 2 - Process Code");
            Rec.SETFILTER("Cue 2 - Process Code Filter", '=%1', ICISupportSetup."Cue 2 - Process Code");
            ProcessField2Text := ICISupportProcess.Description;
        end;
        IF ICISupportSetup."Cue 2 - Process Stage" <> 0 then begin
            ICISupportProcessStage.GET(ICISupportSetup."Cue 2 - Process Code", ICISupportSetup."Cue 2 - Process Stage");
            Rec.SETFILTER("Cue 2 - Process Stage Filter", '=%1', ICISupportSetup."Cue 2 - Process Stage");
            ProcessField2Text := ICISupportProcessStage.Description;
        end;
        Rec.CalcFields("Tickets - Open - Process 2");

        // Tile 3
        CLEAR(ICISupportProcess);
        IF ICISupportSetup."Cue 3 - Process Code" <> '' THEN begin
            ICISupportProcess.GET(ICISupportSetup."Cue 3 - Process Code");
            Rec.SETFILTER("Cue 3 - Process Code Filter", '=%1', ICISupportSetup."Cue 3 - Process Code");
            ProcessField3Text := ICISupportProcess.Description;
        end;
        IF ICISupportSetup."Cue 3 - Process Stage" <> 0 then begin
            ICISupportProcessStage.GET(ICISupportSetup."Cue 3 - Process Code", ICISupportSetup."Cue 3 - Process Stage");
            Rec.SETFILTER("Cue 3 - Process Stage Filter", '=%1', ICISupportSetup."Cue 3 - Process Stage");
            ProcessField3Text := ICISupportProcessStage.Description;
        end;
        Rec.CalcFields("Tickets - Open - Process 3");

        // Tile 4
        CLEAR(ICISupportProcess);
        IF ICISupportSetup."Cue 4 - Process Code" <> '' THEN begin
            ICISupportProcess.GET(ICISupportSetup."Cue 4 - Process Code");
            Rec.SETFILTER("Cue 4 - Process Code Filter", '=%1', ICISupportSetup."Cue 4 - Process Code");
            ProcessField4Text := ICISupportProcess.Description;
        end;
        IF ICISupportSetup."Cue 4 - Process Stage" <> 0 then begin
            ICISupportProcessStage.GET(ICISupportSetup."Cue 4 - Process Code", ICISupportSetup."Cue 4 - Process Stage");
            Rec.SETFILTER("Cue 4 - Process Stage Filter", '=%1', ICISupportSetup."Cue 4 - Process Stage");
            ProcessField4Text := ICISupportProcessStage.Description;
        end;
        Rec.CalcFields("Tickets - Open - Process 4");
    end;

    local procedure CalcQueueTiles()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportUser: Record "ICI Support User";
        counter: Integer;
    begin
        ICISupportSetup.GET();
        ICISupportUser.SetRange(Queue, true);
        IF NOT ICISupportUser.Findset() THEN
            exit;
        counter := 1;
        repeat
            case counter of
                1:
                    begin
                        Rec.SetFilter("Queue 1 - User Filter", ICISupportUser."User ID");
                        Queue1Text := ICISupportUser.Name;
                        Queue1Visible := true;
                    end;
                2:
                    begin
                        Rec.SetFilter("Queue 2 - User Filter", ICISupportUser."User ID");
                        Queue2Text := ICISupportUser.Name;
                        Queue2Visible := true;
                    end;
                3:
                    begin
                        Rec.SetFilter("Queue 3 - User Filter", ICISupportUser."User ID");
                        Queue3Text := ICISupportUser.Name;
                        Queue3Visible := true;
                    end;
                4:
                    begin
                        Rec.SetFilter("Queue 4 - User Filter", ICISupportUser."User ID");
                        Queue4Text := ICISupportUser.Name;
                        Queue4Visible := true;
                    end;
                5:
                    begin
                        Rec.SetFilter("Queue 5 - User Filter", ICISupportUser."User ID");
                        Queue5Text := ICISupportUser.Name;
                        Queue5Visible := true;
                    end;
                6:
                    begin
                        Rec.SetFilter("Queue 6 - User Filter", ICISupportUser."User ID");
                        Queue6Text := ICISupportUser.Name;
                        Queue6Visible := true;
                    end;
                7:
                    begin
                        Rec.SetFilter("Queue 7 - User Filter", ICISupportUser."User ID");
                        Queue7Text := ICISupportUser.Name;
                        Queue7Visible := true;
                    end;
                8:
                    begin
                        Rec.SetFilter("Queue 8 - User Filter", ICISupportUser."User ID");
                        Queue8Text := ICISupportUser.Name;
                        Queue8Visible := true;
                    end;
                9:
                    begin
                        Rec.SetFilter("Queue 9 - User Filter", ICISupportUser."User ID");
                        Queue9Text := ICISupportUser.Name;
                        Queue9Visible := true;
                    end;
                10:
                    begin
                        Rec.SetFilter("Queue 10 - User Filter", ICISupportUser."User ID");
                        Queue10Text := ICISupportUser.Name;
                        Queue10Visible := true;
                    end;
                11:
                    begin
                        Rec.SetFilter("Queue 11 - User Filter", ICISupportUser."User ID");
                        Queue11Text := ICISupportUser.Name;
                        Queue11Visible := true;
                    end;
                12:
                    begin
                        Rec.SetFilter("Queue 12 - User Filter", ICISupportUser."User ID");
                        Queue12Text := ICISupportUser.Name;
                        Queue12Visible := true;
                    end;
                13:
                    begin
                        Rec.SetFilter("Queue 13 - User Filter", ICISupportUser."User ID");
                        Queue13Text := ICISupportUser.Name;
                        Queue13Visible := true;
                    end;
                14:
                    begin
                        Rec.SetFilter("Queue 14 - User Filter", ICISupportUser."User ID");
                        Queue14Text := ICISupportUser.Name;
                        Queue14Visible := true;
                    end;
                15:
                    begin
                        Rec.SetFilter("Queue 15 - User Filter", ICISupportUser."User ID");
                        Queue15Text := ICISupportUser.Name;
                        Queue15Visible := true;
                    end;
                16:
                    begin
                        Rec.SetFilter("Queue 16 - User Filter", ICISupportUser."User ID");
                        Queue16Text := ICISupportUser.Name;
                        Queue16Visible := true;
                    end;
                17:
                    begin
                        Rec.SetFilter("Queue 17 - User Filter", ICISupportUser."User ID");
                        Queue17Text := ICISupportUser.Name;
                        Queue17Visible := true;
                    end;
                18:
                    begin
                        Rec.SetFilter("Queue 18 - User Filter", ICISupportUser."User ID");
                        Queue18Text := ICISupportUser.Name;
                        Queue18Visible := true;
                    end;
                19:
                    begin
                        Rec.SetFilter("Queue 19 - User Filter", ICISupportUser."User ID");
                        Queue19Text := ICISupportUser.Name;
                        Queue19Visible := true;
                    end;
                20:
                    begin
                        Rec.SetFilter("Queue 20 - User Filter", ICISupportUser."User ID");
                        Queue20Text := ICISupportUser.Name;
                        Queue20Visible := true;
                    end;
            end;

            counter += 1;
        until ICISupportUser.Next() = 0;
    end;

    local procedure CalcToDoTiles()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportUser: Record "ICI Support User";
    begin
        IF NOT ICISupportSetup.GET() THEN exit;
        IF NOT ICISupportSetup."Task Integration" THEN exit;
        IF NOT ShowToDos then exit;

        // User Filter is already set
        IF ICISupportUser.GetCurrUser(ICISupportUser) THEN begin
            ShowPersonalToDos := true;
            Rec.SetRange("Salesperson Filter", ICISupportUser."Salesperson Code");
            Rec.SETFILTER("Date Filter 4", '..%1', WORKDATE());

        end;

    end;

    var
        EscalatedICISupportTicket: Record "ICI Support Ticket";
        SpamICIMailrobotLog: Record "ICI Mailrobot Log";
        NoOfSpamMailRobotInbox: Integer;
        NoOfTicketsEscalated: Integer;

        EscalatedStyleExpr: Text;
        ShowMailobot: Boolean;
        ShowCategory: Boolean;
        ShowDepartments: Boolean;
        ShowProcess: Boolean;
        ShowTicketBoard: Boolean;
        Mailbox1Visible: Boolean;
        Mailbox2Visible: Boolean;
        Mailbox3Visible: Boolean;
        Mailbox4Visible: Boolean;
        Mailbox5Visible: Boolean;
        MailboxUnprocessedLbl: Label '%1', Comment = '%1=Mailbox|de-DE=%1';
        Mailbox1Unprocessed: Text;
        Mailbox2Unprocessed: Text;
        Mailbox3Unprocessed: Text;
        Mailbox4Unprocessed: Text;
        Mailbox5Unprocessed: Text;

        CategoryField1Text: Text;
        CategoryField2Text: Text;
        CategoryField3Text: Text;
        CategoryField4Text: Text;
        CategoryField1Visible: Boolean;
        CategoryField2Visible: Boolean;
        CategoryField3Visible: Boolean;
        CategoryField4Visible: Boolean;
        ProcessField1Text: Text;
        ProcessField2Text: Text;
        ProcessField3Text: Text;
        ProcessField4Text: Text;
        ProcessField1Visible: Boolean;
        ProcessField2Visible: Boolean;
        ProcessField3Visible: Boolean;
        ProcessField4Visible: Boolean;
        ShowQueue: Boolean;

        Queue1Text: Text;
        Queue1Visible: Boolean;
        Queue2Text: Text;
        Queue2Visible: Boolean;
        Queue3Text: Text;
        Queue3Visible: Boolean;
        Queue4Text: Text;
        Queue4Visible: Boolean;
        Queue5Text: Text;
        Queue5Visible: Boolean;
        Queue6Text: Text;
        Queue6Visible: Boolean;
        Queue7Text: Text;
        Queue7Visible: Boolean;
        Queue8Text: Text;
        Queue8Visible: Boolean;
        Queue9Text: Text;
        Queue9Visible: Boolean;
        Queue10Text: Text;
        Queue10Visible: Boolean;
        Queue11Text: Text;
        Queue11Visible: Boolean;
        Queue12Text: Text;
        Queue12Visible: Boolean;
        Queue13Text: Text;
        Queue13Visible: Boolean;
        Queue14Text: Text;
        Queue14Visible: Boolean;
        Queue15Text: Text;
        Queue15Visible: Boolean;
        Queue16Text: Text;
        Queue16Visible: Boolean;
        Queue17Text: Text;
        Queue17Visible: Boolean;
        Queue18Text: Text;
        Queue18Visible: Boolean;
        Queue19Text: Text;
        Queue19Visible: Boolean;
        Queue20Text: Text;
        Queue20Visible: Boolean;
        ShowToDos: Boolean;
        ShowPersonalToDos: Boolean;

}
