tableextension 56304 "ICI Language" extends Language
{
    fields
    {
        field(56276; "ICI RMA Custom Layout Code"; Code[20])
        {
            Caption = 'RMA Custom Layout Code', Comment = 'de-DE=RMA Berichtslayout';
            DataClassification = CustomerContent;
            TableRelation = "Custom Report Layout";

            trigger OnLookup()
            var
                ICISupportSetup: Record "ICI Support Setup";
                CustomReportLayout: Record "Custom Report Layout";
                CustomReportLayoutSelection: Page "Custom Report Layouts";
            begin
                ICISupportSetup.GET();

                CustomReportLayout.SetRange("Report ID", ICISupportSetup."RMA-Report ID");
                CustomReportLayoutSelection.SetTableView(CustomReportLayout);
                CustomReportLayoutSelection.EDITABLE(FALSE);
                CustomReportLayoutSelection.LOOKUPMODE(TRUE);
                IF CustomReportLayoutSelection.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                    CustomReportLayoutSelection.GetRecord(CustomReportLayout);
                    Validate("ICI RMA Custom Layout Code", CustomReportLayout.Code);
                end;
            end;
        }

    }
}
