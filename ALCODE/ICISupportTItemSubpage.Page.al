page 56320 "ICI Support T. Item Subpage"
{

    Caption = 'ICI Support Ticket Item Subpage';
    SourceTable = "ICI Support Ticket Item";
    AutoSplitKey = true;
    DelayedInsert = true;
    PageType = ListPart;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';

                    trigger OnValidate()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                        PreparationErrLbl: Label 'This action cannot be used on tickets in preparation', Comment = 'de-DE=Die ausgewählte Funktion steht für Tickets in Vorbereitung nicht zur Verfügung';

                    begin
                        If ICISupportTicket.GET(Rec."Support Ticket No.") then
                            IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation then
                                error(PreparationErrLbl);
                    end;
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'No.', Comment = 'de-DE=Nr.';

                    trigger OnValidate()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                        PreparationErrLbl: Label 'This action cannot be used on tickets in preparation', Comment = 'de-DE=Die ausgewählte Funktion steht für Tickets in Vorbereitung nicht zur Verfügung';

                    begin
                        If ICISupportTicket.GET(Rec."Support Ticket No.") then
                            IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation then
                                error(PreparationErrLbl);
                    end;
                }
                field("Item Variant Code"; Rec."Item Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Item Variant Code', Comment = 'de-DE=Variantencode';
                }
                field("Work Type Code"; Rec."Work Type Code")
                {
                    Visible = ShowWorkType;
                    ApplicationArea = All;
                    ToolTip = 'Work Type Code', Comment = 'de-DE=Arbeitstypcode';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Quantity', Comment = 'de-DE=Menge';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Unit of Measure Code', Comment = 'de-DE=Einheitencode';
                }
                field("Use in Sales Doc."; Rec."Use in Sales Doc.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Use in Sales Doc.', Comment = 'de-DE=In VK Belegen verwenden';
                }
                field("Use in Purch. Doc."; Rec."Use in Purch. Doc.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Use in Purch. Doc.', Comment = 'de-DE=In EK Belegen verwenden';
                }
                field("Use in Service Doc."; Rec."Use in Service Doc.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Use in Service Doc.', Comment = 'de-DE=In Service Belegen verwenden';
                }
                field(Comment; Rec.Comment)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Comment', Comment = 'de-DE=Kommentar';
                }
                field("Line No."; Rec."Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Line No.', Comment = 'de-DE=Zeilennr.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Card)
            {
                ApplicationArea = All;
                Caption = 'Card', Comment = 'de-DE=Öffnen';
                ToolTip = 'Card', Comment = 'de-DE=Öffnet die Karte des zugehörigen Datensatzes in der ausgewählten Zeile';
                Image = Card;

                trigger OnAction()
                var
                    Item: Record "Item";
                    ServiceItem: Record "Service Item";
                    Resource: Record "Resource";
                    ServiceCost: Record "Service Cost";
                begin
                    CASE Rec.Type OF
                        Rec.Type::Item:
                            BEGIN
                                Item.GET(Rec."No.");
                                PAGE.RUN(PAGE::"Item Card", Item);
                            END;
                        Rec.Type::ServiceItem:
                            BEGIN
                                ServiceItem.GET(Rec."No.");
                                PAGE.RUN(PAGE::"Service Item Card", ServiceItem);
                            END;
                        Rec.Type::Resource:
                            BEGIN
                                Resource.GET(Rec."No.");
                                PAGE.RUN(PAGE::"Resource Card", Resource);
                            END;
                        Rec.Type::ServiceCost:
                            BEGIN
                                ServiceCost.GET(Rec."No.");
                                PAGE.RUN(PAGE::"Service Costs", ServiceCost);
                            END;
                    END;
                end;
            }
            action(ShowMore)
            {
                Visible = Not ShowWorkType;
                Caption = 'Show More', Comment = 'de-DE=Mehr Spalten Anzeigen';
                ToolTip = 'Show More', Comment = 'de-DE=Mehr Spalten Anzeigen';
                Image = SetupColumns;

                ApplicationArea = All;

                trigger OnAction()
                begin
                    ShowWorkType := true;
                    CurrPage.Update();
                end;
            }
            action(ShowLess)
            {
                Visible = ShowWorkType;
                Caption = 'Show Less', Comment = 'de-DE=Weniger Spalten Anzeigen';
                ToolTip = 'Show Less', Comment = 'de-DE=Weniger Spalten Anzeigen';
                Image = SetupList;

                ApplicationArea = All;
                trigger OnAction()
                begin
                    ShowWorkType := false;
                    CurrPage.Update();

                end;
            }
        }
    }
    var
        ShowWorkType: Boolean;

}
