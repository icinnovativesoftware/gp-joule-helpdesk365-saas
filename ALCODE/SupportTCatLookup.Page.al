page 56361 "ICI Support T. Cat. Lookup"
{

    Caption = 'Support T. Cat. Lookup', Comment = 'de-DE=Kategorieauswahl';
    PageType = List;
    SourceTable = "ICI Support Ticket Category";
    UsageCategory = None;
    DelayedInsert = true;

    SourceTableView = SORTING("Parent Category", Code);

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Parent Category"; Rec."Parent Category")
                {
                    Visible = false;
                    ToolTip = 'Parent Category', Comment = 'de-DE=Gehört zu Kategorie';
                    ApplicationArea = All;
                }
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    begin
        ICISupportSetup.Get();

        IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers" THEN begin
            OnlyChildCategory := Rec.GETFILTER("Category Filter") <> ''; // Filter ist bei Category 2 gesetzt
            Rec.SetFilter("Parent Category", '=%1', '');
            IF OnlyChildCategory then
                Rec.SetRange("Parent Category", Rec.GETFILTER("Category Filter"));
        end;

        IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Two-Layers" THEN begin
            OnlyLayer2 := Rec.GETFILTER("Category Filter") <> ''; // Filter ist bei Category 2 gesetzt
            Rec.SetRange(Layer, 0);
            IF OnlyLayer2 then
                Rec.SetRange(Layer, 1);
        end;
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        CurrFilter: Code[50];
    begin
#pragma warning disable AA0139
        // IC - SEB - 20220510 - as long as CurrFilter has the same length as "Parent Category" overflow is impossible
        CurrFilter := Rec.GetFilter("Parent Category");
#pragma warning restore AA0139
        if CurrFilter <> '' then
            Rec.Validate("Parent Category", CurrFilter);
    end;

    var
        ICISupportSetup: Record "ICI Support Setup";
        OnlyChildCategory: Boolean;
        OnlyLayer2: Boolean;
}
