page 56287 "ICI Support Mail Setup"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    Caption = 'Support E-Mail Setup', Comment = 'de-DE=Support E-Mail Einrichtung';
    PageType = Card;
    SourceTable = "ICI Support Mail Setup";
    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';

                field("Test Mode"; Rec."Test Mode")
                {
                    ApplicationArea = All;
                    ToolTip = 'If the Test Mode is Enabled, every external E-Mail is sent to the Test Mode E-Mail Receiver. And no E-Mail will be sent to the Contacts/Customers and Supportusers', Comment = 'de-DE=Im Testmodus werden alle ausgehenden E-Mails an die Test E-Mail Empfänger gesendet und keine E-Mail an Kunden oder Supportbenutzer';
                }
                field("Test Mode E-Mail Receiver"; Rec."Test Mode E-Mail Receiver")
                {
                    ApplicationArea = All;
                    ToolTip = 'If the Test Mode is Enabled, every external E-Mail is sent to this Test Mode E-Mail Receiver', Comment = 'de-DE=Im Testmodus werden alle ausgehenden E-Mails an die Test E-Mail Empfänger gesendet.';
                }

                field("E-Mail BCC"; Rec."E-Mail BCC")
                {
                    ApplicationArea = All;
                    ToolTip = 'BCC', Comment = 'de-DE=BCC';
                }


                field("E-Mail Footer"; Rec."E-Mail Footer")
                {
                    ApplicationArea = All;
                    ToolTip = 'Footer', Comment = 'de-DE=Der Footer kann in allen E-Mail Benachrichtigungen mit %footer% eingebunden werden';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Footer", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;

                }
            }
            group(Defaults)
            {
                Caption = 'Defaults', Comment = 'de-DE=Vorbelegungen';
                field("Salutation Code M"; Rec."Salutation Code M")
                {
                    ApplicationArea = All;
                    ToolTip = 'Salutation Code M', Comment = 'de-DE=Anredecode männlich';
                }
                field("Salutation Code F"; Rec."Salutation Code F")
                {
                    ApplicationArea = All;
                    ToolTip = 'Salutation Code F', Comment = 'de-DE=Anredecode weiblich';
                }
                field("Salutation Code D"; Rec."Salutation Code D")
                {
                    ApplicationArea = All;
                    ToolTip = 'Salutation Code D', Comment = 'de-DE=Anredecode divers';
                }
            }

            group(User)
            {
                Caption = 'User', Comment = 'de-DE=Benutzer';

                field("E-Mail U Escalation"; Rec."E-Mail U Escalation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Escalation', Comment = 'de-DE=Eskalation';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U Escalation", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail U Forwarded Active"; Rec."E-Mail U Forwarded Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Weiterleitung Aktiv';
                }
                field("E-Mail U Forwarded"; Rec."E-Mail U Forwarded")
                {
                    ApplicationArea = All;
                    ToolTip = 'Forwarded', Comment = 'de-DE=Ticket weitergeleitet';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U Forwarded", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }

                field("E-Mail U PW forgotten Active"; Rec."E-Mail U PW forgotten Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Passwort vergessen Aktiv';
                }
                field("E-Mail U Password forgotten"; Rec."E-Mail U Password forgotten")
                {
                    ApplicationArea = All;
                    ToolTip = 'Password forgotten', Comment = 'de-DE=Passwort Vergessen';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U Password forgotten", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail U Login Active"; Rec."E-Mail U Login Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Zugangsdatenv Aktiv';
                }
                field("E-Mail U Login"; Rec."E-Mail U Login")
                {
                    ApplicationArea = All;
                    ToolTip = 'Login', Comment = 'de-DE=Zugangsdaten';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U Login", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail U New Message Active"; Rec."E-Mail U New Message Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Neue Nachricht Aktiv';
                }
                field("E-Mail U New Message"; Rec."E-Mail U New Message")
                {
                    ApplicationArea = All;
                    ToolTip = 'New Message', Comment = 'de-DE=Neue Nachricht';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U New Message", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
            }
            group(Contact)
            {
                Caption = 'Contact', Comment = 'de-DE=Kontakt';
                field("E-Mail C PW forgotten Active"; Rec."E-Mail C PW forgotten Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
                field("E-Mail C Password forgotten"; Rec."E-Mail C Password forgotten")
                {
                    ApplicationArea = All;
                    ToolTip = 'Passwort Vergessen', Comment = 'de-DE=Passwort Vergessen';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C Password forgotten", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail C Forwarded Active"; Rec."E-Mail C Forwarded Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
                field("E-Mail C Forwarded"; Rec."E-Mail C Forwarded")
                {
                    ApplicationArea = All;
                    ToolTip = 'Forwarded', Comment = 'de-DE=Ticket weitergeleitet';

                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C Forwarded", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }

                field("E-Mail C Login Active"; Rec."E-Mail C Login Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
                field("E-Mail C Login"; Rec."E-Mail C Login")
                {
                    ApplicationArea = All;
                    ToolTip = 'Login', Comment = 'de-DE=Zugangsdaten';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C Login", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail C New Message Active"; Rec."E-Mail C New Message Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
                field("E-Mail C New Message"; Rec."E-Mail C New Message")
                {
                    ApplicationArea = All;
                    ToolTip = 'New Message', Comment = 'de-DE=Neue Nachricht';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C New Message", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail C New File Active"; Rec."E-Mail C New File Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Active', Comment = 'de-DE=Aktiv';
                }
                field("E-Mail C New File"; Rec."E-Mail C New File")
                {
                    ApplicationArea = All;
                    ToolTip = 'New File', Comment = 'de-DE=Neue Datei';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C New File", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail C RMA"; Rec."E-Mail C RMA")
                {
                    ApplicationArea = All;
                    ToolTip = 'RMA', Comment = 'de-DE=RMA Formular';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail C RMA", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
            }
            group(Webarchiv)
            {
                Caption = 'Webarchiv', Comment = 'de-DE=Belegarchiv';
                Visible = PortalIntegration;

                field("E-Mail Sales Quote"; Rec."E-Mail Sales Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Quote', Comment = 'de-DE=VK Angebot';
                    Visible = ShowSalesQuote;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Quote", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Sales Order"; Rec."E-Mail Sales Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Order', Comment = 'de-DE=VK Auftrag';
                    Visible = ShowSalesOrder;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Order", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Sales Invoice"; Rec."E-Mail Sales Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Invoice', Comment = 'de-DE=VK Rechnung';
                    Visible = ShowSalesInvoice;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Invoice", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Sales Cr.Memo"; Rec."E-Mail Sales Cr.Memo")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Cr.Memo', Comment = 'de-DE=VK Gutschrift';
                    Visible = ShowSalesCreditMemo;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Cr.Memo", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Sales Blanket Order"; Rec."E-Mail Sales Blanket Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Blanket Order', Comment = 'de-DE=VK Rahmenauftrag';
                    Visible = ShowSalesBlanketOrder;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Blanket Order", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Sales Return Order"; Rec."E-Mail Sales Return Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Sales Return Order', Comment = 'de-DE=VK Reklamation';
                    Visible = ShowSalesReturnOrder;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Sales Return Order", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Pst. Sales Shipment"; Rec."E-Mail Pst. Sales Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Sales Shipment', Comment = 'de-DE=Geb. VK Lieferung';
                    Visible = ShowPostedSalesShipment;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Pst. Sales Shipment", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Pst. Sales Invoice"; Rec."E-Mail Pst. Sales Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Sales Invoice', Comment = 'de-DE=Geb. VK Rechnung';
                    Visible = ShowPostedSalesInvoice;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Pst. Sales Invoice", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Pst. Sales Cr.Memo"; Rec."E-Mail Pst. Sales Cr.Memo")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Sales Cr.Memo', Comment = 'de-DE=Geb. VK Gutschrift';
                    Visible = ShowPostedSalesCrMemo;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Pst. Sales Cr.Memo", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Service Quote"; Rec."E-Mail Service Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Service Quote', Comment = 'de-DE=Service Angebot';
                    Visible = ShowServiceQuote;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Quote", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Service Order"; Rec."E-Mail Service Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Service Order', Comment = 'de-DE=Service Auftrag';
                    Visible = ShowServiceOrder;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Order", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Service Invoice"; Rec."E-Mail Service Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Service Invoice', Comment = 'de-DE=Service Rechnung';
                    Visible = ShowServiceInvoice;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Invoice", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Service Cr.Memo"; Rec."E-Mail Service Cr.Memo")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Service Cr.Memo', Comment = 'de-DE=Service Gutschrift';
                    Visible = ShowServiceCreditMemo;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Cr.Memo", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Posted Service Shipment"; Rec."E-Mail Posted Service Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Service Shipment', Comment = 'de-DE=Geb. Service Lieferung';
                    Visible = ShowPostedServiceShipment;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Posted Service Shipment", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Posted Service Invoice"; Rec."E-Mail Posted Service Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Service Invoice', Comment = 'de-DE=Geb. Service Rechnung';
                    Visible = ShowPostedServiceInvoice;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Posted Service Invoice", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Posted Service Cr.Memo"; Rec."E-Mail Posted Service Cr.Memo")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Service Cr.Memo', Comment = 'de-DE=Geb. Service Gutschrift';
                    Visible = ShowPostedServiceCrMemo;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Posted Service Cr.Memo", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Posted Service Contract"; Rec."E-Mail Service Contract")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Service Contract', Comment = 'de-DE=Geb. Servicevertrag';
                    Visible = ShowServiceContract;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Contract", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
                field("E-Mail Posted Service Contract Quote"; Rec."E-Mail Service Contract Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Pst. Service Contract Quote', Comment = 'de-DE=Geb. Servicevertragsangebot';
                    Visible = ShowServiceContractQuote;
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail Service Contract Quote", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ReadMail)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Download and Process all E-Mail Inboxes', Comment = 'de-DE=Alle Postfächer auslesen und E-Mails verarbeiten';
                Image = SendMail;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                ToolTip = 'Synchronizes the IMAP Inboxes to the Mailrobot Log', Comment = 'de-DE=Läd die E-Mails aus allen aktiven Postfächern in das Mailrobot Log und verarbeitet diese';

                trigger OnAction()
                var
                    MailRobotMgt: Codeunit "ICI Mailrobot Mgt.";
                begin
                    MailRobotMgt.DownloadAllMailboxes();
                    COMMIT();
                    MailRobotMgt.ProcessAllMailRobotLogs();
                end;
            }
        }
    }

    var
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        PortalIntegration: Boolean;
        ShowSalesQuote: Boolean;
        ShowSalesOrder: Boolean;
        ShowSalesInvoice: Boolean;
        ShowSalesCreditMemo: Boolean;
        ShowSalesBlanketOrder: Boolean;
        ShowSalesReturnOrder: Boolean;
        ShowPostedSalesShipment: Boolean;
        ShowPostedSalesInvoice: Boolean;
        ShowPostedSalesCrMemo: Boolean;
        ShowServiceQuote: Boolean;
        ShowServiceOrder: Boolean;
        ShowServiceInvoice: Boolean;
        ShowServiceCreditMemo: Boolean;
        ShowPostedServiceShipment: Boolean;
        ShowPostedServiceInvoice: Boolean;
        ShowPostedServiceCrMemo: Boolean;
        ShowServiceContract: Boolean;
        ShowServiceContractQuote: Boolean;



    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICIWebarchiveSetup: Record "ICI Webarchive Setup";
    begin
        Rec.Reset();
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert();
        end;
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() THEN
                PortalIntegration := ICISupportSetup."Portal Integration";

        IF ICIWebarchiveSetup.ReadPermission Then
            IF ICIWebarchiveSetup.GET() THEN begin
                ShowSalesQuote := ICIWebarchiveSetup."Webarchiv Sales Quote";
                ShowSalesOrder := ICIWebarchiveSetup."Webarchiv Sales Order";
                ShowSalesInvoice := ICIWebarchiveSetup."Webarchiv Sales Invoice";
                ShowSalesCreditMemo := ICIWebarchiveSetup."Webarchiv Posted Sales Cr.Memo";
                ShowSalesBlanketOrder := ICIWebarchiveSetup."Webarchiv Sales Blanket Order";
                ShowSalesReturnOrder := ICIWebarchiveSetup."Webarchiv Sales Return Order";
                ShowPostedSalesShipment := ICIWebarchiveSetup."Webar. Posted Sales Shipment";
                ShowPostedSalesInvoice := ICIWebarchiveSetup."Webarchiv Posted Sales Invoice";
                ShowPostedSalesCrMemo := ICIWebarchiveSetup."Webarchiv Sales Credit Memo";
                ShowServiceQuote := ICIWebarchiveSetup."Webarchiv Service Quote";
                ShowServiceOrder := ICIWebarchiveSetup."Webarchiv Service Order";
                ShowServiceInvoice := ICIWebarchiveSetup."Webarchiv Service Invoice";
                ShowServiceCreditMemo := ICIWebarchiveSetup."Webarchiv Service Credit Memo";
                ShowPostedServiceShipment := ICIWebarchiveSetup."Webar. Posted Service Shipment";
                ShowPostedServiceInvoice := ICIWebarchiveSetup."Webar. Posted Service Invoice";
                ShowPostedServiceCrMemo := ICIWebarchiveSetup."Webar. Posted Service Cr.Memo";
                ShowServiceContract := ICIWebarchiveSetup."Webarchiv Service Contract";
                ShowServiceContractQuote := ICIWebarchiveSetup."Webar. Service Contract Quote";
            end;
    end;
}
