codeunit 56300 "ICI Notification Dialog Mgt."
{
    procedure SetTicket(var pICISupportTicket: Record "ICI Support Ticket")
    begin
        ICISupportTicket := pICISupportTicket;
    end;

    procedure GetTicketNo(): Code[20]
    begin
        EXIT(ICISupportTicket."No.");
    end;

    procedure SetInitText(pInitText: Text)
    begin
        InitText := pInitText;
    end;

    procedure SetInitSubject(pInitSubject: Text)
    begin
        InitSubject := pInitSubject;
    end;

    procedure SetInternally(pInternally: Boolean)
    begin
        Internally := pInternally;
    end;

    procedure GetInternally(): Boolean
    begin
        exit(Internally);
    end;

    procedure AddReceiver(ReceiverCode: Code[50]; NotificationMethodType: Enum "Notification Method Type"; Name: Text[100]; EMail: Text[250]; EMailRecipientType: Enum "Email Recipient Type"; pRecordId: RecordID)
    var
        Receiver: JsonObject;
    begin
        Receiver.Add('Code', ReceiverCode);
        Receiver.Add('NotificationMethodType', NotificationMethodType.AsInteger());
        Receiver.Add('Name', Name);
        Receiver.Add('EMail', EMail);
        Receiver.Add('EMailRecipientType', EMailRecipientType.AsInteger());
        Receiver.Add('RecordID', FORMAT(pRecordId));

        Recipients.Add(Receiver);
    end;

    procedure GetRecipientsAsText() RecipientsAsText: Text
    var
        NoOfReceivers: Integer;
        Counter: Integer;
        JTok: JsonToken;
        Recipient: JsonObject;
        Name: Text[100];
        EMail: Text[250];
        EMailRecipientType: Enum "Email Recipient Type";
        RecipientLbl: Label '(%3):%1<%2>;', Comment = '%1=Name;%2=NotificationCode(Mail, Telephone);%3=To/CC/BCC';
    begin
        NoOfReceivers := Recipients.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF Recipients.GET(Counter, JTok) THEN begin
                Recipient := JTok.AsObject();

                IF Recipient.GET('Name', JTok) THEN
                    Name := JTok.AsValue().AsText();
                IF Recipient.GET('EMail', JTok) THEN
                    EMail := JTok.AsValue().AsText();
                IF Recipient.GET('EMailRecipientType', JTok) THEN
                    EMailRecipientType := Enum::"EMail Recipient Type".FromInteger(jTok.AsValue().AsInteger());

                RecipientsAsText := RecipientsAsText + StrSubstNo(RecipientLbl, Name, EMail, EMailRecipientType);
            end;
    end;


    procedure DeleteReceiver(var ICINotReceiverBuffer: Record "ICI Not. Receiver Buffer")
    var
        NoOfReceivers: Integer;
        Counter: Integer;
        JTok: JsonToken;
        MatchingCode: Boolean;
        Recipient: JsonObject;
    begin
        NoOfReceivers := Recipients.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF Recipients.GET(Counter, JTok) THEN begin
                Recipient := JTok.AsObject();
                IF Recipient.GET('Code', JTok) THEN
                    MatchingCode := ICINotReceiverBuffer.Code = JTok.AsValue().AsText();

                IF (MatchingCode) THEN
                    Recipients.RemoveAt(Counter);
            end;
    end;

    procedure ModifyReceiver(var ICINotReceiverBuffer: Record "ICI Not. Receiver Buffer")
    var
        NoOfReceivers: Integer;
        Counter: Integer;
        JTok: JsonToken;
        MatchingCode: Boolean;
        Recipient: JsonObject;
    begin
        NoOfReceivers := Recipients.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF Recipients.GET(Counter, JTok) THEN begin
                Recipient := JTok.AsObject();
                IF Recipient.GET('Code', JTok) THEN
                    MatchingCode := ICINotReceiverBuffer.Code = JTok.AsValue().AsText();

                IF (MatchingCode) THEN begin
                    //Recipient.Replace('Code', ICINotReceiverBuffer.Code);
                    Recipient.Replace('NotificationMethodType', ICINotReceiverBuffer."Notification Method Type".AsInteger());
                    Recipient.Replace('Name', ICINotReceiverBuffer.Description);
                    Recipient.Replace('EMail', ICINotReceiverBuffer."E-Mail");
                    Recipient.Replace('EMailRecipientType', ICINotReceiverBuffer."E-Mail Recipient Type".AsInteger());
                    Recipient.Replace('RecordID', FORMAT(ICINotReceiverBuffer."Record ID"));
                end;
            end;
    end;

    procedure AddAttachment(AttachmentName: Text[250]; ContentType: Text[250]; AttachmentBase64: Text)
    var
        Attachment: JsonObject;
    begin
        Attachment.Add('AttachmentName', AttachmentName);
        Attachment.Add('ContentType', ContentType);
        Attachment.Add('AttachmentBase64', AttachmentBase64);
        Attachments.Add(Attachment);
    end;

    procedure GetAttachmentsAsText() AttachmentsAsText: Text
    var
        NoOfAttachments: Integer;
        Counter: Integer;
        JTok: JsonToken;
        Attachment: JsonObject;
        Name: Text;
        AttachmentLbl: Label '%1;', Comment = '%1=Name';
    begin
        NoOfAttachments := Attachments.Count();
        For Counter := 0 to (NoOfAttachments - 1) do // JSON Arrays start at 0
            IF Attachments.GET(Counter, JTok) THEN begin
                Attachment := JTok.AsObject();

                IF Attachment.GET('AttachmentName', JTok) THEN
                    Name := JTok.AsValue().AsText();
                AttachmentsAsText := AttachmentsAsText + StrSubstNo(AttachmentLbl, Name);
            end;
    end;

    procedure DeleteAttachment(var ICINotAttachmentBuffer: Record "ICI Not. Attachment Buffer")
    var
        NoOfAttachments: Integer;
        Counter: Integer;
        JTok: JsonToken;
        Attachment: JsonObject;
        MatchingName: Boolean;
        MatchingContentType: Boolean;
    begin
        NoOfAttachments := Attachments.Count();
        For Counter := 0 to (NoOfAttachments - 1) do // JSON Arrays start at 0
            IF Attachments.GET(Counter, JTok) THEN begin
                Attachment := JTok.AsObject();

                IF Attachment.GET('AttachmentName', JTok) THEN
                    MatchingName := ICINotAttachmentBuffer.Name = JTok.AsValue().AsText();
                IF Attachment.GET('ContentType', JTok) THEN
                    MatchingContentType := ICINotAttachmentBuffer."Content Type" = JTok.AsValue().AsText();

                IF (MatchingName AND MatchingContentType /*AND MatchingB64*/) THEN
                    Attachments.RemoveAt(Counter);
            end;
    end;

    procedure CreateNotificationValues() InitData: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.Get();

        InitData.Add('TicketNo', ICISupportTicket."No.");
        InitData.Add('InitText', InitText);
        InitData.Add('InitSubject', InitSubject);

        InitData.Add('Attachments', Attachments);

        InitData.Add('Receivers', Recipients);
    end;

    procedure GetInitData(FullScreen: Boolean) InitData: JsonObject
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.Get();
        InitData.Add('FullScreen', FullScreen);
        InitData.Add('ColorContactMessage', ICISupportSetup."Color - Contact Message");
        InitData.Add('ColorUserMessage', ICISupportSetup."Color - User Message");
        InitData.Add('ColorInternalMessage', ICISupportSetup."Color - Internal Message");
        InitData.Add('DefaultInternally', ICISupportSetup."Communication - Default Intern");
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";
        InitText: Text;
        InitSubject: Text;
        Recipients: JsonArray;
        Attachments: JsonArray;
        Internally: Boolean;
}
