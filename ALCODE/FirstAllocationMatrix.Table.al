table 56306 "ICI First Allocation Matrix"
{
    Caption = 'First Allocation Matrix', Comment = 'de-DE=Ticketzuweisungsmatrix';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Department Code"; Code[20])
        {
            Caption = 'Department Code', Comment = 'de-DE=Abteilungscode';
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
        }
        field(2; "Category 1 Code"; Code[20])
        {
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
        }
        field(3; "Category 2 Code"; Code[20])
        {
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Ticket Category" where("Category Filter" = field("Category 1 Code"), Inactive = const(false));
        }
        field(10; "Support User ID"; Code[50])
        {
            Caption = 'Support User ID', Comment = 'de-DE=Benutzer';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support User";
        }
    }
    keys
    {
        key(PK; "Department Code", "Category 1 Code", "Category 2 Code")
        {
            Clustered = true;
        }
    }

    procedure GetFirstAllocation(var SupportTicket: Record "ICI Support Ticket"; OnCreate: Boolean): Code[50]
    var
        FirstAllocationMatrix: Record "ICI First Allocation Matrix";
        ICISupportSetup: Record "ICI Support Setup";
    begin
        CLEAR(FirstAllocationMatrix);
        FirstAllocationMatrix.SetRange("Department Code", SupportTicket."Department Code");
        FirstAllocationMatrix.SetRange("Category 1 Code", SupportTicket."Category 1 Code");
        FirstAllocationMatrix.SetRange("Category 2 Code", SupportTicket."Category 2 Code");
        IF FirstAllocationMatrix.FindFirst() then
            Exit(FirstAllocationMatrix."Support User ID");

        FirstAllocationMatrix.SetFilter("Category 2 Code", '=%1', ''); // Clear filter from Category 2
        IF FirstAllocationMatrix.FindFirst() then
            Exit(FirstAllocationMatrix."Support User ID");

        FirstAllocationMatrix.SetFilter("Category 1 Code", '=%1', ''); // Clear filter from Category 1
        IF FirstAllocationMatrix.FindFirst() then
            Exit(FirstAllocationMatrix."Support User ID");

        FirstAllocationMatrix.SetFilter("Department Code", '=%1', ''); // Clear filter from Department
        IF FirstAllocationMatrix.FindFirst() then
            Exit(FirstAllocationMatrix."Support User ID");


        ICISupportSetup.GET();
        IF OnCreate THEN
            IF ICISupportSetup."First Supportuser Allocation" <> '' then
                EXIT(ICISupportSetup."First Supportuser Allocation");

        EXIT('');
    end;

}
