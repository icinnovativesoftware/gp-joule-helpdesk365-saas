table 56284 "ICI Support Process"
{
    Caption = 'ICI Support Ticket Process', Comment = 'de-DE=Ticket Prozess';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Support Process List";
    DrillDownPageId = "ICI Support Process List";

    fields
    {
        field(1; Code; Code[20])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[50])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "Has Prep"; Boolean)
        {
            Editable = false;
            Caption = 'Has Prep', Comment = 'de-DE=Hat Vorbereitungsstufe';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Support Process Stage" where("Process Code" = field(Code), "Ticket State" = const(Preparation), Initial = const(true)));
        }
        field(12; "Has Open"; Boolean)
        {
            Editable = false;
            Caption = 'Has Open', Comment = 'de-DE=Hat Eröffnetsstufe';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Support Process Stage" where("Process Code" = field(Code), "Ticket State" = const(Open), Initial = const(true)));
        }
        field(13; "Has Process"; Boolean)
        {
            Editable = false;
            Caption = 'Has Process', Comment = 'de-DE=Hat Bearbeitungsstufe';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Support Process Stage" where("Process Code" = field(Code), "Ticket State" = const(Processing), Initial = const(true)));
        }
        field(14; "Has Wait"; Boolean)
        {
            Editable = false;
            Caption = 'Has Wait', Comment = 'de-DE=Hat Wartestufe';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Support Process Stage" where("Process Code" = field(Code), "Ticket State" = const(Waiting), Initial = const(true)));
        }
        field(15; "Has Closed"; Boolean)
        {
            Editable = false;
            Caption = 'Has Closed', Comment = 'de-DE=Hat Abgeschlossenstufe';
            FieldClass = FlowField;
            CalcFormula = exist("ICI Support Process Stage" where("Process Code" = field(Code), "Ticket State" = const(Closed), Initial = const(true)));
        }
        field(16; "No. of Process Lines"; Integer)
        {
            Editable = false;
            Caption = 'No. of Process Lines', Comment = 'de-DE=Anzahl Prozessstufen';
            FieldClass = FlowField;
            CalcFormula = Count("ICI Support Process Stage" where("Process Code" = field(Code)));
        }
        field(17; "Default Escalation Code"; Code[20])
        {
            Caption = 'Default Escalation Code', Comment = 'de-DE=Vorbel. Eskalationscode';
            TableRelation = "ICI Escalation".Code;
        }
        field(18; "Default Accounting Type"; Code[20])
        {
            Caption = 'Accounting', Comment = 'de-DE=Abrechnung';
            TableRelation = "ICI Support Time Acc. Type";
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

}
