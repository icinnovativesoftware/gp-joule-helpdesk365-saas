page 56293 "ICI Support Process Card"
{

    Caption = 'ICI Support Ticket Proc. Card', Comment = 'de-DE=Support Ticket Prozess';
    PageType = Card;
    SourceTable = "ICI Support Process";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Default Escalation Code"; Rec."Default Escalation Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Default Escalation Code', Comment = 'de-DE=Beim Eintragen des Prozesses in das Ticket, wird dieser Eskalationscode mit eingetragen - sollte er noch leer sein';
                }
                field("Default Accounting Type"; Rec."Default Accounting Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Default Accounting Type', Comment = 'de-DE=Beim Eintragen des Prozesses in das Ticket, wird die Abrechnungsart mit eingetragen';
                }
            }
            part(Lines; "ICI Support Proc. Stage Listp.")
            {
                ApplicationArea = All;
                SubPageLink = "Process Code" = field(Code);
            }
        }
    }

}
