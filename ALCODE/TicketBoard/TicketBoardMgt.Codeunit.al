codeunit 56291 "ICI Ticket Board Mgt."
{
    procedure ForwardUserAddLastPrio(var CurrSupportTicket: Record "ICI Support Ticket"; Type: Integer)
    var
        LookupSupportTicket: Record "ICI Support Ticket";
        MaxTicketOrder: Integer;
    begin
        CASE Type OF
            1:
                BEGIN
                    // Ticket wird weitergeleitet. TicketOrderId "hinten anhängen"
                    LookupSupportTicket.SetRange("Support User ID", CurrSupportTicket."Support User ID"); // Is already the New Support User
                    LookupSupportTicket.SetFilter("Ticket State", '<>%1', CurrSupportTicket."Ticket State"::Closed);
                    LookupSupportTicket.SetCurrentKey("Ticket Board Order");
                    IF LookupSupportTicket.FindLast() THEN
                        MaxTicketOrder := LookupSupportTicket."Ticket Board Order"; // Größte TicketBoardOrder suchen

                    CurrSupportTicket."Ticket Board Order" := MaxTicketOrder + 1; // und +1 setzen
                    //CurrSupportTicket.Modify(); // No Modify. Called from Table Validate Trigger
                END;
        END;
    end;

    // Nummeriert die TicketBoardOrder der Tickets für einen Mitarbeiter neu
    procedure DefragTicketOrder(ForUserID: Code[50])
    var
        ICISupportUser: Record "ICI Support User";
        SupportTicket: Record "ICI SUpport TIcket";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        TOrder: Integer;
    begin
        ICISupportUser.GET(ForUserID);

        SupportTicket.SETFILTER("Support User ID", ICISupportUser."User ID");
        SupportTicket.SETFILTER("Ticket State", '<>%1', SupportTicket."Ticket State"::Closed);
        SupportTicket.SETCURRENTKEY("Ticket Board Order");
        IF SupportTicket.FindSet() THEN
            REPEAT
                TOrder += 1;
                IF SupportTicket."Ticket Board Order" <> TOrder THEN
                    TempNameValueBuffer.AddNewEntry(SupportTicket."No.", FORMAT(TOrder));
            UNTIL SupportTicket.NEXT() = 0;

        IF TempNameValueBuffer.FindSet() THEN
            repeat
                Evaluate(TOrder, TempNameValueBuffer.GetValue());
                SupportTicket.GET(TempNameValueBuffer.Name);
                SupportTicket."Ticket Board Order" := TOrder;
                SupportTicket.Modify();
            until TempNameValueBuffer.Next() = 0;

    end;
}
