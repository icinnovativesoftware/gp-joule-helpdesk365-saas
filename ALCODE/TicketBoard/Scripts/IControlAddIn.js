﻿var globalsalesperson = [];
var globalamounts = [];
// --Drag&Drop
var SalesPersonbeforeDrag = [];
var posBeforeDrag = [];
var globalBaseColor;

window.onerror = function (msg, url, linenumber) {
    alert('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
    return true;
}

function getTextColorForBackground(str){
    var match = str.match(/rgba?\((\d{1,3}), ?(\d{1,3}), ?(\d{1,3})\)?(?:, ?(\d(?:\.\d?))\))?/);
    if(match){
        red = match[1];
        green = match[2];
        blue = match[3];
        if ((red*0.299 + green*0.587 + blue*0.114) < 186) 
            return "#ffffff";
    }    
    return "#000000";
}

function SetTicketInfo(ticketno, salespersoncode, ticketorder, description, company, companycontact, creationdate, lastactiondate, ticketcategory, allsalesperson, colorCode) {
    //debugger;
    if ((ticketno != '') && (salespersoncode != '') && (ticketorder != '') && (description != '')) {

        body = buildWindow(ticketno, salespersoncode, ticketorder, description, company, companycontact, creationdate, lastactiondate, ticketcategory, allsalesperson,colorCode);
        document.getElementById("DD"+ salespersoncode).appendChild(body);

        buttons = buildbuttons(ticketno);
        document.getElementById(ticketno).appendChild(buttons);


        // Calc Color Of Texts depending on Background Color
        // Ticket Öffnen Buttons
        openTicketButtons = $('button.open-ticket');
        textBackground = getTextColorForBackground(openTicketButtons.css('backgroundColor'));
        openTicketButtons.css('color',textBackground);

        // Ticket Header
        ticketDiv = $('#'+ticketno + ' .ticket-head');
        textBackground = getTextColorForBackground(ticketDiv.css('backgroundColor'));
        $('#' + ticketno +' .ticket-head b').css("color",textBackground);
    }
}

function InitSalesPersons(sperson, amounts, baseColor){
    globalBaseColor = baseColor;
    createtopbutton();
    init();

    if (sperson!= '') {
        globalsalesperson = sperson.split("|");
        globalamounts = amounts.split("|");
    }
    else{
        alert('Sie gehören keinem Team an!');        
    }

    var i;
    for (i = 0; i < globalsalesperson.length; i++) {

        header = buildWindowheader(globalsalesperson[i],globalamounts[i]);
        document.getElementById("tr1").appendChild(header);
    }
    
    // Set Textcolor to white/black depending on Background
    // Ein/Ausklappen Button
    foldButton = $('#foldAllButton');
    textBackground = getTextColorForBackground(foldButton.css('backgroundColor'));
    $('#foldbutton b').css('color',textBackground);

    // Salesperson Header
    salespersonHeader = $('.vk-title');
    textBackground = getTextColorForBackground(salespersonHeader.css('backgroundColor'));
    $('.vk-title b').css('color',textBackground);

    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("TicketLoaded", []);
}

function init() {
    var div = document.getElementById("controlAddIn");
    div.style.overflowY = "´hidden";
    div.style.height = "100%";
    div.style.width = "100%";
    div.style.whiteSpace = "nowrap"

    var salespersonwrap = document.createElement('div');
    salespersonwrap.className = 'salesperson-wrap';
    div.appendChild(salespersonwrap);

    var table1 = document.createElement('table');
    salespersonwrap.appendChild(table1);

    var tr1 = document.createElement('tr');
    tr1.id = "tr1";
    table1.appendChild(tr1);

}

function createtopbutton() {
    var buttonall = document.createElement('div');
    buttonall.className = "navButtons";

    var buttons = document.createElement('button');
    buttons.className = "fold";
    buttons.id = "foldAllButton";
    if(globalBaseColor){
        buttons.style.backgroundColor = globalBaseColor;
    }
    buttons.color = '#fff';
    buttons.innerHTML = '<p><b>' + 'Alle ein/ausklappen' + '</b></p>';
    buttons.onclick = function () { toggleAllContainerViews() }; ;
    buttonall.appendChild(buttons);

    var i = document.createElement('i');
    i.className = "gg-arrow-down-o";
    buttons.appendChild(i);

    document.getElementById("controlAddIn").appendChild(buttonall);
}

function buildWindowheader(globalsalesperson, amount) {

    var th1 = document.createElement('th');

    var windowheader = document.createElement('div');
    windowheader.className = 'salesperson';
    windowheader.id = globalsalesperson;
    windowheader.style.width = "280px";
    windowheader.style.height = "auto";
    windowheader.style.background = "orange";
    windowheader.style.color = "white";
    windowheader.style.fontFamily = "Arial";
    windowheader.style.alignItems = "center";
    th1.appendChild(windowheader);

    var showinfo = document.createElement('div');
    showinfo.className = "vk-title";
    if(globalBaseColor){
        showinfo.style.backgroundColor = globalBaseColor;
    }
    
    
    windowheader.appendChild(showinfo);

    var dragdrop = document.createElement('div');
    dragdrop.id = "DD"+globalsalesperson;
    dragdrop.className = "Dragdrop";
    dragdrop.style.position = "relative";
    dragdrop.style.height = "100%";
    dragdrop.style.minHeight = "100vh";
    windowheader.appendChild(dragdrop);

    createDragDrop(dragdrop);

    var showinfosp = document.createElement('p');
    //showinfop.style.color = getTextColorForBackground($(showinfop).css('backgroundColor'));
    showinfosp.innerHTML = '<b>' + globalsalesperson + '</b>';

    showinfo.appendChild(showinfosp);

    var showamount = document.createElement('p');
    if(!amount){
        amount = 0;
    }
    showamount.innerHTML = '<b>' + 'Tickets' + ' ' + amount + '</b>';
    showinfo.appendChild(showamount);

    return (th1);
}

function buildWindow(ticketno, salespersoncode, ticketorder, description, company, companycontact, creationdate, lastactiondate, ticketcategory, allsalesperson,colorCode) {

    var window = document.createElement('div');
    window.className = 'ticket';
    window.style.width = "100%";
    window.style.height = "auto";
    window.style.background = "red";
    window.style.fontFamily = "Arial";
    window.style.color = "white";
    window.draggable = "true";
    window.ondrop = function () { dragover(window) };
    window.ondragstart = function () { dragstart(window) };
    window.id = ticketno;

    var folder = document.createElement('div');
    folder.className = 'change-view';
    window.appendChild(folder);

    var imageUrl = Microsoft.Dynamics.NAV.GetImageResource('ALCode/TicketBoard/Images/folder-open-regular.svg');

    var folderpic = document.createElement("img");
    folderpic.className = 'folder1';
    folderpic.setAttribute("src", imageUrl);
    folderpic.setAttribute("alt", "folder");
    folderpic.id = ticketno;
    folderpic.onclick = function () { toggleContainerView(folderpic.id); };
    folder.appendChild(folderpic);

    var tickethead = document.createElement('div');
    tickethead.className = "ticket-head";
    if(colorCode){
        tickethead.style.backgroundColor = colorCode;
    }

    
    window.appendChild(tickethead);

    var tnumber = document.createElement('p');
    tnumber.className = 'ticketnumber';
    tnumber.innerHTML = '<b>' + ticketno + '</b>';
    tickethead.appendChild(tnumber);

    var tsalesp = document.createElement('p');
    tsalesp.className = 'hide';
    tsalesp.id = salespersoncode;
    tsalesp.innerHTML = '<b>' + salespersoncode + '</b>';
    window.appendChild(tsalesp);

    var torder = document.createElement('p');
    torder.className = 'hide prio-info';
    torder.id = ticketorder;
    torder.innerHTML = '<b>' + ticketorder + '</b>';
    window.appendChild(torder);

    var tname = document.createElement('p');
    tname.className = 'ticketname';
    tname.innerHTML = '<b>' + description + '</b>';
    window.appendChild(tname);

    var tabcompany = document.createElement('table');
    tabcompany.className = 'table-company';
    window.appendChild(tabcompany);

    var trcompany = document.createElement('tr');
    trcompany.className = 'company-name-wrap';
    tabcompany.appendChild(trcompany);

    var tdcompany = document.createElement('td');
    tdcompany.className = 'contact-name table-label';
    tdcompany.innerHTML = '<b>' + "Unternehmen: " + '</b>';
    trcompany.appendChild(tdcompany);

    var tdcompanyname = document.createElement('td');
    tdcompanyname.className = 'company-name-info table-info';
    tdcompanyname.innerHTML = '<b>' + company + '</b>';
    trcompany.appendChild(tdcompanyname);

    var closedwrap = document.createElement('div');
    closedwrap.id = ticketno;
    closedwrap.className = 'closed-wrap';
    if (getCookie(ticketno)){
        closedwrap.className += ' hide'; 
    }
    window.appendChild(closedwrap);

    var tableinfo = document.createElement('table');
    tableinfo.className = 'table-info';
    closedwrap.appendChild(tableinfo);

    var contactnamewrap = document.createElement('tr');
    contactnamewrap.className = 'contact-name-wrap';
    tableinfo.appendChild(contactnamewrap);

    var contactnametablelabel = document.createElement('td');
    contactnametablelabel.className = 'contact-name table-label';
    contactnametablelabel.innerHTML = '<b>' + "Ansprechpartner: " + '</b>';
    contactnamewrap.appendChild(contactnametablelabel);

    var contactnameinfo = document.createElement('td');
    contactnameinfo.className = 'contact-name info';
    contactnameinfo.innerHTML = '<b>' + companycontact + '</b>';
    contactnamewrap.appendChild(contactnameinfo);


    var opendatewrap = document.createElement('tr');
    opendatewrap.className = 'open-date-wrap';
    tableinfo.appendChild(opendatewrap);

    var opendatetablelabel = document.createElement('td');
    opendatetablelabel.className = 'open-date table-label';
    opendatetablelabel.innerHTML = '<b>' + "Erstellungsdatum: " + '</b>';
    opendatewrap.appendChild(opendatetablelabel);

    var opendateinfo = document.createElement('td');
    opendateinfo.className = 'open-date-info table-info';
    opendateinfo.innerHTML = '<b>' + creationdate + '</b>';               
    opendatewrap.appendChild(opendateinfo);
				
    var changedatewrap = document.createElement('tr');
    changedatewrap.className = 'change-date-wrap';
    tableinfo.appendChild(changedatewrap);

    var dropdown = builddropdown(ticketno, salespersoncode);
    closedwrap.appendChild(dropdown);

				
    var lastdatechangelabel = document.createElement('td');
	lastdatechangelabel.className = 'last-change-date table-label';
	lastdatechangelabel.innerHTML = '<b>' + "Letzte Änderung: " + '</b>';
	changedatewrap.appendChild(lastdatechangelabel);
					
    var lastchangeinfo = document.createElement('td');
    lastchangeinfo.className = 'last-change-date-info table-info';
    lastchangeinfo.innerHTML = '<b>' + lastactiondate + '</b>';     
	changedatewrap.appendChild(lastchangeinfo);

    var openticket = document.createElement('button');
    openticket.className = 'open-ticket';
    if(globalBaseColor){
        openticket.style.backgroundColor = globalBaseColor;
    }
    openticket.innerHTML = "Ticket öffnen";
    openticket.id = ticketno;
    openticket.onclick = function(){OpenTicketNav(openticket.id);};
    closedwrap.appendChild(openticket);

    var settings = document.createElement('div');
    settings.style.position = "absolute";
    settings.style.top = "8px";
    settings.style.right = "16px";
    settings.style.fontSize = "18px";
    window.appendChild(settings);

    var innersettings = document.createElement('div');
    innersettings.className = 'hide';
    innersettings.style.position = 'absolute';
    innersettings.style.right = '16px';
    innersettings.style.top = '0px';
    innersettings.style.backgroundColor = 'rgb(255, 255, 255)';
    innersettings.style.borderLeft = '1px solid rgba(0, 0, 0, 0.3)';
    innersettings.style.borderTop = '1px solid rgba(0, 0, 0, 0.3)';
    innersettings.style.width = '16px';
    settings.appendChild(innersettings);

    var ticketprio = document.createElement('p');
    ticketprio.className = 'prio-info';
    ticketprio.innerHTML = '<b>' + "PRIO" + `${ticketorder}` + '</b>';
    ticketprio.id = ticketno;
    tickethead.appendChild(ticketprio);

    return (window);
}

function LastTicketSent(lastticket) {
    if (lastticket === true) {
       scrollToSavedPosition();
    }
}
function builddropdown(ticketno,salespersoncode){
    var select = document.createElement("div");
    select.className = 'mySelect-wrap';

    var selectList = document.createElement("select");
    selectList.id = ticketno;
    selectList.className = ticketno;
    selectList.onchange = function () { ChangeSalesPerson(selectList.className, selectList); };
    select.appendChild(selectList);

    var option = document.createElement("option");
    option.value = salespersoncode;
    option.text = salespersoncode;
    selectList.appendChild(option);

    for (var i = 0; i < globalsalesperson.length; i++) {
        var option = document.createElement("option");
        if (globalsalesperson[i] != salespersoncode) {
            option.value = globalsalesperson[i];
            option.text = globalsalesperson[i];
            selectList.appendChild(option);
        }   
    }
    return (select);
}

function buildbuttons(ticketno) {

    var buttonwrap = document.createElement("div");
    buttonwrap.className = 'ticket-buttons-wrap';

    var upbutton = document.createElement("button");
    upbutton.className = 'up';
    upbutton.style.alignItems = 'center';
    upbutton.style.display = 'flex';
    upbutton.style.justifyContent = 'center';
    upbutton.style.padding = '8px';
    upbutton.style.marginLeft = 'auto';
    upbutton.style.marginRight = '0px';
    upbutton.id = ticketno;
    upbutton.onclick = function () { RenumberTicketOrderUp(upbutton.id);};
    buttonwrap.appendChild(upbutton);

    var upinner = document.createElement('div');
    upinner.style.backgroundColor = 'transparent';
    upinner.style.borderLeft = '1px solid rgba(0, 0, 0, 0.3)';
    upinner.style.borderTop = '1px solid rgba(0, 0, 0, 0.3)';
    upinner.style.transform = 'translateY(25%) rotate(45deg)';
    upinner.style.height = '12px';
    upinner.style.width = '12px';
    upbutton.appendChild(upinner);

    var downbutton = document.createElement("button");
    downbutton.className = 'down';
    downbutton.style.alignItems = 'center';
    downbutton.style.display = 'flex';
    downbutton.style.justifyContent = 'center';
    downbutton.style.padding = '8px';
    downbutton.style.marginLeft = 'auto';
    downbutton.style.marginRight = '0px';
    downbutton.id = ticketno;
    downbutton.onclick = function () { RenumberTicketOrderDown(downbutton.id); };
    buttonwrap.appendChild(downbutton);


    var downinner = document.createElement('div');
    downinner.style.backgroundColor = 'transparent';
    downinner.style.borderBottom = '1px solid rgba(0, 0, 0, 0.3)';
    downinner.style.borderRight = '1px solid rgba(0, 0, 0, 0.3)';
    downinner.style.transform = 'translateY(-25%) rotate(45deg)';
    downinner.style.height = '12px';
    downinner.style.width = '12px';
    downbutton.appendChild(downinner);

    return (buttonwrap);
}

function OpenTicketNav(ticketno) {
   
    data = ticketno;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("OpenTicket", [data]);
}


function ChangeSalesPerson(ticketno, selectList) {

    var x = selectList.value;
    data = ticketno + '|' + x;

    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("ChangeSalesPerson", [data]);
}

function RenumberTicketOrderUp(ticketno) {

    var torderup = document.getElementById(ticketno).getElementsByClassName('prio-info');
    temp = torderup.item(0).textContent;

    data = ticketno + '|' + temp + '|' + "1";
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("ChangeTicketOrder", [data]);

}

function RenumberTicketOrderDown(ticketno) {
    //debugger;
    var torderdown = document.getElementById(ticketno).getElementsByClassName('prio-info');
    prio = torderdown.item(0).textContent;

    data = ticketno + '|' + prio + '|' + "2";
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("ChangeTicketOrder", [data]);
    // setTimeout(window.location.reload(), 100);
}

function RenumberTicketOrderDD(currTicket, nextTicket, oldUser,newUser) {

    nextPrio = -1;
    if (nextTicket != -1){
        var torderdown = document.getElementById(nextTicket).getElementsByClassName('hide prio-info');
        if (torderdown.length > 0){
            nextPrio = torderdown.item(0).id;
        }
    }

    data = currTicket + '|' + nextPrio + '|' + oldUser.substring(2) + '|' + newUser.substring(2);
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("ChangeTicketOrderDD", [data]);
}

var hide = false;
function toggleAllContainerViews() {
    hide = !hide;
    $('div.closed-wrap').each(function(i,e){
        var ticketno =  e.id;
        if(hide){
            if($(e).hasClass('hide')){
                $(e).removeClass('hide');
                setCookie(ticketno, false ,1);
            }
        } else {
            if(!$(e).hasClass('hide')){
                $(e).addClass('hide');
                setCookie(ticketno, true ,1);
            }
        }
    }); 
}

function toggleContainerView(ticketno) {
    $('div.closed-wrap#' + ticketno).toggleClass('hide');

    if($('div.closed-wrap#' + ticketno).hasClass('hide')){
        setCookie(ticketno, true ,1);
    } else {
        setCookie(ticketno, false ,1);
    }
}

function scrollToSavedPosition() {
    var scrollPos = getCookie('Scrollpos')
    if (scrollPos) {
        $('.salesperson-wrap').first().scrollTop(scrollPos);
    }
}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

// --- Drag & Drop
function createDragDrop(SalesPerson) {
       new Sortable(SalesPerson, {
        group: 'shared',
        animation: 250,
        setData: function (dataTransfer, dragEl) {
            dragGhost = dragEl.cloneNode(false);
            dragGhost.classList.add('custom-drag-ghost');
            document.body.appendChild(dragGhost);
            dragEl.style.opacity = 0.2;
        },
        onEnd: function (evt) {
            var itemEl = evt.item;
            dragGhost.parentNode.removeChild(dragGhost);
            itemEl.style.opacity = 1;

        }
    })
}

function dragover(this_record) {
    var currTicket =  $(this_record).closest('div').attr('id');
    var newSalesperson = this_record.offsetParent.id;
    
    var posAfterDrag = $(this_record).offset();
    if (posBeforeDrag.top != posAfterDrag.top || posBeforeDrag.left != posAfterDrag.left) {
        if ($(this_record).is(':last-child') == true) {
            nextticket = '-1';
            RenumberTicketOrderDD(currTicket, nextticket,SalesPersonbeforeDrag,newSalesperson);
        } else {
            var temp1 = $(this_record).next();
            var tempnextticket = temp1[0].id;
            nextticket = tempnextticket;
            RenumberTicketOrderDD(currTicket, nextticket,SalesPersonbeforeDrag,newSalesperson);
        }
    } 
  
}

function dragstart(before_drag) {
    posBeforeDrag = $(before_drag).offset();
    currTicket = $(before_drag).closest('div').attr('id');
    SalesPersonbeforeDrag = before_drag.offsetParent.id;
}

function ClearForReload(){
    setCookie('Scrollpos',$('.salesperson-wrap').first().scrollTop(),1);
    document.getElementById("controlAddIn").innerHTML = '';
}