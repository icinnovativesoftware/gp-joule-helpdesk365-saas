controladdin "ICI Ticket Board"
{
    // The Scripts property can reference both external and local scripts.
    Scripts = 'ALCode/TicketBoard/Scripts/Sortable.js',
              'ALCode/TicketBoard/Scripts/jquery-2.1.4.min.js',
              'ALCode/TicketBoard/Scripts/IControlAddIn.js';


    // The StartupScript is a special script that the webclient calls once the page is loaded.
    StartupScript = 'ALCode/TicketBoard/Scripts/Startup.js';

    // Images and StyleSheets can be referenced in a similar fashion.
    StyleSheets = 'ALCode/TicketBoard/Style/style.css';

    Images = 'ALCode/TicketBoard/Images/folder-open-regular.svg',
             'ALCode/TicketBoard/Images/folder-regular.svg',
             'ALCode/TicketBoard/Images/folder-solid.svg';

    // The layout properties define how control add-in are displayed on the page.
    VerticalShrink = true;
    VerticalStretch = true;

    HorizontalStretch = true;
    HorizontalShrink = true;

    RequestedHeight = 700;
    RequestedWidth = 300;

    // The procedure declarations specify what JavaScript methods could be called from AL.
    // AL -> JS
    procedure SetTicketInfo(ticketno: Text; salespersoncode: Text; ticketorder: Text; description: Text; company: Text; companycontact: Text; creationdate: Text; lastactiondate: Text; ticketcategory: Text; allsalesperson: Text; colorCode: Text);
    procedure InitSalesPersons(initsalesperson: Text; amount: text; BaseColor: Text);
    procedure LastTicketSent(lastticket: Boolean);

    procedure ClearForReload();

    // The event declarations specify what callbacks could be raised from JavaScript by using the webclient API:
    // Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('CountryClicked', [{country: 'M}])
    //event CountryClicked(Country: JsonObject);
    // JS -> AL

    event ControlReady();
    event TicketLoaded();
    event ChangeTicketOrderDD(Data: Text);
    event OpenTicket(Data: Text);
    event ChangeSalesPerson(Data: Text);
    event ChangeTicketOrder(Data: Text);

}
