page 56339 "ICI Ticket Board"
{
    UsageCategory = Tasks;
    Caption = 'Ticket Board', Comment = 'de-DE=Ticketboard';
    PageType = StandardDialog;
    DataCaptionExpression = Caption;
    ApplicationArea = All;
    // SourceTable = "ICI Support Ticket";

    layout
    {
        area(content)
        {
            usercontrol(ICTicketBoard; "ICI Ticket Board")
            {
                ApplicationArea = All;

                trigger ControlReady()
                begin
                    LoadAddin();
                end;

                trigger TicketLoaded()
                var
                    ICISupportTicketCategory: Record "ICI Support Ticket Category";
                    ColorCode: Text;
                begin
                    SupportSetup.GET();
                    SupportTicket.SETCURRENTKEY("Ticket Board Order");
                    SupportTicket.SETFILTER("Support User ID", allSalesperson);
                    SupportTicket.SETFILTER("Ticket State", '<>%1', SupportTicket."Ticket State"::Closed);

                    SupportTicket.ASCENDING;

                    IF SupportTicket.FindSet() THEN
                        REPEAT

                            // Get Color Code depending on Category 1 & 2
                            IF ICISupportTicketCategory.GET(SupportTicket."Category 1 Code") then
                                ColorCode := ICISupportTicketCategory."Ticketboard - Color";
                            IF SupportTicket."Category 2 Code" <> '' then
                                IF ICISupportTicketCategory.GET(SupportTicket."Category 2 Code") then
                                    ColorCode := ICISupportTicketCategory."Ticketboard - Color";

                            CurrPage.ICTicketBoard.SetTicketInfo(SupportTicket."No.",
                                                                  SupportTicket."Support User ID",
                                                                  FORMAT(SupportTicket."Ticket Board Order"),
                                                                  SupportTicket.Description,
                                                                  SupportTicket."Comp. Contact Name",
                                                                  SupportTicket."Curr. Contact Name",
                                                                  FORMAT(SupportTicket."Created On"),
                                                                  FORMAT(SupportTicket."Last Activity Date"),
                                                                  SupportTicket."Category 1 Code",
                                                                  allSalesperson,
                                                                  ColorCode
                                                                  );
                        UNTIL SupportTicket.NEXT() = 0;


                    CurrPage.ICTicketBoard.LastTicketSent(TRUE);
                end;

                trigger ChangeTicketOrderDD(Data: Text)
                var
                    CurrSupportTicket: Record "ICI Support Ticket";
                    LookupSupportTicket: Record "ICI Support Ticket";
                    TempNameValueBuffer: Record "Name/Value Buffer" temporary;
                    ICITicketBoardMgt: Codeunit "ICI Ticket Board Mgt.";
                    TOrder: Integer;
                    NextPrio: Text;
                    NextPrioInt: Integer;
                    CurrTicketNo: Code[20];
                    CurrSupportUserID: Code[50];
                    NextSupportUserID: Code[50];
                begin
                    CurrTicketNo := COPYSTR(Explode(Data, '|'), 1, 20);
                    NextPrio := Explode(Data, '|');
                    EVALUATE(NextPrioInt, NextPrio);

                    CurrSupportUserID := COPYSTR(Explode(Data, '|'), 1, 50);
                    NextSupportUserID := COPYSTR(Explode(Data, '|'), 1, 50);

                    CurrSupportTicket.GET(CurrTicketNo);
                    CurrSupportUserID := CurrSupportTicket."Support User ID";
                    if NextPrio <> '-1' THEN begin // CurrTicket wurde vor NextTicket eingeordnet

                        LookupSupportTicket.SetRange("Support User ID", NextSupportUserID);
                        LookupSupportTicket.SetFilter("Ticket State", '<>%1', LookupSupportTicket."Ticket State"::Closed);
                        LookupSupportTicket.SetFilter("No.", '<>%1', CurrSupportTicket."No.");
                        LookupSupportTicket.SetCurrentKey("Ticket Board Order");
                        LookupSupportTicket.SetFilter("Ticket Board Order", '>=%1', NextPrioInt); // Alle Tickets bei dem Zielbenutzer mit größerem TicketBoardOrder suchen und erhöhen

                        TOrder := NextPrioInt;
                        if LookupSupportTicket.FindSet() THEN
                            repeat
                                TOrder += 1;
                                IF LookupSupportTicket."Ticket Board Order" <> TOrder THEN
                                    TempNameValueBuffer.AddNewEntry(LookupSupportTicket."No.", FORMAT(TOrder));
                            until LookupSupportTicket.Next() = 0;
                        IF TempNameValueBuffer.FindSet() THEN
                            repeat
                                Evaluate(TOrder, TempNameValueBuffer.GetValue());
                                CLEAR(LookupSupportTicket);
                                LookupSupportTicket.GET(TempNameValueBuffer.Name);
                                LookupSupportTicket."Ticket Board Order" := TOrder;
                                LookupSupportTicket.Modify();
                            until TempNameValueBuffer.Next() = 0;

                        CurrSupportTicket.VALIDATE("Support User ID", NextSupportUserID);
                        CurrSupportTicket."Ticket Board Order" := NextPrioInt; // Ticket bei zielverkäufer eintragen mit TicketBoardOrder
                        CurrSupportTicket.Modify();
                        if CurrSupportUserID <> NextSupportUserID THEN // Ticket Wurde an einen neuen Benutzer gezogen -> Alten benutzer "renumbern"
                            ICITicketBoardMgt.DefragTicketOrder(CurrSupportUserID);

                    end else Begin // CurrTicket wurde an Letzte stelle geschoben
                        CurrSupportTicket.Validate("Support User ID", NextSupportUserID); // Setzt direkt an letzte stelle des benutzers (gleich - oder anderer)
                        CurrSupportTicket.Modify();
                        ICITicketBoardMgt.DefragTicketOrder(CurrSupportUserID);
                        ICITicketBoardMgt.DefragTicketOrder(NextSupportUserID);

                        // LookupSupportTicket.SetRange("Support User ID", CurrSupportTicket."Support User ID");
                        // LookupSupportTicket.SetFilter("Process State", '<>%1', SupportTicket."Process State"::Closed);
                        // LookupSupportTicket.SetCurrentKey("Ticket Board Order");
                        // LookupSupportTicket.FindLast();
                        // MaxTicketOrder := LookupSupportTicket."Ticket Board Order"; // Größte TicketBoardOrder suchen

                        // CurrSupportTicket."Ticket Board Order" := MaxTicketOrder + 1; // und +1 setzen
                        // CurrSupportTicket.Modify();

                    end;

                    LoadAddin();
                end;

                trigger OpenTicket(Data: Text)
                var
                    ICISUpportTicket: Record "ICI Support Ticket";
                    SupportTicketCard: Page "ICI Support Ticket Card";
                    TicketNo: Code[20];
                begin
                    TicketNo := CopyStr(Explode(Data, '|'), 1, 20);
                    CLEAR(ICISUpportTicket);
                    ICISUpportTicket.SETFILTER("No.", TicketNo);
                    IF ICISUpportTicket.FINDFIRST() THEN BEGIN
                        SupportTicketCard.SETRECORD(ICISUpportTicket);
                        SupportTicketCard.RUN();
                    END;
                end;

                trigger ChangeSalesPerson(Data: Text)
                var
                    ICISUpportTicket: Record "ICI Support Ticket";
                    ICITicketBoardMgt: Codeunit "ICI Ticket Board Mgt.";
                    NewSalesPerson: Code[50];
                    TicketNo: Code[20];
                    CurrSP: Code[50];
                begin
                    TicketNo := CopyStr(Explode(Data, '|'), 1, 20);
                    NewSalesPerson := CopyStr(Explode(Data, '|'), 1, 50);
                    ICISUpportTicket.GET(TicketNo);
                    CurrSP := ICISUpportTicket."Support User ID";
                    IF CurrSP = NewSalesPerson THEN
                        exit;

                    ICISUpportTicket.Validate("Support User ID", NewSalesPerson);
                    ICISUpportTicket.MODIFY();
                    ICITicketBoardMgt.DefragTicketOrder(CurrSP);
                    LoadAddin();
                end;

                trigger ChangeTicketOrder(Data: Text)
                var
                // ICISupportTicket: Record "ICI Support Ticket";
                // ICISupportTicket2: Record "ICI Support Ticket";
                // TicketNo: Code[20];
                // TempCurrTicketPrio: Code[10];
                // UpOrDown: Code[10];
                // CurrTicketPrio_: Code[10];
                // CurrTicketPrio: Integer;
                // CurrSP: Code[50];
                // tempTicketOrder: Integer;
                // tempTicketOrder2: Integer;
                // TicketUpErr: Label 'Ticket can''t be changed in this direction!', Comment = 'de-DE=Ticket kann nicht mehr weiter nach oben verschoben werden.';
                // TicketDownErr: Label 'Ticket can''t be pushed in this direction!', Comment = 'de-DE=Ticket kann nicht mehr weiter nach unten verschoben werden.';
                begin
                    // TicketNo := CopyStr(Explode(Data, '|'), 1, 20);
                    // TempCurrTicketPrio := CopyStr(Explode(Data, '|'), 1, 10);
                    // UpOrDown := CopyStr(Explode(Data, '|'), 1, 10);
                    // CurrTicketPrio_ := DELCHR(TempCurrTicketPrio, '=', 'PRIO');
                    // EVALUATE(CurrTicketPrio, CurrTicketPrio_);

                    LoadAddin();
                end;

            }
        }
    }

    actions
    {
    }

    trigger OnInit()
    begin
        Caption := '';
    end;

    var
        SupportTicket: Record "ICI Support Ticket";
        SupportSetup: Record "ICI Support Setup";
        allSalesperson: Text;
        Caption: Text;

    procedure Explode(var TextRecord: Text; Delimiter: Text[30]) TextField: Text[250]
    var
        Pos: Integer;
    begin
        Pos := STRPOS(TextRecord, Delimiter);
        IF Pos <> 0 THEN BEGIN
            TextField := (COPYSTR(TextRecord, 1, Pos - 1));
            TextRecord := (COPYSTR(TextRecord, Pos + STRLEN(Delimiter)));
        END ELSE BEGIN
            TextField := COPYSTR(TextRecord, 1, 250);
            TextRecord := '';
        END;
    end;

    local procedure CalcTotalTicketsSalesperson(Salesperson: Code[50]): Text
    var
        ICISupportTicket: Record "ICI Support Ticket";
        CurrAmount: Text;
    begin
        ICISupportTicket.SETFILTER("Support User ID", Salesperson);
        ICISupportTicket.SETFILTER("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
        IF ICISupportTicket.FINDSET() THEN
            CurrAmount := FORMAT(ICISupportTicket.COUNT);
        EXIT(CurrAmount);
    end;

    local procedure LoadAddin()
    var
        ICISupportSetup: Record "ICI Support Setup";
        SupportUser: Record "ICI Support User";
        Length: Integer;
        tempSalesPerson: Text;
        tempAmount: Text;
        allAmounts: Text;
        LengthAmount: Integer;
    begin
        ICISupportSetup.GET();
        SupportUser.SETRANGE("Ticket Board Active", TRUE);
        //filterung auf aktuellen TeamCode mit einbauen
        IF SupportUser.FINDSET() THEN BEGIN
            REPEAT
                tempAmount := tempAmount + CalcTotalTicketsSalesperson(SupportUser."User ID");
                tempAmount := tempAmount + '|';
                tempSalesPerson := tempSalesPerson + SupportUser."User ID";
                tempSalesPerson := tempSalesPerson + '|';
            UNTIL SupportUser.NEXT() = 0;
            LengthAmount := STRLEN(tempAmount);
            allAmounts := DELSTR(tempAmount, LengthAmount);
            Length := STRLEN(tempSalesPerson);
            allSalesperson := DELSTR(tempSalesPerson, Length);
        END;

        CurrPage.ICTicketBoard.ClearForReload();
        CurrPage.ICTicketBoard.InitSalesPersons(allSalesperson, allAmounts, ICISupportSetup."Ticketboard - Base Color");
    end;

}
