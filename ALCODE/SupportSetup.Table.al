table 56276 "ICI Support Setup"
{
    Caption = 'Support Setup', Comment = 'de-DE=HelpDesk Einrichtung';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Code; Code[10])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = SystemMetadata;
        }
        field(10; "Created On"; Date)
        {
            Editable = false;
            DataClassification = SystemMetadata;
            Caption = 'Created On', Comment = 'de-DE=Erstellt am';
        }
        field(11; "Next Scheduled License Check"; Date)
        {
            DataClassification = SystemMetadata;
            Caption = 'Next Scheduled License Check', Comment = 'de-DE=Nächste Lizenzprüfung am';
            Editable = false;
        }
        field(12; "Support Ticket Nos."; Code[20])
        {
            Caption = 'Support Ticket Nos.', Comment = 'de-DE=Ticketnummern';
            TableRelation = "No. Series";
            DataClassification = CustomerContent;
        }
        field(20; "Document Integration"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Document Integration', Comment = 'de-DE=Belegintegration';
        }
        field(21; "Service Integration"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Service Integration', Comment = 'de-DE=Serviceintegration';
        }
        field(22; "Time Registration"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Time Registration', Comment = 'de-DE=Zeiterfassung';
        }
        field(23; "Portal Integration"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'Portal Integration', Comment = 'de-DE=Kundenportal';
        }
        field(24; "Task Integration"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Task Integration', Comment = 'de-DE=Aufgabenintegration';
        }
        field(25; "License Module 6"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'Guest Form', Comment = 'de-DE=Gastformular';
        }
        field(26; "License Module 7"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'License Module 7', Comment = 'de-DE=Modul 7 lizenziert';
        }
        field(27; "License Module 8"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'License Module 8', Comment = 'de-DE=Modul 8 lizenziert';
        }
        field(28; "License Module 9"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'License Module 9', Comment = 'de-DE=Modul 9 lizenziert';
        }
        field(29; "License Module 10"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            Caption = 'License Module 10', Comment = 'de-DE=Modul 10 lizenziert';
        }
        field(30; "Customer Template Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Customer Template Code', Comment = 'de-DE=Debitorenvorlagencode';
            TableRelation = "Customer Templ.";
        }

        field(60; "Default Ticket Process"; Code[20])
        {
            Caption = 'Default Ticket Process', Comment = 'de-DE=Vorbelegung Ticketprozess';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Process";
        }

        field(61; "Time Registration Subtype"; Option)
        {
            Caption = 'Time Registration Type', Comment = 'de-DE=Zeiterfassungsart';
            DataClassification = CustomerContent;
            OptionMembers = Item,Resource;
            OptionCaption = 'Item,Resource', Comment = 'de-DE=Artikel,Ressource';
        }
        field(62; "Time Registration No."; Code[20])
        {
            Caption = 'Time No.', Comment = 'de-DE=Zeiterfassungsnr.';
            DataClassification = CustomerContent;
            TableRelation = IF ("Time Registration Subtype" = const(Item)) Item else
            if ("Time Registration Subtype" = const(Resource)) Resource;
        }
        field(63; "Time Reg. Unit of Measure"; Code[10])
        {
            Caption = 'Time Registration Unit of Measure', Comment = 'de-DE=Zeiterfassungseinheit';
            DataClassification = CustomerContent;
            TableRelation = "Unit of Measure";
        }
        field(64; "Cue Indicator Low"; Integer)
        {
            Caption = 'Cue Indicator Low', Comment = 'de-DE=Stapelindikatoren untere Grenze';
            DataClassification = CustomerContent;
            InitValue = 5;
        }
        field(65; "Cue Indicator High"; Integer)
        {
            Caption = 'Cue Indicator Low', Comment = 'de-DE=Stapelindikatoren obere Grenze';
            DataClassification = CustomerContent;
            InitValue = 10;
        }

        field(66; "Default Department Code"; Code[10])
        {
            DataClassification = CustomerContent;
            TableRelation = "ICI Department";
            Caption = 'Default Department Code', Comment = 'de-DE=Vorbelegung Abteilungscode';
        }

        field(67; "Sales Payoff Type"; Enum "Sales Line Type")
        {
            DataClassification = CustomerContent;
            Caption = 'Payoff Type', Comment = 'de-DE=Abrechnungsart';
            ValuesAllowed = " ", Item, "G/L Account", Resource;

            trigger OnValidate()
            begin
                IF "Sales Payoff Type" <> xRec."Sales Payoff Type" THEN
                    "Sales Payoff No." := '';
                UpdateUsersForSalesPayoff();
            end;
        }
        field(68; "Sales Payoff No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Payoff No.', Comment = 'de-DE=Abrechnungsnr.';
            TableRelation = IF ("Sales Payoff Type" = CONST(" ")) "Standard Text"
            ELSE
            IF ("Sales Payoff Type" = CONST("G/L Account")) "G/L Account"
            ELSE
            IF ("Sales Payoff Type" = CONST("Resource")) Resource
            ELSE
            IF ("Sales Payoff Type" = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF ("Sales Payoff Type" = CONST("Charge (Item)")) "Item Charge"
            ELSE
            IF ("Sales Payoff Type" = CONST(Item)) Item;
            ValidateTableRelation = false;
            trigger OnValidate()
            begin
                UpdateUsersForSalesPayoff();
            end;
        }
        field(69; "Link to Portal"; Text[250])
        {
            Caption = 'Link to Portal', Comment = 'de-DE=Link zum Kundenportal';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;

            trigger OnValidate()
            begin
                if Rec."Link to Guest Form" = '' then
                    Rec."Link to Guest Form" := (Format(Rec."Link to Portal") + 'support/de/Tickets/?action=support_instant_ticket');
            end;
        }
        field(70; "Show Filetypes as Image"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Show Filetypes as Image', Comment = 'de-DE=Dateitypen als Bilder anzeigen';
            //InitValue = 'apng.avif.gif.jpg.jpeg.jfif.pjpeg.pjp.png.svg.webp.bmp.ico.cur.tif.tiff'; /// Taken from here: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
        }
        field(71; "Default Portal Cue Set Code"; Code[20])
        {
            Caption = 'Default Portal Cue Set Code', Comment = 'de-DE=Kundenportalkachelsetcode';
            DataClassification = CustomerContent;
            TableRelation = "ICI Portal Cue Set";
        }
        field(72; "Closing Interaction Template"; Code[10])
        {
            Caption = 'Ticket Closing Interaction Template', Comment = 'de-DE=Aktivitätenvorlage für Ticket Schließen';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
        }
        field(73; "Letter Interaction Template"; Code[20])
        {
            Caption = 'Letter Interaction Template', Comment = 'de-DE=Brief Aktivitätenvorlage';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
        }
        field(74; "Phone Interaction Template"; Code[20])
        {
            Caption = 'Phone Interaction Template', Comment = 'de-DE=Telefonat Aktivitätenvorlage';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
        }
        field(75; "Meeting Interaction Template"; Code[20])
        {
            Caption = 'Meeting Interaction Template', Comment = 'de-DE=Termin Aktivitätenvorlage';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
        }
        field(76; "Color - Contact Message"; Text[20])
        {
            Caption = 'Color - Contact Message', Comment = 'de-DE=Farbe Kontaktnachricht';
        }
        field(77; "Color - User Message"; Text[20])
        {
            Caption = 'Color - User Message', Comment = 'de-DE=Farbe Benutzernachricht';
        }
        field(78; "Color - Internal Message"; Text[20])
        {
            Caption = 'Color - Internal Message', Comment = 'de-DE=Farbe interne Nachricht';
        }
        field(79; "Ticketboard - Base Color"; Text[20])
        {
            Caption = 'Ticketboard - Base Color', Comment = 'de-DE=Grundfarbe Ticketboard';
        }
        field(80; "Guest Contact Company No."; Code[20])
        {
            Caption = 'Guest Contact Company No.', Comment = 'de-DE=Gastformular Unternehmenskontaktnr.';
            DataClassification = CustomerContent;
            TableRelation = Contact where(Type = const(Company));

            trigger OnValidate()
            var
                ICISupportTicket: Record "ICI Support Ticket";
                CanNotChangeErrMsg: Label 'Guest Contact Company No. can not be changed, while there are still active Tickets for it!', Comment = 'de-DE=Die Sammelkontaktnr. kann nicht geändert werden, solange noch aktive Tickets für sie bestehen!en-US=Guest Contact Company No. can not be changed, while there are still active Tickets for it!';
            begin
                if xRec."Guest Contact Company No." <> Rec."Guest Contact Company No." then begin
                    ICISupportTicket.SetRange("Company Contact No.", xRec."Guest Contact Company No.");
                    ICISupportTicket.SetFilter("Ticket State", '<>%1', Enum::"ICI Ticket State"::Closed);
                    if ICISupportTicket.FindLast() then
                        Error(CanNotChangeErrMsg);
                end;
            end;
        }
        field(81; "Guest Contact Company Name"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup(Contact.Name Where("No." = FIELD("Guest Contact Company No.")));
            Caption = 'Guest Contact Company Name', Comment = 'de-DE=Gastformular Unternehmensname';
        }
        field(82; "Link to Guest Form"; Text[250])
        {
            Caption = 'Link to GuestForm', Comment = 'de-DE=Link zu Gastformular';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(83; "Communication - Default Intern"; Boolean)
        {
            Caption = 'Communication - Default Intern', Comment = 'de-DE=Kommunikation std. Intern';
            DataClassification = CustomerContent;
        }
        field(84; "Portal Sync. No Of Recs."; Integer)
        {
            Caption = 'Portal Sync. No Of Recs.', Comment = 'de-DE=Anzahl Datensätze pro Request';
            DataClassification = CustomerContent;
        }
        field(85; "Portal - Change Data Allowed"; Boolean)
        {
            Caption = 'Change Data Allowed', Comment = 'de-DE=Stammdaten ändern erlaubt';
            DataClassification = CustomerContent;
        }
        field(87; "Max. Login Failures"; Integer)
        {
            Caption = 'Max. Login Failures', Comment = 'de-DE=Max. Login Fehlversuche';
            DataClassification = CustomerContent;
            InitValue = 5;
        }
        field(88; "Url - AGB"; Text[100])
        {
            Caption = 'Url - AGB', Comment = 'de-DE=Url - AGB';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(89; "Url - DSGVO"; Text[100])
        {
            Caption = 'Url - DSGVO', Comment = 'de-DE=Url - DSGVO';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(90; "Url - Imprint"; Text[100])
        {
            Caption = 'Url - Imprint', Comment = 'de-DE=Url - Impressum';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(91; "Password Use Numbers"; Boolean)
        {
            Caption = 'Password Use Numbers', Comment = 'de-DE=Password benötigt Zahlen';
            DataClassification = CustomerContent;
        }
        field(92; "Password Use Lowercase"; Boolean)
        {
            Caption = 'Password Use Lowercase', Comment = 'de-DE=Password benötigt Kleinbuchstaben';
            DataClassification = CustomerContent;
        }
        field(93; "Password Use Uppercase"; Boolean)
        {
            Caption = 'Password Use Uppercase', Comment = 'de-DE=Password benötigt Großbuchstaben';
            DataClassification = CustomerContent;
        }
        field(94; "Password Use Special"; Boolean)
        {
            Caption = 'Password Use Special', Comment = 'de-DE=Password benötigt Sonderzeichen';
            DataClassification = CustomerContent;
        }
        field(113; "Webservice Language Code"; Code[10])
        {
            Caption = 'Webservice Language Code', Comment = 'de-DE=Sprachcode';
            InitValue = DEU;
            TableRelation = Language;
        }

        field(114; "Cue 1 - Category 1 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 1 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
        }
        field(115; "Cue 1 - Category 2 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 1 - Category 2 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 1 - Category 1 Code"));
        }
        field(116; "Cue 2 - Category 1 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 2 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
        }
        field(117; "Cue 2 - Category 2 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 2 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 2 - Category 1 Code"));
        }
        field(118; "Cue 3 - Category 1 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 3 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
        }
        field(119; "Cue 3 - Category 2 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 3 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 3 - Category 1 Code"));
        }
        field(120; "Cue 4 - Category 1 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 4 - Category 1 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
        }
        field(121; "Cue 4 - Category 2 Code"; Code[50])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cue 4 - Category 2 Filter';
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 4 - Category 1 Code"));
        }

        field(122; "Cue 1 - Process Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Process Code', Comment = 'de-DE=Prozesscode';
            TableRelation = "ICI Support Process";
            trigger OnValidate()
            begin
                // Process Stage löschen, wenn Prozess geleert wurde
                IF "Cue 1 - Process Code" = '' then
                    "Cue 1 - Process Stage" := 0
            end;
        }
        field(123; "Cue 1 - Process Stage"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Stage', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Cue 1 - Process Code"));
            BlankZero = true;
            trigger OnValidate()
            begin
                TestField("Cue 1 - Process Code");
            end;
        }
        field(124; "Cue 2 - Process Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Process Code', Comment = 'de-DE=Prozesscode';
            TableRelation = "ICI Support Process";
            trigger OnValidate()
            begin
                // Process Stage löschen, wenn Prozess geleert wurde
                IF "Cue 2 - Process Code" = '' then
                    "Cue 2 - Process Stage" := 0
            end;
        }
        field(125; "Cue 2 - Process Stage"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Stage', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Cue 2 - Process Code"));
            BlankZero = true;
            trigger OnValidate()
            begin
                TestField("Cue 2 - Process Code");
            end;
        }
        field(126; "Cue 3 - Process Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Process Code', Comment = 'de-DE=Prozesscode';
            TableRelation = "ICI Support Process";
            trigger OnValidate()
            begin
                // Process Stage löschen, wenn Prozess geleert wurde
                IF "Cue 3 - Process Code" = '' then
                    "Cue 3 - Process Stage" := 0
            end;
        }
        field(127; "Cue 3 - Process Stage"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Stage', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Cue 3 - Process Code"));
            BlankZero = true;
            trigger OnValidate()
            begin
                TestField("Cue 3 - Process Code");
            end;
        }
        field(128; "Cue 4 - Process Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Process Code', Comment = 'de-DE=Prozesscode';
            TableRelation = "ICI Support Process";
            trigger OnValidate()
            begin
                // Process Stage löschen, wenn Prozess geleert wurde
                IF "Cue 4 - Process Code" = '' then
                    "Cue 4 - Process Stage" := 0
            end;
        }
        field(129; "Cue 4 - Process Stage"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Stage', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Cue 4 - Process Code"));
            BlankZero = true;
            trigger OnValidate()
            begin
                TestField("Cue 4 - Process Code");
            end;
        }

        field(130; "Person Mandatory"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Person Mandatory', Comment = 'de-DE=Personenkontakt Pflichtfeld';
            InitValue = true;
        }
        field(131; "Token Duration"; Duration)
        {
            Caption = 'Token Duration', Comment = 'de-DE=Token Gültigkeit';
            DataClassification = CustomerContent;
        }
        field(132; "Use Shipment in Ticket"; Boolean)
        {
            Caption = 'Use Shipment in Ticket', Comment = 'de-DE=Lieferadresse in Ticket';
            DataClassification = CustomerContent;
        }
        field(133; "Ticket Category Connection"; Option)
        {
            Caption = 'Ticket Category Connection', Comment = 'de-DE=Kategorieverhalten';
            DataClassification = CustomerContent;
            OptionMembers = "Parent-Child Layers","Two-Layers";
            OptionCaption = 'Parent-Child Layers,Two Layers', Comment = 'de-DE=Ober-Unterkategorie,Zwei Ebenen';
        }
        field(134; "RMA-Report ID"; Integer)
        {
            Caption = 'RMA-Report ID', Comment = 'de-DE=RMA-Berichts-ID';
            DataClassification = CustomerContent;
            TableRelation = AllObjWithCaption."Object ID" WHERE("Object Type" = CONST(Report));
        }
        field(135; "Link to Portal Backend"; Text[250])
        {
            Caption = 'Link to Backend', Comment = 'de-DE=Link zum Kundenportalbackend';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(136; "Allow Ship-To Address modify"; Boolean)
        {
            Caption = 'Allow Ship-To Address modify', Comment = 'de-DE=Lieferadresse änderbar';
            DataClassification = CustomerContent;
        }
        field(137; "Ship-To Address modify"; Option)
        {
            Caption = 'Ship-To Address modify', Comment = 'de-DE=Bei Lieferadressenänderung';
            DataClassification = CustomerContent;
            OptionMembers = "Overwrite Current","Create New if needed";
            OptionCaption = 'Overwrite Current,Create New if needed', Comment = 'de-DE=Aktuelle überschreiben,Neu anlegen';
        }
        field(138; "Cue 1 - Category 1 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
            trigger OnLookup()
            begin
                "Cue 1 - Category 1 Filter" := LookupCategoryFilter('');
            end;
        }
        field(139; "Cue 1 - Category 2 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 1 - Category 1 Filter"));
            trigger OnValidate()
            begin
                TestField("Cue 1 - Category 1 Filter");
            end;

            trigger OnLookup()
            begin
                "Cue 1 - Category 2 Filter" := LookupCategoryFilter("Cue 1 - Category 1 Filter");
            end;
        }
        field(140; "Cue 2 - Category 1 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
            trigger OnLookup()
            begin
                "Cue 2 - Category 1 Filter" := LookupCategoryFilter('');
            end;
        }
        field(141; "Cue 2 - Category 2 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 1 - Category 1 Filter"));
            trigger OnValidate()
            begin
                TestField("Cue 2 - Category 1 Filter");
            end;

            trigger OnLookup()
            begin
                "Cue 2 - Category 2 Filter" := LookupCategoryFilter("Cue 2 - Category 1 Filter");
            end;
        }
        field(142; "Cue 3 - Category 1 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
            trigger OnLookup()
            begin
                "Cue 3 - Category 1 Filter" := LookupCategoryFilter('');
            end;
        }
        field(143; "Cue 3 - Category 2 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 1 - Category 1 Filter"));
            trigger OnValidate()
            begin
                TestField("Cue 3 - Category 1 Filter");
            end;

            trigger OnLookup()
            begin
                "Cue 3 - Category 2 Filter" := LookupCategoryFilter("Cue 3 - Category 1 Filter");
            end;
        }
        field(144; "Cue 4 - Category 1 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
            trigger OnLookup()
            begin
                "Cue 4 - Category 1 Filter" := LookupCategoryFilter('');
            end;
        }
        field(145; "Cue 4 - Category 2 Filter"; Code[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategorie Code';
            ValidateTableRelation = false;
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = FIELD("Cue 1 - Category 1 Filter"));
            trigger OnValidate()
            begin
                TestField("Cue 4 - Category 1 Filter");
            end;

            trigger OnLookup()
            begin
                "Cue 4 - Category 2 Filter" := LookupCategoryFilter("Cue 4 - Category 1 Filter");
            end;
        }
        field(146; "Portal - Change Pass. allowed"; Boolean)
        {
            Caption = 'Portal - Change Pass. allowed', Comment = 'de-DE=Password ändern erlaubt';
            DataClassification = CustomerContent;
        }
        field(147; "First Supportuser Allocation"; Code[50])
        {
            DataClassification = CustomerContent;
            TableRelation = "ICI Support User";
            Caption = 'First Support User Allocation', Comment = 'de-DE=Abwurfbenutzer';
        }
        field(148; "Ticketboard active"; Boolean)
        {
            Caption = 'Ticketboard active', Comment = 'de-DE=Ticketboard';
            DataClassification = CustomerContent;
            InitValue = true;
        }
        field(149; "Send new File as Attachment"; Boolean)
        {
            Caption = 'Send new File as Attachment', Comment = 'de-DE=Neue Datei als Anhang versenden';
            DataClassification = CustomerContent;
        }
        field(150; "Initial Message Mandatory"; Boolean)
        {
            Caption = 'Initial Message Mandatory', Comment = 'de-DE=Initiale Nachricht beim Eröffnen';
            DataClassification = CustomerContent;
        }
        field(151; "CTI Internal Call Prefix"; Code[10])
        {
            Caption = 'CTI Internal Call Prefix', Comment = 'de-DE=Vorwahl - Interne Weiterleitung';
            DataClassification = CustomerContent;
        }
        field(152; "CTI Phone Extension Length"; Integer)
        {
            Caption = 'CTI Phone Extension Length', Comment = 'de-DE=Durchwahl - Länge';
            DataClassification = CustomerContent;
        }
        field(153; "Daylight Savings Time"; Boolean)
        {
            Caption = 'Daylight Savings Time', Comment = 'de-DE=Zeitumstellung berücksichtigen';
            DataClassification = CustomerContent;
        }
        field(154; "Return Service Item"; Boolean)
        {
            Caption = 'Return Service Item', Comment = 'de-DE=Serviceartikel in VK-Reklamation übernehmen';
            DataClassification = CustomerContent;
        }
        field(155; "Link to CMS"; Text[250])
        {
            Caption = 'Link to CMS', Comment = 'de-DE=Link zum CMS';
            DataClassification = CustomerContent;
            ExtendedDatatype = URL;
        }
        field(156; "Service Registration Type"; Option)
        {
            Caption = 'Service Integration Type', Comment = 'de-DE=Erfassungsart';
            DataClassification = CustomerContent;
            OptionMembers = Choice,SerialNo;
            OptionCaption = 'by Choice,by SerialNo.', Comment = 'de-DE=Auswahl,Seriennr.';
        }
        field(157; "Service Doc. w.o. Serviceitem"; Boolean)
        {
            Caption = 'Service Doc. w.o. Serviceitem', Comment = 'de-DE=Servicebeleg o. Serviceartikel';
            DataClassification = CustomerContent;
        }

        field(160; "Ticket Contact Creation"; boolean)
        {
            caption = 'Ticket Contact Creation', Comment = 'de-DE=Personenkontakt in Ticket erfassen';

        }
        field(180; "Guest Form Text Module Code"; Code[30])
        {
            Caption = 'Guest Form Text Module', Comment = 'de-DE=Gastforumlar Textvorlage|en-US=Guest Form Text Module';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module";
        }
    }

    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    var
    begin
        "Created On" := TODAY;
        "Next Scheduled License Check" := CALCDATE('<2W>', TODAY);
    end;

    local procedure UpdateUsersForSalesPayoff()
    var
        ICISupportUser: Record "ICI Support User";
        InitOptionsLbl: Label 'Update all Users with old Payoff to new Payoff, Update All Users, Don''t Update', COmment = 'de-DE=Alle Benutzer aktualisieren,Alle Benutzer mit alter Abrechnung auf Neue aktualisieren,Benutzer nicht aktualisieren';
        ChosenOption: Integer;
        InstructionLbl: Label 'Please chose, which Supportusers shlould be updated as well', Comment = 'de-DE=Bitte wählen Sie aus, welche Supportbenutzer aktualisiert werden sollen';
        UpdatedLbl: Label 'Updated %1 Supportusers', COmment = '%1=No of User|de-DE=%1 Benutzer aktualisiert';
        NoOfUser: Integer;
    begin
        ChosenOption := StrMenu(InitOptionsLbl, 1, InstructionLbl);
        Case ChosenOption of
            1:
                BEGIN
                    ICISupportUser.ModifyAll("Sales Payoff No.", Rec."Sales Payoff No.");
                    ICISupportUser.ModifyAll("Sales Payoff Type", Rec."Sales Payoff Type");
                    MESSAGE(UpdatedLbl, ICISupportUser.Count());
                END;
            2:
                begin
                    ICISupportUser.SetRange("Sales Payoff No.", xRec."Sales Payoff No.");
                    ICISupportUser.SetRange("Sales Payoff Type", xRec."Sales Payoff Type");
                    NoOfUser := ICISupportUser.Count(); // Zählen vor Modify all - sonst 0
                    ICISupportUser.ModifyAll("Sales Payoff No.", Rec."Sales Payoff No.");
                    ICISupportUser.ModifyAll("Sales Payoff Type", Rec."Sales Payoff Type");
                    MESSAGE(UpdatedLbl, NoOfUser);
                end;
        end;
    end;

    procedure LookupCategoryFilter(Category1Filter: Code[250]): Code[250]
    var
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketCategory: Record "ICI Support Ticket Category";
        NameValueLookup: PAge "Name/Value Lookup";
        FilterString: Text;
    begin
        IF Rec."Ticket Category Connection" = "Ticket Category Connection"::"Parent-Child Layers" then
            IF Category1Filter = '' then
                ICISupportTicketCategory.SetFilter("Parent Category", '=%1', '') // Leer bei Kategorie1Filter leer
            else
                ICISupportTicketCategory.SetFilter("Parent Category", Category1Filter);// Parent Category Filter setzen

        IF Rec."Ticket Category Connection" = "Ticket Category Connection"::"Two-Layers" then
            IF Category1Filter = '' then
                ICISupportTicketCategory.SetRange(Layer, 0) // Ebene 0, wenn keine Category1 angegeben wurde
            else
                ICISupportTicketCategory.SetRange(Layer, 1); // Ebene 1, wenn Category1 gefüllt ist

        IF ICISupportTicketCategory.Findset() THEN
            REPEAT
                TempNameValueBuffer.AddNewEntry(FORMAT(ICISupportTicketCategory.Code), ICISupportTicketCategory.Description);
                NameValueLookup.AddItem(FORMAT(ICISupportTicketCategory.Code), ICISupportTicketCategory.Description);
            UNTIL ICISupportTicketCategory.NEXT() = 0;

        NameValueLookup.EDITABLE(FALSE);
        NameValueLookup.LOOKUPMODE(TRUE);
        IF NameValueLookup.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            NameValueLookup.SetSelectionFilter(TempNameValueBuffer);
            TempNameValueBuffer.MARKEDONLY(TRUE);
            IF TempNameValueBuffer.FINDSET() THEN BEGIN
                CLEAR(FilterString);
                REPEAT
                    IF FilterString = '' THEN
                        FilterString := TempNameValueBuffer.Name
                    ELSE
                        FilterString := (FilterString + '|' + TempNameValueBuffer.Name);
                UNTIL TempNameValueBuffer.NEXT() <= 0;
            END;

            EXIT(COPYSTR(FilterString, 1, 100));
        end;
    end;
}
