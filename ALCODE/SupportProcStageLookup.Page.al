page 56302 "ICI Support Proc. Stage Lookup"
{
    // XXX  - Depricated
    UsageCategory = None;
    Caption = 'ICI Support Process Stage Lookup', Comment = 'de-DE=Ticket Prozessstufen';
    PageType = List;
    SourceTable = "ICI Support Process Stage";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Process Code"; Rec."Process Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Process Code', Comment = 'de-DE=Prozess Code';
                }


                /*field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;

                    ToolTip = 'Language Code', Comment = 'de-DE=Sprachcode';
                }*/
                field(Stage; Rec.Stage)
                {
                    ApplicationArea = All;

                    ToolTip = 'Line No.', Comment = 'de-DE=Zeilennr.';
                }

                field(Description; Rec.Description)
                {
                    ApplicationArea = All;

                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Ticket State"; Rec."Ticket State")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket State', Comment = 'de-DE=Ticketstatus';
                }
                field("E-Mail C Text Module Code"; Rec."E-Mail C Text Module Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Contact Text Module Code', Comment = 'de-DE=Kontakt Textvorlage Code';
                }
                field("E-Mail U Text Module Code"; Rec."E-Mail U Text Module Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail User Text Module Code', Comment = 'de-DE=Benutzer Textvorlage Code';
                }
                field("Previous Allowed"; Rec."Previous Allowed")
                {
                    ApplicationArea = All;
                    ToolTip = 'Previous Allowed', Comment = 'de-DE=Gibt an, ob Ticket in dieser Stufe eine Rückführung in die vorherige Stufe erlauben';
                }
                field("Skip Allowed"; Rec."Skip Allowed")
                {
                    ApplicationArea = All;
                    ToolTip = 'Skip Allowed', Comment = 'de-DE=Gibt an, ob diese Stufe übersprungen werden kann';
                }
                field("Jump To Allowed"; Rec."Jump To Allowed")
                {
                    ApplicationArea = All;
                    ToolTip = 'Jump To Allowed', Comment = 'de-DE=Gibt an, ob von einer beliebiger Sufe zu dieser gesprungen werden darf';
                }
                field("Service Item Mandatory"; Rec."Service Item Mandatory")
                {
                    ApplicationArea = All;
                    ToolTip = 'Service Item Mandatory', Comment = 'de-DE=Im Ticket muss ein Serviceartikel angegeben werden';
                }
                field("Document No. Mandatory"; Rec."Document No. Mandatory")
                {
                    ApplicationArea = All;
                    ToolTip = 'Document No. Mandatory', Comment = 'de-DE=Im Ticket muss ein Beleg angegeben werden';
                }
                field("Reset processing time"; Rec."Reset processing time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Reset processing time', Comment = 'de-DE=Gibt an, ob die Bearbeitungszeit zurückgesetzt werden soll';
                }
            }
        }
    }
}
