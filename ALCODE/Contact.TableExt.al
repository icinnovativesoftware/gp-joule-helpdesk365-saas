tableextension 56288 "ICI Contact" extends Contact
{
    fields
    {
        field(56276; "ICI Support Active"; Boolean)
        {
            Caption = 'ICI Support Active', Comment = 'de-DE=Zugang Aktiv';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                IF "ICI Login" = '' then
                    VALIDATE("ICI Login", "E-Mail");

            end;
        }
        field(56277; "ICI Support Password"; Text[50])
        {
            Caption = 'ICI Support Password', Comment = 'de-DE=Passwort';
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
            ObsoleteState = Removed;
            ObsoleteReason = 'Security Update - Only store Password Hash';
        }
        field(56278; "ICI Login"; Text[100])
        {
            Caption = 'ICI Login', Comment = 'de-DE=Login';
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                lContact: Record Contact;
                ICISupportUser: Record "ICI Support User";
            begin
                ICISupportUser.SetRange(Login, "ICI Login");
                IF ICISupportUser.Count() > 0 then
                    "ICI Login" := "No.";

                lContact.SETRANGE("ICI Login", "ICI Login");
                IF lContact.Count() > 0 then
                    "ICI Login" := "No.";
            end;
        }
        field(56279; "ICI Support Password Hash"; Text[50])
        {
            Caption = 'ICI Support Password Hash', Comment = 'de-DE=Hash';
            DataClassification = CustomerContent;
        }
        // Tansferfields from customer - contact 56280 - Ticketno 
        field(56281; "ICI Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Current Contact No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56282; "ICI Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Current Contact No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56283; "ICI Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Current Contact No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56284; "ICI Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Current Contact No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
        field(56285; "ICI Portal Cue Set Code"; Code[20])
        {
            Caption = 'Portal Cue Set Code', Comment = 'de-DE=Portalkacheln';
            DataClassification = CustomerContent;
            TableRelation = "ICI Portal Cue Set";
        }
        field(56286; "ICI E-Mail TLD"; Code[80])
        {
            Caption = 'ICI E-Mail TLD', Comment = 'de-DE=E-Mail TLD';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(56287; "ICI Source Mailrobot Log"; Integer)
        {
            Caption = 'ICI Source Mailrobot Log', Comment = 'de-DE=Urspr. Mailrobotprotokolleintrag';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Log";
            Editable = false;
        }
        field(56288; "Mailrobot Source Mailbox Code"; Code[30])
        {
            Caption = 'Mailrobot Source Mailbox Code', Comment = 'de-DE=Urspr. Mailrobot Postfach Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Mailbox";
            Editable = false;
        }
        field(56289; "Login Failures"; Integer)
        {
            Editable = false;
            Caption = 'Login Failures', Comment = 'de-DE=Anmeldung Fehlversuche';
            DataClassification = SystemMetadata;
        }
        field(56290; "ICI Comp. Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56291; "ICI Comp. Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56292; "ICI Comp. Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56293; "ICI Comp. Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
        field(56294; "ICI Sales Quote"; Boolean)
        {
            Caption = 'Sales Quote', Comment = 'de-DE=Angebot';
        }
        field(56295; "ICI Sales Order"; Boolean)
        {
            Caption = 'Sales Order', Comment = 'de-DE=Auftrag';
        }
        field(56296; "ICI Sales Invoice"; Boolean)
        {
            Caption = 'Sales Invoice', Comment = 'de-DE=Rechnung';
        }
        field(56297; "ICI Sales Credit Memo"; Boolean)
        {
            Caption = 'Sales Credit Memo', Comment = 'de-DE=Gutschrift';
        }
        field(56298; "ICI Sales Blanket Order"; Boolean)
        {
            Caption = 'Sales Blanket Order', Comment = 'de-DE=Rahmenauftrag';
        }
        field(56299; "ICI Sales Return Order"; Boolean)
        {
            Caption = 'Sales Return Order', Comment = 'de-DE=Reklamation';
        }
        field(56300; "ICI Posted Sales Shipment"; Boolean)
        {
            Caption = 'Posted Sales Shipment', Comment = 'de-DE=Geb. Lieferung';
        }
        field(56301; "ICI Posted Sales Invoice"; Boolean)
        {
            Caption = 'Posted Sales Invoice', Comment = 'de-DE=Geb. Rechnung';
        }
        field(56302; "ICI Posted Sales Cr.Memo"; Boolean)
        {
            Caption = 'Posted Sales Credit Memo', Comment = 'de-DE=Geb. Gutschrift';
        }
        field(56303; "ICI Service Quote"; Boolean)
        {
            Caption = 'Service Quote', Comment = 'de-DE=Angebot';
        }
        field(56304; "ICI Service Order"; Boolean)
        {
            Caption = 'Service Order', Comment = 'de-DE=Auftrag';
        }
        field(56305; "ICI Service Invoice"; Boolean)
        {
            Caption = 'Service Invoice', Comment = 'de-DE=Rechnung';
        }
        field(56306; "ICI Service Credit Memo"; Boolean)
        {
            Caption = 'Service Credit Memo', Comment = 'de-DE=Gutschrift';
        }
        field(56307; "ICI Posted Service Shipment"; Boolean)
        {
            Caption = 'Posted Service Shipment', Comment = 'de-DE=Lieferung';
        }
        field(56308; "ICI Posted Service Invoice"; Boolean)
        {
            Caption = 'Pstd. Serv. Invoice', Comment = 'de-DE=Geb. Rechnung';
        }
        field(56309; "ICI Posted Service Cr.Memo"; Boolean)
        {
            Caption = 'Posted Service Credit Memo', Comment = 'de-DE=Geb. Gutschrift';
        }
        field(56310; "ICI Service Contract"; Boolean)
        {
            Caption = 'Service Contract', Comment = 'de-DE=Vertrag';
        }
        field(56311; "ICI Service Contract Quote"; Boolean)
        {
            Caption = 'Service Contract Quote', Comment = 'de-DE=Vertragsangebot';
        }
        field(56312; "CTI Phone No."; Text[50])
        {
            Caption = 'ICI CTI Phone No.', Comment = 'de-DE=CTI Telefonnr.';
            DataClassification = CustomerContent;
        }
        field(56313; "CTI Mobile Phone No."; Text[50])
        {
            Caption = 'ICI CTI Mobile Phone No.', Comment = 'de-DE=CTI Mobilfunknr.';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(SupportActive; "ICI Support Active", "ICI E-Mail TLD")
        {

        }
        key(SupportLogin; "ICI Login", "ICI Support Active") { }
        key(Key14; "CTI Phone No.")
        {
        }
        key(Key15; "CTI Mobile Phone No.")
        {
        }

    }
    trigger OnBeforeModify()
    begin
        IF StrPos("Search E-Mail", '@') > 0 then
            "ICI E-Mail TLD" := CopyStr("Search E-Mail", StrPos("Search E-Mail", '@'), 80);
    end;

    procedure ICIGeneratePassword() RandomPass: Text[50];
    var
        CryptographyManagement: Codeunit "Cryptography Management";
        HashAlgorithmType: Option MD5,SHA1,SHA256,SHA384,SHA512;
    begin
        RandomPass := CREATEGUID();
        RandomPass := DELCHr(RandomPass, '=', '{}-');
        RandomPass := copystr(RandomPass, 1, 12);
        VALIDATE("ICI Support Password Hash", CryptographyManagement.GenerateHash(RandomPass, HashAlgorithmType::MD5));

        Validate("ICI Support Active", true);
        "Login Failures" := 0; // Reset Login Failures on new Login
        Modify(TRUE);
    end;

    procedure GetCustomerNo(): Code[20]
    var
        ContactBusinessRelation: Record "Contact Business Relation";
    begin
        ContactBusinessRelation.SETCURRENTKEY("Link to Table", "Contact No.");
        ContactBusinessRelation.SETRANGE("Contact No.", "Company No.");
        ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);

        IF ContactBusinessRelation.FINDFIRST() THEN
            EXIT(ContactBusinessRelation."No.");
    end;
}
