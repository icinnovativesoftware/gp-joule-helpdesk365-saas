table 56282 "ICI Support Text Module"
{
    Caption = 'Support Text Module', Comment = 'de-DE=Support Textvorlage';
    DataClassification = CustomerContent;
    DrillDownPageId = "ICI Support Text Module";
    LookupPageId = "ICI Support Text Module";
    fields
    {
        field(1; Code; Code[30])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(2; "Language Code"; Code[10])
        {
            Caption = 'Language Code', Comment = 'de-DE=Sprachcode';
            TableRelation = Language;
            DataClassification = CustomerContent;
        }
        field(3; Type; Option)
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            OptionMembers = "E-Mail",Ticket;
            OptionCaption = 'E-Mail,Ticket', Comment = 'de-DE=E-Mail,Ticket';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[250])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; Data; Blob)
        {
            Caption = 'Data', Comment = 'de-DE=Daten';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; "Code", "Language Code", "Type")
        {
            Clustered = true;
        }
    }

}
