table 56280 "ICI Support Process Stage"
{
    Caption = 'ICI Support Process Stage', Comment = 'de-DE=Prozessstufe';
    DataClassification = CustomerContent;
    DrillDownPageId = "ICI Process Stages";
    LookupPageId = "ICI Process Stages";

    fields
    {
        field(1; "Process Code"; Code[20])
        {
            Caption = 'Process Code', Comment = 'de-DE=Prozess Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Process";
        }
        field(2; Stage; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Stage', Comment = 'de-DE=Stufe';
            BlankZero = true;
            MinValue = 1;
            NotBlank = true;

        }

        field(4; "Ticket State"; Enum "ICI Ticket State")
        {
            Caption = 'Ticket State', Comment = 'de-DE=Ticketstatus';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                Case "Ticket State" of
                    "Ticket State"::Closed:
                        "Applys to Ticket State" := "Applys to Ticket State"::Close;
                    "Ticket State"::Preparation:
                        "Applys to Ticket State" := "Applys to Ticket State"::Prep;
                    "Ticket State"::Waiting:
                        "Applys to Ticket State" := "Applys to Ticket State"::WaitForAcceptance;
                    "Ticket State"::Processing, "Ticket State"::Open:
                        "Applys to Ticket State" := "Applys to Ticket State"::"Open/Process";
                End;
            end;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "E-Mail C Text Module Code"; Code[30])
        {
            Caption = 'E-Mail Contact Text Module Code', Comment = 'de-DE=Kontakt E-Mail Textvorlage';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(12; "E-Mail U Text Module Code"; Code[30])
        {
            Caption = 'E-Mail User Text Module Code', Comment = 'de-DE=Benutzer E-Mail Textvorlage';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        // field(13; "No. of Translations"; Integer)
        // {
        //     Caption = 'No. of Translations', Comment = 'de-DE=Anz. Übersetzungen';
        //     Editable = false;
        //     FieldClass = FlowField;
        //     CalcFormula = count("ICI Support Process Stage" where("Process Code" = field("Process Code"), Stage = field(Stage), "Language Code" = filter(<> '')));
        // }
        field(14; "Previous Allowed"; Boolean)
        {
            Caption = 'Previous Allowed', Comment = 'de-DE=Zurück erlaubt';
            DataClassification = CustomerContent;
        }
        field(15; "Skip Allowed"; Boolean)
        {
            Caption = 'Skip Allowed', Comment = 'de-DE=Überspringen erlaubt';
            DataClassification = CustomerContent;
        }
        field(16; "Jump To Allowed"; Boolean)
        {
            Caption = 'Jump To Allowed', Comment = 'de-DE=Hineinspringen erlaubt';
            DataClassification = CustomerContent;
        }
        field(17; "Service Item Mandatory"; Boolean)
        {
            Caption = 'Service Item Mandatory', Comment = 'de-DE=Serviceartikel pflicht';
            DataClassification = CustomerContent;
        }
        field(18; "Document No. Mandatory"; Boolean)
        {
            Caption = 'Document No. Mandatory', Comment = 'de-DE=Belegnr. pflicht';
            DataClassification = CustomerContent;
        }
        field(19; Initial; Boolean)
        {
            Caption = 'Initial', Comment = 'de-DE=Initial';
            DataClassification = CustomerContent;
        }
        field(20; "Ticket State Filter"; ENUM "ICI Ticket State")
        {
            Caption = 'Source State', Comment = 'de-DE=Ticket Status Filter';
            FieldClass = FlowFilter;
        }
        field(21; "Applys to Ticket State"; Option)
        {
            Caption = 'Ticket State', Comment = 'de-DE=Bezieht sich auf Ticketstatus';
            DataClassification = CustomerContent;
            OptionCaption = 'Prep,Open/Process,WaitForAcceptance,Close', Comment = 'de-DE=Vorbereitung,Offen/Bearbeitung,Warten,Geschlossen';
            OptionMembers = Prep,"Open/Process",WaitForAcceptance,Close;
        }
        field(22; Progress; Integer)
        {
            Caption = 'Progress', Comment = 'de-DE=Fortschritt';
            DataClassification = CustomerContent;
            MaxValue = 100;
            MinValue = 0;
            trigger OnValidate()
            begin
                "Progress Ratio" := Progress * 100;
            end;
        }
        field(23; "Activity Code"; Code[20])
        {
            Caption = 'Activity Code', Comment = 'de-DE=Aktivitätencode';
            DataClassification = CustomerContent;
            TableRelation = Activity;
        }
        field(24; "Interaction Template Code"; Code[10])
        {
            Caption = 'Interaction Template', Comment = 'de-DE=Aktivitätenvorlagencode';
            Description = 'CRM';
            TableRelation = "Interaction Template".Code;
        }
        field(25; "Department Code"; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Department Code', Comment = 'de-DE=Abteilungscode';
            TableRelation = "ICI Department";
        }
        field(27; "Progress Ratio"; Integer)
        {
            Caption = 'Progress Ratio', Comment = 'de-DE=Fortschritt in %';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(28; "Support User ID"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = "ICI Support User";
            Caption = 'Support User Code', Comment = 'de-DE=Supportbenutzer';
            trigger OnValidate()
            begin
                if Rec."Support User ID" <> '' then
                    if Rec."Ticket State" = Rec."Ticket State"::Closed then
                        Rec.TestField("Ticket State", Rec."Ticket State"::Processing);
            end;
        }
        field(30; "Reset processing time"; Boolean)
        {
            Caption = 'Reset processing time', Comment = 'de-DE=Bearbeitungszeit zurücksetzen|en-US=Reset processing time';
            DataClassification = CustomerContent;
        }

    }
    keys
    {
        key(PK; "Process Code", Stage)
        {
            Clustered = true;
        }
        key(Portal; "Process Code", "Ticket State") { }

    }
    procedure GetDescription(): Text[100]
    var
        ICIProcStageTranslation: Record "ICI Proc. Stage Translation";
        Language: Codeunit Language;
        LanguageCode: Code[10];
    begin
        LanguageCode := Language.GetLanguageCode(GlobalLanguage);

        IF ICIProcStageTranslation.GET("Process Code", Stage, LanguageCode) then
            EXIT(ICIProcStageTranslation.Description);

        EXIT(Description);
    end;
}
