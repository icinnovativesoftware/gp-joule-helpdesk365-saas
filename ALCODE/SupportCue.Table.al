table 56294 "ICI Support Cue"
{
    Caption = 'ICI Support Cue';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Code; Code[20])
        {
            Caption = 'Code';
            DataClassification = SystemMetadata;
        }
        field(10; "Ticket Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(11; "User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(12; "Contact Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(13; "Document Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(14; "Item Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(15; "Service Item Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(16; "Department Filter"; Code[100])
        {
            FieldClass = FlowFilter;
        }
        field(17; "Date Filter 1"; DateTime)
        {
            FieldClass = FlowFilter;
        }
        field(18; "Date Filter 2"; DateTime)
        {
            FieldClass = FlowFilter;
        }
        field(19; "Date Filter 3"; DateTime)
        {
            FieldClass = FlowFilter;
        }
        field(20; "Tickets - All"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("No." = filter('*'), "Ticket State" = filter(<> closed))); // Filter * anwenden, sonst öffnet sich ticketliste mit Department Filter
        }
        field(21; "My Tickets - All"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Support User ID" = field("User Filter"), "Ticket State" = filter(<> closed)));
        }
        field(22; "My Dep. Tickets - All"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Department Code" = field("Department Filter"), "Ticket State" = filter(<> closed)));
        }
        field(23; "Tickets - Preparation"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Preparation)));
        }
        field(24; "My Tickets - Preparation"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Preparation), "Support User ID" = field("User Filter")));
        }
        field(25; "My Dep. Tickets - Preparation"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Preparation), "Department Code" = field("Department Filter")));
        }
        field(26; "Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Open)));
        }
        field(27; "My Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Open), "Support User ID" = field("User Filter")));
        }
        field(28; "My Dep. Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Open), "Department Code" = field("Department Filter")));
        }
        field(29; "Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Processing)));
        }
        field(30; "My Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Processing), "Support User ID" = field("User Filter")));
        }
        field(31; "My Dep. Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Processing), "Department Code" = field("Department Filter")));
        }
        field(32; "Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Waiting)));
        }
        field(33; "My Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Waiting), "Support User ID" = field("User Filter")));
        }
        field(34; "My Dep. Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Waiting), "Department Code" = field("Department Filter")));
        }
        field(35; "Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Closed)));
        }
        field(36; "My Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Closed), "Support User ID" = field("User Filter")));
        }
        field(37; "My Dep. Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Closed), "Department Code" = field("Department Filter")));
        }
        field(38; "Tickets - Closed Today"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Closed), "Department Code" = field("Department Filter"), "Last Activity Date" = field("Date Filter 1")));
        }
        field(39; "Tickets - Closed Month"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = const(Closed), "Department Code" = field("Department Filter"), "Last Activity Date" = field("Date Filter 2")));
        }
        field(41; "Tickets - Last ch. by Contact"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Last Activity By Type" = const(Contact)));
        }
        field(42; "Tickets - Last ch. by User"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Last Activity By Type" = const(User)));
        }
        field(43; "Mails - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received)));
        }
        field(50; "Mailbox 1 - Filter"; Code[30])
        {
            FieldClass = FlowFilter;
        }
        field(51; "Mailbox 2 - Filter"; Code[30])
        {
            FieldClass = FlowFilter;
        }
        field(52; "Mailbox 3 - Filter"; Code[30])
        {
            FieldClass = FlowFilter;
        }
        field(53; "Mailbox 4 - Filter"; Code[30])
        {
            FieldClass = FlowFilter;
        }
        field(54; "Mailbox 5 - Filter"; Code[30])
        {
            FieldClass = FlowFilter;
        }
        field(60; "Mailbox 1 - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received), "Mailrobot Mailbox Code" = field("Mailbox 1 - Filter")));
        }
        field(61; "Mailbox 2 - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received), "Mailrobot Mailbox Code" = field("Mailbox 2 - Filter")));
        }
        field(62; "Mailbox 3 - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received), "Mailrobot Mailbox Code" = field("Mailbox 3 - Filter")));
        }
        field(63; "Mailbox 4 - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received), "Mailrobot Mailbox Code" = field("Mailbox 4 - Filter")));
        }
        field(64; "Mailbox 5 - Unprocessed"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Received), "Mailrobot Mailbox Code" = field("Mailbox 5 - Filter")));
        }
        field(65; "Mailrobot - Errors"; Integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = count("ICI Mailrobot Log" where("Process State" = const(Error)));
        }
        field(66; "Tickets - Open - Category 1"; Integer)
        {
            Caption = 'Tickets - Open - Category 1', Comment = 'de-DE=Kategoriekachel 1';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Category 1 Code" = field("Cue 1 - Category 1 Filter"), "Category 2 Code" = field("Cue 1 - Category 2 Filter")));
        }
        field(67; "Tickets - Open - Category 2"; Integer)
        {
            Caption = 'Tickets - Open - Category 2', Comment = 'de-DE=Kategoriekachel 2';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Category 1 Code" = field("Cue 2 - Category 1 Filter"), "Category 2 Code" = field("Cue 2 - Category 2 Filter")));
        }
        field(68; "Tickets - Open - Category 3"; Integer)
        {
            Caption = 'Tickets - Open - Category 3', Comment = 'de-DE=Kategoriekachel 3';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Category 1 Code" = field("Cue 3 - Category 1 Filter"), "Category 2 Code" = field("Cue 3 - Category 2 Filter")));
        }
        field(69; "Tickets - Open - Category 4"; Integer)
        {
            Caption = 'Tickets - Open - Category 4', Comment = 'de-DE=Kategoriekachel 4';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Category 1 Code" = field("Cue 4 - Category 1 Filter"), "Category 2 Code" = field("Cue 4 - Category 2 Filter")));
        }
        field(70; "Cue 1 - Category 1 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(71; "Cue 1 - Category 2 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(72; "Cue 2 - Category 1 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(73; "Cue 2 - Category 2 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(74; "Cue 3 - Category 1 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(75; "Cue 3 - Category 2 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(76; "Cue 4 - Category 1 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(77; "Cue 4 - Category 2 Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(78; "Tickets - Open - Process 1"; Integer)
        {
            Caption = 'Tickets - Open - Prozess 1', Comment = 'de-DE=Prozesskachel 1';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Process Code" = field("Cue 1 - Process Code Filter"), "Process Stage" = field("Cue 1 - Process Stage Filter")));
        }
        field(79; "Tickets - Open - Process 2"; Integer)
        {
            Caption = 'Tickets - Open - Prozess 2', Comment = 'de-DE=Prozesskachel 2';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Process Code" = field("Cue 2 - Process Code Filter"), "Process Stage" = field("Cue 2 - Process Stage Filter")));
        }
        field(80; "Tickets - Open - Process 3"; Integer)
        {
            Caption = 'Tickets - Open - Prozess 3', Comment = 'de-DE=Prozesskachel 3';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Process Code" = field("Cue 3 - Process Code Filter"), "Process Stage" = field("Cue 3 - Process Stage Filter")));
        }
        field(81; "Tickets - Open - Process 4"; Integer)
        {
            Caption = 'Tickets - Open - Prozess 4', Comment = 'de-DE=Prozesskachel 4';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Process Code" = field("Cue 4 - Process Code Filter"), "Process Stage" = field("Cue 4 - Process Stage Filter")));
        }
        field(82; "Cue 1 - Process Code Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(83; "Cue 1 - Process Stage Filter"; Integer)
        {
            FieldClass = FlowFilter;
        }
        field(84; "Cue 2 - Process Code Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(85; "Cue 2 - Process Stage Filter"; Integer)
        {
            FieldClass = FlowFilter;
        }
        field(86; "Cue 3 - Process Code Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(87; "Cue 3 - Process Stage Filter"; Integer)
        {
            FieldClass = FlowFilter;
        }
        field(88; "Cue 4 - Process Code Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(89; "Cue 4 - Process Stage Filter"; Integer)
        {
            FieldClass = FlowFilter;
        }
        field(90; "Tickets - Open - Queue - 1"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 1', Comment = 'de-DE=Warteschlange 1';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 1 - User Filter")));
        }
        field(91; "Tickets - Open - Queue - 2"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 2', Comment = 'de-DE=Warteschlange 2';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 2 - User Filter")));
        }
        field(92; "Tickets - Open - Queue - 3"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 3', Comment = 'de-DE=Warteschlange 3';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 3 - User Filter")));
        }
        field(93; "Tickets - Open - Queue - 4"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 4', Comment = 'de-DE=Warteschlange 4';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 4 - User Filter")));
        }
        field(94; "Tickets - Open - Queue - 5"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 5', Comment = 'de-DE=Warteschlange 5';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 5 - User Filter")));
        }
        field(95; "Tickets - Open - Queue - 6"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 6', Comment = 'de-DE=Warteschlange 6';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 6 - User Filter")));
        }
        field(96; "Tickets - Open - Queue - 7"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 7', Comment = 'de-DE=Warteschlange 7';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 7 - User Filter")));
        }
        field(97; "Tickets - Open - Queue - 8"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 8', Comment = 'de-DE=Warteschlange 8';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 8 - User Filter")));
        }
        field(98; "Tickets - Open - Queue - 9"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 9', Comment = 'de-DE=Warteschlange 9';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 9 - User Filter")));
        }
        field(99; "Tickets - Open - Queue - 10"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 10', Comment = 'de-DE=Warteschlange 10';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 10 - User Filter")));
        }
        field(200; "Tickets - Open - Queue - 11"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 11', Comment = 'de-DE=Warteschlange 11';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 11 - User Filter")));
        }
        field(201; "Tickets - Open - Queue - 12"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 12', Comment = 'de-DE=Warteschlange 12';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 12 - User Filter")));
        }
        field(202; "Tickets - Open - Queue - 13"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 13', Comment = 'de-DE=Warteschlange 13';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 13 - User Filter")));
        }
        field(203; "Tickets - Open - Queue - 14"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 14', Comment = 'de-DE=Warteschlange 14';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 14 - User Filter")));
        }
        field(204; "Tickets - Open - Queue - 15"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 15', Comment = 'de-DE=Warteschlange 15';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 15 - User Filter")));
        }
        field(205; "Tickets - Open - Queue - 16"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 16', Comment = 'de-DE=Warteschlange 16';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 16 - User Filter")));
        }
        field(206; "Tickets - Open - Queue - 17"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 17', Comment = 'de-DE=Warteschlange 17';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 17 - User Filter")));
        }
        field(207; "Tickets - Open - Queue - 18"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 18', Comment = 'de-DE=Warteschlange 18';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 18 - User Filter")));
        }
        field(208; "Tickets - Open - Queue - 19"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 19', Comment = 'de-DE=Warteschlange 19';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 19 - User Filter")));
        }
        field(209; "Tickets - Open - Queue - 20"; Integer)
        {
            Caption = 'Tickets - Open - Queue - 20', Comment = 'de-DE=Warteschlange 20';
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Ticket State" = filter(<> Closed), "Support User ID" = field("Queue 20 - User Filter")));
        }

        field(100; "Queue 1 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(101; "Queue 2 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(102; "Queue 3 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(103; "Queue 4 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(104; "Queue 5 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(105; "Queue 6 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(106; "Queue 7 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(107; "Queue 8 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(108; "Queue 9 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(109; "Queue 10 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(110; "Date Filter 4"; Date)
        {
            FieldClass = FlowFilter;
        }
        field(111; "Salesperson Filter"; Code[20])
        {
            FieldClass = FlowFilter;
        }
        field(210; "Queue 11 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(211; "Queue 12 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(212; "Queue 13 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(213; "Queue 14 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(214; "Queue 15 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(215; "Queue 16 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(216; "Queue 17 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(217; "Queue 18 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(218; "Queue 19 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(219; "Queue 20 - User Filter"; Code[50])
        {
            FieldClass = FlowFilter;
        }
        field(112; "No. of Open ToDo"; Integer)
        {
            CalcFormula = Count("To-do" WHERE(Status = FILTER(<> Completed),
                                             "System To-do Type" = CONST(Organizer)));
            Caption = 'No. of Open ToDos', Comment = 'de-DE=Offene Aufgaben';
            Description = 'NSSDE134.03.00';
            Editable = false;
            FieldClass = FlowField;
        }
        field(113; "No. of My Open ToDo"; Integer)
        {
            CalcFormula = Count("To-do" WHERE(Status = FILTER(<> Completed),
                                             "System To-do Type" = CONST(Organizer),
                                             "Salesperson Code" = FIELD("Salesperson Filter")));
            Caption = 'No. of My Open ToDos', Comment = 'de-DE=Meine Off. Aufgaben';
            Description = 'NSSDE134.03.00';
            Editable = false;
            FieldClass = FlowField;
        }
        field(114; "No. of Open ToDo to My Ticket"; Integer)
        {
            CalcFormula = Count("To-do" WHERE(Status = FILTER(<> Completed),
                                             "System To-do Type" = CONST(Organizer),
                                             "ICI Support User ID" = FIELD("User Filter")));
            Caption = 'No. of Open ToDo to My Ticket', Comment = 'de-DE=Of. Aufgaben zu m. Tickets';
            Description = 'NSSDE134.03.00';
            Editable = false;
            FieldClass = FlowField;
        }
        field(115; "No. of My Duty ToDo"; Integer)
        {
            CalcFormula = Count("To-do" WHERE(Status = FILTER(<> Completed),
                                             "System To-do Type" = CONST(Organizer),
                                             "Salesperson Code" = FIELD("Salesperson Filter"),
                                             "Ending Date" = FIELD("Date Filter 4")));
            Caption = 'No. of My Duty ToDo', Comment = 'de-DE=Meine fälligen Aufgaben';
            Description = 'NSSDE134.03.00';
            Editable = false;
            FieldClass = FlowField;
        }
        field(116; "No. of Duty ToDo to My Ticket"; Integer)
        {
            CalcFormula = Count("To-do" WHERE(Status = FILTER(<> Completed),
                                             "System To-do Type" = CONST(Organizer),
                                             "ICI Support User ID" = FIELD("User Filter"),
                                             "Ending Date" = FIELD("Date Filter 4")));
            Caption = 'No. of Duty ToDo to My Ticket', Comment = 'de-DE=Fällige Aufgaben zu m. Ticket';
            Description = 'NSSDE134.03.00';
            Editable = false;
            FieldClass = FlowField;
        }

    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }
}
