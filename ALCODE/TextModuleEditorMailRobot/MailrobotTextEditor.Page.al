page 56352 "ICI Mailrobot Text Editor"
{
    Caption = 'Mailrobot Text Editor', Comment = 'de-DE=Vorschau';
    PageType = CardPart;
    SourceTable = "ICI Mailrobot Log";

    layout
    {
        area(Content)
        {
            usercontrol(Editor; "ICI Mailrobot CKEditor")
            {
                ApplicationArea = All;

                trigger ControlReady()
                begin
                    AddinReady := true;
                    CurrPage.Editor.Init();

                    CurrPage.Editor.SetReadOnly(true);
                end;

                trigger OnAfterInit()
                var
                    Base64Convert: Codeunit "Base64 Convert";
                    lInStream: InStream;
                    TextModuleContent: Text;
                begin
                    IF Rec."Entry No." <> 0 THEN begin
                        Rec.CalcFields(Body);
                        Rec.Body.CreateInStream(lInStream);
                        lInStream.Read(TextModuleContent);
                        TextModuleContent := Base64Convert.FromBase64(TextModuleContent);
                        CurrPage.Editor.Load(TextModuleContent);
                        CurrPage.Editor.SetReadOnly(true);
                    end;
                end;

                trigger SaveRequested(data: Text)
                begin
                    Error('');
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        Base64Convert: Codeunit "Base64 Convert";
        lInStream: InStream;
        TextModuleContent: Text;
    begin
        if AddinReady THEN BEGIN
            CurrPage.Editor.SetReadOnly(false);
            IF rec."Entry No." <> xRec."Entry No." THEN BEGIN
                Rec.CalcFields(Body);
                Rec.Body.CreateInStream(lInStream);
                lInStream.Read(TextModuleContent);
                TextModuleContent := Base64Convert.FromBase64(TextModuleContent);
                CurrPage.Editor.Load(TextModuleContent);
            END;
        END;
    end;

    var
        AddinReady: Boolean;

}
