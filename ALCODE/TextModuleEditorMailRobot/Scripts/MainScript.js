
var InputArea
var Editor

// Editor configuration.
ClassicEditor.defaultConfig = {
	highlight: {
		options: [
			{
				model: 'orangePen',
				class: 'pen-orange',
				title: 'Orange',
				color: '#e59536',
				type: 'pen'
			},
			{
				model: 'lightBluePen',
				class: 'pen-light-blue',
				title: 'Light blue',
				color: '#30caf7',
				type: 'pen'
			},
			{
				model: 'darkBluePen',
				class: 'pen-dark-blue',
				title: 'Dark blue',
				color: '#3982a4',
				type: 'pen'
			},
			{
				model: 'greenPen',
				class: 'pen-green',
				title: 'Green',
				color: '#488f80',
				type: 'pen'
			},
			{
				model: 'grayPen',
				class: 'pen-gray',
				title: 'Dark gray',
				color: '#4d4d4c',
				type: 'pen'
			}
		]
	},
	heading: {
		options: [
			{ model: 'paragraph', title: 'Body copy', class: 'ck-heading_paragraph' },
			{ model: 'heading2', view: 'h2', title: 'Sub Header', class: 'ck-heading_heading2' }
		]
	},
	toolbar: {
		items: [
			'heading',
			'|',
			'highlight',
			'|',
			'bold',
			'italic',
			'underline',
			'|',
			'numberedList',
			'bulletedList',
			'|',
			'blockQuote',
			'|',
			'link',
			'|',
			'imageUpload',
			'|'
		]
	},
	image: {
		toolbar: [
			'imageStyle:full',
			'imageStyle:side',
			'|',
			'imageTextAlternative'
		]
	},
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'en'
};

function Init() {

    var div = document.getElementById("controlAddIn");
    div.innerHTML = "";
    InputArea = document.createElement("textarea");
    InputArea.id = "Comment";
    InputArea.name = "Comment";
    div.appendChild(InputArea);
    

    //debugger;
    ClassicEditor
        .create(document.querySelector('#Comment'),{
            removePlugins: [ 'Heading', 'Link' , 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
            //toolbar: [ 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ]
            toolbar: [ ],
        })
        .then(editor => {
            Editor = editor;
			Editor.isReadOnly = true;
        })
        .catch(error => {
            console.error(error);
        });
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("OnAfterInit",[]);
}

function Load(data) {
    Editor.setData(data);
}

function RequestSave() {
    //debugger;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("SaveRequested",[Editor.getData()]);
}
function SetReadOnly(readonly)
{
    // setReadOnly
    Editor.isReadOnly = readonly;

    // hide Toolbar
    /*var bar = document.getElementsByClassName("ck-toolbar")[0];
    if(bar.style.display == "none"){
        bar.style.display = "";
    }else{
        bar.style.display = "none";
    }
    */
}
