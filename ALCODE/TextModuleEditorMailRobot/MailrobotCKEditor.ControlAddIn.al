controladdin "ICI Mailrobot CKEditor"
{
    VerticalStretch = true;
    HorizontalStretch = true;
    MinimumHeight = 350;
    RequestedHeight = 350;

    Scripts = 'ALCODE\TextModuleEditorMailRobot\Scripts\ckeditor5classic10.js', 'ALCODE\TextModuleEditorMailRobot\Scripts\MainScript.js';
    StartupScript = 'ALCODE\TextModuleEditorMailRobot\Scripts\startupScript.js';
    RecreateScript = 'ALCODE\TextModuleEditorMailRobot\Scripts\recreateScript.js';
    RefreshScript = 'ALCODE\TextModuleEditorMailRobot\Scripts\refreshScript.js';
    Stylesheets = 'ALCode\TextModuleEditorMailRobot\Style\Editor.css';

    event ControlReady();
    event SaveRequested(data: Text);
    event ContentChanged();
    event OnAfterInit();

    procedure Init();
    procedure Load(data: Text);
    procedure RequestSave();
    procedure SetReadOnly(readonly: boolean);
}
