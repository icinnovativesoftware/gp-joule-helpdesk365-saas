enum 56277 "ICI Ticket State"
{
    Extensible = true;

    // value(0; " ")
    // {
    //     Caption = ' ';
    // }
    value(1; Preparation)
    {
        Caption = 'Preparation', Comment = 'de-DE=Vorbereitung';
    }
    value(2; Open)
    {
        Caption = 'Open', Comment = 'de-DE=Offen';
    }
    value(3; Processing)
    {
        Caption = 'Processing', Comment = 'de-DE=In Bearbeitung';
    }
    value(4; Waiting)
    {
        Caption = 'Waiting', Comment = 'de-DE=Warten';
    }
    value(5; Closed)
    {
        Caption = 'Closed', Comment = 'de-DE=Geschlossen';
    }
}
