enum 56279 "ICI Escalation Condition"
{
    Extensible = true;

    value(0; Processstage)
    {
        Caption = 'Too long in Process Stage', Comment = 'de-DE=Zu lange in Prozessstufe';
    }
    value(1; Response)
    {
        Caption = 'too long no Respone', Comment = 'de-DE=Zu lange keine Antwort';
    }
    value(2; Processstate)
    {
        Caption = 'too long in Ticket State', Comment = 'de-DE=Zu lange in Status';
    }
    value(3; EscalationGroup)
    {
        Caption = 'too long in Escalation Group', Comment = 'de-DE=Zu lange in Eskalationsgruppe';
    }
}
