table 56318 "ICI Escalation Group Line"
{
    Caption = 'Escalation Group Line';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Escalation Group Code"; Code[20])
        {
            Caption = 'Escalation Group Code';
            DataClassification = CustomerContent;
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No.';
            DataClassification = SystemMetadata;
        }
        field(10; "Process Code"; Code[20])
        {
            Caption = 'Process Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Process";
        }

        field(11; "Process Description"; Text[50])
        {
            Caption = 'Process Description', Comment = 'de-DE=Prozessbeschreibung';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support Process".Description where(Code = field("Process Code")));
        }
        field(12; "Process Stage"; Integer)
        {
            Caption = 'Process Stage';
            DataClassification = CustomerContent;
        }
        field(13; "Process Stage Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("ICI Support Process Stage".Description where("Process Code" = field("Process Code"), Stage = field("Process Stage")));
            Caption = 'Process Stage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
        }
    }
    keys
    {
        key(PK; "Escalation Group Code", "Line No.")
        {
            Clustered = true;
        }
    }
}
