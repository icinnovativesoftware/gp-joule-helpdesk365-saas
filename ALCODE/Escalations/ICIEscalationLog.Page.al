page 56348 "ICI Escalation Log"
{

    Caption = 'My Escalation Log', Comment = 'de-DE=Meine Eskalationsmeldungen';
    PageType = ListPart;
    SourceTable = "ICI Support ticket log";
    SourceTableView = sorting("Entry No.") order(descending);
    Editable = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Creation Date"; Rec."Creation Date")
                {
                    ToolTip = 'Specifies the value of the Creation Date', Comment = 'de-DE=Erstellt am field';
                    ApplicationArea = All;
                    StyleExpr = EscalatedStyleExpr;
                }
                field("Support Ticket No."; Rec."Support Ticket No.")
                {
                    ToolTip = 'Specifies the value of the Support Ticket No.', Comment = 'de-DE=Ticketnr.';
                    ApplicationArea = All;
                    StyleExpr = EscalatedStyleExpr;
                }
                field("Data Text"; Rec."Data Text")
                {
                    Caption = 'Description', Comment = 'de-DE=Beschreibung';
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung des Eskalierten Tickets';
                    ApplicationArea = All;
                    StyleExpr = EscalatedStyleExpr;
                }
                field("Data Text 2"; Rec."Data Text 2")
                {
                    Caption = 'Company Name', Comment = 'de-DE=Unternehmen';
                    ToolTip = 'Company Name', Comment = 'de-DE=Unternehmen';
                    ApplicationArea = All;
                    StyleExpr = EscalatedStyleExpr;
                }
                field(EscalationReason; EscalationReason)
                {
                    Caption = 'Escalation Reason', Comment = 'de-DE=Grund';
                    ToolTip = 'Escalation Reason', Comment = 'de-DE=Grund';
                    ApplicationArea = All;
                    StyleExpr = EscalatedStyleExpr;
                }

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(Escalation)
            {
                ApplicationArea = All;
                ToolTip = 'Escalation', Comment = 'de-DE=Eskalation ausführen';
                Caption = 'Escalation', Comment = 'de-DE=Eskalation ausführen';
                Image = Timesheet;
                trigger OnAction()
                var
                    ICIEscalationMgt: Codeunit "ICI Escalation Mgt.";
                begin
                    ICIEscalationMgt.EscalationTimer();
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        Rec.SetRange(Type, Rec.Type::Escalation);
        Rec.SetRange("Escalation User", UserId());
    end;

    trigger OnAfterGetRecord()
    var
        ICISupportTicket: Record "ICI Support Ticket";
        EscalationLevel: Record "ICI Escalation Level";
        ICIEscalationGroup: Record "ICI Escalation Group";
        lRecordRef: RecordRef;
        EscalationLbl: Label '%1 %2', Comment = '%1 = Start Time; %2 = Condition';
    begin
        CLEAR(EscalatedStyleExpr);
        IF ICISupportTicket.GET(Rec."Support Ticket No.") then
            IF ICISupportTicket."Ticket State" <> ICISupportTicket."Ticket State"::Closed then
                EscalatedStyleExpr := 'Unfavorable'; // Ticket, auf das sich die eskalationsmeldung bezieht ist noch offen. Dann rot
        IF Rec."Record ID".TableNo <> DATABASE::"ICI Escalation Level" THEN // Bugfix: Renumbering Issue
            exit;
        lRecordRef := Rec."Record ID".GetRecord();
        lRecordRef.SetTable(EscalationLevel);
        IF EscalationLevel.GET(EscalationLevel."Escalation Code", EscalationLevel."Line No.") THEN // Ooopsies. SetTable is sneaky ~.~
            EscalationReason := StrSubstNo(EscalationLbl, EscalationLevel."Start Time", ICIEscalationGroup.TableCaption);
    end;

    var
        EscalatedStyleExpr: Text;
        EscalationReason: Text;
}
