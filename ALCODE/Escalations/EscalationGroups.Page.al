page 56372 "ICI Escalation Groups"
{

    ApplicationArea = All;
    Caption = 'Escalation Groups', Comment = 'de-DE=Eskalationsgruppen';
    PageType = List;
    SourceTable = "ICI Escalation Group";
    UsageCategory = Administration;
    CardPageId = "ICI Escalation Group Card";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Specifies the value of the Code field.', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field.', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("No. of Lines"; Rec."No. of Lines")
                {
                    ToolTip = 'Specifies the value of the No. of Lines field.', Comment = 'de-DE=Anzahl Zeilen';
                    ApplicationArea = All;
                    BlankZero = true;
                }
            }
        }
    }

}
