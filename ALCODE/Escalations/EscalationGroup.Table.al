table 56317 "ICI Escalation Group"
{
    Caption = 'Escalation Group', Comment = 'de-DE=Eskalationsgruppe';
    DataClassification = CustomerContent;
    DrillDownPageId = "ICI Escalation Groups";
    LookupPageId = "ICI Escalation Groups";

    fields
    {
        field(1; "Code"; Code[20])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "No. of Lines"; Integer)
        {
            Caption = 'No. of Lines', Comment = 'de-DE=Anzahl Zeilen';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Count("ICI Escalation Group Line" where("Escalation Group Code" = field("Code")));
        }
    }
    keys
    {
        key(PK; "Code")
        {
            Clustered = true;
        }
    }

    procedure AddStage(var ICISupportProcessStage: Record "ICI Support Process Stage")
    var
        ICIEscalationGroupLine: Record "ICI Escalation Group Line";
        LineNo: Integer;
    begin

        LineNo := 10000;
        ICIEscalationGroupLine.SetRange("Escalation Group Code", Code);
        IF ICIEscalationGroupLine.FindLast() then
            LineNo += ICIEscalationGroupLine."Line No.";

        CLEAR(ICIEscalationGroupLine);
        ICIEscalationGroupLine.Init();
        ICIEscalationGroupLine."Escalation Group Code" := Code;
        ICIEscalationGroupLine."Line No." := LineNo;
        ICIEscalationGroupLine.INSERT(true);
        ICIEscalationGroupLine.VALIDATE("Process Code", ICISupportProcessStage."Process Code");
        ICIEscalationGroupLine.VALIDATE("Process Stage", ICISupportProcessStage.Stage);
        ICIEscalationGroupLine.Modify();
    end;

}
