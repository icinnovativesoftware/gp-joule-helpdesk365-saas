table 56304 "ICI Escalation"
{
    Caption = 'Escalation', Comment = 'de-DE=Eskalation';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Escalations";
    DrillDownPageId = "ICI Escalations";

    fields
    {
        field(1; "Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Code', Comment = 'de-DE=Code';
            NotBlank = true;
        }
        field(2; Description; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
        }
        field(3; "No. of Escalation Level"; Integer)
        {
            CalcFormula = Count("ICI Escalation Level" WHERE("Escalation Code" = FIELD(Code)));
            Caption = 'No. of Escalation Level', Comment = 'de-DE=Anz. Stufen';
            Editable = false;
            FieldClass = FlowField;
        }
        field(4; "Target Time"; Duration)
        {
            DataClassification = CustomerContent;
            Caption = 'Target Time', Comment = 'de-DE=Sollzeit';
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
        key(Key2; Description)
        {
        }
    }

}
