page 56374 "ICI Escalation Group Card"
{

    Caption = 'Escalation Group Card', Comment = 'de-DE=Eskalationsgruppe';
    PageType = Card;
    SourceTable = "ICI Escalation Group";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Specifies the value of the Code field.', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field.', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
            }
            part("ICI Escalation Group Lines"; "ICI Escalation Group Lines")
            {
                ApplicationArea = All;
                SubPageLink = "Escalation Group Code" = field(Code);
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(AddStages)
            {
                ToolTip = 'Add selected Process Stages to Escalationgroup', Comment = 'de-DE=Fügt die ausgewählten Prozessstufen zur Eskalationsgruppe hinzu';
                ApplicationArea = All;
                Caption = 'Add Processstage(s)', Comment = 'de-DE=Prozessstufe(n) hinzufügen';
                Image = Add;
                Promoted = true;
                PromotedCategory = New;
                PromotedOnly = true;

                trigger OnAction()
                var
                    ICISupportProcessStage: Record "ICI Support Process Stage";
                    ICISupportProcessStageList: Page "ICI Support Process Stage List";
                begin
                    // Setfilter Possible
                    //ICISupportProcessStageList.SetTableView(ICISupportProcessStage);

                    ICISupportProcessStageList.EDITABLE(FALSE);
                    ICISupportProcessStageList.LOOKUPMODE(TRUE);
                    IF ICISupportProcessStageList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        ICISupportProcessStageList.SetSelectionFilter(ICISupportProcessStage);
                        ICISupportProcessStage.MARKEDONLY(TRUE);
                        IF ICISupportProcessStage.FINDSET() THEN
                            repeat
                                Rec.AddStage(ICISupportProcessStage);
                            until ICISupportProcessStage.Next() = 0;
                    end;

                    CurrPage.Update();
                end;
            }
        }
    }

}
