page 56347 "ICI Escalation Levels"
{

    Caption = 'Escalation Levels', Comment = 'de-DE=Eskalationsstufen';
    PageType = List;
    SourceTable = "ICI Escalation Level";
    UsageCategory = None;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Escalation Code"; Rec."Escalation Code")
                {
                    ToolTip = 'Specifies the value of the Escalation Code'', Comment = ''de-DE=Eskalationscode field';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Line No."; Rec."Line No.")
                {
                    ToolTip = 'Specifies the value of the Line No'', Comment = ''de-DE=Zeilennr.';
                    ApplicationArea = All;
                    Visible = false;
                }

                field("Start Time"; Rec."Start Time")
                {
                    ToolTip = 'Specifies the value of the Start Time', Comment = 'de-DE=Zeitpunkt field';
                    ApplicationArea = All;
                }
                field("Receiver Type"; Rec."Receiver Type")
                {
                    ToolTip = 'Specifies the value of the Receiver Type', Comment = 'de-DE=Empfängerart field';
                    ApplicationArea = All;
                }
                field("Support User"; Rec."Support User")
                {
                    ToolTip = 'Specifies the value of the Support User', Comment = 'de-DE=Eskalations Benutzer field';
                    ApplicationArea = All;
                }
                field("Escalation Group Code"; Rec."Escalation Group Code")
                {
                    ToolTip = 'Escalation Group Code', Comment = 'de-DE=Eskalationsgruppencode';
                    ApplicationArea = All;
                }
                field("Reset with Ticket Action"; Rec."Reset with Ticket Action")
                {
                    ToolTip = 'Reset with Ticket Action', Comment = 'de-DE=Eskalationstimer nach letzter Kundenaktion zurücksetzen.';
                    ApplicationArea = All;
                }
                field("Send E-Mail"; Rec."Send E-Mail")
                {
                    ToolTip = 'Send E-Mail', Comment = 'de-DE=Zusätzlich zum erzeigen der Eskalationsmeldung wird eine E-Mail versendet';
                    ApplicationArea = All;
                }
                field("Send SMS"; Rec."Send SMS")
                {
                    Visible = false;
                    ToolTip = 'Send SMS', Comment = 'de-DE=Zusätzlich zum erzeigen der Eskalationsmeldung wird eine SMS versendet';
                    ApplicationArea = All;
                }
                field("Send Notification"; Rec."Send Notification")
                {
                    Visible = false;
                    ToolTip = 'Send Notification', Comment = 'de-DE=Zusätzlich zum erzeigen der Eskalationsmeldung wird eine Benachrichtigung versendet';
                    ApplicationArea = All;
                }
                field("Send Link"; Rec."Send Link")
                {
                    Visible = false;
                    ToolTip = 'Send Link', Comment = 'de-DE=Zusätzlich zum erzeigen der Eskalationsmeldung wird eine Notiz versendet';
                    ApplicationArea = All;
                }
                field("E-Mail U Escalation"; Rec."E-Mail U Escalation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Escalation', Comment = 'de-DE=Geben Sie einen Textvorlage an, der für die Nachricht verwendet werden soll. Sollte kein Textvorlage angegeben werden, wird der Eskalationsbaustein aus der Support E-Mail Einrichtung verwendet';
                    trigger OnAssistEdit()
                    var
                        ICISupportTextModule: Record "ICI Support Text Module";
                        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    begin
                        IF ICISupportMailMgt.GetEMailTextModule(Rec."E-Mail U Escalation", '', ICISupportTextModule) then
                            ICISupportMailMgt.AssistEditTextModule(ICISupportTextModule);
                    end;
                }
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec.FilterGroup(0); // From Lookup Flowfield or Runpagelink in Button
        IF Rec.GetFilter("Escalation Code") <> '' then
            Rec."Escalation Code" := COPYSTR(Rec.GetFilter("Escalation Code"), 1, 20);
    end;
}
