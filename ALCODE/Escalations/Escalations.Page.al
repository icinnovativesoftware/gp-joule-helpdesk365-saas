page 56346 "ICI Escalations"
{

    ApplicationArea = All;
    Caption = 'Escalations', Comment = 'de-DE=Eskalationen';
    PageType = List;
    SourceTable = "ICI Escalation";
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field("Target Time"; Rec."Target Time")
                {
                    ToolTip = 'Target Time', Comment = 'de-DE=Sollzeit. Nachdem ein Ticket länger als die angegebene Sollzeit offen ist, wird es in der Ticketliste rot angezeigt';
                    ApplicationArea = All;
                }
                field("No. of Escalation Level"; Rec."No. of Escalation Level")
                {
                    ToolTip = 'Specifies the value of the No. of Escalation Level', Comment = 'de-DE=Anz. Stufen';
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action(EscalationLevel)
            {
                ApplicationArea = All;
                Caption = 'Escalation Level', Comment = 'de-DE=Eskalationsstufen';
                ToolTip = 'Escalation Level', Comment = 'de-DE=Eskalationsstufen';
                Image = Stages;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                RunObject = Page "ICI Escalation Levels";
                RunPageLink = "Escalation Code" = FIELD(Code);

            }
        }
    }
}
