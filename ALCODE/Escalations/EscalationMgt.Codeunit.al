codeunit 56294 "ICI Escalation Mgt."
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    begin
        EscalationTimer();
        COMMIT();
    end;

    procedure EscalationTimer()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        ICIEscalationLevel: Record "ICI Escalation Level";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
    begin
        ICISupportSetup.GET();

        //SupportTicket.SETRANGE(State, SupportTicket.State::Open, SupportTicket.State::WaitForAcceptance);
        ICISupportTicket.SETFILTER("Escalation Code", '<>%1', '');
        IF ICISupportTicket.FINDSET() THEN
            REPEAT

                ICIEscalationLevel.SETRANGE("Escalation Code", ICISupportTicket."Escalation Code");
                IF ICIEscalationLevel.FINDSET() THEN
                    REPEAT
                        IF CheckTicketEscalationLevel(ICISupportTicket, ICIEscalationLevel) THEN begin
                            ICISupportTicketLog.SETRANGE("Support Ticket No.", ICISupportTicket."No.");
                            ICISupportTicketLog.SETRANGE(Type, ICISupportTicketLog.Type::Escalation);
                            ICISupportTicketLog.SetRange("Record ID", ICIEscalationLevel.RecordId());

                            IF ICISupportTicketLog.IsEmpty() THEN BEGIN //Escalation doesnt exist
                                CreateTicketEscalation(ICISupportTicket, ICIEscalationLevel);
                                COMMIT();
                            END;
                        END;
                    UNTIL ICIEscalationLevel.NEXT() = 0;
                COMMIT();
            UNTIL ICISupportTicket.NEXT() = 0;
    end;

    procedure CheckTicketEscalationLevel(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"): Boolean
    begin
        // Check Escalationlevel Condition
        EXIT(CheckTicketEscalationLevelGroup(SupportTicket, SupportEscalationLevel));

        // Case SupportEscalationLevel.Condition of
        //     SupportEscalationLevel.Condition::Processstage:// Too long in Process Stage
        //         EXIT(CheckTicketEscalationLevelStage(SupportTicket, SupportEscalationLevel));
        //     SupportEscalationLevel.Condition::Processstate:// Too long in Process State
        //         EXIT(CheckTicketEscalationLevelState(SupportTicket, SupportEscalationLevel));
        //     SupportEscalationLevel.Condition::Response:// Too long no Message
        //         EXIT(CheckTicketEscalationLevelResponse(SupportTicket, SupportEscalationLevel));
        // END;
    end;

    procedure CheckTicketEscalationLevelGroup(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"): Boolean
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        EscalationGroupLine: Record "ICI Escalation Group Line";
        ElapsedDuration: Duration;
    begin
        EscalationGroupLine.SetRange("Escalation Group Code", SupportEscalationLevel."Escalation Group Code");
        EscalationGroupLine.SETRANGE("Process Code", SupportTicket."Process Code");
        EscalationGroupLine.SETRANGE("Process Stage", SupportTicket."Process Stage");
        IF NOT EscalationGroupLine.FindFirst() THEN // process Stage of ticket is in escalation group
            Exit(false);

        ICISupportProcessStage.GET(EscalationGroupLine."Process Code", EscalationGroupLine."Process Stage");

        // Ticket and Escalationlevel are with same process and stage
        ICISupportTicketLog.SetRange("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::State);
        ICISupportTicketLog.SetRange("State Subtype", ICISupportProcessStage."Ticket State");
        ICISupportTicketLog.SetRange("Record ID", ICISupportProcessStage.RecordId());

        IF NOT ICISupportTicketLog.FINDLAST() THEN
            exit(false); // Ticket has not yet been in this stage

        ElapsedDuration := CURRENTDATETIME() - ICISupportTicketLog."Creation Timestamp";
        IF ElapsedDuration < SupportEscalationLevel."Start Time" then
            exit(false); // Duration until Escalation is not reached yet

        // Ticket is escalated - but maybe there has been a Message/File from Contact, which can reset the timer
        IF SupportEscalationLevel."Reset with Ticket Action" THEN begin
            ICISupportTicketLog.SetRange("Support Ticket No.", SupportTicket."No.");
            ICISupportTicketLog.SetFilter(Type, '%1|%2', ICISupportTicketLog.Type::"External Message", ICISupportTicketLog.Type::"External File");
            ICISupportTicketLog.SetRange("Created By Type", ICISupportTicketLog."Created By Type"::Contact);

            IF ICISupportTicketLog.FINDLAST() THEN begin
                ElapsedDuration := CURRENTDATETIME() - ICISupportTicketLog."Creation Timestamp"; // Elapsed time since last Contact Action (File or Message)
                IF ElapsedDuration < SupportEscalationLevel."Start Time" then
                    exit(false); // Duration until Escalation is not reached yet

            end;
        end;

        exit(true);
    end;


    // Depricated since 10.0.0.20
    [Obsolete('Only use CheckTicketEscalationLevelGroup', '10.0.0.20')]
    procedure CheckTicketEscalationLevelStage(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"): Boolean
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        ElapsedDuration: Duration;
    begin
        IF SupportEscalationLevel.Condition <> SupportEscalationLevel.Condition::Processstage THEN
            exit(false);

        IF SupportEscalationLevel."Ticket Process" = '' THEN
            EXIT;
        IF SupportTicket."Process Code" <> SupportEscalationLevel."Ticket Process" THEN
            exit(false);
        IF SupportTicket."Process Stage" <> SupportEscalationLevel."Ticket Process Stage" THEN
            exit(false);

        IF NOT ICISupportProcessStage.GET(SupportEscalationLevel."Ticket Process", SupportEscalationLevel."Ticket Process Stage") THEN
            exit(false);
        // Ticket and Escalationlevel are with same process and stage
        ICISupportTicketLog.SetRange("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::State);
        ICISupportTicketLog.SetRange("State Subtype", ICISupportProcessStage."Ticket State");
        ICISupportTicketLog.SetRange("Record ID", ICISupportProcessStage.RecordId());

        IF NOT ICISupportTicketLog.FINDLAST() THEN
            exit(false); // Ticket has not yet been in this stage

        ElapsedDuration := CURRENTDATETIME() - ICISupportTicketLog."Creation Timestamp";
        IF ElapsedDuration < SupportEscalationLevel."Start Time" then
            exit(false); // Duration until Escalation is not reached yet


        // XXX - Check if Escalation was already sent

        exit(true);

    end;

    // Depricated since 10.0.0.20
    [Obsolete('Only use CheckTicketEscalationLevelGroup', '10.0.0.20')]
    procedure CheckTicketEscalationLevelState(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"): Boolean
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ElapsedDuration: Duration;
    begin
        IF SupportEscalationLevel.Condition <> SupportEscalationLevel.Condition::Processstate THEN
            exit(false);
        IF SupportTicket."Ticket State" <> SupportEscalationLevel."Ticket State" THEN exit(false);

        ICISupportTicketLog.SetRange("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::State);
        ICISupportTicketLog.SetRange("State Subtype", SupportEscalationLevel."Ticket State");

        IF NOT ICISupportTicketLog.FINDLAST() THEN
            exit(false); // Ticket has not yet been in this stage

        ElapsedDuration := CURRENTDATETIME() - ICISupportTicketLog."Creation Timestamp";
        IF ElapsedDuration < SupportEscalationLevel."Start Time" then
            exit(false); // Duration until Escalation is not reached yet


        // XXX - Check if Escalation was already sent

        exit(true);

    end;

    // Depricated since 10.0.0.20
    [Obsolete('Only use CheckTicketEscalationLevelGroup', '10.0.0.20')]
    procedure CheckTicketEscalationLevelResponse(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"): Boolean
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ElapsedDuration: Duration;
    begin
        IF SupportEscalationLevel.Condition <> SupportEscalationLevel.Condition::Processstate THEN
            exit(false);
        IF SupportTicket."Ticket State" <> SupportEscalationLevel."Ticket State" THEN exit(false);

        ICISupportTicketLog.SetRange("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.SetFilter(Type, '%1|%2', ICISupportTicketLog.Type::"External Message", ICISupportTicketLog.Type::"External File");
        ICISupportTicketLog.SetRange("Created By Type", ICISupportTicketLog."Created By Type"::Contact);

        IF NOT ICISupportTicketLog.FINDLAST() THEN
            exit(false); // No message of file sent yet

        ElapsedDuration := CURRENTDATETIME() - ICISupportTicketLog."Creation Timestamp";
        IF ElapsedDuration < SupportEscalationLevel."Start Time" then
            exit(false); // Duration until Escalatio nis not reached yet

        // XXX - Check if Escalation was already sent

        exit(true);
    end;

    procedure CreateTicketEscalation(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level")
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        lOutStream: OutStream;
    begin
        // Create Log Entry
        ICISupportTicketLog.Init();
        ICISupportTicketLog.insert(true);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.VALIDATE(Type, ICISupportTicketLog.Type::Escalation);
        ICISupportTicketLog.VALIDATE("Record ID", SupportEscalationLevel.RecordId());
        ICISupportTicketLog.Validate("Created By Type", ICISupportTicketLog."Created By Type"::System);
        ICISupportTicketLog.Validate("Data Text", SupportTicket.Description);
        ICISupportTicketLog.Validate("Data Text 2", SupportTicket."Comp. Contact Name");

        IF SupportEscalationLevel."Receiver Type" = SupportEscalationLevel."Receiver Type"::"Current User" then
            ICISupportTicketLog.Validate("Escalation User", SupportTicket."Support User ID")
        else
            ICISupportTicketLog.Validate("Escalation User", SupportEscalationLevel."Support User");

        ICISupportTicketLog.Modify(true);

        ICISupportTicketLog.CalcFields(Data);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream);
        lOutStream.Write(ICISupportTicketLogMgt.GetTicketLogDescription(ICISupportTicketLog."Entry No."));
        ICISupportTicketLog.Modify(true);

        // Send Mail/Notififcation/SMS
        IF SupportEscalationLevel."Send E-Mail" then
            SendEscalationMail(SupportTicket, SupportEscalationLevel, ICISupportTicketLog);
        IF SupportEscalationLevel."Send Notification" then
            SendEscalationNotification(SupportTicket, SupportEscalationLevel, ICISupportTicketLog);
        IF SupportEscalationLevel."Send Notification" then
            ERROR('Not Licensed');
        IF SupportEscalationLevel."Send Link" THEN
            SendRecordLink(SupportTicket, SupportEscalationLevel, ICISupportTicketLog);
    end;

    procedure SendEscalationMail(ICISupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"; var ICISupportTicketLog: Record "ICI Support Ticket Log")
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        EmptyRecordId: RecordId;
        lInStream: InStream;
        Receiver: Text;
        MailSubject: Text;
        MailBody: Text;
        LinkToTicket: Text;
    begin
        ICISupportMailSetup.GET();

        IF SupportEscalationLevel."Receiver Type" = SupportEscalationLevel."Receiver Type"::"Current User" then
            ICISupportUser.GET(ICISupportTicket."Support User ID")
        else
            ICISupportUser.GET(SupportEscalationLevel."Support User");

        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail U Escalation", ICISupportUser."Language Code", ICISupportTextModule);
        IF SupportEscalationLevel."E-Mail U Escalation" <> '' then
            ICISupportMailMgt.GetEMailTextModule(SupportEscalationLevel."E-Mail U Escalation", ICISupportUser."Language Code", ICISupportTextModule); // Textmodule from Escalationlevel > SupportMailSetup

        Receiver := ICISupportUser."E-Mail";

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, ICISupportTicket."Current Contact No.");
        ICISupportMailMgt.GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetEscalationPlaceholders(TempNameValueBuffer, SupportEscalationLevel);

        // Escalation Mails only for Users with access to Business Central
        LinkToTicket := System.GetUrl(ClientType::Web, CompanyName(), ObjectType::Page, Page::"ICI Support Ticket Card", ICISupportTicket);
        TempNameValueBuffer.AddNewEntry('%link_to_ticket%', LinkToTicket);

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICISupportMailMgt.SendMail(Receiver, MailSubject, MailBody, '', '', '', '', ICISupportTicket.RecordId(), EmptyRecordId); // TicketNo is available, but Escalation Mails are always supposed to be sent from Default E-Mail
    end;

    procedure SendEscalationNotification(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"; var ICISupportTicketLog: Record "ICI Support Ticket Log")
    var
        lNotification: Notification;
        OpenTicketLbl: Label 'Open Card';
        lInStream: InStream;
        LogText: Text;
    begin
        ICISupportTicketLog.CalcFields(Data);
        ICISupportTicketLog.Data.CreateInStream(lInStream);
        lInStream.Read(LogText);
        lNotification.Message(LogText);
        // Notification.Scope := NotificationScope::GlobalScope;         // Future TODO - Globalscope is not yet supported
        lNotification.SetData('TicketNo', SupportTicket."No.");
        lNotification.AddAction(OpenTicketLbl, Codeunit::"ICI Support Action Handler", 'OpenTicketCard');
        lNotification.Send();

    end;

    procedure isTicketOverdue(var SupportTicket: Record "ICI SUpport Ticket"): Boolean
    var
        Escalation: Record "ICI Escalation";
        DueDatetime: DateTime;
    begin
        if SupportTicket."Escalation Code" = '' THEN
            EXIT(false);

        IF NOT Escalation.GET(SupportTicket."Escalation Code") THEN
            exit(false);

        IF (Escalation."Target Time" = 0) THEN
            EXIT(false);

        IF (SupportTicket."Ticket State" = SupportTicket."Ticket State"::Closed) then // Geschlossene Tickets eskalieren nicht
            Exit(false);

        SupportTicket.CalcFields("First Opened On");
        IF SupportTicket."First Opened On" = 0DT then
            EXIT(false);

        DueDatetime := SupportTicket."First Opened On" + Escalation."Target Time"; // Calculate DueDate

        IF DueDatetime > CurrentDateTime THEN // Noch nicht eskaliert, eskalationsdatum liegt in der zukunft
            EXIT(false);

        exit(true);
    end;


    procedure SendRecordLink(SupportTicket: Record "ICI Support Ticket"; SupportEscalationLevel: Record "ICI Escalation Level"; var ICISupportTicketLog: Record "ICI Support Ticket Log")
    var
        RecordLink: Record "Record Link";
        NoteOutStream: OutStream;
        Link: Text[2048];
        Message: Text;
        ToUser: Code[50];
        lInStream: InStream;
    begin
        ICISupportTicketLog.CalcFields(Data);
        ICISupportTicketLog.Data.CreateInStream(lInStream);
        lInStream.Read(Message);

        Link := COPYSTR(System.GetUrl(ClientType::Web, CompanyName(), ObjectType::Page, Page::"ICI Support Ticket Card", SupportTicket), 1, 2048);

        ToUser := SupportTicket."Support User ID";
        IF SupportEscalationLevel."Support User" <> '' THEN
            ToUser := SupportEscalationLevel."Support User";

        RecordLink.INIT();
        RecordLink.INSERT(true);
        RecordLink.URL1 := Link;
        RecordLink.Type := RecordLink.Type::Note;

        RecordLink.Note.CREATEOUTSTREAM(NoteOutStream);
        NoteOutStream.WRITE(Message);

        RecordLink.Created := CURRENTDATETIME();
        RecordLink."User ID" := COPYSTR(USERID(), 1, 132);
        RecordLink."To User ID" := ToUser;
        RecordLink.Company := COPYSTR(COMPANYNAME(), 1, 30);
        RecordLink.Notify := TRUE;
        RecordLink.MODIFY(TRUE);
    end;


    procedure GetEscalationPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary; var EscalationLevel: Record "ICI Escalation Level")
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        EscalationGroup: Record "ICI Escalation Group";
    begin
        ICISupportMailSetup.GET();
        NameValueBuffer.AddNewEntry('%escalation_level_start_time%', FORMAT(EscalationLevel."Start Time"));
        IF NOT EscalationGroup.GET(EscalationLevel."Escalation Group Code") THEN;
        NameValueBuffer.AddNewEntry('%escalation_level_group_code%', EscalationLevel."Escalation Group Code");
        NameValueBuffer.AddNewEntry('%escalation_level_group_description%', EscalationGroup.Description);

        // NameValueBuffer.AddNewEntry('%escalation_level_condition%', FORMAT(EscalationLevel.Condition));
        // NameValueBuffer.AddNewEntry('%escalation_level_ticket_process%', FORMAT(EscalationLevel."Ticket Process"));
        // NameValueBuffer.AddNewEntry('%escalation_level_ticket_process_stage%', FORMAT(EscalationLevel."Ticket Process Stage"));
        // NameValueBuffer.AddNewEntry('%escalation_level_ticket_state%', FORMAT(EscalationLevel."Ticket State"));
    end;
}
