page 56373 "ICI Escalation Group Lines"
{

    Caption = 'Escalation Group Lines', Comment = 'de-DE=Zeilen';
    PageType = ListPart;
    SourceTable = "ICI Escalation Group Line";
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Line No."; Rec."Line No.")
                {
                    ToolTip = 'Specifies the value of the Line No. field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Process Code"; Rec."Process Code")
                {
                    ToolTip = 'Specifies the value of the Process Code field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Process Stage"; Rec."Process Stage")
                {
                    ToolTip = 'Specifies the value of the Process Stage field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Process Description"; Rec."Process Description")
                {
                    ToolTip = 'Specifies the value of the Process Description field.', Comment = 'de-DE=Prozessbeschreibung';
                    ApplicationArea = All;
                }
                field("Process Stage Description"; Rec."Process Stage Description")
                {
                    ToolTip = 'Specifies the value of the Process Stage Description field.', Comment = 'de-DE=Prozessstufenbeschreibung';
                    ApplicationArea = All;
                }
            }
        }
    }
}
