table 56305 "ICI Escalation Level"
{
    Caption = 'Escalation Level', Comment = 'de-DE=Eskalationsstufe';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Escalation Levels";
    DrillDownPageId = "ICI Escalation Levels";

    fields
    {
        field(1; "Escalation Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Escalation Code', Comment = 'de-DE=Eskalationscode';
            NotBlank = true;
            TableRelation = "ICI Escalation".Code;
        }
        field(2; "Line No."; Integer)
        {
            DataClassification = SystemMetadata;
            Caption = 'Line No', Comment = 'de-DE=Zeilennr.';
        }
        field(11; "Start Time"; Duration)
        {
            DataClassification = CustomerContent;
            Caption = 'Start Time', Comment = 'de-DE=Zeitpunkt';
            NotBlank = true;
        }
        // field(12; Type; Option)
        // {
        //     DataClassification = CustomerContent;
        //     Caption = 'Type', Comment = 'de-DE=Art';
        //     OptionCaption = 'E-Mail,Notification,SMS', Comment = 'de-DE=E-Mail,Benachrichtigung,SMS';
        //     OptionMembers = "E-Mail",Notification,SMS;
        // }
        field(13; "Receiver Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Receiver Type', Comment = 'de-DE=Empfängerart';
            OptionCaption = 'Current User,Escalation User', Comment = 'de-DE=Aktueller Benutzer,Abweichender Benutzer';
            OptionMembers = "Current User","Escalation User";
        }

        field(14; "Support User"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Support User', Comment = 'de-DE=Benutzer';
            TableRelation = "ICI Support User";
        }
        field(15; Condition; Option)
        {

            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Escalation Groups + Reset with Ticket Action';
            DataClassification = CustomerContent;
            Caption = 'Condition', Comment = 'de-DE=Bedingung';
            OptionCaption = 'too long in Process Stage,too long no Respone,too long in Ticket State', Comment = 'de-DE=zu lange in Prozessstufe,zu lange keine Antwort,zu lange in Ticketstatus';
            OptionMembers = Processstage,Response,Processstate;
        }

        field(16; "Ticket State"; Enum "ICI Ticket State")
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Escalation Groups';
            DataClassification = CustomerContent;
            Caption = 'Ticket State', Comment = 'de-DE=Ticketstatus';
        }
        field(17; "Ticket Process"; Code[20])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Escalation Groups';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Process";
            Caption = 'Ticket Process', Comment = 'de-DE=Prozess';
        }
        field(18; "Ticket Process Stage"; Integer)
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Escalation Groups';
            DataClassification = CustomerContent;
            Caption = 'Ticket Process', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Ticket Process"));
        }

        field(19; "Send E-Mail"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Send E-Mail', Comment = 'de-DE=E-Mail versenden';
        }
        field(20; "Send SMS"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Send SMS', Comment = 'de-DE=SMS versenden';
        }
        field(21; "Send Notification"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Send Note', Comment = 'de-DE=Notiz versenden';
        }
        field(22; "Send Link"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Send Link', Comment = 'de-DE=Link versenden';
        }
        field(23; "E-Mail U Escalation"; Code[30])
        {
            Caption = 'E-Mail U Escalation', Comment = 'de-DE=E-Mail Textvorlage';
            DataClassification = CustomerContent;
            TableRelation = "ICI Support Text Module" where("Type" = const("E-Mail"));
        }
        field(25; "Escalation Group Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Escalation Group Code', Comment = 'de-DE=Eskalationsgruppe';
            TableRelation = "ICI Escalation Group";
        }
        field(26; "Reset with Ticket Action"; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Reset with Ticket Action', Comment = 'de-DE=Nach letzter Aktion';
        }
    }
    keys
    {
        key(Key1; "Escalation Code", "Line No.")
        {
            Clustered = true;
        }
    }
}
