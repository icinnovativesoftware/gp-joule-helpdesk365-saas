page 56321 "ICI Support Time Log Subpage"
{
    Caption = 'Time Log', Comment = 'de-DE=Zeiterfassung';
    DelayedInsert = true;
    PageType = ListPart;
    SourceTable = "ICI Support Time Log Line";
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(Rep)
            {
                field("Support Ticket No."; Rec."Support Ticket No.")
                {
                    ToolTip = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
                    ApplicationArea = All;
                    Visible = false;

                }
                field("Line No."; Rec."Line No.")
                {

                    ToolTip = 'Line No', Comment = 'de-DE=Zeilennr.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Support User"; Rec."Support User")
                {
                    ToolTip = 'Support User', Comment = 'de-DE=Supportbenutzer';
                    ApplicationArea = All;
                    trigger OnValidate()
                    var
                        ICISupportUser: Record "ICI Support User";
                    begin
                        Rec.Calcfields("Sales Payoff Type");
                        IF ICISupportUser.GET(Rec."Support User") then
                            Rec."Sales Payoff No." := ICISupportUser."Sales Payoff No.";
                    end;
                }
                field(Date; Rec.Date)
                {
                    ToolTip = 'Date', Comment = 'de-DE=Datum';
                    ApplicationArea = All;
                }
                field("Time from"; Rec."Time from")
                {
                    ToolTip = 'Time from', Comment = 'de-DE=Zeit von';
                    ApplicationArea = All;
                }
                field("Time to"; Rec."Time to")
                {
                    ToolTip = 'Time to', Comment = 'de-DE=Zeit bis';
                    ApplicationArea = All;
                }
                field("Time Decimal"; Rec."Time Decimal")
                {
                    Caption = 'Duration', Comment = 'de-DE=Dauer';
                    ToolTip = 'Duration', Comment = 'de-DE=Dauer';
                    ApplicationArea = All;
                    Editable = true;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field(Accounting; Rec."Accounting Type")
                {
                    ToolTip = 'Accounting', Comment = 'de-DE=Abrechnung';
                    ApplicationArea = All;
                }
                field("Sales Payoff Type"; Rec."Sales Payoff Type")
                {
                    ToolTip = 'Abrechnungsart', Comment = 'de-DE=Abrechnungsart';
                    ApplicationArea = All;
                }
                field("Sales Payoff No."; Rec."Sales Payoff No.")
                {
                    ToolTip = 'Abrechnungsnr.', Comment = 'de-DE=Abrechnungsnr.';
                    ApplicationArea = All;
                }
                field("Work Type Code"; Rec."Work Type Code")
                {
                    Visible = ShowWorkType;
                    ToolTip = 'Work Type Code', Comment = 'de-DE=Arbeitstypcode';
                    ApplicationArea = All;
                }

                field("No. of Comment Lines"; Rec."No. of Comment Lines")
                {
                    BlankZero = true;
                    ToolTip = 'No. of Comment Lines', Comment = 'de-DE=Anzahl Kommentarzeilen';
                    ApplicationArea = All;
                }
                field("Sales Doc No."; Rec."Sales Doc No.")
                {
                    ToolTip = 'Sales Doc No.', Comment = 'de-DE=VK Belegsnr.';
                    ApplicationArea = All;

                    trigger OnAssistEdit()
                    begin
                        OpenDoc();
                    end;
                }
                field("Service Doc No."; Rec."Service Doc No.")
                {
                    ToolTip = 'Service Doc No.', Comment = 'de-DE=Service Belegsnr.';
                    ApplicationArea = All;

                    trigger OnAssistEdit()
                    begin
                        OpenDoc();
                    end;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ApplicationArea = All;
                    Visible = false;

                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Sales Document")
            {
                Caption = 'Show Document', Comment = 'de-DE=Beleg anzeigen';
                ToolTip = 'Show Document', Comment = 'de-DE=Beleg anzeigen';
                ApplicationArea = All;
                Image = SalesInvoice;

                trigger OnAction()
                begin
                    OpenDoc();
                end;
            }
            action(Comment)
            {
                ApplicationArea = All;
                ToolTip = 'Comments';
                Caption = 'Comments', Comment = 'de-DE=Kommentare';
                Image = Comment;
                RunObject = Page "ICI Support Time Log Comments";
                RunPageLink = "Support Ticket No." = field("Support Ticket No."), "Time Log Line No." = Field("Line No.");
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    var
        ICISupportUser: Record "ICI Support User";
    begin
        Rec.FilterGroup(4); // SubpageLink from Ticket Card
        IF Rec.GetFilter("Support Ticket No.") <> '' then
            TicketNoFilter := COPYSTR(Rec.GetFilter("Support Ticket No."), 1, 20);


        IF SupportTicket.GET(TicketNoFilter) THEN BEGIN
            IF ICISupportUser.GetCurrUser(ICISupportUser) then
                Rec."Employee No." := ICISupportUser."Employee No.";

            Rec."Support User" := ICISupportUser."User ID";
            Rec."Sales Payoff No." := ICISupportUser."Sales Payoff No.";

            Rec."Customer No." := SupportTicket."Customer No.";
            Rec."Support Ticket No." := SupportTicket."No.";
            Rec.Date := TODAY;
            Rec."Time from" := TIME;
            Rec.Description := COPYSTR(SupportTicket.Description, 1, MaxStrLen(Rec.Description));
            Rec."Accounting Type" := SupportTicket."Accounting Type";
            Rec.CalcFields("Sales Payoff Type");
        END;
    end;

    trigger OnAfterGetRecord()
    var
        ICISupportUser: Record "ICI Support User";
    begin
        IF Rec."Sales Payoff No." = '' THEN
            IF ICISupportUser.GET(Rec."Support User") then
                IF ICISupportUser."Sales Payoff No." <> '' THEN begin
                    Rec."Sales Payoff No." := ICISupportUser."Sales Payoff No.";
                    Rec.MODIFY();
                end;
    end;

    trigger OnOpenPage()
    var
        ICISupportUser: Record "ICI Support User";
    begin
        ICISupportUser.SetRange("Sales Payoff Type", ICISupportUser."Sales Payoff Type"::Resource);
        ShowWorkType := ICISupportUser.Count() > 0;
    end;

    var
        SupportTicket: Record "ICI Support Ticket";
        TicketNoFilter: Code[20];

    // procedure SetTicketFilter(TicketNo: Code[20])
    // begin
    //     SETRANGE("Ticket No.", TicketNo);
    //     TicketNoFilter := TicketNo;
    //     CurrPage.UPDATE(FALSE);
    // end;

    // procedure UpdateForm()
    // begin
    //     CurrPage.UPDATE(FALSE);
    // end;

    // local procedure TimetoOnAfterInput(var Text: Text[1024])
    // begin
    //     IF Text <> '' THEN BEGIN
    //         IF UPPERCASE(Text) = 'T' THEN
    //             Text := FORMAT(TIME);
    //     END;
    // end;

    local procedure OpenDoc()
    var
        SalesHeader: Record "Sales Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        ServiceHeader: Record "Service Header";
        ServiceInvoiceHeader: Record "Service Invoice Header";
    begin
        IF Rec."Sales Doc No." <> '' THEN BEGIN
            //Ungebuchter Beleg
            SalesHeader.RESET();
            SalesHeader.SETFILTER("Document Type", '%1|%2', SalesHeader."Document Type"::Order, SalesHeader."Document Type"::Invoice);
            SalesHeader.SETRANGE("No.", Rec."Sales Doc No.");
            IF SalesHeader.FINDLAST() THEN BEGIN
                SalesHeader.SETRECFILTER();
                IF SalesHeader."Document Type" = SalesHeader."Document Type"::Order THEN
                    PAGE.RUN(PAGE::"Sales Order", SalesHeader)
                ELSE
                    PAGE.RUN(PAGE::"Sales Invoice", SalesHeader);
                EXIT;
            END;

            //Geb. VK-Rechnung
            SalesInvoiceHeader.RESET();
            SalesInvoiceHeader.SETCURRENTKEY("Pre-Assigned No.");
            SalesInvoiceHeader.SETRANGE("Pre-Assigned No.", Rec."Sales Doc No.");
            IF SalesInvoiceHeader.FINDLAST() THEN BEGIN
                SalesInvoiceHeader.SETRECFILTER();
                PAGE.RUN(PAGE::"Posted Sales Invoice", SalesInvoiceHeader);
            END ELSE BEGIN
                SalesInvoiceHeader.RESET();
                SalesInvoiceHeader.SETCURRENTKEY("Order No.");
                SalesInvoiceHeader.SETRANGE("Order No.", Rec."Sales Doc No.");
                IF SalesInvoiceHeader.FINDLAST() THEN BEGIN
                    SalesInvoiceHeader.SETRECFILTER();
                    PAGE.RUN(PAGE::"Posted Sales Invoice", SalesInvoiceHeader);
                END;
            END;
        END;

        IF Rec."Service Doc No." <> '' THEN BEGIN
            //Ungebuchter Beleg
            ServiceHeader.RESET();
            ServiceHeader.SETFILTER("Document Type", '%1|%2', ServiceHeader."Document Type"::Order, ServiceHeader."Document Type"::Invoice);
            ServiceHeader.SETRANGE("No.", Rec."Service Doc No.");
            IF ServiceHeader.FINDLAST() THEN BEGIN
                ServiceHeader.SETRECFILTER();
                IF ServiceHeader."Document Type" = ServiceHeader."Document Type"::Order THEN
                    PAGE.RUN(PAGE::"Service Order", ServiceHeader)
                ELSE
                    PAGE.RUN(PAGE::"Service Invoice", ServiceHeader);
                EXIT;
            END;

            //Geb. VK-Rechnung
            ServiceInvoiceHeader.RESET();
            ServiceInvoiceHeader.SETCURRENTKEY("Pre-Assigned No.");
            ServiceInvoiceHeader.SETRANGE("Pre-Assigned No.", Rec."Service Doc No.");
            IF ServiceInvoiceHeader.FINDLAST() THEN BEGIN
                ServiceInvoiceHeader.SETRECFILTER();
                PAGE.RUN(PAGE::"Posted Service Invoice", ServiceInvoiceHeader);
            END ELSE BEGIN
                ServiceInvoiceHeader.RESET();
                ServiceInvoiceHeader.SETCURRENTKEY("Order No.");
                ServiceInvoiceHeader.SETRANGE("Order No.", Rec."Service Doc No.");
                IF ServiceInvoiceHeader.FINDLAST() THEN BEGIN
                    Rec.SETRECFILTER();
                    PAGE.RUN(PAGE::"Posted Service Invoice", ServiceInvoiceHeader);
                END;
            END;
        END;
    end;

    var
        ShowWorkType: Boolean;
}
