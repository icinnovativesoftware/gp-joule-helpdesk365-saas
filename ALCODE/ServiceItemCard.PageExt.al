pageextension 56298 "ICI Service Item Card" extends "Service Item Card"
{
    layout
    {
        addafter("No.")
        {
            field("ICI HD No. 2"; Rec."ICI HD No. 2")
            {
                ApplicationArea = All;
                Visible = false;

            }
        }
        addfirst(factboxes)
        {
            part("ICI Service Item Dragbox"; "ICI Service Item Dragbox")
            {
                SubPageLink = "No." = field("No.");
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
            part("ICI Serv. Item Det. Factbox"; "ICI Serv. Item Ticket Factbox")
            {
                ApplicationArea = All;
                Visible = ShowServiceFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }

    actions
    {
        addlast(processing)
        {
            group(ICIHelpdeskActions)
            {
                Caption = 'Helpdesk 365', Comment = 'de-DE=Helpdesk 365';
                action(CreateTicket)
                {
                    Caption = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ToolTip = 'Create Ticket', Comment = 'de-DE=Ticket erstellen';
                    ApplicationArea = All;
                    Image = New;
                    trigger OnAction()
                    var
                        ICISupportTicket: Record "ICI Support Ticket";
                    begin
                        ICISupportTicket.Init();
                        ICISupportTicket.Insert(true);
                        ICISupportTicket.Validate("Service Item No.", Rec."No.");
                        ICISupportTicket.Validate(Description, Rec.Description);

                        ICISupportTicket.FillMissingContactOrCustomerInformation();
                        ICISupportTicket.Modify(true);

                        PAGE.RUN(PAGE::"ICI Support Ticket Card", ICISupportTicket);
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission THEN
            IF ICISupportSetup.GET() then
                ShowServiceFactbox := ICISupportSetup."Document Integration";
    end;

    var
        ShowServiceFactbox: Boolean;
}
