pageextension 56301 "ICI Customer List" extends "Customer List"
{
    layout
    {
        addfirst(factboxes)
        {
            part("ICI Customer Factbox"; "ICI Customer Factbox")
            {
                ApplicationArea = All;
                Visible = ShowCustomerFactbox;
                SubPageLink = "No." = field("No.");
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        IF ICISupportSetup.ReadPermission Then
            IF ICISupportSetup.GET() then
                ShowCustomerFactbox := true;
    end;

    var
        ShowCustomerFactbox: Boolean;
}
