page 56332 "ICI Portal Tokens"
{

    ApplicationArea = All;
    Caption = 'Portal Tokens', Comment = 'de-DE=Kundenportal Tokens';
    PageType = List;
    SourceTable = "ICI Portal Token";
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ToolTip = 'Specifies the value of the Entry No. field.', Comment = 'de-DE=Lfd. Nr.';
                    ApplicationArea = All;
                }
                field("Contact No."; Rec."Contact No.")
                {
                    ToolTip = 'Specifies the value of the Contact No. field.', Comment = 'de-DE=Kontaktnr.';
                    ApplicationArea = All;
                }
                field("Ticket No."; Rec."Ticket No.")
                {
                    ToolTip = 'Specifies the value of the Ticket No. field.', Comment = 'de-DE=Ticketnr.';
                    ApplicationArea = All;
                }
                field("Create Datetime"; Rec."Create Datetime")
                {
                    ToolTip = 'Specifies the value of the Create Datetime field.', Comment = 'de-DE=Erstellt am';
                    ApplicationArea = All;
                }
                field(Token; Rec.Token)
                {
                    ToolTip = 'Specifies the value of the Token field.', Comment = 'de-DE=Token';
                    ApplicationArea = All;
                }
            }
        }
    }

}
