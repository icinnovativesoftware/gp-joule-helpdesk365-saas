var InputArea
var Editor
var ColorContactMessage = "#ebebeb";
var ColorUserMessage = "#05728f";
var ColorInternalMessage = "red";
var DefaultInternally = false;
var isFullScreen = false;

function getTextColorForBackground(str){
    var match = str.match(/rgba?\((\d{1,3}), ?(\d{1,3}), ?(\d{1,3})\)?(?:, ?(\d(?:\.\d?))\))?/);
    if(match){
        red = match[1];
        green = match[2];
        blue = match[3];
        if ((red*0.299 + green*0.587 + blue*0.114) < 186) 
            return "#ffffff";
    }    
    return "#000000";
}

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

var TextModuleDict = [
    /*Erster: "Erster Textvorlage", 
    Zweiter: "Zweiter Textvorlage xxxx",
    1: "Zweiter Textvorlage xxxx",
    2: "Zweiter Textvorlage xxxx",
    3: "Zweiter Textvorlage xxxx",
    4: "Zweiter Textvorlage xxxx",
    5: "Zweiter Textvorlage xxxx",
    6: "Zweiter Textvorlage xxxx",
    8: "Zweiter Textvorlage xxxx",
    77: "Zweiter Textvorlage xxxx",
    9: "Zweiter Textvorlage xxxx",
    99: "Zweiter Textvorlage xxxx",
    Zw9eiter: "Zweiter Textvorlage xxxx",
    9999: "Zweiter Textvorlage xxxx",
    99999: "Zweiter Textvorlage xxxx",
     "Dritter Text": "Textvorlage nr 3 ist the best"
*/];

function Init(data){
    
    var div = document.getElementById("controlAddIn");
    if(data.FullScreen){
        isFullScreen = true;
        div.classList = "FullScreen";
        // code is from: https://github.com/ajkauffmann/ControlAddInSizeDemo/blob/master/ControlAddIn/Startup.js
        // ParentElement.Parentelement is now necessary since another wrapper was added
        var iframe = window.frameElement;

        iframe.parentElement.parentElement.style.display = 'flex';
        iframe.parentElement.parentElement.style.flexDirection = 'column';
        iframe.parentElement.parentElement.style.flexGrow = '1';
        // $(iframe.parentElement).closest('.ms-nav-layout-main .ms-nav-scrollable').css("padding-right","0px");
        $(iframe.parentElement).closest('.ms-nav-layout-main').css('padding-right','0px')

        iframe.style.removeProperty('height');
        iframe.style.removeProperty('min-height');
        iframe.style.removeProperty('max-height');

        iframe.style.flexGrow = '1';
        iframe.style.flexShrink = '1';
        iframe.style.flexBasis = 'auto';
        iframe.style.paddingBottom = '42px';
    }
    if(data.ColorContactMessage){
        ColorContactMessage = data.ColorContactMessage;
    }
    if(data.ColorUserMessage){
        ColorUserMessage = data.ColorUserMessage;
    }
    if(data.ColorInternalMessage){
        ColorInternalMessage = data.ColorInternalMessage;
    }
    if(data.DefaultInternally){
        DefaultInternally = data.DefaultInternally;
    }
    

    div.innerHTML = "";
    div.style.overflowY = "auto"; 
    div.style.overflowX = "hidden"; 
    // Chat History
    var HistoryDiv = document.createElement('div');
    HistoryDiv.setAttribute('id', 'LogHistoryContainer');
    div.appendChild(HistoryDiv);

    // Chat Input Editor
    InputArea = document.createElement("textarea");
    InputArea.id = "Comment";
    InputArea.name = "Comment";
    div.appendChild(InputArea);    

    //debugger;
    CKEDITOR.editorConfig = function( config ) {
        config.toolbarGroups = [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'colors', groups: [ 'colors' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'about', groups: [ 'about' ] }
        ];
    	// HD365 ---
        config.extraPlugins = 'text_module';
        // HD365 +++
        config.removeButtons = 'Source,Save,NewPage,ExportPdf,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Replace,Find,SelectAll,Scayt,Styles,Format,About,Maximize,ShowBlocks,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Unlink,Anchor,Language,BidiRtl,BidiLtr,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Blockquote,CreateDiv,Outdent,Indent,BulletedList,NumberedList,CopyFormatting,RemoveFormat,Subscript,Superscript,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Form';
    };



    CKEDITOR.replace( 'Comment', {
        /*customConfig: '',
        customConfig: false, //no config.js
        stylesSet: false, //no styles.js
        defaultLanguage: 'de', //default language
        language: 'de', //ui language*/
    });

    // Intern/Extern switch
    var SwitchAndSendContainer = document.createElement('div');
    SwitchAndSendContainer.id = "SwitchAndSendContainer";
    var SwitchLabel = document.createElement('label');
    SwitchLabel.classList += "switch";
    var SwitchInput = document.createElement('input');
    SwitchInput.type = "checkbox";
    SwitchInput.id = "Internally";
    SwitchInput.onchange = function(){
        var SendButton = document.getElementById("SendButton");
        if(this.checked){
            SendButton.innerText = "Intern Senden";
            //SendButton.style.backgroundColor="red";
            //SendButton.style.backgroundColor="#747474";
            if(ColorInternalMessage){
                //SendButton.style.backgroundColor=ColorInternalMessage;
            }
        } else {
            SendButton.innerText = "Senden";
            //SendButton.style.backgroundColor="#747474";
        }
    };
    var SwitchSpan = document.createElement('span');
    SwitchSpan.classList += "slider round";
    SwitchLabel.appendChild(SwitchInput);
    SwitchLabel.appendChild(SwitchSpan);

    SwitchAndSendContainer.appendChild(SwitchLabel);

    // Send Button
    var SendButton = document.createElement('button');
    SendButton.id = "SendButton";
    SendButton.innerText="Senden";
    // Default Internally
    if(DefaultInternally){
        SendButton.innerText = "Intern Senden";
        //SendButton.style.backgroundColor="red";
        if(ColorInternalMessage){
            //SendButton.style.backgroundColor=ColorInternalMessage;
        }
        SwitchInput.checked = true;
    }

    SendButton.onclick = function(){
        var editorData = CKEDITOR.instances.Comment.getData();
        if(editorData == ""){
            return;
        }
        RequestSave();
        Load('');
    };
    SwitchAndSendContainer.appendChild(SendButton);
    div.appendChild(SwitchAndSendContainer);
  
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("OnAfterInit",[]);
}

// Chat Editor
function Load(data) {
    CKEDITOR.instances.Comment.setData(data);
}

function RequestSave() {
    //debugger;
    var editorData = CKEDITOR.instances.Comment.getData();
    var internally = document.getElementById('Internally').checked;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("SaveRequested",[{Text: editorData,Internally:internally}]);
}
function SetReadOnly(readonly)
{
    // setReadOnly
    Editor.isReadOnly = readonly;
}

// Chat History
function LoadChat(Data){
    // debugger;
    var container = document.getElementById("LogHistoryContainer");
    container.innerHTML = '';
    

    for (i = 0; i <= Data.length - 1; i++) {
        logDiv = createDataDiv(Data[i]);
        if (logDiv != null){
            container.appendChild(logDiv);
        }
    }
    //debugger;
    setTimeout(scrollDown, 100);
}

function LoadTicketTextModule(Data){
    //debugger;
    for (i = 0; i <= Data.length - 1; i++) {
        //TextModuleDict[Data[i].Key] = Data[i].Value;

        TextModuleDict.push({
            key:   Data[i].Key,
            value: Data[i].Value
        });

    }
}

function scrollDown(){
    document.getElementById("controlAddIn").scrollTo(0,document.getElementById("controlAddIn").scrollHeight); 
}

function createDataDiv(Data){
    switch(Data.Type){
        case 0: return createStateDiv(Data); // State Change
        case 1: return createExternalMsgDiv(Data); // Chat Message
        case 2: return createFileDiv(Data); // File External
        //case 3: return createDataMsgDiv(Data); // Escalation
        //case 4: return createDataMsgDiv(Data); // Document
        //case 5: return createDataMsgDiv(Data); // External
        case 6: return createInternalMsgDiv(Data); // Chat Message
        case 7: return createInternalFileDiv(Data); // Internal File
        //case 8: This TimeRegistration is depricated
        case 9: return createStateDiv(Data); // Forwarding

    }

}
function createExternalMsgDiv(Data){
        // Data Structure:
    // ('Type', ICISupportTicketLog.Type.AsInteger());
    // ('CreatedBy', ICISupportTicketLog."Created By");
    // ('CreatedByType', ICISupportTicketLog."Created By Type");
    // ('CreationDate', ICISupportTicketLog."Creation Date");
    // ('EntryNo', ICISupportTicketLog."Entry No.");
    // ('Description',
    // CreationTime
    // Data
    // incoming or outgiong msg
    var msgTypeContainer = '';
    var msgTypeContent = '';
    var msgColor = '';
    switch(Data.CreatedByType){
        case 0: 
            msgTypeContainer = 'outgoing_msg'; 
            msgTypeContent = 'sent_msg_content'; 
            msgColor = ColorUserMessage;
            break; // BC User has sent the Message 
        case 1: 
            msgTypeContainer = 'incoming_msg'; 
            msgTypeContent = 'received_msg_content'; 
            msgColor = ColorContactMessage;
            break; // Contact has sent the Message
        default: 
            msgTypeContainer = 'system_msg'; 
            msgTypeContent = 'system_msg_content'; 
            break; // System generated Message
    }

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    var MsgContent = Data.Data;
    //var SenderInformation =  Data.CreatedBy + " | " + Data.CreatedByName;
    //var SenderInformation = DateString + " | " + Data.CreatedByName
    //var SenderInformation = Data.CreatedByName  + " schrieb am " + DateString;
    var SenderInformation = Data.SenderInformation;;

    var container = document.createElement('div');
    container.className = msgTypeContainer;

    var msgDiv = document.createElement('div');
    msgDiv.className = msgTypeContent;
    
    var contentDiv = document.createElement('div');
    contentDiv.style.marginTop="0px";
    contentDiv.style.marginBottom="0px";
    
    contentDiv.innerHTML = MsgContent;
    if(msgColor){
        contentDiv.style.backgroundColor = msgColor;
    }
    if(!isHTML(MsgContent)){ 
        contentDiv.innerText = MsgContent;
    }

    var timeDateSpan = document.createElement('span');
    timeDateSpan.className = 'time_date';
    timeDateSpan.innerHTML = DateString;

    var SenderInformationSpan = document.createElement('span');
    SenderInformationSpan.className = 'sender_information';
    SenderInformationSpan.innerHTML = SenderInformation;
    
    msgDiv.appendChild(SenderInformationSpan);
    msgDiv.appendChild(contentDiv);
    //msgDiv.appendChild(timeDateSpan);
    
    container.appendChild(msgDiv);
    return container;
}
function createInternalMsgDiv(Data){
        // Data Structure:
    // ('Type', ICISupportTicketLog.Type.AsInteger());
    // ('CreatedBy', ICISupportTicketLog."Created By");
    // ('CreatedByType', ICISupportTicketLog."Created By Type");
    // ('CreationDate', ICISupportTicketLog."Creation Date");
    // ('EntryNo', ICISupportTicketLog."Entry No.");
    // ('Description',
    // CreationTime
    // Data
    // incoming or outgiong msg
    var msgTypeContainer = '';
    var msgTypeContent = '';
    switch(Data.CreatedByType){
        case 0: msgTypeContainer = 'outgoing_msg'; msgTypeContent = 'sent_msg_content'; break; // BC User has sent the Message 
        case 1: msgTypeContainer = 'incoming_msg'; msgTypeContent = 'received_msg_content'; break; // Contact has sent the Message
        default: msgTypeContainer = 'system_msg'; msgTypeContent = 'system_msg_content'; break; // System generated Message
    }

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    var MsgContent = Data.Data;
    //var SenderInformation =  Data.CreatedBy + " | " + Data.CreatedByName;
    //var SenderInformation = DateString + " | " + Data.CreatedByName;
    //var SenderInformation = Data.CreatedByName  + " schrieb am " + DateString;
    var SenderInformation = Data.SenderInformation;;
    var container = document.createElement('div');
    container.className = msgTypeContainer;

    var msgDiv = document.createElement('div');
    msgDiv.className = msgTypeContent;
    
    var contentDiv = document.createElement('div');
    contentDiv.id = "IntMsgContentP";
    contentDiv.style.marginTop="0px";
    contentDiv.style.marginBottom="0px";
    //contentP.style.border="solid";
    if(ColorInternalMessage){
        contentDiv.style.backgroundColor=ColorInternalMessage;
    }
    
    contentDiv.innerHTML = MsgContent;

    if(!isHTML(MsgContent)){ 
        contentDiv.innerText = MsgContent;
    }
    

    var timeDateSpan = document.createElement('span');
    timeDateSpan.className = 'time_date';
    timeDateSpan.innerHTML = DateString;

    var SenderInformationSpan = document.createElement('span');
    SenderInformationSpan.className = 'sender_information';
    SenderInformationSpan.innerHTML = SenderInformation;
    
    msgDiv.appendChild(SenderInformationSpan);
    msgDiv.appendChild(contentDiv);
    //msgDiv.appendChild(timeDateSpan);
    
    container.appendChild(msgDiv);
    return container;
}
function createStateDiv(Data){
    // Data Structure:
// ('Type', ICISupportTicketLog.Type.AsInteger());
// ('CreatedBy', ICISupportTicketLog."Created By");
// ('CreatedByType', ICISupportTicketLog."Created By Type");
// ('CreationDate', ICISupportTicketLog."Creation Date");
// ('EntryNo', ICISupportTicketLog."Entry No.");
// ('Description',
    var container = document.createElement('div');
    container.className = 'state_row';

    container.innerHTML = Data.SenderInformation;
    // var col1 = document.createElement('div');
    // col1.className = 'state_column_1';
    // var col2 = document.createElement('div');
    // col2.className = 'state_column_2';
    // var col3 = document.createElement('div');
    // col3.className = 'state_column_3';

    // var DateString =  Data.CreationDate + "   " + Data.CreationTime;
    // var MsgContent = Data.Data;
    // var SenderInformation =  Data.CreatedByName;

    // col1.innerHTML = DateString;
    // col2.innerHTML = MsgContent;
    // col3.innerHTML = SenderInformation;

    // container.appendChild(col1);
    // container.appendChild(col2);
    // container.appendChild(col3);

    return container;

}

var DELAY = 300,
clicks = 0,
timer = null;

function onFileClicked(){
    clicks++;  //count clicks
    if(clicks === 1) {
        timer = setTimeout(function() {
            clicks = 0;  //after action performed, reset counter
        }, DELAY);
    } else {
        clearTimeout(timer);  //prevent single-click action
        clicks = 0;  //after action performed, reset counter
        //console.log("click " + this.value);
        //console.log("dbclick " + this.innerHTML);
                //Call NAV Download with FILEID      
        Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('DownloadFile', [{ID: this.id }]);

    }    
}
function createFileDiv(Data){
    //debugger;

    var filename = Data.DataText;
    var filesize = Data.AdditionalText;

    var contentDiv = document.createElement('div');
    var fileContainer = document.createElement("div");
    fileContainer.classList = "filecontent_div";
    fileContainer.style.border = "solid";
    fileContainer.style.borderWidth =  "3px";
    fileContainer.style.width = "calc(100% - 6px)";

    contentDiv.appendChild(fileContainer);
    fileContainer.id = Data.EntryNo; // Set ID for Download function
    var iconDiv = document.createElement("div");

    var a = document.createElement("a");
    var textNode = document.createTextNode(filename);
    a.href = "#";
    a.appendChild(textNode);
    //fileContainer.appendChild(a);


    fileContainer.appendChild(iconDiv);
    fileContainer.onclick = onFileClicked;
    // fileContainer.appendChild(a);

    var columnDivContainer = document.createElement("div");
    columnDivContainer.classList = 'columnDivContainer';
    var columnDiv1 = document.createElement("div");
    var columnDiv2 = document.createElement("div");
    //var columnDiv3 = document.createElement("div");

    columnDiv1.innerText = filename;
    columnDiv2.innerText = formatBytes(filesize);
    //columnDiv3.innerText = Data.CreationDate + " | " + Data.CreationTime;

    $(columnDiv1).addClass('column1');
    $(columnDiv2).addClass('column2');
    //$(columnDiv3).addClass('column3');

    columnDivContainer.appendChild(columnDiv1);
    columnDivContainer.appendChild(columnDiv2);
    //columnDivContainer.appendChild(columnDiv3);

    iconDiv.appendChild(columnDivContainer);


    switch(filename.split('.').pop().toLocaleLowerCase()){
        case 'txt':  
        case 'doc': 
        case 'docx': 
            iconDiv.setAttribute("id", "documentDiv");
            break;
        case 'xls':  
        case 'xlsx': 
        case 'xlsxc': 
            iconDiv.setAttribute("id", "excelDiv");
            break;
        case 'mpg':  
        case 'mpeg': 
        case 'mp4': 
            iconDiv.setAttribute("id", "videoDiv");
            break;
        case 'pdf':  
            iconDiv.setAttribute("id", "pdfDiv");
        break;
        case 'mp3':  
        case 'wav': 
            iconDiv.setAttribute("id", "audioDiv");
            break;
        case 'jpg':  
        case 'gif': 
        case 'png': 
        case 'jpeg':
        case 'bmp':
            iconDiv.setAttribute("id", "imageDiv");
            break;
        case 'zip':
        case 'rar':
            iconDiv.setAttribute("id", "zipDiv");
            break;
        default: 
            iconDiv.setAttribute("id", "fileDiv");
            break;
    }

    // Just the same as creating a Message -> File is Content
    var msgTypeContainer = '';
    var msgTypeContent = '';
    var msgColor = '';
    switch(Data.CreatedByType){
        case 0: 
            msgTypeContainer = 'outgoing_msg'; 
            msgTypeContent = 'sent_file_content'; 
            msgColor = ColorUserMessage;
            break; // BC User has sent the Message 
        case 1: 
            msgTypeContainer = 'incoming_msg'; 
            msgTypeContent = 'received_file_content'; 
            msgColor = ColorContactMessage;
            break; // Contact has sent the Message
        default: 
            msgTypeContainer = 'system_msg'; 
            msgTypeContent = 'system_file_content'; 
            break; // System generated Message
    }
    fileContainer.style.borderColor =  msgColor;
    fileContainer.style.backgroundColor =  msgColor;

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    //var SenderInformation = DateString + " | " + Data.CreatedByName;
    // var SenderInformation = Data.CreatedByName  + " schrieb am " + DateString;
    var SenderInformation = Data.SenderInformation;
    var container = document.createElement('div');
    container.className = msgTypeContainer;

    var msgDiv = document.createElement('div');
    msgDiv.className = msgTypeContent;

    if(isFullScreen){
        msgDiv.style.paddingRight = "0px";
    }
    

    var timeDateSpan = document.createElement('span');
    timeDateSpan.className = 'time_date';
    timeDateSpan.innerHTML = DateString;

    var SenderInformationSpan = document.createElement('span');
    SenderInformationSpan.className = 'sender_information';
    SenderInformationSpan.innerHTML = SenderInformation;
    
    msgDiv.appendChild(SenderInformationSpan);
    msgDiv.appendChild(contentDiv);
    //msgDiv.appendChild(timeDateSpan);
    
    container.appendChild(msgDiv);
    return container;
}

function createInternalFileDiv(Data){
    //debugger;

    var filename = Data.DataText;
    var filesize = Data.AdditionalText;

    var contentDiv = document.createElement('div');
    var fileContainer = document.createElement("div");
    fileContainer.classList = "filecontent_div";
    fileContainer.style.border = "solid";
    fileContainer.style.borderWidth =  "3px";
    fileContainer.style.width = "calc(100% - 6px)";

    contentDiv.appendChild(fileContainer);
    fileContainer.id = Data.EntryNo; // Set ID for Download function
    var iconDiv = document.createElement("div");

    var a = document.createElement("a");
    var textNode = document.createTextNode(filename);
    a.href = "#";
    a.appendChild(textNode);
    //fileContainer.appendChild(a);


    fileContainer.appendChild(iconDiv);
    fileContainer.onclick = onFileClicked;
    // fileContainer.appendChild(a);

    var columnDivContainer = document.createElement("div");
    columnDivContainer.classList = 'columnDivContainer';
    var columnDiv1 = document.createElement("div");
    var columnDiv2 = document.createElement("div");
    //var columnDiv3 = document.createElement("div");

    columnDiv1.innerText = filename;
    columnDiv2.innerText = formatBytes(filesize);
    //columnDiv3.innerText = Data.CreationDate + " | " + Data.CreationTime;

    $(columnDiv1).addClass('column1');
    $(columnDiv2).addClass('column2');
    //$(columnDiv3).addClass('column3');

    columnDivContainer.appendChild(columnDiv1);
    columnDivContainer.appendChild(columnDiv2);
    //columnDivContainer.appendChild(columnDiv3);

    iconDiv.appendChild(columnDivContainer);


    switch(filename.split('.').pop().toLocaleLowerCase()){
        case 'txt':  
        case 'doc': 
        case 'docx': 
            iconDiv.setAttribute("id", "documentDiv");
            break;
        case 'xls':  
        case 'xlsx': 
        case 'xlsxc': 
            iconDiv.setAttribute("id", "excelDiv");
            break;
        case 'mpg':  
        case 'mpeg': 
        case 'mp4': 
            iconDiv.setAttribute("id", "videoDiv");
            break;
        case 'pdf':  
            iconDiv.setAttribute("id", "pdfDiv");
        break;
        case 'mp3':  
        case 'wav': 
            iconDiv.setAttribute("id", "audioDiv");
            break;
        case 'jpg':  
        case 'gif': 
        case 'png': 
        case 'jpeg':
        case 'bmp':
            iconDiv.setAttribute("id", "imageDiv");
            break;
        case 'zip':
        case 'rar':
            iconDiv.setAttribute("id", "zipDiv");
            break;
        default: 
            iconDiv.setAttribute("id", "fileDiv");
            break;
    }

    // Just the same as creating a Message -> File is Content
    var msgTypeContainer = '';
    var msgTypeContent = '';
    var msgColor = '';
    switch(Data.CreatedByType){
        case 0: 
            msgTypeContainer = 'outgoing_msg'; 
            msgTypeContent = 'sent_file_content'; 
            // msgColor = ColorInternalMessage; Internally
            break; // BC User has sent the Message 
        case 1: 
            msgTypeContainer = 'incoming_msg'; 
            msgTypeContent = 'received_file_content'; 
            // msgColor = ColorContactMessage; Internally
            break; // Contact has sent the Message
        default: 
            msgTypeContainer = 'system_msg'; 
            msgTypeContent = 'system_file_content'; 
            break; // System generated Message
    }
    fileContainer.style.borderColor =  ColorInternalMessage;
    fileContainer.style.backgroundColor =  ColorInternalMessage;

    var DateString =  Data.CreationDate + " | " + Data.CreationTime;
    //var SenderInformation = DateString + " | " + Data.CreatedByName;
    // var SenderInformation = Data.CreatedByName  + " schrieb am " + DateString;
    var SenderInformation = Data.SenderInformation;
    var container = document.createElement('div');
    container.className = msgTypeContainer;

    var msgDiv = document.createElement('div');
    msgDiv.className = msgTypeContent;

    if(isFullScreen){
        msgDiv.style.paddingRight = "0px";
    }
    

    var timeDateSpan = document.createElement('span');
    timeDateSpan.className = 'time_date';
    timeDateSpan.innerHTML = DateString;

    var SenderInformationSpan = document.createElement('span');
    SenderInformationSpan.className = 'sender_information';
    SenderInformationSpan.innerHTML = SenderInformation;
    
    msgDiv.appendChild(SenderInformationSpan);
    msgDiv.appendChild(contentDiv);
    //msgDiv.appendChild(timeDateSpan);
    
    container.appendChild(msgDiv);
    return container;

}

function SendFileToClient(Data){
    // Anything but IE works here
    debugger;
    if (undefined === window.navigator.msSaveOrOpenBlob) {
        var e = document.createElement('a');
        var href = Data.DataURI;
        e.setAttribute('href', href);
        e.setAttribute('download', Data.filename);
        document.body.appendChild(e);
        e.click();
        document.body.removeChild(e);
    }
    // IE-specific code
    else {
        var binary = atob(Data.DataURI.split(',')[1]);
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        var blob = new Blob([new Uint8Array(array)], {type: Data.DataURI.split(',')[0]});

        window.navigator.msSaveOrOpenBlob(blob, Data.filename);
    }

}

function isHTML(str) {
    var doc = new DOMParser().parseFromString(str, "text/html");
    return Array.from(doc.body.childNodes).some(node => node.nodeType === 1);
}
