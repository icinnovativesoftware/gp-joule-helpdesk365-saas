table 56295 "ICI Department"
{
    Caption = 'ICI Department', Comment = 'de-DE=Support Abteilung';
    DataClassification = CustomerContent;

    LookupPageId = "ICI Departments";
    DrillDownPageId = "ICI Departments";

    fields
    {
        field(1; Code; Code[10])
        {
            Caption = 'Code', Comment = 'de-DE=Code';
            DataClassification = CustomerContent;
            NotBlank = true;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
    }

}
