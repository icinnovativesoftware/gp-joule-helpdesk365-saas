page 56377 "ICI Ticket Addr. Search Dialog"
{
    Caption = 'ICI Address Search Dialog', Comment = 'de-DE=Adresssuche';
    PageType = StandardDialog;
    ApplicationArea = All;
    UsageCategory = Tasks;

    layout
    {
        area(content)
        {
            group(Search)
            {
                Caption = 'Search', Comment = 'de-DE=Suche';
                field(SearchAddress; SearchAddress)
                {
                    ApplicationArea = All;
                    Caption = 'Address', Comment = 'de-DE=Adresse';
                    ToolTip = 'Address', Comment = 'de-DE=Adresse';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
                field(SearchAddress2; SearchAddress2)
                {
                    ApplicationArea = All;
                    Caption = 'Address 2', Comment = 'de-DE=Adresse 2';
                    ToolTip = 'Address 2', Comment = 'de-DE=Adresse 2';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
                field(SearchPostCode; SearchPostCode)
                {
                    ApplicationArea = All;
                    Caption = 'Post Code', Comment = 'de-DE=PLZ';
                    ToolTip = 'Post Code', Comment = 'de-DE=PLZ';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
                field(SearchName; SearchName)
                {
                    ApplicationArea = All;
                    Caption = 'Name', Comment = 'de-DE=Name';
                    ToolTip = 'Name', Comment = 'de-DE=Name';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
                field(SearchName2; SearchName2)
                {
                    ApplicationArea = All;
                    Caption = 'Name 2', Comment = 'de-DE=Name 2';
                    ToolTip = 'Name 2', Comment = 'de-DE=Name 2';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
                field(SearchCity; SearchCity)
                {
                    ApplicationArea = All;
                    Caption = 'City', Comment = 'de-DE=Stadt';
                    ToolTip = 'City', Comment = 'de-DE=Stadt';
                    trigger OnValidate()
                    begin
                        // CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
                        // CurrPage."ICI Reference Listpart".Page.Update(false);
                        FillPage();
                    end;
                }
            }

            group(TicketValues)
            {
                Caption = 'Linked Ticket', Comment = 'de-DE=Ticket';
                Visible = ShowTicketNo;
                Editable = false;

                field(TicketNo; TicketNo)
                {
                    ApplicationArea = All;
                    Caption = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    Visible = ShowTicketNo;
                }
                field(TicketCustomerNo; TicketCustomerNo)
                {
                    ApplicationArea = All;
                    Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    Visible = ShowTicketCustomerNo;

                }
            }
            group(Options)
            {
                Caption = 'Options', Comment = 'de-DE=Optionen';
                group(Allgemein)
                {
                    field("Search Cust. by Addr."; ICISupportUser."Search Cust. by Addr.")
                    {
                        ToolTip = 'Specifies the value of the Search Customer by Address', Comment = 'de-DE=Debitor nach Adresse';
                        Caption = 'Specifies the value of the Search Customer by Address', Comment = 'de-DE=Debitor';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Cust. by Addr.", ICISupportUser."Search Cust. by Addr.");
                            ICISupportUser2.MODIFY();
                            FillPage();
                        end;
                    }
                    field("Search Contacts by Addr."; ICISupportUser."Search Contacts by Addr.")
                    {
                        ToolTip = 'Specifies the value of the Search Contacts by Address', Comment = 'de-DE=Kontaktsuche nach Adresse';
                        Caption = 'Specifies the value of the Search Contacts by Address', Comment = 'de-DE=Kontakte';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Contacts by Addr.", ICISupportUser."Search Contacts by Addr.");
                            ICISupportUser2.MODIFY();
                            FillPage();
                        end;
                    }
                    field("Search Ship-To Address"; ICISupportUser."Search Ship-To Address")
                    {
                        ToolTip = 'Specifies the value of the Search Ship-To Address', Comment = 'de-DE=Lieferadressen';
                        Caption = 'Specifies the value of the Search Ship-To Address', Comment = 'de-DE=Lieferadressen';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Ship-To Address", ICISupportUser."Search Ship-To Address");
                            ICISupportUser2.MODIFY();
                            FillPage();
                        end;
                    }
                }

                group(Service)
                {
                    field("Search ServiceItems by Addr."; ICISupportUser."Search ServiceItems by Addr.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikelsuche nach Adresse';
                        Caption = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikel nach Adresse';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search ServiceItems by Addr.", ICISupportUser."Search ServiceItems by Addr.");
                            ICISupportUser2.MODIFY();
                            FillPage();
                        end;
                    }
                    field("Search SI by Ship-To Addr."; ICISupportUser."Search SI by Ship-To Addr.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikelsuche nach Lieferadresse';
                        Caption = 'Specifies the value of the Search Service Items by Address', Comment = 'de-DE=Serviceartikel nach Lieferadresse';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search SI by Ship-To Addr.", ICISupportUser."Search SI by Ship-To Addr.");
                            ICISupportUser2.MODIFY();
                            FillPage();
                        end;
                    }
                }
            }
            part("ICI Reference Listpart"; "ICI Reference Listpart")
            {
                Caption = 'Search Results', Comment = 'de-DE=Suchergebnisse';
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
    }

    trigger OnOpenPage()
    begin
        ICISupportUser.GetCurrUser(ICISupportUser);
        CurrPage."ICI Reference Listpart".Page.SetAddressSearch();
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        ICIReferenceNoBuffer: Record "ICI Reference No. Buffer";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        // ICIServiceIntegrationMgt: Codeunit "ICI Service Integration Mgt.";
        TicketCreated: Boolean;
        AddressProcessed: Boolean;
        // CreatedServiceItemNo: Code[20];
        ConfirmCloseWithUnsavedChangesLbl: Label 'The Site contains unsaved changes. Do you want to close the Wizard anyways?', Comment = 'de-DE=Die Seite enthält ungespeicherte Änderungen. Wollen Sie die Seite dennoch schließen?';
        CloseWithoutCreateMsg: Label 'No Adress was selected. Do you want to close the Wizard anyways?', Comment = 'de-DE=Sie haben keine Adresse ausgewählt. Wollen Sie die Seite dennoch schließen?';
    begin
        IF CloseAction in [ACTION::OK, ACTION::LookupOK] THEN begin
            // Priority: 1. Use ICIReferenceNoBuffer.Code, 2. No ICIReferenceNoBuffer.Code then The Typed Reference No
            CurrPage."ICI Reference Listpart".Page.GetRecord(ICIReferenceNoBuffer);

            IF (SearchAddress = '') AND (SearchAddress2 = '') AND (SearchPostCode = '') AND (SearchName = '') AND (SearchName2 = '') AND (SearchCity = '') THEN
                EXIT(CONFIRM(CloseWithoutCreateMsg));

            TicketCreated := (ICISupportTicket."No." = '');

            AddressProcessed := ICISupportDocumentMgt.ProcessReferenceNoBuffer(ICIReferenceNoBuffer, ICISupportTicket, Copystr((SearchAddress + '|' + SearchAddress2 + '|' + SearchPostCode + '|' + SearchName + '|' + SearchName2 + '|' + SearchCity), 1, 50));

            IF TicketCreated AND AddressProcessed THEN
                Page.Run(PAGE::"ICI Support Ticket Card", ICISupportTicket);
        end else
            if (SearchAddress <> '') OR (SearchAddress2 <> '') OR (SearchPostCode <> '') OR (SearchName <> '') OR (SearchName2 <> '') OR (SearchCity <> '') THEN EXIT(CONFIRM(ConfirmCloseWithUnsavedChangesLbl))
    end;

    procedure SetValues(pTicketNo: Code[20]; pSearchAddress: Text[100]; pSearchAddress2: Text[50]; pSearchPostCode: Code[20]; pSearchName: Text[50]; pSearchName2: Text[50]; pSearchCity: Text[30])
    begin
        TicketNo := pTicketNo;
        ShowTicketNo := (TicketNo <> '');

        ICISupportTicket.GET(TicketNo);
        TicketCustomerNo := ICISupportTicket."Customer No.";

        ShowTicketCustomerNo := (ICISupportTicket."Customer No." <> '');

        SearchAddress := pSearchAddress;
        SearchAddress2 := pSearchAddress2;
        SearchPostCode := pSearchPostCode;
        SearchCity := pSearchCity;
        SearchName := pSearchName;
        SearchName2 := pSearchName2;

        //CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
        FillPage();
    end;

    procedure FillPage()
    begin
        CurrPage."ICI Reference Listpart".Page.FillPageFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity, TicketNo);
        CurrPage."ICI Reference Listpart".Page.Update(false);
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        TicketNo: Code[20];
        SearchAddress: Text[100];
        SearchAddress2: Text[50];
        SearchName: Text[50];
        SearchName2: Text[50];
        SearchPostCode: Code[20];
        SearchCity: Text[30];
        TicketCustomerNo: Code[20];
        ShowTicketNo: Boolean;
        ShowTicketCustomerNo: Boolean;
}
