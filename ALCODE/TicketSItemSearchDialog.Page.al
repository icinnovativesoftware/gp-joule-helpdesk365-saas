page 56345 "ICI Ticket SItem Search Dialog"
{
    Caption = 'ICI Serial No. Search Dialog', Comment = 'de-DE=Seriennummernsuche';
    PageType = StandardDialog;
    ApplicationArea = All;
    UsageCategory = Tasks;

    layout
    {
        area(content)
        {
            group(Search)
            {
                Caption = 'Search', Comment = 'de-DE=Suche';
                field(ReferenceNo; ReferenceNo)
                {
                    ApplicationArea = All;
                    Caption = 'Reference No.', Comment = 'de-DE=Nr.';
                    ToolTip = 'Reference No.', Comment = 'de-DE=Nr.';
                    trigger OnValidate()
                    begin
                        CurrPage."ICI Reference Listpart".Page.FillPageFromReferenceNo(ReferenceNo, TicketNo, 1); // 1 is ServiceItem Search Only
                        CurrPage."ICI Reference Listpart".Page.Update(false);
                    end;
                }
            }

            group(TicketValues)
            {
                Caption = 'Linked Ticket', Comment = 'de-DE=Ticket';
                Visible = ShowTicketNo;
                Editable = false;

                field(TicketNo; TicketNo)
                {
                    ApplicationArea = All;
                    Caption = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    ToolTip = 'Ticket No.', Comment = 'de-DE=Ticketnr.';
                    Visible = ShowTicketNo;
                }
                field(TicketCustomerNo; TicketCustomerNo)
                {
                    ApplicationArea = All;
                    Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    Visible = ShowTicketCustomerNo;

                }
            }
            group(Options)
            {
                Caption = 'Service', Comment = 'de-DE=Service';

                group(Service)
                {
                    Caption = 'Service Item', Comment = 'de-DE=Serviceartikel';

                    field("Search Se. It. by No."; ICISupportUser."Search Se. It. by No.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by No.', Comment = 'de-DE=Serviceartikel nach Nr.';
                        Caption = 'Specifies the value of the Search Service Items by No.', Comment = 'de-DE=Serviceartikelnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Se. It. by No.", ICISupportUser."Search Se. It. by No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search Se. It. by Serial No."; ICISupportUser."Search Se. It. by Serial No.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Serviceartikel nach Seriennr.';
                        Caption = 'Specifies the value of the Search Service Items by Serial No.', Comment = 'de-DE=Seriennummer';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Se. It. by Serial No.", ICISupportUser."Search Se. It. by Serial No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search Se. It. by Item No."; ICISupportUser."Search Se. It. by Item No.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by Item No.', Comment = 'de-DE=Serviceartikel nach Artikelnr.';
                        Caption = 'Specifies the value of the Search Service Items by Item No.', Comment = 'de-DE=Artikelnr.';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Se. It. by Item No.", ICISupportUser."Search Se. It. by Item No.");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                    field("Search Se. It. by Search Desc."; ICISupportUser."Search Se. It. by Search Desc.")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by Search Desc.', Comment = 'de-DE=Serviceartikel nach Suchbegriff';
                        Caption = 'Specifies the value of the Search Service Items by Search Desc.', Comment = 'de-DE=Suchbegriff';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Se. It. by Search Desc.", ICISupportUser."Search Se. It. by Search Desc.");
                            ICISupportUser2.MODIFY();
                        end;
                    }

                    field("Search Se. It. by No. 2"; ICISupportUser."Search Se. It. by No. 2")
                    {
                        ToolTip = 'Specifies the value of the Search Service Items by No. 2', Comment = 'de-DE=Serviceartikel nach Nr. 2';
                        Caption = 'Specifies the value of the Search Service Items by No. 2', Comment = 'de-DE=Nummer 2';
                        ApplicationArea = All;

                        trigger OnValidate()
                        var
                            ICISupportUser2: Record "ICI Support User";
                        begin
                            ICISupportUser2.GetCurrUser(ICISupportUser2);
                            ICISupportUser2.Validate("Search Se. It. by No. 2", ICISupportUser."Search Se. It. by No. 2");
                            ICISupportUser2.MODIFY();
                        end;
                    }
                }
            }
            part("ICI Reference Listpart"; "ICI Reference Listpart")
            {

                Caption = 'Search Results', Comment = 'de-DE=Suchergebnisse';
                ApplicationArea = All;
                UpdatePropagation = Both;
            }
        }
    }

    trigger OnOpenPage()
    begin
        ICISupportUser.GetCurrUser(ICISupportUser);

        CurrPage."ICI Reference Listpart".Page.SetSerialSearch();
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        ICIReferenceNoBuffer: Record "ICI Reference No. Buffer";
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        // ICIServiceIntegrationMgt: Codeunit "ICI Service Integration Mgt.";
        TicketCreated: Boolean;
        SerialNoProcessed: Boolean;
        // CreatedServiceItemNo: Code[20];
        ConfirmCloseWithUnsavedChangesLbl: Label 'The Site contains unsaved changes. Do you want to close the Wizard anyways?', Comment = 'de-DE=Die Seite enthält ungespeicherte Änderungen. Wollen Sie die Seite dennoch schließen?';
        CloseWithoutCreateMsg: Label 'No Reference No. was selected. Do you want to close the Wizard anyways?', Comment = 'de-DE=Sie haben keine Seriennr. ausgewählt. Wollen Sie die Seite dennoch schließen?';
    begin
        IF CloseAction in [ACTION::OK, ACTION::LookupOK] THEN begin
            // Priority: 1. Use ICIReferenceNoBuffer.Code, 2. No ICIReferenceNoBuffer.Code then The Typed Reference No
            CurrPage."ICI Reference Listpart".Page.GetRecord(ICIReferenceNoBuffer);

            IF ReferenceNo = '' THEN
                EXIT(CONFIRM(CloseWithoutCreateMsg));

            TicketCreated := (ICISupportTicket."No." = '');
            SerialNoProcessed := ICISupportDocumentMgt.ProcessReferenceNoBuffer(ICIReferenceNoBuffer, ICISupportTicket, ReferenceNo);

            IF TicketCreated AND SerialNoProcessed THEN
                Page.Run(PAGE::"ICI Support Ticket Card", ICISupportTicket);
        end else
            if ReferenceNo <> '' THEN EXIT(CONFIRM(ConfirmCloseWithUnsavedChangesLbl))
    end;

    procedure SetValues(pTicketNo: Code[20]; pReferenceNo: Code[50])
    begin
        TicketNo := pTicketNo;
        ShowTicketNo := (TicketNo <> '');

        ICISupportTicket.GET(TicketNo);
        TicketCustomerNo := ICISupportTicket."Customer No.";

        ShowTicketCustomerNo := (ICISupportTicket."Customer No." <> '');

        ReferenceNo := pReferenceNo;
        CurrPage."ICI Reference Listpart".Page.FillPageFromReferenceNo(ReferenceNo, TicketNo, 1); // 1 = ServiceItem
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        TicketNo: Code[20];
        ReferenceNo: Code[50];
        TicketCustomerNo: Code[20];
        ShowTicketNo: Boolean;
        ShowTicketCustomerNo: Boolean;
}
