tableextension 56303 "ICI Ship-to Address" extends "Ship-to Address"
{
    procedure OverwriteAddressFromServiceItemCenter(var ShipToAddress: Record "Ship-to Address"; var ServiceItem: Record "Service Item"; pJsonObject: JsonObject)
    var
        lJsonToken: JsonToken;
        jShipToName: Text;
        jShipToAddress: Text;
        jShipToPostCode: Text;
        jShipToCity: Text;
    begin
        IF GetJsonToken(pJsonObject, 'service_item_ship_address', lJsonToken) THEN
            jShipToAddress := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_name', lJsonToken) THEN
            jShipToName := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_post_code', lJsonToken) THEN
            jShipToPostCode := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_city', lJsonToken) THEN
            jShipToCity := lJsonToken.AsValue().AsText();

        ShiptoAddress.VALIDATE(Name, jShipToName);
        ShiptoAddress.VALIDATE(Address, jShipToAddress);
        ShiptoAddress.VALIDATE(City, jShipToCity);
        ShiptoAddress.VALIDATE("Post Code", jShipToPostCode);

        OnBeforeOverwriteAddressFromServiceItemCenter(ShipToAddress, pJsonObject);
        ShiptoAddress.Modify();
    end;

    procedure CreateShipToAddressFromServiceItemCenter(var ServiceItem: Record "Service Item"; pJsonObject: JsonObject): Code[10]
    var
        ShipToAddress: Record "Ship-to Address";
        Customer: Record Customer;
        lJsonToken: JsonToken;
        ShipToCode: Code[10];
        jShipToName: Text;
        jShipToAddress: Text;
        jShipToPostCode: Text;
        jShipToCity: Text;
    begin
        IF NOT Customer.GET(ServiceItem."Customer No.") THEN
            EXIT('');

        IF GetJsonToken(pJsonObject, 'service_item_ship_address', lJsonToken) THEN
            jShipToAddress := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_name', lJsonToken) THEN
            jShipToName := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_post_code', lJsonToken) THEN
            jShipToPostCode := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_city', lJsonToken) THEN
            jShipToCity := lJsonToken.AsValue().AsText();

        ShiptoAddress.SETRANGE("Customer No.", ServiceItem."Customer No.");
        IF ShiptoAddress.FINDLAST() THEN
            ShipToCode := INCSTR(ShiptoAddress.Code);

        IF ShipToCode = '' THEN BEGIN
            ShipToCode := COPYSTR(DELCHR(ServiceItem."No.", '<>=', '-') + '??', 1, 10);
            ShiptoAddress.SETFILTER(Code, ShipToCode);
            IF ShiptoAddress.FINDLAST() THEN
                ShipToCode := INCSTR(ShiptoAddress.Code)
            ELSE
                ShipToCode := COPYSTR(DELCHR(ServiceItem."No.", '<>=', '-') + '00', 1, 10);
        END;

        CLEAR(ShiptoAddress);

        ShiptoAddress."Customer No." := ServiceItem."Customer No.";
        ShiptoAddress.Code := ShipToCode;
        ShiptoAddress.VALIDATE(Name, jShipToName);
        ShiptoAddress.VALIDATE(Address, jShipToAddress);
        ShiptoAddress.VALIDATE(City, jShipToCity);
        ShiptoAddress.VALIDATE("Post Code", jShipToPostCode);

        ShiptoAddress.VALIDATE("Location Code", Customer."Location Code");
        ShiptoAddress.VALIDATE("Shipment Method Code", Customer."Shipment Method Code");
        ShiptoAddress.VALIDATE("Shipping Agent Code", Customer."Shipping Agent Code");
        ShiptoAddress.VALIDATE("Shipping Agent Service Code", Customer."Shipping Agent Service Code");
        ShiptoAddress.INSERT();

        OnBeforeCreateShipToAddressFromServiceItemCenter(ShipToAddress, pJsonObject);
        ShiptoAddress.MODIFY();

        EXIT(ShiptoAddress.Code);
    end;

    procedure FindShipToAddressFromServiceItemCenter(var ServiceItem: Record "Service Item"; pJsonObject: JsonObject): Code[10]
    var
        ShipToAddress: Record "Ship-to Address";
        Customer: Record Customer;
        lJsonToken: JsonToken;
        jShipToName: Text;
        jShipToAddress: Text;
        jShipToPostCode: Text;
        jShipToCity: Text;
    begin
        IF NOT Customer.GET(ServiceItem."Customer No.") THEN
            EXIT('');

        IF GetJsonToken(pJsonObject, 'service_item_ship_address', lJsonToken) THEN
            jShipToAddress := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_name', lJsonToken) THEN
            jShipToName := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_post_code', lJsonToken) THEN
            jShipToPostCode := lJsonToken.AsValue().AsText();
        IF GetJsonToken(pJsonObject, 'service_item_ship_city', lJsonToken) THEN
            jShipToCity := lJsonToken.AsValue().AsText();

        ShiptoAddress.SETRANGE("Customer No.", Customer."No.");
        ShiptoAddress.SETFILTER(Name, '%1', jShipToName);
        ShiptoAddress.SETFILTER(Address, '%1', jShipToAddress);
        ShiptoAddress.SETFILTER("Post Code", '%1', jShipToPostCode);
        ShiptoAddress.SETFILTER(City, '%1', jShipToCity);

        OnBeforeFindShipToAddressFromServiceItemCenter(ShiptoAddress, pJsonObject);

        IF ShiptoAddress.FINDLAST() THEN
            EXIT(ShiptoAddress.Code)
    end;

    local procedure GetJsonToken(JsonObject: JsonObject; TokenKey: Text; var jToken: JsonToken): Boolean
    begin
        EXIT(JsonObject.GET(TokenKey, jToken));
    end;


    [IntegrationEvent(false, false)]
    procedure OnBeforeCreateShipToAddressFromServiceItemCenter(var ShipToAddress: Record "Ship-to Address"; JSON: JsonObject);
    begin
    end;

    [IntegrationEvent(false, false)]
    procedure OnBeforeOverwriteAddressFromServiceItemCenter(var ShipToAddress: Record "Ship-to Address"; JSON: JsonObject);
    begin
    end;

    [IntegrationEvent(false, false)]
    procedure OnBeforeFindShipToAddressFromServiceItemCenter(var ShipToAddress: Record "Ship-to Address"; JSON: JsonObject);
    begin
    end;

}
