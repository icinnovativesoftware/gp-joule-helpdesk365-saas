codeunit 56277 "ICI Support Ticket Log Mgt."
{
    procedure SaveTicketMessage(var ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        lOutStream: OutStream;
        NotificationJSON: JsonObject;
        JTok: JsonToken;
        TicketNo: Code[20];
        CommunicationTxt: Text;
        UserID: Code[50];
    begin
        // Get Mail Values
        NotificationJSON := ICINotificationDialogMgt.CreateNotificationValues();

        IF NotificationJSON.GET('TicketNo', JTok) then
            TicketNo := JTok.AsValue().AsText();

        IF NotificationJSON.GET('InitText', JTok) then
            CommunicationTxt := JTok.AsValue().AsText();

        // IF NotificationJSON.GET('Receivers', JTok) then
        //     RecipientsArray := JTok.AsArray();

        // IF NotificationJSON.GET('Attachments', JTok) then
        //     AttachmentsArray := JTok.AsArray();
        ICISupportUser.GetCurrUser(ICISupportUser);
        UserID := ICISupportUser."User ID";
        IF ICISupportTicket.GET(TicketNo) THEN begin
            ICISupportTicket.SetModifiedByUser(UserId);
            ICISupportTicket.Modify();

            ICISupportTicketLog.Init();
            ICISupportTicketLog.Insert(true);
            ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);
            ICISupportTicketLog.Calcfields(Data);
            ICISupportTicketLog.Data.CreateOutStream(lOutStream);
            lOutStream.Write(CommunicationTxt);
            if ICINotificationDialogMgt.GetInternally() then
                ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"Internal Message")
            else
                ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External Message");
            ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
            ICISupportUser.GetCurrUser(ICISupportUser);
            ICISupportTicketLog.Validate("Created By", ICISupportUser."User ID");

            ICISupportTicketLog.Modify(true);
        end else
            Error('Could not Save Message: Ticket No.: "%1" not found', TicketNo);

    end;

    procedure SaveTicketMessage(TicketNo: Code[20]; CommunicationTxt: Text; Internally: Boolean; SendMail: Boolean)
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportUser: Record "ICI Support User";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lOutStream: OutStream;
        cr: Char;
        lf: Char;
    begin
        IF ICISupportTicket.GET(TicketNo) THEN begin
            ICISupportTicket.SetModifiedByUser(UserId);
            ICISupportTicket.Modify();

            ICISupportTicketLog.Init();
            ICISupportTicketLog.Insert(true);
            ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);
            ICISupportTicketLog.Calcfields(Data);
            ICISupportTicketLog.Data.CreateOutStream(lOutStream);

            cr := 13;
            lf := 10;
            CommunicationTxt := DELCHR(CommunicationTxt, '=', FORMAT(cr));
            CommunicationTxt := DELCHR(CommunicationTxt, '=', FORMAT(lf));

            lOutStream.Write(CommunicationTxt);
            if Internally then
                ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"Internal Message")
            else
                ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External Message");
            ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
            ICISupportUser.GetCurrUser(ICISupportUser);
            ICISupportTicketLog.Validate("Created By", ICISupportUser."User ID");

            ICISupportTicketLog.Modify(true);

            IF NOT Internally AND SendMail then
                ICISupportMailMgt.NewSupportMessageForContact(TicketNo, ICISupportTicketLog."Entry No.");
        end else
            Error('Could not Save Message: Ticket No.: "%1" not found', TicketNo);

    end;

    procedure SaveTicketContactMessage(TicketNo: Code[20]; CommunicationTxt: Text; ContactNo: Code[20])
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lOutStream: OutStream;
        TypeHelper: Codeunit "Type Helper";
        CRLF: Text[2];
    begin
        IF ICISupportTicket.GET(TicketNo) THEN begin
            ICISupportTicket.SetModifiedByContact(ContactNo);
            ICISupportTicket.Modify();

            ICISupportTicketLog.Init();
            ICISupportTicketLog.Insert(true);
            ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);
            ICISupportTicketLog.Calcfields(Data);
            ICISupportTicketLog.Data.CreateOutStream(lOutStream);

            CommunicationTxt := CommunicationTxt.Replace('\r\n', '<br/>');
            CommunicationTxt := CommunicationTxt.Replace('\n', '<br/>');

            //IC - LV - 20240220 ---
            CRLF := TypeHelper.CRLFSeparator();
            CommunicationTxt := CommunicationTxt.Replace(CRLF, '</br>');
            //IC - LV - 20240220 +++

            lOutStream.Write(CommunicationTxt);

            ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::"External Message");
            ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::Contact);
            ICISupportTicketLog.Validate("Created By", ContactNo);

            ICISupportTicketLog.Modify(true);
            IF ICISupportTicket."Ticket State" <> ICISupportTicket."Ticket State"::Preparation THEN
                ICISupportMailMgt.NewSupportMessageForUser(TicketNo, ICISupportTicketLog."Entry No.");
        end else
            Error('Could not Save Message: Ticket No.: "%1" not found', TicketNo);

    end;


    procedure GetTicketLogDescription(EntryNo: Integer) Description: Text
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICIEscalationLevel: Record "ICI Escalation Level";
        ICIEscalationGroup: Record "ICI Escalation Group";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        lRecordRef: RecordRef;
        TimeRegistrationTemplateTxt: Label '%1 registered %2 %3', Comment = '%1=UserID;%2=Qty;%3=UoM|de-DE=%1 erfasste %2 %3';
        DescriptionTemplateTxt: Label '%1 from %2 on %3 %4', Comment = '%1 = Type; %2 = Name; %3 = CreationDate; %4 = CreationTime|de-DE=%1 von %2 am %3 %4';
        DescriptionMailTemplateTxt: Label '%1 wrote on %2 %3', Comment = '%1 = Name; %2 = Date; %3 = Time; |de-DE=%1 schrieb am %2 %3';
        EscalationLbl: Label '%1 %2', Comment = '%1 = Start Time; %2 = Condition';
        lInStream: InStream;
        Name: Text;
    Begin
        IF ICISupportTicketLog.GET(EntryNo) THEN begin
            // Get Name
            IF (ICISupportTicketLog."Created By Type" = ICISupportTicketLog."Created By Type"::Contact) THEN
                IF Contact.GET(ICISupportTicketLog."Created By") THEN // XXX Evtl Fallback?
                    Name := Contact.Name;
            IF (ICISupportTicketLog."Created By Type" = ICISupportTicketLog."Created By Type"::User) THEN
                IF ICISupportUser.GET(ICISupportTicketLog."Created By") THEN // XXX Evtl Fallback?
                    Name := ICISupportUser.Name;
            IF Name = '' then
                Name := FORMAT(ICISupportTicketLog."Created By Type");

            case ICISupportTicketLog.Type of
                ICISupportTicketLog.Type::"External Message":
                    //Description := STRSUBSTNO(DescriptionTemplateTxt, ICISupportTicketLog.Type, Name, ICISupportTicketLog."Creation Date", ICISupportTicketLog."Creation Time");
                    Description := STRSUBSTNO(DescriptionMailTemplateTxt, Name, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), ICISupportTicketLog."Creation Time");
                ICISupportTicketLog.Type::State:
                    BEGIN
                        ICISupportTicketLog.CalcFields(Data);
                        ICISupportTicketLog.Data.CreateInStream(lInStream);
                        lInStream.Read(Description);

                        IF ICISupportTicketLog."Record ID".TableNo = DATABASE::"ICI Support Process Stage" THEN BEGIN
                            lRecordRef := ICISupportTicketLog."Record ID".GetRecord();
                            lRecordRef.SetTable(ICISupportProcessStage);
                            ICISupportProcessStage.GET(ICISupportProcessStage."Process Code", ICISupportProcessStage.Stage);
                            Description := ICISupportProcessStage.GetDescription();
                        END;
                    END;
                ICISupportTicketLog.Type::"External File":
                    IF ICISupportTicketLog."Data Text 2" <> '' then
                        Description := ICISupportTicketLog."Data Text 2" // Renamed Filename
                    else
                        Description := ICISupportTicketLog."Data Text";
                ICISupportTicketLog.Type::Escalation:
                    begin
                        IF ICISupportTicketLog."Record ID".TableNo <> DATABASE::"ICI Escalation Level" THEN // Bugfix: Renumbering Issue
                            exit;
                        lRecordRef := ICISupportTicketLog."Record ID".GetRecord();
                        lRecordRef.SetTable(ICIEscalationLevel);
                        IF ICIEscalationLevel.GET(ICIEscalationLevel."Escalation Code", ICIEscalationLevel."Line No.") THEN // Ooopsies. SetTable is sneaky ~.~
                            Description := StrSubstNo(EscalationLbl, ICIEscalationLevel."Start Time", ICIEscalationGroup.TableCaption);
                    end;
                ICISupportTicketLog.Type::"Time Registration": // Depricated
                    Description := STRSUBSTNO(TimeRegistrationTemplateTxt, ICISupportTicketLog."Time Registration User", ICISupportTicketLog."Time Registration Qty. Hours", ICISupportTicketLog."Time Reg. Unit of Measure");
                ELSE
                    Description := STRSUBSTNO(DescriptionTemplateTxt, ICISupportTicketLog.Type, Name, ICISupportTicketLog."Creation Date", ICISupportTicketLog."Creation Time");
            End;
        end;
    end;

    procedure GetSenderInformation(EntryNo: Integer) Description: Text
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICIEscalationLevel: Record "ICI Escalation Level";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        ICIEscalationGroup: Record "ICI Escalation Group";
        lRecordRef: RecordRef;
        TimeRegistrationTemplateTxt: Label '%1 registered %2 %3', Comment = '%1=UserID;%2=Qty;%3=UoM|de-DE=%1 erfasste %2 %3';
        DescriptionTemplateTxt: Label '%1 from %2 on %3 %4', Comment = '%1 = Type; %2 = Name; %3 = CreationDate; %4 = CreationTime|de-DE=%1 von %2 am %3 %4';
        DescriptionMailTemplateTxt: Label '%1 wrote on %2 %3', Comment = '%1 = Name; %2 = Date; %3 = Time; |de-DE=%1 schrieb am %2 %3';
        EscalationLbl: Label '%1 %2', Comment = '%1 = Start Time; %2 = Condition';
        ForwardingTemplateTxt: Label 'Forwarded to %1 from %2 on %3 %4', Comment = '%1 = New Name;%2 = From Name; %3 = Date; %4 = Time;|de-DE=Weitergeleitet an %1 von %2 am %3 %4';
        StateText: Text;
        lInStream: InStream;
        Name: Text;
        ForwardFromName: Text;
        ForwardToName: Text;
    Begin
        IF ICISupportTicketLog.GET(EntryNo) THEN begin
            // Get Name
            IF (ICISupportTicketLog."Created By Type" = ICISupportTicketLog."Created By Type"::Contact) THEN
                IF Contact.GET(ICISupportTicketLog."Created By") THEN // XXX Evtl Fallback?
                    Name := Contact.Name;
            IF (ICISupportTicketLog."Created By Type" = ICISupportTicketLog."Created By Type"::User) THEN
                IF ICISupportUser.GET(ICISupportTicketLog."Created By") THEN // XXX Evtl Fallback?
                    Name := ICISupportUser.Name;
            IF Name = '' then
                Name := FORMAT(ICISupportTicketLog."Created By Type");

            case ICISupportTicketLog.Type of
                ICISupportTicketLog.Type::"External Message":
                    //Description := STRSUBSTNO(DescriptionTemplateTxt, ICISupportTicketLog.Type, Name, ICISupportTicketLog."Creation Date", ICISupportTicketLog."Creation Time");
                    Description := STRSUBSTNO(DescriptionMailTemplateTxt, Name, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2> Uhr'));
                ICISupportTicketLog.Type::State:
                    BEGIN
                        ICISupportTicketLog.CalcFields(Data);
                        ICISupportTicketLog.Data.CreateInStream(lInStream);
                        lInStream.Read(StateText);

                        IF ICISupportTicketLog."Record ID".TableNo = DATABASE::"ICI Support Process Stage" THEN BEGIN
                            lRecordRef := ICISupportTicketLog."Record ID".GetRecord();
                            lRecordRef.SetTable(ICISupportProcessStage);
                            ICISupportProcessStage.GET(ICISupportProcessStage."Process Code", ICISupportProcessStage.Stage);
                            StateText := ICISupportProcessStage.GetDescription();
                        END;

                        Description := STRSUBSTNO(DescriptionTemplateTxt, StateText, Name, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2> Uhr'));
                    END;
                ICISupportTicketLog.Type::"External File":
                    Description := STRSUBSTNO(DescriptionTemplateTxt, ICISupportTicketLog.Type, Name, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2> Uhr'));
                ICISupportTicketLog.Type::Escalation:
                    begin
                        IF ICISupportTicketLog."Record ID".TableNo <> DATABASE::"ICI Escalation Level" THEN // Bugfix: Renumbering Issue
                            exit;
                        lRecordRef := ICISupportTicketLog."Record ID".GetRecord();
                        lRecordRef.SetTable(ICIEscalationLevel);
                        IF ICIEscalationLevel.GET(ICIEscalationLevel."Escalation Code", ICIEscalationLevel."Line No.") THEN // Ooopsies. SetTable is sneaky ~.~
                            Description := StrSubstNo(EscalationLbl, ICIEscalationLevel."Start Time", ICIEscalationGroup.TableCaption);
                    end;
                ICISupportTicketLog.Type::"Time Registration": // Depricated
                    Description := STRSUBSTNO(TimeRegistrationTemplateTxt, ICISupportTicketLog."Time Registration User", ICISupportTicketLog."Time Registration Qty. Hours", ICISupportTicketLog."Time Reg. Unit of Measure");

                ICISupportTicketLog.Type::Forwarding:
                    begin
                        IF Contact.GET(ICISupportTicketLog."Data Text") then
                            ForwardToName := Contact.Name;
                        IF Contact.GET(ICISupportTicketLog."Data Text 2") then
                            ForwardFromName := Contact.Name;
                        IF (ForwardFromName <> '') AND (ForwardToName <> '') THEN
                            Description := STRSUBSTNO(ForwardingTemplateTxt, ForwardToName, ForwardFromName, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2> Uhr'))
                        else begin
                            ICISupportTicketLog.CalcFields(Data);
                            ICISupportTicketLog.Data.CreateInStream(lInStream);
                            lInStream.Read(Description);
                        end;
                    end;

                ELSE
                    Description := STRSUBSTNO(DescriptionTemplateTxt, ICISupportTicketLog.Type, Name, Format(ICISupportTicketLog."Creation Date", 0, '<Day,2>.<Month,2>.<Year4>'), FORMAT(ICISupportTicketLog."Creation Time", 0, '<Hours24,2>:<Minutes,2> Uhr'));
            End;
        end;
    end;

    procedure GetTicketLogSenderName(EntryNo: Integer) SenderName: Text
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
    begin
        ICISupportTicketLog.GET(EntryNo);
        Case (ICISupportTicketLog."Created By Type") of
            ICISupportTicketLog."Created By Type"::Contact:
                IF Contact.GET(ICISupportTicketLog."Created By") THEN
                    SenderName := Contact.Name;
            ICISupportTicketLog."Created By Type"::User:
                IF ICISupportUser.GET(ICISupportTicketLog."Created By") THEN
                    SenderName := ICISupportUser.Name;

            ICISupportTicketLog."Created By Type"::System:
                SenderName := 'System';
        end;
    end;

    procedure TicketStageChanged(var ICISupportTicket: Record "ICI Support Ticket"; LineNo: Integer)
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportProcessStage: Record "ICI Support Process Stage";
        lOutStream: OutStream;
        TicketNo: Code[20];
        //StateChangedTemplateTxt: Label 'Statuswechsel von %1 nach %2', Comment = '%1 = Old State; %2 = New State;';

        StateNewTemplateTxt: Label 'Status %1', Comment = '%1 = State';

        NewStateText: Text;
    begin
        TicketNo := ICISupportTicket."No.";
        ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");

        ICISupportProcessStage.SetRange(Stage, LineNo);
        IF ICISupportProcessStage.FindFirst() then
            NewStateText := ICISupportProcessStage.Description;

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);

        ICISupportTicketLog.Calcfields(Data);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream);

        lOutStream.Write(STRSUBSTNO(StateNewTemplateTxt, NewStateText));

        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::State);
        ICISupportTicketLog.Validate("State Subtype", ICISupportProcessStage."Ticket State");
        ICISupportTicketLog.Validate("Record ID", ICISupportProcessStage.RecordId);

        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicket."Last Activity By Type");
        ICISupportTicketLog.Validate("Created By", ICISupportTicket."Last Activity By Code");

        ICISupportTicketLog.Modify(true);

        if ICISupportProcessStage."Support User ID" <> '' then
            ICISupportTicket.Validate("Support User ID", ICISupportProcessStage."Support User ID");
    end;

    procedure LogTicketUserForwarding(var SupportTicket: Record "ICI Support Ticket"; OldUserID: Code[50])
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lOutStream: OutStream;
        ForwardingLbl: Label 'Forwarded to %1 - %2', Comment = '%1=UserID;%2=UserName|de-DE=Weitergeleitet an %1 - %2';
    begin
        IF SupportTicket."Support User ID" = OldUserID THEN
            exit;

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.Calcfields(Data);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream);

        SupportTicket.CalcFields("Support User Name");
        lOutStream.Write(StrSubstNo(ForwardingLbl, SupportTicket."Support User ID", SupportTicket."Support User Name"));

        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::Forwarding);

        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
        ICISupportTicketLog.Validate("Created By", UserId());

        ICISupportTicketLog.Modify(true);

        // Send Mail to User
        ICISupportMailMgt.TicketForwardedToUser(SupportTicket, ICISupportTicketLog."Entry No.");

    end;


    procedure LogTicketContactForwarding(var SupportTicket: Record "ICI Support Ticket"; OldContactNo: Code[20])
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lOutStream: OutStream;
        ForwardingLbl: Label 'Forwarded to %1 - %2', Comment = '%1=ContactNo;%2=ContactName|de-DE=Weitergeleitet an %1 - %2';
    begin
        IF SupportTicket."Current Contact No." = OldContactNo THEN
            exit;

        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", SupportTicket."No.");
        ICISupportTicketLog.Calcfields(Data);
        ICISupportTicketLog.Data.CreateOutStream(lOutStream);

        lOutStream.Write(StrSubstNo(ForwardingLbl, SupportTicket."Current Contact No.", SupportTicket."Curr. Contact Name"));

        ICISupportTicketLog.Validate("Data Text", SupportTicket."Current Contact No.");
        ICISupportTicketLog.Validate("Data Text 2", OldContactNo);

        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::Forwarding);

        IF GuiAllowed THEN begin
            ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
            ICISupportTicketLog.Validate("Created By", UserId());
        end else begin
            ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::Contact);
            IF SupportTicket."Last Activity By Type" = SupportTicket."Last Activity By Type"::Contact THEN
                ICISupportTicketLog.Validate("Created By", SupportTicket."Last Activity By Code")
            Else
                ICISupportTicketLog.Validate("Created By", OldContactNo);
        end;


        ICISupportTicketLog.Modify(true);

        // Send Mail to Contact - If ticket is not still in preparation
        IF SupportTicket."Ticket State" <> SupportTicket."Ticket State"::Preparation THEN
            ICISupportMailMgt.TicketForwardedToContact(SupportTicket, ICISupportTicketLog."Entry No.");

    end;

    procedure AddSupportTicketFileLog(ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        lRecordRef: RecordRef;
        AttachmentName: Text[250];
        AttachmentBase64: Text;
        AttachmentContentType: Text[250];
        FileSize: Integer;
        NotificationJSON: JsonObject;
        TicketNo: Code[20];
        AttachmentsArray: JsonArray;
        AttachmentObj: JsonObject;
        jTok: JsonToken;
        NoOfAttachments: Integer;
        Counter: Integer;
        DataUriLbl: Label 'data:application/pdf;base64,%1', Comment = '%1=Base64 Content';
    begin

        // Get Mail Values
        NotificationJSON := ICINotificationDialogMgt.CreateNotificationValues();

        IF NotificationJSON.GET('TicketNo', JTok) then
            TicketNo := JTok.AsValue().AsText();

        ICISupportTicket.GET(TicketNo);
        lRecordRef.GET(ICISupportTicket.RecordId());

        IF NotificationJSON.GET('Attachments', JTok) then
            AttachmentsArray := JTok.AsArray();

        // Add Attachments - Must be After EmailMessage.Create
        NoOfAttachments := AttachmentsArray.Count();
        For Counter := 0 to (NoOfAttachments - 1) do // JSON Arrays start at 0
            IF AttachmentsArray.GET(Counter, JTok) THEN begin
                CLEAR(AttachmentObj);
                AttachmentObj := JTok.AsObject();
                IF AttachmentObj.Get('AttachmentName', JTok) then
                    AttachmentName := (JTok.AsValue().AsText());
                IF AttachmentObj.Get('AttachmentBase64', JTok) then
                    AttachmentBase64 := (JTok.AsValue().AsText());
                IF AttachmentObj.Get('ContentType', JTok) then begin
                    AttachmentContentType := (JTok.AsValue().AsText());
                    if AttachmentContentType = 'application/pdf' then
                        AttachmentBase64 := StrSubstNo(DataUriLbl, AttachmentBase64);
                end;

                FileSize := ICISupportMailMgt.GetFileSizeByB64(AttachmentBase64);

                // ADD Attachment
                ICISupDragboxMgt.setConfiguration(ICISupportTicket.TableName);
                ICISupDragboxMgt.setRecordRef(lRecordRef);
                ICISupDragboxMgt.AddSupportTicketFileLog(lRecordRef, AttachmentName, AttachmentBase64, FileSize, false, ICINotificationDialogMgt.GetInternally());
            end;
    end;

    procedure GetCurrentDateTime(): DateTime
    var
        SupportSetup: Record "ICI Support Setup";
        UTCDatetime: DateTime;
        LastOctSunday: Date;
        LastMarSunday: Date;
    begin
        UTCDatetime := CurrentDateTime();
        IF NOT GuiAllowed then begin
            SupportSetup.GET();
            IF NOT SupportSetup."Daylight Savings Time" THEN
                EXIT(UTCDatetime);

            LastOctSunday := DMY2Date(1, 11);
            LastOctSunday := CALCDATE('<-WD7>', LastOctSunday);  // Letzter sonntag im oktober

            LastMarSunday := DMY2Date(1, 4);
            LastMarSunday := CALCDATE('<-WD7>', LastMarSunday);  // Letzter sonntag im märz

            IF (Today() > LastMarSunday) AND (Today() < LastOctSunday) THEN
                UTCDatetime += (2 * 1000 * 60 * 60) // Add 2 Hours to be in Berlin Time - Sommerzeit
            Else
                UTCDatetime += (1 * 1000 * 60 * 60); // Add 1 Hours to be in Berlin Time - Winterzeit
        end;

        EXIT(UTCDatetime);
    end;

    procedure ResetTicketTime(TicketNo: Code[20])
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
    begin
        ICISupportTicketLog.Init();
        ICISupportTicketLog.Insert(true);
        ICISupportTicketLog.VALIDATE("Support Ticket No.", TicketNo);
        ICISupportTicketLog.Validate(Type, ICISupportTicketLog.Type::TimeReset);
        ICISupportTicketLog.VALIDATE("Created By Type", ICISupportTicketLog."Created By Type"::User);
        ICISupportTicketLog.Validate("Created By", UserId());
        ICISupportTicketLog.Validate("Creation Timestamp", CurrentDateTime);
        ICISupportTicketLog.Validate("Creation Date", Today);
        ICISupportTicketLog.Validate("Creation Time", Time);
        ICISupportTicketLog.Modify(true);
    end;
}
