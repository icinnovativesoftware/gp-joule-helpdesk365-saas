page 56277 "ICI Support Ticket List"
{
    ApplicationArea = All;
    Caption = 'Support Ticket List', Comment = 'de-DE=Support Ticket Liste';
    PageType = List;
    SourceTable = "ICI Support Ticket";
    SourceTableView = sorting("No.") order(descending);
    UsageCategory = Lists;
    CardPageId = "ICI Support Ticket Card";
    Editable = false;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'No.', Comment = 'de-DE=Nr.';
                    StyleExpr = EscalatedStyleExpr;
                }
                field(AttentionText; AttentionText)
                {
                    ApplicationArea = All;
                    Caption = 'Highlight for User', Comment = 'de-DE=Neue Änderung';
                    ToolTip = 'Highlight for User', Comment = 'de-DE=Neue Änderung durch Kontakt';
                    Style = Attention;
                    Width = 10;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                    StyleExpr = EscalatedStyleExpr;
                }
                field("Ticket State"; Rec."Ticket State")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket State', Comment = 'de-DE=Ticket Status';
                }
                field("Process Description"; Rec."Process Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Process State', Comment = 'de-DE=Prozessbeschreibung';
                }

                field("Process Stage Description"; Rec."Process Stage Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Process Stage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
                }
                field("Company Contact Name"; Rec."Comp. Contact Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Company Contact Name', Comment = 'de-DE=Unternehmensname';

                    trigger OnDrillDown()
                    begin
                        Rec.DrillDownContact(Rec."Company Contact No.");
                    end;
                }
                field("Current Contact Name"; Rec."Curr. Contact Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Current Contact Name', Comment = 'de-DE=Kontaktname';

                    trigger OnDrillDown()
                    begin
                        Rec.DrillDownContact(Rec."Current Contact No.");
                    end;
                }
                field("Category 1 Description"; Rec."Category 1 Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Category 1 Description', Comment = 'de-DE=Kategorie 1 Beschreibung';
                }
                field("Category 2 Description"; Rec."Category 2 Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Category 2 Description', Comment = 'de-DE=Kategorie 2 Beschreibung';
                }
                field("Support User Name"; Rec."Support User Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Support User Name', Comment = 'de-DE=Benutzername';
                }
                field("Serial No."; Rec."Serial No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Serial No.', Comment = 'de-DE=Seriennr.';
                }
                field("Ticket Board Order"; Rec."Ticket Board Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Ticket Board Order', Comment = 'de-DE=Ticket Board Sortierung';
                    Visible = ShowTicketBoard;
                }
                field("Progress Ratio"; Rec."Progress Ratio")
                {
                    ApplicationArea = All;
                    ToolTip = 'Progress', Comment = 'de-DE=Zeigt den Fortschritt des Ticketprozesses an';
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        ICISupportUser: Record "ICI Support User";
    begin
        Rec.FilterGroup(0);
        IF (Rec.GetFilters = '') THEN // Wenn aus den Kacheln kommt (Flowfields), dann keine weiteren Filter anwenden
            IF ICISupportUser.GetCurrUser(ICISupportUser) THEN
                IF ICISupportUser."Dep. Filter" <> '' then begin
                    Rec.FilterGroup(4);
                    Rec.SetFilter("Department Code", ICISupportUser."Dep. Filter");
                end;

        IF ICISupportUser.GetCurrUser(ICISupportUser) then
            ShowTicketBoard := ICISupportUser."Ticket Board Active";
    end;

    trigger OnAfterGetRecord()
    begin
        Clear(AttentionText);
        IF Rec."Highlight for User" then
            AttentionText := '!';

        CLEAR(EscalatedStyleExpr);
        IF Rec."Ticket State" = Rec."Ticket State"::Preparation then
            EscalatedStyleExpr := 'Subordinate';
        if Rec.isOverdue() then
            EscalatedStyleExpr := 'Unfavorable';
    end;



    var

        AttentionText: Text;
        EscalatedStyleExpr: Text;
        ShowTicketBoard: Boolean;

}
