table 56298 "ICI Support Ticket Item"
{
    Caption = 'Support Ticket Item', Comment = 'de-DE=Support Ticket Item';

    fields
    {
        field(1; "Support Ticket No."; Code[20])
        {
            Caption = 'Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            DataClassification = SystemMetadata;
            TableRelation = "ICI Support Ticket"."No.";
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No.', Comment = 'de-DE=Zeilenn.';
            DataClassification = SystemMetadata;
        }
        field(10; Type; Option)
        {
            Caption = 'Type', Comment = 'de-DE=Art';
            DataClassification = CustomerContent;
            InitValue = Item;
            OptionCaption = 'ServiceItem,Item,Resource,ServiceCost', Comment = 'de-DE=Serviceartikel,Artikel,Resource,Servicekosten';
            OptionMembers = ServiceItem,Item,Resource,ServiceCost;

            trigger OnValidate()
            begin
                IF Type <> xRec.Type THEN
                    VALIDATE("No.", '');
            end;
        }
        field(11; "No."; Code[20])
        {
            Caption = 'No.', Comment = 'de-DE=Nr.';
            DataClassification = CustomerContent;
            TableRelation = IF (Type = CONST(Item)) Item."No."
            ELSE
            IF (Type = CONST(ServiceItem)) "Service Item"."No."
            ELSE
            IF (Type = CONST(Resource)) Resource."No."
            ELSE
            IF (Type = CONST(ServiceCost)) "Service Cost".Code;

            trigger OnValidate()
            var
                Item: Record Item;
                ServiceItem: Record "Service Item";
                Resource: Record Resource;
                ServiceCost: Record "Service Cost";
            begin
                IF "No." <> xRec."No." THEN BEGIN

                    Description := '';
                    Quantity := 0;
                    "Unit of Measure Code" := '';

                    CASE Type OF
                        Type::Item:
                            IF Item.GET("No.") THEN BEGIN
                                VALIDATE(Description, Item.Description);
                                VALIDATE("Unit of Measure Code", Item."Sales Unit of Measure");
                            END;
                        Type::Resource:
                            IF Resource.GET("No.") THEN BEGIN
                                VALIDATE(Description, Resource.Name);
                                VALIDATE("Unit of Measure Code", Resource."Base Unit of Measure");
                            END;
                        Type::ServiceItem:
                            IF ServiceItem.GET("No.") THEN BEGIN
                                VALIDATE(Description, ServiceItem.Description);
                                VALIDATE(Quantity, 1);
                            END;
                        Type::ServiceCost:
                            IF ServiceCost.GET("No.") THEN BEGIN
                                VALIDATE(Description, ServiceCost.Description);
                                VALIDATE(Quantity, 1);
                            END;
                    END;
                END;

            end;
        }
        field(12; Description; Text[100])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(20; Quantity; Decimal)
        {
            Caption = 'Quantity', Comment = 'de-DE=Menge';
            DataClassification = CustomerContent;
        }
        field(21; "Unit of Measure Code"; Code[10])
        {
            Caption = 'Unit of Measure Code', Comment = 'de-DE=Einheitencode';
            DataClassification = CustomerContent;
            TableRelation = IF (Type = CONST(Item)) "Item Unit of Measure".Code WHERE("Item No." = FIELD("No."));

            trigger OnValidate()
            begin
                IF Type = Type::ServiceItem THEN
                    TESTFIELD("Unit of Measure Code", '');
            end;
        }
        field(22; "Item Variant Code"; Code[10])
        {
            Caption = 'Item Variant Code', Comment = 'de-DE=Variantencode';
            DataClassification = CustomerContent;
            TableRelation = "Item Variant".Code WHERE("Item No." = FIELD("No."));

            trigger OnValidate()
            begin
                TESTFIELD(Type, Type::Item);
            end;
        }
        field(23; "Work Type Code"; Code[20])
        {
            Caption = 'Work type', Comment = 'de-DE=Arbeitstyp';
            TableRelation = "Work Type";
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                TestField("Type", "Type"::Resource);
                TestField("No.");
            end;
        }
        field(50; Comment; Text[250])
        {
            Caption = 'Comment', Comment = 'de-DE=Kommentar';
            DataClassification = CustomerContent;
        }
        field(51; "Use in Sales Doc."; Boolean)
        {
            Caption = 'Use in Sales Doc.', Comment = 'de-DE=In VK Belegen verwenden';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                IF (Type = Type::ServiceItem) or (Type = Type::ServiceCost) THEN
                    TESTFIELD(Type, Type::Item)
            end;
        }
        field(52; "Use in Purch. Doc."; Boolean)
        {
            Caption = 'Use in Purch. Doc.', Comment = 'de-DE=In EK Belegen verwenden';
            DataClassification = CustomerContent;

            trigger OnValidate()
            begin
                TESTFIELD(Type, Type::Item);
            end;
        }
        field(53; "Use in Service Doc."; Boolean)
        {
            Caption = 'Use in Service Doc.', Comment = 'de-DE=In Service Belegen verwenden';
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Support Ticket No.", "Line No.")
        {
            Clustered = true;
        }
    }

}
