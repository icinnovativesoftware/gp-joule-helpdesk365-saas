codeunit 56293 "ICI Chart Mgt."
{
    procedure GenerateDataTicketsByUser(var BusinessChartBuffer: Record "Business Chart Buffer")
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportTicket: Record "ICI Support Ticket";
        Index: Integer;
        ChartType: Integer;
        NoofTicketsLbl: Label 'No of Tickets', Comment = 'de-DE=Anzahl Tickets';
        UserLbl: Label 'User', Comment = 'de-DE=Support Mitarbeiter';

    begin
        BusinessChartBuffer.Initialize(); // 1.

        IF Not ICISupportUser.ReadPermission THEN
            exit;
        IF not ICISupportTicket.ReadPermission THen
            exit;

        ChartType := BusinessChartBuffer."Chart Type"::Line.AsInteger(); // >V18 
        IF ICISupportUser.GetCurrUser(ICISupportUser) THEN
            ChartType := ICISupportUser."Tickets by User Chart Type";

        BusinessChartBuffer.AddMeasure(NoofTicketsLbl, 1, BusinessChartBuffer."Data Type"::Integer, ChartType); // 2
        BusinessChartBuffer.SetXAxis(UserLbl, BusinessChartBuffer."Data Type"::String); // 3

        Index := 0;
        IF ICISupportUser.FindSet() THEN
            repeat
                CLEAR(ICISupportTicket);
                ICISupportTicket.SetRange("Support User ID", ICISupportUser."User ID");
                ICISupportTicket.setfilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);

                if (ICISupportTicket.Count() > 0) THEN begin
                    BusinessChartBuffer.AddColumn(ICISupportUser."User ID"); // 4
                    BusinessChartBuffer.SetValueByIndex(0, Index, ICISupportTicket.Count()); // 5
                    Index += 1;
                end;

            until ICISupportUser.Next() = 0;
    end;

    procedure GenerateDataTicketsByCompany(var BusinessChartBuffer: Record "Business Chart Buffer")
    var
        ICISupportUser: Record "ICI Support User";
        Contact: Record Contact;
        ICISupportTicket: Record "ICI Support Ticket";
        Index: Integer;
        ChartType: Integer;
        NoofTicketsLbl: Label 'No of Tickets', Comment = 'de-DE=Anzahl Tickets';
        CopmanyLbl: Label 'Company', Comment = 'de-DE=Kunde';

    begin
        BusinessChartBuffer.Initialize(); // 1.

        IF Not ICISupportUser.ReadPermission THEN
            exit;
        IF not ICISupportTicket.ReadPermission THen
            exit;
        if not Contact.ReadPermission Then
            exit;

        ChartType := BusinessChartBuffer."Chart Type"::Line.AsInteger(); // >V18 
        IF ICISupportUser.GetCurrUser(ICISupportUser) THEN
            ChartType := ICISupportUser."Tickets by Company Chart Type";


        BusinessChartBuffer.AddMeasure(NoofTicketsLbl, 1, BusinessChartBuffer."Data Type"::Integer, ChartType); // 2
        BusinessChartBuffer.SetXAxis(CopmanyLbl, BusinessChartBuffer."Data Type"::String); // 3

        Index := 0;
        Contact.SetRange("ICI Support Active", true);
        Contact.SetRange(Type, Contact.Type::Company);
        IF Contact.FindSet() THEN
            repeat
                CLEAR(ICISupportTicket);
                ICISupportTicket.SetRange("Company Contact No.", Contact."No.");
                ICISupportTicket.setfilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);

                if (ICISupportTicket.Count() > 0) THEN begin
                    BusinessChartBuffer.AddColumn(Contact."Name"); // 4 - Ugh ...
                    BusinessChartBuffer.SetValueByIndex(0, Index, ICISupportTicket.Count()); // 5
                    Index += 1;
                end;

            until Contact.Next() = 0;
    end;


    procedure DrilldownTicketsByUser(ClickedUserID: Text)
    var
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        ICISupportTicket.SetRange("Support User ID", ClickedUserID);
        ICISupportTicket.setfilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
        PAGE.RUN(PAGE::"ICI Support Ticket List", ICISupportTicket);
    end;

    procedure DrilldownTicketsByCompany(ClickedCompanyName: Text)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ContactFilter: Text;
    begin
        Contact.SetRange(Name, ClickedCompanyName);
        Contact.SetRange(Type, Contact.Type::Company);
        IF Contact.FINDSET() then
            repeat
                IF ContactFilter <> '' then
                    ContactFilter := ContactFilter + '|';
                ContactFilter := ContactFilter + Contact."No.";

            until Contact.Next() = 0;

        ICISupportTicket.Setfilter("Company Contact No.", '%1', ContactFilter);
        ICISupportTicket.setfilter("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
        PAGE.RUN(PAGE::"ICI Support Ticket List", ICISupportTicket);
    end;
}
