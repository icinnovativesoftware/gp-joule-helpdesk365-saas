page 56344 "ICI Tickets By Company Chart"
{

    Caption = 'Tickets By Company Chart', Comment = 'de-DE=Tickets pro Kunde';
    PageType = CardPart;
    SourceTable = "Business Chart Buffer";

    layout
    {
        area(content)
        {
            // For Use with BC24 or greater
            usercontrol(BusinessChart; BusinessChart)

            // For Use with BC23 or less
            // usercontrol(BusinessChart; "Microsoft.Dynamics.Nav.Client.BusinessChart")
            {
                ApplicationArea = All;

                trigger AddInReady()
                begin
                    // For Use with BC24 or greater
                    IsChartAddInReady := true;
                    UpdateBusinessChart();

                    // For Use with BC23 or less
                    // UpdateChart();
                end;

                trigger Refresh()
                begin
                    // For Use with BC24 or greater
                    UpdateBusinessChart();

                    // For Use with BC23 or less
                    // UpdateChart();
                end;

                trigger DataPointClicked(point: JsonObject)
                var
                    JsonTokenXValueString: JsonToken;
                    XValueString: text;
                begin
                    if not point.get('XValueString', JsonTokenXValueString) THEN
                        exit;

                    XValueString := Format(JsonTokenXValueString);
                    XValueString := Delchr(XValueString, '=', '"');
                    ICIChartMgt.DrilldownTicketsByCompany(XValueString);
                end;
            }

        }
    }
    // For Use with BC24 or greater
    local procedure UpdateBusinessChart()
    begin
        if not IsChartAddInReady then
            exit;
        ICIChartMgt.GenerateDataTicketsByCompany(Rec);
        Rec.UpdateChart(CurrPage.BusinessChart);
    end;

    // For Use with BC23 or less
    // local procedure UpdateChart()
    // begin
    //     ICIChartMgt.GenerateDataTicketsByCompany(Rec);
    //     Rec.Update(CurrPage.BusinessChart);
    // end;

    var
        ICIChartMgt: Codeunit "ICI Chart Mgt.";
        IsChartAddInReady: Boolean;
}

