page 56301 "ICI Update Process"
{
    UsageCategory = None;
    Caption = 'ICI Update Process', Comment = 'de-DE=Ticket Prozessstufe aktualisieren';
    DeleteAllowed = false;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field(Actiontype; ActionType)
                {
                    Caption = 'Action', Comment = 'de-DE=Aktion';
                    OptionCaption = 'Next,Previous,Skip,Jump', Comment = 'de-DE=Nächster,Vorheriger,Überspringen,Springe zu';
                    ApplicationArea = RelationshipMgmt;
                    ToolTip = 'Specifies the Options available to the target Ticket in the respective Process and Stage', Comment = 'Gibt an, welche Optionen für das ausgewählte Ticket in speziellen Prozess und Stufe möglich sind.';
                    ValuesAllowed = Next, Previous, Skip, Jump;


                    trigger OnValidate()
                    begin
                        if ActionType = ActionType::Jump then
                            JumpActionTypeOnValidate();
                        if ActionType = ActionType::Skip then
                            SkipActionTypeOnValidate();
                        if ActionType = ActionType::Previous then
                            PreviousActionTypeOnValidate();
                        if ActionType = ActionType::Next then
                            NextActionTypeOnValidate();
                    end;
                }
                field(CurrentProcessStage; CurrentProcessStage)
                {
                    ApplicationArea = All;
                    ToolTip = 'Current Process Stage', Comment = 'de-DE=Aktuelle Stufe';
                    Caption = 'Current Process Stage', Comment = 'de-DE=Aktuelle Stufe';
                    Editable = false;
                }
                field(CurrentProcessStageDescription; CurrentProcessStageDescription)
                {
                    ApplicationArea = All;
                    ToolTip = 'Current Process Stage Description', Comment = 'de-DE=Aktuelle Stufenbeschreibung';
                    Caption = 'Current Process Stage Description', Comment = 'de-DE=Aktuelle Stufenbeschreibung';
                    Editable = false;
                }
                field(NextProcessStage; NextProcessStage)
                {
                    ApplicationArea = All;
                    ToolTip = 'Next Process Stage', Comment = 'de-DE=Neue Stufe';
                    Caption = 'Next Process Stage', Comment = 'de-DE=Neue Stufe';
                    Editable = NextProcessStageEditable;

                    trigger OnValidate()
                    begin
                        Error(''); // No Manual Changes
                    end;

                    trigger OnLookup(var text: Text): Boolean
                    var
                        ICISupportProcessStage: Record "ICI Support Process Stage";
                        TempICISupportProcessStage: Record "ICI Support Process Stage" temporary;
                    begin
                        TempICISupportProcessStage.DELETEALL();
                        ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
                        case ActionType of
                            ActionType::Skip:
                                begin
                                    ICISupportProcessStage.SetFilter(Stage, '>%1', ICISupportTicket."Process Stage");
                                    // Fill Temp Rec with allowed Values
                                    IF ICISupportProcessStage.FindSet() THEN
                                        repeat
                                            TempICISupportProcessStage := ICISupportProcessStage;
                                            IF NOT TempICISupportProcessStage.Insert() THEN
                                                TempICISupportProcessStage.MODIFY();
                                        until (NOT ICISupportProcessStage."Skip Allowed") OR (ICISupportProcessStage.Next() = 0);

                                    if ACTION::LookupOK = PAGE.RunModal(0, TempICISupportProcessStage) then begin
                                        NextProcessStage := TempICISupportProcessStage.Stage;
                                        NextProcessStageDescription := TempICISupportProcessStage.Description;
                                    end
                                end;
                            ActionType::Jump:
                                begin
                                    ICISupportProcessStage.SetRange("Jump To Allowed", true);
                                    // Fill Temp Rec with allowed Values
                                    IF ICISupportProcessStage.FindSet() THEN
                                        repeat
                                            TempICISupportProcessStage := ICISupportProcessStage;
                                            IF NOT TempICISupportProcessStage.Insert() THEN
                                                TempICISupportProcessStage.MODIFY();
                                        until (ICISupportProcessStage.Next() = 0);
                                    if ACTION::LookupOK = PAGE.RunModal(0, TempICISupportProcessStage) then begin
                                        NextProcessStage := TempICISupportProcessStage.Stage;
                                        NextProcessStageDescription := TempICISupportProcessStage.Description;
                                    end

                                end;
                        end;
                    end;
                }
                field(NextProcessStageDescription; NextProcessStageDescription)
                {
                    ApplicationArea = All;
                    ToolTip = 'Next Process Stage Description', Comment = 'de-DE=Neue Stufenbeschreibung';
                    Caption = 'Next Process Stage Description', Comment = 'de-DE=Neue Stufenbeschreibung';
                    Editable = false;
                }
            }
        }
    }
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        NextICISupportProcessStage: Record "ICI Support Process Stage";
        ChangeNotPossibleErr: Label 'Change not possible. Close anyways?', Comment = 'de-DE=Änderung nicht möglich. Möchten Sie die Seite dennoch schließen?';
    begin
        IF NOT NextICISupportProcessStage.GET(ICISupportTicket."Process Code", NextProcessStage, '') then
            EXIT(Confirm(ChangeNotPossibleErr));
        IF (CloseAction = CloseAction::LookupOK) OR (CloseAction = CloseAction::OK) then begin
            // ICISupportTicket.TestField("Process Activated");
            ICISupportTicket.Validate("Process Stage", NextProcessStage);
            ICISupportTicket.Modify(true);
        end
    end;

    /*local procedure FirstActionTypeOnValidate()
    begin
        if not OptionFirstEnable then
            Error(SelectionErr, ActionType);


        NextProcessStageEditable := FALSE;
    end;*/

    local procedure NextActionTypeOnValidate()
    var
        NextICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        if not OptionNextEnable then
            Error(SelectionErr, ActionType);


        NextProcessStageEditable := FALSE;
        // Get Next
        NextICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        NextICISupportProcessStage.SetFilter(Stage, '>%1', ICISupportTicket."Process Stage");
        NextICISupportProcessStage.FindFirst();

        NextProcessStage := NextICISupportProcessStage.Stage;
        NextProcessStageDescription := NextICISupportProcessStage.Description;
    end;

    local procedure PreviousActionTypeOnValidate()
    var
        PreviousICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        if not OptionPreviousEnable then
            Error(SelectionErr, ActionType);


        NextProcessStageEditable := FALSE;

        // Get Previous
        PreviousICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        PreviousICISupportProcessStage.SetFilter(Stage, '<%1', ICISupportTicket."Process Stage");
        PreviousICISupportProcessStage.FindLast();

        NextProcessStage := PreviousICISupportProcessStage.Stage;
        NextProcessStageDescription := PreviousICISupportProcessStage.Description;

    end;

    local procedure SkipActionTypeOnValidate()
    begin
        if not OptionSkipEnable then
            Error(SelectionErr, ActionType);

        NextProcessStageEditable := TRUE;
    end;

    local procedure JumpActionTypeOnValidate()
    begin
        if not OptionJumpEnable then
            Error(SelectionErr, ActionType);

        NextProcessStageEditable := TRUE;
    end;

    procedure SetTicket(pICISupportTicket: Record "ICI Support Ticket")
    var
        NextICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        ICISupportTicket := pICISupportTicket;

        CurrentProcessStage := ICISupportTicket."Process Stage";
        ICISupportTicket.CalcFields("Process Stage Description");
        CurrentProcessStageDescription := ICISupportTicket."Process Stage Description";

        NextICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        NextICISupportProcessStage.SetFilter(Stage, '>%1', ICISupportTicket."Process Stage");
        IF NextICISupportProcessStage.FindFirst() THEN BEGIN
            NextProcessStage := NextICISupportProcessStage.Stage;
            NextProcessStageDescription := NextICISupportProcessStage.Description;
        END;

        UpdateEditable();
    end;

    local procedure UpdateEditable()
    var
        ICISupportProcessStage: Record "ICI Support Process Stage";

        NextICISupportProcessStage: Record "ICI Support Process Stage";

        PreviousICISupportProcessStage: Record "ICI Support Process Stage";
        JumpToICISupportProcessStage: Record "ICI Support Process Stage";
        NextExists: Boolean;
        PreviousExists: Boolean;
    begin
        // Get Current
        ICISupportProcessStage.GET(ICISupportTicket."Process Code", ICISupportTicket."Process Stage");


        // Get Next
        NextICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        NextICISupportProcessStage.SetFilter(Stage, '>%1', ICISupportTicket."Process Stage");
        NextExists := NextICISupportProcessStage.FindFirst();
        IF NextExists then
            OptionNextEnable := true;


        IF NextICISupportProcessStage."Skip Allowed" then
            OptionSkipEnable := TRUE;

        // Get Previous
        PreviousICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        PreviousICISupportProcessStage.SetFilter(Stage, '<%1', ICISupportTicket."Process Stage");
        PreviousExists := Not PreviousICISupportProcessStage.IsEmpty();
        IF PreviousExists AND (ICISupportProcessStage."Previous Allowed") then
            OptionPreviousEnable := true;


        // Get Jump Tp
        JumpToICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
        JumpToICISupportProcessStage.SetRange("Jump To Allowed", true);
        IF NOT JumpToICISupportProcessStage.IsEmpty() then
            OptionJumpEnable := true;
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";

        CurrentProcessStage: Integer;

        NextProcessStage: Integer;

        CurrentProcessStageDescription: Text;

        NextProcessStageDescription: Text;


        ActionType: Option Next,Previous,Skip,Jump;

        OptionNextEnable: Boolean;

        OptionPreviousEnable: Boolean;

        OptionSkipEnable: Boolean;

        OptionJumpEnable: Boolean;
        SelectionErr: Label '%1 is not a valid selection.', Comment = '%1=ActionType|de-DE=%1 ist keine gültige Auswahl';
        NextProcessStageEditable: Boolean;

}
