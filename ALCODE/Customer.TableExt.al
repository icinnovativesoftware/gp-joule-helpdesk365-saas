tableextension 56297 "ICI Customer" extends Customer
{
    fields
    {
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                ICISupportTicket: Record "ICI Support Ticket";
            begin
                if Rec."No." <> '' then begin
                    ICISupportTicket.SetCurrentKey("Current Contact No.", "Company Contact No.", "Ticket State", "Customer No.");
                    ICISupportTicket.SetRange("Customer No.", "No.");
                    IF ICISupportTicket.Count() > 0 then
                        ICISupportTicket.MODIFYALL("Cust. Name", Name, false);
                end;
            end;
        }

        // Tansferfields from costomer - contact 56276-79 are different
        field(56280; "ICI Support Ticket No."; Code[20])
        {
            Caption = 'ICI Support Ticket No.', Comment = 'de-DE=Ticketnr.';
            TableRelation = "ICI Support Ticket";
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                SupportTicket: Record "ICI Support Ticket";
            begin
                IF SupportTicket.GET("ICI Support Ticket No.") then
                    PAGE.RUN(PAGE::"ICI Support Ticket Card", SupportTicket);
            end;
        }
        field(56281; "ICI Tickets - Open"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Customer No." = field("No."), "Ticket State" = const(Open)));
            Editable = false;
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56282; "ICI Tickets - Processing"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Customer No." = field("No."), "Ticket State" = const(Processing)));
            Editable = false;
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56283; "ICI Tickets - Waiting"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Customer No." = field("No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56284; "ICI Tickets - Closed"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("ICI Support Ticket" where("Customer No." = field("No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
        field(56285; "ICI Portal Cue Set Code"; Code[20])
        {
            Enabled = false;
            Caption = 'Portal Cue Set Code', Comment = 'de-DE=Kundenportalkachelsetcode';
            DataClassification = CustomerContent;
            TableRelation = "ICI Portal Cue Set";
        }
        field(56286; "ICI E-Mail TLD"; Code[80])
        {
            Enabled = false;
            Caption = 'ICI E-Mail TLD', Comment = 'de-DE=E-Mail TLD';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(56287; "ICI Source Mailrobot Log"; Integer)
        {
            Enabled = false;
            Caption = 'ICI Source Mailrobot Log', Comment = 'de-DE=Urspr. Mailrobotprotokolleintrag';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Log";
            Editable = false;
        }
        field(56288; "Mailrobot Source Mailbox Code"; Code[30])
        {
            Enabled = false;
            Caption = 'Mailrobot Source Mailbox Code', Comment = 'de-DE=Urspr. Mailrobot Postfach Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Mailbox";
            Editable = false;
        }
        field(56289; "Login Failures"; Integer)
        {
            Enabled = false;
            Editable = false;
            Caption = 'Login Failures', Comment = 'de-DE=Anmeldung Fehlversuche';
            DataClassification = SystemMetadata;
        }
        field(56290; "ICI Comp. Tickets - Open"; Integer)
        {
            Enabled = false;
            FieldClass = FlowField;
            //CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Open)));
            Caption = 'No. of Tickets - Open', Comment = 'de-DE=Tickets - Offen';
        }
        field(56291; "ICI Comp. Tickets - Processing"; Integer)
        {
            Enabled = false;
            FieldClass = FlowField;
            //CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Processing)));
            Caption = 'No. of Tickets - Processing', Comment = 'de-DE=Tickets - In Bearbeitung';
        }
        field(56292; "ICI Comp. Tickets - Waiting"; Integer)
        {
            Enabled = false;
            FieldClass = FlowField;
            //CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Waiting)));
            Editable = false;
            Caption = 'No. of Tickets - Waiting', Comment = 'de-DE=Tickets - Warten';
        }
        field(56293; "ICI Comp. Tickets - Closed"; Integer)
        {
            Enabled = false;
            FieldClass = FlowField;
            //CalcFormula = count("ICI Support Ticket" where("Company Contact No." = field("Company No."), "Ticket State" = const(Closed)));
            Editable = false;
            Caption = 'No. of Tickets - Closed', Comment = 'de-DE=Tickets - Geschlossen';
        }
    }

    keys
    {
        key(Support; "ICI Support Ticket No.") { }
    }
}
