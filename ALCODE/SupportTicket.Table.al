table 56277 "ICI Support Ticket"
{
    Caption = 'Support Ticket';
    DataClassification = CustomerContent;
    LookupPageId = "ICI Support Ticket List";
    DrillDownPageId = "ICI Support Ticket List";
    DataCaptionFields = "No.", Description;
    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'No.', Comment = 'de-DE=Nr.';
            DataClassification = SystemMetadata;
        }
        field(10; Description; Text[250])
        {
            Caption = 'Description', Comment = 'de-DE=Beschreibung';
            DataClassification = CustomerContent;
        }
        field(11; "Company Contact No."; Code[20])
        {
            DataClassification = OrganizationIdentifiableInformation;
            Caption = 'Company Contact No.', Comment = 'de-DE=Unternehmenskontaktnr.';
            TableRelation = Contact."No." WHERE(Type = CONST(Company));
            trigger OnValidate()
            var
                Contact: Record Contact;
                ActivateContactLbl: Label 'Do you want to activate the Contact %1 - "%2" for the Helpdesk 365 Module', Comment = '%1 = Contact No.; %2 = Contact Name|de-DE=Wollen Sie den Kontakt %1 - "%2" für das Helpdesk365 Modul freischalten?';
            begin
                FillMissingContactOrCustomerInformation();
                IF Contact.GET("Company Contact No.") THEN begin
                    IF NOT Contact."ICI Support Active" then
                        IF GuiAllowed THEN
                            IF NOT CONFIRM(STRSUBSTNO(ActivateContactLbl, Contact."No.", Contact.Name)) THEN
                                ERROR('');

                    Contact.Validate("ICI Support Active", true);
                    Contact.Modify(true);
                end;
            end;
        }
        field(12; "Company Contact Name"; Text[100])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Comp. Contact Name';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup(Contact.Name Where("No." = FIELD("Company Contact No.")));
            Caption = 'Company Contact Name', Comment = 'de-DE=Unternehmen';
        }
        field(13; "Current Contact No."; Code[20])
        {
            DataClassification = OrganizationIdentifiableInformation;
            Caption = 'Current Contact No.', Comment = 'de-DE=Personenkontaktnr.';
            TableRelation = if ("Company Contact No." = const('')) Contact."No." else
            Contact."No." WHERE(Type = CONST(Person), "Company No." = FIELD("Company Contact No."));
            trigger OnValidate()
            var
                Contact: Record Contact;
                TicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                ActivateContactLbl: Label 'Do you want to activate the Contact %1 - "%2" for the Helpdesk 365 Module', Comment = '%1 = Contact No.; %2 = Contact Name;|de-DE=Wollen Sie den Kontakt %1 - "%2" für das Helpdesk365 Modul freischalten?';
            begin
                IF Contact.GET("Current Contact No.") THEN begin

                    IF NOT Contact."ICI Support Active" then
                        IF GuiAllowed THEN
                            IF NOT CONFIRM(STRSUBSTNO(ActivateContactLbl, Contact."No.", Contact.Name)) THEN
                                ERROR('');

                    Contact.Validate("ICI Support Active", true);
                    Contact.Modify(true);

                    if CurrFieldNo = Rec.FIELDNO("Current Contact No.") then // Prevent endless lookup from customer -> person -> ccustomer ->...
                        FillMissingContactOrCustomerInformation();

                    IF (xRec."Current Contact No." <> '') AND (xRec."Current Contact No." <> Rec."Current Contact No.") then
                        TicketLogMgt.LogTicketContactForwarding(Rec, xRec."Current Contact No."); // Erstzuweisung ist kein Weiterleiten
                end;
            end;
        }
        field(14; "Current Contact Name"; Text[100])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Curr. Contact Name';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup(Contact.Name Where("No." = FIELD("Current Contact No.")));
            Caption = 'Current Contact Name', Comment = 'de-DE=Kontakt';
        }
        field(15; "Support User ID"; Code[50])
        {
            DataClassification = EndUserIdentifiableInformation;
            TableRelation = "ICI Support User";
            Caption = 'Support User Code', Comment = 'de-DE=Zuständiger Benutzer';
            trigger OnValidate()
            var
                ICISupportUser: Record "ICI Support User";
                TicketBoardMgt: Codeunit "ICI Ticket Board Mgt.";
                TicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                TodoMgt: Codeunit "ICI Todo Mgt.";
                ChangeDepartmentLbl: Label 'Do you also want to change the Department to %1', Comment = '%1 = Department Code|de-DE=Wollen Sie auch den Abteilungscode auf %1 ändern?';
            begin
                // Confirm New Department Change, if guiallowed
                ICISupportUser.GET("Support User ID");
                IF (xRec."Department Code" = '') AND (ICISupportUser."Dep. Code" <> '') then
                    Validate("Department Code", ICISupportUser."Dep. Code");

                IF ICISupportUser."Dep. Code" <> Rec."Department Code" THEN
                    IF GuiAllowed AND (ICISupportUser."Dep. Code" <> '') THEN begin
                        IF Confirm(ChangeDepartmentLbl, true, ICISupportUser."Dep. Code") THEN
                            Validate("Department Code", ICISupportUser."Dep. Code");
                    end else
                        Validate("Department Code", ICISupportUser."Dep. Code");

                TicketBoardMgt.ForwardUserAddLastPrio(Rec, 1);
                IF (xRec."Support User ID" <> '') AND (xRec."Support User ID" <> Rec."Support User ID") AND ("Ticket State" <> "Ticket State"::Preparation) then begin
                    TicketLogMgt.LogTicketUserForwarding(Rec, xRec."Support User ID"); // Erstzuweisung ist kein Weiterleiten
                    TodoMgt.SupportUserChanged(Rec);
                end;
            end;
        }
        field(16; "Support User Name"; Text[250])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("ICI Support User".Name Where("User ID" = FIELD("Support User ID")));
            Caption = 'Support User Name', Comment = 'de-DE=Zuständiger Benutzername';
        }
        field(17; "Process Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Process Code', Comment = 'de-DE=Prozesscode';
            TableRelation = "ICI Support Process";
            trigger OnValidate()
            var
                ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
            begin
                ICISupportTicketProcMgt.InitTicketProcess(Rec);
            end;
        }
        field(18; "Process Stage"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Process Stage', Comment = 'de-DE=Prozessstufe';
            TableRelation = "ICI Support Process Stage".Stage where("Process Code" = field("Process Code"),
                                                                        "Ticket State Filter" = FIELD("Ticket State"));

            trigger OnValidate()
            var
                ICISupportUser: Record "ICI Support User";
                ICISupportProcessStage: Record "ICI Support Process Stage";
                xICISupportTicket: Record "ICI Support Ticket";
                ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
                ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                ToDoMgt: Codeunit "ICI Todo Mgt.";
                ResetTimeLbl: Label 'ResetProcessingTime', COmment = 'Reset Processing Time?|de-DE=Soll die Bearbeitungszeit zurückgesetzt werden?';
            begin
                ICISupportUser.GetCurrUser(ICISupportUser);
                SetModifiedByUser(ICISupportUser."User ID");
                ICISupportProcessStage.GET("Process Code", "Process Stage");
                IF ICISupportProcessStage."Department Code" <> '' then
                    Validate("Department Code", ICISupportProcessStage."Department Code");
                TestField("Process Code");
                IF CurrFieldNo <> fieldno("Ticket State") THEN // Process Code or Process Stage modified from within page -> Update TicketState
                    Validate("Ticket State", ICISupportProcessStage."Ticket State");


                IF xICISupportTicket.GET("No.") AND (xRec."Process Stage" <> 0) AND (Rec."Ticket State" <> Rec."Ticket State"::Preparation) THEN BEGIN
                    // Kein Log + Mail im onInsert Trigger
                    ICISupportTicketLogMgt.TicketStageChanged(Rec, "Process Stage");
                    ICISupportMailMgt.SendContactProcessLineNoChanged("No.", "Process Stage");
                    ICISupportMailMgt.SendUserProcessLineNoChanged("No.", "Process Stage");
                END;

                // Aufgaben aus Kategorie 1 In ticket übernehmen, wenn vorhanden
                IF (ICISupportProcessStage."Activity Code" <> '') AND GuiAllowed() then
                    ToDoMgt.CopyActivitiesToTasks(Rec);

                // Überprüfen, ob die Zeit zurückgesetzt werden soll
                if ICISupportProcessStage."Reset processing time" and GuiAllowed() then begin
                    if Confirm(ResetTimeLbl, true) then begin
                        ICISupportTicketLogMgt.ResetTicketTime("No.");
                    end;
                end;
            end;
        }
        field(19; "Ticket State"; Enum "ICI Ticket State")
        {
            Editable = false;
            DataClassification = CustomerContent;
            Caption = 'Ticket State', Comment = 'de-DE=Ticketstatus';
            trigger OnValidate()
            var
                ICISupportProcessMgt: Codeunit "ICI Support Process Mgt.";
            begin
                IF "Ticket State" <> "Ticket State"::Preparation then
                    ICISupportProcessMgt.CheckMandatoryFields(Rec);

                IF (CurrFieldNo = 0) AND ("Process Code" <> '') THEN
                    IF ("Process Stage" = 0) then // If Modified by System -> Update Process Line, if necessary
                        ICISupportProcessMgt.InitTicketProcess(Rec)
                    else
                        ICISupportProcessMgt.UpdateProcessStageFromStateChange(Rec);
            end;
        }
        field(20; "Process Stage Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("ICI Support Process Stage".Description where("Process Code" = field("Process Code"), Stage = field("Process Stage")));
            Caption = 'Process Stage Description', Comment = 'de-DE=Prozessstufenbeschreibung';
        }
        field(21; "Category 1 Code"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 1 Code', Comment = 'de-DE=Kategoriecode';
            TableRelation = "ICI Support Ticket Category" WHERE("Parent Category" = CONST(''), Inactive = const(false));
            trigger OnValidate()
            var
                ICISupportSetup: Record "ICI Support Setup";
                ICISupportTicketCategory: Record "ICI Support Ticket Category";
                ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
                NewUserID: Code[50];
                UserChangeLbl: Label 'Detected Allocation for new User %1. Do you want to forward?', Comment = 'de-DE=Ticketzuweisung für Benutzer %1 erkannt. Wollen Sie das Ticket weiterleiten?';
            begin
                ICISupportSetup.GET();
                IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers" THEN
                    IF ICISupportTicketCategory.GET("Category 2 Code") then
                        IF ICISupportTicketCategory."Parent Category" <> '' THEN // Nur, wenn Kategorie2 eine Unterkategorie ist
                                                                                 // Category 2 löschen, wenn Category 1 geleert wurde
                            IF "Category 1 Code" = '' then
                                "Category 2 Code" := ''
                            else
                                // Oder, wenn Category 1 nicht mehr die Parent Category von Category 2 ist
                                IF "Category 2 Code" <> '' then
                                    IF ICISupportTicketCategory.GET("Category 2 Code") THEN
                                        IF ICISupportTicketCategory."Parent Category" <> "Category 1 Code" then
                                            "Category 2 Code" := '';

                NewUserID := ICIFirstAllocationMatrix.GetFirstAllocation(Rec, false);
                IF (NewUserID <> '') AND (NewUserID <> Rec."Support User ID") then
                    IF GuiAllowed then
                        IF CONFIRM(UserChangeLbl, True, NewUserID) then
                            "Support User ID" := NewUserID;

                Clear(ICISupportTicketCategory);
                IF ICISupportTicketCategory.GET("Category 1 Code") THEN
                    ICISupportTicketCategory.ApplyCategoryDefaults(Rec);
            end;
        }
        field(22; "Category 1 Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("ICI Support Ticket Category".Description Where("Code" = FIELD("Category 1 Code"), Inactive = const(false)));
            Caption = 'Category 1 Description', Comment = 'de-DE=Kategorie';

        }
        field(23; "Category 2 Code"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Category 2 Code', Comment = 'de-DE=Unterkategoriecode';
            TableRelation = "ICI Support Ticket Category" WHERE("Category Filter" = FIELD("Category 1 Code"));
            trigger OnValidate()
            var
                ICISupportSetup: Record "ICI Support Setup";
                ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
                ICISupportTicketCategory: Record "ICI Support Ticket Category";
                NewUserID: Code[50];
                UserChangeLbl: Label 'Detected Allocation for new User %1. Do you want to forward?', Comment = 'de-DE=Ticketzuweisung für Benutzer %1 erkannt. Wollen Sie das Ticket weiterleiten?';
            begin
                ICISupportSetup.GET();
                IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers" THEN
                    TestField("Category 1 Code");

                NewUserID := ICIFirstAllocationMatrix.GetFirstAllocation(Rec, false);
                IF (NewUserID <> '') AND (NewUserID <> Rec."Support User ID") then
                    IF GuiAllowed then
                        IF CONFIRM(UserChangeLbl, True, NewUserID) then
                            "Support User ID" := NewUserID;

                ICISupportTicketCategory.GET("Category 2 Code");
                ICISupportTicketCategory.ApplyCategoryDefaults(Rec);
            end;
        }
        field(24; "Category 2 Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("ICI Support Ticket Category".Description Where("Code" = FIELD("Category 2 Code")));
            Caption = 'Category 2 Description', Comment = 'de-DE=Unterkategorie';
        }

        field(25; "Service Item No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = if ("Customer No." = const('')) "Service Item" else
            "Service Item" where("Customer No." = field("Customer No."));
            Caption = 'Service Item No.', Comment = 'de-DE=Serviceartikelnr.';
            ValidateTableRelation = false;
            trigger OnValidate()
            var
                ServiceItem: Record "Service Item";
            begin
                if (Rec."Service Item No." <> '') and (Rec."Service Item No." <> xRec."Service Item No.") then begin
                    // It is allowed to add ServiceItems of other Customers even if it is against TableRelation
                    ServiceItem.GET("Service Item No."); // Throw error if not found to counter ValidateTableRelation=false. Only Existing ServiceItems can be added (Customer does not matter)
                    // "Item No." := ServiceItem."Item No.";
                    // "Serial No." := ServiceItem."Serial No.";
                    Rec.Validate("Item No.", ServiceItem."Item No.");
                    Rec.Validate("Serial No.", ServiceItem."Serial No.");

                    IF ("Customer No." = '') AND (ServiceItem."Customer No." <> '') then // Dont overwrite Customer of Ticket
                        Validate("Customer No.", ServiceItem."Customer No.");
                end;
            end;

        }
        field(26; "Service Item Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("Service Item".Description Where("No." = FIELD("Service Item No.")));
            Caption = 'Service Item Description', Comment = 'de-DE=Serviceartikelbeschreibung';
        }

        field(27; "Item No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Item;
            Caption = 'Item No.', Comment = 'de-DE=Artikelnr.';
        }
        field(28; "Item Description"; Text[100])
        {
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("Item".Description Where("No." = FIELD("Item No.")));
            Caption = 'Item Description', Comment = 'de-DE=Artikelbeschreibung';
        }
        field(29; "Serial No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Serial No.', Comment = 'de-DE=Seriennr.';
        }
        field(30; "Customer No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Customer;
            Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';

            trigger OnValidate()
            var
                ICIServiceIntegrationMgt: Codeunit "ICI Service Integration Mgt.";
            begin
                FillMissingContactOrCustomerInformation();
                IF "Service Item No." <> '' THEN
                    ICIServiceIntegrationMgt.UpdateServiceItem("Service Item No.", "Customer No.")
            end;
        }
        field(31; "Customer Name"; Text[100])
        {
            ObsoleteState = Pending;
            ObsoleteReason = 'Moved to Cust. Name';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Lookup("Customer".Name Where("No." = FIELD("Customer No.")));
            Caption = 'Customer Name', Comment = 'de-DE=Debitorenname';
        }
        field(34; "Reference No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Reference No.', Comment = 'de-DE=Referenznr.';
        }
        field(35; "Department Code"; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Department Code', Comment = 'de-DE=Abteilungscode';
            TableRelation = "ICI Department";

            trigger OnValidate()
            var
                ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
                NewUserID: Code[50];
                UserChangeLbl: Label 'Detected Allocation for new User %1. Do you want to forward?', Comment = 'de-DE=Ticketzuweisung für Benutzer %1 erkannt. Wollen Sie das Ticket weiterleiten?';
            begin
                NewUserID := ICIFirstAllocationMatrix.GetFirstAllocation(Rec, false);
                IF (NewUserID <> '') AND (NewUserID <> Rec."Support User ID") then
                    IF GuiAllowed then
                        IF CONFIRM(UserChangeLbl, True, NewUserID) then
                            "Support User ID" := NewUserID;
            end;
        }
        field(36; "Department Description"; Text[100])
        {
            FieldClass = FlowField;
            Caption = 'Department Description', Comment = 'de-DE=Abteilungsbeschreibung';
            CalcFormula = lookup("ICI Department".Description where(Code = field("Department Code")));
            Editable = false;
        }
        field(37; "Last Activity Date"; DateTime)
        {
            DataClassification = CustomerContent;
            Caption = 'Last Activity Date', Comment = 'de-DE=Letzte Aktivität am';
            Editable = false;
        }
        field(38; "Last Activity By Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Last Activity By Type', Comment = 'de-DE=Letzte Aktivität von Art';
            OptionMembers = User,Contact;
            OptionCaption = 'User,Contact', Comment = 'de-DE=Benutzer,Kontakt';
            Editable = false;
        }
        field(39; "Last Activity By Code"; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Last Activity By Code', Comment = 'de-DE=Letzte Aktivität von';
            Editable = false;
        }
        field(41; "Reference No. Style Expr"; Text[30])
        {
            Caption = 'Reference No. Style Expr', Comment = 'Referenznr. Style Expr';
            DataClassification = CustomerContent;
        }

        field(42; "Created On"; DateTime)
        {
            DataClassification = CustomerContent;
            Caption = 'Created On', Comment = 'de-DE=Erstellt am';
            Editable = false;
        }

        field(43; "First Opened On"; DateTime)
        {
            FieldClass = FlowField;
            CalcFormula = min("ICI Support Ticket Log"."Creation Timestamp" where("Support Ticket No." = FIELD("No."), Type = Const(State), "State Subtype" = const(Open)));
            Caption = 'First Opened On', Comment = 'de-DE=Eröffnet am';
            Editable = false;
        }
        field(44; "First Closed On"; DateTime)
        {
            FieldClass = FlowField;
            CalcFormula = min("ICI Support Ticket Log"."Creation Timestamp" where("Support Ticket No." = field("No."), Type = Const(State), "State Subtype" = const(Closed)));
            Caption = 'First Closed On', Comment = 'de-DE=Geschlossen am';
            Editable = false;
        }
        // field(45; "Registered Time"; Decimal)
        // {
        //     FieldClass = FlowField;
        //     CalcFormula = sum("ICI Support Ticket Log"."Time Registration Qty. Hours" where("Support Ticket No." = Field("No."), Type = const("Time Registration")));
        //     Caption = 'Registered Time', Comment = 'de-DE=Erfasste Zeit';
        //     Editable = false;
        // }
        field(46; "Sales Quote No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Quote No.', Comment = 'de-DE=VK-Angebot';
            // TableRelation = "Sales Header"."No." where("Document Type" = const(Quote));
            TableRelation = if ("Customer No." = const('')) "Sales Header"."No." where("Document Type" = const(Quote)) else
            "Sales Header"."No." where("Document Type" = const(Quote), "Sell-to Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                SalesHeader: Record "Sales Header";
                Contact: Record Contact;
            begin
                IF NOT SalesHeader.GET(SalesHeader."Document Type"::Quote, "Sales Quote No.") THEN EXIT;

                IF ("Customer No." = '') AND (SalesHeader."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesHeader."Sell-to Customer No.");

                IF SalesHeader."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesHeader."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesHeader."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesHeader."Sell-to Contact No.");
                        end;
            end;
        }
        field(47; "Sales Order No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Order No.', Comment = 'de-DE=VK-Auftrag';
            // TableRelation = "Sales Header"."No." where("Document Type" = const(Order));
            TableRelation = if ("Customer No." = const('')) "Sales Header"."No." where("Document Type" = const(Order)) else
            "Sales Header"."No." where("Document Type" = const(Order), "Sell-to Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                SalesHeader: Record "Sales Header";
                Contact: Record Contact;
            begin
                IF NOT SalesHeader.GET(SalesHeader."Document Type"::Order, "Sales Order No.") THEN EXIT;
                IF (SalesHeader."Quote No." <> '') AND ("Sales Quote No." = '') then
                    "Sales Quote No." := SalesHeader."Quote No.";

                IF ("Customer No." = '') AND (SalesHeader."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesHeader."Sell-to Customer No.");

                IF SalesHeader."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesHeader."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesHeader."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesHeader."Sell-to Contact No.");
                        end;
            end;
        }
        field(48; "Pst. Sales Invoice No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Sales Invoice Header', Comment = 'de-DE=Geb. VK-Rechnung';
            // TableRelation = "Sales Invoice Header";
            TableRelation = if ("Customer No." = const('')) "Sales Invoice Header" else
            "Sales Invoice Header" where("Sell-to Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                SalesInvoiceHeader: Record "Sales Invoice Header";
                SalesShipmentHeader: Record "Sales Shipment Header";
                Contact: Record Contact;
            begin
                IF NOT SalesInvoiceHeader.GET("Pst. Sales Invoice No.") THEN EXIT;
                IF (SalesInvoiceHeader."Quote No." <> '') AND ("Sales Quote No." = '') then // Angebotsnr in Ticket übernehmen
                    "Sales Quote No." := SalesInvoiceHeader."Quote No.";
                IF (SalesInvoiceHeader."Order No." <> '') AND ("Sales Order No." = '') then // Auftragsnr in Ticket übernehmen
                    "Sales Order No." := SalesInvoiceHeader."Order No.";

                SalesShipmentHeader.SetCurrentKey("Order No."); // Geb Lieferung suchen und eintragen
                SalesShipmentHeader.SetRange("Order No.", SalesInvoiceHeader."Order No."); // Orderno is Key, Source Code not..

                IF SalesShipmentHeader.FindLast() AND ("Pst. Sales Shipment No." = '') then
                    "Pst. Sales Shipment No." := SalesShipmentHeader."No.";

                IF ("Customer No." = '') AND (SalesShipmentHeader."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesShipmentHeader."Sell-to Customer No.");

                IF SalesInvoiceHeader."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesInvoiceHeader."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesInvoiceHeader."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesInvoiceHeader."Sell-to Contact No.");
                        end;
            end;
        }
        field(49; "Pst. Sales Shipment No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Sales Shipment Header', Comment = 'de-DE=Geb. VK-Lieferung';
            // TableRelation = "Sales Shipment Header";
            TableRelation = if ("Customer No." = const('')) "Sales Shipment Header" else
            "Sales Shipment Header" where("Sell-to Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                SalesShipmentHeader: Record "Sales Shipment Header";
                SalesInvoiceHeader: Record "Sales Invoice Header";
                Contact: Record Contact;
            begin
                IF NOT SalesShipmentHeader.GET("Pst. Sales Shipment No.") THEN EXIT;
                IF (SalesShipmentHeader."Quote No." <> '') AND ("Sales Quote No." = '') then // Angebotsnr in Ticket übernehmen
                    "Sales Quote No." := SalesShipmentHeader."Quote No.";
                IF (SalesShipmentHeader."Order No." <> '') AND ("Sales Order No." = '') then // Auftragsnr in Ticket übernehmen
                    "Sales Order No." := SalesShipmentHeader."Order No.";

                SalesInvoiceHeader.SetCurrentKey("Order No."); // Geb Rechnung suchen und eintragen
                SalesInvoiceHeader.SetRange("Order No.", SalesShipmentHeader."Order No."); // Orderno is Key, Source Code not..

                IF SalesInvoiceHeader.FindLast() AND ("Pst. Sales Invoice No." = '') then
                    "Pst. Sales Invoice No." := SalesInvoiceHeader."No.";

                IF ("Customer No." = '') AND (SalesShipmentHeader."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesShipmentHeader."Sell-to Customer No.");

                IF SalesShipmentHeader."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesShipmentHeader."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesShipmentHeader."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesShipmentHeader."Sell-to Contact No.");
                        end;
            end;
        }
        field(50; "Highlight for User"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Highlight for User', Comment = 'de-DE=Neue Änderungen';
        }
        field(51; "Highlight for Contact"; Boolean)
        {
            DataClassification = SystemMetadata;
            Caption = 'Highlight for Contact', Comment = 'de-DE=Neue Änderungen';
        }
        field(52; "Service Quote No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Quote No.', Comment = 'de-DE=Serviceangebot';
            // TableRelation = "Service Header"."No." where("Document Type" = const(Quote));
            TableRelation = if ("Customer No." = const('')) "Service Header"."No." where("Document Type" = const(Quote)) else
            "Service Header"."No." where("Document Type" = const(Quote), "Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                ServiceHeader: Record "Service Header";
                Contact: Record Contact;
            begin
                IF NOT ServiceHeader.GET(ServiceHeader."Document Type"::Quote, "Service Quote No.") THEN EXIT;

                IF ("Customer No." = '') AND (ServiceHeader."Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", ServiceHeader."Customer No.");

                IF ServiceHeader."Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(ServiceHeader."Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", ServiceHeader."Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", ServiceHeader."Contact No.");
                        end;
            end;
        }
        field(53; "Service Order No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Order No.', Comment = 'de-DE=Serviceauftrag';
            // TableRelation = "Service Header"."No." where("Document Type" = const(Order));
            TableRelation = if ("Customer No." = const('')) "Service Header"."No." where("Document Type" = const(Quote)) else
            "Service Header"."No." where("Document Type" = const(Order), "Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                ServiceHeader: Record "Service Header";
                Contact: Record Contact;
            begin
                IF NOT ServiceHeader.GET(ServiceHeader."Document Type"::Order, "Service Order No.") THEN EXIT;
                IF (ServiceHeader."Quote No." <> '') AND ("Service Quote No." = '') then
                    "Service Quote No." := ServiceHeader."Quote No.";

                IF ("Customer No." = '') AND (ServiceHeader."Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", ServiceHeader."Customer No.");

                IF ServiceHeader."Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(ServiceHeader."Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", ServiceHeader."Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", ServiceHeader."Contact No.");
                        end;
            end;
        }
        field(54; "Pst. Service Invoice No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Invoice Header No.', Comment = 'de-DE=Geb. Servicerechnung';
            // TableRelation = "Service Invoice Header";
            TableRelation = if ("Customer No." = const('')) "Service Invoice Header" else
            "Service Invoice Header" where("Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                ServiceInvoiceHeader: Record "Service Invoice Header";
                ServiceShipmentHeader: Record "Service Shipment Header";
                Contact: Record Contact;
            begin
                IF NOT ServiceInvoiceHeader.GET("Pst. Service Invoice No.") THEN EXIT;

                // XXX Angebotsnr nicht in Servive Lieferung
                IF (ServiceInvoiceHeader."Order No." <> '') AND ("Service Order No." = '') then // Serviceauftragsnr in Ticket übernehmen
                    "Service Order No." := ServiceInvoiceHeader."Order No.";

                ServiceShipmentHeader.SetCurrentKey("Order No."); // Geb Rechnung suchen und eintragen
                ServiceShipmentHeader.SetRange("Order No.", ServiceInvoiceHeader."Order No."); // Orderno is Key, Source Code not..

                IF ServiceShipmentHeader.FindLast() AND ("Pst. Service Shipment No." = '') then
                    "Pst. Service Shipment No." := ServiceShipmentHeader."No.";

                IF ServiceInvoiceHeader."Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(ServiceInvoiceHeader."Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", ServiceInvoiceHeader."Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", ServiceInvoiceHeader."Contact No.");
                        end;
            end;

        }
        field(55; "Pst. Service Shipment No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Shipment Header No.', Comment = 'de-DE=Geb. Servicelieferung';
            // TableRelation = "Service Shipment Header";
            TableRelation = if ("Customer No." = const('')) "Service Shipment Header" else
            "Service Shipment Header" where("Customer No." = field("Customer No."));
            ValidateTableRelation = false;

            trigger OnValidate()
            var
                ServiceShipmentHeader: Record "Service Shipment Header";
                ServiceInvoiceHeader: Record "Service Invoice Header";
                Contact: Record Contact;
            begin
                IF NOT ServiceShipmentHeader.GET("Pst. Service Shipment No.") THEN EXIT;

                // XXX Angebotsnr nicht in Servive Lieferung
                IF (ServiceShipmentHeader."Order No." <> '') AND ("Service Order No." = '') then // Serviceauftragsnr in Ticket übernehmen
                    "Service Order No." := ServiceShipmentHeader."Order No.";

                ServiceInvoiceHeader.SetCurrentKey("Order No."); // Geb Rechnung suchen und eintragen
                ServiceInvoiceHeader.SetRange("Order No.", ServiceShipmentHeader."Order No."); // Orderno is Key, Source Code not..

                IF ServiceInvoiceHeader.FindLast() AND ("Pst. Service Invoice No." = '') then
                    "Pst. Service Invoice No." := ServiceInvoiceHeader."No.";

                IF ServiceShipmentHeader."Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(ServiceShipmentHeader."Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", ServiceShipmentHeader."Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", ServiceShipmentHeader."Contact No.");
                        end;
            end;
        }
        field(56; "Service Contract No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Contract No.', Comment = 'de-DE=Servicevertrag';
            // TableRelation = "Service Contract Header"."Contract No." where("Contract Type" = const(Contract));
            TableRelation = if ("Customer No." = const('')) "Service Contract Header"."Contract No." where("Contract Type" = const(Contract)) else
            "Service Contract Header"."Contract No." where("Contract Type" = const(Contract), "Customer No." = field("Customer No."));
            ValidateTableRelation = false;
        }
        field(57; "Service Contract Quote No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Service Contract Quote No.', Comment = 'de-DE=Servicevertragsangebot';
            // TableRelation = "Service Contract Header"."Contract No." where("Contract Type" = const(Quote));
            TableRelation = if ("Customer No." = const('')) "Service Contract Header"."Contract No." where("Contract Type" = const(Quote)) else
            "Service Contract Header"."Contract No." where("Contract Type" = const(Quote), "Customer No." = field("Customer No."));
            ValidateTableRelation = false;
        }
        field(59; "Accounting Type"; Code[20])
        {
            Caption = 'Accounting', Comment = 'de-DE=Abrechnung';
            TableRelation = "ICI Support Time Acc. Type";
            DataClassification = CustomerContent;
        }
        field(60; "Warranty End Date (Labor)"; Date)
        {
            FieldClass = FlowField;
            CalcFormula = lookup("service Item"."Warranty Ending Date (Labor)" where("No." = field("Service Item No.")));
            Caption = 'Warranty Ending Date (Labor)', Comment = 'Garantieende (Arbeit)';
            Editable = false;
        }
        field(61; "Warranty End Date (Parts)"; Date)
        {
            FieldClass = FlowField;
            CalcFormula = lookup("service Item"."Warranty Ending Date (Parts)" where("No." = field("Service Item No.")));
            Caption = 'Warranty Ending Date (Parts)', Comment = 'Garantieende (Teile)';
            Editable = false;
        }
        field(62; "Portal GUID"; GUID)
        {
            Caption = 'Portal GUID', Comment = 'de-DE=Kundenportal GUID';
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(63; "Ticket Board Order"; Integer)
        {
            Caption = 'Ticketboard Order', Comment = 'de-DE=Priorität';
            DataClassification = CustomerContent;
        }
        field(64; "Escalation Code"; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Escalation Code', Comment = 'de-DE=Eskalationscode';
            TableRelation = "ICI Escalation".Code;
        }
        field(65; "Source Mailrobot Log"; Integer)
        {
            Caption = 'Source Mailrobot Log', Comment = 'de-DE=Mailrobot Log';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Log";
        }
        field(66; "Contact Phone No."; Text[80])
        {
            Editable = false;
            Caption = 'Contact Phone No.', Comment = 'de-DE=Telefonnr.';
            DataClassification = CustomerContent;
            TableRelation = Contact."Phone No." where("No." = field("Current Contact No."));
        }
        field(67; "Contact Mobile Phone No."; Text[80])
        {
            Editable = false;
            Caption = 'Contact Mobile Phone No.', Comment = 'de-DE=Mobiltelefonnr.';
            DataClassification = CustomerContent;
            TableRelation = Contact."Mobile Phone No." where("No." = field("Current Contact No."));
        }
        field(68; "Mailrobot Source Mailbox Code"; Code[30])
        {
            Caption = 'Mailrobot Source Mailbox Code', Comment = 'de-DE=Mailrobot Postfach Code';
            DataClassification = CustomerContent;
            TableRelation = "ICI Mailrobot Mailbox";
        }
        field(69; "Process Description"; Text[50])
        {
            Caption = 'Process Description', Comment = 'de-DE=Prozessbeschreibung';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support Process".Description where(Code = field("Process Code")));
        }
        field(70; "Opportunity No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Opportunity No.', Comment = 'de-DE=VK-Chance';
            TableRelation = Opportunity;
            trigger OnValidate()
            var
                // Contact: Record Contact;
                Opportunity: Record Opportunity;
            begin
                IF NOT Opportunity.GET("Opportunity No.") THEN EXIT;
                IF Opportunity."Sales Document No." <> '' then
                    Case Opportunity."Sales Document Type" of
                        Opportunity."Sales Document Type"::Order:
                            "Sales Order No." := Opportunity."Sales Document No.";
                        Opportunity."Sales Document Type"::"Posted Invoice":
                            "Pst. Sales Invoice No." := Opportunity."Sales Document No.";
                        Opportunity."Sales Document Type"::Quote:
                            "Sales Quote No." := Opportunity."Sales Document No.";
                    End;

                IF ("Company Contact No." = '') AND (Opportunity."Contact Company No." <> '') then // Unternehmen übernehmen
                    Validate("Company Contact No.", Opportunity."Contact Company No.");

                IF ("Current Contact No." = '') AND (Opportunity."Contact No." <> '') then // Kontakt übernehmen
                    Validate("Current Contact No.", Opportunity."Contact No.");
            end;
        }
        field(71; "Progress Ratio"; Integer)
        {
            Caption = 'Progress', Comment = 'de-DE=Fortschritt';
            ExtendedDatatype = Ratio;
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = lookup("ICI Support Process Stage"."Progress Ratio" where("Process Code" = field("Process Code"), Stage = field("Process Stage")));

        }
        field(72; Online; Boolean)
        {
            Caption = 'Online', Comment = 'de-DE=Online';
            InitValue = true;

            trigger OnValidate()
            var
                Contact: Record Contact;
                ICISupportSetup: Record "ICI Support Setup";
            begin
                ICISupportSetup.GET();

                if Rec.Online then begin
                    IF ICISupportSetup."Person Mandatory" then // Personenkontakt nehmen, wenn pflicht 
                        Contact.GET(Rec."Current Contact No.")
                    else
                        IF Rec."Current Contact No." <> '' THEN // Personenkontakt nehmen, wenn angegeben, sonst unternehmen
                            Contact.GET(Rec."Current Contact No.")
                        else
                            Contact.GET(Rec."Company Contact No.");

                    Contact.TestField("E-Mail");
                    Contact.TestField("Salutation Code");
                end;
            end;

        }
        field(73; "Ship-to Code"; Code[10])
        {
            Caption = 'Ship-to Code', Comment = 'de-DE=Lief. an Code';
            DataClassification = CustomerContent;
            TableRelation = "Ship-to Address".Code WHERE("Customer No." = FIELD("Customer No."));

            trigger OnValidate()
            var
                ShipToAddr: Record "Ship-to Address";
            begin
                TESTFIELD("Customer No.");
                IF ShipToAddr.GET("Customer No.", "Ship-to Code") THEN BEGIN

                    "Ship-to Name" := ShipToAddr.Name;
                    "Ship-to Name 2" := ShipToAddr."Name 2";
                    "Ship-to Address" := ShipToAddr.Address;
                    "Ship-to Address 2" := ShipToAddr."Address 2";
                    "Ship-to City" := ShipToAddr.City;
                    "Ship-to Post Code" := ShipToAddr."Post Code";
                    "Ship-to County" := ShipToAddr.County;
                    VALIDATE("Ship-to Country/Region Code", ShipToAddr."Country/Region Code");
                    "Ship-to Contact" := ShipToAddr.Contact;
                    "Shipment Method Code" := ShipToAddr."Shipment Method Code";
                    "Location Code" := ShipToAddr."Location Code";
                    VALIDATE("Shipping Agent Code", ShipToAddr."Shipping Agent Code");
                    VALIDATE("Shipping Agent Service Code", ShipToAddr."Shipping Agent Service Code");
                END;
            end;
        }
        field(74; "Ship-to Name"; Text[100])
        {
            Caption = 'Ship-to Name', Comment = 'de-DE=Lief. an Name';
            DataClassification = CustomerContent;
        }
        field(75; "Ship-to Name 2"; Text[50])
        {
            Caption = 'Ship-to Name 2', Comment = 'de-DE=Lief. an Name 2';
            DataClassification = CustomerContent;
        }
        field(76; "Ship-to Address"; Text[100])
        {
            Caption = 'Ship-to Address', Comment = 'de-DE=Lief. an Adresse';
            DataClassification = CustomerContent;
        }
        field(77; "Ship-to Address 2"; Text[50])
        {
            Caption = 'Ship-to Address 2', Comment = 'de-DE=Lief. an Adresse 2';
            DataClassification = CustomerContent;
        }
        field(78; "Ship-to City"; Text[30])
        {
            Caption = 'Ship-to City', Comment = 'de-DE=Lief. an Ort';
            DataClassification = CustomerContent;
            TableRelation = IF ("Ship-to Country/Region Code" = CONST()) "Post Code".City
            ELSE
            IF ("Ship-to Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Ship-to Country/Region Code"));
            ValidateTableRelation = false;

            trigger OnLookup()
            var
                PostCode: Record "Post Code";
                CityTxt: Text;
                ShipToCountyTxt: Text;
            begin
                PostCode.LookupPostCode(CityTxt, "Ship-to Post Code", ShipToCountyTxt, "Ship-to Country/Region Code");
                "Ship-to City" := Copystr(CityTxt, 1, MaxStrLen("Ship-to City"));
                "Ship-to County" := Copystr(ShipToCountyTxt, 1, MaxStrLen("Ship-to County"));
            end;

            trigger OnValidate()
            var
                PostCode: Record "Post Code";
            begin
                PostCode.ValidateCity(
                  "Ship-to City", "Ship-to Post Code", "Ship-to County", "Ship-to Country/Region Code", (CurrFieldNo <> 0) AND GUIALLOWED);
            end;
        }
        field(79; "Ship-to Contact"; Text[100])
        {
            Caption = 'Ship-to Contact', Comment = 'de-DE=Lief. an Kontakt';
            DataClassification = CustomerContent;
        }
        field(80; "Ship-to Post Code"; Code[20])
        {
            Caption = 'Ship-to Post Code', Comment = 'de-DE=Lief. an PLZ-Code';
            DataClassification = CustomerContent;
            TableRelation = IF ("Ship-to Country/Region Code" = CONST()) "Post Code"
            ELSE
            IF ("Ship-to Country/Region Code" = FILTER(<> '')) "Post Code" WHERE("Country/Region Code" = FIELD("Ship-to Country/Region Code"));
            ValidateTableRelation = false;

            trigger OnLookup()
            var
                PostCode: Record "Post Code";
                CityTxt: Text;
                ShipToCountyTxt: Text;
            begin
                PostCode.LookupPostCode(CityTxt, "Ship-to Post Code", ShipToCountyTxt, "Ship-to Country/Region Code");
                "Ship-to City" := Copystr(CityTxt, 1, MaxStrLen("Ship-to City"));
                "Ship-to County" := Copystr(ShipToCountyTxt, 1, MaxStrLen("Ship-to County"));
            end;

            trigger OnValidate()
            var
                PostCode: Record "Post Code";
            begin
                PostCode.ValidatePostCode(
                  "Ship-to City", "Ship-to Post Code", "Ship-to County", "Ship-to Country/Region Code", (CurrFieldNo <> 0) AND GUIALLOWED);
            end;
        }
        field(81; "Ship-to County"; Text[30])
        {
            Caption = 'Ship-to County', Comment = 'de-DE=Lief. an Bundesregion';
            DataClassification = CustomerContent;
        }
        field(82; "Ship-to Country/Region Code"; Code[10])
        {
            Caption = 'Ship-to Country/Region Code', Comment = 'de-DE=Lief. an Länder-/Regionscode';
            DataClassification = CustomerContent;
            TableRelation = "Country/Region";
        }
        field(83; "Ship-to Phone No."; Text[30])
        {
            Caption = 'Phone No.', Comment = 'de-DE=Telefonnr.';
            DataClassification = CustomerContent;
            ExtendedDatatype = PhoneNo;
        }
        field(84; "Shipment Method Code"; Code[10])
        {
            Caption = 'Shipment Method Code', Comment = 'de-DE=Lieferbedingungscode';
            DataClassification = CustomerContent;
            TableRelation = "Shipment Method";
        }
        field(85; "Location Code"; Code[10])
        {
            Caption = 'Location Code', Comment = 'de-DE=Lagerortcode';
            DataClassification = CustomerContent;
            TableRelation = Location WHERE("Use As In-Transit" = CONST(false));
        }
        field(86; "Shipping Agent Code"; Code[10])
        {
            AccessByPermission = TableData 5790 = R;
            Caption = 'Shipping Agent Code', Comment = 'de-DE=Zustellercode';
            DataClassification = CustomerContent;
            TableRelation = "Shipping Agent";

            trigger OnValidate()
            var
                ShippingAgentServices: Record "Shipping Agent Services";
            begin
                ShippingAgentServices.RESET();
                ShippingAgentServices.SETRANGE("Shipping Agent Code", "Shipping Agent Code");
                IF ShippingAgentServices.FINDLAST() THEN
                    "Shipping Agent Service Code" := ShippingAgentServices.Code
                ELSE
                    "Shipping Agent Service Code" := '';
            end;
        }
        field(87; "Shipping Agent Service Code"; Code[10])
        {
            Caption = 'Shipping Agent Service Code', Comment = 'de-DE=Zustellertransportartencode';
            DataClassification = CustomerContent;
            TableRelation = "Shipping Agent Services".Code WHERE("Shipping Agent Code" = FIELD("Shipping Agent Code"));
        }
        field(88; "Curr. Contact Name"; Text[100])
        {
            DataClassification = CustomerContent;
            Caption = 'Company Contact Name', Comment = 'de-DE=Person';
            trigger OnValidate()
            var
                SupportMAilSetup: Record "ICI Support Mail Setup";
            begin
                if ("Curr. Contact Name" <> '') then
                    if "Curr. Contact Salutation Code" = '' then begin
                        if SupportMAilSetup.get() then
                            Validate("Curr. Contact Salutation code", SupportMAilSetup."Salutation Code D");
                    end;
            end;
        }
        field(89; "Comp. Contact Name"; Text[100])
        {
            Editable = false;
            DataClassification = CustomerContent;
            Caption = 'Company Contact Name', Comment = 'de-DE=Unternehmen';
        }
        field(90; "Cust. Name"; Text[100])
        {
            Editable = false;
            DataClassification = CustomerContent;
            Caption = 'Customer Name', Comment = 'de-DE=Debitorenname';
        }
        field(91; "Sales Quote Archive No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Quote Archive No.', Comment = 'de-DE=VK-Angebotsarchiv';
            TableRelation = "Sales Header Archive"."No." where("Document Type" = const(Quote));
            trigger OnValidate()
            var
                SalesHeaderArchive: Record "Sales Header Archive";
                Contact: Record Contact;
            begin
                IF NOT SalesHeaderArchive.GET(SalesHeaderArchive."Document Type"::Quote, "Sales Quote No.") THEN EXIT;

                IF ("Customer No." = '') AND (SalesHeaderArchive."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesHeaderArchive."Sell-to Customer No.");

                IF SalesHeaderArchive."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesHeaderArchive."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesHeaderArchive."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesHeaderArchive."Sell-to Contact No.");
                        end;
            end;
        }
        field(92; "Sales Order Archive No."; Code[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Order Archive No.', Comment = 'de-DE=VK-Auftragsarchiv';
            TableRelation = "Sales Header Archive"."No." where("Document Type" = const(Order));
            trigger OnValidate()
            var
                SalesHeaderArchive: Record "Sales Header Archive";
                Contact: Record Contact;
            begin
                IF NOT SalesHeaderArchive.GET(SalesHeaderArchive."Document Type"::Order, "Sales Order No.") THEN EXIT;
                IF (SalesHeaderArchive."Sales Quote No." <> '') AND ("Sales Quote No." = '') then
                    "Sales Quote No." := SalesHeaderArchive."Sales Quote No.";

                IF ("Customer No." = '') AND (SalesHeaderArchive."Sell-to Customer No." <> '') then // Debitor übernehmen
                    Validate("Customer No.", SalesHeaderArchive."Sell-to Customer No.");

                IF SalesHeaderArchive."Sell-to Contact No." <> '' then // VK an kontaktnr übernehmen
                    IF Contact.GET(SalesHeaderArchive."Sell-to Contact No.") THEN
                        case Contact.Type of
                            Contact.Type::Person:
                                IF xRec."Current Contact No." = '' THEN // xRec weil durch debitor validate evtl primärkontakt drin stehen könnte
                                    Validate("Current Contact No.", SalesHeaderArchive."Sell-to Contact No.");
                            Contact.Type::Company:
                                IF "Company Contact No." = '' THEN
                                    Validate("Company Contact No.", SalesHeaderArchive."Sell-to Contact No.");
                        end;
            end;
        }
        field(93; "Contact E-Mail"; Text[80])
        {
            DataClassification = CustomerContent;
            Caption = 'Email', Comment = 'de-DE=E-Mail';
            ExtendedDatatype = EMail;

            trigger OnValidate()
            begin
                //ValidateEmail(); aus Customer
            end;
        }

        field(201; "Curr. Contact Salutation Code"; Code[10])
        {
            TableRelation = Salutation;
            DataClassification = CustomerContent;
            Caption = 'Contact Salutation', Comment = 'de-DE=Pers. Anrede';
        }
        field(202; "Curr. Contact Phone No."; Text[30])
        {
            DataClassification = CustomerContent;
            ExtendedDatatype = PhoneNo;
            Caption = 'Phone No.', Comment = 'de-DE=Pers. Telefonnr.';
        }


    }

    keys
    {
        key(PK; "No.")
        {
            Clustered = true;
        }
        key(FlowFilters; "Ticket State", "Support User ID", "Department Code") { }
        key(DocumentIntegration; "Sales Quote No.", "Sales Order No.", "Pst. Sales Invoice No.", "Pst. Sales Shipment No.", "Service Contract No.", "Service Contract Quote No.", "Service Order No.", "Service Quote No.", "Pst. Service Invoice No.", "Pst. Service Shipment No.") { }
        key(Ticketboard; "Ticket Board Order") { }
        key(ContactFactbox; "Current Contact No.", "Company Contact No.", "Ticket State", "Customer No.") { }
        key(Online; Online) { }
        key(CompanyTickets; "Company Contact No.", "Ticket State") { }
    }
    fieldgroups
    {
        fieldgroup(DropDown; "No.", Description, "Process Stage Description", "Comp. Contact Name")
        {
        }
        fieldgroup(Brick; "No.", Description, "Process Stage Description", "Comp. Contact Name")
        {
        }
    }

    trigger OnInsert()
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportUser: Record "ICI Support User";
        ICIFirstAllocationMatrix: Record "ICI First Allocation Matrix";
        NoSeriesManagement: Codeunit NoSeriesManagement;
        ICISupportLicenseMgt: Codeunit "ICI Support License Mgt.";
        InsertAsUserID: Code[50];
        NotLicencedErr: Label 'Licence Error. Please check the Licence.', Comment = 'de-DE=Bitte überprüfen Sie die Lizenz';
    begin
        ICISupportSetup.Get();
        ICISupportSetup.TestField("Support Ticket Nos.");

        IF not ICISupportLicenseMgt.CheckLicense() then
            ICISupportLicenseMgt.SyncLicenseStatus();
        IF not ICISupportLicenseMgt.CheckLicense() then
            error(NotLicencedErr);

        if "No." = '' then
            NoSeriesManagement.InitSeries(ICISupportSetup."Support Ticket Nos.", '', 0D, "No.", ICISupportSetup."Support Ticket Nos.");

        IF ("Department Code" = '') AND (ICISupportSetup."Default Department Code" <> '') then
            "Department Code" := ICISupportSetup."Default Department Code";

        InsertAsUserID := ICIFirstAllocationMatrix.GetFirstAllocation(Rec, true);
        IF InsertAsUserID <> '' THEN
            VALIDATE("Support User ID", InsertAsUserID);
        IF ("Support User ID" = '') OR GuiAllowed THEN begin // Nichts über erstzuweisung gefunden, oder Ticket per Hand erstellt, dann aktueller benutzer
            ICISupportUser.GetCurrUser(ICISupportUser); // No User = No Ticket Insert 
            VALIDATE("Support User ID", ICISupportUser."User ID");
        end;

        SetModifiedByUser("Support User ID");

        Validate("Ticket State", "Ticket State"::Preparation);
        IF ICISupportSetup."Default Ticket Process" <> '' then
            Validate("Process Code", ICISupportSetup."Default Ticket Process");

        "Created On" := CurrentDateTime();
        "Portal GUID" := CreateGuid();
    end;

    procedure SetModifiedByUser(UserID: Text)
    var
    //ICISupportUser: Record "ICI Support User";
    begin
        //ICISupportUser.GetCurrUser(ICISupportUser);
        Validate("Last Activity Date", CurrentDateTime());
        Validate("Last Activity By Type", "Last Activity By Type"::User);
        Validate("Last Activity By Code", UserID);
        Validate("Highlight for User", false);
        Validate("Highlight for Contact", true);
    end;

    procedure SetModifiedByContact(ContactNo: Code[50])
    var
        Contact: Record "Contact";
    begin
        Contact.GET(ContactNo);
        Validate("Last Activity Date", CurrentDateTime);
        Validate("Last Activity By Type", "Last Activity By Type"::Contact);
        Validate("Last Activity By Code", ContactNo);
        Validate("Highlight for User", true);
        Validate("Highlight for Contact", false);
    end;

    procedure FillMissingContactOrCustomerInformation()
    var
        Customer: Record Customer;
        CompanyContact: Record Contact;
        PersonContact: Record Contact;
        NameLookupContact: Record Contact;
        ContactBusinessRelation: Record "Contact Business Relation";
        KnownCustomer: Boolean;
        KnownCompany: Boolean;
        KnownPerson: Boolean;
    begin
        KnownCustomer := (CurrFieldNo = FieldNo("Customer No.")) OR (("Customer No." <> '') AND ("Company Contact No." = '') AND ("Current Contact No." = ''));
        KnownCompany := (CurrFieldNo = FieldNo("Company Contact No.")) OR (("Customer No." = '') AND ("Company Contact No." <> '') AND ("Current Contact No." = ''));
        KnownPerson := (CurrFieldNo = FieldNo("Current Contact No.")) OR (("Customer No." = '') AND ("Company Contact No." = '') AND ("Current Contact No." <> ''));

        // Find Customer No by CompanyContact No
        // IF ("Customer No." = '') AND ("Company Contact No." <> '')
        IF KnownCompany THEN BEGIN

            "Customer No." := '';
            ContactBusinessRelation.SETCURRENTKEY("Link to Table", "Contact No.");
            ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);
            ContactBusinessRelation.SETRANGE("Contact No.", "Company Contact No.");
            IF ContactBusinessRelation.FINDFIRST() THEN
                "Customer No." := ContactBusinessRelation."No.";

            IF PersonContact.GET("Current Contact No.") then
                IF PersonContact."Company No." <> "Company Contact No." then
                    "Current Contact No." := '';
        END;

        // Find Company and Person Contact by Customer No
        // IF ("Customer No." <> '') AND ("Company Contact No." = '')
        IF KnownCustomer THEN BEGIN

            "Company Contact No." := '';
            "Current Contact No." := '';

            ContactBusinessRelation.SETCURRENTKEY("Link to Table", "No.");
            ContactBusinessRelation.SETRANGE("No.", "Customer No.");
            ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);
            IF ContactBusinessRelation.FINDFIRST() THEN BEGIN
                CompanyContact.GET(ContactBusinessRelation."Contact No.");
                "Company Contact No." := ContactBusinessRelation."Contact No.";

                // Get Person Contact - Primary Contact in Customer
                Customer.GET("Customer No.");
                IF Customer."Primary Contact No." <> '' then begin
                    Personcontact.setrange(type, PersonContact.type::Person);
                    PersonContact.setrange("No.", Customer."Primary Contact No.");
                    if not PersonContact.findfirst then
                        Clear(PersonContact);
                end ELSE BEGIN
                    // Get Person Contact - Exactly one Person in Company
                    PersonContact.SetCurrentKey("Company No.");
                    PersonContact.SetRange("Company No.", CompanyContact."No.");
                    PersonContact.setrange(Type, PersonContact.type::Person);
                    IF PersonContact.Count() = 1 then
                        PersonContact.FindFirst();
                END;

                IF PersonContact."No." <> '' THEN
                    Validate("Current Contact No.", PersonContact."No.");

            end else begin

                // Customer empty or not connected to CompanyContact 
                "Company Contact No." := '';
                "Current Contact No." := '';
            end;
        END;

        // Find Company and Customer Contact by Current Contact No
        // IF ("Customer No." <> '') AND ("Company Contact No." = '')
        IF KnownPerson THEN BEGIN

            // Find Company No
            IF PersonContact.GET("Current Contact No.") then
                IF "Company Contact No." <> PersonContact."Company No." then
                    "Company Contact No." := PersonContact."Company No.";

            // Find Customer by Company Contact No.
            IF "Company Contact No." <> '' THEN begin
                ContactBusinessRelation.SETCURRENTKEY("Link to Table", "Contact No.");
                ContactBusinessRelation.SETRANGE("Link to Table", ContactBusinessRelation."Link to Table"::Customer);
                ContactBusinessRelation.SETRANGE("Contact No.", "Company Contact No.");

                "Customer No." := '';
                IF ContactBusinessRelation.FINDFIRST() THEN
                    "Customer No." := ContactBusinessRelation."No.";
            end;

        END;

        // Get the Names
        if "Current Contact No." <> '' THEN
            IF NameLookupContact.GET("Current Contact No.") then begin
                "Curr. Contact Name" := NameLookupContact.Name;
                "Contact E-Mail" := NameLookupContact."E-Mail";
                "Curr. Contact Salutation code" := NameLookupContact."Salutation Code";
                "Curr. Contact Phone No." := NameLookupContact."Phone No.";
            end;

        if "Company Contact No." <> '' THEN
            IF NameLookupContact.GET("Company Contact No.") then begin
                "Comp. Contact Name" := NameLookupContact.Name;
                if "Contact E-Mail" = '' then
                    "Contact E-Mail" := NameLookupContact."E-Mail";
            end;

        if "Customer No." <> '' then
            IF Customer.GET("Customer No.") THEN begin
                "Cust. Name" := Customer.Name;
                if "Contact E-Mail" = '' then
                    "Contact E-Mail" := Customer."E-Mail";
            end;

        //Change-Trigger
        if ((Rec."Customer No." <> xRec."Customer No.") and (xRec."Customer No." <> '')) or
         ((Rec."Company Contact No." <> xRec."Company Contact No.") and (xRec."Company Contact No." <> ''))
        then
            OnAfterChangeCompanyOrCustomer(Rec);
    end;

    procedure GetTicketProcessingTime(): Duration
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
    begin
        CalcFields("First Opened On", "First Closed On");
        IF "First Opened On" = 0DT THEN
            EXIT;

        ICISupportTicketLog.SetRange("Support Ticket No.", "No.");
        ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::TimeReset);
        if not ICISupportTicketLog.FindLast() then begin
            IF ("First Closed On" <> 0DT) AND ("Ticket State" = "Ticket State"::Closed) then
                EXIT("First Closed On" - "First Opened On");

            EXIT(CurrentDateTime - "First Opened On");
        end else begin
            if (ICISupportTicketLog."Creation Timestamp" <> 0DT) AND ("Ticket State" = "Ticket State"::Closed) then
                EXIT("First Closed On" - ICISupportTicketLog."Creation Timestamp");

            exit(CurrentDateTime - ICISupportTicketLog."Creation Timestamp");
        end;
    end;

    procedure DocumentNoFilled(): Boolean
    begin
        EXIT(("Sales Order No." <> '') OR ("Sales Quote No." <> '') OR ("Pst. Sales Invoice No." <> '') OR ("Pst. Sales Shipment No." <> ''));
    end;

    procedure ForwardTicketTicketBoard(NewUserID: Code[50])
    var
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        IF ("Ticket State" = "Ticket State"::Closed) THEN
            FIELDERROR("Ticket State");

        "Last Activity Date" := CURRENTDATETIME();
        "Ticket State" := "Ticket State"::Open;
        "Support User ID" := NewUserID;
        ICISupportTicket.SETFILTER("Support User ID", NewUserID);

        ICISupportTicket.SETFILTER("Ticket State", '<>%1', ICISupportTicket."Ticket State"::Closed);
        ICISupportTicket.SETCURRENTKEY("Ticket Board Order");
        IF ICISupportTicket.FINDLAST() THEN
            "Ticket Board Order" := ICISupportTicket."Ticket Board Order" + 1;
        MODIFY(TRUE);
    end;

    procedure CreateOrUpdateServiceItem()
    var
        ServiceItem: Record "Service Item";
        ChangeServiceItemCustomerLbl: Label 'Do you want to change the Customer of Service Item %1 - %2 from %3 to %4 - %5', Comment = '%1=ServiceItemNo;%2=ServiceItemDescription;%3=OldCustomerNo;%5=NewCustomerNo;%6=NewCustomerName|de-DE=Wollen Sie den Debitoren im Serviceartikel %1 - %2 von %3 auf %4 - %5 ändern';
    begin
        IF "Service Item No." <> '' THEN // Update CustomerNo in Service Item
            IF ServiceItem.GET("Service Item No.") THEN
                IF "Customer No." <> ServiceItem."Customer No." then
                    IF GuiAllowed then
                        IF Confirm(ChangeServiceItemCustomerLbl, true, ServiceItem."No.", ServiceItem.Description, ServiceItem."Customer No.", Rec."Customer No.", Rec."Cust. Name") then begin
                            ServiceItem.Validate("Customer No.", Rec."Customer No.");
                            ServiceItem.Modify();
                        end;

        // Or Create Service Item if necessary
        // IF "Service Item No." = '' THEN begin
        //     IF "Serial No." = '' THEN
        //         IF GuiAllowed then
        //             IF NOT CONFIRM(CreateServiceItemWithEmptySerialNoLbl) THEN
        //                 ERROR(ActionAbortedErr);
        //     ServiceItemNo := ICIServiceIntegrationMgt.CreateServiceItemForTicket("Serial No.", "No.");
        //     Validate("Service Item No.", ServiceItemNo);
        //     MODIFY(true);
        // end;
    end;

    procedure isOverdue(): Boolean
    var
        EscalationMgt: Codeunit "ICI Escalation Mgt.";
    begin
        EXIT(EscalationMgt.isTicketOverdue(Rec));
    end;

    PROCEDURE FindShipToAddress();
    VAR
        ICIServiceIntegrationMgt: Codeunit "ICI Service Integration Mgt.";
        FoundShiptoAddressCode: Code[10];
    BEGIN
        FoundShiptoAddressCode := ICIServiceIntegrationMgt.FindShipToAddress("Customer No.", "Ship-to Name", "Ship-to Name 2", "Ship-to Address", "Ship-to Address 2", "Ship-to Post Code", "Ship-to City", "Ship-to County", "Ship-to Country/Region Code");
        IF FoundShiptoAddressCode <> '' THEN
            "Ship-to Code" := FoundShiptoAddressCode;
    END;

    procedure CreateShipToAddress()
    VAR
        ShiptoAddress: Record "Ship-to Address";
        ShipToCode: Code[10];
    BEGIN
        IF "Customer No." = '' THEN
            EXIT;

        TESTFIELD("Ship-to Name");
        TESTFIELD("Ship-to Code", '');

        FindShipToAddress();
        IF "Ship-to Code" <> '' THEN
            EXIT;

        ShiptoAddress.SETRANGE("Customer No.", "Customer No.");
        IF ShiptoAddress.FINDLAST() THEN
            ShipToCode := INCSTR(ShipToCode);

        IF ShipToCode = '' THEN BEGIN
            ShipToCode := COPYSTR(DELCHR("No.", '<>=', '-') + '??', 1, 10);
            ShiptoAddress.SETFILTER(Code, ShipToCode);
            IF ShiptoAddress.FINDLAST() THEN
                ShipToCode := INCSTR(ShiptoAddress.Code)
            ELSE
                ShipToCode := COPYSTR(DELCHR("No.", '<>=', '-') + '00', 1, 10);
        END;

        CLEAR(ShiptoAddress);

        ShiptoAddress."Customer No." := Rec."Customer No.";
        ShiptoAddress.Code := ShipToCode;
        ShiptoAddress.VALIDATE(Name, "Ship-to Name");
        ShiptoAddress.VALIDATE("Name 2", "Ship-to Name 2");
        ShiptoAddress.VALIDATE(Address, "Ship-to Address");
        ShiptoAddress.VALIDATE("Address 2", "Ship-to Address 2");
        ShiptoAddress.VALIDATE("Post Code", "Ship-to Post Code");
        ShiptoAddress.VALIDATE(City, "Ship-to City");
        ShiptoAddress.VALIDATE("Country/Region Code", "Ship-to Country/Region Code");
        ShiptoAddress.VALIDATE(County, "Ship-to County");
        ShiptoAddress.VALIDATE("Phone No.", "Ship-to Phone No.");
        ShiptoAddress.VALIDATE("Location Code", Rec."Location Code");
        ShiptoAddress.VALIDATE("Shipment Method Code", Rec."Shipment Method Code");
        ShiptoAddress.VALIDATE("Shipping Agent Code", Rec."Shipping Agent Code");
        ShiptoAddress.VALIDATE("Shipping Agent Service Code", Rec."Shipping Agent Service Code");
        ShiptoAddress.INSERT();

        "Ship-to Code" := ShiptoAddress.Code;
    end;

    procedure DrillDownContact(ContactNo: Code[20])
    var
        Contact: Record Contact;
        ContactList: Page "Contact List";
    begin
        Contact.SetRange("No.", ContactNo);
        ContactList.SetTableView(Contact);
        ContactList.Run();
    end;


    procedure DrillDownCustomer(CustomerNo: Code[20])
    var
        Customer: Record Customer;
        CustomerList: Page "Customer List";
    begin
        Customer.SetRange("No.", CustomerNo);
        CustomerList.SetTableView(Customer);
        CustomerList.Run();
    end;

    procedure CreatePersonContact()
    var
        Contact: Record Contact;
    begin
        rec.TestField("Company Contact No.");
        rec.TestField("Current Contact No.", '');
        rec.TestField("curr. Contact name");

        contact.init;
        contact.validate(type, "contact type"::Person);
        contact.insert(true);
        contact.validate(name, rec."Curr. Contact Name");
        contact.validate("Company No.", rec."Company Contact No.");
        if rec."Curr. Contact Salutation Code" <> '' then
            contact.validate("Salutation Code", rec."Curr. Contact Salutation Code");
        if rec."Curr. Contact Phone No." <> '' then
            contact.validate("Phone No.", rec."Curr. Contact Phone No.");
        if rec."Contact E-Mail" <> '' then
            contact.validatE("E-Mail", rec."Contact E-Mail");
        contact.Modify(true);

        rec.validate("Current Contact No.", contact."No.");
    end;

    [IntegrationEvent(false, false)]
    procedure OnProcessNewTicketJson(var ICISupportTIcket: Record "ICI Support Ticket"; JSON: JsonObject);
    begin
    end;

    [IntegrationEvent(false, false)]
    procedure OnAfterChangeCompanyOrCustomer(var ICISupportTIcket: Record "ICI Support Ticket");
    begin
    end;
}
