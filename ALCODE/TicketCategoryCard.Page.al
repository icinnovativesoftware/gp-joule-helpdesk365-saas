page 56375 "ICI Ticket Category Card"
{

    Caption = 'Ticket Category Card', Comment = 'de-DE=Ticketkategoriekarte';
    PageType = Card;
    SourceTable = "ICI Support Ticket Category";
    UsageCategory = None;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("Code"; Rec."Code")
                {
                    ToolTip = 'Specifies the value of the Code field.', Comment = 'de-DE=Code';
                    ApplicationArea = All;
                }
                field("Parent Category"; Rec."Parent Category")
                {
                    ToolTip = 'Specifies the value of the Parent Category field.', Comment = 'de-DE=Gehört zu Kategorie';
                    ApplicationArea = All;
                    Visible = ShowParent;
                }
                field(Layer; Rec.Layer)
                {
                    ToolTip = 'Specifies the value of the Layer field.', Comment = 'de-DE=Ebene';
                    ApplicationArea = All;
                    Visible = ShowLayer;
                }
                field(Description; Rec.Description)
                {
                    ToolTip = 'Specifies the value of the Description field.', Comment = 'de-DE=Beschreibung';
                    ApplicationArea = All;
                }
                field(Inactive; Rec.Inactive)
                {
                    ToolTip = 'Specifies the value of the Inactive field.', Comment = 'de-DE=Inaktiv';
                    ApplicationArea = All;
                }


            }
            group(Defaults)
            {
                Caption = 'Defaults', Comment = 'de-DE=Vorbelegungen';
                field("Default Accounting Type"; Rec."Default Accounting Type")
                {
                    ToolTip = 'Specifies the value of the Accounting field.', Comment = 'de-DE=Vorbel. Abrechnung';
                    ApplicationArea = All;
                }
                field("Default Escalation Code"; Rec."Default Escalation Code")
                {
                    ToolTip = 'Specifies the value of the Default Escalation Code field.', Comment = 'de-DE=Vorbel. Eskalationscode';
                    ApplicationArea = All;
                }
                field("Default Online"; Rec."Default Online")
                {
                    ToolTip = 'Specifies the value of the Default Online field.', Comment = 'de-DE=Vorbel. Online';
                    ApplicationArea = All;
                }
                field("Default Process Code"; Rec."Default Process Code")
                {
                    ToolTip = 'Specifies the value of the Process Code field.', Comment = 'de-DE=Vorbel. Prozess Code';
                    ApplicationArea = All;
                }
            }
            group(Additional)
            {
                Caption = 'Additional', Comment = 'de-DE=Zusätzlich';
                field(Online; Rec.Online)
                {
                    ToolTip = 'Specifies the value of the Online field.', Comment = 'de-DE=Online';
                    ApplicationArea = All;
                    Visible = ShowPortal;
                }

                field("Ticketboard - Color"; Rec."Ticketboard - Color")
                {
                    ToolTip = 'Specifies the value of the Ticketboard - Base Color field.', Comment = 'de-DE=Ticketboard Farbe';
                    ApplicationArea = All;
                    Visible = ShowTicketboard;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Translation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Translations', Comment = 'de-DE=Übersetzungen';
                Image = Transactions;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                RunObject = page "ICI Tick. Cat. Translations";
                RunPageLink = "Ticket Category Code" = field("Code");

                ToolTip = 'Shows the Translations of this Category', Comment = 'de-DE=Zeigt die Übersetzungen dieser Kategorie an';
            }
        }
    }
    trigger OnOpenPage()
    begin
        ICISupportSetup.GET();
        ShowLayer := ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Two-Layers";
        ShowParent := ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers";
        ShowTicketboard := ICISupportSetup."Ticketboard active";
        ShowPortal := ICISupportSetup."Portal Integration";
    end;

    var

        ICISupportSetup: Record "ICI Support Setup";
        ShowLayer: Boolean;
        ShowParent: Boolean;
        ShowTicketboard: Boolean;
        ShowPortal: Boolean;
}
