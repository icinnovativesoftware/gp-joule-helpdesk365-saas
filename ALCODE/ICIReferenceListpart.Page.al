page 56316 "ICI Reference Listpart"
{
    Caption = 'ICI Reference Listpart', Comment = 'de-DE=Referenznr. Liste';
    PageType = ListPart;
    SourceTable = "ICI Reference No. Buffer";
    SourceTableTemporary = true;
    Editable = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Code', Comment = 'de-DE=Code';

                    trigger OnDrillDown()
                    begin
                        OpenCorrespondingPage();
                    end;
                }
                field("Search Result"; Rec."Search Result")
                {
                    ApplicationArea = All;
                    ToolTip = 'Search Result', Comment = 'de-DE=Suchergebnis';
                    Visible = IsDocumentSearch OR IsSerialNoSearch;
                }
                field("Document Search Type"; Rec."Document Search Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field("Searched Field"; Rec."Searched Field")
                {
                    ApplicationArea = All;
                    ToolTip = 'Searched Field', Comment = 'de-DE=Gibt an, anhand welches Feldes das Ergebnis gefunden wurde';
                    Visible = IsDocumentSearch OR IsSerialNoSearch;
                }
                field(Address; Rec.Address)
                {
                    ApplicationArea = All;
                    ToolTip = 'Address Field', Comment = 'de-DE=Adresse des gefundenen Datensatzes';
                    Visible = IsAddressSearch;
                }
                field("Address 2"; Rec."Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Address 2 Field', Comment = 'de-DE=Adresse 2 des gefundenen Datensatzes';
                    Visible = IsAddressSearch;
                }
                field("Post Code"; Rec."Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Post Code Field', Comment = 'de-DE=PLZ des gefundenen Datensatzes';
                    Visible = IsAddressSearch;
                }
                field(City; Rec.City)
                {
                    ApplicationArea = All;
                    ToolTip = 'City Field', Comment = 'de-DE=Stadt des gefundenen Datensatzes';
                    Visible = IsAddressSearch;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description/Name', Comment = 'de-DE=Beschreibung/Name';
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Customer Name', Comment = 'de-DE=Debitorenname';
                }
                field("Additional Text"; Rec."Additional Text")
                {
                    ApplicationArea = All;
                    ToolTip = 'Additional Text', Comment = 'de-DE=Hinweis';
                    Style = Unfavorable;
                    StyleExpr = Rec."Additional Text" <> '';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Open)
            {
                Caption = 'Open', Comment = 'de-DE=Öffnen';
                ToolTip = 'Open', Comment = 'de-DE=Öffnen';
                ApplicationArea = All;
                Image = SendMail;

                trigger OnAction()
                begin
                    OpenCorrespondingPage();
                end;
            }
        }
    }

    procedure FillPageFromReferenceNo(ReferenceNo: Code[50]; TicketNo: Code[20]; PageType: Option Documents,ServiceItem)
    begin
        IF ICISupportTicket.GET(TicketNo) THEN;

        FillFromReferenceNo(ReferenceNo, PageType);
        CurrPage.Update(false);
    end;

    local procedure OpenCorrespondingPage()
    var
        ICIReferenceNoBuffer: Record "ICI Reference No. Buffer";
        SalesHeader: Record "Sales Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        ServiceItem: Record "Service Item";
        ServiceContractHeader: Record "Service Contract Header";
        ServiceHeader: Record "Service Header";
        SalesHeaderArchive: Record "Sales Header Archive";
        Contact: Record Contact;
        Customer: Record Customer;
        ShipToAddress: Record "Ship-To Address";

        ServiceDocumentType: Enum "Service Document Type";
        SalesDocumentType: Enum "Sales Document Type";
    begin

        CurrPage.GetRecord(ICIReferenceNoBuffer);
        case ICIReferenceNoBuffer."Document Search Type" of
            ICIReferenceNoBuffer."Document Search Type"::Order:
                begin
                    SalesHeader.GET(SalesDocumentType::Order, ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Sales Order", SalesHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::Quote:
                begin
                    SalesHeader.GET(SalesDocumentType::Quote, ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Sales Quote", SalesHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Pst.Invoice":
                begin
                    SalesInvoiceHeader.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Posted Sales Invoice", SalesInvoiceHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Pst.Shipment":
                begin
                    SalesShipmentHeader.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Posted Sales Shipment", SalesShipmentHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Service Item":
                begin
                    ServiceItem.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Service Item Card", ServiceItem);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Service Contract":
                begin
                    ServiceContractHeader.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Service Contract", ServiceContractHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Service Order":
                begin
                    ServiceHeader.GET(ServiceDocumentType::Order, ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Service Order", ServiceHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Service Contract Quote":
                begin
                    ServiceContractHeader.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Service Contract Quote", ServiceContractHeader);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Order Archive":
                begin
                    SalesHeaderArchive.SetRange("Document Type", SalesDocumentType::Order);
                    SalesHeaderArchive.SetRange("No.", ICIReferenceNoBuffer.Code);
                    SalesHeaderArchive.FindLast();
                    //SalesHeaderArchive.GET(SalesDocumentType::Order, ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Sales Order Archive", SalesHeaderArchive);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Quote Archive":
                begin
                    SalesHeaderArchive.SetRange("Document Type", SalesDocumentType::Quote);
                    SalesHeaderArchive.SetRange("No.", ICIReferenceNoBuffer.Code);
                    SalesHeaderArchive.FindLast();
                    //SalesHeaderArchive.GET(SalesDocumentType::Quote, ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Sales Quote Archive", SalesHeaderArchive);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Contact Address":
                begin
                    Contact.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Contact Card", Contact);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Customer Address":
                begin
                    Customer.GET(ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Customer Card", Customer);
                end;
            ICIReferenceNoBuffer."Document Search Type"::"Ship-To Address":
                begin
                    ShipToAddress.GET(ICIReferenceNoBuffer."Customer No.", ICIReferenceNoBuffer.Code);
                    Page.Run(PAGE::"Ship-to Address", ShipToAddress);
                end;
        end;
    end;


    procedure FillFromReferenceNo(ReferenceNo: Code[50]; PageType: Option Documents,ServiceItem)
    var
        ICISupportUser: Record "ICI Support User";
        SalesHeader: Record "Sales Header";
        SalesHeaderArchive: Record "Sales Header Archive";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        ServiceItem: Record "Service Item";
        ServiceHeader: Record "Service Header";
        ServiceContractHeader: Record "Service Contract Header";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        ServiceShipmentHeader: Record "Service Shipment Header";
    begin
        Rec.DeleteAll();
        Clear(EntryNo);

        IF ReferenceNo = '' then
            EXIT;

        ICISupportUser.GetCurrUser(ICISupportUser);

        IF PageType = PageType::Documents THEN begin
            IF ICISupportUser."Search S.O. by No." THEN begin
                // Fill with Sales Orders
                SalesHeader.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeader.SetFilter("No.", ReferenceNo);
                IF SalesHeader.FindSet(false) then
                    repeat
                        InsertSalesOrderToBuffer(SalesHeader, ReferenceNo, SalesHeader.FIELDNO("No."));
                    until SalesHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.O. by Ext. Doc. No." THEN begin
                // Fill with Sales Orders - External Document No.
                CLEAR(SalesHeader);
                SalesHeader.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeader.SetCurrentKey("External Document No.");
                SalesHeader.SetFilter("External Document No.", ReferenceNo);
                IF SalesHeader.FindSet(false) then
                    repeat
                        InsertSalesOrderToBuffer(SalesHeader, ReferenceNo, SalesHeader.FIELDNO("External Document No."));
                    until SalesHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.O. by Quote No." THEN begin
                // Fill with Sales Orders - Quote No.
                CLEAR(SalesHeader);
                SalesHeader.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeader.SetCurrentKey("Quote No.");
                SalesHeader.SetFilter("Quote No.", ReferenceNo);
                IF SalesHeader.FindSet(false) then
                    repeat
                        InsertSalesOrderToBuffer(SalesHeader, ReferenceNo, SalesHeader.FIELDNO("Quote No."));
                    until SalesHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.Q. by No." THEN begin
                // Fill with Sales Quotes
                CLEAR(SalesHeader);
                SalesHeader.SetRange("Document Type", "Sales Document Type"::Quote);
                SalesHeader.SetFilter("No.", ReferenceNo);
                IF SalesHeader.FindSet(false) then
                    repeat
                        InsertSalesQuoteToBuffer(SalesHeader, ReferenceNo, SalesHeader.FIELDNO("No."));
                    until SalesHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.Q. by Ext. Doc. No." THEN begin
                // Fill with Sales Quotes - External Document No.
                CLEAR(SalesHeader);
                SalesHeader.SetRange("Document Type", "Sales Document Type"::Quote);
                SalesHeader.SetCurrentKey("External Document No.");
                SalesHeader.SetFilter("External Document No.", ReferenceNo);
                IF SalesHeader.FindSet(false) then
                    repeat
                        InsertSalesQuoteToBuffer(SalesHeader, ReferenceNo, SalesHeader.FIELDNO("External Document No."));
                    until SalesHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.I. by No." THEN begin
                // Fill with Sales Invoices
                SalesInvoiceHeader.SetFilter("No.", ReferenceNo);
                IF SalesInvoiceHeader.FindSet(false) then
                    repeat
                        InsertSalesInvoiceToBuffer(SalesInvoiceHeader, ReferenceNo, SalesInvoiceHeader.FIELDNO("No."));
                    until SalesInvoiceHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.I. by Ext. Doc. No." THEN begin
                // Fill with Sales Invoices - External Document No.
                CLEAR(SalesInvoiceHeader);
                SalesInvoiceHeader.SetCurrentKey("External Document No.");
                SalesInvoiceHeader.SetFilter("External Document No.", ReferenceNo);
                IF SalesInvoiceHeader.FindSet(false) then
                    repeat
                        InsertSalesInvoiceToBuffer(SalesInvoiceHeader, ReferenceNo, SalesInvoiceHeader.FIELDNO("External Document No."));
                    until SalesInvoiceHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.I. by Order No." THEN begin
                // Fill with Sales Invoices - Order No.
                CLEAR(SalesInvoiceHeader);
                SalesInvoiceHeader.SetCurrentKey("Order No.");
                SalesInvoiceHeader.SetFilter("Order No.", ReferenceNo);
                IF SalesInvoiceHeader.FindSet(false) then
                    repeat
                        InsertSalesInvoiceToBuffer(SalesInvoiceHeader, ReferenceNo, SalesInvoiceHeader.FIELDNO("Order No."));
                    until SalesInvoiceHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.I. by Quote No." THEN begin
                // Fill with Sales Invoices - Quote No.
                CLEAR(SalesInvoiceHeader);
                SalesInvoiceHeader.SetCurrentKey("Quote No.");
                SalesInvoiceHeader.SetFilter("Quote No.", ReferenceNo);
                IF SalesInvoiceHeader.FindSet(false) then
                    repeat
                        InsertSalesInvoiceToBuffer(SalesInvoiceHeader, ReferenceNo, SalesInvoiceHeader.FIELDNO("Quote No."));
                    until SalesInvoiceHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.S. by No." THEN begin
                // Fill with Sales Shipments
                SalesShipmentHeader.SetFilter("No.", ReferenceNo);
                IF SalesShipmentHeader.FindSet(false) then
                    repeat
                        InsertSalesShipmentToBuffer(SalesShipmentHeader, ReferenceNo, SalesShipmentHeader.FIELDNO("No."));
                    until SalesShipmentHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.S. by Ext. Doc. No." THEN begin
                // Fill with Sales Shipments - External Document No.
                CLEAR(SalesShipmentHeader);
                SalesShipmentHeader.SetCurrentKey("External Document No.");
                SalesShipmentHeader.SetFilter("External Document No.", ReferenceNo);
                IF SalesShipmentHeader.FindSet(false) then
                    repeat
                        InsertSalesShipmentToBuffer(SalesShipmentHeader, ReferenceNo, SalesShipmentHeader.FIELDNO("External Document No."));
                    until SalesShipmentHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.S. by Order No." THEN begin
                // Fill with Sales Shipments - Order No.
                CLEAR(SalesShipmentHeader);
                SalesShipmentHeader.SetCurrentKey("Order No.");
                SalesShipmentHeader.SetFilter("Order No.", ReferenceNo, ReferenceNo);
                IF SalesShipmentHeader.FindSet(false) then
                    repeat
                        InsertSalesShipmentToBuffer(SalesShipmentHeader, ReferenceNo, SalesShipmentHeader.FIELDNO("Order No."));
                    until SalesShipmentHeader.Next() = 0;
            end;
            IF ICISupportUser."Search S.S. by Quote No." THEN begin
                // Fill with Sales Shipments - Quote No.
                CLEAR(SalesShipmentHeader);
                SalesShipmentHeader.SetCurrentKey("Quote No.");
                SalesShipmentHeader.SetFilter("Quote No.", ReferenceNo);
                IF SalesShipmentHeader.FindSet(false) then
                    repeat
                        InsertSalesShipmentToBuffer(SalesShipmentHeader, ReferenceNo, SalesShipmentHeader.FIELDNO("Quote No."));
                    until SalesShipmentHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. Q. by No." then begin
                // Fill With Service Quotes
                CLEAR(ServiceHeader);
                ServiceHeader.SetRange("No.", ReferenceNo);
                ServiceHeader.SetRange("Document Type", "Service Document Type"::Quote);
                IF ServiceHeader.FindSet(false) then
                    repeat
                        InsertServiceQuoteToBuffer(ServiceHeader, ReferenceNo, ServiceHeader.FIELDNO("No."));
                    until ServiceHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. O. by No." then begin
                // Fill With Service Orders
                CLEAR(ServiceHeader);
                ServiceHeader.SetRange("No.", ReferenceNo);
                ServiceHeader.SetRange("Document Type", "Service Document Type"::"Order");
                IF ServiceHeader.FindSet(false) then
                    repeat
                        InsertServiceOrderToBuffer(ServiceHeader, ReferenceNo, ServiceHeader.FIELDNO("No."));
                    until ServiceHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. O. by Quote No." then begin
                // Fill With Service Orders - Quote No.
                CLEAR(ServiceHeader);
                ServiceHeader.SetRange("Document Type", "Service Document Type"::"Order");
                ServiceHeader.SetRange("Quote No.", ReferenceNo);
                IF ServiceHeader.FindSet(false) then
                    repeat
                        InsertServiceOrderToBuffer(ServiceHeader, ReferenceNo, ServiceHeader.FIELDNO("Quote No."));
                    until ServiceHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. S. by No." then begin
                // Fill With Service Shipments
                ServiceShipmentHeader.SetFilter("No.", ReferenceNo);
                IF ServiceShipmentHeader.FindSet(false) then
                    repeat
                        InsertServiceShipmentToBuffer(ServiceShipmentHeader, ReferenceNo, ServiceShipmentHeader.FIELDNO("No."));
                    until ServiceShipmentHeader.Next() = 0;

            end;

            IF ICISupportUser."Search Serv. I. by No." then begin
                // Fill With Service Invoice
                ServiceInvoiceHeader.SetFilter("No.", ReferenceNo);
                IF ServiceInvoiceHeader.FindSet(false) then
                    repeat
                        InsertServiceInvoiceToBuffer(ServiceInvoiceHeader, ReferenceNo, ServiceInvoiceHeader.FIELDNO("No."));
                    until ServiceInvoiceHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. C. by No." then begin
                // Fill With Service Contracts
                CLEAR(ServiceContractHeader);
                ServiceContractHeader.SetRange("Contract No.", ReferenceNo);
                ServiceContractHeader.SetRange("Contract Type", ServiceContractHeader."Contract Type"::Contract);
                IF ServiceContractHeader.FindSet(false) then
                    repeat
                        InsertServiceContractToBuffer(ServiceContractHeader, ReferenceNo, ServiceContractHeader.FIELDNO("Contract No."));
                    until ServiceContractHeader.Next() = 0;
            end;

            IF ICISupportUser."Search Serv. C. Q. by No." then begin
                // Fill With Service Contract Quotes
                CLEAR(ServiceContractHeader);
                ServiceContractHeader.SetRange("Contract No.", ReferenceNo);
                ServiceContractHeader.SetRange("Contract Type", ServiceContractHeader."Contract Type"::Quote);
                IF ServiceContractHeader.FindSet(false) then
                    repeat
                        InsertServiceContractQuoteToBuffer(ServiceContractHeader, ReferenceNo, ServiceContractHeader.FIELDNO("Contract No."));
                    until ServiceContractHeader.Next() = 0;
            end;

            IF ICISupportUser."Search S.O.A. by No." THEN begin
                // Fill with Sales Order Archive
                SalesHeaderArchive.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeaderArchive.SetFilter("No.", ReferenceNo);
                IF SalesHeaderArchive.FindSet(false) then
                    repeat
                        InsertSalesOrderArchiveToBuffer(SalesHeaderArchive, ReferenceNo, SalesHeaderArchive.FIELDNO("No."));
                    until SalesHeaderArchive.Next() = 0;
            end;
            IF ICISupportUser."Search S.O.A. by Ext. Doc. No." THEN begin
                // Fill with Sales Order Archive - External Document No.
                CLEAR(SalesHeaderArchive);
                SalesHeaderArchive.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeaderArchive.SetCurrentKey("External Document No.");
                SalesHeaderArchive.SetFilter("External Document No.", ReferenceNo);
                IF SalesHeaderArchive.FindSet(false) then
                    repeat
                        InsertSalesOrderArchiveToBuffer(SalesHeaderArchive, ReferenceNo, SalesHeaderArchive.FIELDNO("External Document No."));
                    until SalesHeaderArchive.Next() = 0;
            end;
            IF ICISupportUser."Search S.O.A. by Quote No." THEN begin
                // Fill with Sales Order Archive - Quote No.
                CLEAR(SalesHeader);
                SalesHeaderArchive.SetRange("Document Type", "Sales Document Type"::Order);
                SalesHeaderArchive.SetCurrentKey("Sales Quote No.");
                SalesHeaderArchive.SetFilter("Sales Quote No.", ReferenceNo);
                IF SalesHeaderArchive.FindSet(false) then
                    repeat
                        InsertSalesOrderArchiveToBuffer(SalesHeaderArchive, ReferenceNo, SalesHeaderArchive.FIELDNO("Sales Quote No."));
                    until SalesHeaderArchive.Next() = 0;
            end;
            IF ICISupportUser."Search S.Q.A. by No." THEN begin
                // Fill with Sales Quote Archive
                CLEAR(SalesHeaderArchive);
                SalesHeaderArchive.SetRange("Document Type", "Sales Document Type"::Quote);
                SalesHeaderArchive.SetFilter("No.", ReferenceNo);
                IF SalesHeaderArchive.FindSet(false) then
                    repeat
                        InsertSalesQuoteArchiveToBuffer(SalesHeaderArchive, ReferenceNo, SalesHeaderArchive.FIELDNO("No."));
                    until SalesHeaderArchive.Next() = 0;
            end;
            IF ICISupportUser."Search S.Q.A. by Ext. Doc. No." THEN begin
                // Fill with Sales Quote Archive - External Document No.
                CLEAR(SalesHeaderArchive);
                SalesHeaderArchive.SetRange("Document Type", "Sales Document Type"::Quote);
                SalesHeaderArchive.SetCurrentKey("External Document No.");
                SalesHeaderArchive.SetFilter("External Document No.", ReferenceNo);
                IF SalesHeaderArchive.FindSet(false) then
                    repeat
                        InsertSalesQuoteArchiveToBuffer(SalesHeaderArchive, ReferenceNo, SalesHeaderArchive.FIELDNO("External Document No."));
                    until SalesHeaderArchive.Next() = 0;
            end;
            OnAfterInsertDocumentsToBuffer(ICISupportUser, ReferenceNo);
        end;

        if PageType = PageType::ServiceItem THEN begin
            IF ICISupportUser."Search Se. It. by No." THEN begin
                CLEAR(ServiceItem);
                ServiceItem.SetFilter("No.", ReferenceNo);
                IF ServiceItem.FindSet(false) then
                    repeat
                        InsertServiceItemToBuffer(ServiceItem, ReferenceNo, ServiceItem.FIELDNO("No."));
                    until ServiceItem.Next() = 0;
            end;

            IF ICISupportUser."Search Se. It. by Serial No." THEN begin
                CLEAR(ServiceItem);
                ServiceItem.SetCurrentKey("item No.", "Serial No.");
                ServiceItem.SetFilter("Serial No.", ReferenceNo);
                IF ServiceItem.FindSet(false) then
                    repeat
                        InsertServiceItemToBuffer(ServiceItem, ReferenceNo, ServiceItem.FIELDNO("Serial No."));
                    until ServiceItem.Next() = 0;
            end;

            IF ICISupportUser."Search Se. It. by Search Desc." THEN begin
                CLEAR(ServiceItem);
                ServiceItem.SetCurrentKey("Search Description");
                ServiceItem.SetFilter("Search Description", ReferenceNo);
                IF ServiceItem.FindSet(false) then
                    repeat
                        InsertServiceItemToBuffer(ServiceItem, ReferenceNo, ServiceItem.FIELDNO("Search Description"));
                    until ServiceItem.Next() = 0;
            end;

            IF ICISupportUser."Search Se. It. by No. 2" THEN begin
                CLEAR(ServiceItem);
                ServiceItem.SetCurrentKey("ICI HD No. 2");
                ServiceItem.SetFilter("ICI HD No. 2", ReferenceNo);
                IF ServiceItem.FindSet(false) then
                    repeat
                        InsertServiceItemToBuffer(ServiceItem, ReferenceNo, ServiceItem.FIELDNO("ICI HD No. 2"));
                    until ServiceItem.Next() = 0;
            end;

            IF ICISupportUser."Search Se. It. by Item No." THEN begin
                CLEAR(ServiceItem);
                ServiceItem.SetCurrentKey("item No.", "Serial No.");
                ServiceItem.SetFilter("Item No.", ReferenceNo);
                IF ServiceItem.FindSet(false) then
                    repeat
                        InsertServiceItemToBuffer(ServiceItem, ReferenceNo, ServiceItem.FIELDNO("Item No."));
                    until ServiceItem.Next() = 0;
            end;

        end;

    end;

    procedure FillPageFromAddressSearch(SearchAddress: Text[100]; SearchAddress2: Text[50]; SearchPostCode: Code[20]; SearchName: Text[50]; SearchName2: Text[50]; SearchCity: Text[30]; TicketNo: Code[20])

    begin
        IF ICISupportTicket.GET(TicketNo) THEN;

        FillFromAddressSearch(SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity);
        CurrPage.Update(false);
    end;

    procedure FillFromAddressSearch(SearchAddress: Text[100]; SearchAddress2: Text[50]; SearchPostCode: Code[20]; SearchName: Text[50]; SearchName2: Text[50]; SearchCity: Text[30])
    var
        ICISupportUser: Record "ICI Support User";
        Customer: Record Customer;
        Contact: Record Contact;
        ServiceItem: Record "Service Item";
        ShipToAddress: Record "Ship-to Address";
    begin
        Rec.DeleteAll();
        Clear(EntryNo);

        IF (SearchAddress = '') AND (SearchAddress2 = '') AND (SearchPostCode = '') AND (SearchName = '') AND (SearchName2 = '') AND (SearchCity = '') then
            EXIT;

        ICISupportUser.GetCurrUser(ICISupportUser);

        IF ICISupportUser."Search Cust. by Addr." THEN begin
            if SearchAddress <> '' then
                Customer.SetFilter("Address", SearchAddress);
            if SearchAddress2 <> '' then
                Customer.SetFilter("Address 2", SearchAddress2);
            if SearchPostCode <> '' then
                Customer.SetFilter("Post Code", SearchPostCode);
            if SearchName <> '' then
                Customer.SetFilter("Name", SearchName);
            if SearchName2 <> '' then
                Customer.SetFilter("Name 2", SearchName2);
            if SearchCity <> '' then
                Customer.SetFilter("City", SearchCity);

            IF Customer.FindSet(false) then
                repeat
                    InsertCustomerAddressToBuffer(Customer);
                until Customer.Next() = 0;
        end;

        IF ICISupportUser."Search Contacts by Addr." THEN begin
            if SearchAddress <> '' then
                Contact.SetFilter("Address", SearchAddress);
            if SearchAddress2 <> '' then
                Contact.SetFilter("Address 2", SearchAddress2);
            if SearchPostCode <> '' then
                Contact.SetFilter("Post Code", SearchPostCode);
            if SearchName <> '' then
                Contact.SetFilter("Name", SearchName);
            if SearchName2 <> '' then
                Contact.SetFilter("Name 2", SearchName2);
            if SearchCity <> '' then
                Contact.SetFilter("City", SearchCity);

            IF Contact.FindSet(false) then
                repeat
                    InsertContactAddressToBuffer(Contact);
                until Contact.Next() = 0;
        end;

        IF ICISupportUser."Search SI by Ship-To Addr." THEN begin
            Clear(ServiceItem);
            ServiceItem.SetCurrentKey("ICI Ship-to Address", "ICI Ship-to Address 2", "ICI Ship-To City", "ICI Ship-To Post Code");
            if SearchAddress <> '' then
                ServiceItem.SetFilter("ICI Ship-to Address", SearchAddress);
            if SearchAddress2 <> '' then
                ServiceItem.SetFilter("ICI Ship-to Address 2", SearchAddress2);
            if SearchPostCode <> '' then
                ServiceItem.SetFilter("ICI Ship-to Post Code", SearchPostCode);
            if SearchName <> '' then
                ServiceItem.SetFilter("ICI Ship-to Name", SearchName);
            if SearchName2 <> '' then
                ServiceItem.SetFilter("ICI Ship-to Name 2", SearchName2);
            if SearchCity <> '' then
                ServiceItem.SetFilter("ICI Ship-to City", SearchCity);

            IF ServiceItem.FindSet(false) then
                repeat
                    InsertServiceItemShipToAddressToBuffer(ServiceItem);
                until ServiceItem.Next() = 0;
        end;

        IF ICISupportUser."Search ServiceItems by Addr." THEN begin
            Clear(ServiceItem);
            ServiceItem.SetCurrentKey("ICI Address", "ICI Address 2", "ICI City", "ICI Post Code");
            if SearchAddress <> '' then
                ServiceItem.SetFilter("ICI Address", SearchAddress);
            if SearchAddress2 <> '' then
                ServiceItem.SetFilter("ICI Address 2", SearchAddress2);
            if SearchPostCode <> '' then
                ServiceItem.SetFilter("ICI Post Code", SearchPostCode);
            if SearchName <> '' then
                ServiceItem.SetFilter("ICI Name", SearchName);
            if SearchName2 <> '' then
                ServiceItem.SetFilter("ICI Name 2", SearchName2);
            if SearchCity <> '' then
                ServiceItem.SetFilter("ICI City", SearchCity);

            IF ServiceItem.FindSet(false) then
                repeat
                    InsertServiceItemAddressToBuffer(ServiceItem);
                until ServiceItem.Next() = 0;
        end;

        IF ICISupportUser."Search Ship-To Address" THEN begin
            Clear(ShipToAddress);
            if SearchAddress <> '' then
                ShipToAddress.SetFilter("Address", SearchAddress);
            if SearchAddress2 <> '' then
                ShipToAddress.SetFilter("Address 2", SearchAddress2);
            if SearchPostCode <> '' then
                ShipToAddress.SetFilter("Post Code", SearchPostCode);
            if SearchName <> '' then
                ShipToAddress.SetFilter("Name", SearchName);
            if SearchName2 <> '' then
                ShipToAddress.SetFilter("Name 2", SearchName2);
            if SearchCity <> '' then
                ShipToAddress.SetFilter("City", SearchCity);

            IF ShipToAddress.FindSet(false) then
                repeat
                    InsertShipToAddressToBuffer(ShipToAddress);
                until ShipToAddress.Next() = 0;
        end;

        OnAfterInsertAdressesToBuffer(ICISupportUser, SearchAddress, SearchAddress2, SearchPostCode, SearchName, SearchName2, SearchCity);

    end;

    local procedure InsertCustomerAddressToBuffer(var Customer: Record "Customer")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Customer Address");
        Rec.Validate(Code, Customer."No.");
        Rec.Validate(Description, Customer.Name);
        Rec.Validate("Customer No.", Customer."No.");

        Rec.Validate(Address, Customer.Address);
        Rec.Validate("Address 2", Customer."Address 2");
        Rec.Validate("Post Code", Customer."Post Code");
        Rec.Validate(City, Customer.City);

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertContactAddressToBuffer(var Contact: Record "Contact")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Contact Address");
        Rec.Validate(Code, Contact."No.");
        Rec.Validate(Description, Contact.Name);
        Rec.Validate("Customer No.", Contact.GetCustomerNo());

        Rec.Validate(Address, Contact.Address);
        Rec.Validate("Address 2", Contact."Address 2");
        Rec.Validate("Post Code", Contact."Post Code");
        Rec.Validate(City, Contact.City);

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertServiceItemAddressToBuffer(var ServiceItem: Record "Service Item")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Item");
        Rec.Validate(Code, ServiceItem."No.");
        Rec.Validate(Description, ServiceItem.Description);
        Rec.Validate("Customer No.", ServiceItem."Customer No.");

        Rec.Validate(Address, ServiceItem."ICI Address");
        Rec.Validate("Address 2", ServiceItem."ICI Address 2");
        Rec.Validate("Post Code", ServiceItem."ICI Post Code");
        Rec.Validate(City, ServiceItem."ICI City");

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertServiceItemShipToAddressToBuffer(var ServiceItem: Record "Service Item")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Item");
        Rec.Validate(Code, ServiceItem."No.");
        Rec.Validate(Description, ServiceItem.Name);
        Rec.Validate("Customer No.", ServiceItem."Customer No.");

        Rec.Validate(Address, ServiceItem."ICI Ship-To Address");
        Rec.Validate("Address 2", ServiceItem."ICI Ship-To Address 2");
        Rec.Validate("Post Code", ServiceItem."ICI Ship-To Post Code");
        Rec.Validate(City, ServiceItem."ICI Ship-To City");

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertShipToAddressToBuffer(var ShipToAddress: Record "Ship-To Address")
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Ship-To Address");
        Rec.Validate(Code, ShipToAddress.Code); // Save Customer No which is PK in Customer No field - for later retrieving
        Rec.Validate(Description, ShipToAddress.Name);
        Rec.Validate("Customer No.", ShipToAddress."Customer No.");

        Rec.Validate(Address, ShipToAddress."Address");
        Rec.Validate("Address 2", ShipToAddress."Address 2");
        Rec.Validate("Post Code", ShipToAddress."Post Code");
        Rec.Validate(City, ShipToAddress."City");

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertSalesOrderToBuffer(var SalesHeader: Record "Sales Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::Order);
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesHeader."No.");
        Rec.Validate(Description, SalesHeader."Your Reference");
        Rec.Validate("Customer No.", SalesHeader."Sell-to Customer No.");

        lRecordRef.GetTable(SalesHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertSalesQuoteToBuffer(var SalesHeader: Record "Sales Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::Quote);
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesHeader."No.");
        Rec.Validate(Description, SalesHeader."Your Reference");
        Rec.Validate("Customer No.", SalesHeader."Sell-to Customer No.");

        lRecordRef.GetTable(SalesHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertSalesOrderArchiveToBuffer(var SalesHeaderArchive: Record "Sales Header Archive"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Order Archive");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesHeaderArchive."No.");
        Rec.Validate(Description, SalesHeaderArchive."Your Reference");
        Rec.Validate("Customer No.", SalesHeaderArchive."Sell-to Customer No.");

        lRecordRef.GetTable(SalesHeaderArchive);
        lRecordRef.SetRecFilter();
        lRecordRef.FindFirst();
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));

        Rec.Modify(true);
    end;

    local procedure InsertSalesQuoteArchiveToBuffer(var SalesHeaderArchive: Record "Sales Header Archive"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Quote Archive");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesHeaderArchive."No.");
        Rec.Validate(Description, SalesHeaderArchive."Your Reference");
        Rec.Validate("Customer No.", SalesHeaderArchive."Sell-to Customer No.");

        lRecordRef.GetTable(SalesHeaderArchive);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertSalesInvoiceToBuffer(var SalesInvoiceHeader: Record "Sales Invoice Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Pst.Invoice");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesInvoiceHeader."No.");
        Rec.Validate(Description, SalesInvoiceHeader."Your Reference");
        Rec.Validate("Customer No.", SalesInvoiceHeader."Sell-to Customer No.");

        lRecordRef.GetTable(SalesInvoiceHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertSalesShipmentToBuffer(var SalesShipmentHeader: Record "Sales Shipment Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Pst.Shipment");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, SalesShipmentHeader."No.");
        Rec.Validate(Description, SalesShipmentHeader."Your Reference");
        Rec.Validate("Customer No.", SalesShipmentHeader."Sell-to Customer No.");

        lRecordRef.GetTable(SalesShipmentHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    procedure InsertServiceItemToBuffer(var ServiceItem: Record "Service Item"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Item");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceItem."No.");
        Rec.Validate(Description, ServiceItem."Description");
        Rec.Validate("Customer No.", ServiceItem."Customer No.");

        lRecordRef.GetTable(ServiceItem);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceQuoteToBuffer(var ServiceHeader: Record "Service Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Quote");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceHeader."No.");
        Rec.Validate(Description, ServiceHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceHeader."Customer No.");

        lRecordRef.GetTable(ServiceHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceOrderToBuffer(var ServiceHeader: Record "Service Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Order");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceHeader."No.");
        Rec.Validate(Description, ServiceHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceHeader."Customer No.");

        lRecordRef.GetTable(ServiceHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceInvoiceToBuffer(var ServiceInvoiceHeader: Record "Service Invoice Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Pst. Service Invoice");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceInvoiceHeader."No.");
        Rec.Validate(Description, ServiceInvoiceHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceInvoiceHeader."Customer No.");

        lRecordRef.GetTable(ServiceInvoiceHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceShipmentToBuffer(var ServiceShipmentHeader: Record "Service Shipment Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Pst.Shipment");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceShipmentHeader."No.");
        Rec.Validate(Description, ServiceShipmentHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceShipmentHeader."Customer No.");

        lRecordRef.GetTable(ServiceShipmentHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceContractToBuffer(var ServiceContractHeader: Record "Service Contract Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Contract");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceContractHeader."Contract No.");
        Rec.Validate(Description, ServiceContractHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceContractHeader."Customer No.");

        lRecordRef.GetTable(ServiceContractHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;

    local procedure InsertServiceContractQuoteToBuffer(var ServiceContractHeader: Record "Service Contract Header"; ReferenceNo: Code[50]; FieldNo: Integer)
    var
        lRecordRef: RecordRef;
    begin
        Rec.Init();
        Rec.Validate("Entry No.", GetNextEntryNo());
        Rec.Insert(true);
        Rec.Validate("Document Search Type", Rec."Document Search Type"::"Service Contract Quote");
        Rec.Validate("Reference No.", ReferenceNo);
        Rec.Validate(Code, ServiceContractHeader."Contract No.");
        Rec.Validate(Description, ServiceContractHeader."Your Reference");
        Rec.Validate("Customer No.", ServiceContractHeader."Customer No.");

        lRecordRef.GetTable(ServiceContractHeader);
        Rec.Validate("Searched Field", CopyStr(lRecordRef.Field(FieldNo).Caption, 1, 100));
        Rec.Validate("Search Result", CopyStr(FORMAT(lRecordRef.Field(FieldNo).Value), 1, 50));

        IF ICISupportTicket."Customer No." <> '' then
            IF ICISupportTicket."Customer No." <> Rec."Customer No." then
                Rec.Validate("Additional Text", CopyStr(STRSUBSTNO(CustomerMismatchErr, ICISupportTicket."No.", ICISupportTicket."Customer No.", ICISupportTicket."Cust. Name"), 1, 250));
        Rec.Modify(true);
    end;


    procedure GetNextEntryNo(): Integer
    begin
        EntryNo += 1;
        EXIT(EntryNo);
    end;

    procedure SetDocumentSearch()
    begin
        IsDocumentSearch := true;

        IsSerialNoSearch := false;
        IsAddressSearch := false;
    end;

    procedure SetAddressSearch()
    begin
        IsAddressSearch := true;

        IsSerialNoSearch := false;
        IsDocumentSearch := false;
    end;

    procedure SetSerialSearch()
    begin
        IsSerialNoSearch := true;

        IsAddressSearch := false;
        IsDocumentSearch := false;
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterInsertAdressesToBuffer(ICISupportUser: Record "ICI Support User"; SearchAddress: Text[100]; SearchAddress2: Text[50]; SearchPostCode: Code[20]; SearchName: Text[50]; SearchName2: Text[50]; SearchCity: Text[30]);
    begin
    end;

    [IntegrationEvent(true, false)]
    local procedure OnAfterInsertDocumentsToBuffer(ICISupportUser: Record "ICI Support User"; ReferenceNo: Code[50]);
    begin
    end;

    var
        ICISupportTicket: Record "ICI Support Ticket";
        IsAddressSearch: Boolean;
        IsSerialNoSearch: Boolean;
        IsDocumentSearch: Boolean;
        EntryNo: Integer;
        CustomerMismatchErr: Label 'Ticket %1 already belongs to Customer %2 - %3', Comment = '%1=TicketNo,%2=CustomerNo,%3=CustomerName|de-DE=Ticket %1 wurde bereits Debitor %2 - "%3" zugeordnet';

}
