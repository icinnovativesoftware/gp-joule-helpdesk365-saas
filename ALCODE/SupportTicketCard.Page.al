page 56278 "ICI Support Ticket Card"
{

    Caption = 'Ticket Card', Comment = 'de-DE=Ticketkarte';

    PageType = Card;
    SourceTable = "ICI Support Ticket";
    UsageCategory = None;


    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General', Comment = 'de-DE=Allgemein';
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'No.', Comment = 'de-DE=Nr.';
                    Importance = Additional;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                    ShowMandatory = true;
                    NotBlank = true;
                }
                group(Contactdata)
                {
                    Caption = 'Contactdata', Comment = 'de-DE=Kontaktdaten';
                    field("Customer No."; Rec."Customer No.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                        Editable = TicketEditable;
                    }
                    field("Customer Name"; Rec."Cust. Name")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Customer Name', Comment = 'de-DE=Debitorenname';
                        Importance = Promoted;
                        trigger OnDrillDown()
                        begin
                            Rec.DrillDownCustomer(Rec."Customer No.");
                        end;
                    }
                    field("Company Contact No."; Rec."Company Contact No.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Company Contact No.', Comment = 'de-DE=Unternehmensnr.';
                        ShowMandatory = true;
                        Editable = TicketEditable;
                        Importance = Additional;
                    }

                    field("Company Contact Name"; Rec."Comp. Contact Name")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Company Contact Name', Comment = 'de-DE=Unternehmensname';
                        Importance = Additional;
                        trigger OnDrillDown()
                        begin
                            Rec.DrillDownContact(Rec."Company Contact No.");
                        end;
                    }
                    field("Current Contact No."; Rec."Current Contact No.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Current Contact No.', Comment = 'de-DE=Aktuelle Kontaktnr.';
                        ShowMandatory = PersonMandatory;

                        trigger OnValidate()
                        var
                            Contact: Record Contact;
                            ICISupportMailSetup: Record "ICI Support Mail Setup";
                            lNotification: Notification;
                            ContactFieldEmptyLbl: Label '%1 of Contact %2 - %3 is empty', Comment = '%1 = FieldName;%2=Contact No;%3=Contact Name|de-DE=%1 von Kontakt %2 - %3 ist leer';
                            OpenCardLbl: Label 'Open Contact Card', Comment = 'de-DE=Bearbeiten';
                            FillDefaultSalutationLbl: Label 'Use Default %1', Comment = '%1=Salutation Code|de-DE=Verwende %1';
                        begin

                            IF NOT Contact.Get(Rec."Current Contact No.") then
                                EXIT;

                            IF (Contact."E-Mail" = '') THEN begin
                                lNotification.Message(StrSubstNo(ContactFieldEmptyLbl, Contact.FieldCaption("E-Mail"), Contact."No.", Contact.Name));
                                lNotification.Scope := NotificationScope::LocalScope;
                                lNotification.SetData('ContactNo', Rec."Current Contact No.");
                                lNotification.AddAction(OpenCardLbl, Codeunit::"ICI Support Action Handler", 'OpenContactCard');
                                lNotification.Send();
                            end;
                            IF (Contact."Salutation Code" = '') THEN begin
                                lNotification.Message(StrSubstNo(ContactFieldEmptyLbl, Contact.FieldCaption("Salutation Code"), Contact."No.", Contact.Name));
                                lNotification.Scope := NotificationScope::LocalScope;
                                lNotification.SetData('ContactNo', Rec."Current Contact No.");
                                lNotification.AddAction(OpenCardLbl, Codeunit::"ICI Support Action Handler", 'OpenContactCard');
                                lNotification.Send();

                                ICISupportMailSetup.GET();
                                IF ICISupportMailSetup."Salutation Code M" <> '' THEN
                                    lNotification.AddAction(StrSubstNo(FillDefaultSalutationLbl, ICISupportMailSetup."Salutation Code M"), Codeunit::"ICI Support Action Handler", 'SetContactSalutationM');

                                IF ICISupportMailSetup."Salutation Code F" <> '' THEN
                                    lNotification.AddAction(StrSubstNo(FillDefaultSalutationLbl, ICISupportMailSetup."Salutation Code F"), Codeunit::"ICI Support Action Handler", 'SetContactSalutationF');
                            end;
                        end;
                    }
                    field("Current Contact Name"; Rec."Curr. Contact Name")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Current Contact Name', Comment = 'de-DE=Aktueller Kontakt';
                        Importance = Promoted;
                        Visible = not ShowContactPersonCreation;
                        trigger OnDrillDown()
                        begin
                            Rec.DrillDownContact(Rec."Current Contact No.");
                        end;
                    }
                    field("E-Mail"; Rec."Contact E-Mail")
                    {
                        ApplicationArea = All;
                        Visible = not ShowContactPersonCreation;
                        ToolTip = 'Contact E-Mail', Comment = 'de-DE=Kontakt E-Mail';
                    }


                }
                group(NewContactPerson)
                {
                    caption = 'Contact Person', Comment = 'de-DE=Kontaktperson';
                    Visible = ShowContactPersonCreation;
                    field("Curr. Contact Salutation Code"; rec."Curr. Contact Salutation Code")
                    {
                        ApplicationArea = All;
                    }
                    field("Curr. Contact Name"; rec."Curr. Contact Name")
                    {
                        ApplicationArea = All;
                        Importance = Promoted;
                    }
                    field("Curr. Contact Phone No."; rec."Curr. Contact Phone No.")
                    {
                        ApplicationArea = All;

                    }
                    field("Contact E-Mail"; rec."Contact E-Mail")
                    {
                        ApplicationArea = All;

                    }
                }
                group(Category)
                {

                    Caption = 'Category', Comment = 'de-DE=Kategorie';
                    field("Category 1 Code"; Rec."Category 1 Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Category 1 Code', Comment = 'de-DE=Kategoriecode';
                        ShowMandatory = true;
                        Importance = Promoted;
                        NotBlank = true;

                        trigger OnValidate()
                        begin
                            Rec.CalcFields("Category 1 Description");
                            Rec.CalcFields("Category 2 Description");
                            Rec.CalcFields("Support User Name"); // Allocation Matrix might be triggered
                            CurrPage.Update();
                        end;
                    }
                    field("Category 1 Description"; Rec."Category 1 Description")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Category 1 Description', Comment = 'de-DE=Kategrorie Beschreibung';
                        Importance = Additional;
                    }
                    field("Category 2 Code"; Rec."Category 2 Code")
                    {
                        ApplicationArea = All;
                        CaptionClass = Category2CodeTxt;
                        ToolTip = 'Category 2 Code', Comment = 'de-DE=Unterkategoriecode';

                        trigger OnValidate()
                        begin
                            Rec.CalcFields("Category 2 Description");
                            Rec.CalcFields("Support User Name"); // Allocation Matrix might be triggered
                            CurrPage.Update();
                        end;
                    }
                    field("Category 2 Description"; Rec."Category 2 Description")
                    {
                        ApplicationArea = All;
                        CaptionClass = Category2DescriptionTxt;
                        ToolTip = 'Category 2 Description', Comment = 'de-DE=Unterkategorie Beschreibung';
                        Importance = Additional;
                    }
                    field("Escalation Code"; Rec."Escalation Code")
                    {
                        Importance = Additional;
                        ApplicationArea = All;
                        ToolTip = 'Escalation Code', Comment = 'de-DE=Eskalationscode';
                    }
                }
                group(Status1)
                {
                    Caption = 'Status', Comment = 'de-DE=Status';


                    field("Process Code"; Rec."Process Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Process Code', Comment = 'de-DE=Prozesscode';
                        Importance = Promoted;
                        trigger OnValidate()
                        begin
                            Rec.CalcFields("Process Description");
                        end;
                    }

                    field("Process Description"; Rec."Process Description")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Process State', Comment = 'de-DE=Prozessbeschreibung';
                        Importance = Additional;
                    }
                    field("Ticket State"; Rec."Ticket State")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Ticket State', Comment = 'de-DE=Status';

                    }

                    field("Process Stage"; Rec."Process Stage")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Process Stage', Comment = 'de-DE=Prozessstufe';
                        trigger OnValidate()
                        begin
                            Rec.CalcFields("Process Stage Description");
                            setProcessOptions();
                        end;
                    }

                    field("Process Line Description 1"; Rec."Process Stage Description")
                    {
                        ApplicationArea = All;
                        Importance = Promoted;
                        ToolTip = 'State Description', Comment = 'de-DE=Prozessstufenbeschreibung';
                    }
                }
                group(Additional)
                {
                    Caption = 'Additional', Comment = 'de-DE=Weitere Informationen';
                    field(Online; Rec.Online)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Online', Comment = 'de-DE=Zeigt an, ob das Ticket im Kundenportal angezeigt werden soll';
                        Importance = Additional;

                        trigger OnValidate()
                        begin
                            CurrPage.Update();
                        end;
                    }

                    field("Support User Code"; Rec."Support User ID")
                    {
                        Importance = Promoted;
                        ApplicationArea = All;
                        ToolTip = 'Support User ID', Comment = 'de-DE=Benutzer';
                        ShowMandatory = true;
                        NotBlank = true;

                        trigger OnValidate()
                        var
                            ICISupportUser: Record "ICI Support User";
                            ICISupportMailSetup: Record "ICI Support Mail Setup";
                            lNotification: Notification;
                            UserFieldEmptyLbl: Label '%1 of Support User %2 - %3 is empty', Comment = '%1 = FieldName;%2=Userid;%3=User Name|de-DE=%1 von Supportbenutzer %2 - %3 ist leer';
                            OpenListLbl: Label 'Open User list', Comment = 'de-DE=Bearbeiten';
                            FillDefaultSalutationLbl: Label 'Use Default %1', Comment = '%1=Salutation Code|de-DE=Verwende %1';
                        begin
                            Rec.CalcFields("Support User Name");

                            IF NOT ICISupportUser.Get(Rec."Support User ID") then
                                EXIT;

                            IF (ICISupportUser."E-Mail" = '') THEN begin
                                lNotification.Message(StrSubstNo(UserFieldEmptyLbl, ICISupportUser.FieldCaption("E-Mail"), ICISupportUser."User ID", ICISupportUser.Name));
                                lNotification.Scope := NotificationScope::LocalScope;
                                lNotification.SetData('UserID', Rec."Support User ID");
                                lNotification.AddAction(OpenListLbl, Codeunit::"ICI Support Action Handler", 'OpenUserList');
                                lNotification.Send();
                                Clear(lNotification);
                            end;
                            IF (ICISupportUser."Salutation Code" = '') THEN begin
                                lNotification.Message(StrSubstNo(UserFieldEmptyLbl, ICISupportUser.FieldCaption("Salutation Code"), ICISupportUser."User ID", ICISupportUser.Name));
                                lNotification.Scope := NotificationScope::LocalScope;
                                lNotification.SetData('UserID', Rec."Support User ID");
                                lNotification.AddAction(OpenListLbl, Codeunit::"ICI Support Action Handler", 'OpenUserList');

                                ICISupportMailSetup.GET();
                                IF ICISupportMailSetup."Salutation Code M" <> '' THEN
                                    lNotification.AddAction(StrSubstNo(FillDefaultSalutationLbl, ICISupportMailSetup."Salutation Code M"), Codeunit::"ICI Support Action Handler", 'SetUserSalutationM');

                                IF ICISupportMailSetup."Salutation Code F" <> '' THEN
                                    lNotification.AddAction(StrSubstNo(FillDefaultSalutationLbl, ICISupportMailSetup."Salutation Code F"), Codeunit::"ICI Support Action Handler", 'SetUserSalutationF');

                                // Sexist Programming. Only 3 Actions possible
                                //IF ICISupportMailSetup."Salutation Code D" <> '' THEN
                                //    lNotification.AddAction(StrSubstNo(FillDefaultSalutationLbl, ICISupportMailSetup."Salutation Code D"), Codeunit::"ICI Support Action Handler", 'SetUserSalutationD');

                                lNotification.Send();
                            end;
                            CurrPage.Update(true);
                        end;
                    }
                    field("Support User Name"; Rec."Support User Name")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Support User Name', Comment = 'de-DE=Benutzername';
                    }
                    field("Department Code"; Rec."Department Code")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Department Code', Comment = 'de-DE=Abteilungscode';

                        trigger OnValidate()
                        begin
                            Rec.CalcFields("Department Description");
                            Rec.CalcFields("Support User Name"); // Allocation Matrix might be triggered
                        end;
                    }
                    field("Department Description"; Rec."Department Description")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Department Description', Comment = 'de-DE=Abteilung';
                        Editable = false;
                    }
                    field("Accounting Type"; Rec."Accounting Type")
                    {
                        Visible = ShowTimeIntegration;
                        Importance = Additional;
                        ApplicationArea = All;
                        ToolTip = 'Accounting Type', Comment = 'de-DE=Abrechnung';
                    }

                }

            }
            group(Document)
            {
                Caption = 'Source Documents', Comment = 'de-DE=Ursprungsbelege';

                field("Reference No."; Rec."Reference No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Reference No.', Comment = 'de-DE=Referenznr.';
                    StyleExpr = Rec."Reference No. Style Expr";

                    trigger OnAssistEdit()
                    var
                        ICITicketReferenceDialog: Page "ICI Ticket Doc. Search Dialog";
                    begin
                        CurrPage.SaveRecord();
                        COMMIT();
                        ICITicketReferenceDialog.SetValues(Rec."No.", Rec."Reference No.");
                        IF ICITicketReferenceDialog.RunModal() = Action::OK THEN
                            CurrPage.Update(False);
                    end;
                }
                group(SalesDocuments)
                {
                    Caption = 'Sales', Comment = 'de-DE=Verkauf';
                    field("Quote No."; Rec."Sales Quote No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Quote', Comment = 'de-DE=Angebot';
                        ToolTip = 'Quote No.', Comment = 'de-DE=Angebot, auf das sich das Ticket bezieht';
                        Importance = Standard;


                        trigger OnAssistEdit() // Open Card
                        var
                            SalesHeader: Record "Sales Header";
                        begin
                            IF Rec."Sales Quote No." = '' then
                                EXIT;
                            IF SalesHeader.GET(SalesHeader."Document Type"::Quote, Rec."Sales Quote No.") then
                                PAGE.RUN(PAGE::"Sales Quote", SalesHeader)
                        end;
                    }
                    field("Quote Archive No."; Rec."Sales Quote Archive No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Quote Archive', Comment = 'de-DE=Angebotsarchiv';
                        ToolTip = 'Quote No.', Comment = 'de-DE=Angebotsarchiv, auf das sich das Ticket bezieht';
                        Importance = Additional;


                        trigger OnAssistEdit() // Open Card
                        var
                            SalesHeaderArchive: Record "Sales Header Archive";
                        begin
                            IF Rec."Sales Quote No." = '' then
                                EXIT;
                            IF SalesHeaderArchive.GET(SalesHeaderArchive."Document Type"::Quote, Rec."Sales Quote No.") then
                                PAGE.RUN(PAGE::"Sales Quote Archive", SalesHeaderArchive)
                        end;
                    }
                    field("Order No."; Rec."Sales Order No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Order', Comment = 'de-DE=Auftrag';
                        ToolTip = 'Order No.', Comment = 'de-DE=Auftrag, auf das sich das Ticket bezieht';
                        Importance = Promoted;

                        trigger OnAssistEdit() // Open Card
                        var
                            SalesHeader: Record "Sales Header";
                        begin
                            IF Rec."Sales Order No." = '' then
                                EXIT;
                            IF SalesHeader.GET(SalesHeader."Document Type"::Order, Rec."Sales Order No.") then
                                PAGE.RUN(PAGE::"Sales Order", SalesHeader)
                        end;
                    }
                    field("Order Archive No."; Rec."Sales Order Archive No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Order Archive', Comment = 'de-DE=Auftragsarchiv';
                        ToolTip = 'Order No.', Comment = 'de-DE=Auftragsarchiv, auf das sich das Ticket bezieht';
                        Importance = Additional;

                        trigger OnAssistEdit() // Open Card
                        var
                            SalesHeaderArchive: Record "Sales Header Archive";
                        begin
                            IF Rec."Sales Order No." = '' then
                                EXIT;
                            IF SalesHeaderArchive.GET(SalesHeaderArchive."Document Type"::Order, Rec."Sales Order No.") then
                                PAGE.RUN(PAGE::"Sales Order Archive", SalesHeaderArchive)
                        end;
                    }
                    field("Pst. Shipment No."; Rec."Pst. Sales Shipment No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Pst. Shipment', Comment = 'de-DE=Geb. Lieferung';
                        ToolTip = 'Pst. Shipment No.', Comment = 'de-DE=Lieferung, auf das sich das Ticket bezieht';

                        trigger OnAssistEdit() // Open Card
                        var
                            SalesShipmentHeader: Record "Sales Shipment Header";
                        begin
                            IF Rec."Pst. Sales Shipment No." = '' then
                                EXIT;
                            IF SalesShipmentHeader.GET(Rec."Pst. Sales Shipment No.") then
                                PAGE.RUN(PAGE::"Posted Sales Shipment", SalesShipmentHeader)
                        end;
                    }
                    field("Pst. Invoice No."; Rec."Pst. Sales Invoice No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Pst. Invoice', Comment = 'de-DE=Geb. Rechnung';
                        ToolTip = 'Pst. Invoice No.', Comment = 'de-DE=Rechnung, auf das sich das Ticket bezieht';
                        Importance = Promoted;

                        trigger OnAssistEdit() // Open Card
                        var
                            SalesInvoiceHeader: Record "Sales Invoice Header";
                        begin
                            IF Rec."Pst. Sales Invoice No." = '' then
                                EXIT;
                            IF SalesInvoiceHeader.GET(Rec."Pst. Sales Invoice No.") then
                                PAGE.RUN(PAGE::"Posted Sales Invoice", SalesInvoiceHeader)
                        end;
                    }
                }
                group(ServiceDocuments)
                {
                    Caption = 'Service', Comment = 'de-DE=Service';
                    field("Service Quote No."; Rec."Service Quote No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Quote', Comment = 'de-DE=Angebot';
                        ToolTip = 'Service Quote No..', Comment = 'de-DE=Serviceangebot, auf das sich das Ticket bezieht';

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceHeader: Record "Service Header";
                        begin
                            IF Rec."Service Quote No." = '' then
                                EXIT;
                            IF ServiceHeader.GET(ServiceHeader."Document Type"::Quote, Rec."Service Quote No.") then
                                PAGE.RUN(PAGE::"Service Quote", ServiceHeader)
                        end;
                    }
                    field("ServiceContractQuoteNo"; Rec."Service Contract Quote No.")
                    {
                        Importance = Additional;
                        ApplicationArea = All;
                        Caption = 'Contract Quote', Comment = 'de-DE=Vertragsangebot';
                        ToolTip = 'Service Contract Quote No.', Comment = 'de-DE=Servicevertragsangebot, auf das sich das Ticket bezieht';

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceContractHeader: Record "Service Contract Header";
                        begin
                            IF Rec."Service Contract Quote No." = '' then
                                EXIT;
                            IF ServiceContractHeader.GET(ServiceContractHeader."Contract Type"::Quote, Rec."Service Contract Quote No.") then
                                PAGE.RUN(PAGE::"Service Contract Quote", ServiceContractHeader)
                        end;
                    }
                    field("Service Order No."; Rec."Service Order No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Order', Comment = 'de-DE=Auftrag';
                        ToolTip = 'Service Order No.', Comment = 'de-DE=Serviceauftrag, auf das sich das Ticket bezieht';
                        Importance = Promoted;

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceHeader: Record "Service Header";
                        begin
                            IF Rec."Service Order No." = '' then
                                EXIT;
                            IF ServiceHeader.GET(ServiceHeader."Document Type"::Order, Rec."Service Order No.") then
                                PAGE.RUN(PAGE::"Service Order", ServiceHeader)
                        end;
                    }
                    field("ServiceContractNo"; Rec."Service Contract No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Contract', Comment = 'de-DE=Vertrag';
                        ToolTip = 'Service Contract No.', Comment = 'de-DE=Servicevertrag, auf das sich das Ticket bezieht';
                        Importance = Promoted;

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceContractHeader: Record "Service Contract Header";
                        begin
                            IF Rec."Service Contract No." = '' then
                                EXIT;
                            IF ServiceContractHeader.GET(ServiceContractHeader."Contract Type"::Contract, Rec."Service Contract No.") then
                                PAGE.RUN(PAGE::"Service Contract", ServiceContractHeader)
                        end;
                    }

                    field("Pst. Service Shipment No."; Rec."Pst. Service Shipment No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Pst. Shipment', Comment = 'de-DE=Geb. Lieferung';
                        ToolTip = 'Pst. Service Shipment No.', Comment = 'de-DE=Geb. Servicelieferung, auf das sich das Ticket bezieht';

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceShipmentHeader: Record "Service Shipment Header";
                        begin
                            IF Rec."Pst. Service Shipment No." = '' then
                                EXIT;
                            IF ServiceShipmentHeader.GET(Rec."Pst. Service Shipment No.") then
                                PAGE.RUN(PAGE::"Posted Service Shipment", ServiceShipmentHeader)
                        end;
                    }
                    field("Pst. Service Invoice No."; Rec."Pst. Service Invoice No.")
                    {
                        ApplicationArea = All;
                        Caption = 'Pst. Invoice', Comment = 'de-DE=Geb. Rechnung';
                        ToolTip = 'Pst. Service Invoice No.', Comment = 'de-DE=Geb. Servicerechnung, auf das sich das Ticket bezieht';
                        Importance = Promoted;

                        trigger OnAssistEdit() // Open Card
                        var
                            ServiceInvoiceHeader: Record "Service Invoice Header";
                        begin
                            IF Rec."Pst. Service Invoice No." = '' then
                                EXIT;
                            IF ServiceInvoiceHeader.GET(Rec."Pst. Service Invoice No.") then
                                PAGE.RUN(PAGE::"Posted Service Invoice", ServiceInvoiceHeader)
                        end;
                    }
                }

            }
            group(Service)
            {
                Caption = 'Service', Comment = 'de-DE=Servicemanagement';
                field("Service Item No."; Rec."Service Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Service Item No.', Comment = 'de-DE=Serviceartikelnr.';
                    Importance = Promoted;

                    trigger OnValidate()
                    begin
                        Rec.CalcFields("Service Item Description");
                        if (Rec."Service Item No." = '') and (Rec."Service Item No." <> xRec."Service Item No.") then
                            Rec."Serial No." := '';
                    end;

                    trigger OnAssistEdit()
                    var
                        ICITicketReferenceDialog: Page "ICI Ticket SItem Search Dialog";
                    begin
                        CurrPage.SaveRecord();
                        COMMIT();
                        ICITicketReferenceDialog.SetValues(Rec."No.", Rec."Reference No.");
                        IF ICITicketReferenceDialog.RunModal() = Action::OK THEN
                            CurrPage.Update(False);
                    end;
                }

                field("Serial No."; Rec."Serial No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Serial No.', Comment = 'de-DE=Seriennr.';
                    Importance = Promoted;
                    Editable = Rec."Service Item No." = '';

                    trigger OnAssistEdit()
                    var
                        ICITicketReferenceDialog: Page "ICI Ticket SItem Search Dialog";
                    begin
                        CurrPage.SaveRecord();
                        COMMIT();
                        ICITicketReferenceDialog.SetValues(Rec."No.", Rec."Serial No.");
                        IF ICITicketReferenceDialog.RunModal() = Action::OK THEN
                            CurrPage.Update(False);
                    end;
                }

                field("Service Item Description"; Rec."Service Item Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Service Item Description', Comment = 'de-DE=Serviceartikel Beschreibung';
                    Importance = Promoted;
                    trigger OnDrillDown()
                    var
                        ServiceItem: Record "Service Item";
                    begin
                        if Rec."Service Item No." = '' THEN
                            exit;

                        if not ServiceItem.get(Rec."Service Item No.") then
                            exit;

                        IF (Rec."Customer No." <> '') AND (ServiceItem."Customer No." = Rec."Customer No.") THEN
                            ServiceItem.SetRange("Customer No.", Rec."Customer No.");

                        PAGE.RUN(Page::"Service Item Card", ServiceItem);
                    end;
                }
                field("Item No."; Rec."Item No.")
                {
                    Importance = Standard;
                    ApplicationArea = All;
                    ToolTip = 'Item No.', Comment = 'de-DE=Artikelnr.';
                    Editable = Rec."Service Item No." = '';
                    trigger OnValidate()
                    begin
                        Rec.CalcFields("Item Description");
                    end;
                }
                field("Item Description"; Rec."Item Description")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Item Description', Comment = 'de-DE=Artikelbeschreibung';

                    trigger OnDrillDown()
                    var
                        Item: Record "Item";
                    begin
                        if Rec."Item No." = '' THEN
                            exit;
                        if not Item.get(Rec."Item No.") then
                            exit;
                        PAGE.RUN(Page::"Item Card", Item);
                    end;
                }

                field(ServiceItemCustomerNo; ServiceItemCustomerNo)
                {
                    Caption = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ToolTip = 'Customer No.', Comment = 'de-DE=Debitorennr.';
                    ApplicationArea = All;
                    StyleExpr = ServcieItemCustomerStypeExpr;
                    Importance = Standard;
                    Editable = false;

                    trigger OnDrillDown()
                    var
                        Customer: Record Customer;
                    begin
                        Customer.GET(ServiceItemCustomerNo);
                        PAGE.RUN(PAGE::"Customer Card", Customer);
                    end;
                }
                field(ServiceItemCustomerName; ServiceItemCustomerName)
                {
                    Caption = 'Customer Name', Comment = 'de-DE=Debitorenname';
                    ToolTip = 'Customer Name', Comment = 'de-DE=Debitorenname';
                    ApplicationArea = All;
                    StyleExpr = ServcieItemCustomerStypeExpr;
                    Importance = Standard;
                    Editable = false;
                    trigger OnDrillDown()
                    var
                        Customer: Record Customer;
                    begin
                        Customer.GET(ServiceItemCustomerNo);
                        PAGE.RUN(PAGE::"Customer Card", Customer);
                    end;
                }

                field("Service Contract Quote No."; Rec."Service Contract Quote No.")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    Visible = false;
                    Caption = 'Contract Quote', Comment = 'de-DE=Vertragsangebot';
                    ToolTip = 'Service Contract Quote No.', Comment = 'de-DE=Servicevertragsangebot, auf das sich das Ticket bezieht';

                    trigger OnAssistEdit() // Open Card
                    var
                        ServiceContractHeader: Record "Service Contract Header";
                    begin
                        IF Rec."Service Contract Quote No." = '' then
                            EXIT;
                        IF ServiceContractHeader.GET(ServiceContractHeader."Contract Type"::Quote, Rec."Service Contract Quote No.") then
                            PAGE.RUN(PAGE::"Service Contract Quote", ServiceContractHeader)
                    end;
                }
                field("Service Contract No."; Rec."Service Contract No.")
                {
                    Importance = Additional;
                    ApplicationArea = All;
                    Caption = 'Contract', Comment = 'de-DE=Vertrag';
                    ToolTip = 'Service Contract No.', Comment = 'de-DE=Servicevertrag, auf das sich das Ticket bezieht';

                    trigger OnAssistEdit() // Open Card
                    var
                        ServiceContractHeader: Record "Service Contract Header";
                    begin
                        IF Rec."Service Contract No." = '' then
                            EXIT;
                        IF ServiceContractHeader.GET(ServiceContractHeader."Contract Type"::Contract, Rec."Service Contract No.") then
                            PAGE.RUN(PAGE::"Service Contract", ServiceContractHeader)
                    end;
                }
            }

            part("ICI Support T. Item Subpage"; "ICI Support T. Item Subpage")
            {
                ApplicationArea = All;
                Caption = 'Support Ticket Item Subpage', Comment = 'de-DE=Artikel, Ressourcen, Serviceartikel und -kosten';
                SubPageLink = "Support Ticket No." = FIELD("No.");
            }
            part("ICI Support Time Log Subpage"; "ICI Support Time Log Subpage")
            {
                Visible = ShowTimeIntegration;
                ApplicationArea = All;
                Caption = 'Support Time Log Subpage', Comment = 'de-DE=Zeiterfassung';
                UpdatePropagation = SubPart;
                SubPageLink = "Support Ticket No." = FIELD("No.");
            }
            part("ICI Support Task Subpage"; "ICI Support Task Subpage")
            {
                Visible = ShowCRMFunction;
                ApplicationArea = All;
                UpdatePropagation = SubPart;
                SubPageLink = "ICI Support Ticket No." = FIELD("No.");
            }
            group(Shipment)
            {
                Caption = 'Shipment', Comment = 'de-DE=Lieferung';
                Visible = ShowShipment;
                group(ShipToAddress)
                {
                    Caption = 'Ship-to Address', Comment = 'de-DE=Lieferadresse/Standort';
                    field("Ship-to Code"; Rec."Ship-to Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Code', Comment = 'de-DE=Lief. an Code';
                    }
                    field("Ship-to Name"; Rec."Ship-to Name")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Name', Comment = 'de-DE=Lief. an Name';
                        Importance = Promoted;
                    }
                    field("Ship-to Name 2"; Rec."Ship-to Name 2")
                    {
                        Importance = Additional;
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Name 2', Comment = 'de-DE=Lief. an Name 2';
                    }
                    field("Ship-to Address"; Rec."Ship-to Address")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Address', Comment = 'de-DE=Lief. an Adresse';
                        Importance = Promoted;
                    }
                    field("Ship-to Address 2"; Rec."Ship-to Address 2")
                    {
                        Importance = Additional;
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Address 2', Comment = 'de-DE=Lief. an Adresse 2';
                    }
                    field("Ship-to Post Code"; Rec."Ship-to Post Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Post Code', Comment = 'de-DE=Lief. an PLZ';
                        Importance = Promoted;
                    }
                    field("Ship-to City"; Rec."Ship-to City")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Ship-to City', Comment = 'de-DE=Lief. an Ort';
                        Importance = Promoted;
                    }
                    field("Ship-to County"; Rec."Ship-to County")
                    {
                        Importance = Additional;
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Ship-to County', Comment = 'de-DE=Lief. an Bundesregion';
                    }
                    field("Ship-to Country/Region Code"; Rec."Ship-to Country/Region Code")
                    {
                        Importance = Additional;
                        ApplicationArea = All;
                        ToolTip = 'Ship-to Country/Region Code', Comment = 'de-DE=Lief. an Länder-/Regionencode';
                    }
                }
                field("Ship-to Phone No."; Rec."Ship-to Phone No.")
                {
                    ApplicationArea = All;
                    Importance = Promoted;
                    ToolTip = 'Ship-to Phone No.', Comment = 'de-DE=Lief. an Telefonnr';
                }
                group("Ship-to Options")
                {
                    Caption = 'Ship-to Options', Comment = 'de-DE=Lieferoptionen';
                    field("Shipment Method Code"; Rec."Shipment Method Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Shipment Method Code', Comment = 'de-DE=Lieferbedingungscode';
                    }
                    field("Shipping Agent Code"; Rec."Shipping Agent Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Shipping Agent Code', Comment = 'de-DE=Zustellercode';
                    }
                    field("Shipping Agent Service Code"; Rec."Shipping Agent Service Code")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Shipping Agent Service Code', Comment = 'de-DE=Zustellertransportartencode';
                    }
                    field("Location Code"; Rec."Location Code")
                    {
                        ApplicationArea = All;
                        Importance = Additional;
                        ToolTip = 'Location Code', Comment = 'de-DE=Lagerort';
                    }
                }
            }

        }
        area(FactBoxes)
        {
            /*part(NewMessage; "ICI Editor CardPart")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("No.");
                UpdatePropagation = Both;
            }*/
            part(ICIDragbox; "ICI Support Ticket Dragbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("No.");
                UpdatePropagation = SubPart;
            }
            part(ChatHistory; "ICI Ticket Communication CardP")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("No.");
                UpdatePropagation = SubPart;
            }
            part(TicketFactbox; "ICI Support Ticket Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("No.");
            }
            part(DocFactbox; "ICI Ticket Doc. Factbox")
            {
                ApplicationArea = All;
                Visible = ShowSalesDoc;
                SubPageLink = "No." = field("No.");
            }
            part("ICI Contact Ticket Factbox"; "ICI Contact Ticket Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Current Contact No.");
            }
            part(CustomerFactbox; "Customer Details FactBox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Customer No.");
            }
            part(ServiceItem; "ICI Service Item Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Service Item No.");
            }
            part(ServiceItemFactbox; "ICI Serv. Item Ticket Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Service Item No.");
            }
            part(ItemFactbox; "ICI Item Ticket Factbox")
            {
                ApplicationArea = All;
                SubPageLink = "No." = field("Item No.");
            }

        }

    }
    actions
    {
        area(Processing)
        {

            // action(NextState)
            // {
            //     Visible = NextStateAllowed;
            //     Caption = 'Next Process State', Comment = 'de-DE=Nächster Status';
            //     ToolTip = 'Set the Ticket into the next Process Stage', Comment = 'de-DE=Setzt das Ticket in den nächsten Status';

            //     ApplicationArea = All;
            //     Promoted = true;
            //     PromotedOnly = true;
            //     PromotedCategory = Process;
            //     Image = NextSet;

            //     trigger OnAction()
            //     var
            //         ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
            //     begin
            //         CurrPage.SaveRecord();
            //         ICISupportTicketProcMgt.NextProcessLineWithConfirm(Rec."No.");
            //         CurrPage.Update(false);
            //     end;
            // }
            action(OpenTicket)
            {
                Visible = OpenAllowed;
                Caption = 'Open', Comment = 'de-DE=Eröffnen';
                ToolTip = 'Set the Ticket into the next Process Stage', Comment = 'de-DE=Setzt das Ticket in den nächsten Status';

                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = NextSet;

                trigger OnAction()
                var
                    SupportTicket: Record "ICI Support Ticket";
                    ICISupportUser: Record "ICI Support User";
                    ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
                begin
                    ICISupportUser.GetCurrUser(ICISupportUser);

                    CurrPage.SaveRecord();
                    SupportTicket.GET(Rec."No.");
                    SupportTicket.SetModifiedByUser(UserId());
                    SupportTicket.Modify();
                    ICISupportTicketProcMgt.OpenTicket(Rec."No.", false); // Dont hide Confirm
                    CurrPage.Update(false);

                    if (SupportTicket."Support User ID" = ICISupportUser."User ID") then begin
                        ICISupportTicketProcMgt.AcceptTicket(Rec."No.");
                        CurrPage.Update(false);
                    end;
                end;
            }
            action(ProcessTicket)
            {
                Visible = ProcessAllowed;
                Caption = 'Accept', Comment = 'de-DE=Annehmen';
                ToolTip = 'Set the Ticket into the next Process Stage', Comment = 'de-DE=Setzt das Ticket in den nächsten Status';

                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = NextSet;

                trigger OnAction()
                var
                    SupportTicket: Record "ICI Support Ticket";
                    ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
                begin
                    CurrPage.SaveRecord();
                    SupportTicket.GET(Rec."No.");
                    SupportTicket.SetModifiedByUser(UserId());
                    SupportTicket.Modify();
                    ICISupportTicketProcMgt.AcceptTicket(Rec."No.");
                    CurrPage.Update(false);
                end;
            }
            action(WaitTicket)
            {
                Visible = WaitAllowed;
                Caption = 'Set Wait for Acceptance', Comment = 'de-DE=Bereit für Abnahme';
                ToolTip = 'Set the Ticket into the next Process Stage', Comment = 'de-DE=Setzt das Ticket in den nächsten Status';

                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = NextSet;

                trigger OnAction()
                var
                    SupportTicket: Record "ICI Support Ticket";
                    ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
                begin
                    CurrPage.SaveRecord();
                    SupportTicket.GET(Rec."No.");
                    SupportTicket.SetModifiedByUser(UserId());
                    SupportTicket.Modify();
                    ICISupportTicketProcMgt.WfATicket(Rec."No.");
                    CurrPage.Update(false);
                end;
            }
            action(CloseTicket)
            {
                Visible = CloseAllowed;
                Caption = 'Close', Comment = 'de-DE=Schließen';
                ToolTip = 'Set the Ticket into the next Process Stage', Comment = 'de-DE=Setzt das Ticket in den nächsten Status';

                ApplicationArea = All;
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = NextSet;

                trigger OnAction()
                var
                    SupportTicket: Record "ICI Support Ticket";
                    ICISupportTicketProcMgt: Codeunit "ICI Support Process Mgt.";
                    ConfirmForceCloseLbl: Label 'The Current Ticketstate is Waiting. Do you want to continue?', Comment = 'de-DE=Das Ticket steht in Abnahmewartestellung. Wollen Sie fortfahren?';
                begin

                    IF xRec."Ticket State" = Rec."Ticket State"::Waiting THEN // Bei Wfa muss man noch mal ein confirm durchlaufen
                        IF GuiAllowed then
                            IF NOT Confirm(ConfirmForceCloseLbl) THEN
                                exit;
                    CurrPage.SaveRecord();
                    SupportTicket.GET(Rec."No.");
                    SupportTicket.SetModifiedByUser(UserId());
                    SupportTicket.Modify();
                    ICISupportTicketProcMgt.CloseTicket(Rec."No.");
                    CurrPage.Update(false);
                end;
            }

            action(SearchDocumentNo)
            {
                ApplicationArea = All;
                Caption = 'Search Document No.', Comment = 'de-DE=Belegsuche';
                ToolTip = 'Search Document No.', Comment = 'de-DE=Belegsuche';
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = Navigate;

                trigger OnAction()
                var
                    ICITicketDocSearchDialog: Page "ICI Ticket Doc. Search Dialog";
                begin
                    CurrPage.SaveRecord();
                    COMMIT();
                    ICITicketDocSearchDialog.SetValues(Rec."No.", Rec."Reference No.");
                    IF ICITicketDocSearchDialog.RunModal() = Action::OK THEN
                        CurrPage.Update(False);
                end;
            }

            action(SearchSerialNo)
            {
                ApplicationArea = All;
                Caption = 'Search Serial No.', Comment = 'de-DE=Seriennummernsuche';
                ToolTip = 'Search Serial No.', Comment = 'de-DE=Seriennr. Suche';
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = Navigate;

                trigger OnAction()
                var
                    ICITicketSItemSearchDialog: Page "ICI Ticket SItem Search Dialog";
                begin
                    CurrPage.SaveRecord();
                    COMMIT();
                    ICITicketSItemSearchDialog.SetValues(Rec."No.", Rec."Reference No.");
                    IF ICITicketSItemSearchDialog.RunModal() = Action::OK THEN
                        CurrPage.Update(False);
                end;
            }
            action(SearchAddress)
            {
                ApplicationArea = All;
                Caption = 'Search Address', Comment = 'de-DE=Adresssuche';
                ToolTip = 'Search Address', Comment = 'de-DE=Adresssuche';
                Promoted = true;
                PromotedOnly = true;
                PromotedCategory = Process;
                Image = Navigate;

                trigger OnAction()
                var
                    ICITicketAddrSearchDialog: Page "ICI Ticket Addr. Search Dialog";
                begin
                    CurrPage.SaveRecord();
                    COMMIT();
                    ICITicketAddrSearchDialog.SetValues(Rec."No.", '', '', '', '', '', '');
                    IF ICITicketAddrSearchDialog.RunModal() = Action::OK THEN
                        CurrPage.Update(False);
                end;
            }

            action(AddSenderToBlacklist)
            {
                ApplicationArea = All;
                Caption = 'Add Sender To Blacklist', Comment = 'de-DE=Absender auf die Blacklist';
                ToolTip = 'Add Sender To Blacklist', Comment = 'de-DE=Absender auf die Blacklist';
                Image = CancelApprovalRequest;

                trigger OnAction()
                var
                    Contact: Record "Contact";
                    ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
                    ConfirmDeleteTxt: Label 'Do you also want to delete the Ticket?', Comment = 'de-DE=Wollen Sie das Ticket direkt löschen?';
                begin
                    CurrPage.SAVERECORD();
                    Contact.GET(Rec."Current Contact No.");
                    ICIMailrobotMgt.AddToBlacklist(Contact."E-Mail");
                    IF Confirm(ConfirmDeleteTxt) then
                        Rec.Delete(true);
                    CurrPage.Update(false);
                end;

            }
            action(SendRMA)
            {
                ApplicationArea = RelationshipMgmt;
                Caption = 'Send RMA Form', Comment = 'de-DE=RMA-Formular versenden';
                Image = SendMail;

                ToolTip = 'Send RMA Form', Comment = 'de-DE=RMA-Formular versenden';
                trigger OnAction()
                var
                    ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                begin
                    ICISupportMailMgt.SendRMAForTicket(Rec."No.");
                end;
            }
            action("Create Ship-To Address")
            {
                ApplicationArea = All;
                Caption = 'Create Ship-To Address', Comment = 'de-DE=Lieferadresse anlegen';
                ToolTip = 'Create Ship-To Address', Comment = 'de-DE=Lieferadresse anlegen';
                Image = ShipAddress;
                Visible = ShowShipment;

                trigger OnAction()
                begin
                    Rec.CreateShipToAddress();
                end;
            }
            action("Create Person Contact")
            {
                ApplicationArea = All;
                Caption = 'Create Person Contact', Comment = 'de-DE=Personenkontakt anlegen';
                ToolTip = 'Create Person Contact from Contact Data', Comment = 'de-DE=Aus den erfassten Kontaktdaten des Ansprechpartners wird ein neuer Personenkontakt erzeugt und dem Ticket zugewiesen';
                Image = AddContacts;
                Visible = ShowContactPersonCreation;

                trigger OnAction()
                begin
                    Rec.CreatePersonContact();
                end;

            }
            action(OpenPortal)
            {
                Caption = 'Open Portal', Comment = 'de-DE=Kundenansicht öffnen';
                ToolTip = 'Open Portal', Comment = 'de-DE=Im Kundenportal aus Kundensicht öffnen';
                ApplicationArea = All;
                Image = LaunchWeb;
                Visible = PortalIntegration AND Rec.Online;

                trigger OnAction();
                var
                    ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                begin
                    Hyperlink(ICISupportMailMgt.GenerateTicketLink(Rec."No."));
                end;
            }
            group("Activities & ToDo")
            {
                Caption = 'Activities & ToDo', Comment = 'de-DE=Aktivitäten & Aufgaben';
                Visible = ShowCRMFunction;
                action("Aufgabe erstellen")
                {
                    ApplicationArea = All;
                    Caption = 'Create To-do', Comment = 'de-DE=Aufgabe erstellen';
                    ToolTip = 'Create To-do', Comment = 'de-DE=Aufgabe erstellen';
                    Image = NewToDo;
                    Visible = ShowCRMFunction;

                    trigger OnAction()
                    var
                        SupportSetup: Record "ICI Support Setup";
                        TodoMgt: Codeunit "ICI Todo Mgt.";
                    begin
                        SupportSetup.GET();
                        IF not SupportSetup."Task Integration" then
                            exit;
                        TodoMgt.CreateTicketToDo(Rec);
                    end;
                }
                action("Create Activity")
                {
                    ApplicationArea = All;
                    Caption = 'Create Activity', Comment = 'de-DE=Aktivität erstellen';
                    ToolTip = 'Create Activity', Comment = 'de-DE=Aktivität erstellen';
                    Image = CreateInteraction;
                    Visible = ShowCRMFunction;

                    trigger OnAction()
                    var
                        SupportSetup: Record "ICI Support Setup";
                        TodoMgt: Codeunit "ICI Todo Mgt.";
                    begin
                        SupportSetup.GET();
                        IF not SupportSetup."Task Integration" then
                            exit;
                        TodoMgt.CreateInteractionFromTicket(Rec, '');
                    end;
                }
                action(CreateTelephoneCall)
                {
                    ApplicationArea = All;
                    Caption = 'Make Telephone Call', Comment = 'de-DE=Telefonat erstellen';
                    ToolTip = 'Create Telephone Call', Comment = 'de-DE=Telefonat erstellen';
                    Image = Calls;
                    Visible = ShowCRMFunction;

                    trigger OnAction()
                    var
                        SupportSetup: Record "ICI Support Setup";
                        TodoMgt: Codeunit "ICI Todo Mgt.";
                    begin
                        SupportSetup.GET();
                        IF not SupportSetup."Task Integration" then
                            exit;
                        IF SupportSetup."Phone Interaction Template" = '' THEN
                            exit;
                        TodoMgt.MakePhoneCallFromTicket(Rec, SupportSetup."Phone Interaction Template");
                    end;
                }
                action(CreateActivityLetter)
                {
                    ApplicationArea = All;
                    Caption = 'Create Activity Letter', Comment = 'de-DE=Brief erstellen';
                    ToolTip = 'Create Activity Letter', Comment = 'de-DE=Brief erstellen';
                    Image = DocumentEdit;
                    Visible = ShowCRMFunction;

                    trigger OnAction()
                    var
                        SupportSetup: Record "ICI Support Setup";
                        TodoMgt: Codeunit "ICI Todo Mgt.";
                    begin
                        SupportSetup.GET();
                        IF not SupportSetup."Task Integration" then
                            exit;
                        IF SupportSetup."Letter Interaction Template" = '' THEN
                            exit;
                        TodoMgt.CreateInteractionFromTicket(Rec, SupportSetup."Letter Interaction Template");
                    end;
                }
                action(CreateActivityMeeting)
                {
                    ApplicationArea = All;
                    Caption = 'Create Activity Meeting', Comment = 'de-DE=Termin erstellen';
                    ToolTip = 'Create Appointment', Comment = 'de-DE=Termin erstellen';
                    Image = CalendarChanged;
                    Visible = ShowCRMFunction;

                    trigger OnAction()
                    var
                        SupportSetup: Record "ICI Support Setup";
                        TodoMgt: Codeunit "ICI Todo Mgt.";
                    begin
                        SupportSetup.GET();
                        IF not SupportSetup."Task Integration" then
                            exit;
                        IF SupportSetup."Meeting Interaction Template" = '' THEN
                            exit;
                        TodoMgt.CreateInteractionFromTicket(Rec, SupportSetup."Meeting Interaction Template");
                    end;
                }
            }
        }
        area(Creation)
        {
            group(CreateSalesDocs)
            {
                Caption = 'Sales', Comment = 'de-DE=Verkauf';
                Image = Sales;
                action("Create Opportunity")
                {
                    Caption = 'Create Opportunity', Comment = 'de-DE=VK-Chance';
                    ToolTip = 'Create Opportunity from this Ticket', Comment = 'de-DE=Erstellt eine VK Chance zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowCRMFunction;
                    Image = Opportunity;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreateOpportunity(Rec);
                        CurrPage.UPDATE(FALSE);
                    end;
                }

                action("Create Offer")
                {
                    Caption = 'Create Offer', Comment = 'de-DE=VK-Angebot';
                    ToolTip = 'Create Offer from this Ticket', Comment = 'de-DE=Erstellt ein Angebot zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreateSalesDocument(Rec, "Sales Document Type"::Quote, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Order")
                {
                    Caption = 'Create Order', Comment = 'de-DE=VK-Auftrag';
                    ToolTip = 'Create Order from this Ticket', Comment = 'de-DE=Erstellt einen Auftrag zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreateSalesDocument(Rec, "Sales Document Type"::Order, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Invoice")
                {
                    Caption = 'Create Invoice', Comment = 'de-DE=VK-Rechnung';
                    ToolTip = 'Create Invoice from this Ticket', Comment = 'de-DE=Erstellt eine Rechnung zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreateSalesDocument(Rec, "Sales Document Type"::Invoice, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Return Order")
                {
                    Caption = 'Create Return Order', Comment = 'de-DE=VK-Reklamation';
                    ToolTip = 'Create Return Order from this Ticket', Comment = 'de-DE=Erstellt eine Reklamation zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        Rec.CreateOrUpdateServiceItem();
                        ICISupportDocumentMgt.CreateSalesDocument(Rec, "Sales Document Type"::"Return Order", WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Credit Memo")
                {
                    Caption = 'Create Credit Memo', Comment = 'de-DE=VK-Gutschrift';
                    ToolTip = 'Create Credit Memo from this Ticket', Comment = 'de-DE=Erstellt eine Gutschrift zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreateSalesDocument(Rec, "Sales Document Type"::"Credit Memo", WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
            }
            group(CreatePurchaseDocs)
            {
                Caption = 'Purchase', Comment = 'de-DE=Einkauf';
                Image = Purchase;
                action("Create Purchase Order")
                {
                    Caption = 'Create Purchase Order', Comment = 'de-DE=EK-Bestellung';
                    ToolTip = 'Create Purchase Order from this Ticket', Comment = 'de-DE=Erstellt eine Bestellung zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewInvoice;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreatePurchaseDocument(Rec, 1, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Purchase Return Order")
                {
                    Caption = 'Create Purchase Return Order', Comment = 'de-DE=EK-Reklamation';
                    ToolTip = 'Create Purchase Return Order from this Ticket', Comment = 'de-DE=Erstellt eine Reklamation zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewInvoice;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        ICISupportDocumentMgt.CreatePurchaseDocument(Rec, 5, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
            }
            group(CreateServiceDocs)
            {
                Caption = 'Service', Comment = 'de-DE=Service';
                Image = ServiceAgreement;
                action("Create Service Quote")
                {
                    Caption = 'Create Service Quote', Comment = 'de-DE=Serviceangebot';
                    ToolTip = 'Create Service Quote from this Ticket', Comment = 'de-DE=Erstellt ein Serviceangebot zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        Rec.CreateOrUpdateServiceItem();
                        ICISupportDocumentMgt.CreateServiceDocument(Rec, 0, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Service Order")
                {
                    Caption = 'Create Service Order', Comment = 'de-DE=Serviceauftrag';
                    ToolTip = 'Create Service Order from this Ticket', Comment = 'de-DE=Erstellt einen Serviceauftrag zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewSalesQuote;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        Rec.CreateOrUpdateServiceItem();
                        ICISupportDocumentMgt.CreateServiceDocument(Rec, 1, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                action("Create Service Invoice")
                {
                    Caption = 'Create Service Invoice', Comment = 'de-DE=Servicerechnung';
                    ToolTip = 'Create Service Invoice from this Ticket', Comment = 'de-DE=Erstellt eine Servicerechnung zu diesem Ticket';
                    ApplicationArea = All;
                    // Promoted = true;
                    // PromotedOnly = true;
                    Visible = ShowSalesDoc;
                    Image = NewInvoice;
                    // PromotedCategory = New;

                    trigger OnAction()
                    begin
                        CurrPage.SAVERECORD();
                        TestFieldNotInPreparation();
                        Rec.CreateOrUpdateServiceItem();
                        ICISupportDocumentMgt.CreateServiceDocument(Rec, 2, WORKDATE());
                        CurrPage.UPDATE(FALSE);
                    end;
                }
            }

            action(SendMailWithEditor)
            {
                ApplicationArea = RelationshipMgmt;
                Caption = 'E-Mail Editor', Comment = 'de-DE=Nachricht Erfassen';
                Image = SendMail;

                ToolTip = 'Opens the E-Mail Editor', Comment = 'de-DE=Öffnet den E-Mail Editor';
                trigger OnAction()
                var
                    Contact: Record Contact;
                    ICISupportMailSetup: Record "ICI Support Mail Setup";
                    ICISupportTextModule: Record "ICI Support Text Module";
                    TempNameValueBuffer: Record "Name/Value Buffer" temporary;
                    ICISupportUser: Record "ICI Support User";
                    ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
                    ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
                    ICINotificationDialog: Page "ICI Notification Dialog";
                    SendToEMail: Text[250];
                    EMailSubject: Text;
                    EMailBody: Text;
                    lInStream: InStream;
                begin
                    ICINotificationDialogMgt.SetTicket(Rec);

                    ICISupportMailSetup.GET();
                    ICISupportUser.GET(Rec."Support User ID");
                    Contact.GET(Rec."Current Contact No.");

                    // Textvorlage Neie Nachricht Holen
                    IF ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail C New Message", Contact."Language Code", ICISupportTextModule) THEN begin
                        EMailSubject := ICISupportTextModule.Description;
                        ICISupportTextModule.CalcFields(Data);
                        ICISupportTextModule.Data.CreateInStream(lInStream);
                        lInStream.Read(EMailBody);

                        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
                        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
                        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
                        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, Rec."No.");
                        ICISupportMailMgt.GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");

                        EMailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailBody);
                        EMailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailBody); // Apply 2 Times to Resolve Footer
                        EMailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, EMailSubject);
                    end;

                    ICINotificationDialogMgt.SetInitSubject(EMailSubject);
                    ICINotificationDialogMgt.SetInitText(EMailBody);// Textvorlage RMA setzen

                    // Create E-Mail Message
                    SendToEMail := Contact."E-Mail";
                    if Rec."Contact E-Mail" <> '' then
                        SendToEMail := Rec."Contact E-Mail";
                    IF ICISupportMailSetup."Test Mode" then
                        SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";

                    ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEmail, Enum::"Email Recipient Type"::"To", Contact.RecordId());

                    ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
                    ICINotificationDialog.Run();
                end;
            }

        }
        area(Navigation)
        {
            action(Log)
            {
                Caption = 'Log', Comment = 'de-DE=Protokoll';
                ToolTip = 'Log', Comment = 'de-DE=Öffnet das Ticketprotokoll';
                ApplicationArea = All;
                Image = Log;
                RunObject = Page "ICI Support Ticket Log ListP";
                RunPageLink = "Support Ticket No." = Field("No.");
            }
            action(SentMails)
            {
                Caption = 'Sent Mails', Comment = 'de-DE=Gesendete E-Mails';
                ToolTip = 'Sent Mails', Comment = 'de-DE=Öffnet die gesendeten E-Mails zu diesem Ticket';
                ApplicationArea = All;
                Image = Log;
                RunObject = Page "Sent Emails";
                trigger OnAction()
                var
                    EMail: Codeunit Email;
                begin
                    EMail.OpenSentEmails(Database::"ICI Support Ticket", Rec.SystemId);
                end;
            }
            action("Tasks")
            {
                ApplicationArea = RelationshipMgmt;
                Caption = 'T&asks', Comment = 'de-DE=Aufgaben';
                Image = TaskList;
                RunObject = Page "Task List";
                RunPageLink = "ICI Support Ticket No." = FIELD("No.");
                ToolTip = 'View all marketing tasks that involve the contact person.', Comment = 'de-DE=A&ufgaben';
                Visible = ShowCRMFunction;
            }
            action("Aktivitätenprotokollposten")
            {
                ApplicationArea = RelationshipMgmt;
                Caption = 'Interaction Log E&ntries', Comment = 'de-DE=Aktivitätenprotokollposten';
                Image = InteractionLog;
                RunObject = Page "Interaction Log Entries";
                RunPageLink = "ICI Support Ticket No." = FIELD("No.");
                ShortCutKey = 'Ctrl+F7';
                ToolTip = 'View a list of the interactions that you have logged, for example, when you create an interaction, print a cover sheet, a sales order, and so on.', Comment = 'de-DE=Aktivit&ätenprotokollposten';
                Visible = ShowCRMFunction;
            }
        }
        area(Reporting)
        {
            action("RMA-Bill")
            {
                ApplicationArea = All;
                Caption = 'RMA-Bill', Comment = 'de-DE=RMA-Schein';
                Image = "Report";
                ToolTip = 'Print the RMA Bill for this Ticket', Comment = 'de-DE=RMA Schein dieses Tickets ausdrucken';
                trigger OnAction()
                var
                    Contact: Record Contact;
                    ICISupportSetup: Record "ICI Support Setup";
                    Language: Record Language;
                    ReportLayoutSelection: Record "Report Layout Selection";
                    xReportLayoutSelection: Record "Report Layout Selection";
                begin
                    ICISupportSetup.GET();
                    CurrPage.SaveRecord();
                    Rec.SetRecFilter();
                    ICISupportSetup.TestField("RMA-Report ID");

                    // Set Report Layout depending on Language
                    Contact.GET(Rec."Current Contact No.");
                    IF Contact."Language Code" <> '' then begin
                        Language.GET(Contact."Language Code");
                        IF Language."ICI RMA Custom Layout Code" <> '' THEN begin
                            ReportLayoutSelection.GET(ICISupportSetup."RMA-Report ID", CompanyName());
                            xReportLayoutSelection := ReportLayoutSelection;

                            ReportLayoutSelection.VALIDATE("Custom Report Layout Code", Language."ICI RMA Custom Layout Code");
                            ReportLayoutSelection.Modify(false);
                        end;
                    end;

                    REPORT.RUN(ICISupportSetup."RMA-Report ID", false, false, Rec);

                    // Reset  Report Layout depending on Language
                    IF Contact."Language Code" <> '' then begin
                        Language.GET(Contact."Language Code");
                        IF Language."ICI RMA Custom Layout Code" <> '' THEN begin
                            ReportLayoutSelection.Validate(Type, xReportLayoutSelection.Type);
                            ReportLayoutSelection.Validate("Custom Report Layout Code", xReportLayoutSelection."Custom Report Layout Code");
                            ReportLayoutSelection.Modify(false);
                        end;
                    end;
                end;
            }
        }
    }

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        setProcessOptions();
        CurrPage.Update(false);
    end;

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.GET();
        ShowSalesDoc := ICISupportSetup."Document Integration";
        ShowTimeIntegration := ICISupportSetup."Time Registration";
        ShowCRMFunction := ICISupportSetup."Task Integration";
        PersonMandatory := ICISupportSetup."Person Mandatory";
        ShowShipment := ICISupportSetup."Use Shipment in Ticket";
        PortalIntegration := ICISupportSetup."Portal Integration";
        ShowContactPersonCreation := ICISupportSetup."Ticket Contact Creation";

        SetCategory2Caption();

        rec.Reset(); // Reset Filter for Card, so that the ticket doesnt "disappear" (e.g. with Statechange)
        // rec.Get("No.");

    end;

    trigger OnAfterGetRecord()
    var
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        ReferenceNoStyleExpr := Rec."Reference No. Style Expr";
        ICISupportTicket.GET(Rec."No.");
        ICISupportTicket.Validate("Highlight for User", false);
        ICISupportTicket.Modify(false);
        UpdateServiceItemCustomerInfo();
        setProcessOptions();
        CurrPage.Update(false);
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        ConfirmCloseWithTicketinPreparationLbl: Label 'The Ticket is still in preparation. Do you want to close anyways?', Comment = 'de-DE=Das Ticket befindet sich noch in Vorbereitung. Wollen Sie die Seite dennoch schließen?';
    begin
        if Rec."Ticket State" = Rec."Ticket State"::Preparation then
            EXIT(CONFIRM(ConfirmCloseWithTicketinPreparationLbl))
        else
            EXIT(true);
    end;

    local procedure UpdateServiceItemCustomerInfo()
    var
        lServiceItem: Record "Service Item";
    begin
        IF Rec."Service Item No." = '' then
            exit;
        IF NOT lServiceItem.GET(Rec."Service Item No.") THEN
            exit;

        ServiceItemCustomerNo := lServiceItem."Customer No.";

        lServiceItem.CalcFields(Name);
        ServiceItemCustomerName := lServiceItem.Name;

        Clear(ServcieItemCustomerStypeExpr);
        IF (Rec."Customer No." <> ServiceItemCustomerNo) then
            ServcieItemCustomerStypeExpr := 'Unfavorable'; // Rot Anzeigen, wenn Serviceartikel Debitor anders als Debitor
    end;

    local procedure SetCategory2Caption()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        // Caption Entweder "Unterkategorie" oder "Kategorie 2"
        // "Kategorie 2", wenn
        // Setup auf Stufen steht
        // "Unterkategorie", wenn 
        // setup auf Parent/Child steht

        Category2CodeTxt := Rec.FieldCaption("Category 2 Code");
        Category2DescriptionTxt := Rec.FieldCaption("Category 2 Description");

        ICISupportSetup.GET();
        IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Parent-Child Layers" then begin
            Category2CodeTxt := Rec.FieldCaption("Category 2 Code");
            Category2DescriptionTxt := Rec.FieldCaption("Category 2 Description");
        end;

        IF ICISupportSetup."Ticket Category Connection" = ICISupportSetup."Ticket Category Connection"::"Two-Layers" then begin
            Category2CodeTxt := Rec.FieldCaption("Category 1 Code") + ' 2';
            Category2DescriptionTxt := Rec.FieldCaption("Category 1 Description") + ' 2';
        end;
    end;

    local procedure setProcessOptions()
    begin
        TicketEditable := (Rec."Ticket State" = Rec."Ticket State"::Preparation) OR (Rec."Ticket State".AsInteger() < 1);

        CLEAR(OpenAllowed);
        Clear(ProcessAllowed);
        Clear(WaitAllowed);
        Clear(CloseAllowed);

        case Rec."Ticket State" of
            Rec."Ticket State"::Preparation:
                OpenAllowed := true;
            Rec."Ticket State"::Open:
                ProcessAllowed := true;
            Rec."Ticket State"::Processing:
                begin
                    WaitAllowed := true;
                    CloseAllowed := true;
                end;
            Rec."Ticket State"::Waiting:
                begin
                    CloseAllowed := true;
                    ProcessAllowed := true;
                end;
            Rec."Ticket State"::Closed:
                OpenAllowed := true;
        end;
    end;

    local procedure TestFieldNotInPreparation()
    var
        PreparationErrLbl: Label 'This action cannot be used on tickets in preparation', Comment = 'de-DE=Die ausgewählte Funktion steht für Tickets in Vorbereitung nicht zur Verfügung';
    begin
        IF Rec."Ticket State" = Rec."Ticket State"::Preparation then
            Error(PreparationErrLbl);
    end;

    var
        ICISupportDocumentMgt: Codeunit "ICI Support Document Mgt.";
        ShowSalesDoc: Boolean;
        ReferenceNoStyleExpr: Text;


        OpenAllowed: Boolean;

        ProcessAllowed: Boolean;

        WaitAllowed: Boolean;

        CloseAllowed: Boolean;


        TicketEditable: Boolean;
        ShowTimeIntegration: Boolean;
        ShowCRMFunction: Boolean;
        PersonMandatory: Boolean;
        ShowShipment: Boolean;
        ShowContactPersonCreation: Boolean;
        Category2CodeTxt: Text;
        Category2DescriptionTxt: Text;
        ServiceItemCustomerNo: code[20];
        ServiceItemCustomerName: Code[100];
        ServcieItemCustomerStypeExpr: Text;
        PortalIntegration: Boolean;
}
