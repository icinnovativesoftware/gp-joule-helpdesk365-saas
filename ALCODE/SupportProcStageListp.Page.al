page 56294 "ICI Support Proc. Stage Listp."
{

    Caption = 'ICI Support Process Stage Listpart', Comment = 'de-DE=Stufen';
    PageType = ListPart;
    SourceTable = "ICI Support Process Stage";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Process Code"; Rec."Process Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Process Code', Comment = 'de-DE=Prozess Code';
                }


                /*field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;

                    ToolTip = 'Language Code', Comment = 'de-DE=Sprachcode';
                }*/
                field(Stage; Rec.Stage)
                {
                    ApplicationArea = All;

                    ToolTip = 'Line No.', Comment = 'de-DE=Zeilennr.';
                }

                field(Description; Rec.Description)
                {
                    ApplicationArea = All;

                    ToolTip = 'Description', Comment = 'de-DE=Beschreibung';
                }
                field("Ticket State"; Rec."Ticket State")
                {
                    ApplicationArea = All;
                    ToolTip = 'Type', Comment = 'de-DE=Art';
                }
                field("E-Mail C Text Module Code"; Rec."E-Mail C Text Module Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail Contact Text Module Code', Comment = 'de-DE=Kontakt Textvorlage Code';
                }
                field("E-Mail U Text Module Code"; Rec."E-Mail U Text Module Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'E-Mail User Text Module Code', Comment = 'de-DE=Benutzer Textvorlage Code';
                }
                field(Initial; Rec.Initial)
                {
                    ApplicationArea = All;
                    ToolTip = 'Initial State when Ticket State is Changed', Comment = 'de-DE=Wird als die Initiale Stufe gesetzt, wenn das Ticket den Status wechselt';
                }
                field("Applys to Ticket State"; Rec."Applys to Ticket State")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Applys to Ticket State', Comment = 'de-DE=Bezieht sich auf Ticketstatus';
                }
                field("Activity Code"; Rec."Activity Code")
                {
                    Visible = ShowCRM;
                    ApplicationArea = All;
                    ToolTip = 'Activity Code', Comment = 'de-DE=Aktivitätencode';
                }
                field("Interaction Template Code"; Rec."Interaction Template Code")
                {
                    Visible = ShowCRM;
                    ApplicationArea = All;
                    ToolTip = 'Interaction Template', Comment = 'de-DE=Aktivitätenvorlagencode';
                }
                field("Department Code"; Rec."Department Code")
                {
                    ApplicationArea = All;
                    Importance = Additional;
                    ToolTip = 'Department Code', Comment = 'de-DE=Abteilungscode';
                }
                field(Progress; Rec.Progress)
                {
                    ApplicationArea = All;
                    ToolTip = 'Progress', Comment = 'de-DE=Gibt den geschätzten Fortschritt des Prozesses an';
                }
                field("Support User ID"; Rec."Support User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Support User ID', Comment = 'de-DE=Gibt den Supportbenutzer an, an den das Ticket weitergeleitet wird, wenn es in diese Stufe gesetzt wird';
                }
                field("Reset processing time";Rec."Reset processing time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Reset processing time', Comment = 'de-DE=Gibt an, ob die Bearbeitungszeit zurückgesetzt werden soll';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Translation)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Translations', Comment = 'de-DE=Übersetzungen';
                Image = Transactions;
                RunObject = page "ICI Proc. Stage Translations";
                RunPageLink = "Process Code" = field("Process Code"), "Process Stage" = field(Stage);

                ToolTip = 'Shows the Translations of this Stage', Comment = 'de-DE=Zeigt die Übersetzungen dieser Stufe an';
            }
        }
    }

    trigger OnNewRecord(BelowxRec: Boolean)
    var
        ICISupportProcessStage: Record "ICI Support Process Stage";
    begin
        Rec.FilterGroup(4); // SubpageLink from Process Card
        IF Rec.GetFilter("Process Code") <> '' then BEGIN
            Rec."Process Code" := COPYSTR(Rec.GetFilter("Process Code"), 1, 20);

            ICISupportProcessStage.SetRange("Process Code", Rec."Process Code");
            Rec.Stage := 10;
            IF ICISupportProcessStage.FindLast() then
                Rec.Stage := ICISupportProcessStage.Stage + 10;
        END;
    end;

    trigger OnOpenPage()
    var
        ICISupportSetup: Record "ICI Support Setup";
    begin
        ICISupportSetup.GET();
        ShowCRM := ICISupportSetup."Task Integration";
    end;

    var
        ShowCRM: Boolean;

}
