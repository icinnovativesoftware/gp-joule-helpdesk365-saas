codeunit 56280 "ICI Support Mail Mgt."
{
    var
        MailSendSuccessLbl: Label 'E-Mail sent successfully', Comment = 'de-DE=E-Mail erfolgreich gesendet';
        MailSendErrorLbl: Label 'Could not send E-Mail', Comment = 'de-DE=E-Mail konnte nicht verschickt werden';
        StateChangeErr: Label 'Could not E-Mail Statechange: Ticket No.: "%1" not found', Comment = '%1=Ticket No.|de-DE=Konnte Statuswechsel E-Mail nicht versenden. Ticketnr. "%1" konnte nicht gefunden werden';

    procedure SendMail(var ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportLog: Record "ICI Support Log";
        EmailItem: Record "Email Item";
        Email: Codeunit Email;
        EmailMessage: Codeunit "Email Message";
        EmailRelationRecordID: RecordId;
        lRecordRef: RecordRef;
        lFieldRef: FieldRef;
        EMailScenario: Enum "Email Scenario";
        lOutStream: OutStream;
        Recipients: List of [Text];
        CCRecipients: List of [Text];
        BCCRecipients: List of [Text];
        Subject: Text;
        Body: Text;
        TicketNo: Code[20];
        NotificationJSON: JsonObject;
        JTok: JsonToken;
        RecipientsArray: JsonArray;
        RecipientObj: JsonObject;
        AttachmentsArray: JsonArray;
        NoOfReceivers: Integer;
        NoOfAttachments: Integer;
        Counter: Integer;
        AttachmentObj: JsonObject;
        AttachmentName: Text[250];
        ContentType: Text[250];
        AttachmentBase64: Text;
        EMailRecipientTypeInt: Integer;
        RecipientEMail: Text;
        OriginalRecipientEMail: Text;
    begin
        // Check Setup
        ICISupportMailSetup.GET();
        IF ICISupportMailSetup."Test Mode" then
            ICISupportMailSetup.TestField("Test Mode E-Mail Receiver");

        // Get Mail Values
        NotificationJSON := ICINotificationDialogMgt.CreateNotificationValues();

        IF NotificationJSON.GET('TicketNo', JTok) then
            TicketNo := JTok.AsValue().AsText();

        // block E-Mail in case ticket is offline
        IF TicketNo <> '' THEN
            IF ICISupportTicket.GET(TicketNo) then
                if not ICISupportTicket.Online then
                    EXIT;

        IF NotificationJSON.GET('InitText', JTok) then
            Body := JTok.AsValue().AsText();

        IF NotificationJSON.GET('InitSubject', JTok) then
            Subject := JTok.AsValue().AsText();

        IF NotificationJSON.GET('Receivers', JTok) then
            RecipientsArray := JTok.AsArray();

        IF NotificationJSON.GET('Attachments', JTok) then
            AttachmentsArray := JTok.AsArray();

        // Add Receivers
        NoOfReceivers := RecipientsArray.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF RecipientsArray.GET(Counter, JTok) THEN begin
                CLEAR(RecipientObj);
                RecipientObj := JTok.AsObject();
                IF RecipientObj.Get('EMail', JTok) then
                    RecipientEMail := JTok.AsValue().AsText();

                IF RecipientObj.Get('EMailRecipientType', JTok) then
                    EMailRecipientTypeInt := JTok.AsValue().AsInteger();

                // Empfänger anhand der Empfängerart eintragen
                case EMailRecipientTypeInt of
                    Enum::"Email Recipient Type"::"To".AsInteger():
                        begin
                            OriginalRecipientEMail := RecipientEMail;
                            IF ICISupportMailSetup."Test Mode" and (ICISupportMailSetup."Test Mode E-Mail Receiver" <> '') then // Im Testmodus - Empfänger überschreiben
                                RecipientEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
                            Recipients.Add(RecipientEMail);
                        end;
                    Enum::"Email Recipient Type"::Cc.AsInteger():
                        begin
                            IF ICISupportMailSetup."Test Mode" and (ICISupportMailSetup."Test Mode E-Mail Receiver" <> '') then // Im Testmodus - Empfänger überschreiben
                                RecipientEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
                            CCRecipients.Add(RecipientEMail);
                        end;
                    Enum::"Email Recipient Type"::"BCC".AsInteger():
                        begin
                            IF ICISupportMailSetup."Test Mode" and (ICISupportMailSetup."Test Mode E-Mail Receiver" <> '') then // Im Testmodus - Empfänger überschreiben
                                RecipientEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
                            BCCRecipients.Add(RecipientEMail);
                        end;
                end;
            end;
        EMailScenario := GetEMailScenario(TicketNo);

        // Betreff und Inhalt im Testmodus änern
        IF ICISupportMailSetup."Test Mode" and (ICISupportMailSetup."Test Mode E-Mail Receiver" <> '') then begin
            Body += '<br/>' + StrSubstNo(TestModeLbl, OriginalRecipientEMail);
            Subject := TestModeSubjectLbl + ' ' + Subject;
        end Else
            BCCRecipients.Add((ICISupportMailSetup."E-Mail BCC"));

        EmailMessage.Create(Recipients, Subject, Body, true, CCRecipients, BCCRecipients);

        // Add Attachments - Must be After EmailMessage.Create
        NoOfAttachments := AttachmentsArray.Count();
        For Counter := 0 to (NoOfAttachments - 1) do // JSON Arrays start at 0
            IF AttachmentsArray.GET(Counter, JTok) THEN begin
                CLEAR(AttachmentObj);
                AttachmentObj := JTok.AsObject();
                IF AttachmentObj.Get('AttachmentName', JTok) then
                    AttachmentName := (JTok.AsValue().AsText());
                IF AttachmentObj.Get('ContentType', JTok) then
                    ContentType := (JTok.AsValue().AsText());
                IF AttachmentObj.Get('AttachmentBase64', JTok) then
                    AttachmentBase64 := (JTok.AsValue().AsText());

                // ADD Attachment
                EmailMessage.AddAttachment(AttachmentName, ContentType, AttachmentBase64);
            end;

        // Es könnte alles so einfach sein. Ist es aber nicht. Setzt Html Format der Mail
        IF EmailItem.Get(EmailMessage.GetId()) then begin
            EmailItem."Plaintext Formatted" := false;
            EmailItem.Modify();
        end;

        // Relations hinzufügen - Must be After EmailMessage.Create
        IF TicketNo <> '' THEN
            IF ICISupportTicket.GET(TicketNo) then
                IF lRecordRef.GET(ICISupportTicket.RecordId()) THEN begin
                    lFieldRef := lRecordRef.Field(lRecordRef.SystemIdNo);
                    Email.AddRelation(EmailMessage, ICISupportTicket.RecordId().TableNo, lFieldRef.Value, ENUM::"Email Relation Type"::"Primary Source", ENUM::"Email Relation Origin"::"Compose Context"); // Ticket als Primary Source hinzufügen
                end;

        NoOfReceivers := RecipientsArray.Count();
        For Counter := 0 to (NoOfReceivers - 1) do // JSON Arrays start at 0
            IF RecipientsArray.GET(Counter, JTok) THEN begin
                CLEAR(RecipientObj);
                RecipientObj := JTok.AsObject();
                IF RecipientObj.Get('RecordID', JTok) then
                    Evaluate(EmailRelationRecordID, JTok.AsValue().AsText());

                // Empfänger anhand der Empfängerart eintragen - Kann nur eine Primary Source geben. also immer Related Entity
                IF lRecordRef.GET(EmailRelationRecordID) THEN begin
                    lFieldRef := lRecordRef.Field(lRecordRef.SystemIdNo);
                    Email.AddRelation(EmailMessage, EmailRelationRecordID.TableNo, lFieldRef.Value, ENUM::"Email Relation Type"::"Related Entity", ENUM::"Email Relation Origin"::"Compose Context"); // Empfänger als Related Entity hinzufügen
                end;

            end;

        // Email versehden
        IF Email.Send(EmailMessage, EMailScenario) then begin
            ICISupportLog.INIT();
            ICISupportLog.Insert(TRUE);
            ICISupportLog.VALIDATE(Type, ICISupportLog.Type::Activity);
            ICISupportLog.VALIDATE(Description, MailSendSuccessLbl);
            ICISupportLog.Data.CreateOutStream(lOutStream);
            lOutStream.Write(STRSUBSTNO(LogLbl, GetLastErrorText(), FORMAT(CurrentDatetime()), '', FORMAT(Recipients), FORMAT(CCRecipients), FORMAT(BCCRecipients), Subject, Body));
            ICISupportLog.Modify(TRUE);
        end ELSE BEGIN
            ICISupportLog.INIT();
            ICISupportLog.Insert(TRUE);
            ICISupportLog.VALIDATE(Type, ICISupportLog.Type::Error);
            ICISupportLog.VALIDATE(Description, MailSendErrorLbl);
            ICISupportLog.Data.CreateOutStream(lOutStream);
            lOutStream.Write(STRSUBSTNO(LogLbl, GetLastErrorText(), FORMAT(CurrentDatetime()), '', FORMAT(Recipients), FORMAT(CCRecipients), FORMAT(BCCRecipients), Subject, Body));
            ICISupportLog.Modify(TRUE);
        END;
    end;

    local procedure GetEMailScenario(TicketNo: Code[20]) EMailScenario: Enum "Email Scenario"
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICIMailrobotMailbox: Record "ICI Mailrobot Mailbox";
    begin
        EMailScenario := "Email Scenario"::Helpdesk365;
        IF ICISupportTicket.GET(TicketNo) then
            IF ICISupportTicket."Mailrobot Source Mailbox Code" <> '' THEN
                IF ICIMailrobotMailbox.GET(ICISupportTicket."Mailrobot Source Mailbox Code") then
                    IF ICIMailrobotMailbox."Send from Email Scenario" <> ICIMailrobotMailbox."Send from Email Scenario"::Default then
                        EMailScenario := ICIMailrobotMailbox."Send from Email Scenario";
    end;

    procedure SendMail(Recipient: Text; Subject: Text; Body: Text; TicketNo: Code[20]; AttachmentName: Text[250]; ContentType: Text[250]; AttachmentBase64: Text; PrimarySourceRecordID: RecordId; RelatedEntityRecordID: RecordId)
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportLog: Record "ICI Support Log";
        EmailItem: Record "Email Item";
        Email: Codeunit Email;
        EmailMessage: Codeunit "Email Message";
        lRecordRef: RecordRef;
        lFieldRef: FieldRef;
        EMailScenario: Enum "Email Scenario";
        lOutStream: OutStream;
        Recipients: List of [Text];
        CCRecipients: List of [Text];
        BCCRecipients: List of [Text];
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();
        IF ICISupportMailSetup."Test Mode" then
            ICISupportMailSetup.TestField("Test Mode E-Mail Receiver");

        EMailScenario := GetEMailScenario(TicketNo);

        IF ICISupportMailSetup."Test Mode" and (ICISupportMailSetup."Test Mode E-Mail Receiver" <> '') then begin
            Recipients.Add(ICISupportMailSetup."Test Mode E-Mail Receiver");
            Body += '<br/>' + StrSubstNo(TestModeLbl, Recipient);
            Subject := TestModeSubjectLbl + ' ' + Subject;
        end Else begin
            Recipients.Add(Recipient);
            BCCRecipients.Add((ICISupportMailSetup."E-Mail BCC"));
        end;

        EmailMessage.Create(Recipients, Subject, Body, true, CCRecipients, BCCRecipients);

        IF (AttachmentName <> '') AND (ICISupportSetup."Send new File as Attachment") THEN
            EmailMessage.AddAttachment(AttachmentName, ContentType, AttachmentBase64);

        // Es könnte alles so einfach sein. Ist es aber nicht. Setzt Html Format der Mail
        IF EmailItem.Get(EmailMessage.GetId()) then begin
            // TODO - XXX - Evtl Sende E-mail adresse je Mailrobot Postach ursprung des Tickets ändern
            EmailItem."Plaintext Formatted" := false;
            EmailItem.Modify();
        end;

        IF PrimarySourceRecordID.TableNo <> 0 then
            IF lRecordRef.GET(PrimarySourceRecordID) THEN begin
                lFieldRef := lRecordRef.Field(lRecordRef.SystemIdNo);
                Email.AddRelation(EmailMessage, PrimarySourceRecordID.TableNo, lFieldRef.Value, ENUM::"Email Relation Type"::"Primary Source", ENUM::"Email Relation Origin"::"Compose Context");
            end;

        IF RelatedEntityRecordID.TableNo <> 0 then
            IF lRecordRef.GET(RelatedEntityRecordID) THEN begin
                lFieldRef := lRecordRef.Field(lRecordRef.SystemIdNo);
                Email.AddRelation(EmailMessage, RelatedEntityRecordID.TableNo, lFieldRef.Value, ENUM::"Email Relation Type"::"Related Entity", ENUM::"Email Relation Origin"::"Compose Context");
            end;

        IF Email.Send(EmailMessage, EMailScenario) then begin
            ICISupportLog.INIT();
            ICISupportLog.Insert(TRUE);
            ICISupportLog.VALIDATE(Type, ICISupportLog.Type::Activity);
            ICISupportLog.VALIDATE(Description, MailSendSuccessLbl);
            ICISupportLog.Data.CreateOutStream(lOutStream);
            lOutStream.Write(STRSUBSTNO(LogLbl, GetLastErrorText(), FORMAT(CurrentDatetime()), '', FORMAT(Recipients), FORMAT(CCRecipients), FORMAT(BCCRecipients), Subject, Body));
            ICISupportLog.Modify(TRUE);
        end ELSE BEGIN
            ICISupportLog.INIT();
            ICISupportLog.Insert(TRUE);
            ICISupportLog.VALIDATE(Type, ICISupportLog.Type::Error);
            ICISupportLog.VALIDATE(Description, MailSendErrorLbl);
            ICISupportLog.Data.CreateOutStream(lOutStream);
            lOutStream.Write(STRSUBSTNO(LogLbl, GetLastErrorText(), FORMAT(CurrentDatetime()), '', FORMAT(Recipients), FORMAT(CCRecipients), FORMAT(BCCRecipients), Subject, Body));
            ICISupportLog.Modify(TRUE);
        END;
    end;

    procedure AssistEditTextModule(var ICISupportTextModule: Record "ICI Support Text Module")
    var
        ICITextModuleEditorDialog: Page "ICI Text Module Editor Dialog";
    begin
        ICITextModuleEditorDialog.SetRecord(ICISupportTextModule);
        ICITextModuleEditorDialog.RUN();
    end;

    procedure GetEMailTextModule(TextModuleCode: Code[30]; LanguageCode: Code[10]; var ICISupportTextModule: Record "ICI Support Text Module"): Boolean
    begin
        IF ICISupportTextModule.GET(TextModuleCode, LanguageCode, ICISupportTextModule.Type::"E-Mail") THEN
            EXIT(TRUE)
        else
            EXIT(ICISupportTextModule.GET(TextModuleCode, '', ICISupportTextModule.Type::"E-Mail"));
    end;

    procedure GetSetupPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary)
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        NameValueBuffer.AddNewEntry('%setup_link_to_portal%', ICISupportSetup."Link to Portal");
        NameValueBuffer.AddNewEntry('%setup_link_to_guest_form%', ICISupportSetup."Link to Guest Form");
    end;

    procedure GetCompmanyPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary)
    var
        CompanyInformation: Record "Company Information";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        FooterText: Text;
        lInStream: InStream;
    begin
        CompanyInformation.GET();
        NameValueBuffer.AddNewEntry('%company_information_address%', CompanyInformation.Address);
        NameValueBuffer.AddNewEntry('%company_information_address_2%', CompanyInformation."Address 2");
        NameValueBuffer.AddNewEntry('%company_information_area%', CompanyInformation."Area");
        NameValueBuffer.AddNewEntry('%company_information_city%', CompanyInformation.City);
        NameValueBuffer.AddNewEntry('%company_information_email%', CompanyInformation."E-Mail");
        NameValueBuffer.AddNewEntry('%company_information_fax_no%', CompanyInformation."Fax No.");
        NameValueBuffer.AddNewEntry('%company_information_home_page%', CompanyInformation."Home Page");
        NameValueBuffer.AddNewEntry('%company_information_name%', CompanyInformation.Name);
        NameValueBuffer.AddNewEntry('%company_information_name_2%', CompanyInformation."Name 2");
        NameValueBuffer.AddNewEntry('%company_information_phone_no%', CompanyInformation."Phone No.");
        NameValueBuffer.AddNewEntry('%company_information_phone_no_2%', CompanyInformation."Phone No. 2");
        NameValueBuffer.AddNewEntry('%company_information_post_code%', CompanyInformation."Post Code");

        ICISupportMailSetup.GET();
        IF GetEMailTextModule(ICISupportMailSetup."E-Mail Footer", '', ICISupportTextModule) then BEGIN
            ICISupportTextModule.CalcFields(Data);
            ICISupportTextModule.Data.CreateInStream(lInStream);
            lInStream.Read(FooterText);
            NameValueBuffer.AddNewEntry('%footer%', FooterText);
        END;
    end;

    procedure GenerateTicketLink(TicketNo: Code[20]): Text
    var
        ICIPortalToken: Record "ICI Portal Token";
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportSetup: Record "ICI Support Setup";
        TicketLinkLbl: Label '%1?action=open_token_login&token=%2', Comment = '%1 = Link to Portal; %2 = Token|de-DE=%1?action=open_token_login&token=%2';
    begin
        IF TicketNo <> '' THEN
            IF ICISupportTicket.GET(TicketNo) THEN BEGIN
                ICISupportSetup.GET();
                ICIPortalToken.CreateForTicket(ICIPortalToken, ICISupportTicket);
                EXIT(STRSUBSTNO(TicketLinkLbl, ICISupportSetup."Link to Portal", ICIPortalToken.Token));
            end;

        EXIT('#');
    end;

    procedure GenerateContactLink(ContactNo: Code[20]): Text
    var
        ICIPortalToken: Record "ICI Portal Token";
        Contact: Record Contact;
        ICISupportSetup: Record "ICI Support Setup";
        Token: Text[32];
        TicketLinkLbl: Label '%1?action=open_token_login&token=%2', Comment = '%1 = Link to Portal; %2 = Token|de-DE=%1?action=open_token_login&token=%2';
    begin
        ICISupportSetup.GET();
        IF ContactNo <> '' THEN
            IF Contact.GET(ContactNo) THEN BEGIN
                Token := ICIPortalToken.GetCurrentContactToken(ContactNo);
                IF Token <> '' THEN
                    EXIT(STRSUBSTNO(TicketLinkLbl, ICISupportSetup."Link to Portal", Token));

                ICIPortalToken.CreateForContact(ICIPortalToken, Contact);
                EXIT(STRSUBSTNO(TicketLinkLbl, ICISupportSetup."Link to Portal", ICIPortalToken.Token));
            end;
        EXIT('#');
    end;

    procedure GetTicketPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary; TicketNo: Code[20])
    var
        ICISupportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
    begin
        ICISupportSetup.Get();
        IF TicketNo <> '' THEN
            IF ICISupportTicket.GET(TicketNo) THEN BEGIN
                ICISupportTicket.CalcFields("Category 1 Description", "Category 2 Description", "Support User Name", "Service Item Description", "Process Stage Description");
                NameValueBuffer.AddNewEntry('%ticket_no%', ICISupportTicket."No.");
                NameValueBuffer.AddNewEntry('%ticket_description%', ICISupportTicket.Description);
                NameValueBuffer.AddNewEntry('%ticket_category_1_description%', ICISupportTicket."Category 1 Description");
                NameValueBuffer.AddNewEntry('%ticket_category_2_description%', ICISupportTicket."Category 2 Description");
                NameValueBuffer.AddNewEntry('%ticket_company_contact_name%', ICISupportTicket."Comp. Contact Name");
                NameValueBuffer.AddNewEntry('%ticket_current_contact_name%', ICISupportTicket."Curr. Contact Name");
                NameValueBuffer.AddNewEntry('%ticket_support_user_name%', ICISupportTicket."Support User Name");
                NameValueBuffer.AddNewEntry('%ticket_serial_no%', ICISupportTicket."Serial No.");
                NameValueBuffer.AddNewEntry('%ticket_service_item_description%', ICISupportTicket."Service Item Description");
                NameValueBuffer.AddNewEntry('%ticket_service_item_no%', ICISupportTicket."Service Item No.");
                NameValueBuffer.AddNewEntry('%ticket_process_line_description%', ICISupportTicket."Process Stage Description");
                NameValueBuffer.AddNewEntry('%ticket_link%', GenerateTicketLink(TicketNo));
                NameValueBuffer.AddNewEntry('%portal_link%', ICISupportSetup."Link to Portal");
                NameValueBuffer.AddNewEntry('%portal_link_forgot_password%', Format(ICISupportSetup."Link to Portal") + '/support/de/helpdesk_start/?action=support_forgot_password');
            END;
    end;

    procedure GetContactPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary; ContactNo: Code[20])
    var
        Contact: Record Contact;
        Salutation: Text;
    begin
        IF ContactNo <> '' THEN
            IF Contact.GET(ContactNo) THEN BEGIN
                //Contact.CALCFIELDS("Company Name");
                InitContactSalutation(Contact);
                Salutation := Contact.GetSalutation("Salutation Formula Salutation Type"::Formal, Contact."Language Code");
                NameValueBuffer.AddNewEntry('%contact_no%', Contact."No.");
                NameValueBuffer.AddNewEntry('%contact_name%', Contact.Name);
                NameValueBuffer.AddNewEntry('%contact_name_2%', Contact."Name 2");
                NameValueBuffer.AddNewEntry('%contact_company_no%', Contact."Company No.");
                NameValueBuffer.AddNewEntry('%contact_company_name%', Contact."Company Name");
                NameValueBuffer.AddNewEntry('%contact_salutation%', Salutation);
                NameValueBuffer.AddNewEntry('%contact_email%', Contact."E-Mail");
                //NameValueBuffer.AddNewEntry('%contact_password%', Contact."ICI Support Password");
                NameValueBuffer.AddNewEntry('%contact_login%', Contact."ICI Login");
            END
    end;

    procedure InitContactSalutation(var Contact: Record Contact)
    var
        SupportMailSetup: Record "ICI Support Mail Setup";
    begin
        if contact."Salutation Code" = '' then begin
            SupportMailSetup.get();
            contact.validate("Salutation Code", SupportMailSetup."Salutation Code D");
            contact.modify();
        end;
    end;

    procedure InitSupportUserSalutation(var SupportUser: Record "ICI Support User")
    var
        SupportMailSetup: Record "ICI Support Mail Setup";
    begin
        if SupportUser."Salutation Code" = '' then begin
            SupportMailSetup.get();
            SupportUser.validate("Salutation Code", SupportMailSetup."Salutation Code D");
            SupportUser.modify();
        end;
    end;

    procedure InitSalutationFormula(SalutationCode: Code[10]; LanguageCode: Code[10])
    var
        SalutationFormula: Record "Salutation Formula";
        SupportMailSetup: Record "ICI Support Mail Setup";
    begin
        if not SalutationFormula.get(SalutationCode, LanguageCode, SalutationFormula."Salutation Type"::Formal) then begin
            SalutationFormula.init;
            SalutationFormula.validate("Salutation Code", "SalutationCode");
            SalutationFormula.validate("Language Code", "LanguageCode");
            SalutationFormula.Validate("Salutation Type", SalutationFormula."Salutation Type"::Formal);
            SalutationFormula.insert;
            case
                "LanguageCode" of
                '', 'DEU', 'DEA', 'DES':
                    SalutationFormula.Salutation := 'Sehr geehrte Damen und Herren,';
                else
                    SalutationFormula.Salutation := 'Dear Sir or Madam,';
            end;
            SalutationFormula.modify();
        end;
    end;

    procedure GetUserPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary; pUserID: Code[50])
    var
        ICISupportUser: Record "ICI Support User";
        Salutation: Text;
    begin
        IF pUserID <> '' THEN
            IF ICISupportUser.GET(pUserID) THEN BEGIN
                InitSupportUserSalutation(ICISupportUser);
                Salutation := ICISupportUser.GetSalutation("Salutation Formula Salutation Type"::Informal, ICISupportUser."Language Code");
                NameValueBuffer.AddNewEntry('%support_user_user_id%', ICISupportUser."User ID");
                NameValueBuffer.AddNewEntry('%support_user_salutation%', Salutation);
                NameValueBuffer.AddNewEntry('%support_user_email%', ICISupportUser."E-Mail");
                NameValueBuffer.AddNewEntry('%support_user_job_title%', ICISupportUser."Job Title");
                NameValueBuffer.AddNewEntry('%support_user_mobile_phone_no%', ICISupportUser."Mobile Phone No.");
                NameValueBuffer.AddNewEntry('%support_user_phone_no%', ICISupportUser."Phone No.");
                NameValueBuffer.AddNewEntry('%support_user_name%', ICISupportUser.Name);
                NameValueBuffer.AddNewEntry('%support_user_first_name%', ICISupportUser."First Name");
                NameValueBuffer.AddNewEntry('%support_user_middle_name%', ICISupportUser."Middle Name");
                NameValueBuffer.AddNewEntry('%support_user_surname%', ICISupportUser.Surname);
                //NameValueBuffer.AddNewEntry('%support_user_password%', ICISupportUser."Password");
                NameValueBuffer.AddNewEntry('%support_user_login%', ICISupportUser."Login");
            END
    end;

    procedure GetTicketLogPlaceholders(var NameValueBuffer: Record "Name/Value Buffer" temporary; EntryNo: Integer; Prefix: Text)
    var
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        lInStream: InStream;
        DataText: Text;
    begin
        IF Prefix = '' then
            Prefix := 'support_ticket_log';
        IF EntryNo <> 0 THEN
            IF ICISupportTicketLog.GET(EntryNo) THEN BEGIN
                NameValueBuffer.AddNewEntry('%' + Prefix + '_created_by%', ICISupportTicketLog."Created By");
                NameValueBuffer.AddNewEntry('%' + Prefix + '_created_by_type%', FORMAT(ICISupportTicketLog."Created By Type"));
                NameValueBuffer.AddNewEntry('%' + Prefix + '_creation_date%', FORMAT(ICISupportTicketLog."Creation Date"));
                NameValueBuffer.AddNewEntry('%' + Prefix + '_creation_time%', FORMAT(ICISupportTicketLog."Creation Time"));
                NameValueBuffer.AddNewEntry('%' + Prefix + '_data_text%', ICISupportTicketLog."Data Text");
                NameValueBuffer.AddNewEntry('%' + Prefix + '_log_description%', ICISupportTicketLogMgt.GetTicketLogDescription(EntryNo));
                NameValueBuffer.AddNewEntry('%' + Prefix + '_sender_name%', ICISupportTicketLogMgt.GetTicketLogSenderName(EntryNo));

                ICISupportTicketLog.CalcFields(Data);
                IF (ICISupportTicketLog.Type = ICISupportTicketLog.Type::"External File") OR (ICISupportTicketLog.Type = ICISupportTicketLog.Type::"Internal File") THEN
                    ICISupportTicketLog.Data.CreateInStream(lInStream, TextEncoding::Windows)
                else
                    ICISupportTicketLog.Data.CreateInStream(lInStream);
                lInStream.Read(DataText); // Readtext ließt nur eine Zeile
                NameValueBuffer.AddNewEntry('%' + Prefix + '_data%', DataText);
            end;
    end;



    procedure ApplyPlaceHolders(var NameValueBuffer: Record "Name/Value Buffer" temporary; MailText: Text): Text
    var
        lTextBuilder: TextBuilder;
    begin
        lTextBuilder.Append(MailText);
        IF NameValueBuffer.FindSet(false) then
            repeat
                lTextBuilder.Replace(NameValueBuffer.Name, NameValueBuffer.GetValue());
            until NameValueBuffer.Next() = 0;

        EXIT(lTextBuilder.ToText())
    end;

    procedure SendContactLogin(ContactNo: Code[20]; Password: Text[50]): Boolean
    var
        Contact: Record Contact;
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        //ICISupportLog: Record "ICI Support Log";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        EmptyRecID: RecordId;
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        IF NOT Contact.GET(ContactNo) THEN
            EXIT(FALSE);
        IF Contact."E-Mail" = '' THEN
            EXIT(false);
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT(FALSE);
        IF NOT ICISupportMailSetup."E-Mail C Login Active" THEN
            EXIT(FALSE);
        IF ICISupportMailSetup."E-Mail C Login" = '' THEN
            EXIT(FALSE);

        GetEMailTextModule(ICISupportMailSetup."E-Mail C Login", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        TempNameValueBuffer.AddNewEntry('%contact_password%', Password);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetContactPlaceholders(TempNameValueBuffer, ContactNo);

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        SendMail(Contact."E-Mail", MailSubject, MailBody, '', '', '', '', Contact.RecordId(), EmptyRecID);
        EXIT(true);
    end;

    procedure SendUserLogin(SupportUserID: Code[50]; Password: Text[50]): Boolean
    var
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        //ICISupportLog: Record "ICI Support Log";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        EmptyRecID: RecordId;
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        EXIT(false); // Benutzerzugang nicht möglich

        IF NOT ICISupportUser.GET(SupportUserID) THEN
            EXIT(false);
        IF ICISupportUser."E-Mail" = '' THEN
            EXIT(false);
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT(false);
        IF NOT ICISupportMailSetup."E-Mail U Login Active" THEN
            EXIT(false);
        IF ICISupportMailSetup."E-Mail U Login" = '' THEN EXIT(false);
        GetEMailTextModule(ICISupportMailSetup."E-Mail U Login", ICISupportUser."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        TempNameValueBuffer.AddNewEntry('%support_user_password%', Password);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetUserPlaceholders(TempNameValueBuffer, SupportUserID);

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        IF (ICISupportUser."E-Mail" = '') AND (ICISupportUser.Queue) THEN
            exit; // Keine Nachricht, wenn Warteschlangenbenutzer - ohne E-Mail

        SendMail(ICISupportUser."E-Mail", MailSubject, MailBody, '', '', '', '', ICISupportUser.RecordId(), EmptyRecID);
        EXIT(true);
    end;

    procedure SendContactProcessLineNoChanged(TicketNo: Code[20]; LineNo: Integer)
    var
        ICISUpportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportProcessStage: Record "ICI Support Process Stage";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        lInStream: InStream;
        MailBody: Text;
        TextModuleCode: Code[30];
        MailSubject: Text;
        NewStateText: Text;
        SendToEMail: Text;
    begin
        ICISUpportSetup.GET();
        IF ICISupportTicket.GET(TicketNo) THEN begin
            IF NOT ICISupportMailSetup.GET() THEN
                EXIT;
            // block E-Mail in case ticket is offline
            IF NOT ICISupportTicket.Online THEN
                EXIT;
            IF ICISUpportSetup."Person Mandatory" then // Personenkontakt nehmen, wenn pflicht 
                Contact.GET(ICISupportTicket."Current Contact No.")
            else
                IF ICISupportTicket."Current Contact No." <> '' THEN // Personenkontakt nehmen, wenn angegeben, sonst unternehmen
                    Contact.GET(ICISupportTicket."Current Contact No.")
                else
                    Contact.GET(ICISupportTicket."Company Contact No.");
            ICISupportUser.GET(ICISupportTicket."Support User ID");

            ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
            ICISupportProcessStage.SetRange(Stage, LineNo);
            ICISupportProcessStage.FindFirst();

            NewStateText := ICISupportProcessStage.Description;
            TextModuleCode := ICISupportProcessStage."E-Mail C Text Module Code";

            IF TextModuleCode = '' THEN
                EXIT;
            GetEMailTextModule(TextModuleCode, Contact."Language Code", ICISupportTextModule);

            MailSubject := ICISupportTextModule.Description;
            ICISupportTextModule.CalcFields(Data);
            ICISupportTextModule.Data.CreateInStream(lInStream);
            lInStream.Read(MailBody);

            GetCompmanyPlaceholders(TempNameValueBuffer);
            GetSetupPlaceholders(TempNameValueBuffer);
            GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
            GetTicketPlaceholders(TempNameValueBuffer, TicketNo);
            GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");

            // Get Ticket Log Placeholders
            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External Message");
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');
            IF ICISupportTicketLog.FindFirst() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'first_support_message');

            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::State);
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_state');

            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External File");
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_file');

            MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
            MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
            MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

            SendToEMail := Contact."E-Mail";
            if ICISupportTicket."Contact E-Mail" <> '' then
                SendToEMail := ICISupportTicket."Contact E-Mail";

            SendMail(SendToEMail, MailSubject, MailBody, TicketNo, '', '', '', ICISupportTicket.RecordId(), Contact.RecordId());
        end else
            Error(StateChangeErr, TicketNo);
    end;

    procedure SendUserProcessLineNoChanged(TicketNo: Code[20]; LineNo: Integer)
    var
        ICISUpportSetup: Record "ICI Support Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportProcessStage: Record "ICI Support Process Stage";
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        lInStream: InStream;
        MailBody: Text;
        TextModuleCode: Code[30];
        MailSubject: Text;
        NewStateText: Text;
    begin
        ICISUpportSetup.GET();
        IF ICISupportTicket.GET(TicketNo) THEN begin
            IF NOT ICISupportMailSetup.GET() THEN
                EXIT;
            IF ICISUpportSetup."Person Mandatory" then // Personenkontakt nehmen, wenn pflicht 
                Contact.GET(ICISupportTicket."Current Contact No.")
            else
                IF ICISupportTicket."Current Contact No." <> '' THEN // Personenkontakt nehmen, wenn angegeben, sonst unternehmen
                    Contact.GET(ICISupportTicket."Current Contact No.")
                else
                    Contact.GET(ICISupportTicket."Company Contact No.");
            ICISupportUser.GET(ICISupportTicket."Support User ID");

            IF (ICISupportUser."E-Mail" = '') AND (ICISupportUser.Queue) THEN
                exit; // Keine Nachricht, wenn Warteschlangenbenutzer - ohne E-Mail

            ICISupportProcessStage.SetRange("Process Code", ICISupportTicket."Process Code");
            ICISupportProcessStage.SetRange(Stage, LineNo);
            ICISupportProcessStage.FindFirst();

            NewStateText := ICISupportProcessStage.Description;
            TextModuleCode := ICISupportProcessStage."E-Mail U Text Module Code";

            IF TextModuleCode = '' THEN
                EXIT;
            GetEMailTextModule(TextModuleCode, ICISupportUser."Language Code", ICISupportTextModule);

            MailSubject := ICISupportTextModule.Description;
            ICISupportTextModule.CalcFields(Data);
            ICISupportTextModule.Data.CreateInStream(lInStream);
            lInStream.Read(MailBody);

            GetCompmanyPlaceholders(TempNameValueBuffer);
            GetSetupPlaceholders(TempNameValueBuffer);
            GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
            GetTicketPlaceholders(TempNameValueBuffer, TicketNo);
            GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");

            // Get Ticket Log Placeholders
            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External Message");
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');
            IF ICISupportTicketLog.FindFirst() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'first_support_message');

            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::State);
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_state');

            CLEAR(ICISupportTicketLog);
            ICISupportTicketLog.SetRange("Support Ticket No.", TicketNo);
            ICISupportTicketLog.SetRange(Type, ICISupportTicketLog.Type::"External File");
            IF ICISupportTicketLog.FindLast() THEN
                GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_file');

            MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
            MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
            MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

            SendMail(ICISupportUser."E-Mail", MailSubject, MailBody, TicketNo, '', '', '', ICISupportTicket.RecordId(), ICISupportUser.RecordId());
        end else
            Error(StateChangeErr, TicketNo);
    end;


    procedure NewSupportMessageForContact(TicketNo: Code[20]; EntryNo: Integer)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLog2: Record "ICI Support Ticket Log";
        ICISupportUser: Record "ICI Support User";
        ICISupportSetup: Record "ICI Support Setup";
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
    begin
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT;
        // block E-Mail in case ticket is offline
        IF NOT ICISupportTicket.Online THEN
            EXIT;
        // block E-Mail in case of GuestTicket
        if (ICISupportSetup."Guest Contact Company No." <> '') and (ICISupportTicket."Company Contact No." = ICISupportSetup."Guest Contact Company No.") then
            exit;
        IF NOT ICISupportUser.GET(ICISupportTicket."Support User ID") THEN
            EXIT;
        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT;
        IF NOT ICISupportMailSetup."E-Mail C New Message Active" THEN
            EXIT;
        IF ICISupportMailSetup."E-Mail C New Message" = '' THEN
            EXIT;
        IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation THEN
            EXIT;

        GetEMailTextModule(ICISupportMailSetup."E-Mail C New Message", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        GetTicketPlaceholders(TempNameValueBuffer, TicketNo);

        // Get Ticket Log Placeholders
        ICISupportTicketLog.GET(EntryNo);
        GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');

        ICISupportTicketLog2.SetRange("Support Ticket No.", TicketNo);
        ICISupportTicketLog2.SetRange(Type, ICISupportTicketLog.Type::"External Message");
        IF ICISupportTicketLog2.FindFirst() THEN
            GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog2."Entry No.", 'first_support_message');

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";

        SendMail(SendToEMail, MailSubject, MailBody, TicketNo, '', '', '', ICISupportTicket.RecordId(), Contact.RecordId());
    end;

    procedure NewSupportMessageForUser(TicketNo: Code[20]; EntryNo: Integer)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLog2: Record "ICI Support Ticket Log";
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT;
        IF NOT ICISupportUser.GET(ICISupportTicket."Support User ID") THEN
            EXIT;
        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT;
        IF NOT ICISupportMailSetup."E-Mail U New Message Active" THEN
            EXIT;
        IF ICISupportMailSetup."E-Mail U New Message" = '' THEN
            EXIT;
        IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation THEN
            EXIT;

        GetEMailTextModule(ICISupportMailSetup."E-Mail U New Message", ICISupportUser."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        GetTicketPlaceholders(TempNameValueBuffer, TicketNo);

        // Get Ticket Log Placeholders
        ICISupportTicketLog.GET(EntryNo);
        GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');

        ICISupportTicketLog2.SetRange("Support Ticket No.", TicketNo);
        ICISupportTicketLog2.SetRange(Type, ICISupportTicketLog.Type::"External Message");
        IF ICISupportTicketLog2.FindFirst() THEN
            GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog2."Entry No.", 'first_support_message');

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        IF (ICISupportUser."E-Mail" = '') AND (ICISupportUser.Queue) THEN
            exit; // Keine Nachricht, wenn Warteschlangenbenutzer - ohne E-Mail

        SendMail(ICISupportUser."E-Mail", MailSubject, MailBody, TicketNo, '', '', '', ICISupportTicket.RecordId(), ICISupportUser.RecordId());
    end;

    procedure TicketForwardedToUser(var ICISupportTicket: Record "ICI Support Ticket"; EntryNo: Integer)
    var

        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLog2: Record "ICI Support Ticket Log";
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        IF NOT ICISupportUser.GET(ICISupportTicket."Support User ID") THEN
            EXIT;
        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT;
        IF NOT ICISupportMailSetup."E-Mail U Forwarded Active" THEN
            EXIT;
        IF ICISupportMailSetup."E-Mail U Forwarded" = '' THEN
            EXIT;

        GetEMailTextModule(ICISupportMailSetup."E-Mail U Forwarded", ICISupportUser."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        // Get Ticket Log Placeholders
        ICISupportTicketLog.GET(EntryNo);
        GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');

        ICISupportTicketLog2.SetRange("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketLog2.SetRange(Type, ICISupportTicketLog.Type::"External Message");
        IF ICISupportTicketLog2.FindFirst() THEN
            GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog2."Entry No.", 'first_support_message');

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        IF (ICISupportUser."E-Mail" = '') AND (ICISupportUser.Queue) THEN
            exit; // Keine Nachricht, wenn Warteschlangenbenutzer - ohne E-Mail

        SendMail(ICISupportUser."E-Mail", MailSubject, MailBody, ICISupportTicket."No.", '', '', '', ICISupportTicket.RecordId(), ICISupportUser.RecordId());
    end;

    procedure TicketForwardedToContact(var ICISupportTicket: Record "ICI Support Ticket"; EntryNo: Integer)
    var
        Contact: Record Contact;
        ICISupportUser: Record "ICI Support User";
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportTicketLog2: Record "ICI Support Ticket Log";
        lInStream: InStream;
        MailBody: Text;
        MailSubject: Text;
    begin
        // block E-Mail in case ticket is offline
        IF NOT ICISupportTicket.Online THEN
            EXIT;
        IF NOT ICISupportUser.GET(ICISupportTicket."Support User ID") THEN
            EXIT;
        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT;
        IF NOT ICISupportMailSetup."E-Mail C Forwarded Active" THEN
            EXIT;
        IF ICISupportMailSetup."E-Mail C Forwarded" = '' THEN
            EXIT;

        GetEMailTextModule(ICISupportMailSetup."E-Mail C Forwarded", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        // Get Ticket Log Placeholders
        ICISupportTicketLog.GET(EntryNo);
        GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_message');

        ICISupportTicketLog2.SetRange("Support Ticket No.", ICISupportTicket."No.");
        ICISupportTicketLog2.SetRange(Type, ICISupportTicketLog.Type::"External Message");
        IF ICISupportTicketLog2.FindFirst() THEN
            GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog2."Entry No.", 'first_support_message');

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        // Ticket."Contact E-Mail" kann in diesem Fall noch nicht abweichend sein
        SendMail(Contact."E-Mail", MailSubject, MailBody, ICISupportTicket."No.", '', '', '', ICISupportTicket.RecordId(), Contact.RecordId());
    end;

    procedure NewSupportFileForContact(TicketNo: Code[20]; EntryNo: Integer)
    var
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record Contact;
        ICISupportTextModule: Record "ICI Support Text Module";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportTicketLog: Record "ICI Support Ticket Log";
        ICISupportUser: Record "ICI Support User";
        ICIMailrobotMgt: Codeunit "ICI Mailrobot Mgt.";
        lInStream: InStream;
        l2InStream: InStream;
        DataAsText: Text;
        MailBody: Text;
        MailSubject: Text;
        AttachmentName: Text[250];
        ContentType: Text[250];
        AttachmentBase64: Text;
        SendToEmail: Text;
    begin
        IF NOT ICISupportTicket.GET(TicketNo) THEN
            EXIT;
        // block E-Mail in case ticket is offline
        IF NOT ICISupportTicket.Online THEN
            EXIT;
        IF NOT ICISupportUser.GET(ICISupportTicket."Support User ID") THEN
            EXIT;
        IF NOT Contact.GET(ICISupportTicket."Current Contact No.") THEN
            EXIT;
        IF NOT ICISupportMailSetup.GET() THEN
            EXIT;
        IF NOT ICISupportMailSetup."E-Mail C New File Active" THEN
            EXIT;
        IF ICISupportMailSetup."E-Mail C New File" = '' THEN
            EXIT;
        IF ICISupportTicket."Ticket State" = ICISupportTicket."Ticket State"::Preparation THEN
            EXIT;

        GetEMailTextModule(ICISupportMailSetup."E-Mail C New File", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        GetCompmanyPlaceholders(TempNameValueBuffer);
        GetSetupPlaceholders(TempNameValueBuffer);
        GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");
        GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        GetTicketPlaceholders(TempNameValueBuffer, TicketNo);

        // Get Ticket Log Placeholders
        ICISupportTicketLog.GET(EntryNo);
        GetTicketLogPlaceholders(TempNameValueBuffer, ICISupportTicketLog."Entry No.", 'last_support_file');

        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        AttachmentName := ICISupportTicketLog."Data Text"; // Data Text is Filename
        ICISupportTicketLog.CalcFields(Data);
        ICISupportTicketLog.Data.CreateInStream(l2InStream, TextEncoding::UTF8);
        l2InStream.Read(DataAsText);


        // data looks loke this: data:text/plain;base64,L2h0ZG9jcy9...
        ContentType := ICIMailrobotMgt.Explode(DataAsText, ';'); // data looks loke this:base64,L2h0ZG9jcy9...
        ICIMailrobotMgt.Explode(DataAsText, ',');// data looks loke this: L2h0ZG9jcy9...
        AttachmentBase64 := DataAsText;

        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";

        SendMail(SendToEmail, MailSubject, MailBody, TicketNo, AttachmentName, ContentType, AttachmentBase64, ICISupportTicket.RecordId(), Contact.RecordId());
    end;


    // Opens BC Std Mail Editor with Attachment Support and saves the Sent E-Mail with Attachments to the Ticket Chat (if the Mail was sent)
    [Obsolete('moved to NoticationDialog')]
    procedure SendCustomMail(TicketNo: Code[20])
    var
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportTicket: Record "ICI Support Ticket";
        Contact: Record "Contact";
        ICISupportTicketLogMgt: Codeunit "ICI Support Ticket Log Mgt.";
        EmailMessage: Codeunit "Email Message";
        Email: Codeunit Email;
        ICISupDragboxMgt: Codeunit "ICI Sup. Dragbox Mgt.";
        lRecordRef: RecordRef;
        EMailScenario: Enum "Email Scenario";
        EmailAction: Enum "Email Action";
        EMailBody: Text;
        DataURI: Text;
        Filename: Text;
        SendToEMail: Text;
        FileSize: Integer;
        SubjectLbl: Label 'RE: %1 (Ticket %2)', Comment = '%1 = Ticketdescription;%2=Ticketno|de-DE=AW: %1 (Ticket %2)';
    begin
        ICISupportMailSetup.GET();
        ICISupportTicket.GET(TicketNo);
        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");

        SendToEMail := Contact."E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        EmailMessage.Create(Contact."E-Mail", strsubstno(SubjectLbl, ICISupportTicket.Description, ICISupportTicket."No."), '', true);

        EmailAction := EMail.OpenInEditorModally(EmailMessage, EMailScenario::Helpdesk365);
        IF EmailAction <> EmailAction::Sent then
            exit;
        EmailMessage.Get(EmailMessage.GetId()); // get again if user modified Texts or Attachments
        EMailBody := EmailMessage.GetBody();
        ICISupportTicketLogMgt.SaveTicketMessage(TicketNo, EMailBody, False, False);

        IF EmailMessage.Attachments_First() then
            repeat
                clear(DataURI);
                clear(Filename);
                Clear(FileSize);

                DataURI := 'data:' + EmailMessage.Attachments_GetContentType() + ';base64,' + EmailMessage.Attachments_GetContentBase64();
                Filename := EmailMessage.Attachments_GetName();
                FileSize := GetFileSizeByB64(EmailMessage.Attachments_GetContentBase64());

                ICISupDragboxMgt.setConfiguration('ICI SUPPORT TICKET');
                lRecordRef.GetTable(ICISupportTicket);
                ICISupDragboxMgt.setRecordRef(lRecordRef);

                ICISupDragboxMgt.InsertFile(Filename, DataURI, FileSize, false); // Dont send E-Mail
            until EmailMessage.Attachments_Next() = 0;
    end;

    // Opens BC Std Mail Editor with Attachment Support and saves the Sent E-Mail with Attachments to the Ticket Chat (if the Mail was sent)
    procedure SendRMAForTicket(TicketNo: Code[20])
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        Language: Record Language;
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportLayoutSelection: Record "Report Layout Selection";
        xReportLayoutSelection: Record "Report Layout Selection";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        EMailBody: Text;
        EMailSubject: Text;
        SendToEMail: Text;
        RMALbl: Label 'RMA-Form %1.pdf', Comment = '%1 = Ticketno;|de-DE=RMA-Formular %1.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportSetup.TestField("RMA-Report ID");
        ICISupportMailSetup.GET();
        ICISupportTicket.GET(TicketNo);
        lRecordRef.GetTable(ICISupportTicket);
        lRecordRef.SetRecFilter();
        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Textvorlage RMA Holen
        IF GetEMailTextModule(ICISupportMailSetup."E-Mail C RMA", Contact."Language Code", ICISupportTextModule) THEN begin
            EMailSubject := ICISupportTextModule.Description;
            ICISupportTextModule.CalcFields(Data);
            ICISupportTextModule.Data.CreateInStream(lInStream);
            lInStream.Read(EMailBody);

            GetCompmanyPlaceholders(TempNameValueBuffer);
            GetSetupPlaceholders(TempNameValueBuffer);
            GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
            GetTicketPlaceholders(TempNameValueBuffer, TicketNo);
            GetUserPlaceholders(TempNameValueBuffer, ICISupportUser."User ID");

            EMailBody := ApplyPlaceHolders(TempNameValueBuffer, EMailBody);
            EMailBody := ApplyPlaceHolders(TempNameValueBuffer, EMailBody); // Apply 2 Times to Resolve Footer
            EMailSubject := ApplyPlaceHolders(TempNameValueBuffer, EMailSubject);
        end;

        ICINotificationDialogMgt.SetInitSubject(EMailSubject);
        ICINotificationDialogMgt.SetInitText(EMailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Set Report Layout depending on Language
        IF Contact."Language Code" <> '' then begin
            Language.GET(Contact."Language Code");
            IF Language."ICI RMA Custom Layout Code" <> '' THEN begin
                ReportLayoutSelection.GET(ICISupportSetup."RMA-Report ID", CompanyName());
                xReportLayoutSelection := ReportLayoutSelection;

                ReportLayoutSelection.VALIDATE("Custom Report Layout Code", Language."ICI RMA Custom Layout Code");
                ReportLayoutSelection.Modify(false);
            end;
        end;

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if Report.SaveAs(ICISupportSetup."RMA-Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);

            txtB64 := Base64Convert.ToBase64(lInStream);
            ICINotificationDialogMgt.AddAttachment(StrSubstNo(RMALbl, ICISupportTicket."No."), 'application/pdf', txtB64); // PDF Anhang Setzen
        end;

        // Reset  Report Layout depending on Language
        IF Contact."Language Code" <> '' then begin
            Language.GET(Contact."Language Code");
            IF Language."ICI RMA Custom Layout Code" <> '' THEN begin
                ReportLayoutSelection.Validate(Type, xReportLayoutSelection.Type);
                ReportLayoutSelection.Validate("Custom Report Layout Code", xReportLayoutSelection."Custom Report Layout Code");
                ReportLayoutSelection.Modify(false);
            end;
        end;

        OnBeforeSetNotificatioNDialogForSendRMAinTicket(ICINotificationDialog, ICINotificationDialogMgt, ICISupportTicket);
        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachSalesHeaderToTicket(var SalesHeader: Record "Sales Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        SalesHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(SalesHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(SalesHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case SalesHeader."Document Type" OF
            SalesHeader."Document Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Quote");
            "Sales Document Type"::Order:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Order");
            "Sales Document Type"::Invoice:
                ERROR('Invoices can not be printed');
            "Sales Document Type"::"Credit Memo":
                ERROR('Credit Memos can not be printed');
            "Sales Document Type"::"Blanket Order":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Blanket");
            "Sales Document Type"::"Return Order":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Return");
        End;
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        case SalesHeader."Document Type" of
            "Sales Document Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Quote", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::Order:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Order", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::Invoice:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Invoice", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Credit Memo":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Cr.Memo", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Blanket Order":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Blanket Order", Contact."Language Code", ICISupportTextModule);
            "Sales Document Type"::"Return Order":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Sales Return Order", Contact."Language Code", ICISupportTextModule);
        end;
        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then begin
            TempBlob.CreateInStream(lInStream);

            txtB64 := Base64Convert.ToBase64(lInStream);
            ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, SalesHeader."Document Type", SalesHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen
        end;

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachSalesShipmentHeaderToTicket(var SalesShipmentHeader: Record "Sales Shipment Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        SalesShipmentHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(SalesShipmentHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(SalesShipmentHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesShipmentHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Shipment");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Shipment", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Lieferschein', SalesShipmentHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachSalesInvoiceHeaderToTicket(var SalesInvoiceHeader: Record "Sales Invoice Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        SalesInvoiceHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(SalesInvoiceHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(SalesInvoiceHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesInvoiceHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Invoice");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Invoice", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);
        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Rechnung', SalesInvoiceHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachSalesCrMemoHeaderToTicket(var SalesCrMemoHeader: Record "Sales Cr.Memo Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        SalesCrMemoHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(SalesCrMemoHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(SalesCrMemoHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(SalesCrMemoHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"S.Cr.Memo");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Pst. Sales Cr.Memo", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Gutschrift', SalesCrMemoHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachServiceHeaderToTicket(var ServiceHeader: Record "Service Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        ServiceHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(ServiceHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(ServiceHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case ServiceHeader."Document Type" OF
            ServiceHeader."Document Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Quote");
            ServiceHeader."Document Type"::Order:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Order");
            ServiceHeader."Document Type"::Invoice:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");
            ServiceHeader."Document Type"::"Credit Memo":
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");
        End;
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        case ServiceHeader."Document Type" of
            ServiceHeader."Document Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Quote", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::Order:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Order", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::Invoice:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Invoice", Contact."Language Code", ICISupportTextModule);
            ServiceHeader."Document Type"::"Credit Memo":
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Cr.Memo", Contact."Language Code", ICISupportTextModule);
        end;

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, ServiceHeader."Document Type", ServiceHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachServiceShipmentHeaderToTicket(var ServiceShipmentHeader: Record "Service Shipment Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        ServiceShipmentHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(ServiceShipmentHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(ServiceShipmentHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceShipmentHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Shipment");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Shipment", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if Not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Service - Lieferung', ServiceShipmentHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachServiceInvoiceHeaderToTicket(var ServiceInvoiceHeader: Record "Service Invoice Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        ServiceInvoiceHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(ServiceInvoiceHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(ServiceInvoiceHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceInvoiceHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Invoice");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Invoice", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Service - Rechnung', ServiceInvoiceHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachServiceCrMemoHeaderToTicket(var ServiceCrMemoHeader: Record "Service Cr.Memo Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        ServiceCrMemoHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(ServiceCrMemoHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(ServiceCrMemoHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceCrMemoHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Credit Memo");
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Posted Service Cr.Memo", Contact."Language Code", ICISupportTextModule);

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, 'Servicegutschrift', ServiceCrMemoHeader."No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;

    procedure AttachServiceContractHeaderToTicket(var ServiceContractHeader: Record "Service Contract Header")
    var
        ICISupportTicket: Record "ICI Support Ticket";
        ICISupportMailSetup: Record "ICI Support Mail Setup";
        ICISupportSetup: Record "ICI Support Setup";
        Contact: Record "Contact";
        ICISupportTextModule: Record "ICI Support Text Module";
        TempNameValueBuffer: Record "Name/Value Buffer" temporary;
        ICISupportUser: Record "ICI Support User";
        ReportSelections: Record "Report Selections";
        TempBlob: Codeunit "Temp Blob";
        Base64Convert: Codeunit "Base64 Convert";
        ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";
        ICISupportMailMgt: Codeunit "ICI Support Mail Mgt.";
        ICINotificationDialog: Page "ICI Notification Dialog";
        lRecordRef: RecordRef;
        lOutStream: OutStream;
        lInStream: InStream;
        txtB64: Text;
        MailBody: Text;
        MailSubject: Text;
        SendToEMail: Text;
        PDFNameLbl: Label '%1-%2.pdf', Comment = '%1=DocType;%2=DocNo|de-DE=%1-%2.pdf';
    begin
        ICISupportSetup.GET();
        ICISupportMailSetup.GET();

        ServiceContractHeader.TestField("ICI Support Ticket No.");
        ICISupportTicket.GET(ServiceContractHeader."ICI Support Ticket No.");
        IF NOT (ICISupportSetup."Document Integration") THEN
            ERROR('DocumentIntegration not licenced');

        ICISupportTicket.TestField("Current Contact No.");
        Contact.GET(ICISupportTicket."Current Contact No.");

        lRecordRef.GetTable(ServiceContractHeader);
        lRecordRef.SetRecFilter();
        lRecordRef.SetTable(ServiceContractHeader);

        Contact.GET(ICISupportTicket."Current Contact No.");
        Contact.TestField("E-Mail");
        ICISupportUser.GetCurrUser(ICISupportUser);

        ICINotificationDialogMgt.SetTicket(ICISupportTicket); // Ticket setzen

        // Get Report ID to Print
        ReportSelections.SETRANGE("Use for Email Attachment", TRUE);
        Case ServiceContractHeader."Contract Type" OF
            ServiceContractHeader."Contract Type"::Quote:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract Quote");
            ServiceContractHeader."Contract Type"::Contract:
                ReportSelections.SETRANGE(Usage, ReportSelections.Usage::"SM.Contract");
        End;
        ReportSelections.FindFirst();

        // Get Mail Subject and Body
        Case ServiceContractHeader."Contract Type" OF
            ServiceContractHeader."Contract Type"::Quote:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Contract Quote", Contact."Language Code", ICISupportTextModule);
            ServiceContractHeader."Contract Type"::Contract:
                ICISupportMailMgt.GetEMailTextModule(ICISupportMailSetup."E-Mail Service Contract", Contact."Language Code", ICISupportTextModule);
        End;

        MailSubject := ICISupportTextModule.Description;
        ICISupportTextModule.CalcFields(Data);
        ICISupportTextModule.Data.CreateInStream(lInStream);
        lInStream.Read(MailBody);

        // Add Placeholders
        ICISupportMailMgt.GetCompmanyPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetSetupPlaceholders(TempNameValueBuffer);
        ICISupportMailMgt.GetContactPlaceholders(TempNameValueBuffer, Contact."No.");
        ICISupportMailMgt.GetTicketPlaceholders(TempNameValueBuffer, ICISupportTicket."No.");

        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody);
        MailBody := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailBody); // Apply 2 Times to Resolve Footer
        MailSubject := ICISupportMailMgt.ApplyPlaceHolders(TempNameValueBuffer, MailSubject);

        ICINotificationDialogMgt.SetInitSubject(MailSubject);
        ICINotificationDialogMgt.SetInitText(MailBody);// Textvorlage RMA setzen

        // Create E-Mail Message
        SendToEMail := Contact."E-Mail";
        if ICISupportTicket."Contact E-Mail" <> '' then
            SendToEMail := ICISupportTicket."Contact E-Mail";
        IF ICISupportMailSetup."Test Mode" then
            SendToEMail := ICISupportMailSetup."Test Mode E-Mail Receiver";
        ICINotificationDialogMgt.AddReceiver(Contact."No.", Enum::"Notification Method Type"::Email, Contact.Name, SendToEMail, Enum::"Email Recipient Type"::"To", Contact.RecordId()); // Empfänger setzen

        // Print to PDF
        TempBlob.CreateOutStream(lOutStream, TextEncoding::Windows);
        if not Report.SaveAs(ReportSelections."Report ID", '', ReportFormat::Pdf, lOutStream, lRecordRef) then
            Error(CouldNotPrintErr);

        TempBlob.CreateInStream(lInStream);

        txtB64 := Base64Convert.ToBase64(lInStream);
        ICINotificationDialogMgt.AddAttachment(StrSubstNo(PDFNameLbl, ServiceContractHeader."Contract Type", ServiceContractHeader."Contract No."), 'application/pdf', txtB64); // PDF Anhang Setzen

        ICINotificationDialog.SetNotificationDialog(ICINotificationDialogMgt);
        ICINotificationDialog.Run();
    end;


    // Returns Filesize in Bytes
    // See: https://softwareengineering.stackexchange.com/questions/288670/know-file-size-with-a-base64-string
    procedure GetFileSizeByB64(B64: Text): Integer
    var
        last2Chars: Text;
        n: Integer;
        y: Integer;
    begin
        n := strlen(B64);
        last2Chars := CopyStr(B64, StrLen(B64) - 2, 2);
        y := 1;
        if last2Chars = '==' then
            y := 2;
        EXIT((n * (3 / 4)) - y);
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeSetNotificatioNDialogForSendRMAinTicket(ICINotificationDialog: Page "ICI Notification Dialog"; ICINotificationDialogMgt: Codeunit "ICI Notification Dialog Mgt.";

    var
        ICISupportTicket: Record "ICI Support Ticket")
    begin
    end;

    var
        LogLbl: Label 'ErrorText: %1 CreateTime:%2\ From E-Mail Address: %3\ To E-Mail Address: %4\ ToCC:%5\ ToBCC: %6\Subject:%7 \Body:%8', Comment = '%1=ErrorText;%2= CreateTime; %3= FromEmailAddress; %4= ToE-MailAddress; %5 = ToCC; %6 = ToBCC; %7 = Subject; %8 = Body|de-DE=FehlerText: %1\ Erstellt:%2\ Absender: %3\ Empfänger: %4\ CC:%5\ BCC: %6\ Betreff:%7 \Inhalt:%8';
        TestModeLbl: Label 'This E-Mail was sent in Test Mode. Normally the Recipient is %1', Comment = '%1=Receiver|de-DE=Diese E-Mail wurde im Testmodus versendet. Der eigentliche Empfänger lautet %1';
        TestModeSubjectLbl: Label 'TESTMODE', Comment = 'de-DE=TESTMODUS';
        CouldNotPrintErr: Label 'Error while generating Report', Comment = 'de-DE=Fehler bei der Berichtserstellung';
}
